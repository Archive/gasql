/* gasql_xml.c
 *
 * Copyright (C) 1999 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gasql_xml.h"

/* will reallocate a string, the one given as argument will be 
   freed */
gchar *
gasql_xml_clean_string (gchar * str)
{
	gint nb_spaces = 0;
	gint size = 0;
	gchar *ptr, *ptr2, *retval;

	if (str) {
		ptr = str;
		while (*ptr != '\0') {
			if ((*ptr == ' ') || (*ptr == '\n') || (*ptr == '\t'))
				nb_spaces++;
			size++;
			ptr++;
		}

		if (!nb_spaces)
			return str;

		retval = (gchar *) g_malloc (sizeof (gchar) * (size + 1));
		ptr = str;
		ptr2 = retval;
		while (*ptr != '\0') {
			if ((*ptr != ' ') && (*ptr != '\n') && (*ptr != '\t')) {
				*ptr2 = *ptr;
				ptr2++;
			}
			ptr++;
		}
		*ptr2 = '\0';
		g_free (str);
		return retval;
	}
	else
		return NULL;
}

/* will reallocate a string, the one given as argument will be 
   freed */
gchar *
gasql_xml_clean_string_ends (gchar * str)
{
	gint size;
	gchar *ptr, *ptr2, *retval;
	gchar *lptr = NULL;	/* points to the first caracter from the beginning 
				   which is not ' ', '\t' or '\n' */
	gchar *rptr = NULL;	/* points to the last caracter from the beginning 
				   which is not ' ', '\t' or '\n' */

	if (str) {
		ptr = str;
		while (*ptr != '\0') {
			if (!lptr && (*ptr != ' ') && (*ptr != '\n')
			    && (*ptr != '\t'))
				lptr = ptr;	/* left */
			if ((*ptr != ' ') && (*ptr != '\n') && (*ptr != '\t'))
				rptr = ptr;	/* right */
			size++;
			ptr++;
		}

		if (!lptr) {	/* only spaces!!! */
			g_free (str);
			return NULL;
		}

		size = (rptr - lptr) + 1;

		retval = (gchar *) g_malloc (sizeof (gchar) * (size + 1));
		ptr = lptr;
		ptr2 = retval;
		while ((ptr != rptr + 1)) {
			*ptr2 = *ptr;
			ptr2++;
			ptr++;
		}
		*ptr2 = '\0';
		g_free (str);
		return retval;
	}
	else
		return NULL;
}
