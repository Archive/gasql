/* canvas-field.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "canvas-field.h"

static void canvas_field_class_init (CanvasFieldClass * class);
static void canvas_field_init (CanvasField * item);
static void canvas_field_destroy (GtkObject * object);

static void canvas_field_set_arg    (GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id);
static void canvas_field_get_arg    (GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id);


enum
{
	ARG_0,
	ARG_CANVAS_QUERY_VIEW,
	ARG_FIELD
};


guint
canvas_field_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"CanvasField",
			sizeof (CanvasField),
			sizeof (CanvasFieldClass),
			(GtkClassInitFunc) canvas_field_class_init,
			(GtkObjectInitFunc) canvas_field_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (canvas_base_get_type (), &f_info);
	}

	return f_type;
}

static void
canvas_field_class_init (CanvasFieldClass * class)
{
	GtkObjectClass *object_class = NULL;


	object_class = (GtkObjectClass *) class;

	object_class->destroy = canvas_field_destroy;

	/* Arguments */
	gtk_object_add_arg_type ("CanvasField::canvas_query_view", GTK_TYPE_POINTER, GTK_ARG_READWRITE, 
				 ARG_CANVAS_QUERY_VIEW);
	gtk_object_add_arg_type ("CanvasField::field", GTK_TYPE_POINTER, GTK_ARG_READWRITE, ARG_FIELD);
	object_class->set_arg = canvas_field_set_arg;
	object_class->get_arg = canvas_field_get_arg;
	
}


static void enter_notify_cb (CanvasField * cf, gpointer data);
static void leave_notify_cb (CanvasField * cf, gpointer data);
static void
canvas_field_init (CanvasField * cf)
{
	cf->cqv = NULL;
	cf->bg = NULL;
	cf->text = NULL;

	/* connect to the "enter_notify" & "leave_notify" signals" */
	gtk_signal_connect (GTK_OBJECT (cf), "enter_notify",
			    GTK_SIGNAL_FUNC (enter_notify_cb), NULL);
	gtk_signal_connect (GTK_OBJECT (cf), "leave_notify",
			    GTK_SIGNAL_FUNC (leave_notify_cb), NULL);
}

static void 
enter_notify_cb (CanvasField * cf, gpointer data)
{
	canvas_field_set_highlight (cf, TRUE);
}

static void 
leave_notify_cb (CanvasField * cf, gpointer data)
{
	canvas_field_set_highlight (cf, FALSE);
}

static void
canvas_field_destroy (GtkObject * object)
{
	GtkObjectClass *parent_class = NULL;

	parent_class = gtk_type_class (canvas_base_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CANVAS_FIELD (object));

	canvas_field_init (CANVAS_FIELD (object));
	
	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void post_init (CanvasField * cf);
static void 
canvas_field_set_arg    (GtkObject            *object,
			 GtkArg               *arg,
			 guint                 arg_id)
{
	CanvasField *cf;
	gpointer ptr;

	cf = CANVAS_FIELD (object);

	switch (arg_id) {
	case ARG_CANVAS_QUERY_VIEW:
		ptr = GTK_VALUE_POINTER (*arg);
		if (ptr)
			cf->cqv = CANVAS_QUERY_VIEW (ptr);
		break;
	case ARG_FIELD:
		ptr = GTK_VALUE_POINTER (*arg);
		if (ptr) { 
			if (IS_QUERY_FIELD (ptr) || IS_DB_FIELD (ptr)) {
				cf->field = ptr;
				if (IS_DB_FIELD (ptr))
					gnome_canvas_item_set (GNOME_CANVAS_ITEM (cf),
							       "tooltip_object", ptr,
							       NULL);
			}
			else
				g_warning ("ARG_FIELD is of unknown type!\n");
		}
		break;
	}

	if (cf->cqv && cf->field)
		post_init (cf);

}

static void 
canvas_field_get_arg    (GtkObject            *object,
			GtkArg               *arg,
			guint                 arg_id)
{
	g_print ("GetArg %d\n", arg_id);
}


static int field_item_event(GnomeCanvasItem *ci, GdkEvent *event, CanvasField *cf);
static void 
post_init (CanvasField * cf)
{
	GnomeCanvasItem *item, *text;
	gchar *str, *font;

	/* Building new field */
	if (IS_DB_FIELD (cf->field))
		str = DB_FIELD (cf->field)->name;
	else
		str = QUERY_FIELD (cf->field)->name;
	
	gnome_canvas_item_set (GNOME_CANVAS_ITEM (cf), 
			       "allow_move", FALSE,
			       "allow_drag", TRUE,
			       NULL);

	/* text: field's name */
	if (IS_DB_FIELD (cf->field) && DB_FIELD (cf->field)->is_key)
		font = CQV_DEFAULT_VIEW_FONT_BOLD;
	else
		font = CQV_DEFAULT_VIEW_FONT;

	text = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cf),
				     GNOME_TYPE_CANVAS_TEXT,
				     "x", cf->cqv->x_text_space,
				     "y", 0.,
				     "font", font,
				     "text", str,
				     "fill_color", "black",
				     "justification", GTK_JUSTIFY_RIGHT, 
				     "anchor", GTK_ANCHOR_NORTH_WEST, 
				     NULL);

	cf->text = text;
	/* UI metrics */
	cf->cqv->fields_text_height = text->y2 - text->y1;
	if (text->x2 - text->x1 + cf->cqv->x_text_space > cf->cqv->max_text_width)
		cf->cqv->max_text_width = text->x2 - text->x1 + cf->cqv->x_text_space;

	/* underline for non null values */
	if (IS_DB_FIELD (cf->field) && !(DB_FIELD (cf->field)->null_allowed)) {
		GnomeCanvasPoints *points;
		points = gnome_canvas_points_new (2);

		/* fill out the points */
		points->coords[0] = cf->cqv->x_text_space;
		points->coords[1] = text->y2 - text->y1;
		points->coords[2] = cf->cqv->x_text_space + text->x2 - text->x1;
		points->coords[3] = points->coords[1];
		gnome_canvas_item_new(GNOME_CANVAS_GROUP (cf),
				      gnome_canvas_line_get_type(),
				      "points", points,
				      "fill_color", "black",
				      "width_units", 1.,
				      NULL);
		gnome_canvas_points_free(points);
	}

	/* background */
	item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cf),
				     GNOME_TYPE_CANVAS_RECT,
				     "x1", cf->cqv->x_text_space,
				     "y1", 0.,
				     "x2", cf->cqv->x_text_space + text->x2 - text->x1,
				     "y2", text->y2 - text->y1,
				     "fill_color", "white",
				     NULL);

	cf->bg = item;
	gnome_canvas_item_lower_to_bottom (item);	


	/* Signals */
	gtk_signal_connect(GTK_OBJECT (cf), "event",
			   GTK_SIGNAL_FUNC(field_item_event), cf);
}


static int 
field_item_event(GnomeCanvasItem *ci, GdkEvent *event, CanvasField *cf)
{
	gboolean done = TRUE;

	switch (event->type) {
	case GDK_ENTER_NOTIFY:
		/* canvas_field_set_highlight (cf, TRUE); */
		break;
	case GDK_BUTTON_PRESS:
		if (((GdkEventButton *) event)->button != 1) {
			done = TRUE;
			break;
		}
	case GDK_LEAVE_NOTIFY:
		/* canvas_field_set_highlight (cf, FALSE); */
		break;
	default:
		done = FALSE;
		break;
	}

	return done;	
}

void 
canvas_field_set_highlight (CanvasField *cf, gboolean highlight)
{

	g_return_if_fail (IS_CANVAS_FIELD (cf));

	if (highlight) {
		gchar *str;
		DbTable *table;

		if (IS_DB_FIELD (cf->field)) {
			table = database_find_table_from_field (cf->cqv->query->conf->db, DB_FIELD (cf->field));
			g_assert (table);
			if (table->is_view)
				str = CQV_VIEW_COLOR;
			else
				str = CQV_TABLE_COLOR;
		}
		else 
			str = CQV_QUERY_COLOR;

		gnome_canvas_item_set (cf->bg, "fill_color", str, NULL);
	}
	else 
		gnome_canvas_item_set (cf->bg, "fill_color", "white", NULL);
}
