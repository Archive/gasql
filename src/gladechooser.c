/* gladechooser.c
 *
 * Copyright (C) 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gladechooser.h"
#include <parser.h>

static void      glade_chooser_class_init      (GladeChooserClass *class);
static void      glade_chooser_init            (GladeChooser *gchooser);
static void      glade_chooser_post_init       (GladeChooser *gchooser);
static void      glade_chooser_destroy         (GtkObject *object);

enum {
	FORM_CHANGED,
	LAST_SIGNAL
};

static gint glade_chooser_signals[LAST_SIGNAL] = { 0 }; 


guint 
glade_chooser_get_type(void) 
{
	static guint f_type = 0;
  
	if (!f_type)
		{
			GtkTypeInfo f_info =
				{
					"Glade_Chooser",
					sizeof (GladeChooser),	
					sizeof (GladeChooserClass),
					(GtkClassInitFunc) glade_chooser_class_init,
					(GtkObjectInitFunc) glade_chooser_init,
					(GtkArgSetFunc) NULL,
					(GtkArgGetFunc) NULL
				};
      
			f_type = gtk_type_unique (gtk_vbox_get_type (), &f_info);
		}
  
	return f_type;   
}

static void my_form_changed (GladeChooser *gchooser);

static void 
glade_chooser_class_init (GladeChooserClass *class)
{
	GtkObjectClass *object_class = NULL;

	object_class = (GtkObjectClass*) class;
	glade_chooser_signals[FORM_CHANGED] =
		gtk_signal_new ("form_changed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (GladeChooserClass, form_changed),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
  
	gtk_object_class_add_signals (object_class, glade_chooser_signals, 
				      LAST_SIGNAL);
	class->form_changed = my_form_changed;

	object_class->destroy = glade_chooser_destroy;
}

static void my_form_changed (GladeChooser *gchooser)
{
}

static void 
glade_chooser_init(GladeChooser *gchooser)
{
	gchooser->form_struct = g_new0 (GladeFormStruct, 1);
	gchooser->glade_widget = NULL;
}

GtkWidget* 
glade_chooser_new(void)
{
	GtkObject *obj;
	GladeChooser *gchooser;

	obj = gtk_type_new(glade_chooser_get_type());
	gchooser = GLADE_CHOOSER(obj);

	glade_chooser_post_init(gchooser);
  
	return GTK_WIDGET(obj);
}

static void 
glade_chooser_destroy(GtkObject *object)
{
	GladeChooser *gchooser;
	GtkObjectClass *parent_class = NULL;

	parent_class = gtk_type_class (gtk_vbox_get_type());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_GLADE_CHOOSER (object));
  
	gchooser = GLADE_CHOOSER (object);

	if (gchooser->form_struct->glade_file_name)
		g_free(gchooser->form_struct->glade_file_name);
	if (gchooser->form_struct->top_widget_name)
		g_free(gchooser->form_struct->top_widget_name);
	if (gchooser->form_struct->gxml)
		gtk_object_unref(GTK_OBJECT(gchooser->form_struct->gxml));
	g_free (gchooser->form_struct);

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(* GTK_OBJECT_CLASS (parent_class)->destroy) (object); 
}


static void browse_clicked_cb (GtkEntry *entry, GladeChooser *gchooser);
static void top_level_widget_changed (GtkEntry *entry, GladeChooser *gchooser);
static void top_level_widget_chosen_cb (GtkButton *button, GladeChooser *gchooser);
static void dummy_feed_sw (GladeChooser *gchooser);

static void      
glade_chooser_post_init (GladeChooser *gchooser)
{
	GtkWidget *sw, *frame, *combo, *label, *file, *table, *bb, *button;

	gtk_container_set_border_width (GTK_CONTAINER (gchooser), GNOME_PAD/2.);

	/* top level table: file and top level widget selections */
	table = gtk_table_new (2, 2, FALSE);
	gtk_box_pack_start (GTK_BOX (gchooser), table, FALSE, TRUE, 0);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD);

	label = gtk_label_new (_("Glade file:"));
	gtk_table_attach (GTK_TABLE (table), label, 
			  0, 1, 0, 1, 0, 0, 0, 0);
	label = gtk_label_new (_("Top level widget:"));
	gtk_table_attach (GTK_TABLE (table), label, 
			  0, 1, 1, 2, 0, 0, 0, 0);
	file = gnome_file_entry_new ("glade_browser", _("Select Glade file to import"));
	gtk_table_attach_defaults (GTK_TABLE (table), file, 
				   1, 2, 0, 1);
	gtk_signal_connect (GTK_OBJECT (gnome_file_entry_gtk_entry (GNOME_FILE_ENTRY (file))), "activate",
			    GTK_SIGNAL_FUNC (browse_clicked_cb), gchooser);

	combo = gtk_combo_new ();
	gtk_table_attach_defaults (GTK_TABLE (table), combo, 
				   1, 2, 1, 2);
	gtk_entry_set_editable (GTK_ENTRY (GTK_COMBO (combo)->entry), FALSE);
	gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo)->entry), "changed",
			    GTK_SIGNAL_FUNC (top_level_widget_changed), gchooser);
	gchooser->combo = combo;

	gtk_widget_show_all (table);

	/* button in the middle to apply changes */
	bb = gtk_hbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (gchooser), bb, FALSE, TRUE, GNOME_PAD/2.);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);

	button = gtk_button_new_with_label (_("Try this selection"));
	gtk_widget_set_sensitive (button, FALSE);
	gtk_box_pack_start (GTK_BOX (bb), button, FALSE, TRUE, 0);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (top_level_widget_chosen_cb), gchooser);
	gchooser->apply_button = button;
	gtk_widget_show_all (bb);

	/* frame & scrolled window for the widget itself */
	frame = gtk_frame_new (_("Raw preview of the imported form:"));
	gtk_box_pack_start (GTK_BOX (gchooser), frame, TRUE, TRUE, GNOME_PAD/2.);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (frame), sw);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_set_border_width (GTK_CONTAINER (sw), GNOME_PAD/2.);
	gchooser->scrolled_window = sw;

	/* put something in the SW to help the user */
	dummy_feed_sw (gchooser);

	gtk_widget_show_all (frame);
}

static void 
dummy_feed_sw (GladeChooser *gchooser)
{
	GtkWidget *label;

	label = gtk_label_new (_("Here goes a preview of the imported Glade form.\n"
				 "Select a Glade file and the top level widget in the above part."));
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (gchooser->scrolled_window), label);
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (GTK_BIN (gchooser->scrolled_window)->child),
				      GTK_SHADOW_NONE);
	gtk_widget_show (label);
	gchooser->glade_widget = GTK_BIN (gchooser->scrolled_window)->child;
}


static GList* build_potential_roots (GList *list, xmlNodePtr node);
static void 
browse_clicked_cb (GtkEntry *entry, GladeChooser *gchooser)
{
	xmlDocPtr doc;
	gchar *file;

	file = gtk_entry_get_text (entry);

	/* empty previous selection */
	if (gchooser->form_struct->glade_file_name) {
		GList *list;
		g_free (gchooser->form_struct->glade_file_name);
		gchooser->form_struct->glade_file_name = NULL;
		list = g_list_append (NULL, "");
		gtk_combo_set_popdown_strings (GTK_COMBO (gchooser->combo), list);
		g_list_free (list);
	}

	if (gchooser->form_struct->top_widget_name) {
		g_free (gchooser->form_struct->top_widget_name);
		gchooser->form_struct->top_widget_name = NULL;
	}

	if (gchooser->form_struct->gxml) {
		gtk_object_unref (GTK_OBJECT (gchooser->form_struct->gxml));
		gchooser->form_struct->gxml = NULL;

		if (gchooser->glade_widget) {
			gtk_container_remove (GTK_CONTAINER (gchooser->scrolled_window), gchooser->glade_widget);
			gchooser->glade_widget = NULL;
#ifdef debug_signal
			g_print (">> 'FORM_CHANGED' from gladewrapper->browse_clicked_cb\n");
#endif
			gtk_signal_emit (GTK_OBJECT (gchooser), glade_chooser_signals[FORM_CHANGED]);
#ifdef debug_signal
			g_print ("<< 'FORM_CHANGED' from gladewrapper->browse_clicked_cb\n");
#endif
			dummy_feed_sw (gchooser);
		}
	}

	/* new values */
	if (*file == 0)
		return;

	doc = xmlParseFile (file);
	if (!doc) {
		g_warning ("Can't parse %s.", file);
		return;
	}
	else {
		/* listing the possible top level widgets in the file to be chosen */
		xmlNodePtr xnode;
		GList *widgets;

		xnode = doc->xmlRootNode->xmlChildrenNode;
		widgets = build_potential_roots (NULL, xnode);

		gtk_combo_set_popdown_strings (GTK_COMBO (gchooser->combo), widgets);
		while (widgets) {
			g_free (widgets->data);
			widgets = g_list_remove_link (widgets, widgets);
 		}

		xmlFreeDoc (doc);
		gchooser->form_struct->glade_file_name = g_strdup (file);
	}

	
	
}

static GList*
build_potential_roots (GList *list, xmlNodePtr node)
{
	while (node) {
		if (!strcmp(node->name, "widget")) {
			xmlNodePtr subnode = node->xmlChildrenNode;
			gchar *widname = NULL;
			gboolean addit = FALSE;

			while (subnode && (strcmp (subnode->name, "name"))) 
				subnode = subnode->next;
			if (subnode)
				widname = xmlNodeGetContent (subnode);
			else {
				g_warning ("Glade widget has no name!!!");
				widname = g_strdup("???");
			}

			/* test if we can add this widget */
			subnode = node->xmlChildrenNode;
			while (subnode && (strcmp (subnode->name, "class"))) 
				subnode = subnode->next;
			if (subnode) {
				gchar *class;

				class = xmlNodeGetContent (subnode);
				if (!strcmp (class+3, "Frame") || !strcmp (class+3, "ScrolledWindow") ||
				    !strcmp (class+3, "Viewport") || !strcmp (class+4, "Box") ||
				    !strcmp (class+3, "Fixed") || !strcmp (class+3, "Notebook") ||
				    !strcmp (class+3, "Paned") || !strcmp (class+3, "Layout") ||
				    !strcmp (class+3, "Packer") || !strcmp (class+3, "Table"))
					addit = TRUE;
				g_free(class);
			}
			
			if (addit) 
				list = g_list_append (list, widname);
			else
				g_free (widname);
		}

		if (node->xmlChildrenNode)
			list = build_potential_roots (list, node->xmlChildrenNode);

		node = node->next;
	}
	
	return list;
}

static void 
top_level_widget_changed (GtkEntry *entry, GladeChooser *gchooser)
{
	gchar *top = gtk_entry_get_text (entry);
	
	gtk_widget_set_sensitive (gchooser->apply_button,
				  (*top == 0)? FALSE : TRUE);
}

static void 
top_level_widget_chosen_cb (GtkButton *button, GladeChooser *gchooser)
{
	GladeXML *gxml;
	GtkWidget *wid;

	if (gchooser->form_struct->top_widget_name) {
		g_free (gchooser->form_struct->top_widget_name);
		gchooser->form_struct->top_widget_name = NULL;
	}

	if (gchooser->glade_widget) {
		gtk_container_remove (GTK_CONTAINER (gchooser->scrolled_window), gchooser->glade_widget);
		gchooser->glade_widget = NULL;
	}

	if (gchooser->form_struct->gxml) {
		gtk_object_unref (GTK_OBJECT (gchooser->form_struct->gxml));
		gchooser->form_struct->gxml = NULL;
	}

	gchooser->form_struct->top_widget_name = g_strdup (gtk_entry_get_text (GTK_ENTRY (GTK_COMBO (gchooser->combo)->entry)));
	
	gxml =  glade_xml_new(gchooser->form_struct->glade_file_name, gchooser->form_struct->top_widget_name);
	if (!gxml) {
		g_warning ("Cant' load Glade XML file %s (top= %s)\n", gchooser->form_struct->glade_file_name,
			   gchooser->form_struct->top_widget_name);
		
		dummy_feed_sw (gchooser);

		g_free (gchooser->form_struct->top_widget_name);
		gchooser->form_struct->top_widget_name = NULL;
	}
	else {
		gchooser->form_struct->gxml = gxml;

		/* the real widget */
		wid = glade_xml_get_widget(gxml, gchooser->form_struct->top_widget_name);
		gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (gchooser->scrolled_window), wid);
		gtk_viewport_set_shadow_type (GTK_VIEWPORT (GTK_BIN (gchooser->scrolled_window)->child),
					      GTK_SHADOW_NONE);
		gtk_widget_show (wid);
		gchooser->glade_widget = GTK_BIN (gchooser->scrolled_window)->child;
	}

#ifdef debug_signal
	g_print (">> 'FORM_CHANGED' from gladewrapper->top_level_widget_chosen_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (gchooser), glade_chooser_signals[FORM_CHANGED]);
#ifdef debug_signal
	g_print ("<< 'FORM_CHANGED' from gladewrapper->top_level_widget_chosen_cb\n");
#endif
}


GtkWidget*
fetch_glade_widget (GladeChooser *gchooser)
{
	if (gchooser->glade_widget) {
		GtkWidget *form;

		form = GTK_BIN (GLADE_CHOOSER (gchooser)->glade_widget)->child;
		gtk_widget_ref (form);

		gtk_container_remove (GTK_CONTAINER (GLADE_CHOOSER (gchooser)->glade_widget),
				      form);
		gtk_container_remove (GTK_CONTAINER (gchooser->scrolled_window), gchooser->glade_widget);
		gchooser->glade_widget = NULL;

		return form;
	}
	else 
		return NULL;
}
