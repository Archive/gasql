/* query-editor.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "query-editor.h"
#include "query-editor-fields.h"
#include "relship.h"

static GtkObject *parent_class = NULL;

static void query_editor_class_init (QueryEditorClass * class);
static void query_editor_init (QueryEditor * qed);
static void query_editor_initialize (QueryEditor * qed);
static void query_editor_destroy (GtkObject *obj);

guint
query_editor_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Query_Editor",
			sizeof (QueryEditor),
			sizeof (QueryEditorClass),
			(GtkClassInitFunc) query_editor_class_init,
			(GtkObjectInitFunc) query_editor_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_vbox_get_type (), &f_info);
	}

	return f_type;
}

static void
query_editor_class_init (QueryEditorClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	object_class->destroy = query_editor_destroy;
	parent_class = gtk_type_class (gtk_vbox_get_type ());
}

static void
query_editor_init (QueryEditor * qed)
{
	qed->query = NULL;
	qed->main_nb = NULL;
	qed->name = NULL;
	qed->descr = NULL;
	qed->mitems = NULL;
	qed->option_menu = NULL;
}

static void query_destroy_cb (Query *q, QueryEditor *qed);
static void query_name_changed_cb (Query *q, QueryEditor *qed);
static void query_type_changed_cb (Query *q, QueryEditor *qed);
static void query_editor_initialize (QueryEditor * qed);
GtkWidget *
query_editor_new (Query * q)
{
	GtkObject *obj;
	QueryEditor *qed;

	g_return_val_if_fail (q, NULL);
	g_return_val_if_fail (IS_QUERY (q), NULL);

	obj = gtk_type_new (query_editor_get_type ());
	qed = QUERY_EDITOR (obj);
	qed->query = q;

	query_editor_initialize (qed);

	/* signals */
	gtk_signal_connect_while_alive (GTK_OBJECT (q), "destroy",
					GTK_SIGNAL_FUNC (query_destroy_cb), qed,
					GTK_OBJECT (qed));

	gtk_signal_connect_while_alive (GTK_OBJECT (qed->query), "name_changed",
					GTK_SIGNAL_FUNC (query_name_changed_cb), qed,
					GTK_OBJECT (qed));

	gtk_signal_connect_while_alive (GTK_OBJECT (qed->query), "type_changed",
					GTK_SIGNAL_FUNC (query_type_changed_cb), qed,
					GTK_OBJECT (qed));

	return GTK_WIDGET (obj);
}

static void 
query_editor_destroy (GtkObject *object)
{
	QueryEditor *qed;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_EDITOR (object));

	qed = QUERY_EDITOR (object);


	if (qed->mitems) {
		g_slist_free (qed->mitems);
		qed->mitems = NULL;
	}


	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void 
query_destroy_cb (Query *q, QueryEditor *qed)
{
	gtk_object_destroy (GTK_OBJECT (qed));
}

/* Update the GUI because of Query changes */
static void 
query_name_changed_cb (Query *q, QueryEditor *qed)
{
	if (q->name)
		gtk_entry_set_text (GTK_ENTRY (qed->name), q->name);
	else 
		gtk_entry_set_text (GTK_ENTRY (qed->name), "");
	if (q->descr)
		gtk_entry_set_text (GTK_ENTRY (qed->descr), q->descr);
	else
		gtk_entry_set_text (GTK_ENTRY (qed->descr), "");
}

static void optionitem_activate_cb (GtkMenuItem * mitem, QueryEditor * qed);
static void 
query_type_changed_cb (Query *q, QueryEditor *qed)
{
	GSList *list;
	gint hist = -1, i = 0;

	list = qed->mitems;
	while (list) {
		gtk_signal_handler_block_by_func (GTK_OBJECT (list->data),
						  GTK_SIGNAL_FUNC (optionitem_activate_cb), qed);
		if (qed->query->type == GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (list->data),
									      "type")))
			hist = i;
		list = g_slist_next (list);
		i++;
	}

	gtk_option_menu_set_history (GTK_OPTION_MENU (qed->option_menu), hist);

	list = qed->mitems;
	while (list) {
		gtk_signal_handler_unblock_by_func (GTK_OBJECT (list->data),
						    GTK_SIGNAL_FUNC (optionitem_activate_cb), qed);
		list = g_slist_next (list);
	}


	/* set the right notebook */
	if (qed->query->type == QUERY_TYPE_SQL) 
		gtk_notebook_set_page (GTK_NOTEBOOK (qed->main_nb), 0);
	else
		gtk_notebook_set_page (GTK_NOTEBOOK (qed->main_nb), 1);
}


static void entry_name_changed_cb (GtkEntry *entry, QueryEditor * qed);
static void entry_descr_changed_cb (GtkEntry *entry, QueryEditor * qed);
static void
query_editor_initialize (QueryEditor * qed)
{
	GtkWidget *label, *option_menu, *menu, *mitem, *sw, *table, *entry;
	GtkWidget *main_nb, *vb, *text, *bb, *button, *wid, *frame;
	GtkWidget *vp, *hp, *vp2;
	RelShip *rs;
	
	/* Query informations */
	table = gtk_table_new (4, 2, FALSE);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_box_pack_start (GTK_BOX (qed), table, FALSE, FALSE, GNOME_PAD/2.);
	
	label = gtk_label_new (_("Query name:"));
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, 0, 0, 0, 0);
	entry = gtk_entry_new ();
	gtk_table_attach_defaults (GTK_TABLE (table), entry, 1, 2, 0, 1);
	qed->name = entry;
	gtk_signal_connect (GTK_OBJECT (entry), "changed",
			    GTK_SIGNAL_FUNC (entry_name_changed_cb), qed);
	
	label = gtk_label_new (_("Query description:"));
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, 0, 0, 0, 0);
	entry = gtk_entry_new ();
	gtk_table_attach_defaults (GTK_TABLE (table), entry, 1, 4, 1, 2);
	qed->descr = entry;
	gtk_signal_connect (GTK_OBJECT (entry), "changed",
			    GTK_SIGNAL_FUNC (entry_descr_changed_cb), qed);

	label = gtk_label_new (_("Type of query:"));
	gtk_table_attach (GTK_TABLE (table), label, 2, 3, 0, 1, 0, 0, 0, 0);

	option_menu = gtk_option_menu_new ();
	qed->option_menu = option_menu;
	menu = gtk_menu_new ();
	mitem = gtk_menu_item_new_with_label (_("Data SELECT"));
	qed->mitems = g_slist_append (qed->mitems, mitem);
	gtk_menu_append (GTK_MENU (menu), mitem);
	gtk_object_set_data (GTK_OBJECT (mitem), "type", GINT_TO_POINTER (QUERY_TYPE_STD));
	gtk_signal_connect (GTK_OBJECT (mitem), "activate",
			    GTK_SIGNAL_FUNC (optionitem_activate_cb), qed);

	mitem = gtk_menu_item_new_with_label (_("Data INSERT"));
	qed->mitems = g_slist_append (qed->mitems, mitem);
	gtk_menu_append (GTK_MENU (menu), mitem);
	gtk_object_set_data (GTK_OBJECT (mitem), "type", GINT_TO_POINTER (QUERY_TYPE_SQL));
	gtk_signal_connect (GTK_OBJECT (mitem), "activate",
			    GTK_SIGNAL_FUNC (optionitem_activate_cb), qed);


	gtk_widget_set_sensitive (mitem, FALSE);
	mitem = gtk_menu_item_new_with_label (_("Data UPDATE"));
	qed->mitems = g_slist_append (qed->mitems, mitem);
	gtk_menu_append (GTK_MENU (menu), mitem);
	gtk_object_set_data (GTK_OBJECT (mitem), "type", GINT_TO_POINTER (QUERY_TYPE_SQL));
	gtk_signal_connect (GTK_OBJECT (mitem), "activate",
			    GTK_SIGNAL_FUNC (optionitem_activate_cb), qed);


	gtk_widget_set_sensitive (mitem, FALSE);
	mitem = gtk_menu_item_new_with_label (_("Data DELETE"));
	qed->mitems = g_slist_append (qed->mitems, mitem);
	gtk_menu_append (GTK_MENU (menu), mitem);
	gtk_object_set_data (GTK_OBJECT (mitem), "type", GINT_TO_POINTER (QUERY_TYPE_SQL));
	gtk_signal_connect (GTK_OBJECT (mitem), "activate",
			    GTK_SIGNAL_FUNC (optionitem_activate_cb), qed);


	gtk_widget_set_sensitive (mitem, FALSE);
	mitem = gtk_menu_item_new_with_label (_("Union Query"));
	qed->mitems = g_slist_append (qed->mitems, mitem);
	gtk_menu_append (GTK_MENU (menu), mitem);
	gtk_object_set_data (GTK_OBJECT (mitem), "type", GINT_TO_POINTER (QUERY_TYPE_UNION));
	gtk_signal_connect (GTK_OBJECT (mitem), "activate",
			    GTK_SIGNAL_FUNC (optionitem_activate_cb), qed);

	mitem = gtk_menu_item_new_with_label (_("Intersection Query"));
	qed->mitems = g_slist_append (qed->mitems, mitem);
	gtk_menu_append (GTK_MENU (menu), mitem);
	gtk_object_set_data (GTK_OBJECT (mitem), "type", GINT_TO_POINTER (QUERY_TYPE_INTERSECT));
	gtk_signal_connect (GTK_OBJECT (mitem), "activate",
			    GTK_SIGNAL_FUNC (optionitem_activate_cb), qed);

	mitem = gtk_menu_item_new_with_label (_("SQL only Query"));
	qed->mitems = g_slist_append (qed->mitems, mitem);
	gtk_menu_append (GTK_MENU (menu), mitem);;
	gtk_object_set_data (GTK_OBJECT (mitem), "type", GINT_TO_POINTER (QUERY_TYPE_SQL));
	gtk_signal_connect (GTK_OBJECT (mitem), "activate",
			    GTK_SIGNAL_FUNC (optionitem_activate_cb), qed);

	gtk_option_menu_set_menu (GTK_OPTION_MENU (option_menu), menu);
	gtk_table_attach_defaults (GTK_TABLE (table), option_menu, 3, 4, 0, 1);

	main_nb = gtk_notebook_new ();
	gtk_box_pack_start (GTK_BOX (qed), main_nb, TRUE, TRUE, GNOME_PAD/2.);
	qed->main_nb = main_nb;
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (main_nb), FALSE);
	gtk_notebook_set_show_border (GTK_NOTEBOOK (main_nb), FALSE);

	/* SQL only query */
	label = gtk_label_new ("SQL");
	vb = gtk_vbox_new (FALSE, GNOME_PAD/2.);
	gtk_notebook_append_page (GTK_NOTEBOOK (main_nb), vb, label);
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC, 
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (vb), sw, TRUE, TRUE, GNOME_PAD/2.);
	gtk_container_set_border_width (GTK_CONTAINER (sw), GNOME_PAD);
	text = gtk_text_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (sw), text);

	bb = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);
	gtk_box_pack_start (GTK_BOX (vb), bb, FALSE, TRUE, GNOME_PAD/2.);
	button = gnome_stock_button (GNOME_STOCK_BUTTON_APPLY);
	gtk_container_add (GTK_CONTAINER (bb), button);


	/* 
	 * "Normal" (SELECT, INSERT, UPDATE, DELETE) query 
	 */
	vp = gtk_vpaned_new ();
	label = gtk_label_new ("Normal query");
	gtk_notebook_append_page (GTK_NOTEBOOK (main_nb), vp, label);

	hp = gtk_hpaned_new ();
	gtk_paned_add1 (GTK_PANED (vp), hp);

	/* joins */
	frame = gtk_frame_new (_("Joins"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD/2.);
	gtk_paned_add1 (GTK_PANED (hp), frame);

	rs = RELSHIP (relship_find (qed->query));
	wid = relship_get_sw_view (rs);
	gtk_widget_set_usize (wid, 500, 250);
	gtk_container_add (GTK_CONTAINER (frame), wid);

	/* SQL and warnings */
	vp2 = gtk_vpaned_new ();
	gtk_paned_pack2 (GTK_PANED (hp), vp2, FALSE, TRUE);
	
	frame = gtk_frame_new (_("SQL (for information)"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD/2.);
	gtk_paned_add1 (GTK_PANED (vp2), frame);
	
	wid = gtk_text_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (frame), wid);

	frame = gtk_frame_new (_("Warnings"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD/2.);
	gtk_paned_add2 (GTK_PANED (vp2), frame);

	wid = gtk_text_new (NULL, NULL);
	gtk_container_add (GTK_CONTAINER (frame), wid);


	/* Expressions */
	hp = gtk_hpaned_new ();
	gtk_paned_add2 (GTK_PANED (vp), hp);

	frame = gtk_frame_new (_("Expressions"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD/2.);
	gtk_paned_pack1 (GTK_PANED (hp), frame, TRUE, FALSE);
	
	wid = query_editor_fields_new (qed->query);
	gtk_widget_set_usize (wid, 300, 200);
	gtk_container_add (GTK_CONTAINER (frame), wid);

	/* Qual. Cond */
	frame = gtk_frame_new (_("Qualification conditions"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD/2.);
	gtk_paned_add2 (GTK_PANED (hp), frame);

	wid = gtk_label_new ("Nothing Yet!");
	gtk_container_add (GTK_CONTAINER (frame), wid);

	
	/* Show everything but the widget itself */
	gtk_widget_show_all (table);
	gtk_widget_show_all (main_nb);

	/* Initial settings display */
	query_name_changed_cb (qed->query, qed);
	query_type_changed_cb (qed->query, qed);
}

/* Update the Query because of GUI changes */
static void 
entry_name_changed_cb (GtkEntry *entry, QueryEditor * qed)
{
	query_set_name (qed->query, gtk_entry_get_text (entry), NULL);
}

/* Update the Query because of GUI changes */
static void entry_descr_changed_cb (GtkEntry *entry, QueryEditor * qed)
{
	query_set_name (qed->query, NULL, gtk_entry_get_text (entry));
}

static void 
optionitem_activate_cb (GtkMenuItem * mitem, QueryEditor * qed)
{
	QueryType type;

	type = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (mitem), "type"));
	query_set_query_type (qed->query, type);
}

