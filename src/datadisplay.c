/* datadisplay.c
 *
 * Copyright (C) 1999,2000 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "datadisplay.h"

static void data_display_class_init (DataDisplayClass * class);
static void data_display_init (DataDisplay * wid);
static void data_display_destroy (GtkObject * object);

/* signals */
enum
{
	CONTENTS_MODIF_SIGNAL,
	LAST_SIGNAL
};

static gint data_display_signals[LAST_SIGNAL] = { 0 };


guint
data_display_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Data_Display",
			sizeof (DataDisplay),
			sizeof (DataDisplayClass),
			(GtkClassInitFunc) data_display_class_init,
			(GtkObjectInitFunc) data_display_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_vbox_get_type (), &f_info);
	}

	return f_type;
}

static void
data_display_class_init (DataDisplayClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	data_display_signals[CONTENTS_MODIF_SIGNAL] =
		gtk_signal_new ("contents_modified",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DataDisplayClass,
						   contents_modified),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);

	gtk_object_class_add_signals (object_class, data_display_signals,
				      LAST_SIGNAL);
	class->contents_modified = NULL;
	object_class->destroy = data_display_destroy;
}

static void
data_display_init (DataDisplay * wid)
{
	wid->childs = NULL;
}

GtkWidget *
data_display_new ()
{
	GtkObject *obj;
	DataDisplay *wid;

	obj = gtk_type_new (data_display_get_type ());
	wid = DATA_DISPLAY (obj);

	return GTK_WIDGET (obj);
}

static void
data_display_destroy (GtkObject * object)
{
	DataDisplay *dd;
	GtkObject *parent_class = NULL;

	parent_class = gtk_type_class (gtk_object_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_DATA_DISPLAY (object));

	dd = DATA_DISPLAY (object);

	if (dd->childs) {
		g_slist_free (dd->childs);
		dd->childs = NULL;
	}

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


void
data_display_pack_default (DataDisplay * dd, GtkWidget * wid)
{
	gtk_box_pack_start (GTK_BOX (dd), wid, TRUE, TRUE, 0);
}
