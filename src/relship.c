/* relship.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "relship.h"
#include "canvas-query-view.h"

/*
 * 
 * RelShip object
 * 
 */



static void relship_class_init (RelShipClass * class);
static void relship_init       (RelShip * rs);
static void relship_destroy    (GtkObject * object);

guint
relship_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"RelShip",
			sizeof (RelShip),
			sizeof (RelShipClass),
			(GtkClassInitFunc) relship_class_init,
			(GtkObjectInitFunc) relship_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_object_get_type (), &f_info);
	}

	return f_type;
}

static void
relship_class_init (RelShipClass * class)
{
	GtkObjectClass *object_class = NULL;

	object_class = (GtkObjectClass *) class;

	object_class->destroy = relship_destroy;
}


static void
relship_init (RelShip * rs)
{
	rs->query = NULL;
	rs->views = NULL;
	rs->items = NULL;
}



static void query_destroy_cb (Query *q, RelShip *rs);

GtkObject *
relship_find (Query *q) 
{
	GtkObject *obj;
	RelShip *rs;

	g_return_val_if_fail (q, NULL);
	g_return_val_if_fail (IS_QUERY (q), NULL);

	if ((obj = gtk_object_get_data (GTK_OBJECT (q), "RelShip"))) 
		return obj;

	obj = gtk_type_new (relship_get_type ());
	rs = RELSHIP (obj);

	rs->query = q;

	gtk_signal_connect_while_alive (GTK_OBJECT (q), "destroy",
					GTK_SIGNAL_FUNC (query_destroy_cb), rs,
					GTK_OBJECT (rs));

	gtk_object_set_data (GTK_OBJECT (q), "RelShip", rs);
	
	return obj;
}

void relship_copy (Query *q, Query *original, GHashTable *hash)
{
	GtkObject *obj;
	RelShip *rs = NULL;

	g_return_if_fail (q && IS_QUERY (q));
	g_return_if_fail (original && IS_QUERY (original));
	g_return_if_fail (hash);

	if ((obj = gtk_object_get_data (GTK_OBJECT (q), "RelShip"))) 
		gtk_object_destroy (obj);

	if ((obj = gtk_object_get_data (GTK_OBJECT (original), "RelShip"))) {
		GSList *list;

		RelShip *rso = RELSHIP (obj);
		rs = RELSHIP (relship_find (q));
		
		/* Copy of the list of items */
		list = rso->items;
		while (list) {
			RelShipItemData *rsi = RELSHIP_ITEM_DATA_CAST (list->data);

			obj = g_hash_table_lookup (hash, rsi->obj);
			if (obj) {
				RelShipItemData *nrsi;
				
				nrsi = relship_find_item (rs, obj);
				nrsi->x = rsi->x;
				nrsi->y = rsi->y;
			}
			
			list = g_slist_next(list);
		}
	}
}

static void
relship_destroy (GtkObject * object)
{
	GtkObjectClass *parent_class = NULL;
	RelShip *rs;
	GSList *list;

	parent_class = gtk_type_class (gtk_object_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_RELSHIP (object));

	rs = RELSHIP (object);
	list = rs->items;
	while (list) {
		g_free (list->data);
		list = g_slist_next (list);
	}
	if (rs->items) {
		g_slist_free (rs->items);
		rs->items = NULL;
	}

	g_print ("Relship Destroy ... %p\n", object);

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


static void 
query_destroy_cb (Query *q, RelShip *rs)
{
	gtk_object_unref (GTK_OBJECT (rs));
}



/*
 * Getting a new RelShipView
 */
static void adj_changed_cb (GtkAdjustment *adj, RelShip *rs);
static void item_moved_cb (RelShipView *rsv, CanvasBase *item, RelShip *rs);
static void view_destroy_cb (GtkObject *obj, RelShip *rs);
GtkWidget *
relship_get_sw_view (RelShip *rs)
{
	GtkWidget *sw, *rels;
	GtkAdjustment *adj;

	g_return_val_if_fail (rs, NULL);
	g_return_val_if_fail (IS_RELSHIP (rs), NULL);

	sw = gtk_scrolled_window_new(NULL, NULL);
	rels = relship_view_new (rs->query);
	gnome_canvas_set_scroll_region(GNOME_CANVAS (rels), -750, -750, 750, 750);
	
	gtk_widget_set_usize(rels, 500, 500);
	gtk_container_add(GTK_CONTAINER(sw), rels);
	gtk_widget_show (rels);

	adj = gtk_scrolled_window_get_vadjustment (GTK_SCROLLED_WINDOW (sw));
	gtk_signal_connect (GTK_OBJECT (adj), "changed",
			    GTK_SIGNAL_FUNC (adj_changed_cb), rs);
	adj = gtk_scrolled_window_get_hadjustment (GTK_SCROLLED_WINDOW (sw));
	gtk_signal_connect (GTK_OBJECT (adj), "changed",
			    GTK_SIGNAL_FUNC (adj_changed_cb), rs);

	gtk_signal_connect_while_alive (GTK_OBJECT (rels), "item_moved",
					GTK_SIGNAL_FUNC (item_moved_cb), rs,
					GTK_OBJECT (rs));

	gtk_signal_connect_while_alive (GTK_OBJECT (rels), "destroy",
					GTK_SIGNAL_FUNC (view_destroy_cb), rs,
					GTK_OBJECT (rs));

	rs->views = g_slist_append (rs->views, rels);

	return sw;
}

static void 
adj_changed_cb (GtkAdjustment *adj, RelShip *rs)
{
	gtk_signal_disconnect_by_func (GTK_OBJECT (adj), GTK_SIGNAL_FUNC (adj_changed_cb), rs);
	gtk_adjustment_set_value (adj, (adj->upper - adj->lower) / 2.);
}

static void 
item_moved_cb (RelShipView *rsv, CanvasBase *item, RelShip *rs)
{
	if (IS_CANVAS_QUERY_VIEW (item)) {
		QueryView *qv;
		RelShipItemData *id;
		gdouble x, y;
		GSList *list;

		/* FIXME: implement this with the "query_view" property */
		qv = CANVAS_QUERY_VIEW (item)->view;
		
		id = relship_find_item (rs, GTK_OBJECT (qv));
		gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (item), &(id->x), &(id->y), &x, &y);

		/* FIXME: forward the movement */
		list = rs->views;
		while (list) {

			if (list->data != rsv) {
				gtk_signal_handler_block_by_func (GTK_OBJECT (list->data),
								  GTK_SIGNAL_FUNC (item_moved_cb), rs);
				relship_view_refresh_items (RELSHIP_VIEW (list->data));
				gtk_signal_handler_unblock_by_func (GTK_OBJECT (list->data),
								    GTK_SIGNAL_FUNC (item_moved_cb), rs);
			}
			list = g_slist_next (list);
		}
	}
}

static void 
view_destroy_cb (GtkObject *obj, RelShip *rs)
{
	rs->views = g_slist_remove (rs->views, obj);
}

/*
 * Finding an item 
 */
static void object_item_destroy_cb (GtkObject *obj, RelShip *rs);
RelShipItemData *
relship_find_item (RelShip *rs, GtkObject *obj)
{
	RelShipItemData *id = NULL;
	GSList *list;

	list = rs->items;
	while (list && !id) {
		if (RELSHIP_ITEM_DATA_CAST (list->data)->obj == obj)
			id = RELSHIP_ITEM_DATA_CAST (list->data);
		list = g_slist_next (list);
	}

	if (!id) {
		id = g_new0 (RelShipItemData, 1);
		id->obj = obj;
		id->x = 50.;
		id->y = 50.;
		rs->items = g_slist_append (rs->items, id);
		
		gtk_signal_connect_while_alive (GTK_OBJECT (obj), "destroy",
						GTK_SIGNAL_FUNC (object_item_destroy_cb), rs,
						GTK_OBJECT (rs));
	}

	return id;
}

static void 
object_item_destroy_cb (GtkObject *obj, RelShip *rs)
{
	RelShipItemData *id;
	id = relship_find_item (rs, obj);
	if (id) {
		rs->items = g_slist_remove (rs->items, id);
		g_free (id);
	}
}


/*
 * XML saving and loading 
 */
void 
relship_build_xml_tree (Query *start_query, xmlNodePtr toptree, ConfManager * conf)
{
	RelShip *rs;
	GSList *list;
	Query *query;

	if (!start_query)
		query = QUERY (conf->top_query);
	else
		query = start_query;

	rs = gtk_object_get_data (GTK_OBJECT (query), "RelShip");
	if (rs && IS_RELSHIP (rs)) {
		xmlNodePtr tree;	
		gchar *str;

		tree = xmlNewChild (toptree, NULL, "RelShip", NULL);
		str = query_get_xml_id (query);
		xmlSetProp (tree, "queryid", str);
		g_free (str);

		list = rs->items;
		while (list) {
			RelShipItemData *id;

			id = RELSHIP_ITEM_DATA_CAST (list->data);

			str = NULL;
			if (IS_QUERY_VIEW (id->obj))
				str = query_view_get_xml_id (QUERY_VIEW (id->obj));

			if (str) {
				xmlNodePtr tree2;
				tree2 = xmlNewChild (tree, NULL, "RelShipItem", NULL);
				xmlSetProp (tree2, "object", str);
				g_free(str);

				str = g_strdup_printf ("%d.%d", (gint) id->x, (gint) (id->x - (gint) id->x) * 100);
				xmlSetProp (tree2, "xpos", str);
				g_free(str);

				str = g_strdup_printf ("%d.%d", (gint) id->y, (gint) (id->y - (gint) id->y) * 100);
				xmlSetProp (tree2, "ypos", str);
				g_free(str);
			}
			list = g_slist_next (list);
		}
	}

	
	list = query->sub_queries;
	while (list) {
		relship_build_xml_tree (QUERY (list->data), toptree, conf);
		list = g_slist_next (list);
	}
}

RelShip *
relship_build_from_xml_tree (ConfManager * conf, xmlNodePtr node)
{
	RelShip *rs = NULL;
	gchar *str;
	
	str = xmlGetProp (node, "queryid");
	if (str) {
		Query *q;

		q = query_find_from_xml_name (conf, NULL, str);
		if (q) {
			xmlNodePtr tree;
			rs = RELSHIP (relship_find (q));

			tree = node->xmlChildrenNode;
			while (tree) {
				if (!strcmp (tree->name, "RelShipItem")) {
					gchar *str2;

					str2 = xmlGetProp (tree, "object");
					if (str2) {
						gpointer ptr;
						ptr = query_view_find_from_xml_name (conf, NULL, str2);
						g_free (str2);
						if (ptr && GTK_IS_OBJECT (ptr)) {
							RelShipItemData *id;
							gchar *str3;

							id = relship_find_item (rs, GTK_OBJECT (ptr));

							str3 = xmlGetProp (tree, "xpos");
							if (str3) {
								id->x = atof (str3);
								g_free (str3);
							}
							str3 = xmlGetProp (tree, "ypos");
							if (str3) {
								id->y = atof (str3);
								g_free (str3);
							}
						}
					}
				}
				tree = tree->next;
			}
			
		}
		g_free (str);
	}

	return rs;
}
