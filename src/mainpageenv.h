/* mainpageenv.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MAIN_PAGE_ENV__
#define __MAIN_PAGE_ENV__

#include <gnome.h>
#include "conf-manager.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define MAIN_PAGE_ENV(obj)          GTK_CHECK_CAST (obj, main_page_env_get_type(), MainPageEnv)
#define MAIN_PAGE_ENV_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, main_page_env_get_type (), MainPageEnvClass)
#define IS_MAIN_PAGE_ENV(obj)       GTK_CHECK_TYPE (obj, main_page_env_get_type ())


	typedef struct _MainPageEnv      MainPageEnv;
	typedef struct _MainPageEnvClass MainPageEnvClass;


	/* struct for the object's data */
	struct _MainPageEnv
	{
		GtkVBox      object;

		ConfManager *conf;
		GtkWidget   *clist;	/* the GtkCList object */
		GtkWidget   *remove_env;
		GtkWidget   *new_env;
		GtkWidget   *properties;
		gint         sel_row;

		GSList      *queries; /* list of queries whose name appear in the
					 list ov ENVs */
	};

	/* struct for the object's class */
	struct _MainPageEnvClass
	{
		GtkVBoxClass parent_class;
	};

	/* generic widget's functions */
	guint      main_page_env_get_type (void);
	GtkWidget *main_page_env_new      (ConfManager * conf);


#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
