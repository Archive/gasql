/* mainpagequery.h
 *
 * Copyright (C) 1999 - 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MAIN_PAGE_QUERY__
#define __MAIN_PAGE_QUERY__

#include <gnome.h>
#include "database.h"
#include "conf-manager.h"
#include "query.h"
#include "query-env.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define MAIN_PAGE_QUERY(obj)          GTK_CHECK_CAST (obj, main_page_query_get_type(), MainPageQuery)
#define MAIN_PAGE_QUERY_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, main_page_query_get_type (), MainPageQueryClass)
#define IS_MAIN_PAGE_QUERY(obj)       GTK_CHECK_TYPE (obj, main_page_query_get_type ())


	typedef struct _MainPageQuery MainPageQuery;
	typedef struct _MainPageQueryClass MainPageQueryClass;


	/* struct for the object's data */
	struct _MainPageQuery
	{
		GtkVBox object;

		ConfManager   *conf;
		GtkWidget     *ctree;	/* the GtkCTree object */
		GtkWidget     *remove_query;
		GtkWidget     *edit_query;
		GtkWidget     *exec_query;
		GtkWidget     *new_query;

		GSList        *new_dlgs;    /* dialogs for the creation of new queries */
		GtkCTreeNode  *sel_node;	/* selection */
		GtkWidget     *queries_bbox;
	};

	/* struct for the object's class */
	struct _MainPageQueryClass
	{
		GtkVBoxClass parent_class;
	};

	/* generic widget's functions */
	guint main_page_query_get_type (void);
	GtkWidget *main_page_query_new (ConfManager * conf);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
