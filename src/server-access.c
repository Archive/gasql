/* serveraccess.c
 *
 * Copyright (C) 1999 - 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "server-access.h"
#include <string.h>
#include <sys/types.h>
#include <dirent.h>
#include <ltdl.h>
#include <gda-config.h>


/* signals */
enum
{
	CONN_OPENED,
	CONN_TO_CLOSE,
	CONN_CLOSED,
	DATA_TYPES_UPDATED,
	DATA_FUNCTION_ADDED,
	DATA_FUNCTION_REMOVED,
	DATA_FUNCTION_UPDATED,
	DATA_AGGREGATE_ADDED,
	DATA_AGGREGATE_REMOVED,
	DATA_AGGREGATE_UPDATED,
	PROGRESS,
	OBJECTS_BINDINGS_UPDATED,
	LAST_SIGNAL
};

static gint server_access_signals[LAST_SIGNAL] = { 0, 0, 0, 0,
	0, 0, 0, 0, 0, 0, 0
};
static void server_access_class_init (ServerAccessClass * class);
static void server_access_init (ServerAccess * srv);
static void server_access_destroy (GtkObject * object);
static void m_conn_open (ServerAccess * srv, gpointer data);
static void m_conn_closed (ServerAccess * srv);

/* get a pointer to the parents to be able to call their destructor */
static GtkObject *parent_class = NULL;


GtkType
server_access_get_type (void)
{
	static GtkType f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Sql_Access",
			sizeof (ServerAccess),
			sizeof (ServerAccessClass),
			(GtkClassInitFunc) server_access_class_init,
			(GtkObjectInitFunc) server_access_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gda_connection_get_type (),
					  &f_info);
	}

	return f_type;
}

static void
server_access_class_init (ServerAccessClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	server_access_signals[CONN_OPENED] =
		gtk_signal_new ("conn_opened",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass,
						   conn_opened),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	server_access_signals[CONN_TO_CLOSE] =
		gtk_signal_new ("conn_to_close", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass,
						   conn_to_close),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	server_access_signals[CONN_CLOSED] =	/* runs after user handlers */
		gtk_signal_new ("conn_closed",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass,
						   conn_closed),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	server_access_signals[DATA_TYPES_UPDATED] =
		gtk_signal_new ("data_types_updated", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass,
						   data_types_updated),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	server_access_signals[DATA_FUNCTION_ADDED] =
		gtk_signal_new ("data_function_added", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass,
						   data_function_added),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	server_access_signals[DATA_FUNCTION_REMOVED] =
		gtk_signal_new ("data_function_removed", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass,
						   data_function_removed),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	server_access_signals[DATA_FUNCTION_UPDATED] =
		gtk_signal_new ("data_function_updated", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass,
						   data_function_updated),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	server_access_signals[DATA_AGGREGATE_ADDED] =
		gtk_signal_new ("data_aggregate_added", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass,
						   data_aggregate_added),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	server_access_signals[DATA_AGGREGATE_REMOVED] =
		gtk_signal_new ("data_aggregate_removed", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass,
						   data_aggregate_removed),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	server_access_signals[DATA_AGGREGATE_UPDATED] =
		gtk_signal_new ("data_aggregate_updated", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass,
						   data_aggregate_updated),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	server_access_signals[PROGRESS] =
		gtk_signal_new ("progress", GTK_RUN_FIRST, object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass, progress),
				gtk_marshal_NONE__POINTER_UINT_UINT,
				GTK_TYPE_NONE, 3, GTK_TYPE_POINTER,
				GTK_TYPE_UINT, GTK_TYPE_UINT);
	server_access_signals[OBJECTS_BINDINGS_UPDATED] =
		gtk_signal_new ("objects_bindings_updated", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerAccessClass,
						   objects_bindings_updated),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);

	gtk_object_class_add_signals (object_class, server_access_signals,
				      LAST_SIGNAL);
	class->conn_opened = NULL;
	class->conn_to_close = NULL;
	class->conn_closed = m_conn_closed;
	class->data_types_updated = NULL;
	class->data_function_added = NULL;
	class->data_function_removed = NULL;
	class->data_function_updated = NULL;
	class->data_aggregate_added = NULL;
	class->data_aggregate_removed = NULL;
	class->data_aggregate_updated = NULL;
	class->progress = NULL;
	class->objects_bindings_updated = NULL;
	object_class->destroy = server_access_destroy;
}

static void
server_access_init (ServerAccess * srv)
{
	gint i;

	srv->gda_datasource = g_string_new ("");
	srv->user_name = g_string_new (getenv ("USER"));
	srv->password = g_string_new ("");
	srv->description = NULL;

	/* creation of the data types known by postgreSQL */
	srv->data_types = NULL;
	srv->data_functions = NULL;
	srv->data_aggregates = NULL;

	/* display plugins */
	i = lt_dlinit ();
	if (i) {
		g_warning ("Cannot init the LTDL library!");
		exit (1);
	}
	srv->data_types_display = sql_data_display_get_initial_list ();
	srv->types_objects_hash = g_hash_table_new (NULL, NULL);	/* pointers! */
	srv->bindable_objects = NULL;

	srv->cmd = NULL;

	/* init the features */
	srv->features.sequences = FALSE;
	srv->features.procs = FALSE;
	srv->features.inheritance = FALSE;
	srv->features.xml_queries = FALSE;

	/* declare the binding functions for Functions and aggregates here */
	server_access_declare_object_bindable (srv,
					    server_function_binding_func);
}


GtkObject *
server_access_new (CORBA_ORB orb)
{
	GtkObject *obj;

	g_return_val_if_fail (orb != 0, 0);
	obj = gtk_type_new (server_access_get_type ());
	GDA_CONNECTION (obj)->orb = orb;

	gtk_signal_connect (obj, "open", GTK_SIGNAL_FUNC (m_conn_open), NULL);

	return obj;
}

void
server_access_free (ServerAccess * srv)
{
	gtk_object_destroy (GTK_OBJECT (srv));
}

static void
server_access_destroy (GtkObject * object)
{
	ServerAccess *srv;
	GSList *list;

	parent_class = gtk_type_class (gtk_object_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_SERVER_ACCESS (object));

	srv = SERVER_ACCESS (object);

	if (gda_connection_is_open (GDA_CONNECTION (srv)))
		server_access_close_connect (srv);

	if (srv->description)
		g_free (srv->description);
	g_string_free (srv->gda_datasource, TRUE);
	g_string_free (srv->user_name, TRUE);
	g_string_free (srv->password, TRUE);

	/* data types */
	if (srv->data_types) {
		list = srv->data_types;
		while (list) {
			gtk_object_destroy (GTK_OBJECT (list->data));
			list = g_slist_next (list);
		}
		g_slist_free (srv->data_types);
	}
	srv->data_types = NULL;

	/* data functions */
	if (srv->data_functions) {
		list = srv->data_functions;
		while (list) {
			gtk_object_destroy (GTK_OBJECT (list->data));
			list = g_slist_next (list);
		}
		g_slist_free (srv->data_functions);
	}
	srv->data_functions = NULL;

	/* data aggregates */
	if (srv->data_aggregates) {
		list = srv->data_aggregates;
		while (list) {
			gtk_object_destroy (GTK_OBJECT (list->data));
			list = g_slist_next (list);
		}
		g_slist_free (srv->data_aggregates);
	}
	srv->data_aggregates = NULL;

	/* hash table */
	if (srv->types_objects_hash)
		g_hash_table_destroy (srv->types_objects_hash);

	if (srv->cmd)
		gda_command_free (srv->cmd);

	/* Display functions and Plugins */
	while (srv->data_types_display) {
		SqlDataDisplayFns *fns;
		fns = (SqlDataDisplayFns *) (srv->data_types_display->data);
		if (fns->plugin_file) {	/* it is a plugin */
			void (*release_struct) (SqlDataDisplayFns * fns);
			g_print ("Freeing plugin %s\n", fns->descr);
			g_free (fns->plugin_file);
			release_struct =
				lt_dlsym (fns->lib_handle, "release_struct");
			if (release_struct)
				(release_struct) (fns);
			lt_dlclose (fns->lib_handle);
			if (release_struct)
				fns = NULL;
		}
		else {		/* built in data type display */
			g_print ("Freeing builtin %s\n", fns->descr);
			sql_data_display_free_display_fns (fns);
			fns = NULL;
		}
		if (fns)
			g_free (fns);
		list = srv->data_types_display;
		srv->data_types_display = g_slist_remove_link (srv->data_types_display,
							       srv->data_types_display);
		g_slist_free_1 (list);
	}

	/* closing ltdl lib */
	lt_dlexit ();

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
m_conn_closed (ServerAccess * srv)
{
	GSList *list, *hold;

	/* Note: empty the hash table of used plugins is done automatically through 
	 * callbacks ((update_hash_table_as_objects_destroyed_cb...) */
	/* FIXME on the above! */

	/* destroy all information lists */
	if (srv->data_types) {
		list = srv->data_types;
		while (list) {
			gtk_object_destroy (GTK_OBJECT (list->data));
			hold = list;
			list = g_slist_remove_link (list, list);
			g_slist_free_1 (hold);
		}
		srv->data_types = NULL;
#ifdef debug_signal
		g_print (">> 'DATA_TYPES_UPDATED' from m_conn_closed\n");
#endif
		gtk_signal_emit_by_name (GTK_OBJECT (srv),
					 "data_types_updated");
#ifdef debug_signal
		g_print ("<< 'DATA_TYPES_UPDATED' from m_conn_closed\n");
#endif
	}
	if (srv->data_functions) {
		list = srv->data_functions;
		while (list) {
			gtk_object_destroy (GTK_OBJECT (list->data));
			hold = list;
			list = g_slist_remove_link (list, list);
			g_slist_free_1 (hold);
		}
		srv->data_functions = NULL;
	}
	if (srv->data_aggregates) {
		list = srv->data_aggregates;
		while (list) {
			gtk_object_destroy (GTK_OBJECT (list->data));
			hold = list;
			list = g_slist_remove_link (list, list);
			g_slist_free_1 (hold);
		}
		srv->data_aggregates = NULL;
	}

	/* The command needs to change whenever the connection is closed */
	if (srv->cmd) {
		gda_command_free (srv->cmd);
		srv->cmd = NULL;
	}
}

static void
m_conn_open (ServerAccess * srv, gpointer data)
{
	srv->features.sequences = gda_connection_supports
		(GDA_CONNECTION (srv), GDA_Connection_FEATURE_SEQUENCES);
	srv->features.procs = gda_connection_supports
		(GDA_CONNECTION (srv), GDA_Connection_FEATURE_PROCS);
	srv->features.inheritance = gda_connection_supports
		(GDA_CONNECTION (srv), GDA_Connection_FEATURE_INHERITANCE);
	srv->features.xml_queries = gda_connection_supports
		(GDA_CONNECTION (srv), GDA_Connection_FEATURE_XML_QUERIES);

#ifdef debug_signal
	g_print (">> 'CONN_OPENED' from serveraccess->m_conn_open\n");
#endif
	gtk_signal_emit (GTK_OBJECT (srv), server_access_signals[CONN_OPENED]);
#ifdef debug_signal
	g_print ("<< 'CONN_OPENED' from serveraccess->m_conn_open\n");
#endif
}


/*
 * open/close and query functions
 */

void
server_access_open_connect (ServerAccess * srv)
{
	GdaDsn *dsn;

	if (gda_connection_is_open (GDA_CONNECTION (srv)))
		server_access_close_connect (srv);

	dsn = gda_dsn_find_by_name (srv->gda_datasource->str);
	if (!dsn) {
		GdaError *error;
		error = gda_error_new ();
		error->description =
			g_strdup_printf (_
					 ("No datasource '%s' defined in your GDA configuration"),
					 srv->gda_datasource->str);
		error->source = g_strdup ("[GNOME DB Client Widgets]");
		gda_connection_add_single_error (GDA_CONNECTION (srv), error);
		return;
	}

	gda_connection_set_provider (GDA_CONNECTION (srv),
				     GDA_DSN_PROVIDER (dsn));

	gda_connection_open (GDA_CONNECTION (srv), GDA_DSN_DSN (dsn),
			     srv->user_name->str, srv->password->str);
}

gboolean
server_access_is_open (ServerAccess * srv)
{
	return gda_connection_is_open (GDA_CONNECTION (srv)) ? TRUE : FALSE;
}

void
server_access_close_connect (ServerAccess * srv)
{
	if (gda_connection_is_open (GDA_CONNECTION (srv))) {
#ifdef debug_signal
		g_print (">> 'CONN_TO_CLOSE' from server_access_close_connect\n");
#endif
		gtk_signal_emit (GTK_OBJECT (srv),
				 server_access_signals[CONN_TO_CLOSE]);
#ifdef debug_signal
		g_print ("<< 'CONN_TO_CLOSE' from server_access_close_connect\n");
#endif
		gda_connection_close (GDA_CONNECTION (srv));
#ifdef debug_signal
		g_print (">> 'CONN_CLOSED' from server_access_close_connect\n");
#endif
		gtk_signal_emit (GTK_OBJECT (srv),
				 server_access_signals[CONN_CLOSED]);
#ifdef debug_signal
		g_print ("<< 'CONN_CLOSED' from server_access_close_connect\n");
#endif
	}
}

void
server_access_close_connect_no_warning (ServerAccess * srv)
{
	if (gda_connection_is_open (GDA_CONNECTION (srv))) {
		gda_connection_close (GDA_CONNECTION (srv));
#ifdef debug_signal
		g_print (">> 'CONN_CLOSED' from server_access_close_connect_no_warning\n");
#endif
		gtk_signal_emit (GTK_OBJECT (srv),
				 server_access_signals[CONN_CLOSED]);
#ifdef debug_signal
		g_print ("<< 'CONN_CLOSED' from server_access_close_connect_no_warning\n");
#endif
	}
}

ServerResultset *
server_access_do_query (ServerAccess * srv, gchar * query)
{
	ServerResultset *qres = NULL;
	GdaRecordset *res;
	gulong reccount;

	if (gda_connection_is_open (GDA_CONNECTION (srv))) {
		if (!srv->cmd) {
			srv->cmd = gda_command_new ();
			gda_command_set_connection (srv->cmd,
						    GDA_CONNECTION (srv));
		}

		gda_command_set_text (srv->cmd, query);
		res = gda_command_execute (srv->cmd, &reccount, 0);

		if (res)
			qres = SERVER_RESULTSET (server_resultset_new (res));
	}
	return qres;
}


/*
 * data type, etc lookup and management
 */

void
server_access_refresh_datas (ServerAccess * srv)
{
	server_data_type_update_list (srv);
	server_function_update_list (srv);
	server_aggregate_update_list (srv);
}

gchar *
server_access_get_data_type (ServerAccess * srv, gchar * oid)
{
	ServerDataType *dt;
	dt = server_data_type_get_from_server_type (srv->data_types, atoi (oid));
	if (dt)
		return dt->sqlname;
	else
		return NULL;
}

GList *
server_access_get_data_type_list (ServerAccess * srv)
{
	return server_data_type_get_name_list (srv->data_types);
}

ServerDataType *
server_access_get_type_from_name (ServerAccess * srv, gchar * name)
{
	return server_data_type_get_from_name (srv->data_types, name);
}

ServerDataType *
server_access_get_type_from_oid (ServerAccess * srv, gchar * oid)
{
	return server_data_type_get_from_server_type (srv->data_types,
						   atoi (oid));
}


void
server_access_build_xml_tree (ServerAccess * srv, xmlDocPtr doc)
{
	xmlNodePtr toptree, tree, subtree, subsubtree;
	GSList *list, *list2;
	gchar *str;

	/* main node */
	toptree = xmlNewChild (doc->xmlRootNode, NULL, "SERVER", NULL);
	xmlSetProp (toptree, "descr", srv->description);

	/* data types */
	tree = xmlNewChild (toptree, NULL, "DATATYPES", NULL);
	list = srv->data_types;
	while (list) {
		ServerDataType *dt;
		SqlDataDisplayFns *fns;

		dt = SERVER_DATA_TYPE (list->data);
		subtree = xmlNewChild (tree, NULL, "type", NULL);
		str = g_strdup_printf ("DT%s", dt->sqlname);
		xmlSetProp (subtree, "name", str);
		g_free (str);
		xmlSetProp (subtree, "descr", dt->descr);
		str = g_strdup_printf ("%d", dt->numparams);
		xmlSetProp (subtree, "nparam", str);
		g_free (str);
		str = g_strdup_printf ("%d", dt->server_type);
		xmlSetProp (subtree, "sqltype", str);
		g_free (str);
		str = g_strdup_printf ("%d", dt->gda_type);
		xmlSetProp (subtree, "gdatype", str);
		g_free (str);
		/* if there is a plugin for that data type, write it */
		if ((fns = g_hash_table_lookup (srv->types_objects_hash, dt))) {
			xmlSetProp (subtree, "plugin", fns->plugin_name);
		}
		list = g_slist_next (list);
	}
	/* functions */
	tree = xmlNewChild (toptree, NULL, "PROCEDURES", NULL);
	list = srv->data_functions;
	while (list) {
		ServerFunction *df;
		df = SERVER_FUNCTION (list->data);
		subtree = xmlNewChild (tree, NULL, "function", NULL);
		xmlSetProp (subtree, "name", df->sqlname);
		str = g_strdup_printf ("PR%s", df->objectid);
		xmlSetProp (subtree, "id", str);
		g_free (str);
		xmlSetProp (subtree, "descr", df->descr);
		if (df->is_user)
			xmlSetProp (subtree, "user", "t");
		else
			xmlSetProp (subtree, "user", "f");
		/* params */
		if (df->result_type) {
			subsubtree =
				xmlNewChild (subtree, NULL, "param", NULL);
			str = g_strdup_printf ("DT%s",
					       df->result_type->sqlname);
			xmlSetProp (subsubtree, "type", str);
			g_free (str);
			xmlSetProp (subsubtree, "way", "out");
		}
		list2 = df->args;
		while (list2) {
			subsubtree =
				xmlNewChild (subtree, NULL, "param", NULL);
			str = g_strdup_printf ("DT%s",
					       SERVER_DATA_TYPE (list2->data)->
					       sqlname);
			xmlSetProp (subsubtree, "type", str);
			g_free (str);
			xmlSetProp (subsubtree, "way", "in");
			list2 = g_slist_next (list2);
		}
		list = g_slist_next (list);
	}

	/* aggregates */
	tree = xmlNewChild (toptree, NULL, "AGGREGATES", NULL);
	list = srv->data_aggregates;
	while (list) {
		ServerAggregate *da;
		da = SERVER_AGGREGATE (list->data);
		subtree = xmlNewChild (tree, NULL, "agg", NULL);
		xmlSetProp (subtree, "name", da->sqlname);
		xmlSetProp (subtree, "descr", da->descr);
		str = g_strdup_printf ("AG%s", da->objectid);
		xmlSetProp (subtree, "id", str);
		g_free (str);
		/* params */
		if (da->arg_type) {
			str = g_strdup_printf ("DT%s", da->arg_type->sqlname);
			xmlSetProp (subtree, "arg", str);
			g_free (str);
		}
		list = g_slist_next (list);
	}
}


/* WARNING: the XML doc MUST be a valid one with regards to the DTD,
   otherwise, the result is not known! */
gboolean
server_access_build_from_xml_tree (ServerAccess * srv, xmlNodePtr node)
{
	gboolean ok = TRUE;
	xmlNodePtr tree, subtree;
	gchar *str;

	if (srv->data_types || srv->data_functions || srv->data_aggregates) {
		gnome_error_dialog (_
				    ("INTERNAL ERROR:\nTrying to load an ServerAccess object "
				     "which already have some data inside. Clean it "
				     "before."));
		return FALSE;
	}

	tree = node->xmlChildrenNode;
	while (tree) {
		if (!strcmp (tree->name, "DATATYPES")) {
			subtree = tree->xmlChildrenNode;
			while (subtree) {
				ServerDataType *dt;
				if (!strcmp (subtree->name, "type")) {
					dt = SERVER_DATA_TYPE (server_data_type_new
							    ());
					str = xmlGetProp (subtree, "name");
					server_data_type_set_sqlname (dt,
								   str + 2);
					g_free (str);

					if ((str =
					     xmlGetProp (subtree, "descr"))) {
						server_data_type_set_descr (dt,
									 str);
						g_free (str);
					}

					str = xmlGetProp (subtree, "nparam");
					dt->numparams = atoi (str);
					g_free (str);

					str = xmlGetProp (subtree, "sqltype");
					dt->server_type = atoi (str);
					g_free (str);

					str = xmlGetProp (subtree, "gdatype");
					dt->gda_type = atoi (str);
					g_free (str);

					/* default display function */
					dt->display_fns =
						server_access_get_display_fns_from_gda
						(srv, dt);
					srv->data_types =
						g_slist_append (srv->
								data_types,
								dt);

					/* plugin for this data type */
					if ((str =
					     xmlGetProp (subtree,
							 "plugin"))) {
						GSList *plist;
						gboolean found = FALSE;
						SqlDataDisplayFns *fns;
						plist = srv->
							data_types_display;
						while (plist && !found) {
							fns = (SqlDataDisplayFns *) (plist->data);
							if ((fns->plugin_name)
							    && !strcmp (fns->
									plugin_name,
									str))
							{
								server_access_bind_object_display
									(srv,
									 GTK_OBJECT
									 (dt),
									 fns);
								found = TRUE;
							}
							plist = g_slist_next
								(plist);
						}
						g_free (str);
					}
				}
				subtree = subtree->next;
			}
#ifdef debug_signal
			g_print (">> 'DATA_TYPES_UPDATED' from server_access_build_from_xml_tree\n");
#endif
			gtk_signal_emit_by_name (GTK_OBJECT (srv),
						 "data_types_updated");
#ifdef debug_signal
			g_print ("<< 'DATA_TYPES_UPDATED' from server_access_build_from_xml_tree\n");
#endif
		}

		if (!strcmp (tree->name, "PROCEDURES")) {
			subtree = tree->xmlChildrenNode;
			while (subtree) {
				xmlNodePtr node;
				ServerFunction *df;
				if (!strcmp (subtree->name, "function")) {
					df = SERVER_FUNCTION
						(server_function_new ());
					str = xmlGetProp (subtree, "name");
					server_function_set_sqlname (df,
								       str);
					g_free (str);

					str = xmlGetProp (subtree, "id");
					df->objectid = g_strdup (str + 2);
					g_free (str);

					if ((str =
					     xmlGetProp (subtree, "descr"))) {
						server_function_set_descr
							(df, str);
						g_free (str);
					}

					str = xmlGetProp (subtree, "user");
					if (*str == 't')
						df->is_user = TRUE;
					else
						df->is_user = FALSE;
					g_free (str);

					/* parameters */
					node = subtree->xmlChildrenNode;
					while (node) {
						if (!strcmp
						    (node->name, "param")) {
							ServerDataType *dt;
							str = xmlGetProp
								(node,
								 "type");
							dt = server_data_type_get_from_xml_id (srv, str);
							g_free (str);
							if (!dt)
								ok = FALSE;
							else {
								str = xmlGetProp (node, "way");
								if (!strcmp
								    (str,
								     "out"))
									df->result_type = dt;
								else
									df->args = g_slist_append (df->args, dt);
								g_free (str);
							}
						}
						if (ok)
							node = node->next;
						else
							node = NULL;
					}
					if (ok)
						srv->data_functions =
							g_slist_append (srv->
									data_functions,
									df);
				}
				subtree = subtree->next;
			}
		}

		if (!strcmp (tree->name, "AGGREGATES")) {
			subtree = tree->xmlChildrenNode;
			while (subtree) {
				ServerAggregate *da;
				ServerDataType *dt;
				if (!strcmp (subtree->name, "agg")) {
					da = SERVER_AGGREGATE
						(server_aggregate_new ());
					str = xmlGetProp (subtree, "name");
					server_aggregate_set_sqlname (da,
									str);
					g_free (str);

					str = xmlGetProp (subtree, "id");
					da->objectid = g_strdup (str + 2);
					g_free (str);

					if ((str =
					     xmlGetProp (subtree, "descr"))) {
						server_aggregate_set_descr
							(da, str);
						g_free (str);
					}

					if ((str =
					     xmlGetProp (subtree, "arg"))) {
						dt = server_data_type_get_from_xml_id (srv, str);
						if (dt)
							da->arg_type = dt;
						else
							ok = FALSE;
						g_free (str);
					}
					else
						da->arg_type = NULL;

					if (ok)
						srv->data_aggregates =
							g_slist_append (srv->
									data_aggregates,
									da);
				}
				subtree = subtree->next;
			}
		}

		if (tree)
			tree = tree->next;
	}

	return ok;
}

SqlDataDisplayFns *
server_access_get_display_fns_from_gda (ServerAccess * srv, ServerDataType * dt)
{
	SqlDataDisplayFns *fns = NULL, *ptr;
	gint i;
	GSList *list;

	list = srv->data_types_display;	/* SHOULD NEVER BE NULL! */
	while (list && !fns) {
		ptr = (SqlDataDisplayFns *) (list->data);
		if (ptr->nb_gda_type == 0)	/* 0 => ALL gda types */
			fns = ptr;
		i = 0;
		while (!fns && (i < ptr->nb_gda_type)) {
			if (ptr->valid_gda_types[i] == dt->gda_type)
				fns = ptr;
			i++;
		}

		list = g_slist_next (list);
	}

	return fns;
}

SqlDataDisplayFns *
server_access_get_object_display_fns (ServerAccess * srv, GtkObject * obj)
{
	SqlDataDisplayFns *fns = NULL;
	GSList *list;
	gpointer data;

	if (obj && GTK_IS_OBJECT (obj) && srv) {
		/* check to see if there is a specified bind for that object */
		fns = g_hash_table_lookup (srv->types_objects_hash, obj);

		/* if no fns has been found, we get the default ones */
		if (!fns && IS_SERVER_DATA_TYPE (obj))
			fns = SERVER_DATA_TYPE (obj)->display_fns;

		/* if no function found, then let's try to find a plugin for the 
		   object, using the declared functions to bind objects */
		list = srv->bindable_objects;
		while (list && !fns) {
			data = ((gpointer (*)(GtkObject *)) (list->data))
				(obj);
			if (data) {
				fns = g_hash_table_lookup (srv->
							   types_objects_hash,
							   data);

				/* if no fns has been found, we get the default ones */
				if (!fns && GTK_IS_OBJECT (data)
				    && IS_SERVER_DATA_TYPE (data)) {
					fns = SERVER_DATA_TYPE (data)->
						display_fns;
				}
			}
			list = g_slist_next (list);
		}
	}
	if (!fns)		/* default basic functions for every object */
		fns = (SqlDataDisplayFns *) srv->data_types_display->data;
	return fns;
}

static void bound_object_destroy_cb (GtkObject * obj, ServerAccess * srv);
void
server_access_bind_object_display (ServerAccess * srv,
				GtkObject * obj, SqlDataDisplayFns * fns)
{
	g_hash_table_insert (srv->types_objects_hash, obj, fns);
	gtk_signal_connect (obj, "destroy",
			    GTK_SIGNAL_FUNC (bound_object_destroy_cb), srv);
	gtk_signal_emit (GTK_OBJECT (srv),
			 server_access_signals[OBJECTS_BINDINGS_UPDATED]);
}

static void
bound_object_destroy_cb (GtkObject * obj, ServerAccess * srv)
{
	server_access_unbind_object_display (srv, obj);
}


void
server_access_unbind_object_display (ServerAccess * srv, GtkObject * obj)
{
	g_hash_table_remove (srv->types_objects_hash, obj);
	gtk_signal_disconnect_by_func (obj,
				       GTK_SIGNAL_FUNC
				       (bound_object_destroy_cb), srv);
	gtk_signal_emit (GTK_OBJECT (srv),
			 server_access_signals[OBJECTS_BINDINGS_UPDATED]);
}


struct foreach_hash_struct
{
	GHashTable *hash;
	gpointer rem_value;
	gpointer repl_value;	/* if not NULL; for every node with rem_value, 
				   create a new node with repl_value instead */
};

static void foreach_hash_cb (gpointer key, gpointer value,
			     struct foreach_hash_struct *user_data);
void
server_access_rescan_display_plugins (ServerAccess * srv, gchar * path)
{
	struct dirent *dir;
	DIR *dstream;
	gchar *str, *key, *key2;
	GSList *list, *list2, *newones = NULL;
	struct foreach_hash_struct *fhs;

	/* FIXME: it appears that the program hangs if we try to access
	   not a lib but an executable program. maybe try to use the "standard"
	   libtool calls. */
	dstream = opendir (path);
	if (!dstream) {
		g_print ("Cannot open %s\n", path);
	}
	else {
		dir = readdir (dstream);
		while (dir) {
			if ((strlen (dir->d_name) > 3) &&	/* only .so files */
			    !strcmp (dir->d_name + strlen (dir->d_name) - 3,
				     ".so")) {
				lt_dlhandle lib;
				SqlDataDisplayFns *fns;
				SqlDataDisplayFns *(*fpi) ();

				str = g_strdup_printf ("%s/%s", path,
						       dir->d_name);
				if ((lib = lt_dlopen (str))) {
					fpi = lt_dlsym (lib,
							"fetch_plugin_interface");
					if (fpi) {
						fns = (fpi) ();
						g_print ("\tNew plugin (%p/%p): %s(V%s)\n", fns, fns->gdafield_to_widget, fns->plugin_name, fns->version);
						fns->lib_handle = lib;
						fns->plugin_file =
							g_strdup (str);
						srv->data_types_display =
							g_slist_append (srv->
									data_types_display,
									fns);
						newones =
							g_slist_append
							(newones, fns);
					}
					else {
						lt_dlclose (lib);
					}
				}
				g_free (str);
			}
			dir = readdir (dstream);
		}
		closedir (dstream);

		/*
		 * Now removing any old plugin (reloaded ones get replaced into the
		 * hash table)
		 */
		fhs = g_new (struct foreach_hash_struct, 1);
		fhs->hash = srv->types_objects_hash;
		list = srv->data_types_display;
		while (list) {
			if ((((SqlDataDisplayFns *) (list->data))->plugin_name) && !g_slist_find (newones, list->data)) {	/* only old plugins */
				void (*release_struct) (SqlDataDisplayFns *
							fns);
				SqlDataDisplayFns *fns;
				gboolean found;

				fns = (SqlDataDisplayFns *) (list->data);
				fhs->rem_value = fns;
				fhs->repl_value = NULL;

				/* is there a new plugin with the same key? */
				key = (fns->get_unique_key) ();
				list2 = newones;
				found = FALSE;
				while (list2 && !found) {
					key2 = (((SqlDataDisplayFns
						  *) (list2->data))->
						get_unique_key)
						();
					if (!strcmp (key, key2)) {
						fhs->repl_value = list2->data;
						found = TRUE;
					}
					g_free (key2);
					list2 = g_slist_next (list2);
				}
				g_free (key);

				g_hash_table_foreach (srv->types_objects_hash,
						      (GHFunc)
						      (foreach_hash_cb), fhs);
				srv->data_types_display = g_slist_remove_link (srv->data_types_display,
									       list);
				g_slist_free_1 (list);
				/* freeing the ressources for that plugin */
				g_free (fns->plugin_file);
				release_struct =
					lt_dlsym (fns->lib_handle,
						  "release_struct");
				if (release_struct)
					(release_struct) (fns);
				lt_dlclose (fns->lib_handle);

				list = srv->data_types_display;
			}
			else
				list = g_slist_next (list);
		}
		g_free (fhs);
		g_slist_free (newones);
	}
}

static void
foreach_hash_cb (gpointer key, gpointer value,
		 struct foreach_hash_struct *user_data)
{
	if (value == user_data->rem_value) {
		g_hash_table_remove (user_data->hash, key);
		if (user_data->repl_value)
			g_hash_table_insert (user_data->hash, key,
					     user_data->repl_value);
	}
}


void
server_access_declare_object_bindable (ServerAccess * srv,
				    gpointer (*func) (GtkObject *))
{
	if (!g_slist_find (srv->bindable_objects, func))
		srv->bindable_objects =
			g_slist_append (srv->bindable_objects, func);
}
