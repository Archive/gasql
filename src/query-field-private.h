/* query-field-private.h
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 * Copyright (C) 2001 Fernando Martins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY_FIELD_PRIVATE__
#define __QUERY_FIELD_PRIVATE__

#include <gtk/gtkobject.h>
#include "conf-manager.h"
#include "database.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

/* Private stuff for the QueryField object */
	void query_field_set_activated (QueryField *qf, gboolean activated);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
