/* database.h
 *
 * Copyright (C) 1999 - 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __DATABASE__
#define __DATABASE__

#include <gtk/gtksignal.h>
#include <gtk/gtkobject.h>
#include <gtk/gtk.h>
#include "server-rs.h"
#include "server-access.h"
#include "gasql_xml.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */


/*
 *
 * Main object: Database
 *
 */

#define DATABASE(obj)          GTK_CHECK_CAST (obj, database_get_type(), Database)
#define DATABASE_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, database_get_type (), DatabaseClass)
#define IS_DATABASE(obj)       GTK_CHECK_TYPE (obj, database_get_type ())

	typedef struct _Database Database;
	typedef struct _DatabaseClass DatabaseClass;
	typedef struct _DbItem DbItem;
	typedef struct _DbItemClass DbItemClass;
	typedef struct _DbTable DbTable;
	typedef struct _DbTableClass DbTableClass;
	typedef struct _DbField DbField;
	typedef struct _DbFieldClass DbFieldClass;
	typedef struct _DbSequence DbSequence;
	typedef struct _DbSequenceClass DbSequenceClass;
	typedef struct _FieldLink FieldLink;
	typedef struct _FieldLinkClass FieldLinkClass;


	/* struct for the object's data */
	struct _Database
	{
		GtkObject     object;

		/* Db accessibility */
		gchar        *name;
		gboolean      is_fault;
		GSList       *tables;	/* list of data on DbTable objects */
		GSList       *sequences;	/* list of data on DbSequence objects */
		GSList       *users;	/* holds the list of users (ptrs to gchar) */

		ServerAccess *srv;
	};

	/* struct for the object's class */
	struct _DatabaseClass
	{
		GtkObjectClass parent_class;

		void (*updated) (Database * db);
		void (*struct_saved) (Database * db);
		void (*fault) (Database * db);
		void (*table_created) (Database * db, DbTable * new_table);
		void (*table_created_f) (Database * db, DbTable * new_table);
		void (*table_dropped) (Database * db, DbTable * table);
		void (*seq_created) (Database * db, DbSequence * new_sequence);
		void (*seq_dropped) (Database * db, DbSequence * sequence);
		void (*field_created) (Database * db, DbTable * table,
				       DbField * new_field);
		void (*field_dropped) (Database * db, DbTable * table,
				       DbField * field);
		void (*fs_link_created) (Database * db, DbSequence * seq,
					 DbField * f);
		void (*fs_link_dropped) (Database * db, DbSequence * seq,
					 DbField * f);
		void (*progress) (Database * db, gchar * msg, guint now,
				  guint total);
	};

	/* 
	 * generic widget's functions 
	 */
	GtkType    database_get_type               (void);
	GtkObject *database_new                    (ServerAccess * srv);
	void       database_dump_tables            (Database * db);	/* prints the structure */
	void       database_dump_links             (Database * db);
	GSList    *find_user_name                  (Database * db, gchar * uname);

	/*
	 * memory structure lookup
	 */
	DbTable   *database_find_table_from_name     (Database * db, gchar * name);
	DbTable   *database_find_table_from_field    (Database * db, DbField * field);
	DbField   *database_find_field_from_names    (Database * db, gchar * table, gchar *field);


	/* returns the order of a field in the field list (1 for first element and
	   0 if field does not belong to any table).
	   if table = NULL, looks for the right table first. */
	guint       database_get_field_order           (Database * db, DbTable * table, DbField *field);

	DbSequence *database_find_sequence_by_name     (Database * db, gchar * name);
	DbSequence *database_find_sequence_to_field    (Database * db, DbField * field);

	/*
	 * Field links manipulation
	 */
	void       database_insert_seq_field_link      (Database * db, DbSequence *seq, DbField *field);
	/* if seq==NULL, removes all sequence links to that field 
	   if field=NULL, removes all links from the sequence */
	void       database_delete_seq_field_link      (Database * db, DbSequence *seq, DbField *field);

	/*
	 * XML storage
	 */
	DbTable    *database_find_table_from_xml_name   (Database * db, gchar * xmlname);
	DbField    *database_find_field_from_xml_name   (Database * db, gchar * xmlname);
	DbSequence *database_find_sequence_from_xml_name(Database * db, gchar * xmlname);
	void        database_refresh                    (Database * db, ServerAccess * srv);
	void        database_build_xml_tree             (Database * db, xmlDocPtr doc);
	gboolean    database_build_db_from_xml_tree     (Database * db, xmlNodePtr node);
	void        sql_mem_link_load_from_xml_node     (Database * db, xmlNodePtr tabletree);


/*
 *
 * Object common to memory representation of the DB: DbItems
 * items such as tables, fields, sequences, links inherit this 
 * object. Not to be instantiated!
 *
 */
#define DB_ITEM(obj)          GTK_CHECK_CAST (obj, db_item_get_type(), DbItem)
#define DB_ITEM_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, db_item_get_type (), DbItemClass)
#define IS_DB_ITEM(obj)       GTK_CHECK_TYPE (obj, db_item_get_type ())

	/* struct for the object's data */
	struct _DbItem
	{
		GtkObject object;
		gboolean  updated;
	};

	/* struct for the object's class */
	struct _DbItemClass
	{
		GtkObjectClass parent_class;

		void (*load_fault) (DbItem * item);
	};

	/* generic widget's functions */
	GtkType    db_item_get_type (void);
	GtkObject *db_item_new      (void);



/*
 *
 * Object representing a table
 *
 */

#define DB_TABLE(obj)          GTK_CHECK_CAST (obj, db_table_get_type(), DbTable)
#define DB_TABLE_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, db_table_get_type (), DbTableClass)
#define IS_DB_TABLE(obj)       GTK_CHECK_TYPE (obj, db_table_get_type ())

	/* struct for the object's data */
	struct _DbTable
	{
		DbItem    object;
		gchar    *name;
		gchar    *comments;
		gboolean  is_user_comments;
		gchar    *owner;	/* ptr do a gchar* which is part of a list => don't free */
		gboolean  is_view;	/* view or table? */
		GSList   *fields;	/* list data on DbField */
		GSList   *parents;	/* list data on DbTable */
	};

	/* struct for the object's class */
	struct _DbTableClass
	{
		DbItemClass parent_class;

		void (*field_created)    (Database * db, DbField * new_field);
		void (*field_dropped)    (Database * db, DbField * field);
		void (*comments_changed) (DbTable * table);
	};

	/* generic widget's functions */
	GtkType    db_table_get_type           (void);
	GtkObject *db_table_new                (void);

	void       db_table_dump_fields        (DbTable * t);
	void       db_table_dump_as_graph      (DbTable * t, FILE * st);
	DbField   *db_table_find_field_by_name (DbTable * t, gchar * name);
	void       db_table_set_comments       (DbTable * table, gpointer conf_manager,
						gchar * comments, gboolean is_user_comments);
	
	/* xml ID */
	gchar     *db_table_get_xml_id         (DbTable * table);

/*
 *
 * Object representing a field
 *
 */

#define DB_FIELD(obj)          GTK_CHECK_CAST (obj, db_field_get_type(), DbField)
#define DB_FIELD_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, db_field_get_type (), DbFieldClass)
#define IS_DB_FIELD(obj)       GTK_CHECK_TYPE (obj, db_field_get_type ())

	/* struct for the object's data */
	struct _DbField
	{
		DbItem           object;
		gchar           *name;
		ServerDataType  *type;
		gint             length;	/* length of the field and -1 if variable (f.e. text) */
		gboolean         null_allowed;	/* defaults to TRUE */
		gboolean         is_key;	/* defaults to FALSE */

		/* put a pointer to a default value here */
		gchar            *default_val;	/* the str is the string which needs to be included
						   into an insert string, without any modification
						   fe: 'hello' */
	};

	/* struct for the object's class */
	struct _DbFieldClass
	{
		DbItemClass parent_class;
	};

	/* generic widget's functions */
	GtkType    db_field_get_type   (void);
	GtkObject *db_field_new        (void);

	void       db_field_dump       (DbField * field);

	/* xml ID */
	gchar     *db_field_get_xml_id (DbField * field, Database *db);

/*
 *
 * Object representing a sequence
 *
 */

#define DB_SEQUENCE(obj)          GTK_CHECK_CAST (obj, db_sequence_get_type(), DbSequence)
#define DB_SEQUENCE_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, db_sequence_get_type (), DbSequenceClass)
#define IS_DB_SEQUENCE(obj)       GTK_CHECK_TYPE (obj, db_sequence_get_type ())

	/* struct for the object's data */
	struct _DbSequence
	{
		DbItem    object;
		gchar    *name;
		gchar    *comments;
		gchar    *owner;	/* ptr do a gchar* which is part of a list => don't free */
		GSList   *field_links;	/* data to DbField objects on which the seq. acts */
	};

	/* struct for the object's class */
	struct _DbSequenceClass
	{
		DbItemClass parent_class;
	};

	/* generic widget's functions */
	GtkType    db_sequence_get_type   (void);
	GtkObject *db_sequence_new        (void);

	/* xml ID */
	gchar     *db_sequence_get_xml_id (DbSequence * seq);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
