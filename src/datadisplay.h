/* datadisplay.h
 *
 * Copyright (C) 1999 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __DATA_DISPLAY__
#define __DATA_DISPLAY__

#include <gtk/gtk.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define DATA_DISPLAY(obj)          GTK_CHECK_CAST (obj, data_display_get_type(), DataDisplay)
#define DATA_DISPLAY_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, data_display_get_type (), DataDisplayClass)
#define IS_DATA_DISPLAY(obj)       GTK_CHECK_TYPE (obj, data_display_get_type ())

	/*
	 * Very simple object wrapper for the widgets that will be used to display
	 * the data. What is important is the "contents_modified" signal
	 */

	typedef struct _DataDisplay DataDisplay;
	typedef struct _DataDisplayClass DataDisplayClass;

	/* struct for the object's data */
	struct _DataDisplay
	{
		GtkVBox object;
		GSList *childs;	/* list of GtkWidgets for easier retreival */
	};

	/* struct for the object's class */
	struct _DataDisplayClass
	{
		GtkVBoxClass parent_class;

		void (*contents_modified) (DataDisplay * wid);
	};

	/* generic widget's functions */
	guint data_display_get_type (void);
	GtkWidget *data_display_new ();

	/* default packing method */
	void data_display_pack_default (DataDisplay * dd, GtkWidget * wid);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
