/* sqldatawid.h
 *
 * Copyright (C) 1999, 2000 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __SQL_DATA_WID__
#define __SQL_DATA_WID__

#include <gnome.h>
#include "server-access.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define SQL_DATA_WID(obj)          GTK_CHECK_CAST (obj, sql_data_wid_get_type(), SqlDataWid)
#define SQL_DATA_WID_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, sql_data_wid_get_type (), SqlDataWidClass)
#define IS_SQL_DATA_WID(obj)       GTK_CHECK_TYPE (obj, sql_data_wid_get_type ())


	typedef struct _SqlDataWid SqlDataWid;
	typedef struct _SqlDataWidClass SqlDataWidClass;

	/* struct for the object's data */
	struct _SqlDataWid
	{
		GtkVBox object;

		ServerAccess *srv;
		gint mode;	/* 0 -> no function displayed */
		/* 1 -> only user defined functions displayed */
		/* 2 -> only system functions displayed */
		/* 3 -> all functions are displayed */

		/* main widgets */
		GtkWidget *notebook;
		ServerDataType *sel_type;
		GtkWidget *typesclist;
		GtkWidget *typestext;
		ServerFunction *sel_func;
		GtkWidget *functionsclist;
		GtkWidget *functionstext;
		ServerAggregate *sel_agg;
		GtkWidget *aggsclist;
		GtkWidget *aggstext;
	};

	/* struct for the object's class */
	struct _SqlDataWidClass
	{
		GtkVBoxClass parent_class;
	};

	/* generic widget's functions */
	guint sql_data_wid_get_type (void);
	GtkWidget *sql_data_wid_new (ServerAccess * srv);


	/* get a GnomeDialog with a SqlDataWid widget inside */
	GtkWidget *sql_data_wid_new_dlg (ServerAccess * srv);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
