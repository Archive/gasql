/* relship-view.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __RELSHIP_VIEW__
#define __RELSHIP_VIEW__

#include <gnome.h>
#include "query.h"
#include "canvas-base.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */


#define RELSHIP_VIEW(obj)          GTK_CHECK_CAST (obj, relship_view_get_type(), RelShipView)
#define RELSHIP_VIEW_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, relship_view_get_type (), RelShipViewClass)
#define IS_RELSHIP_VIEW(obj)       GTK_CHECK_TYPE (obj, relship_view_get_type ())


	typedef struct _RelShipView      RelShipView;
	typedef struct _RelShipViewClass RelShipViewClass;


	/* struct for the object's data */
	struct _RelShipView
	{
		GnomeCanvas         widget;

		GnomeCanvasItem    *background;
		Query              *query;
		GSList             *items; /* canvas items, non ordered */
	};

	/* struct for the object's class */
	struct _RelShipViewClass
	{
		GnomeCanvasClass parent_class;

		void (*item_moved)  (RelShipView * rs, CanvasBase *item);
	};

	/* generic widget's functions */
	guint            relship_view_get_type        (void);
	GtkWidget       *relship_view_new             (Query *q);

	void             relship_view_refresh_items   (RelShipView * rs);

	GnomeCanvasItem *relship_view_find_query_view (RelShipView * rs, QueryView *qv);
#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
