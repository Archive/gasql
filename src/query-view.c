/* query-view.c
 *
 * Copyright (C) 2002 Vivien Malerba
 * Copyright (C) 2002 Fernando Martins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "query.h"


static void query_view_class_init (QueryViewClass * class);
static void query_view_init       (QueryView * qv);
static void query_view_destroy    (GtkObject * object);

guint
query_view_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"QueryView",
			sizeof (QueryView),
			sizeof (QueryViewClass),
			(GtkClassInitFunc) query_view_class_init,
			(GtkObjectInitFunc) query_view_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_object_get_type (), &f_info);
	}

	return f_type;
}

static void
query_view_class_init (QueryViewClass * class)
{
	GtkObjectClass *object_class = NULL;

	object_class = (GtkObjectClass *) class;

	object_class->destroy = query_view_destroy;
}


static void
query_view_init (QueryView * qv)
{
	qv->obj = NULL;
	qv->occ = 0;
	qv->alias = NULL;
	qv->alias_exp = NULL;
	qv->ref = NULL;
	qv->query = NULL;
}



GtkObject *
query_view_new (Query *q) 
{
	GtkObject *obj;
	QueryView *qv;

	g_return_val_if_fail (q, NULL);
	g_return_val_if_fail (IS_QUERY (q), NULL);


	obj = gtk_type_new (query_view_get_type ());
	qv = QUERY_VIEW (obj);

	qv->query = q;

	return obj;
}


GtkObject *
query_view_new_copy (QueryView *qv)
{
	QueryView *newqv;

	g_return_val_if_fail (qv, NULL);
	g_return_val_if_fail (IS_QUERY_VIEW (qv), NULL);

	newqv = QUERY_VIEW (query_view_new (qv->query));
	query_view_set_view_obj (newqv, qv->obj);
	newqv->occ = qv->occ;
	if (qv->alias)
		newqv->alias = g_strdup (qv->alias);
	if (qv->ref)
		newqv->ref = g_strdup (qv->ref);
	return GTK_OBJECT (newqv);
}

static void view_obj_destroy_cb (GtkObject *obj, QueryView *qv);
static void
query_view_destroy (GtkObject * object)
{
	GtkObjectClass *parent_class = NULL;
	QueryView *qv;

	parent_class = gtk_type_class (gtk_object_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_VIEW (object));

	qv = QUERY_VIEW (object);

	if (qv->obj) { 
		gtk_signal_disconnect_by_func (qv->obj,
					       GTK_SIGNAL_FUNC (view_obj_destroy_cb),
					       qv);
		qv->obj = NULL;
	}

	if (qv->alias) {
		g_free (qv->alias);
		qv->alias = NULL;
	}
	if (qv->alias_exp) {
		g_free (qv->alias_exp);
		qv->alias_exp = NULL;
	}
	if (qv->ref) {
		g_free (qv->ref);
		qv->ref = NULL;
	}
	qv->occ = -1;

	g_print ("QueryView Destroy ... %p\n", object);

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

void query_view_set_query (QueryView *qv, Query *q)
{
	g_return_if_fail (qv && IS_QUERY_VIEW (qv));
	g_return_if_fail (q && IS_QUERY (q));
	
	qv->query = q;
}

void
query_view_set_view_obj (QueryView *qv, GtkObject *obj)
{
	g_return_if_fail (qv && IS_QUERY_VIEW (qv));

	if (qv->obj) 
		gtk_signal_disconnect_by_func (qv->obj,
					       GTK_SIGNAL_FUNC (view_obj_destroy_cb),
					       qv);
	
	qv->obj = obj;
	if (obj)
		gtk_signal_connect (qv->obj, "destroy",
				    GTK_SIGNAL_FUNC (view_obj_destroy_cb),
				    qv);
}

static void 
view_obj_destroy_cb (GtkObject *obj, QueryView *qv)
{
	gtk_object_destroy (GTK_OBJECT (qv));
}



/*
 * XML Related stuff 
 */

gchar *
query_view_get_xml_id (QueryView *qv)
{
	gchar *str, *id;

	id = query_get_xml_id (qv->query);
	str = g_strdup_printf ("%s:QV%d", id, g_slist_index (qv->query->views, qv));
	g_free (id);

	return str;
}

xmlNodePtr 
query_view_save_to_xml (QueryView *qv)
{
	xmlNodePtr node;
	gchar *str;

	node = xmlNewNode (NULL, "QueryView");

	str = query_view_get_xml_id (qv);
	xmlSetProp (node, "id", str);
	g_free (str);

	if (IS_DB_TABLE (qv->obj))
		str = db_table_get_xml_id (DB_TABLE (qv->obj));
	else
		str = query_get_xml_id (QUERY (qv->obj));
	xmlSetProp (node, "object", str);
	g_free (str);
	
	str = g_strdup_printf ("%d", qv->occ);
	xmlSetProp (node, "occ", str);
	g_free (str);
	
	if (qv->alias) 
		xmlSetProp (node, "alias", qv->alias);

	return node;
}

void query_view_load_from_xml (QueryView *qv, xmlNodePtr node)
{
	gchar *str;

	str = xmlGetProp (node, "object");
	if (str) {
		GtkObject *obj = NULL;
		gpointer ptr;

		/* QueryView is a table ?, a Query ? */
		ptr = database_find_table_from_xml_name (qv->query->conf->db, str);
		if (ptr)
			obj = GTK_OBJECT (ptr);
		else {
			ptr = query_find_from_xml_name (qv->query->conf, NULL, str);
			if (ptr)
				obj = GTK_OBJECT (ptr);
		}
				
		if (obj) {
			query_view_set_view_obj (qv, obj);
			g_free (str);
		}
		else {
			/* do it later */
			query_view_set_view_obj (qv, NULL);
			qv->ref = str; /* later we try to find this */
		}
	}
			
	str = xmlGetProp (node, "occ");
	if (str) {
		qv->occ = atoi (str);
		g_free (str);
	}
	else
		qv->occ = 0;
			
	str = xmlGetProp (node, "alias");
	qv->alias = str;
}

QueryView *
query_view_find_from_xml_name (ConfManager * conf, Query * start_query, gchar *xmlname)
{
	gchar *copy, *ptr;
	Query *q;
	QueryView *qv = NULL;

	copy = g_strdup (xmlname);
	ptr = strtok (copy , ":");
	q = query_find_from_xml_name (conf, start_query, ptr);
	if (q) {
		gint i;
		ptr = strtok (NULL , ":");
		i = atoi (ptr+2);
		qv = g_slist_nth_data (q->views, i);
	}

	g_free (copy);
	return qv;
}

/* field can be DbField or QueryField */
gboolean
query_view_contains_field (QueryView *qv, GtkObject *field)
{
	gboolean found = FALSE;

	g_return_val_if_fail (IS_QUERY_VIEW (qv), FALSE);
	if (IS_DB_TABLE (qv->obj)) {
		if (IS_DB_FIELD (field)) 
			found = (g_slist_find (DB_TABLE (qv->obj)->fields, DB_FIELD (field)) != NULL);
	}
	else {
		if (IS_QUERY (qv->obj)) {
			if (IS_QUERY_FIELD (field))
				found = (g_slist_find (QUERY (qv->obj)->fields, QUERY_FIELD (field)) != NULL);
		}
	}

	return found;
}

#define QUERY_VIEW_NAME(qv) IS_QUERY (qv->obj) ? \
		QUERY (qv->obj)->name : \
		DB_TABLE (qv->obj)->name

gchar *
query_view_get_textual (QueryView *qv)
{
	gchar *str, *retval;

	if (IS_QUERY (qv->obj))
		str = QUERY (qv->obj)->name;
	else
		str = DB_TABLE (qv->obj)->name;
	if (qv->occ)
		retval = g_strdup_printf ("%s (%d)", str, qv->occ);
	else
		retval = g_strdup (str);

	return retval;
}			


/* query_view_is_alias
 *
 * returns TRUE if the QueryView holds a duplicate instance of an
 * existing Query or Table in the current query. FALSE if it is a
 * real Query or Table
 *
 */

gboolean
query_view_is_alias (QueryView *qv)
{
	return qv->occ;
}

/* query_view_get_name
 *
 * if the QueryView is an alias, returns the alias. Otherwise,
 * returns the real name of its Table or Query. No need to free the returned string.
 * 
 *
 */

gchar *
query_view_get_name (QueryView *qv)
{
	gchar *str;

	if (qv->occ)
		str = qv->alias;
	else
		str = QUERY_VIEW_NAME(qv);

	return str;
}


/* query_view_get_real_name
 *
 * returns the name of its Table or Query, even if the QueryView is an alias, ie,
 * the alias name is ignored. No need to free the returned string.
 *
 */

gchar *
query_view_get_real_name (QueryView *qv)
{
	return QUERY_VIEW_NAME(qv);
}


/* query_view_get_aliased_name
 *
 * returns the SQL expression to define the alias, e.g., table AS table_1 or simply
 * the table name if there is no alias. No need to free the returned string.
 *
 * Implementation note: for efficiency reasons and user code simplification, the first time
 * this function is called, the alias expression is created (if there is an alias) and 
 * stored in the query view object
 *
 */

gchar *
query_view_get_alias_exp (QueryView *qv)
{
	gchar *str;

	if (qv->occ) {
		if (!qv->alias_exp)
			qv->alias_exp = g_strdup_printf ("%s AS %s", QUERY_VIEW_NAME(qv), qv->alias);
		str = qv->alias_exp;
	}
	else
		str = QUERY_VIEW_NAME(qv);

	return str;
}

gchar *
query_view_get_field_name (QueryView *qv, GtkObject *qvf)
{
	gchar *str = NULL;

	if (IS_QUERY (qv->obj)) {
		if (qvf && IS_QUERY_FIELD (qvf) && (QUERY_FIELD (qvf)->query == QUERY (qv->obj))) {
			if (QUERY_FIELD (qvf)->name && *(QUERY_FIELD (qvf)->name))
				str = g_strdup (QUERY_FIELD (qvf)->name);
			else {
				if (QUERY_FIELD (qvf)->is_printed)
					str = g_strdup (QUERY_FIELD (qvf)->alias);
				else
					str = query_field_get_xml_id (QUERY_FIELD (qvf));
			}
		}
	}
	
	if (!str && IS_DB_TABLE (qv->obj)) {
		if (qvf && IS_DB_FIELD (qvf) && g_slist_find (DB_TABLE (qv->obj)->fields, qvf)) {
			str = g_strdup (DB_FIELD (qvf)->name);
		}
	}

	if (!str)
		g_warning ("query_view_get_field_name returns a NULL value");

	return str;
}
