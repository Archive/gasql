/* choicecombo.c
 *
 * Copyright (C) 1999 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "choicecombo.h"

static void choice_combo_class_init (ChoiceComboClass * class);
static void choice_combo_init (ChoiceCombo * wid);
static void choice_combo_destroy (GtkObject * object);
static void choice_combo_changed_cb (GtkWidget * wid, ChoiceCombo * combo);

enum
{
	C_SELECTION_CHANGED,
	LAST_SIGNAL
};

static gint choice_combo_signals[LAST_SIGNAL] = { 0 };


guint
choice_combo_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Choice_Combo",
			sizeof (ChoiceCombo),
			sizeof (ChoiceComboClass),
			(GtkClassInitFunc) choice_combo_class_init,
			(GtkObjectInitFunc) choice_combo_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_combo_get_type (), &f_info);
	}

	return f_type;
}

static void
choice_combo_class_init (ChoiceComboClass * class)
{
	GtkObjectClass *object_class = NULL;

	object_class = (GtkObjectClass *) class;
	choice_combo_signals[C_SELECTION_CHANGED] =
		gtk_signal_new ("selection_changed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ChoiceComboClass,
						   selection_changed),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	gtk_object_class_add_signals (object_class, choice_combo_signals,
				      LAST_SIGNAL);
	class->selection_changed = NULL;
	object_class->destroy = choice_combo_destroy;
}

static void
choice_combo_init (ChoiceCombo * wid)
{
	wid->main_list = NULL;
}

GtkWidget *
choice_combo_new (void)
{
	GtkObject *obj;
	ChoiceCombo *wid;

	obj = gtk_type_new (choice_combo_get_type ());
	wid = CHOICE_COMBO (obj);

	gtk_combo_set_use_arrows (GTK_COMBO (wid), TRUE);
	gtk_editable_set_editable (GTK_EDITABLE (GTK_COMBO (wid)->entry),
				   FALSE);
	wid->changed_signal_handler =
		gtk_signal_connect (GTK_OBJECT (GTK_COMBO (wid)->entry),
				    "changed",
				    GTK_SIGNAL_FUNC (choice_combo_changed_cb),
				    wid);

	return GTK_WIDGET (obj);
}

static void
choice_combo_destroy (GtkObject * object)
{
	ChoiceCombo *combo;
	GtkObjectClass *parent_class = NULL;

	parent_class = gtk_type_class (gtk_combo_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CHOICE_COMBO (object));

	combo = CHOICE_COMBO (object);
	g_slist_free (combo->main_list);

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

void
choice_combo_set_content (ChoiceCombo * combo, GSList * list, gint offset)
{
	GList *nlist, *nptr;
	gpointer *selection;
	GSList *ptr, *lastptr;
	gchar **laststr = NULL, **str, *cptr;
	gchar *strinc;
	gint i, item, selitem;

	selection = choice_combo_get_selection (combo);
	nlist = NULL;
	if (combo->main_list)
		g_slist_free (combo->main_list);

	if (list) {
		item = 0;
		selitem = -1;
		combo->main_list = g_slist_copy (list);

		ptr = lastptr = list;
		i = 0;
		while (ptr) {
			if (ptr->data == NULL) {
				ptr = g_slist_next (ptr);
				strinc = g_strdup (_("NONE"));
				nlist = g_list_append (nlist, strinc);
				lastptr = ptr;
				laststr = NULL;
				item++;
			}
			else {
				cptr = (char *) (ptr->data);
				str = (gchar **) (cptr + offset);

				if ((lastptr != ptr) && laststr
				    && (!strcmp (*str, *laststr)))
					i++;
				else
					i = 0;
				strinc = g_strdup_printf ("%-*s",
							  i + (gint) strlen (*str),
							  *str);
				nlist = g_list_append (nlist, strinc);
				lastptr = ptr;
				laststr = str;

				if (ptr->data == selection)	/* set the selection to this item */
					selitem = item;
				ptr = g_slist_next (ptr);
				item++;
			}
		}
		gtk_signal_handler_block (GTK_OBJECT
					  (GTK_COMBO (combo)->entry),
					  combo->changed_signal_handler);
		gtk_combo_set_popdown_strings (GTK_COMBO (combo), nlist);
		if (selitem >= 0)
			gtk_list_select_item (GTK_LIST
					      (GTK_COMBO (combo)->list),
					      selitem);
		gtk_signal_handler_unblock (GTK_OBJECT
					    (GTK_COMBO (combo)->entry),
					    combo->changed_signal_handler);
		if (selitem < 0) {
#ifdef debug_signal
			g_print (">> 'SELECTION_CHANGED' from choice_combo_set_content\n");
#endif
			gtk_signal_emit (GTK_OBJECT (combo),
					 choice_combo_signals
					 [C_SELECTION_CHANGED],
					 choice_combo_get_selection
					 (CHOICE_COMBO (combo)));
#ifdef debug_signal
			g_print ("<< 'SELECTION_CHANGED' from choice_combo_set_content\n");
#endif
		}
		nptr = nlist;
		while (nptr) {
			g_free (nptr->data);
			nptr = g_list_next (nptr);
		}
		g_list_free (nlist);
	}
	else {
		combo->main_list = NULL;
		nlist = g_list_append (nlist, "NONE");
		gtk_combo_set_popdown_strings (GTK_COMBO (combo), nlist);
		g_list_free (nlist);
	}
}

gpointer
choice_combo_get_selection (ChoiceCombo * combo)
{
	gint pos;

	if (GTK_LIST (GTK_COMBO (combo)->list)->selection) {
		pos = g_list_index (GTK_LIST (GTK_COMBO (combo)->list)->
				    children,
				    GTK_LIST (GTK_COMBO (combo)->list)->
				    selection->data);

		return g_slist_nth_data (combo->main_list, pos);
	}
	else
		return NULL;
}

void
choice_combo_set_selection_txt (ChoiceCombo * combo, gchar * item)
{
	gint pos;
	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (combo)->entry), item);
	pos = g_list_index (GTK_LIST (GTK_COMBO (combo)->list)->children,
			    item);
	gtk_list_select_item (GTK_LIST (GTK_COMBO (combo)->list), pos);
}

void
choice_combo_set_selection_num (ChoiceCombo * combo, gint num)
{
	gtk_list_select_item (GTK_LIST (GTK_COMBO (combo)->list), num);
}

void
choice_combo_set_selection_ptr (ChoiceCombo * combo, gpointer ptr)
{
	gint num;

	if (g_slist_find (combo->main_list, ptr)) {
		num = g_slist_index (combo->main_list, ptr);
		gtk_list_select_item (GTK_LIST (GTK_COMBO (combo)->list),
				      num);
	}
}

static void
choice_combo_changed_cb (GtkWidget * wid, ChoiceCombo * combo)
{
#ifdef debug_signal
	g_print (">> 'SELECTION_CHANGED' from choice_combo_changed_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (combo),
			 choice_combo_signals[C_SELECTION_CHANGED],
			 choice_combo_get_selection (CHOICE_COMBO (combo)));
#ifdef debug_signal
	g_print (">> 'SELECTION_CHANGED' from choice_combo_changed_cb\n");
#endif
}
