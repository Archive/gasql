/* interface_cb.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "interface_cb.h"
#include "sqlwiddbtree.h"
#include "server-rs.h"
#include "sqldatawid.h"
#include "mainpagequery.h"
#include "relship.h"
#include <liboaf/liboaf.h>
#include <bonobo.h>
#include <gnome-db.h>

#include "passwd.xpm"

#define XML_GASQL_DTD_FILE DTDINSTALLDIR"/gasql.dtd"
/* DTD validation */
extern int xmlDoValidityCheckingDefaultValue;

/* Password dialog pixmap */
static GdkPixmap *passwd_icon = NULL;
static GdkBitmap *passwd_mask = NULL;

/********************************************************************
 *
 * User Interface actions CBs
 *
 ********************************************************************/



/* Cb to open the connexion to the SQL server */
void
sql_conn_open_cb (GtkWidget * widget, ConfManager * conf)
{
	if (*(conf->srv->gda_datasource->str) != '\0') {
		if (conf->config_dlg)
			gtk_object_destroy (GTK_OBJECT (conf->config_dlg));

		/* we check if we have a password or not; if not, ask for one */
		if (conf->user_name && (!conf->user_passwd || (*conf->user_passwd==0))) {
			GtkWidget *passdlg, *label, *entry, *pixmap, *table;
			gint button;
			gchar *str;

			passdlg = gnome_dialog_new (_("Password requested"), 
						    GNOME_STOCK_BUTTON_OK,  GNOME_STOCK_BUTTON_CANCEL, NULL);
			gnome_dialog_set_default (GNOME_DIALOG (passdlg), 0);

			table = gtk_table_new (2, 2, FALSE);
			gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (passdlg)->vbox), 
					    table, TRUE, TRUE, GNOME_PAD);
			gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD/2.);
			gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD/2.);

			if (!passwd_icon)
				passwd_icon =
					gdk_pixmap_create_from_xpm_d (GTK_WIDGET
								      (conf->app)->window, &passwd_mask,
								      NULL, passwd_xpm);
			pixmap = gtk_pixmap_new (passwd_icon, passwd_mask);
			gtk_table_attach (GTK_TABLE (table), pixmap, 0, 1, 0, 1,
					  GTK_SHRINK, GTK_SHRINK, GNOME_PAD, GNOME_PAD);
			
			str = g_strdup_printf(_("Enter password for user '%s' to open connection,\n"
						"or leave it empty if no password is required."),
					      conf->user_name);
			label = gtk_label_new (str);
			g_free (str);
			gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 2, 0, 1);

			label = gtk_label_new (_("Password:"));
			gtk_table_attach_defaults (GTK_TABLE (table), label, 0, 1, 1, 2);

			entry = gtk_entry_new ();
			gtk_entry_set_visibility (GTK_ENTRY (entry), FALSE);
			gtk_table_attach_defaults (GTK_TABLE (table), entry, 1, 2, 1, 2);

			gtk_widget_show_all (GNOME_DIALOG (passdlg)->vbox);
			gtk_widget_ref (entry);
			button = gnome_dialog_run_and_close (GNOME_DIALOG (passdlg));
			switch (button) {
			case -1: /* dialog closed */
			case 1:  /* CANCEL pressed */
				return;
				break;
			case 0:  /* OK */
				/* fetch the password */
				if (conf->user_passwd)
					g_free (conf->user_passwd);
				conf->user_passwd = g_strdup (gtk_entry_get_text (GTK_ENTRY (entry)));
				g_string_assign (conf->srv->password, conf->user_passwd);
				break;
			}
			gtk_widget_unref (entry);

		}
		server_access_open_connect (conf->srv);
	}
	else {
		options_config_cb (NULL, conf);
		/* sets the "Apply" button active because we consider a modif done */
		gnome_property_box_changed (GNOME_PROPERTY_BOX
					    (conf->config_dlg));
		conf->conn_open_requested = TRUE;
	}
}

/* Cb to close the connexion for the SQL server */
void
sql_conn_close_cb (GtkWidget * widget, ConfManager * conf)
{
	gint answer;
	GtkWidget *dialog, *label, *hb, *pixmap;

	/* ask if the user really wants to quit */
	if (!conf->save_up_to_date) {
		dialog = gnome_dialog_new (_("Disconnect confirmation"), 
					   _("Save and Disconnect"), _("Disconnect without saving"),
					   GNOME_STOCK_BUTTON_CANCEL, NULL);

		label = gtk_label_new (_("Some data have not yet been saved and should be saved before\n"
					 "closing the connection (or will be lost).\n\n"
					 "What do you want to do?"));

	}
	else {
		dialog = gnome_dialog_new (_("Disconnect confirmation"), 
					   _("Yes, disconnect"),
					   GNOME_STOCK_BUTTON_CANCEL, NULL);

		label = gtk_label_new (_("Do you really want to disconnect?"));
	}

	hb = gtk_hbox_new (FALSE, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), hb, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (hb);

	pixmap = gnome_stock_pixmap_widget_new (NULL, GNOME_STOCK_PIXMAP_CLOSE);
	gtk_box_pack_start (GTK_BOX (hb), pixmap, TRUE, TRUE, GNOME_PAD/2.);
	gtk_widget_show (pixmap);

	gtk_box_pack_start (GTK_BOX (hb), label, TRUE, TRUE, GNOME_PAD/2.);
	gtk_widget_show (label);
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	gnome_dialog_set_parent (GNOME_DIALOG (dialog), GTK_WINDOW (conf->app));
	answer = gnome_dialog_run_and_close (GNOME_DIALOG (dialog));

	if (answer == -1) /* dialog destroyed */
		return;
	if ((!conf->save_up_to_date && (answer == 2)) ||
	    (conf->save_up_to_date && (answer == 1)))
		return;

	/* at this point the user really wants to disconnect */

	if ((!conf->save_up_to_date) && (answer == 0)) { /* save */
		file_save_cb (NULL, conf);
		if (!conf->working_file)
			conf->close_after_saving = TRUE;
	}
	
	if (!conf->save_up_to_date) /* ALL data will be lost */
		conf->save_up_to_date = TRUE;

	/* here we delay the closing of the connection because the user need to choose
	   a filename to save data to */
	if (! conf->close_after_saving)
		server_access_close_connect (conf->srv);
}


/*
 * options_config_cb
 */

static void apply_cb (GnomePropertyBox * obj, gint page,
		      ConfManager * conf);
static void apply_sql_server_changes (GnomePropertyBox * props,
				      ConfManager * conf);
static gint open_conn_idle_func (ConfManager * conf);
static void notify_change_cb (GtkObject * obj, GtkWidget * props);
static void notify_destroy_config_dlg_cb (GtkWidget * wid,
					  ConfManager * conf);

void
options_config_cb (GtkWidget * widget, ConfManager * conf)
{
	GtkWidget *props, *label, *page;

	if (conf->config_dlg) {
		gdk_window_raise (conf->config_dlg->window);
		return;
	}

	props = gnome_property_box_new ();
	conf->config_dlg = props;

	/* SQL servers config */
	label = gtk_label_new (_("Server configuration"));
	gtk_widget_show (label);
	page = gnome_db_login_new (conf->datasource);
	gtk_object_set_data (GTK_OBJECT (props), "login", page);

	/* Integrate what is already in the variables (gda_dbname,...) */
	if (*conf->srv->gda_datasource->str != '\0')
		gtk_entry_set_text (GTK_ENTRY
				    (GTK_COMBO
				     (GNOME_DB_LOGIN (page)->gda_dbname)->
				     entry), conf->srv->gda_datasource->str);
	gtk_entry_set_text (GTK_ENTRY (GNOME_DB_LOGIN (page)->username_entry),
			    conf->srv->user_name->str);
	gtk_entry_set_text (GTK_ENTRY (GNOME_DB_LOGIN (page)->password_entry),
			    conf->srv->password->str);

	/* changes notification */
	gtk_signal_connect (GTK_OBJECT
			    (GTK_COMBO (GNOME_DB_LOGIN (page)->gda_dbname)->
			     entry), "changed",
			    GTK_SIGNAL_FUNC (notify_change_cb), props);
	gtk_signal_connect (GTK_OBJECT
			    (GNOME_DB_LOGIN (page)->username_entry),
			    "changed", GTK_SIGNAL_FUNC (notify_change_cb),
			    props);
	gtk_signal_connect (GTK_OBJECT
			    (GNOME_DB_LOGIN (page)->password_entry),
			    "changed", GTK_SIGNAL_FUNC (notify_change_cb),
			    props);

	gnome_property_box_append_page (GNOME_PROPERTY_BOX (props), page,
					label);
	gtk_container_set_border_width (GTK_CONTAINER (page), GNOME_PAD);
	gtk_widget_show (page);

	/* signals */
	gtk_window_set_title (GTK_WINDOW (props),
			      _("Connection preferences"));
	gtk_signal_connect (GTK_OBJECT (props), "apply",
			    GTK_SIGNAL_FUNC (apply_cb), conf);
	gtk_signal_connect (GTK_OBJECT (props), "destroy",
			    GTK_SIGNAL_FUNC (notify_destroy_config_dlg_cb),
			    conf);
	gtk_widget_show (props);

	/* consider that there is necessaryly a change to take into account */
	if (!conf->srv->gda_datasource ||
	    (*conf->srv->gda_datasource->str == 0))
		gnome_property_box_changed (GNOME_PROPERTY_BOX
					    (conf->config_dlg));
}

static void
apply_cb (GnomePropertyBox * obj, gint page, ConfManager * conf)
{
	switch (page) {
	case 0:
		apply_sql_server_changes (obj, conf);
		break;
	default:
		/* nothing to be done */
	}
}

static void
apply_sql_server_changes (GnomePropertyBox * props, ConfManager * conf)
{
	gchar *dbclear, *user, *password;
	GnomeDbLogin *login;

	login = GNOME_DB_LOGIN (GTK_WIDGET
				(gtk_object_get_data
				 (GTK_OBJECT (props), "login")));
	dbclear =
		gtk_entry_get_text (GTK_ENTRY
				    (GTK_COMBO (login->gda_dbname)->entry));
	user = gtk_entry_get_text (GTK_ENTRY (login->username_entry));
	password = gtk_entry_get_text (GTK_ENTRY (login->password_entry));
	conf->save_up_to_date = FALSE;


	/* fetching DSN and provider */
	g_string_assign (conf->srv->gda_datasource, dbclear);
	g_string_assign (conf->srv->user_name, user);
	g_string_assign (conf->srv->password, password);

	if (conf->user_name) 
		g_free (conf->user_name);
	conf->user_name = g_strdup (user);
	if (conf->user_passwd) 
		g_free (conf->user_passwd);
	conf->user_passwd = g_strdup (password);

	/* if the connection was to be opened, we open it now */
	if (conf->conn_open_requested)
		gtk_idle_add ((GtkFunction) open_conn_idle_func, conf);
}

/* used as a gtk_idle_func! */
static gint
open_conn_idle_func (ConfManager * conf)
{
	sql_conn_open_cb (NULL, conf);
	return FALSE;
}



static void
notify_change_cb (GtkObject * obj, GtkWidget * props)
{
	gnome_property_box_changed (GNOME_PROPERTY_BOX (props));
}

static void
notify_destroy_config_dlg_cb (GtkWidget * wid, ConfManager * conf)
{
	conf->config_dlg = NULL;
	conf->conn_open_requested = FALSE;
}


/*
 * sql_show_relations_cb
 */

static void show_relations_dialog_clicked_cb (GnomeDialog *dlg, gint button_number,
					      ConfManager *conf);
static gint show_relations_dialog_closed_cb (GnomeDialog *dlg, ConfManager *conf);
static void show_relations_dialog_conn_close_cb (ServerAccess *srv, ConfManager *conf);

void
sql_show_relations_cb (GtkObject * obj, ConfManager * conf)
{
	/* to hide the DLG */
	if (!conf) 
		return;

	if (!conf->relations_dialog) {
		GtkWidget *dlg, *rels;
		RelShip *rs;

		dlg = gnome_dialog_new (_("Database Relations"),
                                        GNOME_STOCK_BUTTON_CLOSE, NULL);
		gtk_window_set_policy (GTK_WINDOW (dlg), TRUE, TRUE, FALSE);
		gtk_signal_connect (GTK_OBJECT (dlg), "clicked",
                                    GTK_SIGNAL_FUNC (show_relations_dialog_clicked_cb), conf);
		gtk_signal_connect (GTK_OBJECT (dlg), "close",
                                    GTK_SIGNAL_FUNC (show_relations_dialog_closed_cb), conf);
		gtk_signal_connect_while_alive (GTK_OBJECT (conf->srv), "conn_to_close",
						GTK_SIGNAL_FUNC (show_relations_dialog_conn_close_cb), conf,
						GTK_OBJECT (dlg));
		gnome_dialog_close_hides (GNOME_DIALOG (dlg), FALSE);

		/* dialog contents */
		rs = RELSHIP (relship_find (QUERY (conf->top_query)));
		rels = relship_get_sw_view (rs);
		gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), rels, TRUE, TRUE, 0);

		gtk_widget_set_usize (dlg, 825, 570);
		gtk_widget_show_all (dlg);

		conf->relations_dialog = dlg;
	}
	else
		gdk_window_raise (conf->relations_dialog->window);
}


static gint 
show_relations_dialog_closed_cb (GnomeDialog *dlg, ConfManager *conf)
{
	conf->relations_dialog = NULL;
	return FALSE;
}

static void 
show_relations_dialog_clicked_cb (GnomeDialog *dlg, gint button_number, ConfManager *conf)
{
	gnome_dialog_close (dlg);
	conf->relations_dialog = NULL;
}


static void 
show_relations_dialog_conn_close_cb (ServerAccess *srv, ConfManager *conf)
{
	gnome_dialog_close (GNOME_DIALOG (conf->relations_dialog));
	conf->relations_dialog = NULL;
}








/*
 * sql_data_view_cb
 */

void
sql_data_view_cb (GtkWidget * widget, ConfManager * conf)
{
	GtkWidget *dlg;

	dlg = sql_data_wid_new_dlg (conf->srv);
	gtk_widget_show (dlg);
}


/*
 * sql_mem_update_cb
 */

static gboolean clean_empty_shell_queries (Query *q);

void
sql_mem_update_cb (GtkWidget * widget, ConfManager * conf)
{
	/* update of the data types known by the provider */
	server_access_refresh_datas (conf->srv);

	/* let's refresh the memory's representation */
	database_refresh (conf->db, conf->srv);

	/* clean the Queries which are just empty shells */
	clean_empty_shell_queries (QUERY (conf->top_query));
}

/* This function destroys any Query which is only an empty shell (no QueryFields and
   no sub queries), it returns TRUE if the query q was cleaned (removed) */
static gboolean
clean_empty_shell_queries (Query *q)
{
	GSList *list;

	g_assert (q); 

	/* clean the sub queries first */
	list = q->sub_queries;
	while (list) {
		if (clean_empty_shell_queries (QUERY (list->data)))
			list = q->sub_queries;
		else
			list = g_slist_next (list);
	}

	/* do we remove that query ? */
	if (!q->fields && !q->sub_queries && q->parent) {
		query_del_sub_query (q->parent, q);
		return TRUE;
	}
	else
		return FALSE;
}

/*
 * rescan_display_plugins_cb
 */

static void refresh_plugins_table_cb (GtkObject * obj,
				      ConfManager * conf);
static gboolean is_plugin_used (ConfManager * conf,
				SqlDataDisplayFns * fns);
struct foreach_hash_struct
{
	gpointer find_value;
	gboolean found;
};
static void foreach_hash_cb (gpointer key, gpointer value,
			     struct foreach_hash_struct *user_data);


void
rescan_display_plugins_cb (GtkObject * obj, ConfManager * conf)
{
	server_access_rescan_display_plugins (conf->srv, conf->plugins_dir);
	if (conf->config_plugins_dlg)
		refresh_plugins_table_cb (NULL, conf);
}

static void
refresh_plugins_table_cb (GtkObject * obj, ConfManager * conf)
{
	GSList *list;
	gchar *text[5];
	SqlDataDisplayFns *fns;
	GtkCList *clist;
	gint row;

	clist = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				     "clist");
	list = conf->srv->data_types_display;
	gtk_clist_freeze (clist);
	while (list) {
		fns = (SqlDataDisplayFns *) (list->data);
		if (fns->plugin_name) {	/* making sure it is a plugin */
			row = gtk_clist_find_row_from_data (clist,
							    list->data);
			text[0] = fns->plugin_name;
			text[1] = fns->version;

			if (is_plugin_used (conf, fns))
				text[2] = _("Yes");
			else
				text[2] = _("No");
			text[3] = fns->descr;
			text[4] = fns->plugin_file;
			if (row >= 0) {
				gtk_clist_remove (clist, row);
				gtk_clist_insert (clist, row, text);
			}
			else
				row = gtk_clist_append (clist, text);
			gtk_clist_set_row_data (clist, row, fns);
		}
		list = g_slist_next (list);
	}

	/* removing any row not related to a plugin anymore */
	row = 0;
	while (row < clist->rows) {
		fns = gtk_clist_get_row_data (clist, row);
		if (!g_slist_find (conf->srv->data_types_display, fns))
			gtk_clist_remove (clist, row);
		else
			row++;
	}

	gtk_clist_thaw (clist);
}

static gboolean
is_plugin_used (ConfManager * conf, SqlDataDisplayFns * fns)
{
	struct foreach_hash_struct *fhs;
	fhs = g_new (struct foreach_hash_struct, 1);
	fhs->find_value = fns;
	fhs->found = FALSE;

	g_hash_table_foreach (conf->srv->types_objects_hash,
			      (GHFunc) (foreach_hash_cb), fhs);

	return fhs->found;
}

static void
foreach_hash_cb (gpointer key, gpointer value,
		 struct foreach_hash_struct *user_data)
{
	if (value == user_data->find_value)
		user_data->found = TRUE;
}


/*
 * config_display_plugins_cb
 */

static void change_location_cb (GtkObject * obj, ConfManager * conf);
static void change_location_cancel_cb (GtkObject * obj,
				       ConfManager * conf);
static void change_location_ok_cb (GtkObject * obj, ConfManager * conf);
static void data_types_clist_select_cb (GtkWidget * widget, gint row,
					gint col, GdkEventButton * event,
					ConfManager * conf);
static void plugins_ch_combo_sel_changed_cb (GtkObject * obj,
					     gpointer * newsel,
					     ConfManager * conf);
static void data_types_clist_unselect_cb (GtkWidget * widget, gint row,
					  gint col, GdkEventButton * event,
					  ConfManager * conf);
static void plugin_list_update_description (GtkWidget * Bdescr,
					    GtkWidget * Ddescr,
					    SqlDataDisplayFns * fns);
static void plug_menu_table_created_dropped_cb (GtkObject * obj,
						DbTable * new_table,
						ConfManager * conf);
static void table_field_clist_select_cb (GtkWidget * clist, gint row,
					 gint col, GdkEventButton * event,
					 ConfManager * conf);
/* build the contents of the tables clist contents */
static void plugins_ch_combo_sel_changed_cb2 (GtkObject * object,
					      gpointer * newsel,
					      ConfManager * conf);
static void table_field_clist_unselect_cb (GtkWidget * clist,
					   gint row, gint col,
					   GdkEventButton * event,
					   ConfManager * conf);
static void refresh_table_fields_menu_cb (Database * db, DbTable * table,
					  DbField * field,
					  ConfManager * conf);
static void notify_destroy_config_plugins_dlg_cb (GtkWidget * wid,
						  ConfManager * conf);
/* refreshes the clist with the data types */
static void data_types_updated_cb (GtkObject * obj, ConfManager * conf);
static void build_tables_contents_menu_cb (GtkObject * mitem,
					   ConfManager * conf);

void
config_display_plugins_cb (GtkObject * obj, ConfManager * conf)
{
	GtkWidget *props, *label, *page, *wid, *sw, *box, *table, *box2,
		*clist;
	GtkWidget *frame, *tmpwid, *menu;
	GSList *list;

	if (conf->config_plugins_dlg) {
		gdk_window_raise (conf->config_plugins_dlg->window);
		return;
	}

	props = gnome_property_box_new ();
	conf->config_plugins_dlg = props;

	/* 
	 * known plugins and their usage: 1st page
	 */
	label = gtk_label_new (_("Plugins list"));
	gtk_widget_show (label);
	page = gtk_hbox_new (FALSE, GNOME_PAD);
	gtk_object_set_data (GTK_OBJECT (props), "pluglist", page);

	/* on the left is a clist in its sw and the location of plugins */
	box = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (page), box, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (box);

	box2 = gtk_hbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (box), box2, FALSE, TRUE, GNOME_PAD);
	gtk_widget_show (box2);
	wid = gtk_label_new (_("Plugins location:"));
	gtk_box_pack_start (GTK_BOX (box2), wid, FALSE, TRUE, GNOME_PAD);
	gtk_widget_show (wid);
	wid = gtk_label_new (conf->plugins_dir);
	gtk_object_set_data (GTK_OBJECT (props), "loclabel", wid);
	gtk_box_pack_start (GTK_BOX (box2), wid, FALSE, TRUE, GNOME_PAD);
	gtk_widget_show (wid);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (box), sw, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (sw);
	clist = gtk_clist_new (5);
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "clist",
			     clist);
	gtk_clist_set_column_title (GTK_CLIST (clist), 0, _("Plugin"));
	gtk_clist_set_column_title (GTK_CLIST (clist), 1, _("Version"));
	gtk_clist_set_column_title (GTK_CLIST (clist), 2, _("Used"));
	gtk_clist_set_column_title (GTK_CLIST (clist), 3, _("Description"));
	gtk_clist_set_column_title (GTK_CLIST (clist), 4, _("File"));
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 0, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 1, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 2, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 3, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 4, TRUE);
	gtk_clist_set_selection_mode (GTK_CLIST (clist),
				      GTK_SELECTION_SINGLE);
	gtk_clist_column_titles_show (GTK_CLIST (clist));
	gtk_clist_column_titles_passive (GTK_CLIST (clist));
	gtk_container_add (GTK_CONTAINER (sw), clist);
	gtk_widget_show (clist);

	/* on the right are some buttons and the dir. location of the plugins */
	box = gtk_vbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (box),
				   GTK_BUTTONBOX_SPREAD);
	gtk_box_pack_start (GTK_BOX (page), box, FALSE, FALSE, GNOME_PAD);
	gtk_widget_show (box);
	wid = gtk_button_new_with_label (_("Change location"));
	gtk_signal_connect (GTK_OBJECT (wid), "clicked",
			    GTK_SIGNAL_FUNC (change_location_cb), conf);
	gtk_container_add (GTK_CONTAINER (box), wid);
	gtk_widget_show (wid);
	wid = gtk_button_new_with_label (_("Rescan plugins"));
	gtk_signal_connect (GTK_OBJECT (wid), "clicked",
			    GTK_SIGNAL_FUNC (rescan_display_plugins_cb),
			    conf);
	gtk_container_add (GTK_CONTAINER (box), wid);
	gtk_widget_show (wid);
	wid = gtk_button_new_with_label (_("Refresh"));
	gtk_container_add (GTK_CONTAINER (box), wid);
	gtk_widget_show (wid);
	gtk_signal_connect (GTK_OBJECT (wid), "clicked",
			    GTK_SIGNAL_FUNC (refresh_plugins_table_cb), conf);

	gnome_property_box_append_page (GNOME_PROPERTY_BOX (props), page,
					label);
	gtk_container_set_border_width (GTK_CONTAINER (page), GNOME_PAD);
	refresh_plugins_table_cb (NULL, conf);
	gtk_widget_show (page);

	/* 
	 * Data types bindings: 2nd page
	 */
	label = gtk_label_new (_("Data Type bindings"));
	gtk_widget_show (label);
	page = gtk_hbox_new (FALSE, GNOME_PAD);
	gtk_object_set_data (GTK_OBJECT (props), "dtbinding", page);

	/* on the left side is a clist in a sw */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (page), sw, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (sw);
	wid = gtk_clist_new (1);
	gtk_clist_set_column_title (GTK_CLIST (wid), 0, _("Data types"));
	gtk_clist_set_selection_mode (GTK_CLIST (wid), GTK_SELECTION_SINGLE);
	gtk_clist_column_titles_show (GTK_CLIST (wid));
	gtk_clist_column_titles_passive (GTK_CLIST (wid));
	gtk_container_add (GTK_CONTAINER (sw), wid);
	gtk_widget_show (wid);
	list = conf->srv->data_types;
	while (list) {
		gint row;
		row = gtk_clist_append (GTK_CLIST (wid),
					&(SERVER_DATA_TYPE (list->data)->
					  sqlname));
		gtk_clist_set_row_data (GTK_CLIST (wid), row, list->data);
		list = g_slist_next (list);
	}
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "tclist",
			     wid);
	gtk_signal_connect (GTK_OBJECT (wid), "select_row",
			    GTK_SIGNAL_FUNC (data_types_clist_select_cb),
			    conf);
	gtk_signal_connect (GTK_OBJECT (wid), "unselect_row",
			    GTK_SIGNAL_FUNC (data_types_clist_unselect_cb),
			    conf);
	gtk_signal_connect_while_alive (GTK_OBJECT (conf->srv),
					"data_types_updated",
					GTK_SIGNAL_FUNC
					(data_types_updated_cb), conf,
					GTK_OBJECT (wid));
	gtk_signal_connect_while_alive (GTK_OBJECT (conf->srv),
					"objects_bindings_updated",
					GTK_SIGNAL_FUNC
					(refresh_plugins_table_cb), conf,
					GTK_OBJECT (wid));

	/* on the right side is the action area */
	box = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (page), box, TRUE, TRUE, GNOME_PAD);

	table = gtk_table_new (3, 2, FALSE);
	gtk_table_set_col_spacing (GTK_TABLE (table), 0, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (box), table, FALSE, FALSE, 0);
	wid = gtk_label_new (_("Data Type:"));
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 0, 1);
	gtk_widget_show (wid);
	wid = gtk_label_new (_("<Select one>"));
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg),
			     "seltname", wid);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 0, 1);
	gtk_widget_show (wid);
	wid = gtk_label_new (_("Description:"));
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 1, 2);
	gtk_widget_show (wid);
	wid = gtk_label_new (_("<NONE>"));
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg),
			     "seltdescr", wid);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 1, 2);
	gtk_widget_show (wid);
	wid = gtk_label_new (_("Display Plugin:"));
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 2, 3);
	gtk_widget_show (wid);
	wid = choice_combo_new ();
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "selcc",
			     wid);
	choice_combo_set_content (CHOICE_COMBO (wid), NULL, 0);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 2, 3);
	gtk_widget_show (wid);
	gtk_widget_show (table);
	gtk_widget_show (box);

	/* below is a frame and label to put detailled description of the plugin */
	frame = gtk_frame_new (_("Plugin description"));
	gtk_box_pack_start (GTK_BOX (box), frame, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (frame);
	wid = gtk_vbox_new (FALSE, GNOME_PAD);
	gtk_container_add (GTK_CONTAINER (frame), wid);
	gtk_widget_show (wid);

	tmpwid = gtk_label_new ("");
	gtk_box_pack_start (GTK_BOX (wid), tmpwid, FALSE, FALSE, 0);
	gtk_widget_show (tmpwid);
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "Bdescr",
			     tmpwid);

	tmpwid = gtk_label_new ("");
	gtk_box_pack_start (GTK_BOX (wid), tmpwid, FALSE, FALSE, 0);
	gtk_widget_show (tmpwid);
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "Ddescr",
			     tmpwid);


	gnome_property_box_append_page (GNOME_PROPERTY_BOX (props), page,
					label);
	gtk_container_set_border_width (GTK_CONTAINER (page), GNOME_PAD);
	gtk_widget_show (page);

	/* 
	 * Fine tuned plugins usage: 3rd page
	 */
	label = gtk_label_new (_("Individual objects bindings"));
	gtk_widget_show (label);
	page = gtk_hbox_new (FALSE, GNOME_PAD);
	gtk_object_set_data (GTK_OBJECT (props), "objbinding", page);


	/* on the left side is a menu and a clist in a sw */
	box = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (page), box, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (box);

	/* menu */
	tmpwid = gtk_option_menu_new ();
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg),
			     "tablesomenu", tmpwid);
	gtk_box_pack_start (GTK_BOX (box), tmpwid, FALSE, TRUE, 0);
	gtk_widget_show (tmpwid);
	menu = gtk_menu_new ();
	list = conf->db->tables;
	while (list) {
		wid = gtk_menu_item_new_with_label (DB_TABLE
						    (list->data)->name);
		gtk_widget_show (wid);
		gtk_object_set_data (GTK_OBJECT (wid), "titem", list->data);
		gtk_signal_connect (GTK_OBJECT (wid), "activate",
				    GTK_SIGNAL_FUNC
				    (build_tables_contents_menu_cb), conf);
		gtk_menu_append (GTK_MENU (menu), wid);
		list = g_slist_next (list);
	}
	gtk_option_menu_set_menu (GTK_OPTION_MENU (tmpwid), menu);
	gtk_widget_show (menu);
	/* signals for coherent menu contents */
	gtk_signal_connect_while_alive (GTK_OBJECT (conf->db),
					"table_created_filled",
					GTK_SIGNAL_FUNC
					(plug_menu_table_created_dropped_cb),
					conf, GTK_OBJECT (tmpwid));
	gtk_signal_connect_while_alive (GTK_OBJECT (conf->db),
					"table_dropped",
					GTK_SIGNAL_FUNC
					(plug_menu_table_created_dropped_cb),
					conf, GTK_OBJECT (tmpwid));

	/* clist in a sw */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (box), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);
	wid = gtk_clist_new (1);
	gtk_clist_column_titles_hide (GTK_CLIST (wid));
	gtk_clist_set_selection_mode (GTK_CLIST (wid), GTK_SELECTION_SINGLE);
	gtk_container_add (GTK_CONTAINER (sw), wid);
	gtk_widget_show (wid);
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg),
			     "tablesclist", wid);
	gtk_signal_connect (GTK_OBJECT (wid), "select_row",
			    GTK_SIGNAL_FUNC (table_field_clist_select_cb),
			    conf);
	gtk_signal_connect (GTK_OBJECT (wid), "unselect_row",
			    GTK_SIGNAL_FUNC (table_field_clist_unselect_cb),
			    conf);
	gtk_signal_connect_while_alive (GTK_OBJECT (conf->db),
					"field_created",
					GTK_SIGNAL_FUNC
					(refresh_table_fields_menu_cb), conf,
					GTK_OBJECT (wid));
	gtk_signal_connect_while_alive (GTK_OBJECT (conf->db),
					"field_dropped",
					GTK_SIGNAL_FUNC
					(refresh_table_fields_menu_cb), conf,
					GTK_OBJECT (wid));

	/* on the right side is the action area */
	box = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (page), box, TRUE, TRUE, GNOME_PAD);

	table = gtk_table_new (3, 2, FALSE);
	gtk_table_set_col_spacing (GTK_TABLE (table), 0, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (box), table, FALSE, FALSE, 0);
	wid = gtk_label_new (_("Object:"));
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 0, 1);
	gtk_widget_show (wid);
	wid = gtk_label_new (_("<Select one>"));
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "seltab",
			     wid);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 0, 1);
	gtk_widget_show (wid);
	wid = gtk_label_new (_("Data type:"));
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 1, 2);
	gtk_widget_show (wid);
	wid = gtk_label_new ("-");
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "tabtype",
			     wid);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 1, 2);
	gtk_widget_show (wid);
	wid = gtk_label_new (_("Display Plugin:"));
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 2, 3);
	gtk_widget_show (wid);
	wid = choice_combo_new ();
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg),
			     "seltabplug", wid);
	choice_combo_set_content (CHOICE_COMBO (wid), NULL, 0);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 2, 3);
	gtk_widget_show (wid);
	gtk_widget_show (table);
	gtk_widget_show (box);

	/* below is a frame and label to put detailled description of the plugin */
	frame = gtk_frame_new (_("Plugin description"));
	gtk_box_pack_start (GTK_BOX (box), frame, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (frame);
	wid = gtk_vbox_new (FALSE, GNOME_PAD);
	gtk_container_add (GTK_CONTAINER (frame), wid);
	gtk_widget_show (wid);

	tmpwid = gtk_label_new ("");
	gtk_box_pack_start (GTK_BOX (wid), tmpwid, FALSE, FALSE, 0);
	gtk_widget_show (tmpwid);
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "Bdescr2",
			     tmpwid);

	tmpwid = gtk_label_new ("");
	gtk_box_pack_start (GTK_BOX (wid), tmpwid, FALSE, FALSE, 0);
	gtk_widget_show (tmpwid);
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "Ddescr2",
			     tmpwid);

	/* building the tables contents */
	if (gtk_menu_get_active (GTK_MENU (menu)))
		build_tables_contents_menu_cb
			(GTK_OBJECT (gtk_menu_get_active (GTK_MENU (menu))),
			 conf);

	gnome_property_box_append_page (GNOME_PROPERTY_BOX (props), page,
					label);
	gtk_container_set_border_width (GTK_CONTAINER (page), GNOME_PAD);
	gtk_widget_show (page);


	gtk_window_set_title (GTK_WINDOW (props), _("Plugins preferences"));
	gtk_widget_set_usize (props, 600, 300);
	gtk_widget_hide (GNOME_PROPERTY_BOX (props)->apply_button);
	gtk_signal_connect (GTK_OBJECT (props), "destroy",
			    GTK_SIGNAL_FUNC
			    (notify_destroy_config_plugins_dlg_cb), conf);
	gtk_widget_show (props);
}

static void
change_location_cb (GtkObject * obj, ConfManager * conf)
{
	GtkWidget *fw;

	fw = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				  "fw");
	if (!fw) {
		fw = gtk_file_selection_new ("Select plugins directory");
		gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg),
				     "fw", fw);
		gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION
							(fw));
		gtk_signal_connect (GTK_OBJECT
				    (GTK_FILE_SELECTION (fw)->cancel_button),
				    "clicked",
				    GTK_SIGNAL_FUNC
				    (change_location_cancel_cb), conf);
		gtk_signal_connect (GTK_OBJECT
				    (GTK_FILE_SELECTION (fw)->ok_button),
				    "clicked",
				    GTK_SIGNAL_FUNC (change_location_ok_cb),
				    conf);
		gtk_widget_show (fw);
	}
	else
		gdk_window_raise (fw->window);
}

static void
change_location_cancel_cb (GtkObject * obj, ConfManager * conf)
{
	GtkWidget *fw;
	fw = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				  "fw");
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "fw",
			     NULL);
	gtk_widget_destroy (fw);
}

static void
change_location_ok_cb (GtkObject * obj, ConfManager * conf)
{
	gchar *str, *ptr;
	GtkWidget *fw;
	GtkWidget *wid;

	fw = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				  "fw");
	str = g_strdup (gtk_file_selection_get_filename
			(GTK_FILE_SELECTION (fw)));
	/* removing trailing caracters in case we have a file instead of a dir */
	ptr = str + strlen (str) - 1;
	while (*ptr != '/') {
		*ptr = '\0';
		ptr = str + strlen (str) - 1;
	}
	*ptr = '\0';
	if (conf->plugins_dir)
		g_free (conf->plugins_dir);
	conf->plugins_dir = g_strdup (str);
	g_free (str);
	/* refreshing the display */
	wid = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "loclabel");
	gtk_label_set_text (GTK_LABEL (wid), conf->plugins_dir);
	rescan_display_plugins_cb (NULL, conf);

	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "fw",
			     NULL);
	gtk_widget_destroy (fw);
}


static void
data_types_clist_select_cb (GtkWidget * widget, gint row, gint col,
			    GdkEventButton * event, ConfManager * conf)
{
	GtkWidget *wid, *clist;
	ServerDataType *dt;
	GSList *blist, *list;
	SqlDataDisplayFns *fns;
	gint i;

	clist = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				     "tclist");
	dt = gtk_clist_get_row_data (GTK_CLIST (clist), row);
	if (dt) {
		gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg),
				     "dt", dt);
		wid = gtk_object_get_data (GTK_OBJECT
					   (conf->config_plugins_dlg),
					   "seltname");
		gtk_label_set_text (GTK_LABEL (wid), dt->sqlname);
		wid = gtk_object_get_data (GTK_OBJECT
					   (conf->config_plugins_dlg),
					   "seltdescr");
		gtk_label_set_text (GTK_LABEL (wid), dt->descr);
		/* computing the list for choice combo from the gda types accepted
		   by each plugin */
		wid = gtk_object_get_data (GTK_OBJECT
					   (conf->config_plugins_dlg),
					   "selcc");
		blist = g_slist_append (NULL, NULL);
		list = conf->srv->data_types_display;
		while (list) {
			fns = (SqlDataDisplayFns *) (list->data);
			if (fns->plugin_name) {
				gboolean found;
				found = FALSE;
				i = 0;
				if (fns->nb_gda_type == 0)	/* any Gda Type is OK */
					found = TRUE;
				while ((i < fns->nb_gda_type) && !found) {
					if (fns->valid_gda_types[i] ==
					    dt->gda_type)
						found = TRUE;
					i++;
				}
				if (found)
					blist = g_slist_append (blist, fns);
			}
			list = g_slist_next (list);
		}
		choice_combo_set_content (CHOICE_COMBO (wid), blist,
					  GTK_STRUCT_OFFSET
					  (SqlDataDisplayFns, plugin_name));
		/* if a plugin is already bound, set it as default */
		fns = g_hash_table_lookup (conf->srv->types_objects_hash, dt);
		if (fns) {
			i = g_slist_index (blist, fns);
			if (i >= 0) {
				GtkWidget *w1, *w2;
				choice_combo_set_selection_num (CHOICE_COMBO
								(wid), i);
				/* updating the plugin description */
				w1 = gtk_object_get_data (GTK_OBJECT
							  (conf->
							   config_plugins_dlg),
							  "Bdescr");
				w2 = gtk_object_get_data (GTK_OBJECT
							  (conf->
							   config_plugins_dlg),
							  "Ddescr");
				plugin_list_update_description (w1, w2, fns);
			}
		}
		g_slist_free (blist);
		gtk_signal_connect (GTK_OBJECT (wid), "selection_changed",
				    GTK_SIGNAL_FUNC
				    (plugins_ch_combo_sel_changed_cb), conf);
	}
	else
		gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg),
				     "dt", NULL);
}

static void
plugins_ch_combo_sel_changed_cb (GtkObject * obj, gpointer * newsel,
				 ConfManager * conf)
{
	gpointer ptr;
	ServerDataType *dt;
	GtkWidget *w1, *w2;

	dt = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				  "dt");
	ptr = g_hash_table_lookup (conf->srv->types_objects_hash, dt);
	if (ptr)
		server_access_unbind_object_display (conf->srv, GTK_OBJECT (dt));

	if (newsel) {
		server_access_bind_object_display (conf->srv, GTK_OBJECT (dt),
						(SqlDataDisplayFns *) newsel);
		/* tell that we have modified the working environment */
		conf->save_up_to_date = FALSE;
	}
	refresh_plugins_table_cb (NULL, conf);

	w1 = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				  "Bdescr");
	w2 = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				  "Ddescr");
	plugin_list_update_description (w1, w2, (SqlDataDisplayFns *) newsel);
}

static void
data_types_clist_unselect_cb (GtkWidget * widget, gint row, gint col,
			      GdkEventButton * event,
			      ConfManager * conf)
{
	GtkWidget *wid;

	wid = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "seltname");
	gtk_label_set_text (GTK_LABEL (wid), _("<Select one>"));
	wid = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "seltdescr");
	gtk_label_set_text (GTK_LABEL (wid), _("<NONE>"));

	/* removing any comment in the plugin detail frame */
	wid = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "Bdescr");
	gtk_label_set_text (GTK_LABEL (wid), "");
	wid = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "Ddescr");
	gtk_label_set_text (GTK_LABEL (wid), "");

	/* choice combo */
	wid = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "selcc");
	gtk_signal_disconnect_by_func (GTK_OBJECT (wid),
				       GTK_SIGNAL_FUNC
				       (plugins_ch_combo_sel_changed_cb),
				       conf);
	choice_combo_set_content (CHOICE_COMBO (wid), NULL, 0);
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "dt",
			     NULL);
}


static void
plugin_list_update_description (GtkWidget * Bdescr,
				GtkWidget * Ddescr, SqlDataDisplayFns * fns)
{
	GtkWidget *wid;

	wid = Bdescr;
	if (fns)
		gtk_label_set_text (GTK_LABEL (wid), fns->descr);
	else
		gtk_label_set_text (GTK_LABEL (wid), "");
	wid = Ddescr;
	if (fns) {
		if (fns->detailled_descr)
			gtk_label_set_text (GTK_LABEL (wid),
					    fns->detailled_descr);
		else
			gtk_label_set_text (GTK_LABEL (wid), "");
	}
	else
		gtk_label_set_text (GTK_LABEL (wid), "");
}


/* having an updated version of the list of tables/vues in the menu */
static void
plug_menu_table_created_dropped_cb (GtkObject * obj,
				    DbTable * new_table,
				    ConfManager * conf)
{
	GtkWidget *wid, *mitem, *menu, *optionmenu;
	gpointer titem;
	GSList *list;

	optionmenu =
		gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				     "tablesomenu");
	/* what is the selection now? */
	mitem = gtk_menu_get_active
		(GTK_MENU (gtk_option_menu_get_menu
			   (GTK_OPTION_MENU (optionmenu))));
	if (mitem)
		titem = gtk_object_get_data (GTK_OBJECT (mitem), "titem");
	else
		titem = NULL;

	/* clear the displayed menu */
	gtk_option_menu_remove_menu (GTK_OPTION_MENU (optionmenu));

	/* new menu now */
	menu = gtk_menu_new ();
	mitem = NULL;
	list = conf->db->tables;
	while (list) {
		wid = gtk_menu_item_new_with_label (DB_TABLE
						    (list->data)->name);
		gtk_object_set_data (GTK_OBJECT (wid), "titem", list->data);
		gtk_signal_connect (GTK_OBJECT (wid), "activate",
				    GTK_SIGNAL_FUNC
				    (build_tables_contents_menu_cb), conf);
		gtk_menu_append (GTK_MENU (menu), wid);
		gtk_widget_show (wid);
		if (titem == list->data)
			mitem = wid;
		list = g_slist_next (list);
	}
	gtk_option_menu_set_menu (GTK_OPTION_MENU (optionmenu), menu);
	gtk_widget_show (menu);

	/* set the selection as it was */
	if (mitem)
		gtk_menu_item_activate (GTK_MENU_ITEM (mitem));
	else {
		gpointer argobj;
		argobj = gtk_menu_get_active (GTK_MENU (menu));
		if (argobj)
			build_tables_contents_menu_cb (argobj, conf);
		else
			build_tables_contents_menu_cb (NULL, conf);
	}
}

static void
table_field_clist_select_cb (GtkWidget * clist, gint row, gint col,
			     GdkEventButton * event, ConfManager * conf)
{
	GtkWidget *wid;
	ServerDataType *dt;
	GSList *blist, *list;
	SqlDataDisplayFns *fns;
	gint i;
	DbField *field;
	DbTable *table;
	gchar *str;

	/* set the new selection */
	gtk_object_set_data (GTK_OBJECT (clist), "selrow",
			     GINT_TO_POINTER (row + 1));

	field = gtk_clist_get_row_data (GTK_CLIST (clist), row);
	table = database_find_table_from_field (conf->db, field);
	dt = field->type;
	if (dt) {
		gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg),
				     "obj", field);
		wid = gtk_object_get_data (GTK_OBJECT
					   (conf->config_plugins_dlg),
					   "seltab");
		str = g_strdup_printf ("%s.%s", table->name, field->name);
		gtk_label_set_text (GTK_LABEL (wid), str);
		g_free (str);
		wid = gtk_object_get_data (GTK_OBJECT
					   (conf->config_plugins_dlg),
					   "tabtype");
		gtk_label_set_text (GTK_LABEL (wid), dt->sqlname);
		/* computing the list for choice combo from tge gda types accepted
		   by each plugin */
		wid = gtk_object_get_data (GTK_OBJECT
					   (conf->config_plugins_dlg),
					   "seltabplug");
		blist = g_slist_append (NULL, NULL);
		list = conf->srv->data_types_display;
		while (list) {
			fns = (SqlDataDisplayFns *) (list->data);
			if (fns->plugin_name) {
				gboolean found;
				found = FALSE;
				i = 0;
				if (fns->nb_gda_type == 0)	/* any Gda Type is OK */
					found = TRUE;
				while ((i < fns->nb_gda_type) && !found) {
					if (fns->valid_gda_types[i] ==
					    dt->gda_type)
						found = TRUE;
					i++;
				}
				if (found)
					blist = g_slist_append (blist, fns);
			}
			list = g_slist_next (list);
		}
		choice_combo_set_content (CHOICE_COMBO (wid), blist,
					  GTK_STRUCT_OFFSET
					  (SqlDataDisplayFns, plugin_name));
		/* if a plugin is already bound, set it as default */
		fns = g_hash_table_lookup (conf->srv->types_objects_hash,
					   field);
		if (fns) {
			i = g_slist_index (blist, fns);
			if (i >= 0) {
				GtkWidget *w1, *w2;
				choice_combo_set_selection_num (CHOICE_COMBO
								(wid), i);
				/* updating the plugin description */
				w1 = gtk_object_get_data (GTK_OBJECT
							  (conf->
							   config_plugins_dlg),
							  "Bdescr2");
				w2 = gtk_object_get_data (GTK_OBJECT
							  (conf->
							   config_plugins_dlg),
							  "Ddescr2");
				plugin_list_update_description (w1, w2, fns);
			}
		}
		g_slist_free (blist);
		gtk_signal_connect (GTK_OBJECT (wid), "selection_changed",
				    GTK_SIGNAL_FUNC
				    (plugins_ch_combo_sel_changed_cb2), conf);
	}
	else
		gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg),
				     "obj", NULL);
}

/* build the contents of the tables clist contents */
static void
plugins_ch_combo_sel_changed_cb2 (GtkObject * object,
				  gpointer * newsel, ConfManager * conf)
{
	gpointer ptr;
	gpointer obj;
	GtkWidget *w1, *w2;

	obj = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "obj");
	ptr = g_hash_table_lookup (conf->srv->types_objects_hash, obj);
	if (ptr) {
		server_access_unbind_object_display (conf->srv,
						  GTK_OBJECT (obj));
		g_print ("Unbinded object %p\n", obj);
	}

	if (newsel) {
		server_access_bind_object_display (conf->srv, GTK_OBJECT (obj),
						(SqlDataDisplayFns *) newsel);
		g_print ("Binded object %p to %s\n", obj,
			 ((SqlDataDisplayFns *) newsel)->descr);
		/* tell that we have modified the working environment */
		conf->save_up_to_date = FALSE;
	}
	refresh_plugins_table_cb (NULL, conf);

	w1 = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				  "Bdescr2");
	w2 = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				  "Ddescr2");
	plugin_list_update_description (w1, w2, (SqlDataDisplayFns *) newsel);
}


static void
table_field_clist_unselect_cb (GtkWidget * clist, gint row, gint col,
			       GdkEventButton * event,
			       ConfManager * conf)
{
	GtkWidget *wid;

	/* set the new selection */
	gtk_object_set_data (GTK_OBJECT (clist), "selrow",
			     GINT_TO_POINTER (0));

	wid = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "seltab");
	gtk_label_set_text (GTK_LABEL (wid), _("<Select one>"));
	wid = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "tabtype");
	gtk_label_set_text (GTK_LABEL (wid), "-");

	/* removing any comment in the plugin detail frame */
	wid = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "Bdescr2");
	gtk_label_set_text (GTK_LABEL (wid), "");
	wid = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "Ddescr2");
	gtk_label_set_text (GTK_LABEL (wid), "");

	/* choice combo */
	wid = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				   "seltabplug");
	gtk_signal_disconnect_by_func (GTK_OBJECT (wid),
				       GTK_SIGNAL_FUNC
				       (plugins_ch_combo_sel_changed_cb2),
				       conf);
	choice_combo_set_content (CHOICE_COMBO (wid), NULL, 0);
	gtk_object_set_data (GTK_OBJECT (conf->config_plugins_dlg), "obj",
			     NULL);
}

/* called to refresh the contents of the clist when a field is added or 
   removed in a table */
static void
refresh_table_fields_menu_cb (Database * db, DbTable * table,
			      DbField * field, ConfManager * conf)
{
	GtkWidget *clist;
	gint selrow, row;
	DbField *sel_field = NULL;

	clist = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				     "tablesclist");
	/* undo the selection */
	selrow = GPOINTER_TO_INT (gtk_object_get_data
				  (GTK_OBJECT (clist), "selrow"));
	if (selrow) {
		sel_field =
			gtk_clist_get_row_data (GTK_CLIST (clist),
						selrow - 1);
		gtk_clist_unselect_row (GTK_CLIST (clist), selrow - 1, 0);
	}

	/* remove or add the field */
	row = gtk_clist_find_row_from_data (GTK_CLIST (clist), field);
	if (row >= 0)		/* field to be removed */
		gtk_clist_remove (GTK_CLIST (clist), row);
	else {			/* field to be added */

		gchar *txt[1];
		txt[0] = field->name;
		gtk_clist_insert (GTK_CLIST (clist),
				  g_slist_index (table->fields, field), txt);
	}

	if (sel_field && (sel_field != field)) {
		selrow = gtk_clist_find_row_from_data (GTK_CLIST (clist),
						       sel_field);
		gtk_clist_select_row (GTK_CLIST (clist), selrow, 0);
	}
}

static void
notify_destroy_config_plugins_dlg_cb (GtkWidget * wid,
				      ConfManager * conf)
{
	conf->config_plugins_dlg = NULL;
}

static void
data_types_updated_cb (GtkObject * obj, ConfManager * conf)
{
	gint row;
	GtkCList *clist;
	gpointer data;
	GSList *list;

	clist = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				     "tclist");

	/* add the ones not yet there */
	list = conf->srv->data_types;
	while (list) {
		row = gtk_clist_find_row_from_data (clist, list->data);
		if (row < 0) {	/* need to be added */
			row = gtk_clist_append (clist,
						&(SERVER_DATA_TYPE (list->data)->
						  sqlname));
			gtk_clist_set_row_data (clist, row, list->data);
		}
		list = g_slist_next (list);
	}

	/* remove the ones not present anymore */
	row = 0;
	while (row < clist->rows) {
		data = gtk_clist_get_row_data (clist, row);
		if (!g_slist_find (conf->srv->data_types, data))
			gtk_clist_remove (clist, row);
		else
			row++;
	}
}

static void
build_tables_contents_menu_cb (GtkObject * mitem, ConfManager * conf)
{
	DbTable *titem;
	GtkWidget *clist;
	gint selrow;

	clist = gtk_object_get_data (GTK_OBJECT (conf->config_plugins_dlg),
				     "tablesclist");
	/* if there was a selection, unselect it. */
	selrow = GPOINTER_TO_INT (gtk_object_get_data
				  (GTK_OBJECT (clist), "selrow"));
	if (selrow)
		gtk_clist_unselect_row (GTK_CLIST (clist), selrow - 1, 0);

	gtk_clist_freeze (GTK_CLIST (clist));
	gtk_clist_clear (GTK_CLIST (clist));
	if (mitem) {
		GSList *list;
		gchar *content[1];
		gint i;

		titem = gtk_object_get_data (mitem, "titem");
		/* filling the clist with the fields' names */
		list = titem->fields;
		while (list) {
			content[0] = DB_FIELD (list->data)->name;
			i = gtk_clist_append (GTK_CLIST (clist), content);
			gtk_clist_set_row_data (GTK_CLIST (clist), i,
						list->data);
			list = g_slist_next (list);
		}
		/* no row selected */
		gtk_object_set_data (GTK_OBJECT (clist), "selrow",
				     GINT_TO_POINTER (0));
	}
	gtk_clist_thaw (GTK_CLIST (clist));
}


/*
 * users_settings_cb
 */

static void bonobo_dialog_clicked_cb (GtkWidget * dlg, gint button_number,
				      ConfManager * conf);

/* returns a GnomeDbControlWidget if successfull and NULL 
   (and displays an error message) otherwise */
static GtkWidget *get_gnome_db_control_widget (gchar * goad_id,
					       ConfManager * conf);

void
users_settings_cb (GtkObject * obj, ConfManager * conf)
{
	if (conf->users_list_dlg)
		gdk_window_raise (conf->users_list_dlg->window);
	else {
		if (conf->srv->gda_datasource &&
		    (*(conf->srv->gda_datasource->str) != '\0')) {
			gchar *bonobo_name;	/* = "control:gnome-db-users-ac"; */
			GtkWidget *control_widget;
			GtkWidget *dlg;
			gchar *str;
			GdaDsn *dsn;

			dsn = gda_dsn_find_by_name (conf->srv->
						    gda_datasource->str);
			str = GDA_DSN_PROVIDER (dsn);
			g_print ("Bonobo control Request:\n");
			g_print ("* datasource for request: %s\n",
				 conf->srv->gda_datasource->str);
			g_print ("* gda name: %s\n", GDA_DSN_GDA_NAME (dsn));
			g_print ("* provider: %s\n", str);
			bonobo_name =
				g_strdup_printf ("control:%s-users-list",
						 str);
			g_free (str);
			control_widget =
				get_gnome_db_control_widget (bonobo_name,
							     conf);
			if (control_widget) {
				dlg = gnome_dialog_new (_("Users settings"),
							GNOME_STOCK_BUTTON_OK,
							NULL);
				gtk_box_pack_start (GTK_BOX
						    (GNOME_DIALOG (dlg)->
						     vbox), control_widget,
						    TRUE, TRUE, 0);
				gtk_widget_set_usize (dlg, 450, 450);
				gtk_signal_connect (GTK_OBJECT (dlg),
						    "clicked",
						    GTK_SIGNAL_FUNC
						    (bonobo_dialog_clicked_cb),
						    conf);
				conf->users_list_dlg = dlg;

				/* setting some basic properties */
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_DSN, dsn);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_IDENT_NAME,
					 conf->srv->user_name->str);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_IDENT_PASS,
					 conf->srv->password->str);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_IDENT_SHOW,
					 FALSE);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_START, TRUE);
				/* starting the widget to display usefull data */

				gtk_widget_show_all (dlg);
			}
			if (dsn)
				g_free (dsn);
			g_free (bonobo_name);
		}
		else {
			options_config_cb (NULL, conf);
			/* sets the "Apply" button active because we consider 
			   a modif done */
			gnome_property_box_changed (GNOME_PROPERTY_BOX
						    (conf->config_dlg));
		}
	}
}

static void
bonobo_dialog_clicked_cb (GtkWidget * dlg, gint button_number,
			  ConfManager * conf)
{
	gtk_object_destroy (GTK_OBJECT (dlg));
	if (conf->users_list_dlg == dlg)
		conf->users_list_dlg = NULL;
	if (conf->users_acl_dlg == dlg)
		conf->users_acl_dlg = NULL;
}

static GtkWidget *
get_gnome_db_control_widget (gchar * goad_id, ConfManager * conf)
{
	GtkWidget *control_widget = NULL;

	if (gnome_db_control_widget_supported (goad_id)) {
		control_widget = gnome_db_control_widget_new (goad_id,
							      GNOME_DB_CONTROL_WIDGET
							      (control_widget)->
							      bonobo_widget);
		if (!GNOME_DB_CONTROL_WIDGET (control_widget)->bonobo_widget) {
			gtk_object_destroy (GTK_OBJECT (control_widget));
			gnome_error_dialog_parented (_("The Bonobo control "
						       "for this operation "
						       "cannot be loaded"),
						     GTK_WINDOW (conf->app));
			control_widget = NULL;
		}
	}
	else
		gnome_error_dialog_parented (_
					     ("The Bonobo control for this operation "
					      "cannot be found"),
					     GTK_WINDOW (conf->app));

	return control_widget;
}


/*
 * users_access_cb
 */

void
users_access_cb (GtkObject * obj, ConfManager * conf)
{
	if (conf->users_acl_dlg)
		gdk_window_raise (conf->users_acl_dlg->window);
	else {
		if (conf->srv->gda_datasource &&
		    (*(conf->srv->gda_datasource->str) != '\0')) {
			gchar *bonobo_name;	/* = "control:gnome-db-users-ac"; */
			GtkWidget *control_widget;
			GtkWidget *dlg;
			gchar *str;
			gchar *dsn;

			str = g_strdup_printf ("/gdalib/%s/",
					       conf->srv->gda_datasource->
					       str);
			gnome_config_push_prefix (str);
			g_free (str);
			str = gnome_config_get_string ("Provider");
			dsn = gnome_config_get_string ("DSN");
			gnome_config_pop_prefix ();
			bonobo_name =
				g_strdup_printf ("control:%s-users-ac", str);
			g_free (str);
			control_widget =
				get_gnome_db_control_widget (bonobo_name,
							     conf);
			if (control_widget) {
				dlg = gnome_dialog_new (_
							("Users access rights"),
							GNOME_STOCK_BUTTON_OK,
							NULL);
				gtk_box_pack_start (GTK_BOX
						    (GNOME_DIALOG (dlg)->
						     vbox), control_widget,
						    TRUE, TRUE, 0);
				gtk_widget_set_usize (dlg, 500, 450);
				gtk_signal_connect (GTK_OBJECT (dlg),
						    "clicked",
						    GTK_SIGNAL_FUNC
						    (bonobo_dialog_clicked_cb),
						    conf);
				conf->users_acl_dlg = dlg;

				/* setting some basic properties */
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_DSN, dsn);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_IDENT_NAME,
					 conf->srv->user_name->str);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_IDENT_PASS,
					 conf->srv->password->str);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_IDENT_SHOW,
					 FALSE);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_START, TRUE);
				/* starting the widget to display usefull data */

				gtk_widget_show_all (dlg);
			}
			if (dsn)
				g_free (dsn);
			g_free (bonobo_name);
		}
		else {
			options_config_cb (NULL, conf);
			/* sets the "Apply" button active because we consider 
			   a modif done */
			gnome_property_box_changed (GNOME_PROPERTY_BOX
						    (conf->config_dlg));
		}
	}
}


/*
 * users_groups_cb
 */

void
users_groups_cb (GtkObject * obj, ConfManager * conf)
{
	if (conf->users_groups_dlg)
		gdk_window_raise (conf->users_groups_dlg->window);
	else {
		if (conf->srv->gda_datasource &&
		    (*(conf->srv->gda_datasource->str) != '\0')) {
			gchar *bonobo_name;	/* = "control:gnome-db-users-groups"; */
			GtkWidget *control_widget;
			GtkWidget *dlg;
			gchar *str;
			gchar *dsn;

			str = g_strdup_printf ("/gdalib/%s/",
					       conf->srv->gda_datasource->
					       str);
			gnome_config_push_prefix (str);
			g_free (str);
			str = gnome_config_get_string ("Provider");
			dsn = gnome_config_get_string ("DSN");
			gnome_config_pop_prefix ();
			bonobo_name =
				g_strdup_printf ("control:%s-users-groups",
						 str);
			g_free (str);
			control_widget =
				get_gnome_db_control_widget (bonobo_name,
							     conf);
			if (control_widget) {
				dlg = gnome_dialog_new (_("Users groups"),
							GNOME_STOCK_BUTTON_OK,
							NULL);
				gtk_box_pack_start (GTK_BOX
						    (GNOME_DIALOG (dlg)->
						     vbox), control_widget,
						    TRUE, TRUE, 0);
				gtk_widget_set_usize (dlg, 500, 450);
				gtk_signal_connect (GTK_OBJECT (dlg),
						    "clicked",
						    GTK_SIGNAL_FUNC
						    (bonobo_dialog_clicked_cb),
						    conf);
				conf->users_groups_dlg = dlg;

				/* setting some basic properties */
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_DSN, dsn);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_IDENT_NAME,
					 conf->srv->user_name->str);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_IDENT_PASS,
					 conf->srv->password->str);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_IDENT_SHOW,
					 FALSE);
				gnome_db_control_widget_set_prop
					(GNOME_DB_CONTROL_WIDGET
					 (control_widget),
					 GNOME_DB_CONTROL_PROP_START, TRUE);
				/* starting the widget to display usefull data */

				gtk_widget_show_all (dlg);
			}
			if (dsn)
				g_free (dsn);
			g_free (bonobo_name);
		}
		else {
			options_config_cb (NULL, conf);
			/* sets the "Apply" button active because we consider 
			   a modif done */
			gnome_property_box_changed (GNOME_PROPERTY_BOX
						    (conf->config_dlg));
		}
	}
}


/*
 * printer_setup_cb
 */

void
printer_setup_cb (GtkObject * obj, ConfManager * conf)
{
	GnomePrinter *printer = NULL;
	GnomePrintContext *pc;

	printer = gnome_printer_dialog_new_modal ();
	if (printer) {
		pc = gnome_print_context_new_with_paper_size (printer, "A4");
		conf->printer = printer;
		conf->printcontext = pc;
	}
}


/*
 * CBs to set the tab of the notebook to display the right page
 */

void
show_tables_page_cb (GtkWidget * shortcut, ConfManager * conf)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (conf->nb), 0);
}

void
show_seqs_page_cb (GtkWidget * wid, ConfManager * conf)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (conf->nb), 1);
}

void
show_queries_page_cb (GtkWidget * shortcut, ConfManager * conf)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (conf->nb), 2);
}

void
show_forms_page_cb (GtkWidget * shortcut, ConfManager * conf)
{
	gtk_notebook_set_page (GTK_NOTEBOOK (conf->nb), 3);
}


/*
 * run_gnomedb_manager_cb
 */
static void manager_destroy_cb (GtkWidget * app, BonoboUIContainer * uic);
static void manager_close_cb (GtkWidget * app, GtkWidget * bonobo_win);
static int manager_delete_cb (GtkWidget * widget, GdkEvent * event,
			      gpointer data);
void
run_gnomedb_manager_cb (GtkWidget * w, ConfManager * conf)
{
	if (conf->manager_bonobo_win) {
		gtk_widget_show (conf->manager_bonobo_win);
		gdk_window_raise (conf->manager_bonobo_win->window);
	}
	else {
		GtkWidget *box, *control, *sep, *button, *bbox, *label;
		GtkWidget *bonobo_win;
		BonoboUIContainer *uic;

		/*  ui container */
		uic = bonobo_ui_container_new ();

		/* create a bonobo application (window) */
		bonobo_win = bonobo_window_new (_("GnomeDb Manager"),
						_("Possible connections"));
		gtk_widget_set_usize (GTK_WIDGET (bonobo_win), 400, 350);
		bonobo_ui_container_set_win (uic, BONOBO_WINDOW (bonobo_win));

		/* get a widget, containing the control */
		control = bonobo_widget_new_control
			("OAFIID:GNOME_Database_UIManager_Control",
			 BONOBO_OBJREF (uic));
		if (!control) {
			gnome_app_error (GNOME_APP (conf->app),
					 _
					 ("Can't find the Gnome-DB Manager component"));
			bonobo_object_unref (BONOBO_OBJECT (uic));
			gtk_widget_destroy (bonobo_win);
			return;
		}

		/* a box */
		box = gtk_vbox_new (FALSE, 2);
		gtk_container_set_border_width (GTK_CONTAINER (box),
						GNOME_PAD);
		bonobo_window_set_contents (BONOBO_WINDOW (bonobo_win), box);

		label = gtk_label_new (_
				       ("Management of possible connections:"));
		gtk_box_pack_start (GTK_BOX (box), label, FALSE, FALSE, GNOME_PAD/2.);
		gtk_box_pack_start_defaults (GTK_BOX (box), control);

		/* separator */
		sep = gtk_hseparator_new ();
		gtk_box_pack_start (GTK_BOX (box), sep, FALSE, FALSE, 0);

		/* bbox */
		bbox = gtk_hbutton_box_new ();
		gtk_container_set_border_width (GTK_CONTAINER (bbox),
						GNOME_PAD);
		gtk_box_pack_start (GTK_BOX (box), bbox, FALSE, FALSE, 0);

		/* create buttons */
		button = gnome_stock_button (GNOME_STOCK_BUTTON_CLOSE);
		gtk_container_add (GTK_CONTAINER (bbox), button);

		/* signals */
		gtk_signal_connect (GTK_OBJECT (bonobo_win), "delete_event",
				    GTK_SIGNAL_FUNC (manager_delete_cb),
				    NULL);
		gtk_signal_connect (GTK_OBJECT (bonobo_win), "destroy",
				    GTK_SIGNAL_FUNC (manager_destroy_cb),
				    uic);
		gtk_signal_connect (GTK_OBJECT (button), "clicked",
				    GTK_SIGNAL_FUNC (manager_close_cb),
				    bonobo_win);

		conf->manager_bonobo_win = bonobo_win;
		gtk_widget_show_all (GTK_WIDGET (bonobo_win));
	}
}

static void
manager_destroy_cb (GtkWidget * app, BonoboUIContainer * uic)
{
	bonobo_object_unref (BONOBO_OBJECT (uic));
}

static void
manager_close_cb (GtkWidget * app, GtkWidget * bonobo_win)
{
	gtk_widget_hide (bonobo_win);
}

static int
manager_delete_cb (GtkWidget * widget, GdkEvent * event, gpointer data)
{
	manager_close_cb (NULL, widget);
	return FALSE;
}


/* 
 * set_opened_file
 */

gint
set_opened_file (ConfManager * conf, gchar * filetxt)
{
	gchar *txt;
	gint retval = 0;
	xmlDocPtr doc;
	xmlNodePtr tree;

	if (!g_file_test (filetxt, G_FILE_TEST_ISFILE)) {
		txt = g_strdup_printf (_
				       ("Cannot open file '%s'\n(file does not exist "
					"or is not readable)."), filetxt);
		gnome_error_dialog (txt);
		g_free (txt);
		return -1;
	}
	else {
		doc = xmlParseFile (filetxt);
		if (doc) {
			txt = xmlGetProp (doc->xmlRootNode, "id_serial");
			if (txt) {
				conf->id_serial = atoi (txt);
			}
			tree = doc->xmlRootNode->xmlChildrenNode;
			while (tree) {
				txt = gasql_xml_clean_string_ends
					(xmlNodeGetContent (tree));
				if (!strcmp (tree->name, "gda_datasource")) {
					if (txt)
						g_string_assign (conf->srv->gda_datasource, txt);
					else
						g_string_assign (conf->srv->gda_datasource, "");
				}

				if (!strcmp (tree->name, "username")) {
					if (txt)
						g_string_assign (conf->srv->user_name, txt);
					else
						g_string_assign (conf->srv->user_name, "");
					if (conf->user_name)
						g_free (conf->user_name);
					conf->user_name = g_strdup (conf->srv->user_name->str);
				}

				tree = tree->next;
				if (txt)
					g_free (txt);
			}
			xmlFreeDoc (doc);
			g_string_assign (conf->srv->password, "");

			if (conf->working_file)
				g_free (conf->working_file);
			conf->working_file = filetxt;
			txt = conf_manager_get_title (conf);
			gtk_window_set_title (GTK_WINDOW (conf->app), txt);
			g_free (txt);
		}
		else {
			gnome_warning_dialog (_("An error has occured while loading the "
						"connexion parameters.\n"
						"Set them manually."));
			retval = -1;	/* ERROR backpropagation */
		}
	}

	return retval;
}


/*
 * file_new_cb
 */

static void
dummy_cb (gint reply, gpointer data)
{
}
static void file_new_dlg_cancel_cb (GtkWidget * widget, void *data);
static void file_new_dlg_ok_cb (GtkWidget * widget, void *data);

void
file_new_cb (GtkWidget * widget, ConfManager * conf)
{
	GtkWidget *dlg;

	dlg = gtk_file_selection_new (_("New file"));
	gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
	gtk_object_set_data (GTK_OBJECT (dlg), "conf", conf);
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dlg)->cancel_button), "clicked", GTK_SIGNAL_FUNC (file_new_dlg_cancel_cb), dlg);	/* ! */
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dlg)->ok_button),
			    "clicked",
			    GTK_SIGNAL_FUNC (file_new_dlg_ok_cb), dlg);
	gtk_widget_show (dlg);
}

static void
file_new_dlg_cancel_cb (GtkWidget * widget, void *data)
{
	gtk_object_destroy (GTK_OBJECT (data));
}

static void
file_new_dlg_ok_cb (GtkWidget * widget, void *data)
{
	ConfManager *conf;
	gchar *filetxt;

	conf = (ConfManager *) gtk_object_get_data (GTK_OBJECT (data),
							  "conf");
	filetxt =
		g_strdup (gtk_file_selection_get_filename
			  (GTK_FILE_SELECTION (data)));
	gtk_object_destroy (GTK_OBJECT (data));

	if (conf->working_file && !conf->save_up_to_date) {
		GtkWidget *wid;

		wid = gnome_app_question_modal (GNOME_APP (conf->app),
						_("Some work has not been "
						  "saved.\nDo you want to save it now?"),
						(GnomeReplyCallback)
						(dummy_cb), conf);
		if (!gnome_dialog_run_and_close (GNOME_DIALOG (wid)))
			file_save_cb (NULL, conf);
	}
	if (conf->working_file)
		g_free (conf->working_file);
	conf->working_file = filetxt;
	filetxt = conf_manager_get_title (conf);
	gtk_window_set_title (GTK_WINDOW (conf->app), filetxt);
	g_free (filetxt);
}


/*
 * file_open_cb
 */

static void file_open_dlg_ok_cb (GtkWidget * widget, void *data);
static void file_open_dlg_cancel_cb (GtkWidget * widget, void *data);

void
file_open_cb (GtkWidget * widget, ConfManager * conf)
{
	GtkWidget *dlg;

	/* first test if the connection is established, ... */
	if (server_access_is_open (conf->srv)) {
		gnome_warning_dialog (_
				      ("The connection to an SQL server is already "
				       "established.\nDisconnect before trying to open "
				       "another file."));
		return;
	}

	/* let's now open a dialog... */
	dlg = gtk_file_selection_new (_("File to open"));
	gtk_file_selection_hide_fileop_buttons (GTK_FILE_SELECTION (dlg));
	gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
	gtk_object_set_data (GTK_OBJECT (dlg), "conf", conf);
	gtk_signal_connect (GTK_OBJECT
			    (GTK_FILE_SELECTION (dlg)->cancel_button),
			    "clicked",
			    GTK_SIGNAL_FUNC (file_open_dlg_cancel_cb), dlg);
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dlg)->ok_button),
			    "clicked", GTK_SIGNAL_FUNC (file_open_dlg_ok_cb),
			    dlg);
	gtk_widget_show (dlg);
}

static void
file_open_dlg_ok_cb (GtkWidget * widget, void *data)
{
	ConfManager *conf;
	gchar *filetxt;

	conf = (ConfManager *) gtk_object_get_data (GTK_OBJECT (data),
							  "conf");
	filetxt =
		g_strdup (gtk_file_selection_get_filename
			  (GTK_FILE_SELECTION (data)));
	gtk_object_destroy (GTK_OBJECT (data));

	if (set_opened_file (conf, filetxt) == -1)
		g_free (filetxt);
}

static void
file_open_dlg_cancel_cb (GtkWidget * widget, void *data)
{
	gtk_object_destroy (GTK_OBJECT (data));
}


/*
 * file_close_cb
 */

void
file_close_cb (GtkWidget * widget, ConfManager * conf)
{
	gchar *str;

	if (conf->working_file) {
		g_free (conf->working_file);
		conf->working_file = NULL;
		str = conf_manager_get_title (conf);
		gtk_window_set_title (GTK_WINDOW (conf->app), str);
		g_free (str);
	}
}


/*
 * file_save_cb
 */
static void quit_real (ConfManager * conf);
void
file_save_cb (GtkWidget * widget, ConfManager * conf)
{
	xmlDocPtr doc;
	xmlNodePtr tree;
	gchar *str;

	if (conf->working_file) {
		doc = xmlNewDoc ("1.0");
		/* DTD validation */
		xmlCreateIntSubset (doc, "CONNECTION", NULL,
				    XML_GASQL_DTD_FILE);
		doc->xmlRootNode =
			xmlNewDocNode (doc, NULL, "CONNECTION", NULL);
		str = g_strdup_printf ("%d", conf->id_serial);
		xmlSetProp (doc->xmlRootNode, "id_serial", str);
		g_free (str);
		tree = xmlNewChild (doc->xmlRootNode, NULL, "gda_datasource",
				    conf->srv->gda_datasource->str);
		tree = xmlNewChild (doc->xmlRootNode, NULL, "username",
				    conf->srv->user_name->str);

		/* the ServerAccess object */
		server_access_build_xml_tree (conf->srv, doc);

		/* the Database object */
		database_build_xml_tree (conf->db, doc);

		/* all the Query objects, from the top level one */
		g_assert (conf->top_query);
		query_build_xml_tree (QUERY (conf->top_query), doc->xmlRootNode, conf);
		
		/* ALL the GUI goes here */
		tree = xmlNewChild (doc->xmlRootNode, NULL, "GUI", NULL);
		relship_build_xml_tree (NULL, tree, conf);


		xmlSaveFile (conf->working_file, doc);
		xmlFreeDoc (doc);
		conf->save_up_to_date = TRUE;

		/* check if we were waiting after saving to close the connection */
		if (conf->close_after_saving) {
			conf->close_after_saving = FALSE;
			server_access_close_connect (conf->srv);
		}
		/* check if we were waiting after saving to quit the application */
		if (conf->quit_after_saving) {
			conf->quit_after_saving = FALSE;
			quit_real (conf);
		}
	}
	else if (!conf->save_up_to_date)
		file_save_as_cb (NULL, conf);
}

/*
 * file_save_as_cb
 */

static void file_saveas_dlg_ok_cb (GtkWidget * widget, void *data);

void
file_save_as_cb (GtkWidget * widget, ConfManager * conf)
{
	GtkWidget *dlg;

	/* let's now open a dialog... */
	dlg = gtk_file_selection_new (_("File to save to"));
	gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
	gtk_object_set_data (GTK_OBJECT (dlg), "conf", conf);
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dlg)->cancel_button), "clicked", GTK_SIGNAL_FUNC (file_open_dlg_cancel_cb), dlg);	/* ! */
	gtk_signal_connect (GTK_OBJECT (GTK_FILE_SELECTION (dlg)->ok_button),
			    "clicked",
			    GTK_SIGNAL_FUNC (file_saveas_dlg_ok_cb), dlg);
	gtk_widget_show (dlg);
}


static void
file_saveas_dlg_ok_cb (GtkWidget * widget, void *data)
{
	ConfManager *conf;
	gchar *txt, *filetxt;

	conf = (ConfManager *) gtk_object_get_data (GTK_OBJECT (data),
							  "conf");
	filetxt =
		g_strdup (gtk_file_selection_get_filename
			  (GTK_FILE_SELECTION (data)));
	gtk_object_destroy (GTK_OBJECT (data));

	if (g_file_test (filetxt, G_FILE_TEST_ISDIR)) {
		txt = g_strdup_printf (_("File '%s' is a directory!\n"),
				       filetxt);
		gnome_error_dialog (txt);
		g_free (txt);
		return;
	}
	else {
		if (conf->working_file)
			g_free (conf->working_file);
		conf->working_file = filetxt;
		txt = conf_manager_get_title (conf);
		gtk_window_set_title (GTK_WINDOW (conf->app), txt);
		g_free (txt);
		file_save_cb (NULL, conf);
	}
	
	/* we set the following flag back to FALSE */
	conf->close_after_saving = FALSE;
}

void
quit_cb (GtkWidget * widget, ConfManager * conf)
{
	gint answer;
	GtkWidget *dialog, *label, *hb, *pixmap;

	/* ask if the user really wants to quit */
	if (server_access_is_open (conf->srv) && !conf->save_up_to_date) {
		dialog = gnome_dialog_new (_("Quit confirmation"), 
					   _("Save and Quit"), _("Quit without saving"),
					   GNOME_STOCK_BUTTON_CANCEL, NULL);

		label = gtk_label_new (_("Some data have not yet been saved and should be saved before\n"
					 "quitting (or will be lost).\n\n"
					 "What do you want to do?"));

	}
	else {
		dialog = gnome_dialog_new (_("Quit confirmation"), 
					   _("Yes, quit"),
					   GNOME_STOCK_BUTTON_CANCEL, NULL);

		label = gtk_label_new (_("Do you really want to quit?"));
	}

	hb = gtk_hbox_new (FALSE, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dialog)->vbox), hb, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (hb);

	pixmap = gnome_stock_pixmap_widget_new (NULL, GNOME_STOCK_PIXMAP_QUIT);
	gtk_box_pack_start (GTK_BOX (hb), pixmap, TRUE, TRUE, GNOME_PAD/2.);
	gtk_widget_show (pixmap);

	gtk_box_pack_start (GTK_BOX (hb), label, TRUE, TRUE, GNOME_PAD/2.);
	gtk_widget_show (label);
	gtk_window_set_modal (GTK_WINDOW (dialog), TRUE);
	gnome_dialog_set_parent (GNOME_DIALOG (dialog), GTK_WINDOW (conf->app));
	answer = gnome_dialog_run_and_close (GNOME_DIALOG (dialog));

	if (answer == -1) /* dialog destroyed */
		return;
	if (((server_access_is_open (conf->srv) && !conf->save_up_to_date) && (answer == 2)) ||
	    ((!server_access_is_open (conf->srv) || conf->save_up_to_date) && (answer == 1)))
		return;

	/* at this point the user really wants to quit */

	/* saving state of the configuration */
	gnome_config_push_prefix ("/gASQL/SqlServer/");
	gnome_config_private_set_string ("datasource",
					 conf->srv->gda_datasource->str);
	gnome_config_set_string ("plugins_dir", conf->plugins_dir);
	gnome_config_pop_prefix ();
	gnome_config_sync ();
	
	if ((server_access_is_open (conf->srv) && !conf->save_up_to_date) && (answer == 0)) { 
		/* save */
		file_save_cb (NULL, conf);
		if (!conf->working_file)
			conf->quit_after_saving = TRUE;
	}

	if (!conf->quit_after_saving)
		quit_real (conf);
}

static void 
quit_real (ConfManager * conf)
{
	if (!conf->save_up_to_date) /* ALL data will be lost */
		conf->save_up_to_date = TRUE;
	
	/* clears everything */
	gtk_object_unref (GTK_OBJECT (conf));

	/* and then quit */
	gtk_main_quit ();
}


/********************************************************************
 *
 * CBs to signals emitted by Database objects
 *
 ********************************************************************/


/*
 * sql_server_conn_open_cb
 */

static void ask_to_refresh_after_connect_cb (gint reply,
					     ConfManager * conf);
static void xml_parser_error_func (void *ctx, const char *msg, ...);
static void
xml_parser_error_func (void *ctx, const char *msg, ...)
{
	static gboolean has_error = FALSE;
	xmlValidCtxtPtr ctxt = (xmlValidCtxtPtr) ctx;
	ConfManager *conf;
	va_list args;
	gchar *errmsg, *str;

	if (!has_error) {
		GtkWidget *dlg;
		conf = (ConfManager *) (ctxt->userData);
		va_start (args, msg);
		str = g_strdup_vprintf (msg, args);
		va_end (args);

		errmsg = g_strdup_printf (_
					  ("The XML file has the following error:\n%s"),
					  str);
		g_free (str);

		dlg = gnome_error_dialog (errmsg);
		gnome_dialog_run_and_close (GNOME_DIALOG (dlg));
		g_free (errmsg);
		has_error = TRUE;
	}
}

/* from the server to tell the connection is UP */
void
sql_server_conn_open_cb (GtkObject * wid, ConfManager * conf)
{
	xmlDocPtr doc;
	xmlNodePtr tree;
	gboolean ok, csud, res;
	gchar *txt;

	conf->loading_in_process = TRUE;
	gnome_app_flash (GNOME_APP (conf->app),
			 _("Connection to the SQL server opened"));
	csud = conf->save_up_to_date;

	/* try to load the structure from an XML file if it exists */
	if (conf->working_file && g_file_test (conf->working_file,
					       G_FILE_TEST_ISFILE)) {
		/* 
		 * load the XML file 
		 */

		ok = TRUE;
		xmlDoValidityCheckingDefaultValue = 0;
		doc = xmlParseFile (conf->working_file);
		if (doc) {
			xmlValidCtxtPtr validc;
			gint i;
			validc = g_new0 (xmlValidCtxt, 1);
			validc->userData = conf;
			validc->error = xml_parser_error_func;
			validc->warning = xml_parser_error_func;
			xmlDoValidityCheckingDefaultValue = 1;
			i = xmlValidateDocument (validc, doc);
			if (!i) {
				xmlFreeDoc (doc);
				doc = NULL;
			}
		}
		
		if (doc) {
			tree = doc->xmlRootNode->xmlChildrenNode;
			while (tree) {
				txt = gasql_xml_clean_string (xmlNodeGetContent (tree));
				/*
				 * ServerAccess object
				 */
				if (!strcmp (tree->name, "SERVER")) {
					res = server_access_build_from_xml_tree (conf->srv, tree);
					ok = res ? ok : FALSE;
				}

				/*
				 * Database object
				 */
				if (!strcmp (tree->name, "DATABASE")) {
					res = database_build_db_from_xml_tree (conf->db, tree);
					ok = res ? ok : FALSE;
				}


				/*
				 * Query Objects
				 */
				if (!strcmp (tree->name, "Query")) {
					/* conf->top_query already exists because the
					   connection is opened */
					Query *q;
					q = QUERY (query_build_from_xml_tree (conf, tree, NULL));
					ok = q ? ok : FALSE;
				}

				/*
				 * QueryEnv Objects
				 */
				if (!strcmp (tree->name, "FORMS")) {
					xmlNodePtr node;

					node = tree->xmlChildrenNode;
					while (node) {
						if (!strcmp (node->name, "QueryEnv"))
							query_env_build_from_xml_tree (conf, node);
						node = node->next;
					}
				}

				/*
				 *  GUI parts
				 */
				if (!strcmp (tree->name, "GUI")) {
					xmlNodePtr node;

					node = tree->xmlChildrenNode;
					while (node) {
						if (!strcmp (node->name, "RelShip"))
							relship_build_from_xml_tree (conf, node);
						node = node->next;
					}
				}


				tree = tree->next;
				if (txt)
					g_free (txt);
			}
			xmlFreeDoc (doc);
			conf->save_up_to_date = csud;
		}
		else
			ok = FALSE;

		if (!ok)
			gnome_warning_dialog (_
					      ("An error has occured while loading the "
					       "selected document. The file (XML format)\n"
					       "is probably corrupted. Try to repair it."));
		else {
			/* ask to refresh the structure */
			txt = g_strdup_printf (_("DataBase '%s':\n"
						 "do you want to update the list of\n data types,"
						 " tables, functions, "
						 "sequences, ... from the server\n(recommanded "
						 "if the structure of the DB has been "
						 "modified).\n"
						 "Say Yes if unsure."),
					       conf->srv->gda_datasource->
					       str);
			gnome_question_dialog (txt,
					       (GnomeReplyCallback)
					       ask_to_refresh_after_connect_cb,
					       conf);
			g_free (txt);
		}
	}
	else {
		server_access_refresh_datas (conf->srv);
		database_refresh (conf->db, conf->srv);
	}

	/* sets the notebook to be displayed */
	if (conf->srv->features.sequences)
		gtk_widget_show (conf->sequences_page);
	else
		gtk_widget_hide (conf->sequences_page);

	gtk_widget_hide (conf->welcome);
	gtk_widget_show (conf->working_box);

	conf->loading_in_process = FALSE;
}

static void
ask_to_refresh_after_connect_cb (gint reply, ConfManager * conf)
{
	if (reply == 0)
		sql_mem_update_cb (NULL, conf);
}


/*
 * sql_server_conn_to_close_cb
 */

/* from the server to tell the connection is going to go DOWN */
void
sql_server_conn_to_close_cb (GtkObject * wid, ConfManager * conf)
{
	if (conf->check_dlg) {
		conf->check_perform = FALSE;
		gnome_dialog_close (GNOME_DIALOG (conf->check_dlg));
		conf->check_dlg = NULL;
		conf->check_pbar = NULL;
		conf->check_link_name = NULL;
		conf->check_errors = NULL;
	}
}


/*
 * sql_server_conn_close_cb
 */

/* from the server to tell the connection is DOWN */
void
sql_server_conn_close_cb (GtkObject * wid, ConfManager * conf)
{
	gnome_app_flash (GNOME_APP (conf->app),
			 _("Connection to the SQL server closed"));

	/* sets the welcome message to be displayed */
	gtk_widget_hide (conf->working_box);
	gtk_widget_show (conf->welcome);

	/* Tell that the work has not been modified (like when starting gASQL) */
	conf->save_up_to_date = TRUE;
}




/*
 * sql_server_catch_errors_cb
 */
static gint err_dlg_closed (GnomeDialog * dialog, ConfManager * conf);

void
sql_server_catch_errors_cb (GtkWidget * wid, GList * list,
			    ConfManager * conf)
{
	if (!conf->error_dlg) {
		conf->error_dlg =
			gnome_db_error_dialog_new (
					       _("Error Viewer"));
		gnome_db_error_dialog_show_errors (GNOME_DB_ERROR_DIALOG
					       (conf->error_dlg),
						gda_connection_get_errors (GDA_CONNECTION (conf->srv)));
		gtk_signal_connect (GTK_OBJECT (conf->error_dlg), "close",
				    GTK_SIGNAL_FUNC (err_dlg_closed), conf);
	}
}

static gint
err_dlg_closed (GnomeDialog * dialog, ConfManager * conf)
{
	conf->error_dlg = NULL;
	return 0;
}
