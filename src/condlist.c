/* condlist.c
 *
 * Copyright (C) 2000 - 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "condlist.h"


static void cond_list_class_init (CondListClass * class);
static void cond_list_init (CondList * cdlist);
static void cond_list_initialize (CondList * cdlist,
				  gint columns,
				  gint tree_column, gchar * titles[]);
static void cond_list_destroy (GtkObject * object);

/* signals */
enum
{
	NODE_CREATED,
	NODE_DROPPED,
	TREE_MOVE,
	SELECTION_CHANGED,
	LAST_SIGNAL
};

static gint cond_list_signals[LAST_SIGNAL] = { 0, 0, 0, 0 };

GtkType
cond_list_get_type (void)
{
	static GtkType f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Cond_List",
			sizeof (CondList),
			sizeof (CondListClass),
			(GtkClassInitFunc) cond_list_class_init,
			(GtkObjectInitFunc) cond_list_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_hbox_get_type (), &f_info);
	}

	return f_type;
}

static void
cond_list_class_init (CondListClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	cond_list_signals[NODE_CREATED] =
		gtk_signal_new ("node_created",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (CondListClass,
						   node_created),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	cond_list_signals[NODE_DROPPED] =
		gtk_signal_new ("node_dropped", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (CondListClass,
						   node_dropped),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	cond_list_signals[TREE_MOVE] =
		gtk_signal_new ("tree_move", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (CondListClass, tree_move),
				gtk_marshal_NONE__POINTER_POINTER_POINTER,
				GTK_TYPE_NONE, 3, GTK_TYPE_POINTER,
				GTK_TYPE_POINTER, GTK_TYPE_POINTER);
	cond_list_signals[SELECTION_CHANGED] =
		gtk_signal_new ("selection_changed", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (CondListClass,
						   selection_changed),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);

	gtk_object_class_add_signals (object_class, cond_list_signals,
				      LAST_SIGNAL);
	class->tree_move = NULL;
	class->node_created = NULL;
	class->node_dropped = NULL;
	class->selection_changed = NULL;
	object_class->destroy = cond_list_destroy;
}

static void
cond_list_init (CondList * cdlist)
{
	cdlist->selection = NULL;
	cdlist->no_select_list = NULL;
}

GtkWidget *
cond_list_new (gint columns, gint tree_column, gchar * titles[])
{
	GtkObject *obj;
	CondList *cdlist;

	obj = gtk_type_new (cond_list_get_type ());
	cdlist = COND_LIST (obj);

	cdlist->nb_columns = columns;
	cdlist->tree_column = tree_column;
	cond_list_initialize (cdlist, columns, tree_column, titles);

	return GTK_WIDGET (obj);
}

static void node_moved_cb (GtkCTree * ctree,
			   GtkCTreeNode * node,
			   GtkCTreeNode * new_parent,
			   GtkCTreeNode * new_sibling, CondList * cdlist);
static void select_row_cb (GtkCTree * ctree,
			   GList * node, gint column, CondList * cdlist);
static void unselect_row_cb (GtkCTree * ctree,
			     GList * node, gint column, CondList * cdlist);

static void up_clicked_cb (GtkWidget * wid, CondList * cdlist);
static void down_clicked_cb (GtkWidget * wid, CondList * cdlist);
static void right_clicked_cb (GtkWidget * wid, CondList * cdlist);
static void left_clicked_cb (GtkWidget * wid, CondList * cdlist);
static void update_where_arrow_buttons (CondList * cdlist);

static void
cond_list_initialize (CondList * cdlist,
		      gint columns, gint tree_column, gchar * titles[])
{
	GtkWidget *ctree, *button, *bb, *arrow, *sw;
	GdkColormap *cmap;
	gint i;

	/* scrolled window */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_container_set_border_width (GTK_CONTAINER (sw), 0);
	gtk_box_pack_start (GTK_BOX (cdlist), sw, TRUE, TRUE, GNOME_PAD);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_widget_show (sw);

	/* ctree */
	ctree = gtk_ctree_new_with_titles (columns, tree_column, titles);
	for (i = 0; i < columns; i++)
		gtk_clist_set_column_auto_resize (GTK_CLIST (ctree), i, TRUE);
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (sw),
					       ctree);
	cdlist->ctree = ctree;
	gtk_object_set_data (GTK_OBJECT (ctree), "cl", cdlist);
	gtk_signal_connect (GTK_OBJECT (ctree), "tree_move",
			    GTK_SIGNAL_FUNC (node_moved_cb), cdlist);
	gtk_clist_column_titles_passive (GTK_CLIST (ctree));
	gtk_clist_set_selection_mode (GTK_CLIST (ctree),
				      GTK_SELECTION_EXTENDED);
	gtk_signal_connect (GTK_OBJECT (ctree), "tree-select-row",
			    GTK_SIGNAL_FUNC (select_row_cb), cdlist);
	gtk_signal_connect (GTK_OBJECT (ctree), "tree-unselect-row",
			    GTK_SIGNAL_FUNC (unselect_row_cb), cdlist);
	gtk_ctree_set_show_stub (GTK_CTREE (ctree), TRUE);
	gtk_widget_show (ctree);

	/* buttons on the right */
	bb = gtk_vbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (cdlist), bb, FALSE, FALSE, GNOME_PAD);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);
	gtk_widget_show (bb);


	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bb), button);
	cdlist->up_button = button;
	gtk_widget_show (button);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (up_clicked_cb), cdlist);
	arrow = gtk_arrow_new (GTK_ARROW_UP, GTK_SHADOW_OUT);
	gtk_container_add (GTK_CONTAINER (button), arrow);
	gtk_widget_show (arrow);


	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bb), button);
	cdlist->dn_button = button;
	gtk_widget_show (button);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (down_clicked_cb), cdlist);
	arrow = gtk_arrow_new (GTK_ARROW_DOWN, GTK_SHADOW_OUT);
	gtk_container_add (GTK_CONTAINER (button), arrow);
	gtk_widget_show (arrow);

	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bb), button);
	cdlist->left_button = button;
	gtk_widget_show (button);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (left_clicked_cb), cdlist);
	arrow = gtk_arrow_new (GTK_ARROW_LEFT, GTK_SHADOW_OUT);
	gtk_container_add (GTK_CONTAINER (button), arrow);
	gtk_widget_show (arrow);

	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bb), button);
	cdlist->right_button = button;
	gtk_widget_show (button);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (right_clicked_cb), cdlist);
	arrow = gtk_arrow_new (GTK_ARROW_RIGHT, GTK_SHADOW_OUT);
	gtk_container_add (GTK_CONTAINER (button), arrow);
	gtk_widget_show (arrow);

	cmap = gdk_colormap_get_system ();
	cdlist->where_select_bg = g_new0 (GdkColor, 1);
	cdlist->where_select_bg->red = 20000;
	cdlist->where_select_bg->green = 50000;
	cdlist->where_select_bg->blue = 60000;

	if (!gdk_colormap_alloc_color
	    (cmap, cdlist->where_select_bg, FALSE, TRUE))
		gdk_color_white (cmap, cdlist->where_select_bg);

	update_where_arrow_buttons (cdlist);
}

static void
cond_list_destroy (GtkObject * object)
{
	CondList *cdlist;
	GtkObject *parent_class = NULL;

	parent_class = gtk_type_class (gtk_object_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_COND_LIST (object));

	cdlist = COND_LIST (object);

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void
node_moved_cb (GtkCTree * ctree,
	       GtkCTreeNode * node,
	       GtkCTreeNode * new_parent,
	       GtkCTreeNode * new_sibling, CondList * cdlist)
{
#ifdef debug_signal
	g_print (">> 'TREE_MOVE' from condlist.c->node_moved_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (cdlist), cond_list_signals[TREE_MOVE],
			 node, new_parent, new_sibling);
#ifdef debug_signal
	g_print ("<< 'TREE_MOVE' from condlist.c->node_moved_cb\n");
#endif
}

static void where_visual_selection (GtkCTree * ctree,
				    GtkCTreeNode * node, GdkColor * color);
static void
select_row_cb (GtkCTree * ctree, GList * node, gint column, CondList * cdlist)
{
	GtkCTreeRow *row;
	gboolean is_leaf;

	/* visual feedback */
	is_leaf = FALSE;
	row = GTK_CTREE_ROW (node);
	is_leaf = row->is_leaf;
	if (!is_leaf) {
		GtkCTreeNode *child;
		gtk_clist_freeze (GTK_CLIST (ctree));
		child = GTK_CTREE_ROW (node)->children;
		while (child) {
			gtk_ctree_pre_recursive (ctree, child,
						 (GtkCTreeFunc)
						 where_visual_selection,
						 cdlist->where_select_bg);
			child = GTK_CTREE_ROW (child)->sibling;
		}
		gtk_clist_thaw (GTK_CLIST (ctree));
	}

	/* dealing with the selection */
	if (!g_slist_find (cdlist->selection, node))
		cdlist->selection = g_slist_append (cdlist->selection, node);

	/* action buttons update */
	update_where_arrow_buttons (cdlist);

#ifdef debug_signal
	g_print (">> 'SELECTION_CHANGED' from condlist.c->select_row_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (cdlist),
			 cond_list_signals[SELECTION_CHANGED]);
#ifdef debug_signal
	g_print ("<< 'SELECTION_CHANGED' from condlist.c->select_row_cb\n");
#endif
}

static void
unselect_row_cb (GtkCTree * ctree,
		 GList * node, gint column, CondList * cdlist)
{
	GtkCTreeRow *row;
	gboolean is_leaf;

	/* visual feedback */
	is_leaf = FALSE;
	row = GTK_CTREE_ROW (node);
	is_leaf = row->is_leaf;
	if (!is_leaf) {
		GtkCTreeNode *child;
		gtk_clist_freeze (GTK_CLIST (ctree));
		child = GTK_CTREE_ROW (node)->children;
		while (child) {
			gtk_ctree_pre_recursive (ctree, child,
						 (GtkCTreeFunc)
						 where_visual_selection,
						 NULL);
			child = GTK_CTREE_ROW (child)->sibling;
		}
		gtk_clist_thaw (GTK_CLIST (ctree));
	}

	/* dealing with the selection */
	if (g_slist_find (cdlist->selection, node))
		cdlist->selection = g_slist_remove (cdlist->selection, node);

	/* action buttons update */
	update_where_arrow_buttons (cdlist);

#ifdef debug_signal
	g_print (">> 'SELECTION_CHANGED' from condlist.c->unselect_row_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (cdlist),
			 cond_list_signals[SELECTION_CHANGED]);
#ifdef debug_signal
	g_print ("<< 'SELECTION_CHANGED' from condlist.c->unselect_row_cb\n");
#endif
}

static void
where_visual_selection (GtkCTree * ctree,
			GtkCTreeNode * node, GdkColor * color)
{
	CondList *cdlist;

	cdlist = gtk_object_get_data (GTK_OBJECT (ctree), "cl");
	if (color == cdlist->where_select_bg)
		gtk_ctree_node_set_selectable (ctree, node, FALSE);
	else
		gtk_ctree_node_set_selectable (ctree, node, TRUE);

	gtk_ctree_node_set_background (ctree, node, color);
	gtk_widget_queue_draw (GTK_WIDGET (ctree));
}

static gboolean is_selection_siblings_only (CondList * cdlist);
static gint get_selection_top_row (CondList * cdlist);
static gint get_selection_bottom_row (CondList * cdlist);
static gint get_last_possible_row (CondList * cdlist);

static void
update_where_arrow_buttons (CondList * cdlist)
{
	gboolean siblings;
	gint row;

	siblings = is_selection_siblings_only (cdlist);
	gtk_widget_set_sensitive (cdlist->left_button, siblings);
	gtk_widget_set_sensitive (cdlist->right_button, siblings);

	row = get_selection_top_row (cdlist);
	gtk_widget_set_sensitive (cdlist->up_button,
				  (row == 0) ? FALSE : TRUE);

	row = get_selection_bottom_row (cdlist);
	if (row == get_last_possible_row (cdlist)) {
		GtkCTreeNode *node;
		node = gtk_ctree_node_nth (GTK_CTREE (cdlist->ctree), row);
		if (g_slist_find (cdlist->selection, node) &&
		    node && GTK_CTREE_ROW (node)->parent)
			gtk_widget_set_sensitive (cdlist->dn_button, TRUE);
		else
			gtk_widget_set_sensitive (cdlist->dn_button, FALSE);
	}
	else
		gtk_widget_set_sensitive (cdlist->dn_button, TRUE);
}

/* returns FALSE if no selection */
static gboolean
is_selection_siblings_only (CondList * cdlist)
{
	gboolean retval = TRUE;
	GtkCTreeNode *father;
	GtkCTreeRow *row;
	GSList *list;

	if (!cdlist->selection)
		return FALSE;
	row = GTK_CTREE_ROW (cdlist->selection->data);
	father = row->parent;
	list = g_slist_next (cdlist->selection);
	while (retval && list) {
		row = GTK_CTREE_ROW (list->data);
		if (father != row->parent)
			retval = FALSE;
		list = g_slist_next (list);
	}

	return retval;
}

static gint
get_selection_top_row (CondList * cdlist)
{
	gint retval = -1;
	gint i;
	GtkCTreeNode *node;

	if (!cdlist->selection)
		return 0;

	i = 0;
	while ((retval < 0) && (i < GTK_CLIST (cdlist->ctree)->rows)) {
		node = gtk_ctree_node_nth (GTK_CTREE (cdlist->ctree), i);
		if (g_slist_find (cdlist->selection, node))
			retval = i;
		i++;
	}

	return retval;
}

/* returns last possible row if no selection */
static gint
get_selection_bottom_row (CondList * cdlist)
{
	gint retval = -1;
	gint i;
	GtkCTreeNode *node;
	GSList *ptr;
	gboolean ancestor;

	i = get_last_possible_row (cdlist);

	if (!cdlist->selection)
		return i;

	while ((retval < 0) && (i >= 0)) {
		node = gtk_ctree_node_nth (GTK_CTREE (cdlist->ctree), i);
		if (g_slist_find (cdlist->selection, node))
			retval = i;
		/* see if there is an ancestor in the selection list */
		ancestor = FALSE;
		ptr = cdlist->selection;
		while (!ancestor && ptr) {
			if (gtk_ctree_is_ancestor (GTK_CTREE (cdlist->ctree),
						   GTK_CTREE_NODE (ptr->data),
						   node))
				ancestor = TRUE;
			ptr = g_slist_next (ptr);
		}
		if (ancestor)
			retval = i;
		i--;
	}

	return retval;
}

static gint
get_last_possible_row (CondList * cdlist)
{
	gint retval;
	gboolean found;

	if (!cdlist->no_select_list)
		return GTK_CLIST (cdlist->ctree)->rows - 1;

	found = FALSE;
	retval = GTK_CLIST (cdlist->ctree)->rows - 1;
	while (!found && (retval > 0)) {
		if (gtk_ctree_node_nth (GTK_CTREE (cdlist->ctree), retval) ==
		    GTK_CTREE_NODE (cdlist->no_select_list->data))
			found = TRUE;
		else
			retval--;
	}

	return retval - 1;
}

static void
up_clicked_cb (GtkWidget * wid, CondList * cdlist)
{
	/* here the top row of the selection is not row 0 */
	GtkCTreeNode *topsel, *node, *parent = NULL, *sibling = NULL;
	GtkCTreeRow *ctreerow;
	gint row;
	GSList *sels, *to_remove;

	gtk_clist_freeze (GTK_CLIST (cdlist->ctree));

	row = get_selection_top_row (cdlist);
	node = gtk_ctree_node_nth (GTK_CTREE (cdlist->ctree), row - 1);
	topsel = gtk_ctree_node_nth (GTK_CTREE (cdlist->ctree), row);
	ctreerow = GTK_CTREE_ROW (node);

	if (ctreerow->parent != GTK_CTREE_ROW (topsel)->parent) {	/* topsel and node are not siblings */
		if (GTK_CTREE_ROW (topsel)->parent == node) {
			sibling = node;
			parent = ctreerow->parent;
		}
		else {
			/* find the sibling of topsel from node ancestors */
			GtkCTreeNode *sib = NULL, *node2;
			node2 = ctreerow->parent;
			while (!sib && node2) {
				if (GTK_CTREE_ROW (node2)->sibling == topsel)
					sib = node2;
				node2 = GTK_CTREE_ROW (node2)->parent;
			}
			/* sib SHOULD not be NULL here */
			sibling = NULL;
			parent = sib;
		}
	}
	else {			/* topsel and node are siblings */
		parent = ctreerow->parent;
		sibling = node;
	}

	/* move all the selection to the new parent and sibling position */
	to_remove = NULL;
	sels = cdlist->selection;
	while (sels) {
		GtkCTreeNode *oldp;

		oldp = GTK_CTREE_ROW (GTK_CTREE_NODE (sels->data))->parent;
		gtk_ctree_move (GTK_CTREE (cdlist->ctree),
				GTK_CTREE_NODE (sels->data), parent, sibling);

		/* if the old parent has no more child, destroy it */
		if (oldp && !GTK_CTREE_ROW (oldp)->children)
			to_remove = g_slist_append (to_remove, oldp);

		sels = g_slist_next (sels);
	}

	while (to_remove) {
		GtkCTreeNode *oldp;
		GSList *hold;

		oldp = (GtkCTreeNode *) to_remove->data;
		if (!GTK_CTREE_ROW (oldp)->children)
			cond_list_remove_node (cdlist, oldp);
		hold = to_remove;
		to_remove = g_slist_remove_link (to_remove, to_remove);
		g_slist_free_1 (hold);
	}

	gtk_clist_thaw (GTK_CLIST (cdlist->ctree));

	update_where_arrow_buttons (cdlist);
}

static GtkCTreeNode *get_selection_bottom_node (CondList * cdlist);
static void
down_clicked_cb (GtkWidget * wid, CondList * cdlist)
{
	/* here the bottom row of the selection is before the row 
	   with the automatic dependencies */
	GtkCTreeNode *botsel, *node, *parent = NULL, *sibling = NULL;
	GtkCTreeRow *ctreerow;
	gint row;
	GSList *sels;

	gtk_clist_freeze (GTK_CLIST (cdlist->ctree));

	row = get_selection_bottom_row (cdlist);
	node = gtk_ctree_node_nth (GTK_CTREE (cdlist->ctree), row + 1);
	botsel = get_selection_bottom_node (cdlist);
	if (node)
		ctreerow = GTK_CTREE_ROW (node);
	else
		ctreerow = NULL;

	if (!GTK_CTREE_ROW (botsel)->sibling) {	/* botsel has no sibling */
		sibling = NULL;
		/* GTK_CTREE_ROW(botsel)->parent can't be NULL because otherwise
		   we are on the last row which is not selectable */
		parent = GTK_CTREE_ROW (GTK_CTREE_ROW (botsel)->parent)->
			parent;
		if (GTK_CTREE_ROW (GTK_CTREE_ROW (botsel)->parent)->sibling ==
		    node)
			sibling = node;
	}
	else {
		/* node is the sibling of selbot 
		   NB: ctreerow SHOULD not be NULL at this point because otherwise 
		   GTK_CTREE_ROW(botsel)->sibling is NULL */
		if (ctreerow->is_leaf ||
		    (!ctreerow->is_leaf && !ctreerow->expanded)) {
			sibling = ctreerow->sibling;;
			parent = ctreerow->parent;
		}
		else {
			sibling = ctreerow->children;
			parent = node;
		}
	}

	/* move all the selection to the new parent and sibling position */
	sels = cdlist->selection;
	while (sels) {
		GtkCTreeNode *oldp;

		oldp = GTK_CTREE_ROW (GTK_CTREE_NODE (sels->data))->parent;
		gtk_ctree_move (GTK_CTREE (cdlist->ctree),
				GTK_CTREE_NODE (sels->data), parent, sibling);

		/* if the old parent has no more child, destroy it */
		if (oldp && !GTK_CTREE_ROW (oldp)->children)
			cond_list_remove_node (cdlist, oldp);

		sels = g_slist_next (sels);
	}

	gtk_clist_thaw (GTK_CLIST (cdlist->ctree));

	update_where_arrow_buttons (cdlist);
}

/* returns NULL if no selection */
static GtkCTreeNode *
get_selection_bottom_node (CondList * cdlist)
{
	GtkCTreeNode *node, *retval = NULL;
	gint i;

	i = get_last_possible_row (cdlist);
	if (!cdlist->selection)
		return NULL;

	while (!retval && (i >= 0)) {
		node = gtk_ctree_node_nth (GTK_CTREE (cdlist->ctree), i);
		if (g_slist_find (cdlist->selection, node))
			retval = node;
		i--;
	}

	return retval;
}

static void
left_clicked_cb (GtkWidget * wid, CondList * cdlist)
{
	/* here the selectionned rows DO HAVE the same direct parent */
	GtkCTreeNode *parent, *nparent, *sibs;
	GSList *sels;

	sels = cdlist->selection;
	if (!sels)
		return;

	parent = GTK_CTREE_ROW (sels->data)->parent;
	if (!parent)
		return;

	/* move the selection */
	nparent = GTK_CTREE_ROW (parent)->parent;
	while (sels) {
		sibs = GTK_CTREE_NODE (sels->data);
		gtk_ctree_move (GTK_CTREE (cdlist->ctree), sibs, nparent,
				parent);
		sels = g_slist_next (sels);
	}

	/* if the parent has no more children, remove it. */
	if (!GTK_CTREE_ROW (parent)->children)
		cond_list_remove_node (cdlist, parent);

	update_where_arrow_buttons (cdlist);
}

static void
right_clicked_cb (GtkWidget * wid, CondList * cdlist)
{
	/* here the selectionned rows DO HAVE the same direct parent */
	GtkCTreeNode *parent, *nparent, *sibs;
	GSList *sels;
	gchar **text;

	sels = cdlist->selection;
	if (!sels)
		return;

	parent = GTK_CTREE_ROW (sels->data)->parent;
	text = g_new0 (gchar *, cdlist->nb_columns);
	text[cdlist->tree_column] = _("AND");
	nparent = gtk_ctree_insert_node (GTK_CTREE (cdlist->ctree), parent, GTK_CTREE_NODE (sels->data),	/*FIXME: top sel */
					 text, 0, NULL, NULL, NULL, NULL,
					 FALSE, TRUE);
#ifdef debug_signal
	g_print (">> 'NODE_CREATED' from condlist.c->right_clicked_cb\n");
#endif
	g_print (">> 'NODE_CREATED' from condlist.c->right_clicked_cb\n");
	gtk_signal_emit (GTK_OBJECT (cdlist), cond_list_signals[NODE_CREATED],
			 nparent);
#ifdef debug_signal
	g_print ("<< 'NODE_CREATED' from condlist.c->right_clicked_cb\n");
#endif
	g_free (text);
	while (sels) {
		sibs = GTK_CTREE_NODE (sels->data);
		gtk_ctree_move (GTK_CTREE (cdlist->ctree), sibs, nparent,
				NULL);
		sels = g_slist_next (sels);
	}

	update_where_arrow_buttons (cdlist);
}


/*
 * 
 * Contents Manipulation
 *
 */
GtkCTreeNode *
cond_list_insert_node (CondList * cdlist,
		       gpointer mem_ptr,
		       GtkCTreeNode * parent,
		       GtkCTreeNode * sibling,
		       gchar * text[], gboolean is_leaf, gboolean expanded)
{
	GtkCTreeNode *node, *par, *sib;
	gboolean error;
	GSList *unsel;
	g_return_val_if_fail ((cdlist != NULL), NULL);

	/* some tests to insert the node BEFORE the non selectable nodes */
	par = parent;
	sib = sibling;
	if (cdlist->no_select_list) {
		if (parent) {
			unsel = cdlist->no_select_list;
			error = FALSE;
			while (unsel && !error) {
				if (gtk_ctree_is_ancestor
				    (GTK_CTREE (cdlist->ctree),
				     GTK_CTREE_NODE (unsel->data), parent))
					error = TRUE;
				unsel = g_slist_next (unsel);
			}
			if (error) {
				par = NULL;
				sib = cdlist->no_select_list->data;
			}
		}
		else {
			if (!sibling ||
			    ((sibling != cdlist->no_select_list->data) &&
			     g_slist_find (cdlist->no_select_list, sibling)))
				sib = cdlist->no_select_list->data;
		}
	}

	node = gtk_ctree_insert_node (GTK_CTREE (cdlist->ctree), par,
				      sib, text,
				      0, NULL, NULL, NULL, NULL,
				      is_leaf, expanded);
	if (mem_ptr)
		gtk_ctree_node_set_row_data (GTK_CTREE (cdlist->ctree), node,
					     mem_ptr);
	update_where_arrow_buttons (cdlist);

#ifdef debug_signal
	g_print (">> 'NODE_CREATED' from condlist.c->cond_list_insert_node\n");
#endif
	gtk_signal_emit (GTK_OBJECT (cdlist), cond_list_signals[NODE_CREATED],
			 node);
#ifdef debug_signal
	g_print ("<< 'NODE_CREATED' from condlist.c->cond_list_insert_node\n");
#endif

	return node;
}

GtkCTreeNode *
cond_list_insert_unselectable_node (CondList * cdlist,
				    gpointer mem_ptr,
				    GtkCTreeNode * parent,
				    GtkCTreeNode * sibling,
				    gchar * text[],
				    gboolean is_leaf, gboolean expanded)
{
	GtkCTreeNode *node, *par, *sib;
	gboolean error;
	GSList *unsel;
	g_return_val_if_fail ((cdlist != NULL), NULL);

	/* node among the non selectable items */
	par = parent;
	sib = sibling;
	if (!cdlist->no_select_list)
		par = sib = NULL;
	else {
		if (parent) {
			unsel = cdlist->no_select_list;
			error = FALSE;
			while (unsel && !error) {
				if (!gtk_ctree_is_ancestor
				    (GTK_CTREE (cdlist->ctree),
				     GTK_CTREE_NODE (unsel->data), parent))
					error = TRUE;
				unsel = g_slist_next (unsel);
			}
			if (error)
				par = sib = NULL;
		}
		else {
			if (sibling
			    && !g_slist_find (cdlist->no_select_list,
					      sibling))
				sib = cdlist->no_select_list->data;
		}
	}


	node = gtk_ctree_insert_node (GTK_CTREE (cdlist->ctree), par,
				      sib, text,
				      0, NULL, NULL, NULL, NULL,
				      is_leaf, expanded);
	gtk_ctree_node_set_row_data (GTK_CTREE (cdlist->ctree), node,
				     mem_ptr);
	gtk_ctree_node_set_selectable (GTK_CTREE (cdlist->ctree), node,
				       FALSE);
	if (!parent)
		cdlist->no_select_list =
			g_slist_append (cdlist->no_select_list, node);
	update_where_arrow_buttons (cdlist);

	return node;
}

void
cond_list_move_node (CondList * cdlist,
		     GtkCTreeNode * node,
		     GtkCTreeNode * new_parent, GtkCTreeNode * new_sibling)
{
	g_return_if_fail (cdlist != NULL);
	g_return_if_fail (node != NULL);

	gtk_ctree_move (GTK_CTREE (cdlist->ctree),
			node, new_parent, new_sibling);
	update_where_arrow_buttons (cdlist);
}

void
cond_list_remove_node (CondList * cdlist, GtkCTreeNode * node)
{
	g_return_if_fail (cdlist != NULL);
	g_return_if_fail (node != NULL);

	/* unselect the row if selected */
	if (g_slist_find (cdlist->selection, node))
		gtk_ctree_unselect (GTK_CTREE (cdlist->ctree), node);

	g_print ("Selection is %p\n", cdlist->selection);

#ifdef debug_signal
	g_print (">> 'NODE_DROPPED' from condlist.c->cond_list_remove_node\n");
#endif
	g_print (">> 'NODE_DROPPED' from condlist.c->cond_list_remove_node\n");
	gtk_signal_emit (GTK_OBJECT (cdlist), cond_list_signals[NODE_DROPPED],
			 node);
#ifdef debug_signal
	g_print ("<< 'NODE_DROPPED' from condlist.c->cond_list_remove_node\n");
#endif

	/* the actual removal takes place after the signal being propagated because
	   otherwise the node reference is just an empty shell */
	gtk_ctree_remove_node (GTK_CTREE (cdlist->ctree), node);
}

void
cond_list_remove_node_from_data (CondList * cdlist, gpointer mem_ptr)
{
	GtkCTreeNode *node;
	g_return_if_fail (cdlist != NULL);

	node = gtk_ctree_find_by_row_data (GTK_CTREE (cdlist->ctree),
					   NULL, mem_ptr);
	if (node)
		cond_list_remove_node (cdlist, node);
	else
		g_warning ("No node found for ptr %p\n", mem_ptr);
}

void
cond_list_update_node (CondList * cdlist, GtkCTreeNode * node, gchar * text[])
{
	gint i, num;
	g_return_if_fail (cdlist != NULL);
	g_return_if_fail (node != NULL);

	num = GTK_CLIST (cdlist->ctree)->columns;
	for (i = 0; i < num; i++)
		gtk_ctree_node_set_text (GTK_CTREE (cdlist->ctree),
					 node, i, text[i]);
}

GtkCTreeNode *
cond_list_find_node_from_data (CondList * cdlist, gpointer mem_ptr)
{
	GtkCTreeNode *node;
	g_return_val_if_fail (cdlist != NULL, NULL);

	if (!mem_ptr)
		return NULL;
	node = gtk_ctree_find_by_row_data (GTK_CTREE (cdlist->ctree),
					   NULL, mem_ptr);
	return node;
}

gpointer
cond_list_find_data_from_node (CondList * cdlist, GtkCTreeNode * node)
{
	g_return_val_if_fail (cdlist != NULL, NULL);
	g_return_val_if_fail (node != NULL, NULL);
	return gtk_ctree_node_get_row_data (GTK_CTREE (cdlist->ctree), node);
}
