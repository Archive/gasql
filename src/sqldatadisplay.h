/* sqldatadisplay.h
 *
 * Copyright (C) 1999 - 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __SQL_DATA_DISPLAY__
#define __SQL_DATA_DISPLAY__

#include "datadisplay-common.h"
#include "server-rs.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */




/*****************************************************************************/
/*                                                                           */
/* Default functions                                                         */
/*                                                                           */
/*****************************************************************************/
	gchar *gdafield_to_str_default (GdaField * field);
	DataDisplay *gdafield_to_widget_default (GdaField * field);
	void gdafield_to_widget_up_default (DataDisplay * wid,
					    GdaField * field);
	gchar *gdafield_to_sql_default (GdaField * field);
	gchar *widget_to_sql_default (DataDisplay * wid);
	DataDisplay *sql_to_widget_default (gchar * sql);

/*****************************************************************************/
/*                                                                           */
/* Functions for the numbers                                                 */
/*                                                                           */
/*****************************************************************************/
	gchar *gdafield_to_str_number (GdaField * field);
	DataDisplay *gdafield_to_widget_number (GdaField * field);
	void gdafield_to_widget_up_number (DataDisplay * wid,
					   GdaField * field);
	gchar *gdafield_to_sql_number (GdaField * field);
	gchar *widget_to_sql_number (DataDisplay * wid);
	DataDisplay *sql_to_widget_number (gchar * sql);

/*****************************************************************************/
/*                                                                           */
/* Functions for the booleans                                                */
/*                                                                           */
/*****************************************************************************/
	gchar *gdafield_to_str_bool (GdaField * field);
	DataDisplay *gdafield_to_widget_bool (GdaField * field);
	void gdafield_to_widget_up_bool (DataDisplay * wid, GdaField * field);
	gchar *gdafield_to_sql_bool (GdaField * field);
	gchar *widget_to_sql_bool (DataDisplay * wid);
	DataDisplay *sql_to_widget_bool (gchar * sql);

/*****************************************************************************/
/*                                                                           */
/* Functions for the dates                                                   */
/*                                                                           */
/*****************************************************************************/
	gchar *gdafield_to_str_date (GdaField * field);
	DataDisplay *gdafield_to_widget_date (GdaField * field);
	void gdafield_to_widget_up_date (DataDisplay * wid, GdaField * field);
	gchar *gdafield_to_sql_date (GdaField * field);
	gchar *widget_to_sql_date (DataDisplay * wid);
	DataDisplay *sql_to_widget_date (gchar * sql);

/*****************************************************************************/
/*                                                                           */
/* Functions for the times                                                   */
/*                                                                           */
/*****************************************************************************/
	gchar *gdafield_to_str_time (GdaField * field);
	DataDisplay *gdafield_to_widget_time (GdaField * field);
	void gdafield_to_widget_up_time (DataDisplay * wid, GdaField * field);
	gchar *gdafield_to_sql_time (GdaField * field);
	gchar *widget_to_sql_time (DataDisplay * wid);
	DataDisplay *sql_to_widget_time (gchar * sql);


/*****************************************************************************/
/*                                                                           */
/* Helper functions                                                          */
/*                                                                           */
/*****************************************************************************/

	/* conversion of a text string to escape the forbidden characters */
	/* WARNING: the str string is freed, and the returned value will have
	   to be freeed */
	gchar *val_access_escape_chars (gchar * str);
	gchar *val_access_unescape_chars (gchar * str);

	/* Returns a list of SqlDataDisplayFns structs packaging all the above 
	   functions */
	GSList *sql_data_display_get_initial_list ();
	void sql_data_display_free_display_fns (SqlDataDisplayFns * fns);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
