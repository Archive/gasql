/* datadisplay_common.h
 *
 * Copyright (C) 2000,2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __DATA_DISPLAY_COMMON__
#define __DATA_DISPLAY_COMMON__

#include <gnome.h>
#include <GDA.h>
#include <gda-field.h>
#include "datadisplay.h"
#include <ltdl.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

	typedef struct _SqlDataDisplayFns SqlDataDisplayFns;
	struct _SqlDataDisplayFns
	{
		gchar *descr;
		gchar *detailled_descr;
		gchar *plugin_name;	/* NULL if builtin Data display */
		gchar *plugin_file;	/* NULL if builtin Data display */
		lt_dlhandle lib_handle;	/* NULL if builtin Data display */
		gchar *version;	/* NULL if builtin Data display */
		gchar *(*get_unique_key) ();	/* NULL if builtin Data display */
		guint nb_gda_type;
		GDA_ValueType *valid_gda_types;

		/* All these functions can return NULL except for gdafield_to_widget */
		gchar *(*gdafield_to_str) (GdaField * field);
		DataDisplay *(*gdafield_to_widget) (GdaField * field);	/* NO NULL */
		void (*gdafield_to_widget_update) (DataDisplay * wid,
						   GdaField * field);
		gchar *(*widget_to_sql) (DataDisplay * wid);
		DataDisplay *(*sql_to_widget) (gchar * sql);
		gchar *(*gdafield_to_sql) (GdaField * field);

		/* tell if the plugin will need to use the free space provided in a box for
		   example */
		  gboolean (*use_free_space) (DataDisplay * wid);
	};

#ifdef __cplusplus
}
#endif				/* __cplusplus */


#endif
