/* query-env-editor.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY_ENV_EDITOR__
#define __QUERY_ENV_EDITOR__

#include <gnome.h>
#include "query-env.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define QUERY_ENV_EDITOR(obj)          GTK_CHECK_CAST (obj, query_env_editor_get_type(), QueryEnvEditor)
#define QUERY_ENV_EDITOR_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, query_env_editor_get_type (), QueryEnvEditorClass)
#define IS_QUERY_ENV_EDITOR(obj)       GTK_CHECK_TYPE (obj, query_env_editor_get_type ())


	typedef struct _QueryEnvEditor      QueryEnvEditor;
	typedef struct _QueryEnvEditorClass QueryEnvEditorClass;

	/* struct for the object's data */
	struct _QueryEnvEditor
	{
		GtkVBox    widget;

		QueryEnv  *env;

		GtkWidget *glade_import_dlg; /* dialog to import a glade widget */
		gboolean   no_insert;	/* TRUE when we don't have enough info to insert new data */
		
		GtkWidget *layout_label;
		GtkWidget *name_entry;
		GtkWidget *descr_entry;

		/* Form or Grid menu */
		GtkWidget *types_option_menu;
		GtkWidget *form_menu_item;
		GtkWidget *grid_menu_item;
		
		/* modif tables */
		GtkWidget *tables_option_menu;
		GtkWidget *tables_menu;
	};

	/* struct for the object's class */
	struct _QueryEnvEditorClass
	{
		GtkVBoxClass parent_class;
	};

	/* generic widget's functions */
	guint      query_env_editor_get_type          (void);
	GtkWidget *query_env_editor_new               (QueryEnv * q);

	/* Helper function: get a widget in a dialog */
	GtkWidget *query_env_editor_get_in_dialog (QueryEnv * env);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
