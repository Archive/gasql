/* query-field.c
 *
 * Copyright (C) 2001 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "query.h"
#include "query-field-private.h"


static void query_field_class_init (QueryFieldClass * class);
static void query_field_init (QueryField * srv);
static void query_field_destroy (GtkObject * object);

/* 
 * Main static functions 
 */
static QueryFieldIface *find_interface (QueryField *qf);
static void init_types_list (QueryFieldClass * class);


/*
 * static variables 
 */

/* get a pointer to the parents to be able to call their destructor */
static GtkObject *parent_class = NULL;

/* get a pointer on the class of QueryField for static data access */
static QueryFieldClass *query_field_class = NULL;

enum
{
	FIELD_MODIFIED,
	FIELD_TYPE_CHANGED,
	NAME_CHANGED,
	ALIAS_CHANGED,
	STATUS_CHANGED,
	LAST_SIGNAL
};

static gint query_field_signals[LAST_SIGNAL] = { 0, 0, 0, 0, 0 };


guint
query_field_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"QueryField",
			sizeof (QueryField),
			sizeof (QueryFieldClass),
			(GtkClassInitFunc) query_field_class_init,
			(GtkObjectInitFunc) query_field_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_object_get_type (), &f_info);
	}

	return f_type;
}

static void
query_field_class_init (QueryFieldClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	query_field_class = class;

	query_field_signals[FIELD_MODIFIED] =
		gtk_signal_new ("field_modified", GTK_RUN_FIRST, object_class->type,
				GTK_SIGNAL_OFFSET (QueryFieldClass, field_modified),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	query_field_signals[FIELD_TYPE_CHANGED] =
		gtk_signal_new ("field_type_changed", GTK_RUN_FIRST, object_class->type,
				GTK_SIGNAL_OFFSET (QueryFieldClass, field_type_changed),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	query_field_signals[NAME_CHANGED] =
		gtk_signal_new ("name_changed", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryFieldClass,
						   name_changed),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	query_field_signals[ALIAS_CHANGED] =
		gtk_signal_new ("alias_changed", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryFieldClass,
						   alias_changed),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);

	query_field_signals[STATUS_CHANGED] =
		gtk_signal_new ("status_changed", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryFieldClass,
						   status_changed),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);


	gtk_object_class_add_signals (object_class, query_field_signals,
				      LAST_SIGNAL);
	class->field_modified = NULL;
	class->field_type_changed = NULL;
	class->name_changed = NULL;
	class->alias_changed = NULL;
	class->status_changed = NULL;

	/* need to initialize the possible object types */
	init_types_list (class);

	object_class->destroy = query_field_destroy;
	parent_class = gtk_type_class (gtk_object_get_type ());
}

/* this function creates a list of all the possible query object types */
static void init_types_list (QueryFieldClass * class)
{
	class->field_types = NULL;
	class->field_types = g_slist_append (class->field_types, query_field_field_get_iface());
	class->field_types = g_slist_append (class->field_types, query_field_allfields_get_iface());
	class->field_types = g_slist_append (class->field_types, query_field_function_get_iface());
	/* ... */
}

static void
query_field_init (QueryField * qf)
{
	qf->query = NULL;
	qf->name = NULL;
	qf->alias = NULL;
	qf->is_printed = FALSE;
	qf->activated = FALSE;
	qf->private_data = NULL; /* safe value */
	qf->id = 0;
	qf->ref_counter = 0;
}

GtkObject *
query_field_new (Query *q, gchar * name, QueryFieldType field_type)
{
	GtkObject *obj;
	QueryField *qf;
	QueryFieldIface *iface;

	g_assert (q != NULL);

	obj = gtk_type_new (query_field_get_type ());
	qf = QUERY_FIELD (obj);

	qf->query = q;

	qf->id = query_get_unique_field_id (q, 0);
	if (name)
		qf->name = g_strdup (name);

	qf->field_type = field_type;

	iface = find_interface (qf);
	g_assert (iface != NULL);
	(* iface->init)(qf);

	return obj;
}

/* detects the kind of object to build and makes a new one. q can't be NULL */
GtkObject *
query_field_new_objref (Query *q, gchar * name, gchar *type)
{
	QueryFieldType field_type = QUERY_FIELD_LAST;
	gboolean found = FALSE;
	GtkObject *obj = NULL;

	if ((*type == 'f') && (*(type+1) == 'i')) {
		field_type = QUERY_FIELD_FIELD;
		found = TRUE;
	}

	if ((!found) && (*type == 'a') && (*(type+1) == 'l')) { 
		field_type = QUERY_FIELD_ALLFIELDS;
		found = TRUE;
	}

	if ((!found) && (*type == 'a') && (*(type+1) == 'g')) {
		field_type = QUERY_FIELD_AGGREGATE;
		found = TRUE;
	}

	if ((!found) && (*type == 'f') && (*(type+1) == 'u')) { 
		field_type = QUERY_FIELD_FUNCTION;
		found = TRUE;
	}

	if ((!found) && (*type == 'v') && (*(type+1) == 'a')) { 
		field_type = QUERY_FIELD_VALUE;
		found = TRUE;
	}

	if ((!found) && (*type == 'q') && (*(type+1) == 'u') && (strlen(type)==5)) { 
		field_type = QUERY_FIELD_QUERY;
		found = TRUE;
	}

	if ((!found) && (*type == 'q') && (*(type+1) == 'u')) { 
		field_type = QUERY_FIELD_QUERY_FIELD;
		found = TRUE;
	}


	if (found) 
		obj = query_field_new (q, name, field_type);
	else
		g_warning ("QueryField'type not found at %s line %d\n", __FILE__, __LINE__);

	return obj;
}

/* creates a QueryField which is a copy of the one given in arg */
GtkObject *
query_field_new_copy (QueryField *field)
{
	GtkObject *obj;
	QueryField *newfield;
	QueryFieldIface *iface;

	g_return_val_if_fail (field != NULL, NULL);
       
	obj = query_field_new (field->query, field->name, field->field_type);
	newfield = QUERY_FIELD (obj);

	iface = find_interface (newfield);
	g_assert (iface != NULL);

	(* iface->destroy)(newfield);
	(* iface->init)(newfield);
	(* iface->copy_other_field)(newfield, field);

	/* object's name and print name */
	if (field->name)
		query_field_set_name (newfield, field->name);
	if (field->alias)
		query_field_set_alias (newfield, field->alias);
	return obj;
}

void
query_field_use_ref (QueryField *field)
{
	g_return_if_fail (field);
	g_return_if_fail (IS_QUERY_FIELD (field));

	field->ref_counter++;
}

void
query_field_free_ref (QueryField *field)
{
	g_return_if_fail (field);
	g_return_if_fail (IS_QUERY_FIELD (field));

	g_print ("query_field_free_ref (%p)\n", field);

	if (! field->ref_counter) {
		g_warning ("Reference counter for QueryField  %p is already 0!", field);
		return;
	}

	field->ref_counter--;

	if (! field->ref_counter) {
		if (!field->name ||
		    (field->name && (*(field->name)==0)) ||
		    !field->is_printed ) {
			g_print ("Query field %p destroyed because ref dropped to 0!!", field);
			gtk_object_destroy (GTK_OBJECT (field));
		}
	}
}

static GSList *copy_recurs (QueryField *field, QueryField *user, GHashTable *hash, GSList *objects);

GtkObject *
query_field_new_copy_all   (QueryField *field, GSList **list)
{
	GSList *nlist, *fields, *last=NULL;
	GHashTable *hash;

	g_assert (list);

	if (*list) {
		nlist = *list;
		last = g_slist_last (*list);
	}
	else
		nlist = NULL;
	hash = g_hash_table_new (NULL, NULL);

	nlist = copy_recurs (field, NULL, hash, nlist);
	g_hash_table_destroy (hash);
	
	*list = nlist;

	fields = nlist;
	while (fields) {
		gtk_object_set_data (GTK_OBJECT (fields->data), "qf_list", list);
		fields = g_slist_next (fields);
	}

	if (nlist) {
		if (last) {
			if (g_slist_next (last))
				return GTK_OBJECT (g_slist_next (last)->data);
			else {
				g_warning ("Copy of QueryField did not create a new QueryField!");
				return GTK_OBJECT (nlist->data);
			}
		}		   
		else
			return GTK_OBJECT (nlist->data);
	}
	else
		return NULL;
}

static GSList *
copy_recurs (QueryField *field, QueryField *user, GHashTable *hash, GSList *objects)
{
	QueryField *qf;
	gpointer ptr;
	GSList *list, *objs;
	
	g_print ("COPY RECURS copying %p (user=%p)\n", field, user);
	list = objects;
	/* we check if field has been copied and marked as such in the hash table,
	   or we create a new one, copy of field */
	if ((ptr = g_hash_table_lookup (hash, field))) 
		qf = QUERY_FIELD (ptr);
	else {
		qf = QUERY_FIELD (query_field_new_copy (field));
		list = g_slist_append (list, qf);

		/* if the qf has a name, then we don't want a duplicate and so we
		   put it into the hash table for future lookup */
		if (field->name && *(field->name)) {
			/* insert into hash table */
			g_hash_table_insert (hash, field, qf);
		}
	}

	/* replace the reference in the calling object (user) if any */
	if (user) {
		gtk_object_set_data (GTK_OBJECT (user), "qf_list", &list);		
		query_field_replace_ref_ptr (user, GTK_OBJECT (field), GTK_OBJECT (qf));
	}
	
	/* apply this function recursively to the monitored objects if they are
	 QueryFields */
	objs = query_field_get_monitored_objects (field);
	while (objs) {
		if (IS_QUERY_FIELD (objs->data)) 
			list = copy_recurs (QUERY_FIELD (objs->data), qf, hash, list);
		objs = g_slist_next (objs);
	}
	g_slist_free (objs);

	return list;
}

void 
query_field_copy_object (QueryField *dest, QueryField *orig)
{
	QueryFieldIface *iface1, *iface2;
	gboolean types = FALSE, name = FALSE, alias = FALSE;

	g_return_if_fail (dest != NULL);
	g_return_if_fail (orig != NULL);

	/* finding interfaces */
	iface1 = find_interface (orig);
	iface2 = find_interface (dest);
	g_assert (iface1 != NULL);
	g_assert (iface2 != NULL);

	/* keep a memory of which changes will occur to emit
	   the right signals */
	if (orig->field_type != dest->field_type)
		types = TRUE;
	if (strcmp (orig->name, dest->name))
		name = TRUE;
	if (strcmp (orig->alias, dest->alias))
		alias = TRUE;

	/* freeing any memory, etc that was used by this object */
	(* iface2->destroy)(dest);
	(* iface2->init)(dest);

	/* copying the other field */
	(* iface1->copy_other_field)(dest, orig);

	/* copy the name and alias */
	if (dest->name) {
		g_free (dest->name);
		dest->name = NULL;
	}

	/* FIXME: use the right methods to do this */
	if (orig->name)
		dest->name = g_strdup (orig->name);
	if (dest->alias) {
		g_free (dest->alias);
		dest->alias = NULL;
		dest->is_printed = FALSE;
	}
	if (orig->alias) {
		dest->alias = g_strdup (orig->alias);
		dest->is_printed = TRUE;
	}

	/* emit necessary signals */
	if (name) {
#ifdef debug_signal
		g_print (">> 'NAME_CHANGED' from query_field_copy_object\n");
#endif
		gtk_signal_emit (GTK_OBJECT (dest),
				 query_field_signals[NAME_CHANGED]);
#ifdef debug_signal
		g_print ("<< 'NAME_CHANGED' from query_field_copy_object\n");
#endif	
	}
	if (alias) {
#ifdef debug_signal
		g_print (">> 'ALIAS_CHANGED' from query_field_copy_object\n");
#endif
		gtk_signal_emit (GTK_OBJECT (dest),
				 query_field_signals[ALIAS_CHANGED]);
#ifdef debug_signal
		g_print ("<< 'ALIAS_CHANGED' from query_field_copy_object\n");
#endif	
	}
	if (types) {
#ifdef debug_signal
		g_print (">> 'FIELD_TYPE_CHANGED' from query_field_copy_object\n");
#endif
		gtk_signal_emit (GTK_OBJECT (dest),
				 query_field_signals[FIELD_TYPE_CHANGED]);
#ifdef debug_signal
		g_print ("<< 'FIELD_TYPE_CHANGED' from query_field_copy_object\n");
#endif	
	}

#ifdef debug_signal
	g_print (">> 'FIELD_MODIFIED' from query_field_copy_object\n");
#endif
	gtk_signal_emit (GTK_OBJECT (dest),
			 query_field_signals[FIELD_MODIFIED]);
#ifdef debug_signal
	g_print ("<< 'FIELD_MODIFIED' from query_field_copy_object\n");
#endif	
}

gboolean
query_field_is_equal (QueryField *field, QueryField *other)
{
	QueryFieldIface *iface;

	if (other->field_type != field->field_type)
		return FALSE;

	iface = find_interface (field);
	g_assert (iface != NULL);
	return (* iface->is_equal_to)(field, other);
}

static QueryFieldIface *
find_interface (QueryField *qf)
{
	GSList *list;
	QueryFieldIface *retval = NULL;

	g_assert (qf);
	g_assert (IS_QUERY_FIELD (qf));

	g_assert (query_field_class != NULL);
	list = 	QUERY_FIELD_CLASS(query_field_class)->field_types;

	while (list && !retval) {
		if (((QueryFieldIface *)(list->data))->field_type == qf->field_type)
			retval = (QueryFieldIface *)(list->data);
		list = g_slist_next (list);
	}

	return retval;
}


static void 
query_field_destroy (GtkObject * object)
{
	QueryFieldIface *iface;
	QueryField *qf;

	g_return_if_fail (object != NULL);
        g_return_if_fail (IS_QUERY_FIELD (object));

	qf = QUERY_FIELD (object);

	iface = find_interface (qf);
	g_assert (iface != NULL);
	(* iface->destroy)(qf);

	if (qf->name) {
		g_free (qf->name);
		qf->name = NULL;
	}

	if (qf->alias) {
		g_free (qf->alias);
		qf->alias = NULL;
		qf->is_printed = FALSE;
	}
		

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
                (*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


void 
query_field_set_name (QueryField * qf, gchar * name)
{
	gboolean changed = FALSE;
	g_return_if_fail (qf && IS_QUERY_FIELD (qf));

	if (name) {
		if (qf->name && !strcmp (qf->name, name))
			return;
		else { /* needs replacement */
			gchar *str;
			
			str = g_strdup (name);

			/* look for name duplication */
			if (qf->query && g_slist_find (qf->query->fields, qf)) {
				guint i = 1;
				GSList *list;
				
				list = qf->query->fields;
				while (list) {
					if (QUERY_FIELD (list->data)->name &&
					    !strcmp (str, QUERY_FIELD (list->data)->name)) {
						g_free (str);
						str = g_strdup_printf ("%s_%d", name, i++);
						list = qf->query->fields;
					}
					else
						list = g_slist_next (list);
				}
			}
				
			if (qf->name)
				g_free (qf->name);
			qf->name = str;
			changed = TRUE;
		}
	}
	else {
		if (qf->name) {
			g_free (qf->name);
			qf->name = NULL;
			changed = TRUE;
		}
	}

	if (changed) {
#ifdef debug_signal
		g_print (">> 'NAME_CHANGED' from query_field_set_name\n");
#endif
		gtk_signal_emit (GTK_OBJECT (qf),
				 query_field_signals[NAME_CHANGED]);
#ifdef debug_signal
		g_print ("<< 'NAME_CHANGED' from query_field_set_name\n");
#endif
		g_print ("NAME SET to %s\n", qf->name);
	}
}

void 
query_field_set_alias (QueryField * qf, gchar * alias)
{
	gboolean changed = FALSE;
	g_return_if_fail (qf && IS_QUERY_FIELD (qf));

	if (alias) {
		if (qf->alias && !strcmp (qf->alias, alias))
			return;
		else { /* needs replacement */
			gchar *str;
			
			str = g_strdup (alias);

			/* look for name duplication */
			if (qf->query && g_slist_find (qf->query->fields, qf)) {
				guint i = 1;
				GSList *list;
				
				list = qf->query->fields;
				while (list) {
					if (QUERY_FIELD (list->data)->alias &&
					    !strcmp (str, QUERY_FIELD (list->data)->alias)) {
						g_free (str);
						str = g_strdup_printf ("%s_%d", alias, i++);
						list = qf->query->fields;
					}
					else
						list = g_slist_next (list);
				}
			}
				
			if (qf->alias)
				g_free (qf->alias);
			qf->is_printed = TRUE;
			qf->alias = str;

			changed = TRUE;
		}
	}
	else {
		if (qf->alias) {
			g_free (qf->alias);
			qf->alias = NULL;
			qf->is_printed = FALSE;
			changed = TRUE;
		}
	}

	if (changed) {
#ifdef debug_signal
		g_print (">> 'ALIAS_CHANGED' from query_field_set_alias\n");
#endif
		gtk_signal_emit (GTK_OBJECT (qf),
				 query_field_signals[ALIAS_CHANGED]);
#ifdef debug_signal
		g_print ("<< 'ALIAS_CHANGED' from query_field_set_alias\n");
#endif		
		g_print ("ALIAS SET to %s\n", qf->alias);
	}
}

void
query_field_set_is_printed (QueryField * qf, gboolean is_printed)
{
	gboolean changed = FALSE;
	g_return_if_fail (qf && IS_QUERY_FIELD (qf));

	if (is_printed != qf->is_printed) {
		changed = TRUE;
		if (qf->is_printed) {
			if (qf->alias) {
				g_free (qf->alias);
				qf->alias = NULL;
			}
			qf->is_printed = FALSE;
		}
		else {
			if (qf->name)
				qf->alias = g_strdup (qf->name);
			else
				qf->alias = g_strdup ("");
			qf->is_printed = TRUE;
		}
	}

	if (changed) {
#ifdef debug_signal
		g_print (">> 'ALIAS_CHANGED' from query_field_set_is_printed\n");
#endif
		gtk_signal_emit (GTK_OBJECT (qf),
				 query_field_signals[ALIAS_CHANGED]);
#ifdef debug_signal
		g_print ("<< 'ALIAS_CHANGED' from query_field_set_is_alias\n");
#endif		
		g_print ("ALIAS SET to %s\n", qf->alias);
	}
}

void       
query_field_set_id (QueryField * qf, guint id)
{
	g_return_if_fail (qf && IS_QUERY_FIELD (qf));
	g_return_if_fail (qf->query);

	qf->id = query_get_unique_field_id (qf->query, id);
}

void 
query_field_set_activated (QueryField *qf, gboolean activated)
{
	g_return_if_fail (qf && IS_QUERY_FIELD (qf));

	if (qf->activated != activated) {
		qf->activated = activated;
		if (activated)
			g_print ("\tACTIVATED\n");
		else
			g_print ("\tNON ACTIVATED\n");
#ifdef debug_signal
		g_print (">> 'STATUS_CHANGED' from query_field_set_activated\n");
#endif
		gtk_signal_emit (GTK_OBJECT (qf),
				 query_field_signals[STATUS_CHANGED]);
#ifdef debug_signal
		g_print ("<< 'STATUS_CHANGED' from query_field_set_activated\n");
#endif
	}
}

void 
query_field_deactivate (QueryField * qf)
{
	QueryFieldIface *iface;

	g_return_if_fail (qf && IS_QUERY_FIELD (qf));

	iface = find_interface (qf);
	g_assert (iface != NULL);
	(* iface->deactivate)(qf);

	query_field_set_activated (qf, FALSE);
}

void 
query_field_activate (QueryField * qf)
{
	QueryFieldIface *iface;

	g_return_if_fail (qf && IS_QUERY_FIELD (qf));

	iface = find_interface (qf);
	g_assert (iface != NULL);
	(* iface->activate)(qf);
}


GtkWidget * 
query_field_get_edit_widget (QueryField *qf)
{
	QueryFieldIface *iface;

	g_return_val_if_fail (qf && IS_QUERY_FIELD (qf), NULL);

	iface = find_interface (qf);
	g_assert (iface != NULL);
	return (* iface->get_edit_widget)(qf);
}

GtkWidget * 
query_field_get_select_widget (QueryField *qf, GtkSignalFunc func, gpointer data)
{
	QueryFieldIface *iface;

	g_return_val_if_fail (!qf || (qf && IS_QUERY_FIELD (qf)), NULL);

	if (qf && IS_QUERY_FIELD (qf)) {
		iface = find_interface (qf);
		g_assert (iface != NULL);
		return (* iface->get_sel_widget)(qf, func, data);
	}
	else { 
		/* default "???" buton */
		GtkWidget *button;

		button = gtk_button_new_with_label (_("???"));
		gtk_signal_connect (GTK_OBJECT (button), "clicked",
				    func, data);
		gtk_object_set_data (GTK_OBJECT (button), "qf", qf);

		/* Set the "QF_obj_emit_sig" attribute so that we can attach attributes to that button
		   which will be transmitted when the user clicks on it */
		gtk_object_set_data (GTK_OBJECT (button), "QF_obj_emit_sig", button);

		return button;
	}
}
	
gchar     * 
query_field_render_as_sql   (QueryField *qf, GSList * missing_values)
{
	QueryFieldIface *iface;

	g_return_val_if_fail (qf && IS_QUERY_FIELD (qf), NULL);

	iface = find_interface (qf);
	g_assert (iface != NULL);
	return (* iface->render_as_sql)(qf, missing_values);
}

xmlNodePtr  
query_field_render_as_xml   (QueryField *qf, GSList * missing_values)
{
	QueryFieldIface *iface;

	g_return_val_if_fail (qf && IS_QUERY_FIELD (qf), NULL);

	iface = find_interface (qf);
	g_assert (iface != NULL);
	return (* iface->render_as_xml)(qf, missing_values);
}

gchar     * 
query_field_render_as_string(QueryField *qf, GSList * missing_values)
{
	QueryFieldIface *iface;

	g_return_val_if_fail (qf && IS_QUERY_FIELD (qf), NULL);

	iface = find_interface (qf);
	g_assert (iface != NULL);
	return (* iface->render_as_string)(qf, missing_values);
}

xmlNodePtr  
query_field_save_to_xml     (QueryField *qf)
{
	QueryFieldIface *iface;
	xmlNodePtr node;
	gchar *str;

	g_return_val_if_fail (qf && IS_QUERY_FIELD (qf), NULL);

	iface = find_interface (qf);
	g_assert (iface != NULL);

	/* node creation */
	node = xmlNewNode (NULL, "QueryField");
	if (qf->name)
		xmlSetProp (node, "name", qf->name);
	if (qf->is_printed) {
		xmlSetProp (node, "is_printed", "t");
		if (qf->alias && *(qf->alias))
			xmlSetProp (node, "alias", qf->alias);
	}
	else
		xmlSetProp (node, "is_printed", "f");
	
	str = query_field_get_xml_id (qf);
	xmlSetProp (node, "id", str);
	g_free (str);

	/* node filling */
	(* iface->save_to_xml)(qf, node);
	/* FIXME */
	return node;
}

gchar * 
query_field_get_xml_id      (QueryField *qf)
{
	gchar *str;
	g_return_val_if_fail (qf && IS_QUERY_FIELD (qf), NULL);
	
	str = g_strdup_printf ("QU%d:QF%d", qf->query->id, qf->id);
	
	return str;
}

void        
query_field_load_from_xml   (QueryField *qf, xmlNodePtr node)
{
	QueryFieldIface *iface;
	gchar *str;
	g_return_if_fail (qf && IS_QUERY_FIELD (qf));

	iface = find_interface (qf);
	g_assert (iface != NULL);

	str = xmlGetProp (node, "name");
	if (str) {
		query_field_set_name (qf, str);
		g_free (str);
	}
	str = xmlGetProp (node, "is_printed");
	if (str) {
		if (*str == 't') {
			gchar *str2;
			str2 = xmlGetProp (node, "alias");
			if (str2) {
				query_field_set_alias (qf, str2);
				g_free (str2);
			}
			else
				query_field_set_is_printed (qf, TRUE);
		}
		else {
			query_field_set_is_printed (qf, FALSE);
		}
		g_free (str);
	}
	str = xmlGetProp (node, "id");
	if (str) {
		gchar *ptr;
		ptr = strtok (str, ":");
		ptr = strtok (NULL, ":");
		query_field_set_id (qf, atoi (ptr+2));
		g_free (str);
	}
	
	(* iface->load_from_xml)(qf, node);
}


GSList *
query_field_get_monitored_objects (QueryField *qf)
{
	QueryFieldIface *iface;
	g_return_val_if_fail (qf && IS_QUERY_FIELD (qf), NULL);

	iface = find_interface (qf);
	g_assert (iface != NULL);
	return (* iface->get_monitored_objects)(qf);
}

void 
query_field_replace_ref_ptr (QueryField *qf, GtkObject *old, GtkObject *new)
{
	QueryFieldIface *iface;
	g_return_if_fail (qf && IS_QUERY_FIELD (qf));

	/* is the ptr to be raplaced the one to Query ? */
	if ((GTK_OBJECT (qf->query) == old) &&
	    (new && IS_QUERY (new))) {
		qf->query = QUERY (new);
		return;
	}

	iface = find_interface (qf);
	g_assert (iface != NULL);
	return (* iface->replace_comp)(qf, -1, old, new);
}

void 
query_field_replace_ref_int (QueryField *qf, gint ref, QueryField *new)
{
	QueryFieldIface *iface;
	g_return_if_fail (qf && IS_QUERY_FIELD (qf));

	iface = find_interface (qf);
	g_assert (iface != NULL);
	return (* iface->replace_comp)(qf, ref, NULL, GTK_OBJECT (new));
}
