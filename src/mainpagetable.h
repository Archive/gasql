/* mainpagetable.h
 *
 * Copyright (C) 1999 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __MAIN_PAGE_TABLE__
#define __MAIN_PAGE_TABLE__

#include <gnome.h>
#include "database.h"
#include "conf-manager.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define MAIN_PAGE_TABLE(obj)          GTK_CHECK_CAST (obj, main_page_table_get_type(), MainPageTable)
#define MAIN_PAGE_TABLE_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, main_page_table_get_type (), MainPageTableClass)
#define IS_MAIN_PAGE_TABLE(obj)       GTK_CHECK_TYPE (obj, main_page_table_get_type ())


	typedef struct _MainPageTable MainPageTable;
	typedef struct _MainPageTableClass MainPageTableClass;


	/* struct for the object's data */
	struct _MainPageTable
	{
		GtkVBox object;

		ConfManager *conf;
		GtkWidget *clist;	/* the GtkCList object */
		GtkWidget *remove_table;
		GtkWidget *edit_table;
		GtkWidget *view_table;
		GtkWidget *new_table;
		gint sel_row;
	};

	/* struct for the object's class */
	struct _MainPageTableClass
	{
		GtkVBoxClass parent_class;
	};

	/* generic widget's functions */
	guint main_page_table_get_type (void);
	GtkWidget *main_page_table_new (ConfManager * conf);


	/* to be used by the TableEdit object */
	void main_page_table_set_data_dlg (MainPageTable * mpt, guint row,
					   GtkWidget * dlg);
	GtkWidget *main_page_table_get_data_dlg (MainPageTable * mpt,
						 guint row);
	gint main_page_table_get_row (MainPageTable * mpt,
				      DbTable * table);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
