/* serveraccess.h
 *
 * Copyright (C) 1999 - 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


/*
 * Implementation of the objects here is in two files:
 *    serveraccess.c
 *    sqldata.c
 */

#ifndef __SERVER_ACCESS__
#define __SERVER_ACCESS__

#include <config.h>
#include <gnome.h>
#include "server-rs.h"
#include <gda-connection.h>
#include <gda-command.h>
#include "sqldatadisplay.h"
#include "gasql_xml.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

	typedef struct _ServerAccess ServerAccess;
	typedef struct _ServerAccessClass ServerAccessClass;
	typedef struct _ServerAccessItemType ServerAccessItemType;

	typedef struct _ServerDataType ServerDataType;
	typedef struct _ServerDataTypeClass ServerDataTypeClass;

	typedef struct _ServerFunction ServerFunction;
	typedef struct _ServerFunctionClass ServerFunctionClass;

	typedef struct _SqlDataOperator SqlDataOperator;
	typedef struct _SqlDataOperatorClass SqlDataOperatorClass;

	typedef struct _ServerAggregate ServerAggregate;
	typedef struct _ServerAggregateClass ServerAggregateClass;

/*
 * 
 * ServerAccess object
 *
 */
#define SERVER_ACCESS(obj)          GTK_CHECK_CAST (obj, server_access_get_type(), ServerAccess)
#define SERVER_ACCESS_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, server_access_get_type (), ServerAccessClass)
#define IS_SERVER_ACCESS(obj)       GTK_CHECK_TYPE (obj, server_access_get_type ())


	typedef struct
	{
		gboolean sequences;
		gboolean procs;
		gboolean inheritance;
		gboolean xml_queries;
	}
	GdaConnection_Supports;

	/* struct for the object's data */
	struct _ServerAccess
	{
		GdaConnection object;

		/* serveur description */
		gchar *description;
		GdaConnection_Supports features;

		/* SQL connectivity */
		GString *gda_datasource;
		GString *user_name;
		GString *password;

		/* Data types and stuff */
		GSList *data_types;
		GSList *data_functions;
		GSList *data_aggregates;
		GSList *data_types_display;	/* list of SqlDataDisplayFns structs */
		GHashTable *types_objects_hash;	/* hash to store the bindings of Display 
						   fns to several objects */
		GSList *bindable_objects;	/* list the functions called by 
						   server_access_get_object_display_fns() */

		/* Queries execution */
		GdaCommand *cmd;
	};

	/* struct for the object's class */
	struct _ServerAccessClass
	{
		GdaConnectionClass parent_class;

		void (*conn_opened) (ServerAccess * srv);
		void (*conn_to_close) (ServerAccess * srv);	/* connexion about to be closed */
		void (*conn_closed) (ServerAccess * srv);
		/* data types, functions, operators and aggregates signals */
		void (*data_types_updated) (ServerAccess * srv);
		void (*data_function_added) (ServerAccess * srv,
					     ServerFunction * func);
		void (*data_function_removed) (ServerAccess * srv,
					       ServerFunction * func);
		void (*data_function_updated) (ServerAccess * srv);
		void (*data_aggregate_added) (ServerAccess * srv,
					      ServerAggregate * func);
		void (*data_aggregate_removed) (ServerAccess * srv,
						ServerAggregate * func);
		void (*data_aggregate_updated) (ServerAccess * srv);
		void (*progress) (ServerAccess * srv, gchar * msg, guint now,
				  guint total);
		/* for the objects to plugins bindings */
		void (*objects_bindings_updated) (ServerAccess * srv);
	};

	/*
	 * generic widget's functions
	 */
	GtkType server_access_get_type (void);
	GtkObject *server_access_new (CORBA_ORB orb);
	void server_access_free (ServerAccess * srv);

	/*
	 * open/close and query functions
	 */
	void server_access_open_connect (ServerAccess * srv);
	gboolean server_access_is_open (ServerAccess * srv);
	void server_access_close_connect (ServerAccess * srv);
	void server_access_close_connect_no_warning (ServerAccess * srv);
	ServerResultset *server_access_do_query (ServerAccess * srv, gchar * query);

	/*
	 * data type, etc lookup and management
	 */
	void server_access_refresh_datas (ServerAccess * srv);
	gchar *server_access_get_data_type (ServerAccess * srv, gchar * oid);
	GList *server_access_get_data_type_list (ServerAccess * srv);	/* to free */
	ServerDataType *server_access_get_type_from_name (ServerAccess * srv,
						    gchar * name);
	ServerDataType *server_access_get_type_from_oid (ServerAccess * srv,
						   gchar * oid);

	/*
	 * XML functions
	 */
	void server_access_build_xml_tree (ServerAccess * srv, xmlDocPtr doc);
	gboolean server_access_build_from_xml_tree (ServerAccess * srv,
						 xmlNodePtr tree);

	/*
	 * plugins management
	 */
	SqlDataDisplayFns *server_access_get_display_fns_from_gda (ServerAccess *
								srv,
								ServerDataType *
								dt);
	/* returns the display functions associated with an object 
	   (ServerDataType objects get their onw default data functions in case 
	   they are non overloaded) Cannot return NULL! */
	SqlDataDisplayFns *server_access_get_object_display_fns (ServerAccess * srv,
							      GtkObject *
							      obj);
	void server_access_bind_object_display (ServerAccess * srv, GtkObject * obj,
					     SqlDataDisplayFns * fns);
	void server_access_unbind_object_display (ServerAccess * srv,
					       GtkObject * obj);
	void server_access_rescan_display_plugins (ServerAccess * srv,
						gchar * path);
	/* this function allows modules to give another object to look for in the 
	   hash table, given an object. For example if the object is a 
	   DbTableField, then the 'func' function will return the 
	   data type of the field (this is set by the Database module) */
	void server_access_declare_object_bindable (ServerAccess * srv,
						 gpointer (*func) (GtkObject
								   *));



/*
 *
 * ServerDataType object
 *
 */

#define SERVER_DATA_TYPE(obj)          GTK_CHECK_CAST (obj, server_data_type_get_type(), ServerDataType)
#define SERVER_DATA_TYPE_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, server_data_type_get_type (), ServerDataTypeClass)
#define IS_SERVER_DATA_TYPE(obj)       GTK_CHECK_TYPE (obj, server_data_type_get_type ())


	/* struct for the object's data */
	struct _ServerDataType
	{
		GtkObject object;

		gchar *descr;
		gchar *sqlname;
		guint numparams;
		gint server_type;	/* provider representation for the type */
		GDA_ValueType gda_type;

		/* Types display implementation */
		SqlDataDisplayFns *display_fns;
		gboolean updated;
	};

	/* struct for the object's class */
	struct _ServerDataTypeClass
	{
		GtkObjectClass parent_class;
	};

	/*
	 * generic widget's functions
	 */
	GtkType server_data_type_get_type (void);
	GtkObject *server_data_type_new (void);

	/*
	 * data types management
	 */
	void server_data_type_set_sqlname (ServerDataType * dt, gchar * name);
	void server_data_type_set_descr (ServerDataType * dt, gchar * name);
	void server_data_type_update_list (struct _ServerAccess *srv);
#ifdef debug
	void server_data_type_show_types (GSList * dtl);
#endif

	/* 
	 * data types lookup
	 */
	ServerDataType *server_data_type_get_from_server_type (GSList * dtl,
							 gint st);
	ServerDataType *server_data_type_get_from_name (GSList * dtl, gchar * name);
	GList *server_data_type_get_name_list (GSList * dtl);
	ServerDataType *server_data_type_get_from_xml_id (struct _ServerAccess *srv,
						    gchar * id);



/*
 *
 * ServerFunction object
 *
 */

#define SERVER_FUNCTION(obj)          GTK_CHECK_CAST (obj, server_function_get_type(), ServerFunction)
#define SERVER_FUNCTION_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, server_function_get_type (), ServerFunctionClass)
#define IS_SERVER_FUNCTION(obj)       GTK_CHECK_TYPE (obj, server_function_get_type ())

	/* struct for the object's data */
	struct _ServerFunction
	{
		GtkObject object;

		gchar *descr;
		gchar *sqlname;
		gchar *objectid;	/* unique id for the function */
		ServerDataType *result_type;
		GSList *args;	/* ServerDataType list */
		gboolean is_user;	/* TRUE  if user defined function */
		gboolean updated;
	};

	/* struct for the object's class */
	struct _ServerFunctionClass
	{
		GtkObjectClass parent_class;
	};

	/*
	 * generic widget's functions 
	 */
	GtkType server_function_get_type (void);
	GtkObject *server_function_new (void);

	/*
	 * function management
	 */
	void server_function_set_sqlname (ServerFunction * df,
					    gchar * name);
	void server_function_set_descr (ServerFunction * df, gchar * name);
	void server_function_update_list (struct _ServerAccess *srv);
#ifdef debug
	void server_function_show_functions (GSList * dfl);
#endif

	/* 
	 * functions lookup
	 */
	GSList *server_function_get_list_from_name (GSList * dfl,
						      gchar * name);
	/* argtypes is a list of ServerDataTypes in the right order */
	ServerFunction *server_function_get_from_name (GSList * dfl,
							  gchar * name,
							  GSList * argtypes);
	ServerFunction *server_function_get_from_objid (GSList * dfl,
							   gchar * id);
	ServerFunction *server_function_get_from_xml_id (struct _ServerAccess
							    *srv, gchar * id);
	/* function to bind for data types and plugins usage */
	gpointer server_function_binding_func (GtkObject * obj);

/*
 *
 * ServerAggregate object
 *
 */

#define SERVER_AGGREGATE(obj)          GTK_CHECK_CAST (obj, server_aggregate_get_type(), ServerAggregate)
#define SERVER_AGGREGATE_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, server_aggregate_get_type (), ServerAggregateClass)
#define IS_SERVER_AGGREGATE(obj)       GTK_CHECK_TYPE (obj, server_aggregate_get_type ())


	/* struct for the object's data */
	struct _ServerAggregate
	{
		GtkObject object;

		gchar *descr;
		gchar *sqlname;
		gchar *objectid;
		ServerDataType *arg_type;
		gboolean updated;
	};

	/* struct for the object's class */
	struct _ServerAggregateClass
	{
		GtkObjectClass parent_class;
	};

	/*
	 * generic widget's aggregates 
	 */
	GtkType server_aggregate_get_type (void);
	GtkObject *server_aggregate_new (void);

	/*
	 * aggregate management
	 */
	void server_aggregate_set_sqlname (ServerAggregate * da,
					     gchar * name);
	void server_aggregate_set_descr (ServerAggregate * da,
					   gchar * name);
	void server_aggregate_update_list (struct _ServerAccess *srv);
#ifdef debug
	void server_aggregate_show_aggregates (GSList * dal);
#endif


	/*
	 * aggregates lookup
	 */
	ServerAggregate *server_aggregate_get_from_name (GSList * dal,
							    gchar * name,
							    ServerDataType *
							    arg_type);
	ServerAggregate *server_aggregate_get_from_objid (GSList * dal,
							     gchar * oid);
	ServerAggregate *server_aggregate_get_from_xml_id (struct
							      _ServerAccess *srv,
							      gchar * id);


#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
