#ifndef _GLADE_DEFS_H
#define _GLADE_DEFS_H

typedef struct _GladeForm GladeFormStruct;

struct _GladeForm {
	gchar *glade_file_name;
	gchar *top_widget_name;
	GladeXML *gxml;
};

#endif
