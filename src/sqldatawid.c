/* sqldatawid.c
 *
 * Copyright (C) 1999,2000 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "sqldatawid.h"

static void sql_data_wid_class_init (SqlDataWidClass * class);
static void sql_data_wid_init (SqlDataWid * dw);
static void sql_data_wid_destroy (GtkObject * object);

/*
 * static functions for the widget
 */
static void data_info_display_cb (GtkWidget * wid,
				  gint row, gint column,
				  GdkEventButton * event, SqlDataWid * dw);
static void user_defined_functions_toggled_cb (GtkWidget * wid,
					       SqlDataWid * dw);
static void system_defined_functions_toggled_cb (GtkWidget * wid,
						 SqlDataWid * dw);
static void function_info_display_cb (GtkWidget * wid,
				      gint row, gint column,
				      GdkEventButton * event,
				      SqlDataWid * dw);
static void aggregate_info_display_cb (GtkWidget * wid,
				       gint row, gint column,
				       GdkEventButton * event,
				       SqlDataWid * dw);
static void function_add_cb (GtkObject * obj, ServerFunction * df,
			     SqlDataWid * dw);
static void function_del_cb (GtkObject * obj, ServerFunction * df,
			     SqlDataWid * dw);
static void aggregate_add_cb (GtkObject * obj, ServerAggregate * da,
			      SqlDataWid * dw);
static void aggregate_del_cb (GtkObject * obj, ServerAggregate * da,
			      SqlDataWid * dw);
static void populate_data_types (SqlDataWid * dw);
static void populate_functions (SqlDataWid * dw);
static void populate_aggregates (SqlDataWid * dw);

/* get a pointer to the parents to be able to call their destructor */
static GtkObject *parent_class = NULL;

guint
sql_data_wid_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Sql_Data_Wid",
			sizeof (SqlDataWid),
			sizeof (SqlDataWidClass),
			(GtkClassInitFunc) sql_data_wid_class_init,
			(GtkObjectInitFunc) sql_data_wid_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_vbox_get_type (), &f_info);
	}

	return f_type;
}

static void
sql_data_wid_class_init (SqlDataWidClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	parent_class = gtk_type_class (gtk_vbox_get_type ());
	object_class->destroy = sql_data_wid_destroy;
}

static void
sql_data_wid_init (SqlDataWid * dw)
{
	dw->sel_func = NULL;
	dw->sel_type = NULL;
	dw->sel_agg = NULL;
	dw->mode = 1;
}

static void
sql_data_wid_post_init (SqlDataWid * dw)
{
	GtkWidget *clist;
	GtkWidget *sw, *text;
	GtkWidget *vb, *wid, *hb;
	gchar *titles[] = { N_("Type"), N_("Description") };
	gchar *titles2[] = { N_("Function"), N_("Description") };
	gchar *titles3[] = { N_("Aggregate"), N_("Description") };
	GdkColor red_color, *rcolor;
	GdkColormap *colormap;

	/* notebook */
	wid = gtk_notebook_new ();
	gtk_box_pack_start (GTK_BOX (dw), wid, TRUE, TRUE, 0);
	gtk_notebook_set_tab_pos (GTK_NOTEBOOK (wid), GTK_POS_TOP);
	gtk_widget_show (wid);
	dw->notebook = wid;

	/* notebook Data Types page */
	wid = gtk_label_new (_("Data types"));
	gtk_widget_show (wid);

	vb = gtk_vbox_new (FALSE, GNOME_PAD);
	gtk_notebook_append_page (GTK_NOTEBOOK (dw->notebook), vb, wid);
	gtk_widget_show (vb);

	wid = gtk_label_new (_("List of types (select one):"));
	gtk_box_pack_start (GTK_BOX (vb), wid, FALSE, TRUE, GNOME_PAD / 2);
	gtk_widget_show (wid);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_box_pack_start (GTK_BOX (vb), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	clist = gtk_clist_new_with_titles (2, titles);
	gtk_clist_set_selection_mode (GTK_CLIST (clist),
				      GTK_SELECTION_SINGLE);
	gtk_clist_set_reorderable (GTK_CLIST (clist), FALSE);
	gtk_container_add (GTK_CONTAINER (sw), clist);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 0, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 1, TRUE);
	gtk_clist_column_titles_passive (GTK_CLIST (clist));
	gtk_signal_connect (GTK_OBJECT (clist), "select_row",
			    GTK_SIGNAL_FUNC (data_info_display_cb), dw);
	gtk_widget_show (clist);
	dw->typesclist = clist;

	wid = gtk_label_new (_("Information on the selected type:"));
	gtk_box_pack_start (GTK_BOX (vb), wid, FALSE, TRUE, 0);
	gtk_widget_show (wid);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_box_pack_start (GTK_BOX (vb), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	text = gtk_text_new (NULL, NULL);
	gtk_text_set_editable (GTK_TEXT (text), FALSE);
	gtk_text_set_word_wrap (GTK_TEXT (text), FALSE);
	gtk_container_add (GTK_CONTAINER (sw), text);
	gtk_widget_show (text);
	dw->typestext = text;

	/* notebook Functions page */
	wid = gtk_label_new (_("Functions"));
	gtk_widget_show (wid);

	vb = gtk_vbox_new (FALSE, GNOME_PAD);
	gtk_notebook_append_page (GTK_NOTEBOOK (dw->notebook), vb, wid);
	gtk_widget_show (vb);

	hb = gtk_hbox_new (TRUE, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (vb), hb, FALSE, TRUE, 0);
	gtk_widget_show (hb);

	wid = gtk_check_button_new_with_label (_("User defined functions"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (wid), TRUE);
	gtk_signal_connect (GTK_OBJECT (wid), "toggled",
			    GTK_SIGNAL_FUNC
			    (user_defined_functions_toggled_cb), dw);
	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, TRUE, GNOME_PAD / 2);
	gtk_widget_show (wid);

	wid = gtk_check_button_new_with_label (_("System functions"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (wid), FALSE);
	gtk_signal_connect (GTK_OBJECT (wid), "toggled",
			    GTK_SIGNAL_FUNC
			    (system_defined_functions_toggled_cb), dw);
	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, TRUE, GNOME_PAD / 2);
	gtk_widget_show (wid);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_box_pack_start (GTK_BOX (vb), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	clist = gtk_clist_new_with_titles (2, titles2);
	gtk_clist_set_selection_mode (GTK_CLIST (clist),
				      GTK_SELECTION_SINGLE);
	gtk_clist_set_reorderable (GTK_CLIST (clist), FALSE);
	gtk_container_add (GTK_CONTAINER (sw), clist);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 0, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 1, TRUE);
	gtk_clist_column_titles_passive (GTK_CLIST (clist));
	gtk_signal_connect (GTK_OBJECT (clist), "select_row",
			    GTK_SIGNAL_FUNC (function_info_display_cb), dw);
	gtk_widget_show (clist);
	dw->functionsclist = clist;

	wid = gtk_label_new (_("Information on the selected function:"));
	gtk_box_pack_start (GTK_BOX (vb), wid, FALSE, TRUE, 0);
	gtk_widget_show (wid);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_box_pack_start (GTK_BOX (vb), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	text = gtk_text_new (NULL, NULL);
	gtk_text_set_editable (GTK_TEXT (text), FALSE);
	gtk_text_set_word_wrap (GTK_TEXT (text), FALSE);
	gtk_container_add (GTK_CONTAINER (sw), text);
	gtk_widget_show (text);
	dw->functionstext = text;

	/* notebook Aggregates page */
	wid = gtk_label_new (_("Aggregates"));
	gtk_widget_show (wid);

	vb = gtk_vbox_new (FALSE, GNOME_PAD);
	gtk_notebook_append_page (GTK_NOTEBOOK (dw->notebook), vb, wid);
	gtk_widget_show (vb);

	wid = gtk_label_new (_("List of aggregates (select one):"));
	gtk_box_pack_start (GTK_BOX (vb), wid, FALSE, TRUE, GNOME_PAD / 2);
	gtk_widget_show (wid);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_box_pack_start (GTK_BOX (vb), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	clist = gtk_clist_new_with_titles (2, titles3);
	gtk_clist_set_selection_mode (GTK_CLIST (clist),
				      GTK_SELECTION_SINGLE);
	gtk_clist_set_reorderable (GTK_CLIST (clist), FALSE);
	gtk_container_add (GTK_CONTAINER (sw), clist);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 0, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 1, TRUE);
	gtk_clist_column_titles_passive (GTK_CLIST (clist));
	gtk_signal_connect (GTK_OBJECT (clist), "select_row",
			    GTK_SIGNAL_FUNC (aggregate_info_display_cb), dw);
	gtk_widget_show (clist);
	dw->aggsclist = clist;

	wid = gtk_label_new (_("Information on the selected aggregate:"));
	gtk_box_pack_start (GTK_BOX (vb), wid, FALSE, TRUE, 0);
	gtk_widget_show (wid);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_box_pack_start (GTK_BOX (vb), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	text = gtk_text_new (NULL, NULL);
	gtk_text_set_editable (GTK_TEXT (text), FALSE);
	gtk_text_set_word_wrap (GTK_TEXT (text), FALSE);
	gtk_container_add (GTK_CONTAINER (sw), text);
	gtk_widget_show (text);
	dw->aggstext = text;

	/* notebook Other informations page */
	wid = gtk_label_new (_("Other informations"));
	gtk_widget_show (wid);

	colormap = gdk_colormap_get_system ();
	red_color.red = 0xffff;
	red_color.green = 0;
	red_color.blue = 0;
	if (!gdk_color_alloc (colormap, &red_color))
		rcolor = NULL;
	else
		rcolor = &red_color;

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_notebook_append_page (GTK_NOTEBOOK (dw->notebook), sw, wid);
	gtk_widget_show (sw);

	text = gtk_text_new (NULL, NULL);
	gtk_text_set_editable (GTK_TEXT (text), FALSE);
	gtk_text_set_word_wrap (GTK_TEXT (text), TRUE);
	gtk_container_add (GTK_CONTAINER (sw), text);
	gtk_widget_show (text);

	/* server type */
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL,
			 _("Supported features by the database:"), -1);
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL,
			 _("\n * use of sequences: "), -1);
	if (dw->srv->features.sequences)
		gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL,
				 _("Yes"), -1);
	else
		gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL, _("No"),
				 -1);
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL,
			 _("\n * tables inheritance: "), -1);
	if (dw->srv->features.inheritance)
		gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL,
				 _("Yes"), -1);
	else
		gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL, _("No"),
				 -1);

	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL,
			 _("\n\nMisc:"), -1);
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL,
			 _("\n * Bonobo support: "), -1);
	gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL, _("Yes"), -1);
}

GtkWidget *
sql_data_wid_new (ServerAccess * srv)
{
	GtkWidget *obj;
	SqlDataWid *dw;

	obj = GTK_WIDGET (gtk_type_new (sql_data_wid_get_type ()));
	dw = SQL_DATA_WID (obj);
	dw->srv = srv;
	sql_data_wid_post_init (dw);
	populate_data_types (dw);
	populate_functions (dw);
	populate_aggregates (dw);

	/* signals */
	gtk_signal_connect_while_alive (GTK_OBJECT (srv),
					"data_function_added",
					GTK_SIGNAL_FUNC (function_add_cb), dw,
					GTK_OBJECT (obj));
	gtk_signal_connect_while_alive (GTK_OBJECT (srv),
					"data_function_removed",
					GTK_SIGNAL_FUNC (function_del_cb), dw,
					GTK_OBJECT (obj));
	gtk_signal_connect_while_alive (GTK_OBJECT (srv),
					"data_aggregate_added",
					GTK_SIGNAL_FUNC (aggregate_add_cb),
					dw, GTK_OBJECT (obj));
	gtk_signal_connect_while_alive (GTK_OBJECT (srv),
					"data_aggregate_removed",
					GTK_SIGNAL_FUNC (aggregate_del_cb),
					dw, GTK_OBJECT (obj));
	return obj;
}


static void
sql_data_wid_destroy (GtkObject * object)
{
	SqlDataWid *qw;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_SQL_DATA_WID (object));

	qw = SQL_DATA_WID (object);

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


static void
data_info_display_cb (GtkWidget * wid,
		      gint row, gint column,
		      GdkEventButton * event, SqlDataWid * dw)
{
	GtkWidget *text = dw->typestext;
	GdkColor red_color, blue_color;
	GdkColor *rcolor, *bcolor;
	GdkColormap *colormap;
	ServerDataType *type;
	gchar *str;
	GString *gstr;
	GSList *list, *list2;
	gboolean first;

	/* colors */
	colormap = gdk_colormap_get_system ();
	red_color.red = 0xffff;
	red_color.green = 0;
	red_color.blue = 0;
	if (!gdk_color_alloc (colormap, &red_color))
		rcolor = NULL;
	else
		rcolor = &red_color;
	blue_color.red = 0;
	blue_color.green = 0;
	blue_color.blue = 0xffff;
	if (!gdk_color_alloc (colormap, &blue_color))
		bcolor = NULL;
	else
		bcolor = &blue_color;

	/* init */
	type = SERVER_DATA_TYPE (gtk_clist_get_row_data (GTK_CLIST (wid), row));
	dw->sel_type = type;
	gtk_text_freeze (GTK_TEXT (text));
	gtk_text_set_point (GTK_TEXT (text), 0);
	if (gtk_text_get_length (GTK_TEXT (text)))
		gtk_text_forward_delete (GTK_TEXT (text),
					 gtk_text_get_length (GTK_TEXT
							      (text)));

	/* type info */
	gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL, _("Type:\n"),
			 -1);
	str = g_strdup_printf ("\t%s\n", type->sqlname);
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, str, -1);
	g_free (str);
	str = g_strdup_printf ("\t%s\n", _(type->descr));
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, str, -1);
	g_free (str);
	str = g_strdup_printf (_("\tServer type:%d\n"), type->server_type);
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, str, -1);
	g_free (str);

	/* funtions */
	gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL,
			 _("\nFunctions for " "that type:\n"), -1);
	list = dw->srv->data_functions;
	while (list) {
		if (g_slist_find (SERVER_FUNCTION (list->data)->args, type)) {
			str = g_strdup_printf ("\t%s",
					       SERVER_FUNCTION (list->
								  data)->
					       sqlname);
			gtk_text_insert (GTK_TEXT (text), NULL, bcolor, NULL,
					 str, -1);
			g_free (str);
			gstr = g_string_new (" (");
			list2 = SERVER_FUNCTION (list->data)->args;
			first = TRUE;
			while (list2) {
				if (first) {
					g_string_sprintfa (gstr, "%s",
							   SERVER_DATA_TYPE
							   (list2->data)->
							   sqlname);
					first = FALSE;
				}
				else
					g_string_sprintfa (gstr, ", %s",
							   SERVER_DATA_TYPE
							   (list2->data)->
							   sqlname);
				list2 = g_slist_next (list2);
			}
			g_string_sprintfa (gstr, _(") returns %s\n"),
					   SERVER_FUNCTION (list->data)->
					   result_type->sqlname);
			gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL,
					 gstr->str, -1);
			g_string_free (gstr, TRUE);
		}
		list = g_slist_next (list);
	}
	/* Aggregates */
	gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL,
			 _("\nAggregates for " "that type:\n"), -1);
	list = dw->srv->data_aggregates;
	while (list) {
		if (SERVER_AGGREGATE (list->data)->arg_type == type) {
			str = g_strdup_printf ("\t%s",
					       SERVER_AGGREGATE (list->
								   data)->
					       sqlname);
			gtk_text_insert (GTK_TEXT (text), NULL, bcolor, NULL,
					 str, -1);
			g_free (str);
			str = g_strdup_printf (" (%s)\n", type->sqlname);
			gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL,
					 str, -1);
			g_free (str);
		}
		list = g_slist_next (list);
	}

	gtk_text_thaw (GTK_TEXT (text));
	gtk_adjustment_set_value (GTK_TEXT (text)->vadj, 0.);
}


static void
user_defined_functions_toggled_cb (GtkWidget * wid, SqlDataWid * dw)
{
	if (GTK_TOGGLE_BUTTON (wid)->active)
		dw->mode += 1;
	else
		dw->mode -= 1;

	gtk_clist_freeze (GTK_CLIST (dw->functionsclist));
	gtk_clist_clear (GTK_CLIST (dw->functionsclist));
	populate_functions (dw);
	gtk_clist_thaw (GTK_CLIST (dw->functionsclist));
}

static void
system_defined_functions_toggled_cb (GtkWidget * wid, SqlDataWid * dw)
{
	if (GTK_TOGGLE_BUTTON (wid)->active)
		dw->mode += 2;
	else
		dw->mode -= 2;

	gtk_clist_freeze (GTK_CLIST (dw->functionsclist));
	gtk_clist_clear (GTK_CLIST (dw->functionsclist));
	populate_functions (dw);
	gtk_clist_thaw (GTK_CLIST (dw->functionsclist));
}


static void
function_info_display_cb (GtkWidget * wid,
			  gint row, gint column,
			  GdkEventButton * event, SqlDataWid * dw)
{
	GtkWidget *text = dw->functionstext;
	GdkColor red_color;
	GdkColor *rcolor;
	GdkColormap *colormap;
	ServerFunction *func;
	gchar *str;
	GSList *list;


	/* colors */
	colormap = gdk_colormap_get_system ();
	red_color.red = 0xffff;
	red_color.green = 0;
	red_color.blue = 0;
	if (!gdk_color_alloc (colormap, &red_color))
		rcolor = NULL;
	else
		rcolor = &red_color;

	/* init */
	func = SERVER_FUNCTION (gtk_clist_get_row_data
				  (GTK_CLIST (wid), row));
	dw->sel_func = func;
	gtk_text_freeze (GTK_TEXT (text));
	gtk_text_set_point (GTK_TEXT (text), 0);
	if (gtk_text_get_length (GTK_TEXT (text)))
		gtk_text_forward_delete (GTK_TEXT (text),
					 gtk_text_get_length (GTK_TEXT
							      (text)));

	/* informations */
	gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL,
			 _("Function:\n"), -1);
	str = g_strdup_printf ("\t%s\n", func->sqlname);
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, str, -1);
	g_free (str);
	gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL,
			 _("Description:\n"), -1);
	str = g_strdup_printf ("\t%s\n", func->descr);
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, str, -1);
	g_free (str);
	gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL,
			 _("Arguments:\n"), -1);
	list = func->args;
	while (list) {
		str = g_strdup_printf ("\t%s\n",
				       SERVER_DATA_TYPE (list->data)->sqlname);
		gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, str, -1);
		g_free (str);
		list = g_slist_next (list);
	}
	gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL,
			 _("Return type:\n"), -1);
	str = g_strdup_printf ("\t%s\n", func->result_type->sqlname);
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, str, -1);
	g_free (str);
	/* end */
	gtk_text_thaw (GTK_TEXT (text));
	gtk_adjustment_set_value (GTK_TEXT (text)->vadj, 0.);
}

static void
aggregate_info_display_cb (GtkWidget * wid,
			   gint row, gint column,
			   GdkEventButton * event, SqlDataWid * dw)
{
	GtkWidget *text = dw->aggstext;
	GdkColor red_color;
	GdkColor *rcolor;
	GdkColormap *colormap;
	ServerAggregate *agg;
	gchar *str;
	GSList *list;

	/* colors */
	colormap = gdk_colormap_get_system ();
	red_color.red = 0xffff;
	red_color.green = 0;
	red_color.blue = 0;
	if (!gdk_color_alloc (colormap, &red_color))
		rcolor = NULL;
	else
		rcolor = &red_color;

	/* init */
	agg = SERVER_AGGREGATE (gtk_clist_get_row_data
				  (GTK_CLIST (wid), row));
	dw->sel_agg = agg;
	gtk_text_freeze (GTK_TEXT (text));
	gtk_text_set_point (GTK_TEXT (text), 0);
	if (gtk_text_get_length (GTK_TEXT (text)))
		gtk_text_forward_delete (GTK_TEXT (text),
					 gtk_text_get_length (GTK_TEXT
							      (text)));

	/* informations */
	gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL,
			 _("Aggregate:\n"), -1);
	str = g_strdup_printf ("\t%s\n", agg->sqlname);
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, str, -1);
	g_free (str);
	gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL,
			 _("Description:\n"), -1);
	if (agg->descr)
		str = g_strdup_printf ("\t%s\n", agg->descr);
	else
		str = g_strdup ("\t\n");
	gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL, str, -1);
	g_free (str);
	gtk_text_insert (GTK_TEXT (text), NULL, rcolor, NULL,
			 _("Possible data type argument:\n"), -1);
	list = dw->srv->data_aggregates;
	while (list) {
		if (!strcmp
		    (agg->sqlname,
		     SERVER_AGGREGATE (list->data)->sqlname)) {
			if (SERVER_AGGREGATE (list->data)->arg_type)
				str = g_strdup_printf ("\t%s\n",
						       SERVER_AGGREGATE
						       (list->data)->
						       arg_type->sqlname);
			else
				str = g_strdup (_("\tAny data type\n"));
			gtk_text_insert (GTK_TEXT (text), NULL, NULL, NULL,
					 str, -1);
			g_free (str);
		}
		list = g_slist_next (list);
	}
	/* end */
	gtk_text_thaw (GTK_TEXT (text));
	gtk_adjustment_set_value (GTK_TEXT (text)->vadj, 0.);
}

static void
function_add_cb (GtkObject * obj, ServerFunction * df, SqlDataWid * dw)
{
	gchar *line[2];
	gint i, nb;
	GSList *names, *nlist;
	ServerFunction *func;
	gboolean found;

	/* generating the names list */
	names = NULL;
	nb = GTK_CLIST (dw->functionsclist)->rows;
	for (i = 0; i < nb; i++) {
		func = gtk_clist_get_row_data (GTK_CLIST (dw->functionsclist),
					       i);
		names = g_slist_append (names, func->sqlname);
	}

	/* does the name exist in the printed list? */
	found = TRUE;
	nlist = names;
	i = 0;
	while (nlist && found) {
		if ((strcmp ((gchar *) (nlist->data), df->sqlname) > 0) ||
		    ((strcmp ((gchar *) (nlist->data), df->sqlname) < 0) &&
		     (!g_slist_next (nlist))))
			found = FALSE;
		if (strcmp ((gchar *) (nlist->data), df->sqlname) < 0)
			i++;
		nlist = g_slist_next (nlist);
	}
	g_slist_free (names);

	if (((dw->mode == 1) && (df->is_user)) ||
	    ((dw->mode == 2) && (!df->is_user)) || (dw->mode == 3)) {
		line[0] = g_strdup (df->sqlname);
		line[1] = g_strdup (df->descr);
		gtk_clist_insert (GTK_CLIST (dw->functionsclist), i, line);
		gtk_clist_set_row_data (GTK_CLIST (dw->functionsclist), i,
					df);
		g_free (line[0]);
		g_free (line[1]);
	}
}

static void
function_del_cb (GtkObject * obj, ServerFunction * df, SqlDataWid * dw)
{
	gint i;
	GtkWidget *text = dw->functionstext;

	if (df == dw->sel_func) {
		gtk_text_freeze (GTK_TEXT (text));
		gtk_text_set_point (GTK_TEXT (text), 0);
		if (gtk_text_get_length (GTK_TEXT (text)))
			gtk_text_forward_delete (GTK_TEXT (text),
						 gtk_text_get_length (GTK_TEXT
								      (text)));
		gtk_text_thaw (GTK_TEXT (text));
		dw->sel_func = NULL;
	}
	i = gtk_clist_find_row_from_data (GTK_CLIST (dw->functionsclist), df);
	if (i >= 0)
		gtk_clist_remove (GTK_CLIST (dw->functionsclist), i);
	/* redisplay the contents of the data type window */
	if (dw->sel_type) {
		i = gtk_clist_find_row_from_data (GTK_CLIST (dw->typesclist),
						  dw->sel_type);
		gtk_clist_select_row (GTK_CLIST (dw->typesclist), i, 0);
	}
}

static void
aggregate_add_cb (GtkObject * obj, ServerAggregate * da, SqlDataWid * dw)
{
	GSList *names, *nlist;
	gchar *line[2];
	gint i, nb;
	gboolean found;
	ServerAggregate *agg;

	/* generating the names list */
	names = NULL;
	nb = GTK_CLIST (dw->aggsclist)->rows;
	for (i = 0; i < nb; i++) {
		agg = gtk_clist_get_row_data (GTK_CLIST (dw->aggsclist), i);
		names = g_slist_append (names, agg->sqlname);
	}
	/* does the name exist in the printed list? */
	found = TRUE;
	nlist = names;
	i = 0;
	while (nlist && found) {
		if ((strcmp ((gchar *) (nlist->data), da->sqlname) > 0) ||
		    ((strcmp ((gchar *) (nlist->data), da->sqlname) < 0) &&
		     (!g_slist_next (nlist))))
			found = FALSE;
		if (strcmp ((gchar *) (nlist->data), da->sqlname) < 0)
			i++;
		nlist = g_slist_next (nlist);
	}
	g_slist_free (names);
	if (!found) {
		line[0] = g_strdup (da->sqlname);
		line[1] = g_strdup (da->descr);
		gtk_clist_insert (GTK_CLIST (dw->aggsclist), i, line);
		gtk_clist_set_row_data (GTK_CLIST (dw->aggsclist), i, da);
		g_free (line[0]);
		g_free (line[1]);
	}

	i = gtk_clist_find_row_from_data (GTK_CLIST (dw->aggsclist),
					  dw->sel_agg);
	gtk_clist_select_row (GTK_CLIST (dw->aggsclist), i, 0);
	i = gtk_clist_find_row_from_data (GTK_CLIST (dw->typesclist),
					  dw->sel_type);
	gtk_clist_select_row (GTK_CLIST (dw->typesclist), i, 0);
}

static void
aggregate_del_cb (GtkObject * obj, ServerAggregate * da, SqlDataWid * dw)
{
	gint i;

	i = gtk_clist_find_row_from_data (GTK_CLIST (dw->aggsclist),
					  dw->sel_agg);

	gtk_clist_freeze (GTK_CLIST (dw->aggsclist));
	gtk_clist_clear (GTK_CLIST (dw->aggsclist));
	populate_aggregates (dw);
	gtk_clist_thaw (GTK_CLIST (dw->aggsclist));

	if (i < GTK_CLIST (dw->aggsclist)->rows)
		gtk_clist_select_row (GTK_CLIST (dw->aggsclist), i, 0);
	else
		gtk_clist_select_row (GTK_CLIST (dw->aggsclist),
				      GTK_CLIST (dw->aggsclist)->rows - 1, 0);
	i = gtk_clist_find_row_from_data (GTK_CLIST (dw->typesclist),
					  dw->sel_type);
	gtk_clist_select_row (GTK_CLIST (dw->typesclist), i, 0);
}


static void
populate_data_types (SqlDataWid * dw)
{
	GSList *list = dw->srv->data_types;
	gchar *line[2];
	gint i;

	while (list) {
		line[0] = g_strdup (SERVER_DATA_TYPE (list->data)->sqlname);
		line[1] = g_strdup (_(SERVER_DATA_TYPE (list->data)->descr));
		i = gtk_clist_append (GTK_CLIST (dw->typesclist), line);
		gtk_clist_set_row_data (GTK_CLIST (dw->typesclist), i,
					list->data);
		g_free (line[0]);
		g_free (line[1]);
		list = g_slist_next (list);
	}
}

static void
populate_functions (SqlDataWid * dw)
{
	GSList *list = dw->srv->data_functions;
	gchar *line[2];
	gint i;

	while (list) {
		if (((dw->mode == 1)
		     && (SERVER_FUNCTION (list->data)->is_user))
		    || ((dw->mode == 2)
			&& (!SERVER_FUNCTION (list->data)->is_user))
		    || (dw->mode == 3)) {
			line[0] =
				g_strdup (SERVER_FUNCTION (list->data)->
					  sqlname);
			line[1] =
				g_strdup (SERVER_FUNCTION (list->data)->
					  descr);
			i = gtk_clist_append (GTK_CLIST (dw->functionsclist),
					      line);
			gtk_clist_set_row_data (GTK_CLIST
						(dw->functionsclist), i,
						list->data);
			g_free (line[0]);
			g_free (line[1]);
		}
		list = g_slist_next (list);
	}
}

static void
populate_aggregates (SqlDataWid * dw)
{
	GSList *list = dw->srv->data_aggregates;
	GSList *names, *nlist;
	gchar *line[2];
	gint i;
	gboolean found;

	names = NULL;
	while (list) {
		found = FALSE;
		nlist = names;
		while (nlist && !found) {
			if (!strcmp ((gchar *) (nlist->data),
				     SERVER_AGGREGATE (list->data)->
				     sqlname))
				found = TRUE;
			nlist = g_slist_next (nlist);
		}
		if (!found) {
			line[0] =
				g_strdup (SERVER_AGGREGATE (list->data)->
					  sqlname);
			line[1] =
				g_strdup (SERVER_AGGREGATE (list->data)->
					  descr);
			i = gtk_clist_append (GTK_CLIST (dw->aggsclist),
					      line);
			gtk_clist_set_row_data (GTK_CLIST (dw->aggsclist), i,
						list->data);
			g_free (line[0]);
			g_free (line[1]);
			names = g_slist_append (names,
						SERVER_AGGREGATE (list->
								    data)->
						sqlname);
		}
		list = g_slist_next (list);
	}
	g_slist_free (names);
}


/*
 *
 * Functions related to the Dialog
 *
 */

static void main_dialog_clicked_cb (GnomeDialog * dialog,
				    gint button_number, gpointer data);
static void catch_close_to_connect_cb (GtkObject * obj, GnomeDialog * dialog);
GtkWidget *
sql_data_wid_new_dlg (ServerAccess * srv)
{
	GtkWidget *dlg, *wid;

	dlg = gnome_dialog_new (_("Data types and stuff"),
				GNOME_STOCK_BUTTON_OK, NULL);
	gtk_window_set_policy (GTK_WINDOW (dlg), TRUE, TRUE, FALSE);

	/* widgets at the top */
	wid = sql_data_wid_new (srv);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox),
			    wid, TRUE, TRUE, 0);
	gtk_widget_show (wid);

	gtk_signal_connect (GTK_OBJECT (dlg), "clicked",
			    GTK_SIGNAL_FUNC (main_dialog_clicked_cb), NULL);
	gtk_signal_connect_while_alive (GTK_OBJECT (srv), "conn_to_close",
					GTK_SIGNAL_FUNC
					(catch_close_to_connect_cb), dlg,
					GTK_OBJECT (dlg));
	gtk_widget_set_usize (dlg, 370, 420);
	return dlg;
}

static void
main_dialog_clicked_cb (GnomeDialog * dialog,
			gint button_number, gpointer data)
{
	gnome_dialog_close (dialog);
}

static void
catch_close_to_connect_cb (GtkObject * obj, GnomeDialog * dialog)
{
	gnome_dialog_close (dialog);
}
