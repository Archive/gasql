/* gladefeditor.c
 *
 * Copyright (C) 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gladefeditor.h"
#include "gladechooser.h"
#include "choicecombo.h"
#include <parser.h>

static void      glade_form_editor_class_init      (GladeFormEditorClass *class);
static void      glade_form_editor_init            (GladeFormEditor *gfe);
static void      glade_form_editor_post_init       (GladeFormEditor *gfe);
static void      glade_form_editor_destroy         (GtkObject *object);

enum {
	FORM_CHANGED,
	LAST_SIGNAL
};

typedef struct {
	gchar *name; /* widget's name, do not free */
	GtkWidget *widget;
	GtkWidget *signal_widget;
} FormWidget;

static gint glade_form_editor_signals[LAST_SIGNAL] = { 0 }; 


guint 
glade_form_editor_get_type(void) 
{
  static guint f_type = 0;
  
  if (!f_type)
    {
      GtkTypeInfo f_info =
      {
        "Glade Form Editor",
        sizeof (GladeFormEditor),	
        sizeof (GladeFormEditorClass),
        (GtkClassInitFunc) glade_form_editor_class_init,
        (GtkObjectInitFunc) glade_form_editor_init,
        (GtkArgSetFunc) NULL,
        (GtkArgGetFunc) NULL
      };
      
      f_type = gtk_type_unique (gtk_vbox_get_type (), &f_info);
    }
  
  return f_type;   
}

static void my_form_changed (GladeFormEditor *gfe);

static void 
glade_form_editor_class_init (GladeFormEditorClass *class)
{
  GtkObjectClass *object_class = NULL;

  object_class = (GtkObjectClass*) class;
  glade_form_editor_signals[FORM_CHANGED] =
	  gtk_signal_new ("form_changed",
			  GTK_RUN_FIRST,
			  object_class->type,
			  GTK_SIGNAL_OFFSET (GladeFormEditorClass, form_changed),
			  gtk_signal_default_marshaller, GTK_TYPE_NONE,
			  0);
  
  gtk_object_class_add_signals (object_class, glade_form_editor_signals, 
				LAST_SIGNAL);
  class->form_changed = my_form_changed;

  object_class->destroy = glade_form_editor_destroy;
}

static void my_form_changed (GladeFormEditor *gfe)
{
}

static void 
glade_form_editor_init(GladeFormEditor *gfe)
{
	gfe->form_struct = g_new0 (GladeFormStruct, 1);
	gfe->glade_widget = NULL;
	gfe->selected = NULL;
	gfe->signal_selected = NULL;
	gfe->form_widgets = NULL;
}

GtkWidget* 
glade_form_editor_new(void)
{
	GtkObject *obj;
	GladeFormEditor *gfe;
	
	obj = gtk_type_new(glade_form_editor_get_type());
	gfe = GLADE_FORM_EDITOR(obj);
	
	glade_form_editor_post_init(gfe);
	/* FIXME: Query signals */
  
	return GTK_WIDGET(obj);
}

static void 
glade_form_editor_destroy(GtkObject *object)
{
  GladeFormEditor *gfe;
  GtkObjectClass *parent_class = NULL;

  g_print("\tGFE DESTROY...\n");
  parent_class = gtk_type_class (gtk_vbox_get_type());
  g_return_if_fail (object != NULL);
  g_return_if_fail (IS_GLADE_FORM_EDITOR (object));
  
  gfe = GLADE_FORM_EDITOR (object);

  /* form struct */
  if (gfe->form_struct->glade_file_name)
	  g_free(gfe->form_struct->glade_file_name);
  if (gfe->form_struct->top_widget_name)
	  g_free(gfe->form_struct->top_widget_name);
  if (gfe->form_struct->gxml)
	  gtk_object_unref(GTK_OBJECT(gfe->form_struct->gxml));
  g_free (gfe->form_struct);

  /* form widgets */
  while (gfe->form_widgets) {
	  FormWidget *fw = (FormWidget *) (gfe->form_widgets->data);
	  gfe->form_widgets = g_slist_remove (gfe->form_widgets, fw);
	  g_free (fw);
  } 

  /* for the parent class */
  if (GTK_OBJECT_CLASS (parent_class)->destroy)
    (* GTK_OBJECT_CLASS (parent_class)->destroy) (object); 
}


static void import_button_clicked_cb (GtkButton *button, GladeFormEditor *gfe);
static void combo_sel_widget_changed_cb (GtkEntry *entry, GladeFormEditor *gfe);
static void dummy_feed_sw (GladeFormEditor *gfe);

static void      
glade_form_editor_post_init (GladeFormEditor *gfe)
{
	GtkWidget *sw, *frame, *combo, *label, *paned, *table, *bb, *button, *box, *clist;
	gchar *titles[2] = {N_("Widget name"), N_("Corresponding form entry")};

	gtk_container_set_border_width (GTK_CONTAINER (gfe), GNOME_PAD/2.);

	paned = gtk_hpaned_new ();
	gtk_box_pack_start (GTK_BOX (gfe), paned, TRUE, TRUE, 0);
	gtk_widget_set_usize (GTK_WIDGET (gfe), 700, 500);

	/* left part: the glade widget preview */
	frame = gtk_frame_new (_("Raw preview of the imported form:"));
	gtk_paned_add1 (GTK_PANED (paned), frame);
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD/2.);
	gtk_widget_set_usize (frame, 400, 400);

	box = gtk_vbox_new (FALSE, GNOME_PAD/2.);
	gtk_container_add (GTK_CONTAINER (frame), box);
	
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (box), sw, TRUE, TRUE, 0);	
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_set_border_width (GTK_CONTAINER (sw), GNOME_PAD/2.);
	gfe->scrolled_window = sw;

	bb = gtk_hbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (box), bb, FALSE, TRUE, GNOME_PAD/2.);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);

	button = gtk_button_new_with_label (_("Import from Glade file"));
	gtk_box_pack_start (GTK_BOX (bb), button, FALSE, TRUE, 0);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
	  GTK_SIGNAL_FUNC (import_button_clicked_cb), gfe);
	

	/* right part of the widget */
	frame = gtk_frame_new (_("Edition of correspondances:"));
	gtk_paned_add2 (GTK_PANED (paned), frame);
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD/2.);

	box = gtk_vbox_new (FALSE, GNOME_PAD/2.);
	gtk_container_set_border_width (GTK_CONTAINER (box), GNOME_PAD/2.);

	gtk_container_add (GTK_CONTAINER (frame), box);

	table = gtk_table_new (3, 2, FALSE);
	gtk_box_pack_start (GTK_BOX (box), table, FALSE, TRUE, GNOME_PAD/2.);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD/2.);
	label = gtk_label_new (_("Selected Widget:"));
	gtk_table_attach (GTK_TABLE (table), label, 
			  0, 1, 0, 1, 0, 0, 0, 0);
	label = gtk_label_new (_("corresponds to"));
	gtk_table_attach (GTK_TABLE (table), label, 
			  0, 2, 1, 2, 0, 0, 0, 0);
	label = gtk_label_new (_("Form entry:"));
	gtk_table_attach (GTK_TABLE (table), label, 
			  0, 1, 2, 3, 0, 0, 0, 0);
	combo = gtk_combo_new ();
	gtk_table_attach_defaults (GTK_TABLE (table), combo, 
				   1, 2, 0, 1);
	gtk_entry_set_editable (GTK_ENTRY (GTK_COMBO (combo)->entry), FALSE);
	gtk_signal_connect (GTK_OBJECT (GTK_COMBO (combo)->entry), "changed", 
 			    GTK_SIGNAL_FUNC (combo_sel_widget_changed_cb), gfe); 
	gfe->combo_sel_widget = combo;
	combo = choice_combo_new ();
	gtk_table_attach_defaults (GTK_TABLE (table), combo, 
				   1, 2, 2, 3);
	gfe->combo_sel_entry = combo;

	bb = gtk_hbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (box), bb, FALSE, TRUE, GNOME_PAD/2.);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);
	button = gnome_stock_button (GNOME_STOCK_BUTTON_APPLY);
	gtk_box_pack_start (GTK_BOX (bb), button, FALSE, TRUE, 0);
	gtk_widget_set_sensitive (button, FALSE);
	gfe->apply_button = button;

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (box), sw, TRUE, TRUE, 0);	
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_set_border_width (GTK_CONTAINER (sw), GNOME_PAD/2.);
	clist = gtk_clist_new_with_titles (2, titles);
	gtk_container_add (GTK_CONTAINER (sw), clist);
	gfe->clist = clist;

	/* put something in the SW to help the user */
	dummy_feed_sw (gfe);

	gtk_widget_show_all (paned);
}

static void 
dummy_feed_sw (GladeFormEditor *gfe)
{
	GtkWidget *label;

	label = gtk_label_new (_("Here goes a preview of\nthe imported Glade form.\n\n"
				 "You need to import from a Glade file."));
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (gfe->scrolled_window), label);
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (GTK_BIN (gfe->scrolled_window)->child),
				      GTK_SHADOW_NONE);
	gtk_widget_show (label);
	gfe->glade_widget = GTK_BIN (gfe->scrolled_window)->child;
}


static void chooser_form_changed_cb(GladeChooser *chooser, GnomeDialog *dlg);
/* sets some signal callbacks on the widgets to be able to select them */
static void glade_widget_real_initialize (GtkWidget * widget, GladeFormEditor *gfe);
static void clean_form_editor (GladeFormEditor *gfe);
static void 
import_button_clicked_cb (GtkButton *button, GladeFormEditor *gfe)
{
	GtkWidget *dlg, *chooser, *form;
	gint result;

	dlg = gnome_dialog_new (_("Select Glade Form to import"), GNOME_STOCK_BUTTON_OK,
				GNOME_STOCK_BUTTON_CANCEL, NULL);
	gtk_window_set_policy (GTK_WINDOW (dlg), TRUE, TRUE, TRUE);
	chooser = glade_chooser_new ();
	gtk_widget_show (chooser);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), chooser, TRUE, TRUE, 0);
	gtk_signal_connect (GTK_OBJECT (chooser), "form_changed",
			    GTK_SIGNAL_FUNC (chooser_form_changed_cb), GNOME_DIALOG (dlg));
	gnome_dialog_set_sensitive (GNOME_DIALOG (dlg), 0, FALSE);
	result = gnome_dialog_run (GNOME_DIALOG (dlg));
	switch (result) {
	case -1: /* do nothing, the dialog has already been destroyed */
		break;
	case 0:
		clean_form_editor (gfe);
		
		gfe->form_struct->glade_file_name = g_strdup (GLADE_CHOOSER (chooser)->form_struct->top_widget_name);
		gfe->form_struct->top_widget_name = g_strdup (GLADE_CHOOSER (chooser)->form_struct->top_widget_name);
		gtk_object_ref (GTK_OBJECT (GLADE_CHOOSER (chooser)->form_struct->gxml));
		gfe->form_struct->gxml = GLADE_CHOOSER (chooser)->form_struct->gxml;

		form = fetch_glade_widget (GLADE_CHOOSER (chooser));
		gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (gfe->scrolled_window), form);
		gtk_widget_unref (form);
		gtk_viewport_set_shadow_type (GTK_VIEWPORT (GTK_BIN (gfe->scrolled_window)->child),
					      GTK_SHADOW_NONE);
		gfe->glade_widget = GTK_BIN (gfe->scrolled_window)->child;
		gtk_widget_show_all (gfe->glade_widget);

		glade_widget_real_initialize (gfe->glade_widget, gfe);
	case 1:
		gtk_widget_destroy (dlg);
		break;
	}
}

static void 
chooser_form_changed_cb(GladeChooser *chooser, GnomeDialog *dlg)
{
	gnome_dialog_set_sensitive (dlg, 0, chooser->form_struct->gxml ? TRUE : FALSE);
}

static void 
clean_form_editor (GladeFormEditor *gfe)
{
	GList *list;
	list = g_list_append (NULL, "");
	gtk_combo_set_popdown_strings (GTK_COMBO (gfe->combo_sel_widget), list);
	g_list_free (list);

	if (gfe->form_struct->glade_file_name) {
		g_free (gfe->form_struct->glade_file_name);
		gfe->form_struct->glade_file_name = NULL;
	}
	
	if (gfe->form_struct->top_widget_name) {
		g_free (gfe->form_struct->top_widget_name);
		gfe->form_struct->top_widget_name = NULL;
	}
	
	if (gfe->glade_widget) {
		gtk_container_remove (GTK_CONTAINER (gfe->scrolled_window), gfe->glade_widget);
		gfe->glade_widget = NULL;
	}
	
	if (gfe->form_struct->gxml) {
		gtk_object_unref (GTK_OBJECT (gfe->form_struct->gxml));
		gfe->form_struct->gxml = NULL;
	}		
}


/*
 * Below is the code to be able to select widgets in the imported
 * Glade form
 */
static void glade_widget_add_mouse_signals (GtkWidget * widget, GladeFormEditor *gfe);
static void glade_widget_add_draw_signals (GtkWidget * widget, GladeFormEditor *gfe);

static void
glade_widget_real_initialize (GtkWidget * widget, GladeFormEditor *gfe) 
{
	/* free the old selection */
	gfe->selected = NULL;
	gfe->signal_selected = NULL;
	while (gfe->form_widgets) {
		FormWidget *fw = (FormWidget *) (gfe->form_widgets->data);
		gfe->form_widgets = g_slist_remove (gfe->form_widgets, fw);
		g_free (fw);
	}

	/* signals for the new widgets */
	glade_widget_add_mouse_signals (widget, gfe);
	glade_widget_add_draw_signals (widget, gfe);

	/* sets the widget names list for selection */
	gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (gfe->combo_sel_widget)->entry), "");
	gtk_editable_changed (GTK_EDITABLE (GTK_COMBO (gfe->combo_sel_widget)->entry));
	if (gfe->form_widgets) {
		GList *list = NULL;
		GSList *ptr = gfe->form_widgets;
		while (ptr) {
			FormWidget *fw = (FormWidget *) (ptr->data);
			list = g_list_append (list, fw->name);
			ptr = g_slist_next (ptr);
		}
		gtk_combo_set_popdown_strings (GTK_COMBO (gfe->combo_sel_widget), list);
		g_list_free (list);
	}
	else {
		GList *list;
		list = g_list_append (NULL, "");
		gtk_combo_set_popdown_strings (GTK_COMBO (gfe->combo_sel_widget), list);
		g_list_free (list);
	}
}

/* Mouse signals */
static void glade_add_mouse_signals_recursive (GtkWidget *widget, GladeFormEditor *gfe);
static void
glade_widget_add_mouse_signals (GtkWidget * widget, GladeFormEditor *gfe)
{
	glade_add_mouse_signals_recursive (widget, gfe);
}

static gint glade_widget_on_button_press (GtkWidget * signal_widget,
					  GdkEventButton * event,
					  GladeFormEditor *gfe);
static gint form_widgets_compare_func (FormWidget *f1, FormWidget *f2);
static void
glade_add_mouse_signals_recursive (GtkWidget *widget, GladeFormEditor *gfe)
{
	/* Ensure that the event mask is set so we get button press & release
	   events. */
	if (!GTK_WIDGET_NO_WINDOW (widget)) {
		if (!GTK_WIDGET_REALIZED (widget)) {
			gtk_widget_set_events (widget, gtk_widget_get_events (widget)
					       | GDK_BUTTON_PRESS_MASK
					       | GDK_BUTTON_RELEASE_MASK);
		}
		else {
			GdkEventMask event_mask;
			
			event_mask = gdk_window_get_events (widget->window);
			gdk_window_set_events (widget->window, event_mask
					       | GDK_BUTTON_PRESS_MASK
					       | GDK_BUTTON_RELEASE_MASK);
		}
	}

	/* FIXME: GtkText cannot be selected because there is a bug making a crash;
	   gdb says in gtk_text_forward_delete () ??? */
	if (GTK_IS_EDITABLE (widget) && !GTK_IS_TEXT (widget)) {
		GtkWidget *real_widget = widget ;
		FormWidget *fw;

		gtk_signal_connect (GTK_OBJECT (widget), "button_press_event", 
				    GTK_SIGNAL_FUNC (glade_widget_on_button_press), gfe); 

		/* append this widget name to the list of names */
		if (GTK_IS_ENTRY (widget)) {
			if (widget->parent && GTK_IS_COMBO (widget->parent))
				real_widget = widget->parent;
		}
		fw = g_new0 (FormWidget, 1);
		fw->name = gtk_widget_get_name(real_widget);
		fw->widget = real_widget;
		fw->signal_widget = widget;
		gfe->form_widgets = g_slist_insert_sorted (gfe->form_widgets, fw, 
							   (GCompareFunc) form_widgets_compare_func);
	}

	if (GTK_IS_CONTAINER (widget))
		gtk_container_forall (GTK_CONTAINER (widget), 
				      (GtkCallback) glade_add_mouse_signals_recursive, gfe);
}

static gint form_widgets_compare_func (FormWidget *f1, FormWidget *f2)
{
	return strcmp (f1->name, f2->name);
}

static void glade_draw_selection_on_widget (GtkWidget * widget, GladeFormEditor *gfe);

static gint
glade_widget_on_button_press (GtkWidget * signal_widget,
			      GdkEventButton * event,
			      GladeFormEditor *gfe)
{
	GtkWidget *real_select=signal_widget;
	
	/* We only want single button press events. */
	if ((event->button == 1) && (event->type == GDK_BUTTON_PRESS))
		{
			/* determine which widget is really selected */
			real_select = signal_widget;
			if (GTK_IS_ENTRY (signal_widget)) {
				if (signal_widget->parent && GTK_IS_COMBO (signal_widget->parent))
					real_select = signal_widget->parent;
			}
				
			/* Unselect the previous selection */
			if (gfe->selected && (gfe->selected != real_select)) {
				GtkWidget *wid = gfe->signal_selected;
				gfe->selected = gfe->signal_selected = NULL;
				gtk_signal_emit_by_name (GTK_OBJECT(wid), "draw");
			}

			if ((gfe->selected && (gfe->selected != real_select)) ||
			    !gfe->selected) {
				gfe->selected = real_select;
				gfe->signal_selected = signal_widget;
				glade_draw_selection_on_widget(signal_widget, gfe);

				/* update the combo of selectable widgets */
				gtk_entry_set_text (GTK_ENTRY (GTK_COMBO (gfe->combo_sel_widget)->entry), 
						    gtk_widget_get_name(real_select));

				/*gtk_editable_changed (GTK_EDITABLE (GTK_COMBO (gfe->combo_sel_widget)->entry));*/
			}
		}
	if (event->button != 2) {
		gtk_signal_emit_stop_by_name(GTK_OBJECT(signal_widget), "button_press_event");
	}
	
	return FALSE;
}



/* X events */
static gint glade_expose_widget (GtkWidget * widget, GdkEventExpose * event, GladeFormEditor *gfe);
static gint glade_draw_widget (GtkWidget * widget, GdkRectangle * area, GladeFormEditor *gfe);
static void glade_draw_widget_focus (GtkWidget * widget, GladeFormEditor *gfe);

static void
glade_widget_add_draw_signals (GtkWidget * widget, GladeFormEditor *gfe)
{
	if (!GTK_WIDGET_NO_WINDOW (widget)) {
		if (!GTK_WIDGET_REALIZED (widget)) {
			gtk_widget_set_events (widget, gtk_widget_get_events (widget)
					       | GDK_EXPOSURE_MASK | GDK_POINTER_MOTION_MASK
					       | GDK_BUTTON1_MOTION_MASK
					       | GDK_POINTER_MOTION_HINT_MASK);
		}
		else {
			GdkEventMask event_mask;
			
			event_mask = gdk_window_get_events (widget->window);
			gdk_window_set_events (widget->window, event_mask
					       | GDK_EXPOSURE_MASK | GDK_POINTER_MOTION_MASK
					       | GDK_BUTTON1_MOTION_MASK
					       | GDK_POINTER_MOTION_HINT_MASK);
		}
	}
	
	gtk_signal_connect_after (GTK_OBJECT (widget), "expose_event",
				  GTK_SIGNAL_FUNC (glade_expose_widget), gfe);
	gtk_signal_connect_after (GTK_OBJECT (widget), "draw",
				  GTK_SIGNAL_FUNC (glade_draw_widget), gfe);
	
	/* Needed for button, others? */
	gtk_signal_connect_after (GTK_OBJECT (widget), "draw_default",
				  GTK_SIGNAL_FUNC (glade_draw_widget_focus), gfe);
	gtk_signal_connect_after (GTK_OBJECT (widget), "draw_focus",
				  GTK_SIGNAL_FUNC (glade_draw_widget_focus), gfe);
	
	/* Needed for scrolled window, clist? & possibly other widgets */
	if (GTK_IS_CONTAINER (widget))
		gtk_container_forall (GTK_CONTAINER (widget),
				      (GtkCallback) glade_widget_add_draw_signals, gfe);
}


static gint
glade_expose_widget (GtkWidget * widget, GdkEventExpose * event, GladeFormEditor *gfe)
{
	/* Ignore spurious exposes before widget is positioned. */
	if (widget->allocation.x == -1 || widget->allocation.y == -1)
		return FALSE;

	glade_draw_selection_on_widget (widget, gfe);

	return FALSE;
}

static gint
glade_draw_widget (GtkWidget * widget, GdkRectangle * area, GladeFormEditor *gfe)
{
	glade_draw_selection_on_widget (widget, gfe);

	return FALSE;
}

static void
glade_draw_widget_focus (GtkWidget * widget, GladeFormEditor *gfe)
{
	glade_draw_selection_on_widget (widget, gfe);
}


/* Drawing of the selection rectangle in the selected widget:
 * can safely be called for any widget, will only draw for
 * the selected widget
 */
static void
glade_draw_selection_on_widget (GtkWidget * widget, GladeFormEditor *gfe)
{
#define CORNER_WIDTH 7
#define CORNER_HEIGHT 7

	GdkGC *gc;
	gint x, y, w, h;

	if (widget != gfe->signal_selected)
		return;

	/* Check widget is drawable in case it has been deleted. */
	if (!GTK_WIDGET_DRAWABLE (widget))
		return;

	/* Don't try to draw anything if the width or height of the widget is 0. */
	if (widget->allocation.width == 0 || widget->allocation.height == 0)
		return;
			
	gc = widget->style->black_gc;
	gdk_gc_set_subwindow (gc, GDK_INCLUDE_INFERIORS);

	x = y = 0;
	gdk_window_get_size (widget->window, &w, &h);
	/*x = widget->allocation.x; */
	/*y = widget->allocation.y; */
	/*w = widget->allocation.width; */
	/*h = widget->allocation.height; */
	/* This should not be needed.  */
	/*gdk_window_clear (widget->window); */
	gdk_draw_rectangle (widget->window, gc, TRUE,
			    x, y, CORNER_WIDTH, CORNER_HEIGHT);
	gdk_draw_rectangle (widget->window, gc, TRUE,
			    x, y+h-CORNER_HEIGHT, CORNER_WIDTH, CORNER_HEIGHT);
	gdk_draw_rectangle (widget->window, gc, TRUE,
			    x+w-CORNER_WIDTH, y, CORNER_WIDTH, CORNER_HEIGHT);
	gdk_draw_rectangle (widget->window, gc, TRUE,
			    x+w-CORNER_WIDTH, y+h-CORNER_HEIGHT, CORNER_WIDTH, CORNER_HEIGHT);
	gdk_draw_rectangle (widget->window, gc, FALSE,
			    x, y, w-1, h-1);
	gdk_gc_set_subwindow (gc, GDK_CLIP_BY_CHILDREN);
}


/* 
 * other GUI callbacks 
 */
static FormWidget* find_form_widget (GladeFormEditor *gfe, gchar *name);

static void 
combo_sel_widget_changed_cb (GtkEntry *entry, GladeFormEditor *gfe)
{
	gchar *sel;

	sel = gtk_entry_get_text (entry);
	gtk_widget_set_sensitive (gfe->apply_button, (*sel == 0) ? FALSE : TRUE);

	if (*sel != 0) {
		/* update the widget in the display if necessary */
		FormWidget *fw;
		fw = find_form_widget (gfe, sel);
		if (!fw)
			g_warning ("Can't find form widget for name %s, file %s line %d\n", sel, __FILE__,
				   __LINE__);
		else {
			if (fw->widget != gfe->selected) {
				/* undrawing any previous selection */
				if (gfe->signal_selected) {
					GtkWidget *wid = gfe->signal_selected;
					gfe->selected = gfe->signal_selected = NULL;
					gtk_signal_emit_by_name (GTK_OBJECT(wid), "draw");
				}

				g_print ("Nouveau widget selectionn� � marquer: '%s'\n", sel);        
				gfe->selected = fw->widget;
				gfe->signal_selected = fw->signal_widget;
				glade_draw_selection_on_widget (fw->signal_widget, gfe);
			}
		}

		/* update the displayed attached widget in the other combo */
		g_print ("Mise � jour du widget attach� correspondant\n");
	}
}

static FormWidget* 
find_form_widget (GladeFormEditor *gfe, gchar *name)
{
	GSList *list;
	FormWidget *fw, *retval = NULL; 
	
	list = gfe->form_widgets;
	while (list && !retval) {
		fw = (FormWidget *) (list->data);
		if (!strcmp (fw->name, name))
			retval = fw;
		list = g_slist_next (list);
	}

	return retval;
}


/*
 * setting the form to be displayed from the outside
 */
gboolean         
glade_form_editor_set_form (GladeFormEditor *gfe, 
			    gchar *mem_glade_doc, gchar *top_widget)
{
	GladeXML *gxml;
	GtkWidget *wid;

	g_return_val_if_fail (IS_GLADE_FORM_EDITOR (gfe), FALSE);
	g_return_val_if_fail (mem_glade_doc != NULL, FALSE);
	g_return_val_if_fail (top_widget != NULL, FALSE);

	/* empty what may have been there */
	clean_form_editor (gfe)	;

	/* make the Glade Widget, if possible */
	gxml = glade_xml_new_from_memory (mem_glade_doc, strlen (mem_glade_doc), top_widget, NULL);
	if (!gxml) {
		dummy_feed_sw (gfe);
		g_warning ("Can't load glade form from memory:\n%s\n", mem_glade_doc);
		return FALSE;
	}

	wid = glade_xml_get_widget(gxml, top_widget);
	if (!wid) {
		g_warning ("Can't get glade form widget %s\n", top_widget);
		dummy_feed_sw (gfe);
		return FALSE;
	}

	gfe->form_struct->top_widget_name = g_strdup (top_widget);
	gfe->form_struct->gxml = gxml;

	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (gfe->scrolled_window), wid);

	gtk_viewport_set_shadow_type (GTK_VIEWPORT (GTK_BIN (gfe->scrolled_window)->child),
				      GTK_SHADOW_NONE);
	gfe->glade_widget = GTK_BIN (gfe->scrolled_window)->child;
	gtk_widget_show_all (gfe->glade_widget);

	glade_widget_real_initialize (gfe->glade_widget, gfe);
	return TRUE;
}
