/* canvas-query-join.c
 *
 * Copyright (C) 2002 Vivien Malerba
 * Copyright (C) 2002 Fernando Martins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "canvas-query-join.h"
#include "canvas-field.h"
#include "relship-view.h"

static void canvas_query_join_class_init (CanvasQueryJoinClass * class);
static void canvas_query_join_init (CanvasQueryJoin * item);
static void canvas_query_join_destroy (GtkObject * object);

static void canvas_query_join_set_arg    (GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id);
static void canvas_query_join_get_arg    (GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id);


enum
{
	ARG_0,
	ARG_QUERY,
	ARG_QUERY_JOIN
};


guint
canvas_query_join_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"CanvasQueryJoin",
			sizeof (CanvasQueryJoin),
			sizeof (CanvasQueryJoinClass),
			(GtkClassInitFunc) canvas_query_join_class_init,
			(GtkObjectInitFunc) canvas_query_join_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (canvas_base_get_type (), &f_info);
	}

	return f_type;
}

static void
canvas_query_join_class_init (CanvasQueryJoinClass * class)
{
	GtkObjectClass *object_class = NULL;


	object_class = (GtkObjectClass *) class;

	object_class->destroy = canvas_query_join_destroy;

	/* Arguments */
	gtk_object_add_arg_type ("CanvasQueryJoin::query", GTK_TYPE_POINTER, GTK_ARG_READWRITE, ARG_QUERY);
	gtk_object_add_arg_type ("CanvasQueryJoin::query_join", GTK_TYPE_POINTER, GTK_ARG_READWRITE, ARG_QUERY_JOIN);
	object_class->set_arg = canvas_query_join_set_arg;
	object_class->get_arg = canvas_query_join_get_arg;
	
}


static void enter_notify_cb (CanvasQueryJoin * cqj, gpointer data);
static void leave_notify_cb (CanvasQueryJoin * cqj, gpointer data);
static void
canvas_query_join_init (CanvasQueryJoin * cqj)
{
	cqj->query = NULL;
	cqj->join = NULL;

	cqj->x_text_space = 3.;
	cqj->y_text_space = 3.;
	cqj->bg_frame = NULL;

	cqj->items = NULL;
	cqj->ant_cview = NULL;
	cqj->suc_cview = NULL;

	/* connect to the "enter_notify" & "leave_notify" signals" */
	gtk_signal_connect (GTK_OBJECT (cqj), "enter_notify",
			    GTK_SIGNAL_FUNC (enter_notify_cb), NULL);
	gtk_signal_connect (GTK_OBJECT (cqj), "leave_notify",
			    GTK_SIGNAL_FUNC (leave_notify_cb), NULL);
}

static void 
enter_notify_cb (CanvasQueryJoin * cqj, gpointer data)
{
	CanvasField *cf;
	GSList *list;

	list = cqj->items;
	while (list) {
		gnome_canvas_item_raise_to_top (GNOME_CANVAS_ITEM (list->data));
		gnome_canvas_item_set (GNOME_CANVAS_ITEM (list->data),
				       "width_units", 3.,
				       "fill_color", "red",
				       NULL);
		list = g_slist_next (list);
	}

	list = cqj->join->pairs;
	while (list) {
		QueryJoinPair *pair = QUERY_JOIN_PAIR_CAST (list->data);
			
		cf = CANVAS_FIELD (canvas_query_view_find_field (CANVAS_QUERY_VIEW (cqj->ant_cview), 
								 pair->ant_field));
		g_assert (cf);
		canvas_field_set_highlight (cf, TRUE);
		cf = CANVAS_FIELD (canvas_query_view_find_field (CANVAS_QUERY_VIEW (cqj->suc_cview), 
								 pair->suc_field));
		g_assert (cf);
		canvas_field_set_highlight (cf, TRUE);
		list = g_slist_next (list);
	}
}

static void 
leave_notify_cb (CanvasQueryJoin * cqj, gpointer data)
{
	CanvasField *cf;
	GSList *list;

	list = cqj->items;
	while (list) {
		gnome_canvas_item_set (GNOME_CANVAS_ITEM (list->data),
				       "width_units", 1.5,
				       "fill_color", "black",
				       NULL);
		list = g_slist_next (list);
	}
	list = cqj->join->pairs;
	while (list) {
		QueryJoinPair *pair = QUERY_JOIN_PAIR_CAST (list->data);
			
		cf = CANVAS_FIELD (canvas_query_view_find_field (CANVAS_QUERY_VIEW (cqj->ant_cview), 
								 pair->ant_field));
		g_assert (cf);
		canvas_field_set_highlight (cf, FALSE);
		cf = CANVAS_FIELD (canvas_query_view_find_field (CANVAS_QUERY_VIEW (cqj->suc_cview), 
								 pair->suc_field));
		g_assert (cf);
		canvas_field_set_highlight (cf, FALSE);
		list = g_slist_next (list);
	}
}

static void
canvas_query_join_destroy (GtkObject * object)
{
	GtkObjectClass *parent_class = NULL;
	CanvasQueryJoin *cqj;

	parent_class = gtk_type_class (canvas_base_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CANVAS_QUERY_JOIN (object));

	cqj = CANVAS_QUERY_JOIN (object);
	if (cqj->props_dlg)
		gnome_dialog_close (cqj->props_dlg);

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void post_init (CanvasQueryJoin * cqj);
static void 
canvas_query_join_set_arg    (GtkObject            *object,
			      GtkArg               *arg,
			      guint                 arg_id)
{
	CanvasQueryJoin *cqj;
	gpointer ptr;

	cqj = CANVAS_QUERY_JOIN (object);

	switch (arg_id) {
	case ARG_QUERY:
		ptr = GTK_VALUE_POINTER (*arg);
		g_assert (IS_QUERY (ptr));
		cqj->query = QUERY (ptr);
		break;
	case ARG_QUERY_JOIN:
		ptr = GTK_VALUE_POINTER (*arg);
		if (ptr) {
			cqj->join = QUERY_JOIN (ptr);
			gnome_canvas_item_set (GNOME_CANVAS_ITEM (cqj),
					       "tooltip_object", cqj->join,
					       NULL);
		}
		break;
	}

	if (cqj->query && cqj->join)
		post_init (cqj);

}

static void 
canvas_query_join_get_arg    (GtkObject            *object,
			GtkArg               *arg,
			guint                 arg_id)
{
	g_print ("GetArg %d\n", arg_id);
}


static void redraw_view_contents (CanvasQueryJoin *cqj);
static void pairs_changed_cb  (GtkObject *obj, GtkObject *obj2, CanvasQueryJoin *cqj);
static void type_card_changed_cb  (GtkObject *obj, CanvasQueryJoin *cqj);
static void join_destroy_cb (GtkObject *obj, CanvasQueryJoin *cqj);
static int  join_item_event(GnomeCanvasItem *ci, GdkEvent *event, CanvasQueryJoin *cqj);
static void 
post_init (CanvasQueryJoin * cqj)
{
	RelShipView *rs;

	/* Finding the CanvasQueryView objects for the two views involved in this
	   join */
	rs = RELSHIP_VIEW (GNOME_CANVAS_ITEM (cqj)->canvas);
	cqj->ant_cview = relship_view_find_query_view (rs, cqj->join->ant_view);
	cqj->suc_cview = relship_view_find_query_view (rs, cqj->join->suc_view);	

	/* Drawing the contents */
	redraw_view_contents (cqj);

	/* Signals to keep the display up to date */
	gtk_signal_connect_while_alive (GTK_OBJECT (cqj->join), "pair_added",
					GTK_SIGNAL_FUNC (pairs_changed_cb), cqj,
					GTK_OBJECT (cqj));
	gtk_signal_connect_while_alive (GTK_OBJECT (cqj->join), "pair_removed",
					GTK_SIGNAL_FUNC (pairs_changed_cb), cqj,
					GTK_OBJECT (cqj));
	gtk_signal_connect_while_alive (GTK_OBJECT (cqj->join), "type_changed",
					GTK_SIGNAL_FUNC (type_card_changed_cb), cqj,
					GTK_OBJECT (cqj));
	gtk_signal_connect_while_alive (GTK_OBJECT (cqj->join), "card_changed",
					GTK_SIGNAL_FUNC (type_card_changed_cb), cqj,
					GTK_OBJECT (cqj));
	gtk_signal_connect_while_alive (GTK_OBJECT (cqj->ant_cview), "moved_cont",
					GTK_SIGNAL_FUNC (type_card_changed_cb), cqj,
					GTK_OBJECT (cqj));
	gtk_signal_connect_while_alive (GTK_OBJECT (cqj->suc_cview), "moved_cont",
					GTK_SIGNAL_FUNC (type_card_changed_cb), cqj,
					GTK_OBJECT (cqj));
	gtk_signal_connect_while_alive (GTK_OBJECT (cqj->ant_cview), "moved",
					GTK_SIGNAL_FUNC (type_card_changed_cb), cqj,
					GTK_OBJECT (cqj));
	gtk_signal_connect_while_alive (GTK_OBJECT (cqj->suc_cview), "moved",
					GTK_SIGNAL_FUNC (type_card_changed_cb), cqj,
					GTK_OBJECT (cqj));
	gtk_signal_connect_while_alive (GTK_OBJECT (cqj->join), "destroy",
					GTK_SIGNAL_FUNC (join_destroy_cb), cqj,
					GTK_OBJECT (cqj));

	gnome_canvas_item_set (GNOME_CANVAS_ITEM (cqj),
			       "allow_move", FALSE, 
			       "allow_drag", FALSE,
			       NULL);

	gtk_signal_connect(GTK_OBJECT (cqj),"event",
			   GTK_SIGNAL_FUNC(join_item_event), cqj);
}

static void 
join_destroy_cb (GtkObject *obj, CanvasQueryJoin *cqj)
{
	gtk_object_destroy (GTK_OBJECT (cqj));
}

static void edit_join_props_cb (GtkWidget *button, CanvasQueryJoin *cqj);
static void delete_join_cb (GtkWidget *button, QueryJoin *qj);
static int  
join_item_event(GnomeCanvasItem *ci, GdkEvent *event, CanvasQueryJoin *cqj)
{
	gboolean done = TRUE;
	GtkWidget *menu, *entry;
	gboolean is_relation = FALSE; /* TRUE if we edit global relations, and FALSE otherwise */

	if (cqj->query == QUERY (cqj->query->conf->top_query))
		is_relation = TRUE;

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (((GdkEventButton*)event)->button == 3) {
			menu = gtk_menu_new ();
			if (is_relation)
				entry = gtk_menu_item_new_with_label (_("Edit relation properties"));
			else
				entry = gtk_menu_item_new_with_label (_("Edit join properties"));
			gtk_signal_connect (GTK_OBJECT (entry), "activate",
					    GTK_SIGNAL_FUNC (edit_join_props_cb), cqj);
			gtk_menu_append (GTK_MENU (menu), entry);
			gtk_widget_show (entry);
			
			if (is_relation)
				entry = gtk_menu_item_new_with_label (_("Delete relation"));
			else
				entry = gtk_menu_item_new_with_label (_("Delete join"));
			gtk_signal_connect (GTK_OBJECT (entry), "activate",
					    GTK_SIGNAL_FUNC (delete_join_cb), cqj->join);
			gtk_menu_append (GTK_MENU (menu), entry);
			gtk_widget_show (entry);
			gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
					NULL, NULL, ((GdkEventButton *)event)->button,
					((GdkEventButton *)event)->time);
		}
		else
			done = FALSE;
		break;
	default:
		done = FALSE;
		break;
	}

	return done;	
}

static void 
pairs_changed_cb  (GtkObject *obj, GtkObject *obj2, CanvasQueryJoin *cqj)
{
	redraw_view_contents (cqj);
}

static void 
type_card_changed_cb  (GtkObject *obj, CanvasQueryJoin *cqj)
{
	redraw_view_contents (cqj);
}

static int pair_item_event(GnomeCanvasItem *center, GdkEvent *event, QueryJoinPair *pair);
static void 
redraw_view_contents (CanvasQueryJoin *cqj)
{
	GnomeCanvasItem *item;
	double x1a, x2a, x1s, x2s, Xa, Xs, Ya, Ys, Xb, Yb, y1, y2, y3, y4;
	GSList *list;
	GnomeCanvasPoints *points;
	double Xao, Xso;
	double sq = 5.;
	guint i;

	/* Removing any item previously displayed */
	list = cqj->items;
	while (list) {
		gtk_object_destroy (GTK_OBJECT (list->data));
		list = g_slist_next (list);
	}
	g_slist_free (cqj->items);
	cqj->items = NULL;

	/* check if join's views have been swapped in the meanwhile; if so, the views in
	 * the canvas item also need to be swapped; UGLY HACK FER this should be transparent */
	if (CANVAS_QUERY_VIEW(cqj->ant_cview)->view == cqj->join->suc_view) {
		GnomeCanvasItem    *tmp;
		tmp = cqj->ant_cview;
		cqj->ant_cview = cqj->suc_cview;
		cqj->suc_cview = tmp;
	}

	/* New drawing */
	if (g_slist_length (cqj->join->pairs)) {
		/* finding the x positions for the join lines: 
		   Xa is the x pos for ant_view
		   Xs is the x pos for suc_view
		*/
		GdkLineStyle style;
		
		/* Lines style for all the pairs */
		if (cqj->join->card == QUERY_JOIN_UNDEFINED)
			style = GDK_LINE_ON_OFF_DASH;
		else
			style = GDK_LINE_SOLID;
		

		gnome_canvas_item_get_bounds (cqj->ant_cview, &x1a, &Ya, &x2a, NULL);
		gnome_canvas_item_get_bounds (cqj->suc_cview, &x1s, &Ys, &x2s, NULL);
		
		if (x1s > x2a) { /* case 1 */
			Xa = x2a;
			Xs = x1s;
			Xao = 2 * sq;
			Xso = -2 * sq;
		}
		else {
			if (x1a >= x2s) { /* case 7 */
				Xa = x1a;
				Xs = x2s;
				Xao = - 2 * sq;
				Xso = 2 * sq;
			}
			else {
				if ((x1a + x2a) < (x1s + x2s)) { /* case 3 */
					Xa = x1a;
					Xs = x1s;
					Xao = -2 * sq;
					Xso = -2 * sq;
				}
				else { /* case 5 */
					Xa = x2a;
					Xs = x2s;
					Xao = 2 * sq;
					Xso = 2 * sq;
				}
			}
		}
		
		Xb = (Xa + Xao + Xs + Xso) / 2.;
		
		
		/* Displaying the lines for the pairs */
		list = cqj->join->pairs;
		i = 0;

		while (list) {
			CanvasField *cf;
			QueryJoinPair *pair = QUERY_JOIN_PAIR_CAST (list->data);

			cf = CANVAS_FIELD (canvas_query_view_find_field (CANVAS_QUERY_VIEW (cqj->ant_cview), 
									 pair->ant_field));
			g_assert (cf);
			gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (cf), NULL, &y1, NULL, &y2);

			cf = CANVAS_FIELD (canvas_query_view_find_field (CANVAS_QUERY_VIEW (cqj->suc_cview),
									 pair->suc_field));
			g_assert (cf);
			gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (cf), NULL, &y3, NULL, &y4);
			Yb = (Ya + (y1 + y2) /2. + Ys + (y3 + y4) /2.) / 2.;

			/* 
			 *  First line 
			 */
			if ((cqj->join->join_type == QUERY_JOIN_RIGHT_OUTER) ||
			    (cqj->join->join_type == QUERY_JOIN_FULL_OUTER)) {
				/* First part */
				points = gnome_canvas_points_new (2);
				points->coords[0] = Xb;
				points->coords[1] = Yb;
				points->coords[2] = Xa + Xao;
				points->coords[3] = Ya + (y1 + y2)/2.;
				item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqj),
							      gnome_canvas_line_get_type(),
							      "points", points,
							      "fill_color", "black",
							      "width_units", 1.5,
							      "cap_style", GDK_CAP_ROUND,
							      "line_style", style,
							      "last_arrowhead", TRUE,
							      "smooth", TRUE,
							      "arrow_shape_a", 3.*sq,
							      "arrow_shape_b", 3.*sq,
							      "arrow_shape_c", sq,
							      NULL);
				cqj->items = g_slist_append (cqj->items, item);
				gnome_canvas_points_free(points);
				gtk_signal_connect(GTK_OBJECT (item),"event",
						   GTK_SIGNAL_FUNC(pair_item_event), pair);
				
				/* Second part */
				points = gnome_canvas_points_new (2);
				points->coords[0] = Xa;
				points->coords[1] = Ya + (y1 + y2)/2.;
				points->coords[2] = Xa + Xao;
				points->coords[3] = points->coords[1];
				item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqj),
							      gnome_canvas_line_get_type(), 
							      "points", points,
							      "fill_color", "black",
							      "width_units", 1.5,
							      "cap_style", GDK_CAP_ROUND,
							      "line_style", style,
							      NULL);
				cqj->items = g_slist_append (cqj->items, item);
				gnome_canvas_points_free(points);
				gtk_signal_connect(GTK_OBJECT (item),"event",
						   GTK_SIGNAL_FUNC(pair_item_event), pair);
			}
			else {
				/* one polyline only */
				points = gnome_canvas_points_new (3);
				points->coords[0] = Xa;
				points->coords[1] = Ya + (y1 + y2)/2.;
				points->coords[2] = Xa + Xao;
				points->coords[3] = points->coords[1];
				points->coords[4] = Xb;
				points->coords[5] = Yb;
				item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqj),
							      gnome_canvas_line_get_type(), 
							      "points", points,
							      "fill_color", "black",
							      "width_units", 1.5,
							      "cap_style", GDK_CAP_ROUND,
							      "line_style", style,
							      NULL);
				cqj->items = g_slist_append (cqj->items, item);
				gnome_canvas_points_free(points);
				gtk_signal_connect(GTK_OBJECT (item),"event",
						   GTK_SIGNAL_FUNC(pair_item_event), pair);
			}
			/* Crow's notation if necessary for ant_field */
			if (cqj->join->card == QUERY_JOIN_N_1) {
				points = gnome_canvas_points_new (3);
				points->coords[0] = Xa;
				points->coords[1] = Ya + y1;
				points->coords[2] = Xa + Xao;
				points->coords[3] = Ya + (y1 + y2)/2.;
				points->coords[4] = Xa;
				points->coords[5] = Ya +  y2;
				item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqj),
							      gnome_canvas_line_get_type(),
							      "points", points,
							      "fill_color", "black",
							      "width_units", 1.5,
							      "cap_style", GDK_CAP_ROUND,
							      NULL);
				cqj->items = g_slist_append (cqj->items, item);
				gnome_canvas_points_free(points);
				gtk_signal_connect(GTK_OBJECT (item),"event",
						   GTK_SIGNAL_FUNC(pair_item_event), pair);
			}
			

			/* 
			 *  Second line 
			 */
			if ((cqj->join->join_type == QUERY_JOIN_LEFT_OUTER) ||
			    (cqj->join->join_type == QUERY_JOIN_FULL_OUTER)) {
				/* First part */
				points = gnome_canvas_points_new (2);
				points->coords[0] = Xb;
				points->coords[1] = Yb;
				points->coords[2] = Xs + Xso;
				points->coords[3] = Ys + (y3 + y4)/2.;
				item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqj),
							      gnome_canvas_line_get_type(),
							      "points", points,
							      "fill_color", "black",
							      "width_units", 1.5,
							      "cap_style", GDK_CAP_ROUND,
							      "line_style", style,
							      "last_arrowhead", TRUE,
							      "smooth", TRUE,
							      "arrow_shape_a", 3.*sq,
							      "arrow_shape_b", 3.*sq,
							      "arrow_shape_c", sq,
							      NULL);
				cqj->items = g_slist_append (cqj->items, item);
				gnome_canvas_points_free(points);
				gtk_signal_connect(GTK_OBJECT (item),"event",
						   GTK_SIGNAL_FUNC(pair_item_event), pair);
				
				/* Second part */
				points = gnome_canvas_points_new (2);
				points->coords[0] = Xs;
				points->coords[1] = Ys + (y3 + y4)/2.;
				points->coords[2] = Xs + Xso;
				points->coords[3] = points->coords[1];
				item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqj),
							      gnome_canvas_line_get_type(), 
							      "points", points,
							      "fill_color", "black",
							      "width_units", 1.5,
							      "cap_style", GDK_CAP_ROUND,
							      "line_style", style,
							      NULL);
				cqj->items = g_slist_append (cqj->items, item);
				gnome_canvas_points_free(points);
				gtk_signal_connect(GTK_OBJECT (item),"event",
						   GTK_SIGNAL_FUNC(pair_item_event), pair);
			}
			else {
				points = gnome_canvas_points_new (3);
				points->coords[0] = Xs;
				points->coords[1] = Ys + (y3 + y4)/2.;
				points->coords[2] = Xs + Xso;
				points->coords[3] = points->coords[1];
				points->coords[4] = Xb;
				points->coords[5] = Yb;
				item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqj),
							      gnome_canvas_line_get_type(),
							      "points", points,
							      "fill_color", "black",
							      "width_units", 1.5,
							      "cap_style", GDK_CAP_ROUND,
							      "line_style", style,
							      NULL);
				cqj->items = g_slist_append (cqj->items, item);
				gnome_canvas_points_free(points);
				gtk_signal_connect(GTK_OBJECT (item),"event",
						   GTK_SIGNAL_FUNC(pair_item_event), pair);
			}
			
			/* Crow's notation if necessary for suc_field */
			if (cqj->join->card == QUERY_JOIN_1_N) {

				points = gnome_canvas_points_new (3);
				points->coords[0] = Xs;
				points->coords[1] = Ys + y3;
				points->coords[2] = Xs + Xso;
				points->coords[3] = Ys + (y3 + y4)/2.;
				points->coords[4] = Xs;
				points->coords[5] = Ys +  y4;
				item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqj),
							      gnome_canvas_line_get_type(),
							      "points", points,
							      "fill_color", "black",
							      "width_units", 1.5,
							      "cap_style", GDK_CAP_ROUND,
							      NULL);
				cqj->items = g_slist_append (cqj->items, item);
				gnome_canvas_points_free(points);
				gtk_signal_connect(GTK_OBJECT (item),"event",
						   GTK_SIGNAL_FUNC(pair_item_event), pair);
			}
			
			list = g_slist_next (list);
			i++;
		}
	}
}



static void delete_join_pair_cb (GtkWidget *button, QueryJoinPair *pair);
static int 
pair_item_event(GnomeCanvasItem *center, GdkEvent *event, QueryJoinPair *pair)
{
	gboolean done = TRUE;
	GtkWidget *menu, *entry;
	CanvasQueryJoin *cqj;
	gboolean is_relation = FALSE; /* TRUE if we edit global relations, and FALSE otherwise */

	cqj = CANVAS_QUERY_JOIN (center->parent);
	if (cqj->query == QUERY (cqj->query->conf->top_query))
		is_relation = TRUE;


	switch (event->type) {
	case GDK_BUTTON_PRESS:
		switch (((GdkEventButton*)event)->button) {
		case 3:
			menu = gtk_menu_new ();
			if (is_relation)
				entry = gtk_menu_item_new_with_label (_("Edit relation properties"));
			else
				entry = gtk_menu_item_new_with_label (_("Edit join properties"));

			gtk_signal_connect (GTK_OBJECT (entry), "activate",
					    GTK_SIGNAL_FUNC (edit_join_props_cb), cqj);
			gtk_menu_append (GTK_MENU (menu), entry);
			gtk_widget_show (entry);

			if (g_slist_length (pair->qj->pairs) > 1) {
				entry = gtk_menu_item_new_with_label (_("Delete this relation only"));
				gtk_signal_connect (GTK_OBJECT (entry), "activate",
						    GTK_SIGNAL_FUNC (delete_join_pair_cb), pair);
				gtk_menu_append (GTK_MENU (menu), entry);
				gtk_widget_show (entry);
			}

			if (is_relation)
				entry = gtk_menu_item_new_with_label (_("Delete relation"));
			else
				entry = gtk_menu_item_new_with_label (_("Delete join"));
			gtk_signal_connect (GTK_OBJECT (entry), "activate",
					    GTK_SIGNAL_FUNC (delete_join_cb), pair->qj);
			gtk_menu_append (GTK_MENU (menu), entry);
			gtk_widget_show (entry);
			gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
					NULL, NULL, ((GdkEventButton *)event)->button,
					((GdkEventButton *)event)->time);
			break;
		}
		break;
	case GDK_2BUTTON_PRESS:
		if (((GdkEventButton*)event)->button == 1) 
			edit_join_props_cb (NULL, cqj);
		break;
	default:
		done = FALSE;
		break;
	}

	return done;	
}

static void 
delete_join_cb (GtkWidget *button, QueryJoin *qj)
{
	query_del_join (qj->query, qj);
}

static void 
delete_join_pair_cb (GtkWidget *button, QueryJoinPair *pair)
{
	query_join_del_pair (pair->qj, pair);
}



/*
 * Join edition GUI
 */
static gchar *type_prop_name (QueryJoinType type);
static void change_join_type_cb (GtkToggleButton *tb, QueryJoin *qj);
static void join_type_changed_cb (QueryJoin *qj, GnomeDialog *dlg);

static gchar *card_prop_name (QueryJoinCard card);
static void change_join_card_cb (GtkToggleButton *tb, QueryJoin *qj);
static void join_card_changed_cb (QueryJoin *qj, GnomeDialog *dlg);

static void props_dlg_clicked_cb (GnomeDialog *dlg, gint button, CanvasQueryJoin *cqj);
static gint props_dlg_close_cb (GnomeDialog *dlg, CanvasQueryJoin *cqj);

static void 
edit_join_props_cb (GtkWidget *button, CanvasQueryJoin *cqj)
{
	if (cqj->props_dlg) 
		gdk_window_raise (GTK_WIDGET (cqj->props_dlg)->window);
	else {
		gboolean is_relation = FALSE; /* TRUE if we edit global relations, and FALSE otherwise */
		gchar *str;
		gchar *name1, *name2;
		GnomeDialog *dlg;
		GtkWidget *label, *frame, *rb, *rb2, *vbox, *cb, *table;
		GtkWidget *window;
	
		if (cqj->query == QUERY (cqj->query->conf->top_query))
			is_relation = TRUE;

		if (is_relation)
			str = _("Relation properties");
		else
			str = _("Join properties");

		/* New dialog */
		dlg = GNOME_DIALOG (gnome_dialog_new (str, GNOME_STOCK_BUTTON_CLOSE, NULL));

		/* A label for title */
		name1 = query_view_get_textual (cqj->join->ant_view);
		name2 = query_view_get_textual (cqj->join->suc_view);
		if (is_relation)
			str = g_strdup_printf (_("Properties of relation between\n"
						 "%s and %s"), name1, name2);
		else
			str = g_strdup_printf (_("Properties of join between\n"
						 "%s and %s"), name1, name2);
		g_free (name1);
		g_free (name2);
		label = gtk_label_new (str);
		gtk_box_pack_start (GTK_BOX (dlg->vbox), label, FALSE, FALSE, GNOME_PAD/2.);

		/* Type of join */
		if (!is_relation) {
			frame = gtk_frame_new (_("Join type"));
			gtk_box_pack_start (GTK_BOX (dlg->vbox), frame, FALSE, FALSE, GNOME_PAD/2.);
			
			table = gtk_table_new (4, 2, FALSE);
			gtk_container_set_border_width (GTK_CONTAINER (table),GNOME_PAD/2.);
			gtk_container_add (GTK_CONTAINER (frame), table);
			
			rb = gtk_radio_button_new_with_label (NULL, _("Inner join"));
			gtk_table_attach_defaults (GTK_TABLE (table), rb, 0, 1, 0, 1);
			gtk_object_set_data (GTK_OBJECT (dlg), type_prop_name (QUERY_JOIN_INNER), rb);
			gtk_object_set_data (GTK_OBJECT (rb), "type", GINT_TO_POINTER (QUERY_JOIN_INNER));
			gtk_signal_connect (GTK_OBJECT (rb), "toggled",
					    GTK_SIGNAL_FUNC (change_join_type_cb), cqj->join);
			
			
			rb2 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (rb), 
									   _("Left outer join"));
			gtk_table_attach_defaults (GTK_TABLE (table), rb2, 0, 1, 1, 2);
			gtk_object_set_data (GTK_OBJECT (dlg), type_prop_name (QUERY_JOIN_LEFT_OUTER), rb2);
			gtk_object_set_data (GTK_OBJECT (rb2), "type", GINT_TO_POINTER (QUERY_JOIN_LEFT_OUTER));
			gtk_signal_connect (GTK_OBJECT (rb2), "toggled",
					    GTK_SIGNAL_FUNC (change_join_type_cb), cqj->join);

		
			rb = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (rb2), 
									  _("Right outer join"));
			gtk_table_attach_defaults (GTK_TABLE (table), rb, 0, 1, 2, 3);
			gtk_object_set_data (GTK_OBJECT (dlg), type_prop_name (QUERY_JOIN_RIGHT_OUTER), rb);
			gtk_object_set_data (GTK_OBJECT (rb), "type", GINT_TO_POINTER (QUERY_JOIN_RIGHT_OUTER));
			gtk_signal_connect (GTK_OBJECT (rb), "toggled",
					    GTK_SIGNAL_FUNC (change_join_type_cb), cqj->join);
			
			rb2 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (rb), 
									   _("Full outer join"));
			gtk_table_attach_defaults (GTK_TABLE (table), rb2, 0, 1, 3, 4);
			gtk_object_set_data (GTK_OBJECT (dlg), type_prop_name (QUERY_JOIN_FULL_OUTER), rb2);
			gtk_object_set_data (GTK_OBJECT (rb2), "type", GINT_TO_POINTER (QUERY_JOIN_FULL_OUTER));
			gtk_signal_connect (GTK_OBJECT (rb2), "toggled",
					    GTK_SIGNAL_FUNC (change_join_type_cb), cqj->join);
			
			label = gtk_label_new ("");
			gtk_object_set_data (GTK_OBJECT (dlg), "typelabel", label);
			
			gtk_table_attach_defaults (GTK_TABLE (table), label, 1, 2, 0, 4);
			gtk_table_set_col_spacing (GTK_TABLE (table), 1, GNOME_PAD *3.);
			join_type_changed_cb (cqj->join, dlg);
		}			
		
		if (is_relation) {
			/* Referential integrity */
			frame = gtk_frame_new (_("Referential integrity"));
			gtk_box_pack_start (GTK_BOX (dlg->vbox), frame, FALSE, FALSE, GNOME_PAD/2.);
			gtk_widget_set_sensitive (frame, FALSE);
			
			vbox = gtk_vbox_new (TRUE, GNOME_PAD/2.);
			gtk_container_set_border_width (GTK_CONTAINER (vbox),GNOME_PAD/2.);
			gtk_container_add (GTK_CONTAINER (frame), vbox);
			
			cb = gtk_check_button_new_with_label (_("Apply"));
			gtk_box_pack_start (GTK_BOX (vbox), cb, FALSE, FALSE, 0);
			
			cb = gtk_check_button_new_with_label (_("Update in cascade"));
			gtk_box_pack_start (GTK_BOX (vbox), cb, FALSE, FALSE, 0);
			
			cb = gtk_check_button_new_with_label (_("Delete in cascade"));
			gtk_box_pack_start (GTK_BOX (vbox), cb, FALSE, FALSE, 0);
		}

		/* Cardinality */
		frame = gtk_frame_new (_("Join cardinality"));
		gtk_box_pack_start (GTK_BOX (dlg->vbox), frame, FALSE, FALSE, GNOME_PAD/2.);
		
		vbox = gtk_vbox_new (TRUE, GNOME_PAD/2.);
		gtk_container_set_border_width (GTK_CONTAINER (vbox),GNOME_PAD/2.);
		gtk_container_add (GTK_CONTAINER (frame), vbox);

		rb = gtk_radio_button_new_with_label (NULL, _("One to One"));
		gtk_box_pack_start (GTK_BOX (vbox), rb, FALSE, FALSE, 0);
		gtk_object_set_data (GTK_OBJECT (dlg), card_prop_name (QUERY_JOIN_1_1), rb);
		gtk_object_set_data (GTK_OBJECT (rb), "card", GINT_TO_POINTER (QUERY_JOIN_1_1));
		gtk_signal_connect (GTK_OBJECT (rb), "toggled",
				    GTK_SIGNAL_FUNC (change_join_card_cb), cqj->join);


		rb2 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (rb), 
								  _("One to Many"));
		gtk_box_pack_start (GTK_BOX (vbox), rb2, FALSE, FALSE, 0);
		gtk_object_set_data (GTK_OBJECT (dlg), card_prop_name (QUERY_JOIN_1_N), rb2);
		gtk_object_set_data (GTK_OBJECT (rb2), "card", GINT_TO_POINTER (QUERY_JOIN_1_N));
		gtk_signal_connect (GTK_OBJECT (rb2), "toggled",
				    GTK_SIGNAL_FUNC (change_join_card_cb), cqj->join);

		rb = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (rb), 
								  _("Many to One"));
		gtk_box_pack_start (GTK_BOX (vbox), rb, FALSE, FALSE, 0);
		gtk_object_set_data (GTK_OBJECT (dlg), card_prop_name (QUERY_JOIN_N_1), rb);
		gtk_object_set_data (GTK_OBJECT (rb), "card", GINT_TO_POINTER (QUERY_JOIN_N_1));
		gtk_signal_connect (GTK_OBJECT (rb), "toggled",
				    GTK_SIGNAL_FUNC (change_join_card_cb), cqj->join);


		rb2 = gtk_radio_button_new_with_label_from_widget (GTK_RADIO_BUTTON (rb), 
								   _("Undefined"));
		gtk_box_pack_start (GTK_BOX (vbox), rb2, FALSE, FALSE, 0);
		gtk_object_set_data (GTK_OBJECT (dlg), card_prop_name (QUERY_JOIN_UNDEFINED), rb2);
		gtk_object_set_data (GTK_OBJECT (rb2), "card", GINT_TO_POINTER (QUERY_JOIN_UNDEFINED));
		gtk_signal_connect (GTK_OBJECT (rb2), "toggled",
				    GTK_SIGNAL_FUNC (change_join_card_cb), cqj->join);
		join_card_changed_cb (cqj->join, dlg);


		/* Signals */
		gtk_signal_connect (GTK_OBJECT (dlg), "clicked",
				    GTK_SIGNAL_FUNC (props_dlg_clicked_cb), cqj);
		gtk_signal_connect (GTK_OBJECT (dlg), "close",
				    GTK_SIGNAL_FUNC (props_dlg_close_cb), cqj);

		gtk_signal_connect_while_alive (GTK_OBJECT (cqj->join), "type_changed",
						GTK_SIGNAL_FUNC (join_type_changed_cb), dlg,
						GTK_OBJECT (dlg));
		gtk_signal_connect_while_alive (GTK_OBJECT (cqj->join), "card_changed",
						GTK_SIGNAL_FUNC (join_card_changed_cb), dlg,
						GTK_OBJECT (dlg));
		cqj->props_dlg = dlg;

		window = gtk_widget_get_ancestor (GTK_WIDGET (GNOME_CANVAS_ITEM (cqj)->canvas), 
						  gtk_window_get_type ());
		gnome_dialog_set_parent (dlg, GTK_WINDOW (window));

		gtk_widget_show_all (GTK_WIDGET (dlg));
	}
}

/*
 * Dialog management
 */

static void 
props_dlg_clicked_cb (GnomeDialog *dlg, gint button, CanvasQueryJoin *cqj)
{
	gnome_dialog_close (dlg);
}

static gint 
props_dlg_close_cb (GnomeDialog *dlg, CanvasQueryJoin *cqj)
{
	cqj->props_dlg = NULL;
	return 0;
}


/*
 * Type of join management
 */

static gchar *
type_prop_name (QueryJoinType type)
{
	gchar *str;
	
	switch (type) {
	case QUERY_JOIN_INNER:
		str = "typein";
		break;
	case QUERY_JOIN_LEFT_OUTER:
		str = "typelo";
		break;
	case QUERY_JOIN_RIGHT_OUTER:
		str = "typero";
		break;
	case QUERY_JOIN_FULL_OUTER:
		str = "typefo";
		break;
	default:
		str = "type??";
		break;
	}

	return str;
}


/* Update the GUI because of a change in the join */
static void set_type_handlers (QueryJoin *qj, GnomeDialog *dlg, gboolean active);
static void 
join_type_changed_cb (QueryJoin *qj, GnomeDialog *dlg)
{
	GtkWidget *tb, *label;
	gchar *str, *ent1, *ent2;

	tb = gtk_object_get_data (GTK_OBJECT (dlg), type_prop_name (qj->join_type));
	set_type_handlers (qj, dlg, FALSE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (tb), TRUE);
	set_type_handlers (qj, dlg, TRUE);

	label = gtk_object_get_data (GTK_OBJECT (dlg), "typelabel");
	ent1 = query_view_get_textual (qj->ant_view);
	ent2 = query_view_get_textual (qj->suc_view);
	switch (qj->join_type) {
	case QUERY_JOIN_INNER:
		str = g_strdup_printf (_("Select only the records\nof the two entities\n"
					 "('%s' and '%s')\nwhere the fields are equal."), 
				       ent1, ent2);
		break;
	case QUERY_JOIN_LEFT_OUTER:
		str = g_strdup_printf (_("Select all the records\nof '%s'\n"
					 "and those of '%s'\nwhere the fields\n"
					 "are equal."), ent1, ent2);
		break;
	case QUERY_JOIN_RIGHT_OUTER:
		str = g_strdup_printf (_("Select all the records\nof '%s'\n"
					 "and those of '%s'\nwhere the fields\n"
					 "are equal."), ent2, ent1);
		break;
	case QUERY_JOIN_FULL_OUTER:
		str = g_strdup_printf (_("Select all the records\nof '%s'\n"
					 "and those of '%s'\nlinking the two\n"
					 "when the fields are equal."), ent1, ent2);
		break;
	default:
		str = g_strdup ("ERROR!!!");
		break;
	}
	g_free (ent1);
	g_free (ent2);

	gtk_label_set_text (GTK_LABEL (label), str);
	g_free (str);
}

static void 
set_type_handlers (QueryJoin *qj, GnomeDialog *dlg, gboolean active)
{
	QueryJoinType type;
	GtkObject *obj;
	
	for (type = QUERY_JOIN_INNER; type <= QUERY_JOIN_FULL_OUTER; type++) {
		obj = gtk_object_get_data (GTK_OBJECT (dlg), type_prop_name (type));
		if (obj) {
			if (active)
				gtk_signal_handler_unblock_by_func (obj, 
								    GTK_SIGNAL_FUNC (change_join_type_cb),
								    qj);
			else
				gtk_signal_handler_block_by_func (obj, 
								  GTK_SIGNAL_FUNC (change_join_type_cb),
								  qj);
		}
	}
}

/* Update the join because of a change in the GUI */
static void 
change_join_type_cb (GtkToggleButton *tb, QueryJoin *qj)
{
	QueryJoinType type;

	type = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (tb), "type"));
	if (gtk_toggle_button_get_active (tb)) 
		query_join_set_join_type (qj, type);
}






/*
 * Cardinality of join management
 */

static gchar *
card_prop_name (QueryJoinCard card)
{
	gchar *str;
	
	switch (card) {
	case QUERY_JOIN_1_1:
		str = "card11";
		break;
	case QUERY_JOIN_1_N:
		str = "card1N";
		break;
	case QUERY_JOIN_N_1:
		str = "cardN1";
		break;
	case QUERY_JOIN_UNDEFINED:
		str = "cardUN";
		break;
	default:
		str = "card??";
		break;
	}

	return str;
}

/* Update the GUI because of a change in the join */
static void set_card_handlers (QueryJoin *qj, GnomeDialog *dlg, gboolean active);
static void 
join_card_changed_cb (QueryJoin *qj, GnomeDialog *dlg)
{
	GtkWidget *tb;

	tb = gtk_object_get_data (GTK_OBJECT (dlg), card_prop_name (qj->card));
	set_card_handlers (qj, dlg, FALSE);
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (tb), TRUE);
	set_card_handlers (qj, dlg, TRUE);
}

static void 
set_card_handlers (QueryJoin *qj, GnomeDialog *dlg, gboolean active)
{
	QueryJoinCard card;
	GtkObject *obj;
	
	for (card = QUERY_JOIN_1_1; card <= QUERY_JOIN_UNDEFINED; card++) {
		obj = gtk_object_get_data (GTK_OBJECT (dlg), card_prop_name (card));
		if (obj) {
			if (active)
				gtk_signal_handler_unblock_by_func (obj, 
								    GTK_SIGNAL_FUNC (change_join_card_cb),
								    qj);
			else
				gtk_signal_handler_block_by_func (obj, 
								  GTK_SIGNAL_FUNC (change_join_card_cb),
								  qj);
		}
	}
}

/* Update the join because of a change in the GUI */
static void 
change_join_card_cb (GtkToggleButton *tb, QueryJoin *qj)
{
	QueryJoinCard card;

	card = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (tb), "card"));
	if (gtk_toggle_button_get_active (tb)) 
		query_join_set_card (qj, card);
}
