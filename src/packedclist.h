/* packedclist.h
 *
 * Copyright (C) 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __PACKED_CLIST__
#define __PACKED_CLIST__

#include <gnome.h>
#include <config.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define PACKED_CLIST(obj)          GTK_CHECK_CAST (obj, packed_clist_get_type(), PackedCList)
#define PACKED_CLIST_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, packed_clist_get_type (), PackedCListClass)
#define IS_PACKED_CLIST(obj)       GTK_CHECK_TYPE (obj, packed_clist_get_type ())


	typedef struct _PackedCList PackedCList;
	typedef struct _PackedCListClass PackedCListClass;


	/* struct for the object's data */
	struct _PackedCList
	{
		GtkHBox    object;

		GtkWidget*  clist;
		GtkWidget*  arrows_box; 
		GtkWidget*  arrow_up;
		GtkWidget*  arrow_down;
		gpointer    actual_selection;
	};

	/* struct for the object's class */
	struct _PackedCListClass
	{
		GtkHBoxClass parent_class;

		void (*objects_swapped) (PackedCList * clist, gpointer obj1, gpointer obj2);
	};

	/* generic widget's functions */
	guint      packed_clist_get_type (void);
	GtkWidget* packed_clist_new (gint columns, gboolean buttons_go_right);
	GtkWidget* packed_clist_new_with_titles (gint columns, gchar *titles[],
						 gboolean buttons_go_right);

	void       packed_clist_set_show_arrows (PackedCList *clist, gboolean show);
	void       packed_clist_refresh_arrows (PackedCList *clist);
#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
