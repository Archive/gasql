/* gasql_xml.h
 *
 * Copyright (C) 1999 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef _GASQL_XML_H
#define _GASQL_XML_H

#include <config.h>
#include <gnome.h>
#include <tree.h>		/* libxml/tree.h ? */
#include <parser.h>		/* libxml/tree.h ? */

/* This funtion removes ANY space, \n or \t from an allocated
   string:
   will reallocate a string, the one given as argument will be 
   freed */
gchar *gasql_xml_clean_string (gchar * str);

/* This funtion removes ANY space, \n or \t from an allocated
   string at the beginning or the end of a string:
   will reallocate a string, the one given as argument will be 
   freed */
gchar *gasql_xml_clean_string_ends (gchar * str);

#endif
