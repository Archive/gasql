/* sqlwiddbtree.c
 *
 * Copyright (C) 1999 - 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "sqlwiddbtree.h"
#include "query.h"

static void sql_wid_db_tree_class_init (SqlWidDbTreeClass * class);
static void sql_wid_db_tree_init (SqlWidDbTree * wid);
static void sql_wid_db_tree_initialize (SqlWidDbTree * wid);
static GtkTreeItem *find_tree_item_from_data_ptr (GtkTree * tree,
						  gpointer * ptr);
static void signals_cb (GtkObject * obj, SqlWidDbTree *wid);

/* GUI CB */
static void sql_wid_db_tree_add_table_cb (GtkObject * obj,
					  DbTable * table, gpointer data);
static void sql_wid_db_tree_drop_table_cb (GtkObject * obj,
					   DbTable * table,
					   gpointer data);
static void sql_wid_db_tree_add_seq_cb (GtkObject * obj,
					DbSequence * seq, gpointer data);
static void sql_wid_db_tree_drop_seq_cb (GtkObject * obj,
					 DbSequence * seq, gpointer data);
static void sql_wid_db_tree_add_field_cb (GtkObject * obj,
					  DbTable * table,
					  DbField * field, gpointer data);
static void sql_wid_db_tree_drop_field_cb (GtkObject * obj,
					   DbTable * table,
					   DbField * field,
					   gpointer data);
static void sql_wid_db_tree_query_added_cb (ConfManager *conf, 
					    GtkObject * new_query, SqlWidDbTree *wid);
static void sql_wid_db_tree_query_removed_cb (ConfManager *conf, 
					      GtkObject * old_query, SqlWidDbTree *wid);


static void init_tables (SqlWidDbTree * wid);
static void init_seqs (SqlWidDbTree * wid);
static void init_queries (SqlWidDbTree * wid);


/* signals */
enum
{
	FIELD_SELECTED_SIGNAL,
	TABLE_SELECTED_SIGNAL,
	SEQ_SELECTED_SIGNAL,
	LAST_SIGNAL
};

static gint sql_wid_db_tree_signals[LAST_SIGNAL] = { 0, 0, 0 };


guint
sql_wid_db_tree_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Sql_Wid_Db_Tree",
			sizeof (SqlWidDbTree),
			sizeof (SqlWidDbTreeClass),
			(GtkClassInitFunc) sql_wid_db_tree_class_init,
			(GtkObjectInitFunc) sql_wid_db_tree_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_vbox_get_type (), &f_info);
	}

	return f_type;
}

static void
sql_wid_db_tree_class_init (SqlWidDbTreeClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	sql_wid_db_tree_signals[FIELD_SELECTED_SIGNAL] =
		gtk_signal_new ("field_selected",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (SqlWidDbTreeClass,
						   field_selected),
				gtk_marshal_NONE__POINTER_POINTER,
				GTK_TYPE_NONE, 2, GTK_TYPE_POINTER,
				GTK_TYPE_POINTER);
	sql_wid_db_tree_signals[TABLE_SELECTED_SIGNAL] =
		gtk_signal_new ("table_selected", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (SqlWidDbTreeClass,
						   table_selected),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	sql_wid_db_tree_signals[SEQ_SELECTED_SIGNAL] =
		gtk_signal_new ("seq_selected",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (SqlWidDbTreeClass,
						   seq_selected),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	gtk_object_class_add_signals (object_class, sql_wid_db_tree_signals,
				      LAST_SIGNAL);
	class->field_selected = NULL;
	class->table_selected = NULL;
	class->seq_selected = NULL;
}

static void
sql_wid_db_tree_init (SqlWidDbTree * wid)
{
	/* scrolled window */
	wid->sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (wid->sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (wid), wid->sw, TRUE, TRUE, 0);
	gtk_widget_show (wid->sw);

	/* the tree root */
	wid->tree = gtk_tree_new ();
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (wid->sw),
					       wid->tree);
	gtk_tree_set_selection_mode (GTK_TREE (wid->tree),
				     GTK_SELECTION_SINGLE);
	gtk_widget_show (wid->tree);
	gtk_signal_connect (GTK_OBJECT (wid->tree), "selection_changed",
			    GTK_SIGNAL_FUNC (signals_cb), wid);

	wid->tables_ti = gtk_tree_item_new_with_label (_("Tables"));
	gtk_tree_append (GTK_TREE (wid->tree), wid->tables_ti);
	gtk_widget_show (wid->tables_ti);

	wid->sequences_ti = gtk_tree_item_new_with_label (_("Sequences"));
	gtk_tree_append (GTK_TREE (wid->tree), wid->sequences_ti);
	gtk_tree_item_collapse (GTK_TREE_ITEM (wid->sequences_ti));
	gtk_widget_show (wid->sequences_ti);

	wid->queries_ti = gtk_tree_item_new_with_label (_("Queries"));
	gtk_tree_append (GTK_TREE (wid->tree), wid->queries_ti);
	gtk_tree_item_collapse (GTK_TREE_ITEM (wid->queries_ti));
	gtk_widget_show (wid->queries_ti);
	

	wid->tables = NULL;
	wid->sequences = NULL;
	wid->queries = NULL;
	
	wid->mode = SQL_WID_DB_TREE_SEQS |
		SQL_WID_DB_TREE_TABLES |
		SQL_WID_DB_TREE_TABLE_FIELDS |
		SQL_WID_DB_TREE_SEQS |
		SQL_WID_DB_TREE_SEQS_SEL |
		SQL_WID_DB_TREE_TABLES_SEL | 
		SQL_WID_DB_TREE_TABLE_FIELDS_SEL;
}

GtkWidget *
sql_wid_db_tree_new (ConfManager *conf)
{
	GtkObject *obj;
	SqlWidDbTree *wid;

	obj = gtk_type_new (sql_wid_db_tree_get_type ());
	wid = SQL_WID_DB_TREE (obj);
	wid->conf = conf;
	wid->selection = NULL;

	sql_wid_db_tree_initialize (wid);
	gtk_widget_set_usize (GTK_WIDGET (wid), 125, 200);

	return GTK_WIDGET (obj);
}

static void
init_seqs (SqlWidDbTree * wid)
{
	GSList *list;

	list = wid->conf->db->sequences;
	while (list) {
		sql_wid_db_tree_add_seq_cb (GTK_OBJECT (wid->conf->db),
					    DB_SEQUENCE (list->data), wid);
		list = g_slist_next (list);
	}
}

static void
init_tables (SqlWidDbTree * wid)
{
	GSList *list, *l2;

	/* init the tables and the fields */
	list = wid->conf->db->tables;
	while (list) {
		sql_wid_db_tree_add_table_cb (GTK_OBJECT (wid->conf->db),
					      DB_TABLE (list->data),
					      wid);
		l2 = DB_TABLE (list->data)->fields;
		while (l2) {
			sql_wid_db_tree_add_field_cb (GTK_OBJECT (wid->conf->db),
						      DB_TABLE (list->
								     data),
						      DB_FIELD (l2->
								     data),
						      wid);
			l2 = g_slist_next (l2);
		}
		list = g_slist_next (list);
	}
}

static void
init_queries (SqlWidDbTree * wid)
{
	/* TODO */
}

static void
sql_wid_db_tree_initialize (SqlWidDbTree * wid)
{
	init_tables (wid);
	init_seqs (wid);
	init_queries (wid);

	/* signals */
	gtk_signal_connect_while_alive (GTK_OBJECT (wid->conf->db), "table_created",
					GTK_SIGNAL_FUNC	(sql_wid_db_tree_add_table_cb), wid,
					GTK_OBJECT (wid));
	gtk_signal_connect_while_alive (GTK_OBJECT (wid->conf->db), "table_dropped",
					GTK_SIGNAL_FUNC	(sql_wid_db_tree_drop_table_cb), wid,
					GTK_OBJECT (wid));
	gtk_signal_connect_while_alive (GTK_OBJECT (wid->conf->db), "seq_created",
					GTK_SIGNAL_FUNC	(sql_wid_db_tree_add_seq_cb), wid,
					GTK_OBJECT (wid));
	gtk_signal_connect_while_alive (GTK_OBJECT (wid->conf->db), "seq_dropped",
					GTK_SIGNAL_FUNC	(sql_wid_db_tree_drop_seq_cb), wid,
					GTK_OBJECT (wid));
	gtk_signal_connect_while_alive (GTK_OBJECT (wid->conf->db), "field_created",
					GTK_SIGNAL_FUNC	(sql_wid_db_tree_add_field_cb), wid,
					GTK_OBJECT (wid));
	gtk_signal_connect_while_alive (GTK_OBJECT (wid->conf->db), "field_dropped",
					GTK_SIGNAL_FUNC	(sql_wid_db_tree_drop_field_cb), wid,
					GTK_OBJECT (wid));

	gtk_signal_connect_while_alive (GTK_OBJECT (wid->conf), "query_added",
					GTK_SIGNAL_FUNC	(sql_wid_db_tree_query_added_cb), wid,
					GTK_OBJECT (wid));
	gtk_signal_connect_while_alive (GTK_OBJECT (wid->conf), "query_removed",
					GTK_SIGNAL_FUNC	(sql_wid_db_tree_query_removed_cb), wid,
					GTK_OBJECT (wid));
}

/*****************************************************************************/

/* this CB is intended to be connected to the "table_created" signal of the 
   Database objects it represents.  So:
   - obj is a Database object
   - data is the recepting SqlWidDbTree object 
*/
void
sql_wid_db_tree_add_table_cb (GtkObject * obj, DbTable * table,
			      gpointer data)
{
	GtkWidget *ti;
	SqlWidDbTree *wid = SQL_WID_DB_TREE (data);
	GList *list;
	guint pos;
	gboolean here = FALSE;
	gchar *str;

	/* mode test */
	if (wid->mode & SQL_WID_DB_TREE_TABLES) {
		ti = gtk_tree_item_new_with_label (table->name);
		gtk_object_set_data (GTK_OBJECT (ti), "objptr", table);
		gtk_widget_show (ti);

		/* the Tables Tree */
		if (!wid->tables) {
			wid->tables = gtk_tree_new ();
			gtk_tree_item_set_subtree (GTK_TREE_ITEM
						   (wid->tables_ti),
						   wid->tables);
			gtk_widget_show (wid->tables);
			gtk_tree_item_expand (GTK_TREE_ITEM (wid->tables_ti));
		}

		/* The new table is inserted regarding its alphabetical order */
		list = gtk_container_children (GTK_CONTAINER (wid->tables));
		pos = 0;
		while (list && !here) {
			gtk_label_get (GTK_LABEL
				       (GTK_BIN (list->data)->child), &str);
			if (strcmp (table->name, str) < 0)
				here = TRUE;
			else
				pos++;
			list = g_list_next (list);
		}
		gtk_tree_insert (GTK_TREE (wid->tables), ti, pos);
		g_list_free (list);
	}
}

/* this CB is intended to be connected to the "table_dropped" signal of the 
   Database objects it represents.  So:
   - obj is a Database object
   - data is the recepting SqlWidDbTree object 
*/
void
sql_wid_db_tree_drop_table_cb (GtkObject * obj, DbTable * table,
			       gpointer data)
{
	GtkTreeItem *ti;
	gboolean found;
	GList *list, *list_head;
	GSList *sl;
	gpointer tabledata;
	SqlWidDbTree *wid = SQL_WID_DB_TREE (data);

	/* check if a field of that table was selected */
	sl = table->fields;
	found = FALSE;
	while (sl && !found) {
		if (sl->data == wid->selection)
			found = TRUE;
		else
			sl = g_slist_next (sl);
	}
	if (found && (wid->mode & SQL_WID_DB_TREE_TABLE_FIELDS)) {
		wid->selection = NULL;
#ifdef debug_signal
		g_print (">> 'FIELD_SELECTED_SIGNAL' from sql_wid_db_tree_drop_table_cb\n");
#endif
		gtk_signal_emit (GTK_OBJECT (wid),
				 sql_wid_db_tree_signals
				 [FIELD_SELECTED_SIGNAL], NULL, NULL);
#ifdef debug_signal
		g_print ("<< 'FIELD_SELECTED_SIGNAL' from sql_wid_db_tree_drop_table_cb\n");
#endif
	}
	/* check if the table was selected */
	if (wid->selection == table) {
		wid->selection = NULL;
#ifdef debug_signal
		g_print (">> 'TABLE_SELECTED_SIGNAL' from sql_wid_db_tree_drop_table_cb\n");
#endif
		gtk_signal_emit (GTK_OBJECT (wid),
				 sql_wid_db_tree_signals
				 [TABLE_SELECTED_SIGNAL], NULL);
#ifdef debug_signal
		g_print ("<< 'TABLE_SELECTED_SIGNAL' from sql_wid_db_tree_drop_table_cb\n");
#endif
	}

	/* mode test */
	if (wid->mode & SQL_WID_DB_TREE_TABLES) {
		/* find the GtkTreeItem for that data */
		list_head =
			gtk_container_children (GTK_CONTAINER (wid->tables));
		list = list_head;
		found = FALSE;
		while (list && !found) {
			ti = GTK_TREE_ITEM (list->data);
			tabledata =
				gtk_object_get_data (GTK_OBJECT (ti),
						     "objptr");
			if (tabledata == table) {
				GList *torem = NULL;

				found = TRUE;
				torem = g_list_append (torem, ti);
				gtk_tree_remove_items (GTK_TREE (wid->tables),
						       torem);
			}
			else
				list = g_list_next (list);
		}
		if (g_list_length (list_head) == 1) {
			wid->tables = NULL;
		}
		g_list_free (list_head);
	}
}

/* this CB is intended to be connected to the "field_created" signal of the 
   Database objects it represents.  So:
   - obj is a Database object
   - data is the recepting SqlWidDbTree object 
*/
void
sql_wid_db_tree_add_field_cb (GtkObject * obj, DbTable * table,
			      DbField * field, gpointer data)
{
	GtkTreeItem *tabti;
	SqlWidDbTree *wid = SQL_WID_DB_TREE (data);
	GtkWidget *ti, *tree;

	/* mode test */
	if (wid->mode & SQL_WID_DB_TREE_TABLE_FIELDS) {
		tabti = find_tree_item_from_data_ptr (GTK_TREE (wid->tables),
						      (gpointer) table);
		if (tabti) {
			if (!tabti->subtree) {	/* first create a subtree */
				tree = gtk_tree_new ();
				gtk_tree_item_set_subtree (tabti, tree);
				gtk_widget_show (tree);
				gtk_tree_item_expand (tabti);
			}
			/* insert the field */
			tree = tabti->subtree;
			ti = gtk_tree_item_new_with_label (field->name);
			gtk_object_set_data (GTK_OBJECT (ti), "objptr",
					     field);
			gtk_tree_append (GTK_TREE (tree), ti);
			gtk_widget_show (ti);
			gtk_tree_item_collapse (tabti);
		}
	}
}

/* this CB is intended to be connected to the "field_dropped" signal of the 
   Database objects it represents.  So:
   - obj is a Database object
   - data is the recepting SqlWidDbTree object 
*/
void
sql_wid_db_tree_drop_field_cb (GtkObject * obj, DbTable * table,
			       DbField * field, gpointer data)
{
	GtkTreeItem *tabti, *ti;
	SqlWidDbTree *wid = SQL_WID_DB_TREE (data);
	GtkWidget *tree;

	/* mode test */
	if (wid->mode & SQL_WID_DB_TREE_TABLE_FIELDS) {
		tabti = find_tree_item_from_data_ptr (GTK_TREE (wid->tables),
						      (gpointer) table);
		if (tabti) {
			tree = tabti->subtree;
			if (tree) {
				ti = find_tree_item_from_data_ptr (GTK_TREE
								   (tree),
								   (gpointer)
								   field);
				if (ti) {
					GList *list = NULL;

					list = g_list_append (list, ti);
					gtk_tree_remove_items (GTK_TREE
							       (tree), list);
					/*gtk_container_remove(GTK_CONTAINER(tree), GTK_WIDGET(ti)); */
				}
			}
		}
		if (wid->selection == field) {
			wid->selection = NULL;
#ifdef debug_signal
			g_print (">> 'FIELD_SELECTED_SIGNAL' "
				 "from sql_wid_db_tree_drop_field_cb\n");
#endif
			gtk_signal_emit (GTK_OBJECT (wid),
					 sql_wid_db_tree_signals
					 [FIELD_SELECTED_SIGNAL], NULL, NULL);
#ifdef debug_signal
			g_print ("<< 'FIELD_SELECTED_SIGNAL' "
				 "from sql_wid_db_tree_drop_field_cb\n");
#endif
		}
	}
}

/* this CB is intended to be connected to the "seq_created" signal of the 
   Database objects it represents.  So:
   - obj is a Database object
   - data is the recepting SqlWidDbTree object 
*/
void
sql_wid_db_tree_add_seq_cb (GtkObject * obj, DbSequence * seq, gpointer data)
{
	GtkWidget *ti;
	SqlWidDbTree *wid = SQL_WID_DB_TREE (data);
	GList *list;
	guint pos;
	gboolean here = FALSE;
	gchar *str;

	/* mode test */
	if (wid->mode & SQL_WID_DB_TREE_SEQS) {
		ti = gtk_tree_item_new_with_label (seq->name);
		gtk_object_set_data (GTK_OBJECT (ti), "objptr", seq);
		gtk_widget_show (ti);

		/* the Sequences Tree */
		if (!wid->sequences) {
			wid->sequences = gtk_tree_new ();
			gtk_tree_item_set_subtree (GTK_TREE_ITEM
						   (wid->sequences_ti),
						   wid->sequences);
			gtk_widget_show (wid->sequences);
			gtk_tree_item_expand (GTK_TREE_ITEM
					      (wid->sequences_ti));
		}

		/* The new seq is inserted regarding its alphabetical order */
		list = gtk_container_children (GTK_CONTAINER
					       (wid->sequences));
		pos = 0;
		while (list && !here) {
			gtk_label_get (GTK_LABEL
				       (GTK_BIN (list->data)->child), &str);
			if (strcmp (seq->name, str) < 0)
				here = TRUE;
			else
				pos++;
			list = g_list_next (list);
		}
		gtk_tree_insert (GTK_TREE (wid->sequences), ti, pos);
		g_list_free (list);
	}
}

/* this CB is intended to be connected to the "seq_dropped" signal of the 
   Database objects it represents.  So:
   - obj is a Database object
   - data is the recepting SqlWidDbTree object 
*/
void
sql_wid_db_tree_drop_seq_cb (GtkObject * obj, DbSequence * seq, gpointer data)
{
	GtkTreeItem *ti;
	gboolean found = FALSE;
	GList *list, *list_head;
	gpointer seqdata;
	SqlWidDbTree *wid = SQL_WID_DB_TREE (data);

	/* mode test */
	if (wid->mode & SQL_WID_DB_TREE_SEQS) {
		/* find the GtkTreeItem for that data */
		list_head =
			gtk_container_children (GTK_CONTAINER
						(wid->sequences));
		list = list_head;
		while (list && !found) {
			ti = GTK_TREE_ITEM (list->data);
			seqdata =
				gtk_object_get_data (GTK_OBJECT (ti),
						     "objptr");
			if (seqdata == seq) {
				GList *list2 = NULL;

				found = TRUE;
				list2 = g_list_append (list2, ti);
				gtk_tree_remove_items (GTK_TREE
						       (wid->sequences),
						       list2);
				/*gtk_container_remove(GTK_CONTAINER(wid->sequences),GTK_WIDGET(ti)); */
			}
			list = g_list_next (list);
		}
		if (g_list_length (list_head) == 1) {
			wid->sequences = NULL;
		}
		g_list_free (list_head);

		if (wid->selection == seq) {
			wid->selection = NULL;
#ifdef debug_signal
			g_print (">> 'SEQ_SELECTED_SIGNAL' "
				 "from sql_wid_db_tree_drop_seq_cb\n");
#endif
			gtk_signal_emit (GTK_OBJECT (wid),
					 sql_wid_db_tree_signals
					 [SEQ_SELECTED_SIGNAL], NULL);
#ifdef debug_signal
			g_print ("<< 'SEQ_SELECTED_SIGNAL' "
				 "from sql_wid_db_tree_drop_seq_cb\n");
#endif
		}
	}
}

static void 
sql_wid_db_tree_query_added_cb (ConfManager *conf, GtkObject * new_query, SqlWidDbTree *wid)
{
	GtkWidget *ti;
	GList *list;
	Query *q = QUERY (new_query);

	/* mode test */
	if (wid->mode & SQL_WID_DB_TREE_QUERIES) {
		ti = gtk_tree_item_new_with_label (q->name);
		gtk_object_set_data (GTK_OBJECT (ti), "objptr", q);
		gtk_widget_show (ti);

		/* the Queries Tree */
		if (!wid->sequences) {
			wid->queries = gtk_tree_new ();
			gtk_tree_item_set_subtree (GTK_TREE_ITEM (wid->queries_ti), wid->queries);
			gtk_widget_show (wid->queries);
			gtk_tree_item_expand (GTK_TREE_ITEM (wid->queries_ti));
		}

		gtk_tree_append (GTK_TREE (wid->queries), ti);
		g_list_free (list);
	}
}

static void
sql_wid_db_tree_query_removed_cb (ConfManager *conf, GtkObject * new_query, SqlWidDbTree *wid)
{
	/* FIXME */
}

static GtkTreeItem *
find_tree_item_from_data_ptr (GtkTree * tree, gpointer * ptr)
{
	GList *list, *list_head;
	GtkTreeItem *ti;
	gpointer data, found = NULL;;

	if (tree) {
		list_head = gtk_container_children (GTK_CONTAINER (tree));
		list = list_head;
		while (list && !found) {
			ti = GTK_TREE_ITEM (list->data);
			data = gtk_object_get_data (GTK_OBJECT (ti),
						    "objptr");
			if (data == ptr)
				found = list->data;
			list = g_list_next (list);
		}
		g_list_free (list_head);
	}
	return found;
}


/****************************************************************************/
void
sql_wid_db_tree_set_mode (SqlWidDbTree * wid, guint mode)
{
	guint mm = mode;
	guint om = wid->mode;


	/* if SQL_WID_DB_TREE_TABLE_FIELDS then SQL_WID_DB_TREE_TABLES */
	if ((mm & SQL_WID_DB_TREE_TABLE_FIELDS) && !(mm & SQL_WID_DB_TREE_TABLES)) {
		mm += SQL_WID_DB_TREE_TABLES;
	}
	wid->mode = mm;

	/* update widget with new mode */
	/* tables */
	if ((mm & SQL_WID_DB_TREE_TABLES) && !(om & SQL_WID_DB_TREE_TABLES)) {
		wid->tables_ti = gtk_tree_item_new_with_label (_("Tables"));
		gtk_tree_append (GTK_TREE (wid->tree), wid->tables_ti);
		gtk_widget_show (wid->tables_ti);
		init_tables (wid);
	}

	if (!(mm & SQL_WID_DB_TREE_TABLES) && (om & SQL_WID_DB_TREE_TABLES)) {
		gtk_container_remove (GTK_CONTAINER (wid->tree),
				      wid->tables_ti);
		wid->tables = NULL;
		wid->tables_ti = NULL;
	}

	/* fields */
	if (((mm & SQL_WID_DB_TREE_TABLE_FIELDS) &&
	     !(om & SQL_WID_DB_TREE_TABLE_FIELDS)) ||
	    (!(mm & SQL_WID_DB_TREE_TABLE_FIELDS)
	     && (om & SQL_WID_DB_TREE_TABLE_FIELDS))) {
		gtk_container_remove (GTK_CONTAINER (wid->tree),
				      wid->tables_ti);
		wid->tables = NULL;
		wid->tables_ti = NULL;
		wid->tables_ti = gtk_tree_item_new_with_label (_("Tables"));
		gtk_tree_append (GTK_TREE (wid->tree), wid->tables_ti);
		gtk_widget_show (wid->tables_ti);
		init_tables (wid);
	}

	/* sequences */
	if ((mm & SQL_WID_DB_TREE_SEQS) && !(om & SQL_WID_DB_TREE_SEQS)) {
		wid->sequences_ti =
			gtk_tree_item_new_with_label (_("Sequences"));
		gtk_tree_append (GTK_TREE (wid->tree), wid->sequences_ti);
		gtk_tree_item_collapse (GTK_TREE_ITEM (wid->sequences_ti));
		gtk_widget_show (wid->sequences_ti);
		init_seqs (wid);
	}

	if (!(mm & SQL_WID_DB_TREE_SEQS) && (om & SQL_WID_DB_TREE_SEQS)) {
		gtk_container_remove (GTK_CONTAINER (wid->tree),
				      wid->sequences_ti);
		wid->sequences = NULL;
		wid->sequences_ti = NULL;
	}

}

/****************************************************************************/
/* this CB handles the emission of all the signals for this widget */
static void
signals_cb (GtkObject * obj, SqlWidDbTree *wid)
{
	GList *sel, *list, *list2, *hold;
	GtkTree *tree = GTK_TREE (obj);
	gboolean found;
	gpointer retval, table;

	sel = GTK_TREE_SELECTION (GTK_TREE (obj));

	/* in any case we want to emit a signal to tell the previous selection is no more */
	if (IS_DB_SEQUENCE (wid->selection) && (wid->mode & SQL_WID_DB_TREE_SEQS_SEL)) {
#ifdef debug_signal
		g_print (">> 'SEQ_SELECTED_SIGNAL' from sqlwiddbtree->signals_cb (NULL)\n");
#endif
		gtk_signal_emit (GTK_OBJECT (wid), 
				 sql_wid_db_tree_signals[SEQ_SELECTED_SIGNAL], NULL);
#ifdef debug_signal
		g_print ("<< 'SEQ_SELECTED_SIGNAL' from sqlwiddbtree->signals_cb\n");
#endif		
	}

	if (IS_DB_TABLE (wid->selection) && (wid->mode & SQL_WID_DB_TREE_TABLES_SEL)) {
#ifdef debug_signal
		g_print (">> 'TABLE_SELECTED_SIGNAL' from sqlwiddbtree->signals_cb (NULL)\n");
#endif
		gtk_signal_emit (GTK_OBJECT (wid),
				 sql_wid_db_tree_signals[TABLE_SELECTED_SIGNAL],
				 NULL);
#ifdef debug_signal
		g_print ("<< 'TABLE_SELECTED_SIGNAL' from sqlwiddbtree->signals_cb\n");
#endif
	}

	if (IS_DB_FIELD (wid->selection) && (wid->mode & SQL_WID_DB_TREE_TABLE_FIELDS_SEL)) {
#ifdef debug_signal
		g_print (">> 'FIELD_SELECTED_SIGNAL' from sqlwiddbtree->signals_cb (NULL.NULL)\n");
#endif
		gtk_signal_emit (GTK_OBJECT (wid),
				 sql_wid_db_tree_signals [FIELD_SELECTED_SIGNAL],
				 NULL, NULL);
#ifdef debug_signal
		g_print ("<< 'FIELD_SELECTED_SIGNAL' from sqlwiddbtree->signals_cb\n");
#endif
	}
		

	/* we have to find the DbSequence, DbTable or DbField selected */
	if (sel) {
		/* first look among the sequences */
		found = FALSE;
		if (wid->sequences) {
			tree = GTK_TREE (wid->sequences);
			list = gtk_container_children (GTK_CONTAINER (tree));
			while (list && !found) {
				if (sel->data == list->data) {
					found = TRUE;
					retval = gtk_object_get_data (GTK_OBJECT (list->data), "objptr");
					wid->selection = retval;
					if (wid->mode & SQL_WID_DB_TREE_SEQS_SEL) {
#ifdef debug_signal
						g_print (">> 'SEQ_SELECTED_SIGNAL' " "from sqlwiddbtree->signals_cb (%p)\n",
							 retval);
#endif
						gtk_signal_emit (GTK_OBJECT (wid),
								 sql_wid_db_tree_signals[SEQ_SELECTED_SIGNAL],
								 retval);
#ifdef debug_signal
						g_print ("<< 'SEQ_SELECTED_SIGNAL' " "from sqlwiddbtree->signals_cb\n");
#endif
					}
				}
				hold = list;
				list = g_list_remove_link (list, list);
				g_list_free_1 (hold);
			}
		}

		if (wid->tables) {
			/* then a look at the tables */
			found = FALSE;
			tree = GTK_TREE (wid->tables);
			list = gtk_container_children (GTK_CONTAINER (tree));
			while (list && !found) {
				if (sel->data == list->data) {
					found = TRUE;
					retval = gtk_object_get_data
						(GTK_OBJECT (list->data),
						 "objptr");
					wid->selection = retval;
					if (wid->mode & SQL_WID_DB_TREE_TABLES_SEL)
					{
#ifdef debug_signal
						g_print (">> 'TABLE_SELECTED_SIGNAL' " "from sqlwiddbtree->signals_cb (%p)\n",
							 retval);
#endif
						gtk_signal_emit (GTK_OBJECT (wid),
								 sql_wid_db_tree_signals[TABLE_SELECTED_SIGNAL],
								 retval);
#ifdef debug_signal
						g_print ("<< 'TABLE_SELECTED_SIGNAL' " "from sqlwiddbtree->signals_cb\n");
#endif
					}
				}
				hold = list;
				list = g_list_remove_link (list, list);
				g_list_free_1 (hold);
			}

			/* finally looking at the fields */
			found = FALSE;
			list = gtk_container_children (GTK_CONTAINER (tree));
			while (list && !found) {
				if (GTK_TREE_ITEM (list->data)->subtree) {
					list2 = gtk_container_children (GTK_CONTAINER 
									(GTK_TREE_ITEM (list->data)->subtree));
					while (list2 && !found) {
						if (sel->data == list2->data) {
							found = TRUE;
							retval = gtk_object_get_data (GTK_OBJECT (list2->data), "objptr");
							wid->selection = retval;
							table = gtk_object_get_data (GTK_OBJECT (list->data), "objptr");
							if (wid->mode & SQL_WID_DB_TREE_TABLE_FIELDS_SEL)
							{
#ifdef debug_signal
								g_print (">> 'FIELD_SELECTED_SIGNAL' " "from sqlwiddbtree->signals_cb (%p.%p)\n",
									 table, retval);
#endif
								gtk_signal_emit (GTK_OBJECT (wid),
									 sql_wid_db_tree_signals[FIELD_SELECTED_SIGNAL],
									 table, retval);
#ifdef debug_signal
								g_print ("<< 'FIELD_SELECTED_SIGNAL' " "from sqlwiddbtree->signals_cb\n");
#endif
							}
						}
						hold = list2;
						list2 = g_list_remove_link (list2, list2);
						g_list_free_1 (hold);
					}
				}
				hold = list;
				list = g_list_remove_link (list, list);
				g_list_free_1 (hold);
			}
		}
	}
}
