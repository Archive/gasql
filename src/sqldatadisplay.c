/* sqldatadisplay.c
 *
 * Copyright (C) 1999 - 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <string.h>
#include "sqldatadisplay.h"

/* these two functions allocate some memory for the returned string,
   and free the string given as argument */
static struct tm *sqlstr_to_tmstruct_dbspec (gchar * date);
static struct tm *sqlstr_to_tmstruct_locale (gchar * date);

/* the returned char is allocated and the one in argument freed */
static gchar *server_access_escape_chars (gchar * str);
/* the returned char is allocated and the one in argument freed */
static gchar *server_access_unescape_chars (gchar * str);

static gboolean
use_free_space (DataDisplay * dd)
{
	return FALSE;
}

/*****************************************************************************/
/*                                                                           */
/* Default functions                                                         */
/*                                                                           */
/*****************************************************************************/
/* signal callbacks */
static void
contents_changed_cb (GtkObject * obj, DataDisplay * dd)
{
	gtk_signal_emit_by_name (GTK_OBJECT (dd), "contents_modified");
}

gchar *
gdafield_to_str_default (GdaField * field)
{
	if (field) {
		if (((gda_field_get_gdatype (field) == GDA_TypeVarchar) ||
		     (gda_field_get_gdatype (field) == GDA_TypeChar) ||
		     (gda_field_get_gdatype (field) == GDA_TypeLongvarchar)) &&
		    !gda_field_get_string_value (field))
			return NULL;


		return server_resultset_stringify (field);
	}
	else
		return g_strdup ("");
}

DataDisplay *
gdafield_to_widget_default (GdaField * field)
{
	DataDisplay *dd;
	GtkWidget *wid;

	dd = DATA_DISPLAY (data_display_new ());
	wid = gtk_entry_new ();
	data_display_pack_default (dd, wid);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);
	if (field)
		gdafield_to_widget_up_default (dd, field);
	gtk_signal_connect (GTK_OBJECT (wid), "changed",
			    GTK_SIGNAL_FUNC (contents_changed_cb), dd);

	return dd;
}

void
gdafield_to_widget_up_default (DataDisplay * dd, GdaField * field)
{
	gchar *str;

	if (field) {
		str = gdafield_to_str_default (field);
		if (str) {
			gtk_entry_set_text (GTK_ENTRY (dd->childs->data),
					    str);
			g_free (str);
		}
		else
			gtk_entry_set_text (GTK_ENTRY (dd->childs->data), "");
	}
	else
		gtk_entry_set_text (GTK_ENTRY (dd->childs->data), "");
}

gchar *
gdafield_to_sql_default (GdaField * field)
{
	gchar *str, *str2, *retval;

	if (((gda_field_get_gdatype (field) == GDA_TypeVarchar) ||
	     (gda_field_get_gdatype (field) == GDA_TypeChar) ||
	     (gda_field_get_gdatype (field) == GDA_TypeLongvarchar)) &&
	    !gda_field_get_string_value (field))
		return NULL;

	str = server_resultset_stringify (field);
	if (str) {
		str2 = server_access_escape_chars (str);
		retval = g_strdup_printf ("'%s'", str2);
		g_free (str2);
	}
	else
		retval = NULL;

	return retval;
}

gchar *
widget_to_sql_default (DataDisplay * dd)
{
	gchar *str, *str2;

	str = gtk_entry_get_text (GTK_ENTRY (dd->childs->data));
	str2 = server_access_escape_chars (g_strdup (str));
	str = g_strdup_printf ("'%s'", str2);
	g_free (str2);
	return str;
}

DataDisplay *
sql_to_widget_default (gchar * sql)
{
	DataDisplay *dd;
	GtkWidget *wid;
	gchar *str;

	dd = DATA_DISPLAY (data_display_new ());
	wid = gtk_entry_new ();
	data_display_pack_default (dd, wid);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);
	if (sql && (*sql != 0)) {	/* sql is like 'my val' with the quotes */
		str = g_strdup (sql + 1);
		*(str + strlen (str) - 1) = 0;
		gtk_entry_set_text (GTK_ENTRY (wid), str);
		g_free (str);
	}
	gtk_signal_connect (GTK_OBJECT (wid), "changed",
			    GTK_SIGNAL_FUNC (contents_changed_cb), dd);

	return dd;
}


/*****************************************************************************/
/*                                                                           */
/* Functions for the numbers                                                 */
/*                                                                           */
/*****************************************************************************/
gchar *
gdafield_to_str_number (GdaField * field)
{
	return server_resultset_stringify (field);
}

DataDisplay *
gdafield_to_widget_number (GdaField * field)
{
	DataDisplay *dd;
	GtkWidget *wid;

	dd = DATA_DISPLAY (data_display_new ());
	wid = gtk_entry_new ();
	data_display_pack_default (dd, wid);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);
	if (field)
		gdafield_to_widget_up_number (dd, field);
	gtk_signal_connect (GTK_OBJECT (wid), "changed",
			    GTK_SIGNAL_FUNC (contents_changed_cb), dd);

	return dd;
}

void
gdafield_to_widget_up_number (DataDisplay * dd, GdaField * field)
{
	gchar *str;

	if (field) {
		str = gdafield_to_str_number (field);
		if (str) {
			gtk_entry_set_text (GTK_ENTRY (dd->childs->data),
					    str);
			g_free (str);
		}
		else
			gtk_entry_set_text (GTK_ENTRY (dd->childs->data), "");
	}
	else
		gtk_entry_set_text (GTK_ENTRY (dd->childs->data), "");
}

gchar *
gdafield_to_sql_number (GdaField * field)
{
	gchar *retval, *ptr;

	retval = server_resultset_stringify (field);
	/* FIXME: take better care of the locales here */
	ptr = retval;
	while (*ptr != '\0') {
		if (*ptr == ',')
			*ptr = '.';
		ptr++;
	}

	return retval;
}



static gchar *
str_to_sql_number (gchar * str)
{
	gchar *retval;
	if (*str == '\0')
		retval = NULL;
	else {
		/* FIXME: take better care of the locales here */
		gchar *ptr;

		retval = g_strdup (str);
		ptr = retval;
		while (*ptr != '\0') {
			if (*ptr == ',')
				*ptr = '.';
			ptr++;
		}
	}

	return retval;
}

gchar *
widget_to_sql_number (DataDisplay * dd)
{
	gchar *str;

	str = str_to_sql_number (gtk_entry_get_text
				 (GTK_ENTRY (dd->childs->data)));
	if (!str)
		str = g_strdup ("0");

	return str;
}

DataDisplay *
sql_to_widget_number (gchar * sql)
{
	DataDisplay *dd;
	GtkWidget *wid;

	dd = DATA_DISPLAY (data_display_new ());
	wid = gtk_entry_new ();
	data_display_pack_default (dd, wid);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);
	if (sql)
		gtk_entry_set_text (GTK_ENTRY (wid), sql);

	gtk_signal_connect (GTK_OBJECT (wid), "changed",
			    GTK_SIGNAL_FUNC (contents_changed_cb), dd);

	return dd;
}

/*****************************************************************************/
/*                                                                           */
/* Functions for the booleans                                                */
/*                                                                           */
/*****************************************************************************/
static void
bool_contents_changed_cb (GtkObject * obj, DataDisplay * dd)
{
	if (GTK_TOGGLE_BUTTON (obj)->active)
		gtk_label_set_text (GTK_LABEL (GTK_BIN (obj)->child),
				    _("Yes"));
	else
		gtk_label_set_text (GTK_LABEL (GTK_BIN (obj)->child),
				    _("No"));
	gtk_signal_emit_by_name (GTK_OBJECT (dd), "contents_modified");
}


gchar *
gdafield_to_str_bool (GdaField * field)
{
	return server_resultset_stringify (field);
}

DataDisplay *
gdafield_to_widget_bool (GdaField * field)
{
	DataDisplay *dd;
	GtkWidget *wid, *hb;

	dd = DATA_DISPLAY (data_display_new ());
	hb = gtk_hbox_new (FALSE, 0);
	data_display_pack_default (dd, hb);
	gtk_widget_show (hb);
	wid = gtk_toggle_button_new_with_label (_("No"));
	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);
	if (field) {
		if (gda_field_get_boolean_value (field)) {
			gtk_label_set_text (GTK_LABEL (GTK_BIN (wid)->child),
					    _("Yes"));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (wid),
						      TRUE);
		}
		else {
			gtk_label_set_text (GTK_LABEL (GTK_BIN (wid)->child),
					    _("No"));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (wid),
						      FALSE);
		}
	}
	gtk_signal_connect (GTK_OBJECT (wid), "toggled",
			    GTK_SIGNAL_FUNC (bool_contents_changed_cb), dd);

	return dd;
}

void
gdafield_to_widget_up_bool (DataDisplay * dd, GdaField * field)
{
	if (field) {
		if (gda_field_get_boolean_value (field))
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
						      (dd->childs->data),
						      TRUE);
		else
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
						      (dd->childs->data),
						      FALSE);
	}
	else
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON
					      (dd->childs->data), FALSE);
}

gchar *
gdafield_to_sql_bool (GdaField * field)
{
	gchar *retval = NULL;

	if (gda_field_is_null (field))
		retval = NULL;
	else {
		if (gda_field_get_boolean_value (field))
			retval = g_strdup ("'t'");
		else
			retval = g_strdup ("'f'");
	}

	return retval;
}

gchar *
widget_to_sql_bool (DataDisplay * dd)
{
	gchar *retval;

	if (GTK_TOGGLE_BUTTON (dd->childs->data)->active)
		retval = g_strdup ("'t'");
	else
		retval = g_strdup ("'f'");
	return retval;
}

DataDisplay *
sql_to_widget_bool (gchar * sql)
{
	DataDisplay *dd;
	GtkWidget *wid, *hb;

	dd = DATA_DISPLAY (data_display_new ());
	hb = gtk_hbox_new (FALSE, 0);
	data_display_pack_default (dd, hb);
	gtk_widget_show (hb);
	wid = gtk_toggle_button_new_with_label (_("No"));
	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);
	if (sql) {
		if ((*sql != 0) && (*(sql + 1) == 't')) {
			gtk_label_set_text (GTK_LABEL (GTK_BIN (wid)->child),
					    _("Yes"));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (wid),
						      TRUE);
		}
		else {
			gtk_label_set_text (GTK_LABEL (GTK_BIN (wid)->child),
					    _("No"));
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (wid),
						      FALSE);
		}
	}
	gtk_signal_connect (GTK_OBJECT (wid), "toggled",
			    GTK_SIGNAL_FUNC (bool_contents_changed_cb), dd);

	return dd;
}

/*****************************************************************************/
/*                                                                           */
/* Functions for the dates                                                   */
/*                                                                           */
/*****************************************************************************/
gchar *
gdafield_to_str_date (GdaField * field)
{
	return server_resultset_stringify (field);
}

DataDisplay *
gdafield_to_widget_date (GdaField * field)
{
	DataDisplay *dd;
	GtkWidget *wid, *hb;
	time_t now;
	struct tm *stm;
	gboolean alloced = FALSE;

	dd = DATA_DISPLAY (data_display_new ());
	hb = gtk_hbox_new (FALSE, 0);
	data_display_pack_default (dd, hb);
	gtk_widget_show (hb);

	if (field) {
		stm = g_new0 (struct tm, 1);
		alloced = TRUE;
		stm->tm_mday = gda_field_get_date_value (field)->day;
		stm->tm_mon = gda_field_get_date_value (field)->month - 1;
		stm->tm_year = gda_field_get_date_value (field)->year - 1900;
	}
	else {
		now = time (NULL);
		stm = localtime (&now);
	}
	now = mktime (stm);
	wid = gnome_date_edit_new (now, FALSE, FALSE);
	if (alloced)
		g_free (stm);

	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);

	gtk_signal_connect (GTK_OBJECT (wid), "date_changed",
			    GTK_SIGNAL_FUNC (contents_changed_cb), dd);

	return dd;
}

void
gdafield_to_widget_up_date (DataDisplay * dd, GdaField * field)
{
	time_t now;
	struct tm *stm;

	if (field) {
		stm = g_new0 (struct tm, 1);
		stm->tm_mday = gda_field_get_date_value (field)->day;
		stm->tm_mon = gda_field_get_date_value (field)->month - 1;
		stm->tm_year = gda_field_get_date_value (field)->year - 1900;
		now = mktime (stm);
		g_free (stm);
	}
	else
		now = time (NULL);

	gnome_date_edit_set_time (GNOME_DATE_EDIT (dd->childs->data), now);
}

gchar *
gdafield_to_sql_date (GdaField * field)
{
	gchar *str;
	str = g_strdup_printf ("'%02d-%02d-%04d'",
			       gda_field_get_date_value (field)->month,
			       gda_field_get_date_value (field)->day,
			       gda_field_get_date_value (field)->year);
	return str;
}

gchar *
widget_to_sql_date (DataDisplay * dd)
{
	time_t thetime;
	struct tm *stm;
	gchar *retval;

	thetime =
		gnome_date_edit_get_date (GNOME_DATE_EDIT (dd->childs->data));
	stm = localtime (&thetime);

	retval = g_strdup_printf ("'%02d-%02d-%04d'", stm->tm_mon + 1,
				  stm->tm_mday, stm->tm_year + 1900);
	g_print ("Retval: %s\n", retval);
	return retval;
}

DataDisplay *
sql_to_widget_date (gchar * sql)
{
	DataDisplay *dd;
	GtkWidget *wid, *hb;
	time_t now;
	struct tm *stm;
	gboolean alloced = FALSE;

	dd = DATA_DISPLAY (data_display_new ());
	hb = gtk_hbox_new (FALSE, 0);
	data_display_pack_default (dd, hb);
	gtk_widget_show (hb);

	if (sql) {
		gchar *csql, *ptr;
		stm = g_new0 (struct tm, 1);
		alloced = TRUE;
		csql = g_strdup (sql);
		ptr = strtok (csql, "-");
		stm->tm_mon = atoi (ptr) - 1;
		ptr = strtok (NULL, "-");
		stm->tm_mday = atoi (ptr);
		ptr = strtok (NULL, "-");
		stm->tm_year = atoi (ptr) - 1900;
		g_free (csql);
	}
	else {
		now = time (NULL);
		stm = localtime (&now);
	}
	now = mktime (stm);
	wid = gnome_date_edit_new (now, FALSE, FALSE);
	if (alloced)
		g_free (stm);

	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);

	gtk_signal_connect (GTK_OBJECT (wid), "date_changed",
			    GTK_SIGNAL_FUNC (contents_changed_cb), dd);

	return dd;
}

/*****************************************************************************/
/*                                                                           */
/* Functions for the times                                                   */
/*                                                                           */
/*****************************************************************************/
gchar *
gdafield_to_str_time (GdaField * field)
{
	return server_resultset_stringify (field);
}

DataDisplay *
gdafield_to_widget_time (GdaField * field)
{
	DataDisplay *dd;
	GtkWidget *wid, *hb;
	time_t now;
	struct tm *stm;
	gboolean alloced = FALSE;

	dd = DATA_DISPLAY (data_display_new ());
	hb = gtk_hbox_new (FALSE, 0);
	data_display_pack_default (dd, hb);
	gtk_widget_show (hb);

	if (field) {
		stm = g_new0 (struct tm, 1);
		alloced = TRUE;
		stm->tm_hour = gda_field_get_time_value (field).hour;
		stm->tm_min = gda_field_get_time_value (field).minute;
		stm->tm_sec = gda_field_get_time_value (field).second;
	}
	else {
		now = time (NULL);
		stm = localtime (&now);
	}
	now = mktime (stm);
	if (alloced)
		g_free (stm);
	wid = gnome_date_edit_new (now, TRUE, FALSE);
	/* hiding some buttons,... for the date part */
	gtk_widget_hide (GNOME_DATE_EDIT (wid)->date_entry);
	gtk_widget_hide (GNOME_DATE_EDIT (wid)->date_button);

	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);

	gtk_signal_connect (GTK_OBJECT (wid), "time_changed",
			    GTK_SIGNAL_FUNC (contents_changed_cb), dd);

	return dd;
}

void
gdafield_to_widget_up_time (DataDisplay * dd, GdaField * field)
{
	time_t now;
	struct tm *stm;

	if (field) {
		stm = g_new0 (struct tm, 1);
		stm->tm_hour = gda_field_get_time_value (field).hour;
		stm->tm_min = gda_field_get_time_value (field).minute;
		stm->tm_sec = gda_field_get_time_value (field).second;
		now = mktime (stm);
		g_free (stm);
	}
	else
		now = time (NULL);

	gnome_date_edit_set_time (GNOME_DATE_EDIT (dd->childs->data), now);
}

gchar *
gdafield_to_sql_time (GdaField * field)
{
	gchar *str;
	str = g_strdup_printf ("'%02d-%02d-%02d'",
			       (gda_field_get_time_value (field)).hour,
			       (gda_field_get_time_value (field)).minute,
			       (gda_field_get_time_value (field)).second);
	return str;
}

gchar *
widget_to_sql_time (DataDisplay * dd)
{
	time_t thetime;
	struct tm *stm;
	gchar *retval;

	thetime =
		gnome_date_edit_get_date (GNOME_DATE_EDIT (dd->childs->data));
	stm = localtime (&thetime);

	retval = g_strdup_printf ("'%02d-%02d-%02d'", stm->tm_hour,
				  stm->tm_min, stm->tm_sec);
	return retval;
}

DataDisplay *
sql_to_widget_time (gchar * sql)
{
	DataDisplay *dd;
	GtkWidget *wid, *hb;
	time_t now;
	struct tm *stm;
	gboolean alloced = FALSE;

	dd = DATA_DISPLAY (data_display_new ());
	hb = gtk_hbox_new (FALSE, 0);
	data_display_pack_default (dd, hb);
	gtk_widget_show (hb);

	if (sql) {
		gchar *csql, *ptr;
		stm = g_new0 (struct tm, 1);
		alloced = TRUE;
		csql = g_strdup (sql);
		ptr = strtok (csql, "-");
		stm->tm_hour = atoi (ptr);
		ptr = strtok (NULL, "-");
		stm->tm_min = atoi (ptr);
		ptr = strtok (NULL, "-");
		stm->tm_sec = atoi (ptr);
		g_free (csql);
	}
	else {
		now = time (NULL);
		stm = localtime (&now);
	}
	now = mktime (stm);
	if (alloced)
		g_free (stm);
	wid = gnome_date_edit_new (now, TRUE, FALSE);
	/* hiding some buttons,... for the date part */
	gtk_widget_hide (GNOME_DATE_EDIT (wid)->date_entry);
	gtk_widget_hide (GNOME_DATE_EDIT (wid)->date_button);

	gtk_box_pack_start (GTK_BOX (hb), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);

	gtk_signal_connect (GTK_OBJECT (wid), "time_changed",
			    GTK_SIGNAL_FUNC (contents_changed_cb), dd);

	return dd;
}

/***************************************************************************/
/*                                                                         */
/* Some usefull functions                                                  */
/*                                                                         */
/***************************************************************************/

/* dbspec: 
   Converting MM-DD-YYYY to struct tm */
static struct tm *
sqlstr_to_tmstruct_dbspec (gchar * date)
{
	int day, month, year;
	char *ptr;
	char mdate[11];
	static struct tm stm;

	if ((date == NULL) || (*date == '\0')) {
		return NULL;
	}
	strncpy (mdate, date, 10);
	mdate[10] = '\0';
	ptr = (char *) strtok (mdate, "-");
	month = atoi (ptr);
	if (!(ptr = (char *) strtok (NULL, "-")))
		return NULL;	/* Error */
	day = atoi (ptr);
	if (!(ptr = (char *) strtok (NULL, "-")))
		return NULL;	/* Error */
	year = atoi (ptr);

	stm.tm_mday = day;
	stm.tm_mon = month - 1;
	stm.tm_year = year - 1900;

	return &stm;
}

/* locale:
   it is needed to introduce the way dates are represented in the current 
   locale. 
   At the moment: MM*DD*YYYY is the only supported, * being either -, / or . */
static struct tm *
sqlstr_to_tmstruct_locale (gchar * date)
{
	int day, month, year;
	char *ptr;
	char mdate[11];
	static struct tm stm;

	if ((date == NULL) || (*date == '\0')) {
		return NULL;
	}
	strncpy (mdate, date, 10);
	mdate[10] = '\0';
	ptr = (char *) strtok (mdate, "-/.");
	month = atoi (ptr);
	if (!(ptr = (char *) strtok (NULL, "-/.")))
		return NULL;	/* Error */
	day = atoi (ptr);
	if (!(ptr = (char *) strtok (NULL, "-/.")))
		return NULL;	/* Error */
	year = atoi (ptr);

	stm.tm_mday = day;
	stm.tm_mon = month - 1;
	stm.tm_year = year - 1900;

	return &stm;
}

/* the returned char is allocated and the one in argument freed */
static gchar *
server_access_escape_chars (gchar * str)
{
	gchar *ptr = str, *ret, *retptr;
	gint size;

	/* determination of the new string size */
	size = 1;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str)
				size += 2;
			else {
				if (*(ptr - 1) == '\\')
					size += 1;
				else
					size += 2;
			}
		}
		else
			size += 1;
		ptr++;
	}

	ptr = str;
	ret = (gchar *) malloc (sizeof (gchar) * size);
	retptr = ret;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str) {
				*retptr = '\\';
				retptr++;
			}
			else if (*(ptr - 1) != '\\') {
				*retptr = '\\';
				retptr++;
			}
		}
		*retptr = *ptr;
		retptr++;
		ptr++;
	}
	*retptr = '\0';
	g_free (str);
	return ret;
}

/* the returned char is allocated and the one in argument freed */
static gchar *
server_access_unescape_chars (gchar * str)
{
	gchar *ptr = str, *ret, *retptr;
	gint size;

	/* determination of the new string size */
	size = 1;
	while (*ptr != '\0') {
		if ((*ptr == '\\') && (*(ptr + 1) == '\'')) {
			ptr++;
		}
		else {
			size += 1;
			ptr++;
		}
	}

	ptr = str;
	ret = (gchar *) malloc (sizeof (gchar) * size);
	retptr = ret;
	while (*ptr != '\0') {
		if ((*ptr == '\\') && (*(ptr + 1) == '\''))
			ptr++;
		else {
			*retptr = *ptr;
			retptr++;
			ptr++;
		}
	}
	*retptr = '\0';
	g_free (str);
	return ret;
}

GSList *
sql_data_display_get_initial_list ()
{
	GSList *list = NULL;
	SqlDataDisplayFns *fns;

	/* 
	 * WARNING: The order here is important, because in the 
	 * sqldata::get_display_fns_from_gda(), the returned value is the first one
	 * which matches the gda type of the given ServerDataType.
	 */

	/* FIXME: the other ones here (only builting ones, not plugins) */

	/* 1 - numerical values */
	fns = (SqlDataDisplayFns *) g_malloc (sizeof (SqlDataDisplayFns));
	fns->descr = g_strdup (_("Numerical data representation"));
	fns->plugin_name = NULL;
	fns->plugin_file = NULL;
	fns->lib_handle = NULL;
	fns->version = NULL;
	fns->get_unique_key = NULL;
	fns->nb_gda_type = 10;	/* means ALL GDA types */
	fns->valid_gda_types =
		(GDA_ValueType *) g_malloc (10 * sizeof (GDA_ValueType));
	fns->valid_gda_types[0] = GDA_TypeBigint;
	fns->valid_gda_types[1] = GDA_TypeDecimal;
	fns->valid_gda_types[2] = GDA_TypeDouble;
	fns->valid_gda_types[3] = GDA_TypeInteger;
	fns->valid_gda_types[4] = GDA_TypeNumeric;
	fns->valid_gda_types[5] = GDA_TypeSingle;
	fns->valid_gda_types[6] = GDA_TypeSmallint;
	fns->valid_gda_types[7] = GDA_TypeTinyint;
	fns->valid_gda_types[8] = GDA_TypeUBigint;
	fns->valid_gda_types[9] = GDA_TypeUSmallint;
	fns->widget_to_sql = widget_to_sql_number;
	fns->sql_to_widget = sql_to_widget_number;
	fns->gdafield_to_str = gdafield_to_str_number;
	fns->gdafield_to_widget = gdafield_to_widget_number;
	fns->gdafield_to_widget_update = gdafield_to_widget_up_number;
	fns->gdafield_to_sql = gdafield_to_sql_number;
	fns->use_free_space = use_free_space;
	list = g_slist_append (list, fns);

	/* 2 - Boolean values */
	fns = (SqlDataDisplayFns *) g_malloc (sizeof (SqlDataDisplayFns));
	fns->descr = g_strdup (_("Boolean data representation"));
	fns->plugin_name = NULL;
	fns->plugin_file = NULL;
	fns->lib_handle = NULL;
	fns->version = NULL;
	fns->get_unique_key = NULL;
	fns->nb_gda_type = 1;	/* means ALL GDA types */
	fns->valid_gda_types =
		(GDA_ValueType *) g_malloc (sizeof (GDA_ValueType));
	fns->valid_gda_types[0] = GDA_TypeBoolean;
	fns->widget_to_sql = widget_to_sql_bool;
	fns->sql_to_widget = sql_to_widget_bool;
	fns->gdafield_to_str = gdafield_to_str_bool;
	fns->gdafield_to_widget = gdafield_to_widget_bool;
	fns->gdafield_to_widget_update = gdafield_to_widget_up_bool;
	fns->gdafield_to_sql = gdafield_to_sql_bool;
	fns->use_free_space = use_free_space;
	list = g_slist_append (list, fns);

	/* 3 - Dates values */
	fns = (SqlDataDisplayFns *) g_malloc (sizeof (SqlDataDisplayFns));
	fns->descr = g_strdup (_("Dates representation"));
	fns->plugin_name = NULL;
	fns->plugin_file = NULL;
	fns->lib_handle = NULL;
	fns->version = NULL;
	fns->get_unique_key = NULL;
	fns->nb_gda_type = 1;	/* means ALL GDA types */
	fns->valid_gda_types =
		(GDA_ValueType *) g_malloc (sizeof (GDA_ValueType));
	fns->valid_gda_types[0] = GDA_TypeDbDate;
	fns->widget_to_sql = widget_to_sql_date;
	fns->sql_to_widget = sql_to_widget_date;
	fns->gdafield_to_str = gdafield_to_str_date;
	fns->gdafield_to_widget = gdafield_to_widget_date;
	fns->gdafield_to_widget_update = gdafield_to_widget_up_date;
	fns->gdafield_to_sql = gdafield_to_sql_date;
	fns->use_free_space = use_free_space;
	list = g_slist_append (list, fns);

	/* 4 - Times values */
	fns = (SqlDataDisplayFns *) g_malloc (sizeof (SqlDataDisplayFns));
	fns->descr = g_strdup (_("Dates representation"));
	fns->plugin_name = NULL;
	fns->plugin_file = NULL;
	fns->lib_handle = NULL;
	fns->version = NULL;
	fns->get_unique_key = NULL;
	fns->nb_gda_type = 1;	/* means ALL GDA types */
	fns->valid_gda_types =
		(GDA_ValueType *) g_malloc (sizeof (GDA_ValueType));
	fns->valid_gda_types[0] = GDA_TypeDbTime;
	fns->widget_to_sql = widget_to_sql_time;
	fns->sql_to_widget = sql_to_widget_time;
	fns->gdafield_to_str = gdafield_to_str_time;
	fns->gdafield_to_widget = gdafield_to_widget_time;
	fns->gdafield_to_widget_update = gdafield_to_widget_up_time;
	fns->gdafield_to_sql = gdafield_to_sql_time;
	fns->use_free_space = use_free_space;
	list = g_slist_append (list, fns);

	/* LAST - default values */
	fns = (SqlDataDisplayFns *) g_malloc (sizeof (SqlDataDisplayFns));
	fns->descr = g_strdup (_("Default data representation"));
	fns->plugin_name = NULL;
	fns->plugin_file = NULL;
	fns->lib_handle = NULL;
	fns->version = NULL;
	fns->get_unique_key = NULL;
	fns->nb_gda_type = 0;	/* means ALL GDA types */
	fns->valid_gda_types = NULL;	/* means ALL GDA types */
	fns->widget_to_sql = widget_to_sql_default;
	fns->sql_to_widget = sql_to_widget_default;
	fns->gdafield_to_str = gdafield_to_str_default;
	fns->gdafield_to_widget = gdafield_to_widget_default;
	fns->gdafield_to_widget_update = gdafield_to_widget_up_default;
	fns->gdafield_to_sql = gdafield_to_sql_default;
	fns->use_free_space = use_free_space;
	list = g_slist_append (list, fns);

	return list;
}

void
sql_data_display_free_display_fns (SqlDataDisplayFns * fns)
{
	if (fns->valid_gda_types)
		g_free (fns->valid_gda_types);
	g_free (fns->descr);
	g_free (fns);
}
