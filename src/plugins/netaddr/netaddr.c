#include <config.h>
#include "baseplugin.h"

#define PLUG_NAME "NetAddr"
#define PLUG_VERSION "0.6.0"

/* functions declaration */
static gchar *gdafield_to_str (GdaField * field);
static DataDisplay *gdafield_to_widget (GdaField * field);
static void gdafield_to_widget_up (DataDisplay * wid, GdaField * field);
static gchar *gdafield_to_sql (GdaField * field);
static gchar *widget_to_sql (DataDisplay * wid);
static DataDisplay *sql_to_widget (gchar * sql);
static gboolean use_free_space (DataDisplay * wid);

/*
 * main functions for the plugin
 */
static gchar *
get_unique_key ()
{
	gchar *retval;

	retval = g_strdup_printf ("%sV%s", PLUG_NAME, PLUG_VERSION);
	return retval;
}

SqlDataDisplayFns *
fetch_plugin_interface ()
{
	SqlDataDisplayFns *fns;

	fns = g_new (SqlDataDisplayFns, 1);
	fns->descr = _("IP addresses ex:172.16.49.19");
	fns->detailled_descr = _("Designed to be used with the\n"
				 "Postgres cidr data type");
	fns->plugin_name = PLUG_NAME;
	fns->plugin_file = NULL;
	fns->lib_handle = NULL;
	fns->version = PLUG_VERSION;
	fns->get_unique_key = get_unique_key;
	fns->nb_gda_type = 1;
	fns->valid_gda_types =
		(GDA_ValueType *) g_malloc (1 * sizeof (GDA_ValueType));
	fns->valid_gda_types[0] = GDA_TypeVarchar;
	fns->widget_to_sql = widget_to_sql;
	fns->sql_to_widget = sql_to_widget;
	fns->gdafield_to_str = gdafield_to_str;
	fns->gdafield_to_widget = gdafield_to_widget;
	fns->gdafield_to_widget_update = gdafield_to_widget_up;
	fns->gdafield_to_sql = gdafield_to_sql;
	fns->use_free_space = use_free_space;
	return fns;
}

void
release_struct (SqlDataDisplayFns * fns)
{
	g_free (fns->valid_gda_types);
}




/* 
 * functions implementation 
 */
/* signal callbacks */
static void
contents_changed_cb (GtkObject * obj, DataDisplay * dd)
{
	gtk_signal_emit_by_name (GTK_OBJECT (dd), "contents_modified");
}

static gboolean
use_free_space (DataDisplay * wid)
{
	return FALSE;
}

static gchar *
gdafield_to_str (GdaField * field)
{
	if (field) {
		if (((gda_field_get_gdatype (field) == GDA_TypeVarchar) ||
	             (gda_field_get_gdatype (field) == GDA_TypeChar) ||
                     (gda_field_get_gdatype (field) == GDA_TypeLongvarchar)) &&
	            !gda_field_get_string_value (field))
			return NULL;


		if (gda_field_is_null (field))
			return NULL;
		else
			return gda_stringify_value (NULL, 0, field);
	}
	else
		return NULL;
}

static DataDisplay *any_to_widget ();
static DataDisplay *
gdafield_to_widget (GdaField * field)
{
	DataDisplay *dd;

	dd = any_to_widget ();
	if (field)
		gdafield_to_widget_up (dd, field);
	return dd;
}

static void gdafield_to_widget_up_str (DataDisplay * dd, gchar * str);
static DataDisplay *
sql_to_widget (gchar * sql)
{
	DataDisplay *dd;
	gchar *str;

	dd = any_to_widget ();
	if (sql && (*sql != 0)) {	/* sql is like 'my val' with the quotes */
		str = g_strdup (sql + 1);
		*(str + strlen (str) - 1) = 0;
		gdafield_to_widget_up_str (dd, str);
		g_free (str);
	}

	return dd;
}


static DataDisplay *
any_to_widget ()
{
	DataDisplay *dd;
	GtkWidget *hb, *wid, *table;
	GSList *list;

	dd = DATA_DISPLAY (data_display_new ());
	hb = gtk_hbox_new (FALSE, 0);
	data_display_pack_default (dd, hb);
	gtk_widget_show (hb);
	table = gtk_table_new (2, 7, FALSE);
	gtk_box_pack_start (GTK_BOX (hb), table, FALSE, FALSE, 0);
	gtk_widget_show (table);

	/* filling the top table row with entries */
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 0, 1);
	dd->childs = g_slist_append (NULL, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 2, 3, 0, 1);
	dd->childs = g_slist_append (dd->childs, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 4, 5, 0, 1);
	dd->childs = g_slist_append (dd->childs, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 6, 7, 0, 1);
	dd->childs = g_slist_append (dd->childs, wid);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 1, 2, 0, 1, 0, 0, 0, 0);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 3, 4, 0, 1, 0, 0, 0, 0);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 5, 6, 0, 1, 0, 0, 0, 0);
	gtk_widget_show (wid);

	/* filling the bottom table row with entries */
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 1, 1, 2);
	dd->childs = g_slist_append (dd->childs, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 2, 3, 1, 2);
	dd->childs = g_slist_append (dd->childs, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 4, 5, 1, 2);
	dd->childs = g_slist_append (dd->childs, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new_with_max_length (3);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 6, 7, 1, 2);
	dd->childs = g_slist_append (dd->childs, wid);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 1, 2, 1, 2, 0, 0, 0, 0);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 3, 4, 1, 2, 0, 0, 0, 0);
	gtk_widget_show (wid);
	wid = gtk_label_new (".");
	gtk_table_attach (GTK_TABLE (table), wid, 5, 6, 1, 2, 0, 0, 0, 0);
	gtk_widget_show (wid);

	list = dd->childs;
	while (list) {
		/* FIXME: sth better with fonts handling for size... */
		gtk_widget_set_usize (GTK_WIDGET (list->data), 30, 16);
		gtk_signal_connect (GTK_OBJECT (list->data), "changed",
				    GTK_SIGNAL_FUNC (contents_changed_cb),
				    dd);
		list = g_slist_next (list);
	}

	return dd;
}

static void
gdafield_to_widget_up_str (DataDisplay * dd, gchar * str)
{
	gchar *ptr, *mask, *ip;
	GSList *list;
	gint i, j, nb, p;

	if (str) {
		list = dd->childs;

		ip = strtok (str, "/");
		mask = strtok (NULL, "/");
		i = 0;
		if (ip) {
			ptr = strtok (ip, ".");
			while (ptr) {
				gtk_entry_set_text (GTK_ENTRY (list->data),
						    ptr);
				list = g_slist_next (list);
				ptr = strtok (NULL, ".");
				i++;
			}
		}
		for (j = i; j < 4; j++) {
			gtk_entry_set_text (GTK_ENTRY (list->data), "0");
			list = g_slist_next (list);
		}

		if (mask) {
			nb = atoi (mask);
			mask = g_new0 (gchar, 33);
			for (i = 0; i < nb; i++)
				mask[i] = '1';
			for (; i < 32; i++)
				mask[i] = '0';
			for (i = 0; i < 4; i++) {
				nb = 0;
				p = 1;	/* 2^0 */
				for (j = i * 8 + 7; j >= i * 8; j--) {
					if (mask[j] == '1')
						nb += p;
					p *= 2;
				}
				ptr = g_strdup_printf ("%d", nb);
				gtk_entry_set_text (GTK_ENTRY (list->data),
						    ptr);
				g_free (ptr);
				list = g_slist_next (list);
			}
			g_free (mask);
		}
	}
}

static void
gdafield_to_widget_up (DataDisplay * dd, GdaField * field)
{
	gchar *str;

	if (field) {
		str = gdafield_to_str (field);
		gdafield_to_widget_up_str (dd, str);
		g_free (str);
	}
}

static gchar *
gdafield_to_sql (GdaField * field)
{
	gchar *str, *retval;

	if (((gda_field_get_gdatype (field) == GDA_TypeVarchar) ||
	     (gda_field_get_gdatype (field) == GDA_TypeChar) ||
             (gda_field_get_gdatype (field) == GDA_TypeLongvarchar)) &&
            !gda_field_get_string_value (field))
		return NULL;

	str = gda_stringify_value (NULL, 0, field);
	retval = g_strdup_printf ("'%s'", str);
	g_free (str);
	return retval;
}

static gchar *
widget_to_sql (DataDisplay * dd)
{
	gint i, j, nb, p;
	GSList *list;
	GString *string;
	gboolean first;
	gchar *str, *mask;

	list = dd->childs;
	string = g_string_new ("'");
	first = TRUE;
	for (i = 0; i < 4; i++) {
		str = gtk_entry_get_text (GTK_ENTRY (list->data));
		if (first)
			first = FALSE;
		else
			g_string_append (string, ".");
		if (!str || (str && (*str == '\0')))
			g_string_append (string, "0");
		else
			g_string_append (string, str);
		list = g_slist_next (list);
	}

	mask = g_new0 (gchar, 33);
	for (i = 0; i < 4; i++) {
		nb = atoi (gtk_entry_get_text (GTK_ENTRY (list->data)));
		p = 128;
		for (j = i * 8; j <= i * 8 + 7; j++) {
			if (nb / p == 1)
				mask[j] = '1';
			else
				mask[j] = '0';
			nb = nb % p;
			p = p / 2;
		}
		list = g_slist_next (list);
	}

	str = mask;
	nb = 0;
	while (*str == '1') {
		nb++;
		str++;
	}
	g_free (mask);
	g_string_sprintfa (string, "/%d'", nb);

	str = string->str;
	g_string_free (string, FALSE);

	return str;
}
