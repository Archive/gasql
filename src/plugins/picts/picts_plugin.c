#include <config.h>
#include "baseplugin.h"
#include <gdk-pixbuf/gdk-pixbuf.h>

#define PLUG_NAME "Picts"
#define PLUG_VERSION "0.6.0"
#define IMAGE_X_SIZE 150
#define IMAGE_Y_SIZE 200

/* functions declaration */
static gchar *server_access_escape_chars (gchar * str);
static gchar *gdafield_to_str (GdaField * field);
static DataDisplay *gdafield_to_widget (GdaField * field);
static void gdafield_to_widget_up (DataDisplay * wid, GdaField * field);
static gchar *gdafield_to_sql (GdaField * field);
static gchar *widget_to_sql (DataDisplay * wid);
static DataDisplay *sql_to_widget (gchar * sql);
static gboolean use_free_space (DataDisplay * wid);

/*
 * main functions for the plugin
 */
static gchar *
get_unique_key ()
{
	gchar *retval;

	retval = g_strdup_printf ("%sV%s", PLUG_NAME, PLUG_VERSION);
	return retval;
}

SqlDataDisplayFns *
fetch_plugin_interface ()
{
	SqlDataDisplayFns *fns;

	fns = g_new (SqlDataDisplayFns, 1);
	fns->descr = _("Pictures plugin");
	fns->detailled_descr = _("Displays images from file names!");
	fns->plugin_name = PLUG_NAME;
	fns->plugin_file = NULL;
	fns->lib_handle = NULL;
	fns->version = PLUG_VERSION;
	fns->get_unique_key = get_unique_key;
	fns->nb_gda_type = 1;
	fns->valid_gda_types =
		(GDA_ValueType *) g_malloc (1 * sizeof (GDA_ValueType));
	fns->valid_gda_types[0] = GDA_TypeVarchar;
	fns->widget_to_sql = widget_to_sql;
	fns->sql_to_widget = sql_to_widget;
	fns->gdafield_to_str = gdafield_to_str;
	fns->gdafield_to_widget = gdafield_to_widget;
	fns->gdafield_to_widget_update = gdafield_to_widget_up;
	fns->gdafield_to_sql = gdafield_to_sql;
	fns->use_free_space = use_free_space;
	return fns;
}

void
release_struct (SqlDataDisplayFns * fns)
{
	g_free (fns->valid_gda_types);
}




/* 
 * functions implementation 
 */
/* signal callbacks */
static void gdafield_to_widget_up_str (DataDisplay * dd, gchar * str);

static void
filesel_cancel_cb (GtkWidget * btn, DataDisplay * dd)
{
	GtkWidget *dlg;
	dlg = gtk_object_get_data (GTK_OBJECT (dd), "seldlg");
	if (dlg) {
		gtk_object_set_data (GTK_OBJECT (dd), "seldlg", NULL);
		gtk_widget_destroy (dlg);
	}
}

static void
filesel_ok_cb (GtkWidget * btn, DataDisplay * dd)
{
	GtkWidget *dlg;
	dlg = gtk_object_get_data (GTK_OBJECT (dd), "seldlg");

	if (dlg) {
		gchar *str;
		str = gtk_file_selection_get_filename (GTK_FILE_SELECTION
						       (dlg));
		gdafield_to_widget_up_str (dd, g_strdup (str));	/* allocate str! */
		gtk_signal_emit_by_name (GTK_OBJECT (dd),
					 "contents_modified");
	}
	filesel_cancel_cb (NULL, dd);
}

static void
dd_destroy_cb (GtkWidget * wid, DataDisplay * dd)
{
	filesel_cancel_cb (NULL, dd);
}

static void
change_btn_clicked_cb (GtkWidget * btn, DataDisplay * dd)
{
	GtkWidget *dlg;

	dlg = gtk_object_get_data (GTK_OBJECT (dd), "seldlg");
	if (!dlg) {
		dlg = gtk_file_selection_new (_("Select an image file"));
		if (dd->childs->next) {
			gchar *str;
			str = (gchar *) (dd->childs->next->data);
			gtk_file_selection_set_filename (GTK_FILE_SELECTION
							 (dlg), str);
		}
		gtk_signal_connect (GTK_OBJECT
				    (GTK_FILE_SELECTION (dlg)->ok_button),
				    "clicked",
				    GTK_SIGNAL_FUNC (filesel_ok_cb),
				    (gpointer) dd);
		gtk_signal_connect (GTK_OBJECT
				    (GTK_FILE_SELECTION (dlg)->cancel_button),
				    "clicked",
				    GTK_SIGNAL_FUNC (filesel_cancel_cb),
				    (gpointer) dd);
		gtk_object_set_data (GTK_OBJECT (dd), "seldlg", dlg);
		gtk_widget_show (dlg);
	}
	else {
		gdk_window_raise (dlg->window);
	}
}

static gboolean
use_free_space (DataDisplay * wid)
{
	return TRUE;
}

static gchar *
gdafield_to_str (GdaField * field)
{
	if (field) {
		if (((gda_field_get_gdatype (field) == GDA_TypeVarchar) ||
		     (gda_field_get_gdatype (field) == GDA_TypeChar) ||
	             (gda_field_get_gdatype (field) == GDA_TypeLongvarchar)) &&
	            !gda_field_get_string_value (field))
			return NULL;


		return gda_stringify_value (NULL, 0, field);
	}
	else
		return g_strdup ("");
}

static DataDisplay *
gdafield_to_widget (GdaField * field)
{
	DataDisplay *dd;
	GtkWidget *wid, *sw, *btn;

	dd = DATA_DISPLAY (data_display_new ());
	wid = gtk_hbox_new (FALSE, GNOME_PAD);
	data_display_pack_default (dd, wid);
	gtk_widget_show (wid);

	/* SW for the pictures */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (wid), sw, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_widget_show (sw);
	dd->childs = g_slist_append (NULL, sw);

	/* button to change it */
	btn = gtk_button_new_with_label ("...");
	gtk_box_pack_start (GTK_BOX (wid), btn, FALSE, TRUE, 0);
	gtk_widget_show (btn);

	if (field)
		gdafield_to_widget_up (dd, field);

	gtk_signal_connect (GTK_OBJECT (btn), "clicked",
			    GTK_SIGNAL_FUNC (change_btn_clicked_cb), dd);

	gtk_signal_connect (GTK_OBJECT (dd), "destroy",
			    GTK_SIGNAL_FUNC (dd_destroy_cb), dd);
	return dd;
}

static void
gdafield_to_widget_up (DataDisplay * dd, GdaField * field)
{
	gchar *str = NULL;

	if (field)
		str = gdafield_to_str (field);
	gdafield_to_widget_up_str (dd, str);
}

/* WARNING: str needs to be allocated because it will be freed when not used
   anymore */
static void
gdafield_to_widget_up_str (DataDisplay * dd, gchar * str)
{
	GdkPixbuf *gdkpix = NULL;
	GtkWidget *wid;

	if (dd->childs->next) {	/* there is already an str */
		g_free (dd->childs->next->data);
		dd->childs =
			g_slist_remove_link (dd->childs, dd->childs->next);
	}

	if (str)
		gdkpix = gdk_pixbuf_new_from_file (str);

	if (gdkpix) {
		GdkPixmap *pixmap;
		GdkBitmap *mask;
		gint width = IMAGE_X_SIZE, height = IMAGE_Y_SIZE;

		gdk_pixbuf_render_pixmap_and_mask (gdkpix, &pixmap, &mask,
						   10);
		wid = gtk_pixmap_new (pixmap, mask);
		if (gdk_pixbuf_get_height (gdkpix) < IMAGE_Y_SIZE)
			height = gdk_pixbuf_get_height (gdkpix);
		if (gdk_pixbuf_get_width (gdkpix) < IMAGE_X_SIZE)
			width = gdk_pixbuf_get_width (gdkpix);
		gtk_widget_set_usize (GTK_WIDGET (dd->childs->data),
				      width + GNOME_PAD, height + GNOME_PAD);
		gdk_pixbuf_unref (gdkpix);
	}
	else {
		if (str)
			wid = gnome_pixmap_new_from_file
				(gnome_pixmap_file ("gnome-who.png"));
		else
			wid = gnome_pixmap_new_from_file
				(gnome_pixmap_file ("BulletHole.xpm"));
	}

	if (wid) {
		GtkWidget *viewport, *sw;

		sw = GTK_WIDGET (dd->childs->data);
		if (GTK_BIN (sw)->child) {
			gtk_container_remove (GTK_CONTAINER (sw),
					      GTK_BIN (sw)->child);
		}
		gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW
						       (sw), wid);
		viewport = GTK_BIN (sw)->child;
		gtk_viewport_set_shadow_type (GTK_VIEWPORT (viewport),
					      GTK_SHADOW_NONE);
		gtk_widget_show (wid);
	}
	if (str)
		dd->childs = g_slist_append (dd->childs, str);
}

static gchar *
gdafield_to_sql (GdaField * field)
{
	gchar *str, *str2, *retval;

	if (((gda_field_get_gdatype (field) == GDA_TypeVarchar) ||
	     (gda_field_get_gdatype (field) == GDA_TypeChar) ||
             (gda_field_get_gdatype (field) == GDA_TypeLongvarchar)) &&
            !gda_field_get_string_value (field))
		return NULL;

	if (gda_field_is_null (field))
		retval = NULL;
	else {
		str = gda_stringify_value (NULL, 0, field);
		str2 = server_access_escape_chars (str);
		retval = g_strdup_printf ("'%s'", str2);
		g_free (str2);
	}
	return retval;
}

static gchar *
widget_to_sql (DataDisplay * dd)
{
	gchar *str = NULL, *str2;

	if (dd->childs->next)
		str = (gchar *) (dd->childs->next->data);
	else
		str = "";	/*FIXME: can we return NULL ? */
	str2 = server_access_escape_chars (g_strdup (str));
	str = g_strdup_printf ("'%s'", str2);
	g_free (str2);
	return str;
}

static DataDisplay *
sql_to_widget (gchar * sql)
{
	DataDisplay *dd;
	GtkWidget *wid, *sw, *btn;

	dd = DATA_DISPLAY (data_display_new ());
	wid = gtk_hbox_new (FALSE, GNOME_PAD);
	data_display_pack_default (dd, wid);
	gtk_widget_show (wid);

	/* SW for the pictures */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (wid), sw, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_widget_show (sw);
	dd->childs = g_slist_append (NULL, sw);

	/* button to change it */
	btn = gtk_button_new_with_label ("...");
	gtk_box_pack_start (GTK_BOX (wid), btn, FALSE, TRUE, 0);
	gtk_widget_show (btn);

	if (sql && (*sql != 0)) {
		gchar *str;
		str = g_strdup (sql + 1);
		*(str + strlen (str) - 1) = 0;
		gdafield_to_widget_up_str (dd, str);
		g_free (str);
	}

	gtk_signal_connect (GTK_OBJECT (btn), "clicked",
			    GTK_SIGNAL_FUNC (change_btn_clicked_cb), dd);

	gtk_signal_connect (GTK_OBJECT (dd), "destroy",
			    GTK_SIGNAL_FUNC (dd_destroy_cb), dd);
	return dd;
}

static gchar *
server_access_escape_chars (gchar * str)
{
	gchar *ptr = str, *ret, *retptr;
	gint size;

	/* determination of the new string size */
	size = 1;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str)
				size += 2;
			else {
				if (*(ptr - 1) == '\\')
					size += 1;
				else
					size += 2;
			}
		}
		else
			size += 1;
		ptr++;
	}

	ptr = str;
	ret = (gchar *) malloc (sizeof (gchar) * size);
	retptr = ret;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str) {
				*retptr = '\\';
				retptr++;
			}
			else if (*(ptr - 1) != '\\') {
				*retptr = '\\';
				retptr++;
			}
		}
		*retptr = *ptr;
		retptr++;
		ptr++;
	}
	*retptr = '\0';
	g_free (str);
	return ret;
}
