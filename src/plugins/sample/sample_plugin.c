#include <config.h>
#include "baseplugin.h"

#define PLUG_NAME "Test"
#define PLUG_VERSION "0.6.0"

/* functions declaration */
static gchar *server_access_escape_chars (gchar * str);
static gchar *gdafield_to_str (GdaField * field);
static DataDisplay *gdafield_to_widget (GdaField * field);
static void gdafield_to_widget_up (DataDisplay * wid, GdaField * field);
static gchar *gdafield_to_sql (GdaField * field);
static gchar *widget_to_sql (DataDisplay * wid);
static DataDisplay *sql_to_widget (gchar * sql);
static gboolean use_free_space (DataDisplay * wid);

/*
 * main functions for the plugin
 */
static gchar *
get_unique_key ()
{
	gchar *retval;

	retval = g_strdup_printf ("%sV%s", PLUG_NAME, PLUG_VERSION);
	return retval;
}

SqlDataDisplayFns *
fetch_plugin_interface ()
{
	SqlDataDisplayFns *fns;

	fns = g_new (SqlDataDisplayFns, 1);
	fns->descr = _("Sample plugin");
	fns->detailled_descr = _("Not to be used anywhere, really!");
	fns->plugin_name = PLUG_NAME;
	fns->plugin_file = NULL;
	fns->lib_handle = NULL;
	fns->version = PLUG_VERSION;
	fns->get_unique_key = get_unique_key;
	fns->nb_gda_type = 1;
	fns->valid_gda_types =
		(GDA_ValueType *) g_malloc (1 * sizeof (GDA_ValueType));
	fns->valid_gda_types[0] = GDA_TypeVarchar;
	fns->widget_to_sql = widget_to_sql;
	fns->sql_to_widget = sql_to_widget;
	fns->gdafield_to_str = gdafield_to_str;
	fns->gdafield_to_widget = gdafield_to_widget;
	fns->gdafield_to_widget_update = gdafield_to_widget_up;
	fns->gdafield_to_sql = gdafield_to_sql;
	fns->use_free_space = use_free_space;
	return fns;
}

void
release_struct (SqlDataDisplayFns * fns)
{
	g_free (fns->valid_gda_types);
}




/* 
 * functions implementation 
 */
/* signal callbacks */
static void
contents_changed_cb (GtkObject * obj, DataDisplay * dd)
{
	gtk_signal_emit_by_name (GTK_OBJECT (dd), "contents_modified");
}

static gboolean
use_free_space (DataDisplay * wid)
{
	return FALSE;
}

static gchar *
gdafield_to_str (GdaField * field)
{
	if (field) {
		if (((gda_field_get_gdatype (field) == GDA_TypeVarchar) ||
		     (gda_field_get_gdatype (field) == GDA_TypeChar) ||
	             (gda_field_get_gdatype (field) == GDA_TypeLongvarchar)) &&
	            !gda_field_get_string_value (field))
			return NULL;


		return gda_stringify_value (NULL, 0, field);
	}
	else
		return g_strdup ("");
}

static DataDisplay *
gdafield_to_widget (GdaField * field)
{
	DataDisplay *dd;
	GtkWidget *wid;

	dd = DATA_DISPLAY (data_display_new ());
	wid = gtk_label_new ("Sample plugin used!!!");
	data_display_pack_default (dd, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new ();
	data_display_pack_default (dd, wid);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);
	if (field)
		gdafield_to_widget_up (dd, field);
	gtk_signal_connect (GTK_OBJECT (wid), "changed",
			    GTK_SIGNAL_FUNC (contents_changed_cb), dd);

	return dd;
}

static void
gdafield_to_widget_up (DataDisplay * dd, GdaField * field)
{
	gchar *str;
	if (field) {
		str = gdafield_to_str (field);
		if (str) {
			gtk_entry_set_text (GTK_ENTRY (dd->childs->data),
					    str);
			g_free (str);
		}
		else
			gtk_entry_set_text (GTK_ENTRY (dd->childs->data), "");
	}
	else
		gtk_entry_set_text (GTK_ENTRY (dd->childs->data), "");
}

static gchar *
gdafield_to_sql (GdaField * field)
{
	gchar *str, *str2, *retval;

	if (((gda_field_get_gdatype (field) == GDA_TypeVarchar) ||
             (gda_field_get_gdatype (field) == GDA_TypeChar) ||
             (gda_field_get_gdatype (field) == GDA_TypeLongvarchar)) &&
            !gda_field_get_string_value (field))
		return NULL;

	if (gda_field_is_null (field))
		retval = NULL;
	else {
		str = gda_stringify_value (NULL, 0, field);
		str2 = server_access_escape_chars (str);
		retval = g_strdup_printf ("'%s'", str2);
		g_free (str2);
	}
	return retval;
}

static gchar *
widget_to_sql (DataDisplay * dd)
{
	gchar *str, *str2;

	str = gtk_entry_get_text (GTK_ENTRY (dd->childs->data));
	str2 = server_access_escape_chars (g_strdup (str));
	str = g_strdup_printf ("'%s'", str2);
	g_free (str2);
	return str;
}

static DataDisplay *
sql_to_widget (gchar * sql)
{
	DataDisplay *dd;
	GtkWidget *wid;
	gchar *str;

	dd = DATA_DISPLAY (data_display_new ());
	wid = gtk_label_new ("Sample plugin used!!!");
	data_display_pack_default (dd, wid);
	gtk_widget_show (wid);
	wid = gtk_entry_new ();
	data_display_pack_default (dd, wid);
	gtk_widget_show (wid);
	dd->childs = g_slist_append (NULL, wid);
	if (sql && (*sql != 0)) {	/* sql is like 'my val' with the quotes */
		str = g_strdup (sql + 1);
		*(str + strlen (str) - 1) = 0;
		gtk_entry_set_text (GTK_ENTRY (wid), str);
		g_free (str);
	}

	gtk_signal_connect (GTK_OBJECT (wid), "changed",
			    GTK_SIGNAL_FUNC (contents_changed_cb), dd);

	return dd;
}

static gchar *
server_access_escape_chars (gchar * str)
{
	gchar *ptr = str, *ret, *retptr;
	gint size;

	/* determination of the new string size */
	size = 1;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str)
				size += 2;
			else {
				if (*(ptr - 1) == '\\')
					size += 1;
				else
					size += 2;
			}
		}
		else
			size += 1;
		ptr++;
	}

	ptr = str;
	ret = (gchar *) malloc (sizeof (gchar) * size);
	retptr = ret;
	while (*ptr != '\0') {
		if (*ptr == '\'') {
			if (ptr == str) {
				*retptr = '\\';
				retptr++;
			}
			else if (*(ptr - 1) != '\\') {
				*retptr = '\\';
				retptr++;
			}
		}
		*retptr = *ptr;
		retptr++;
		ptr++;
	}
	*retptr = '\0';
	g_free (str);
	return ret;
}
