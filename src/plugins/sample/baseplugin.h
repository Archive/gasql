/* baseplugin.h
 *
 * Copyright (C) 2000 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __BASE_PLUGIN_H__
#define __BASE_PLUGIN_H__

/* This file is the interface which DB plugins must implement.
   It is not meant to be #included in any file of the src directory, 
   only in the fiels of this directory.*/

#include <gnome.h>
#include <datadisplay-common.h>

/* retreive the usefull functions of the plugin */
SqlDataDisplayFns *fetch_plugin_interface ();

/* will take care of freeing all the allocated data inside fns, except
   for fns->plugin_file and fns->lib_handle */
void release_struct (SqlDataDisplayFns * fns);

#endif
