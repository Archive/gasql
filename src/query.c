/* query.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 * Copyright (C) 2001 - 2002 Fernando Martins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "query.h"
#include "query-env.h"
#include "relship.h"

/* 
 * Main static functions 
 */
static void query_class_init (QueryClass * class);
static void query_init (Query * srv);
static void query_destroy (GtkObject * object);
static void m_changed (Query * q, gpointer data);

static GtkObject *query_new_id (gchar * name, Query *parent_query, ConfManager * conf, guint id);

static gchar *get_from_part_of_sql (Query * q);

static void query_list_joins (Query *q);
static void query_print_joins (Query *q);

/* get a pointer to the parents to be able to call their destructor */
static GtkObject *parent_class = NULL;

enum
{
	CHANGED,
	NAME_CHANGED,
	TYPE_CHANGED,
	FIELD_CREATED,
	FIELD_DROPPED,
	FIELD_MODIFIED,
	FIELD_NAME_MODIFIED,
	FIELD_ALIAS_MODIFIED,
	JOIN_CREATED,
	JOIN_DROPPED,
	JOIN_MODIFIED,
	WHERE_CREATED,
	WHERE_DROPPED,
	WHERE_MODIFIED,
	WHERE_MOVED,
	ENV_CREATED,
	ENV_DROPPED,
	QUERY_VIEW_ADDED,
	QUERY_VIEW_REMOVED,
	QUERY_CREATED,
	QUERY_DROPPED,
	LAST_SIGNAL
};

static gint query_signals[LAST_SIGNAL] =
	{ 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };


guint
query_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Query",
			sizeof (Query),
			sizeof (QueryClass),
			(GtkClassInitFunc) query_class_init,
			(GtkObjectInitFunc) query_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_object_get_type (), &f_info);
	}

	return f_type;
}

static void
query_class_init (QueryClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;

	query_signals[CHANGED] =
		gtk_signal_new ("changed", GTK_RUN_FIRST, object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, changed),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	query_signals[NAME_CHANGED] =
		gtk_signal_new ("name_changed", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, name_changed),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	query_signals[TYPE_CHANGED] =
		gtk_signal_new ("type_changed", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, type_changed),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	query_signals[FIELD_CREATED] =
		gtk_signal_new ("field_created",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, field_created),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[FIELD_DROPPED] =
		gtk_signal_new ("field_dropped", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, field_dropped),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[FIELD_MODIFIED] =
		gtk_signal_new ("field_modified", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, field_modified),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[FIELD_NAME_MODIFIED] =
		gtk_signal_new ("field_name_modified", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, field_name_modified),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[FIELD_ALIAS_MODIFIED] =
		gtk_signal_new ("field_alias_modified", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, field_alias_modified),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[JOIN_CREATED] =
		gtk_signal_new ("join_created", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, join_created),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[JOIN_DROPPED] =
		gtk_signal_new ("join_dropped", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, join_dropped),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[JOIN_MODIFIED] =
		gtk_signal_new ("join_modified", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, join_modified),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[WHERE_CREATED] =
		gtk_signal_new ("where_created", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass,
						   where_created),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[WHERE_DROPPED] =
		gtk_signal_new ("where_dropped", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, where_dropped),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[WHERE_MODIFIED] =
		gtk_signal_new ("where_modified", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, where_modified),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[WHERE_MOVED] =
		gtk_signal_new ("where_moved", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, where_moved),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	query_signals[ENV_CREATED] =
		gtk_signal_new ("env_created", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, env_created),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	query_signals[ENV_DROPPED] =
		gtk_signal_new ("env_dropped", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, env_dropped),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	query_signals[QUERY_VIEW_ADDED] =
		gtk_signal_new ("query_view_added", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, query_view_added),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	query_signals[QUERY_VIEW_REMOVED] =
		gtk_signal_new ("query_view_removed", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, query_view_removed),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	query_signals[QUERY_CREATED] =
		gtk_signal_new ("query_created", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, query_created),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	query_signals[QUERY_DROPPED] =
		gtk_signal_new ("query_dropped", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryClass, query_dropped),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	gtk_object_class_add_signals (object_class, query_signals,
				      LAST_SIGNAL);
	class->field_created =
		(void (*)(Query * q, QueryField * field)) m_changed;
	class->field_dropped =
		(void (*)(Query * q, QueryField * field)) m_changed;
	class->field_modified =
		(void (*)(Query * q, QueryField * field)) m_changed;
	class->join_created = 
		(void (*)(Query * q, QueryJoin * qj))	m_changed;
	class->join_dropped = 
		(void (*)(Query * q, QueryJoin * qj)) m_changed;
	class->join_modified = 
		(void (*)(Query * q, QueryJoin * qj)) m_changed;
	class->changed = NULL;
	class->name_changed = NULL;
	class->type_changed = (void (*)(Query * q)) m_changed;
	class->where_created =
		(void (*)(Query * q, QueryWhere * node)) m_changed;
	class->where_dropped =
		(void (*)(Query * q, QueryWhere * node)) m_changed;
	class->where_modified =
		(void (*)(Query * q, QueryWhere * node)) m_changed;
	class->where_moved =
		(void (*)(Query * q, QueryWhere * node)) m_changed;

	class->query_view_added = NULL;
	class->query_view_removed = NULL;

	class->query_created = NULL;
	class->query_dropped = NULL;

	class->env_created = NULL;
	class->env_dropped = NULL;

	object_class->destroy = query_destroy;
	parent_class = gtk_type_class (gtk_object_get_type ());
}

static void
query_init (Query * q)
{
	q->parent = NULL;
	q->sub_queries = NULL;

	q->name = NULL;
	q->descr = NULL;
	q->type = QUERY_TYPE_STD;

	q->views = NULL;
	q->fields_counter = 0;
	q->fields = NULL;
	q->joins = NULL;
	q->where_tree = g_node_new (q);

	q->group_by_list = NULL;
	q->order_by_list = NULL;

	q->text_sql = NULL;

	q->envs = NULL;
}


GtkObject *
query_new (gchar * name, Query *parent_query, ConfManager * conf)
{
	return query_new_id (name, parent_query, conf, conf_manager_get_id_serial (conf));
}

static GtkObject *
query_new_id (gchar * name, Query *parent_query, ConfManager * conf, guint id)
{
	GtkObject *obj;

	Query *q;
	obj = gtk_type_new (query_get_type ());
	q = QUERY (obj);

	q->conf = conf;
	q->id = id;
	q->name = g_strdup (name);

	if (parent_query) {
		g_return_val_if_fail (IS_QUERY (parent_query), obj);
		query_add_sub_query (parent_query, NULL, q);
		/* otherwise we have a non referenced query */
	}

	return obj;
}

static void
copy_recursive_where_nodes (Query * oldq, Query * newq,
			    GNode * oldnode, GNode * newnode);
static void query_activate_all_fields (Query *q);
static GtkObject *query_copy_real (Query * q);

GtkObject *
query_new_copy (Query * q)
{
	Query *newq;
	newq = QUERY (query_copy_real (q));
	g_assert (newq);
	query_activate_all_fields (newq);

	return GTK_OBJECT (newq);
}

/* Only make empty queries with the right structure of sub queries */
static Query *do_copy_bare_queries (Query * q, GHashTable *hash);

/* For the given copy of a query (and its original), populates it with QueryView objects */
static void   do_copy_query_views  (Query *q, Query *original, GHashTable *hash);

/* For the given copy of a query (and its original), populates it with QueryField objects */
static void   do_copy_query_fields (Query *q, Query *original, GHashTable *hash);

/* For the given copy of a query (and its original), populates it with QueryJoin objects */
static void   do_copy_query_joins (Query *q, Query *original, GHashTable *hash);

/* For the given copy of a query (and its original), make sure the RelShip object associated,
   if it exists, is copied as well */
static void   do_copy_query_relships (Query *q, Query *original, GHashTable *hash);
static GtkObject *
query_copy_real (Query * q)
{
	Query *newquery;
	GHashTable *htable;

	htable = g_hash_table_new (NULL, NULL);
	
	/* New empty queries tree */
	newquery = do_copy_bare_queries (q, htable);

	/* Copy of the QueryView objects */
	do_copy_query_views (newquery, q, htable);

	/* Copy of the QueryFields */
	do_copy_query_fields (newquery, q, htable);

	/* Copy of the QueryJoins */
	do_copy_query_joins (newquery, q, htable);

	/* Copy of the QueryWhere conditions FIXME: see if it needs to be changed */
	copy_recursive_where_nodes (q, newquery, q->where_tree,
				    newquery->where_tree);
	/* Other Query attributes */
	if (q->text_sql)
		newquery->text_sql = g_strdup (q->text_sql);

	/* Then the RelShip objects which hold the graphical parameters */
	do_copy_query_relships (newquery, q, htable);
	
	g_hash_table_destroy (htable);

	return GTK_OBJECT (newquery);
}

static Query *
do_copy_bare_queries (Query * q, GHashTable *hash)
{
	gchar *str;
	GSList *list;
	Query *newquery;

	str = g_strdup (q->name);
	newquery = QUERY (query_new (str, q->parent, q->conf));
	g_free (str);
	g_hash_table_insert (hash, q, newquery);
	
	/* sub queries */
	list = q->sub_queries;
	while (list) {
		Query *childq;

		childq = do_copy_bare_queries (QUERY (list->data), hash);
		query_add_sub_query (newquery, NULL, childq);
		list = g_slist_next (list);
	}

	return newquery;
}

static void
do_copy_query_views (Query *q, Query *original, GHashTable *hash)
{
	GSList *list;
	g_return_if_fail (q && IS_QUERY (q));
	g_return_if_fail (original && IS_QUERY (original));

	list = original->views;
	while (list) {
		QueryView *qv;
		gpointer data;

		qv = QUERY_VIEW (query_view_new_copy (QUERY_VIEW (list->data)));
		g_hash_table_insert (hash, list->data, qv);
		/* changing the query to the new one */
		query_view_set_query (qv, q);

		/* if the obj attribute has been changed, do it as well */
		if ((data = g_hash_table_lookup (hash, qv->obj))) 
			query_view_set_view_obj (qv, GTK_OBJECT (data));

		query_add_view (q, qv);
		list = g_slist_next (list);	
	}

	/* sub queries */
	list = original->sub_queries;
	while (list) {
		Query *childq;

		childq = QUERY (g_hash_table_lookup (hash, list->data));
		do_copy_query_views (childq, QUERY (list->data), hash);
		list = g_slist_next (list);
	}
}

static void
do_copy_query_fields (Query *q, Query *original, GHashTable *hash)
{
	GSList *list;
	g_return_if_fail (q && IS_QUERY (q));
	g_return_if_fail (original && IS_QUERY (original));

	list = original->fields;
	while (list) {
		QueryField *qf, *nqf;
		GSList *views;

		qf = QUERY_FIELD (list->data);
		nqf = QUERY_FIELD (query_field_new_copy (qf));
		g_hash_table_insert (hash, list->data, nqf);

		/* changing the query to the new one */
		query_field_replace_ref_ptr (nqf, GTK_OBJECT (original), GTK_OBJECT (q));

		views = original->views;
		while (views) {
			QueryView *qv;
			
			qv = QUERY_VIEW (g_hash_table_lookup (hash, views->data));
			if (!qv)
				g_warning ("Can't find corresponding QueryView for original QueryView %p\n", views->data);
			else
				query_field_replace_ref_ptr (nqf, GTK_OBJECT (views->data), GTK_OBJECT (qv));

			views = g_slist_next (views);
		}

		query_add_field (q, nqf);
		list = g_slist_next (list);
	}

	/* sub queries */
	list = original->sub_queries;
	while (list) {
		Query *childq;

		childq = QUERY (g_hash_table_lookup (hash, list->data));
		do_copy_query_fields (childq, QUERY (list->data), hash);
		list = g_slist_next (list);
	}
}

static void
do_copy_query_joins (Query *q, Query *original, GHashTable *hash)
{
	/* TODO */
}

static void
do_copy_query_relships (Query *q, Query *original, GHashTable *hash)
{
	GSList *list;

	relship_copy (q, original, hash);
	list = original->sub_queries;
	while (list) {
		Query *childq;

		childq = QUERY (g_hash_table_lookup (hash, list->data));
		do_copy_query_relships (childq, QUERY (list->data), hash);
		list = g_slist_next (list);
	}
}

static void 
query_activate_all_fields (Query *q)
{
	GSList *list;
	
	/* activate all the fields */
	list = q->fields;
	while (list) {
		query_field_activate (QUERY_FIELD (list->data));
		if (! QUERY_FIELD (list->data)->activated)
			g_warning ("QueryField %s.%s can't be activated (%s line %d)\n", 
				   q->name, QUERY_FIELD (list->data)->name, __FILE__, __LINE__);

		list = g_slist_next (list);
	}
	
	/* for the sub queries as well */
	list = q->sub_queries;
	while (list) {
		query_activate_all_fields (QUERY (list->data));
		list = g_slist_next (list);
	}
}

gboolean
query_fields_activated   (Query *q)
{
	gboolean retval = TRUE;
	GSList *list;

	list = q->fields;
	while (list && retval) {
		retval = QUERY_FIELD (list->data)->activated;
		if (! retval)
			g_warning ("QueryField %s.%s is not activated (%s line %d)\n", 
				   q->name, QUERY_FIELD (list->data)->name, __FILE__, __LINE__);

		list = g_slist_next (list);
	}
	
	if (retval) {
		/* for the sub queries as well */
		list = q->sub_queries;
		while (list && retval) {
			retval = query_fields_activated (QUERY (list->data));

			list = g_slist_next (list);
		}
	}

	return retval;
}

/* for this recursive function, we assume it is called with 
   existing GNodes (and their data correctely set), 
   and it creates and update the GNodes and data of the new
   query for all the children of the GNodes 
*/
static void
copy_recursive_where_nodes (Query * oldq, Query * newq,
			    GNode * oldnode, GNode * newnode)
{
	GNode *onde, *nnde;
	QueryWhere *owh, *nwh;

	return;
	if (!oldnode || !newnode)
		return;

	onde = oldnode->children;
	while (onde) {
		/* data part */
		/* FIXME VIV owh = QUERY_WHERE (onde->data); */
/* 		nwh = QUERY_WHERE (query_where_new (newq)); */
/* 		nwh->where_type = owh->where_type; */

		/* FIXME: the object refs are wrong because the ref'd objects are part 
		   of the old query where they should be part of the new one */
		/* nwh->left_op = owh->left_op; */
		/* nwh->right_op = owh->right_op; */
		nwh->op_type = owh->op_type;

		/* GNodes part */
		nnde = g_node_append_data (newnode, nwh);
		/* FIXME VIV copy_recursive_where_nodes (oldq, newq, onde, nnde); */	/* children */

		onde = onde->next;
	}
}

static void
query_destroy (GtkObject * object)
{
	Query *q;
	GSList *list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY (object));

	q = QUERY (object);
	g_print ("destroying query %d ...\n", q->id);

	/* sub queries */
	list = q->sub_queries;
	while (list) {
		GtkObject *obj;

		obj = GTK_OBJECT (list->data);
		gtk_object_unref (GTK_OBJECT (obj));
		list = g_slist_next (list);
	}

	if (q->sub_queries) {
		g_slist_free (q->sub_queries);
		q->sub_queries = NULL;
	}

	/* Joins */
	while (q->joins) {
		GSList *sub_list;
		sub_list = (GSList *) q->joins->data;
		query_del_join (q, QUERY_JOIN (sub_list->data));
	}

	/* Query Fields */
	while (q->fields) {
		GtkObject *obj = GTK_OBJECT (q->fields->data);

		query_del_field (q, QUERY_FIELD (obj));
	}

	/* Query Envs */
	while (q->envs) {
		query_del_env (q, GTK_OBJECT (q->envs->data));
	}

	/* Query Views */
	while (q->views) {
		query_del_view (q, QUERY_VIEW (q->views->data));
	}

	/* Query Where area */
	/* FIXME: part is already done, but need to finish the job */

	/* minor memory allocations */
	if (q->name) {
		g_free (q->name);
		q->name = NULL;
	}
	if (q->descr) {
		g_free (q->descr);
		q->descr = NULL;
	}

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
	g_print ("query %d now destroyed\n", q->id);
}

static void
m_changed (Query * q, gpointer data)
{
	q->conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'CHANGED' from query->m_changed\n");
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[CHANGED]);
#ifdef debug_signal
	g_print ("<< 'CHANGED' from query->m_changed\n");
#endif
}



/*
 * Helper functions
 */

void
query_set_name (Query * q, gchar * name, gchar * descr)
{
	gboolean changed = FALSE;

	g_return_if_fail (q);
	g_return_if_fail (IS_QUERY (q));

	if (name) {
		if (q->name) {
			changed = strcmp (q->name, name) ? TRUE : FALSE;
			g_free (q->name);
		}
		else
			changed = TRUE;
		q->name = g_strdup (name);
	}

	if (descr) {
		if (q->descr) {
			changed = strcmp (q->descr, descr) ? TRUE : changed;
			g_free (q->descr);
		}
		else
			changed = TRUE;
		q->descr = g_strdup (descr);
	}

	if (changed) {
#ifdef debug_signal
		g_print (">> 'NAME_CHANGED' from query_set_name (%s/%s)\n", q->name, q->descr);
#endif
		gtk_signal_emit (GTK_OBJECT (q), query_signals[NAME_CHANGED]);
#ifdef debug_signal
		g_print ("<< 'NAME_CHANGED' from query_set_name (%s/%s)\n", q->name, q->descr);
#endif
	}
}

void 
query_set_query_type (Query * q, QueryType type)
{
	g_return_if_fail (q);
	g_return_if_fail (IS_QUERY (q));
	if (q->type != type) {
		q->type = type;
#ifdef debug_signal
		g_print (">> 'TYPE_CHANGED' from query_set_query_type\n");
#endif
		gtk_signal_emit (GTK_OBJECT (q), query_signals[TYPE_CHANGED]);
#ifdef debug_signal
		g_print ("<< 'TYPE_CHANGED' from query_set_query_type\n");
#endif
	}
}

void
query_set_text_sql (Query * q, gchar * sql)
{
	g_return_if_fail (q);
	g_return_if_fail (IS_QUERY (q));

	if (q->text_sql) {
		g_free (q->text_sql);
		q->text_sql = NULL;
	}

	if (sql) {
		q->text_sql = g_strdup (sql);
		query_set_query_type (q, QUERY_TYPE_SQL);
	}

#ifdef debug_signal
	g_print (">> 'CHANGED' from query_set_text_sql\n");
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[CHANGED]);
#ifdef debug_signal
	g_print ("<< 'CHANGED' from query_set_text_sql\n");
#endif
}


/*
 * saving as XML
 */

static void build_where_xml_recursive (Query * q, GNode * node,
				       xmlNodePtr tree);
void
query_build_xml_tree (Query * q, xmlNodePtr maintree, ConfManager * conf)
{
	xmlNodePtr toptree, tree;
	GSList *list;
	gint order;
	gchar *str;

	toptree = xmlNewChild (maintree, NULL, "Query", NULL);
	str = query_get_xml_id (q);
	xmlSetProp (toptree, "id", str);
	g_free (str);
	xmlSetProp (toptree, "name", q->name);
	if (q->descr)
		xmlSetProp (toptree, "descr", q->descr);
	switch (q->type) {
	case QUERY_TYPE_STD:
		str = "std";
		break;
	case QUERY_TYPE_UNION:
		str = "union";
		break;
	case QUERY_TYPE_INTERSECT:
		str = "intersect";
		break;
	case QUERY_TYPE_SQL:
		str = "SQL";
		break;
	default:
		str = "unknown";
	}
	xmlSetProp (toptree, "type", str);

	/* Query views */
	list = q->views;
	while (list) {
		QueryView *qv = QUERY_VIEW (list->data);

		tree = query_view_save_to_xml (qv);
		xmlAddChild (toptree, tree);
		list = g_slist_next (list);
	}

	/* Query fields */
	list = q->fields;
	order = 0;
	while (list) {
		QueryField *qf;

		qf = QUERY_FIELD (list->data);
		tree = query_field_save_to_xml (qf);
		xmlAddChild (toptree, tree);
		list = g_slist_next (list);
	}

	/* Query Joins Lists */
	list = q->joins;
	while (list) {
		GSList *jlist;
		jlist = (GSList *) (list->data);
		g_print("save QueryJoin list\n");
		tree = query_join_list_save_to_xml (jlist);
		xmlAddChild (toptree, tree);
		list = g_slist_next (list);
	}

	/* tree of where clauses */
	g_assert (q->where_tree); /* at least we have a root tree node */
	if (q->where_tree->children) {
		tree = xmlNewChild (toptree, NULL, "QueryWhere", NULL);
		build_where_xml_recursive (q, q->where_tree, tree);
	}


	/* The text of the query if specified */
	if (q->text_sql)
		tree = xmlNewChild (toptree, NULL, "QueryText", q->text_sql);

	/* the Query environments */
	list = q->envs;
	while (list) {
		query_env_build_xml_tree (QUERY_ENV (list->data), toptree);
		list = g_slist_next (list);
	}

	/* sub queries */
	list = q->sub_queries;
	while (list) {
		query_build_xml_tree (QUERY (list->data), toptree, conf);
		
		list = g_slist_next (list);
	}
}

gchar * 
query_get_xml_id (Query *q)
{
	gchar *str;

	g_return_val_if_fail (q, NULL);
	g_return_val_if_fail (IS_QUERY (q), NULL);

	str = g_strdup_printf ("QU%d", q->id);
	return str;
}


/* if start_query is NULL then start from the top level query */
Query *
query_find_from_xml_name (ConfManager * conf, Query * start_query, gchar *xmlname)
{
	Query *q, *retval = NULL;
	gchar *str;

	/* test if current query */
	if (start_query)
		q = start_query;
	else
		q = QUERY (conf->top_query);

	g_assert (q);
	str = query_get_xml_id (q);
	if (!strcmp (str, xmlname))
		retval = q;
	g_free (str);

	/* if not found, see children queries */
	if (!retval) { 
		GSList *list;

		list = q->sub_queries;
		while (list && !retval) {
			retval = query_find_from_xml_name (conf, QUERY (list->data), xmlname);
			list = g_slist_next (list);
		}
	}

	return retval;
}


static void
build_where_xml_recursive (Query * q, GNode * node, xmlNodePtr tree)
{
/* FIXME VIV */	
/* GNode *gnode; */
/* 	xmlNodePtr ntree; */
/* 	QueryWhere *qwh; */

/* 	gnode = node->children; */

/* 	while (gnode) { */
/* 		qwh = QUERY_WHERE (gnode->data); */
/* 		ntree = query_where_save_to_xml (qwh); */
/* 		ntree->parent = tree; */

/* 		build_where_xml_recursive (q, gnode, ntree); */
/* 		gnode = gnode->next; */
/* 	} */
}


/*
 * loading from XML
 */

static Query *real_query_build_from_xml_tree (ConfManager * conf, xmlNodePtr node, Query *parent);
static void activate_pending_query_views (Query *q);

Query *
query_build_from_xml_tree (ConfManager * conf, xmlNodePtr node, Query *parent)
{
	Query *q;

	q = real_query_build_from_xml_tree (conf, node, parent);

	/* Some Query views can't be useable right out of the box because they reference 
	   other queries which do not exist when the query to which they belong is parsed,
	   so we try to render them useable now that all the queries have been loaded */
	activate_pending_query_views (q);	
	
	return q;
}

static void 
activate_pending_query_views (Query *q)
{
	GSList *list;
	QueryView *qv;
	GtkObject *obj;

	list = gtk_object_get_data (GTK_OBJECT (q), "pending_views");
	while (list) {
		qv = QUERY_VIEW (list->data);
		if (!qv->obj) {
			gpointer ptr;

			g_assert (qv->ref);
			ptr = query_find_from_xml_name (q->conf, NULL, qv->ref);
			obj = NULL;
			if (ptr)
				obj = GTK_OBJECT (ptr);
			else {
				ptr = database_find_table_from_xml_name (q->conf->db, qv->ref);
				if (ptr)
					obj = GTK_OBJECT (ptr);
			}

			g_free (qv->ref);
			qv->ref = NULL;

			g_assert (obj);
			query_view_set_view_obj (qv, obj);
		}
		query_add_view (q, qv);
		list = g_slist_next (list);
	}

	list = gtk_object_get_data (GTK_OBJECT (q), "pending_views");
	g_slist_free (list);
	gtk_object_set_data (GTK_OBJECT (q), "pending_views", NULL);

	list = q->sub_queries;
	while (list) {
		activate_pending_query_views (QUERY (list->data));
		list = g_slist_next (list);
	}

}

static void build_recursive_where_tree_from_xml (Query * q, GNode * node,
						 xmlNodePtr tree);
static Query *
real_query_build_from_xml_tree (ConfManager * conf, xmlNodePtr node, Query *parent)
{
	xmlNodePtr tree;
	gchar *str;
	guint id;
	gboolean ok = TRUE;
	Query *q;
	GSList *list;

	str = xmlGetProp (node, "id");
	id = atoi (str + 2);
	
	g_free (str);

	/* new query: when parent is NULL, then we know it is the top level query to be used
	   and it can be found in the ConfManager object, so we don't build one in that case */
	if (parent) {
		str = xmlGetProp (node, "name");
		
		q = QUERY (query_new_id (str, parent, conf, id));
		g_free (str);
	}
	else {
		g_assert (conf->top_query);
		q = QUERY (conf->top_query);
		q->id = id;
	}

	str = xmlGetProp (node, "descr");
	if (str) {
		query_set_name (q, NULL, str);
		g_free (str);
	}

	str = xmlGetProp (node, "type");
	if (str) {
		switch (*str) {
		case 'u':
			q->type = QUERY_TYPE_UNION;
			break;
		case 'i':
			q->type = QUERY_TYPE_INTERSECT;
			break;
		case 'S':
			q->type = QUERY_TYPE_SQL;
			break;
		default:
			q->type = QUERY_TYPE_STD;
		}
		g_free (str);
	}

	/* First pass: load all the QueryViews and QueryFields */
	tree = node->xmlChildrenNode;
	while (tree) {
		if (!strcmp (tree->name, "QueryView")) {
			QueryView *qv;

			qv = QUERY_VIEW (query_view_new (q));
			query_view_load_from_xml (qv, tree);

			if (qv->obj)
				query_add_view (q, qv);
			else {
				/* make a list of QueryViews to be "activated" later */
				GSList *pending;

				pending = gtk_object_get_data (GTK_OBJECT (q), "pending_views");
				pending = g_slist_append (pending, qv);
				gtk_object_set_data (GTK_OBJECT (q), "pending_views", pending);
			}
		}

		if (!strcmp (tree->name, "QueryField")) {
			QueryField *qf;

			str = xmlGetProp (tree, "type");
			qf = QUERY_FIELD (query_field_new_objref (q, NULL, str));
			g_free (str);

			if (qf) {
				query_field_load_from_xml (qf, tree);
				query_add_field (q, qf);
			}
			else {
				/* Fixme: signal the error */
			}
		}
		tree = tree->next;
	}

	/* Activate all the QueryFields which are not activated */
	list = q->fields;
	while (list) {
		query_field_activate (QUERY_FIELD (list->data));
		list = g_slist_next (list);
	}


	
	/* Second pass: for the sub queries, joins, environments and where conditions */
	tree = node->xmlChildrenNode;

	while (tree) {
		/* Sub queries */
		if (!strcmp (tree->name, "Query")) {
			query_build_from_xml_tree (conf, tree, q);
		}

		/* Joins */
		if (!strcmp (tree->name, "QueryJoinList")) {
			list = query_join_list_new_from_xml (conf, tree);
			q->joins = g_slist_append (q->joins, list);
			g_print("\n%s\n", get_from_part_of_sql(q)); /* DEBUG FER */
		}

		if (!strcmp (tree->name, "QueryWhere"))
			build_recursive_where_tree_from_xml (q, q->where_tree,
							     tree->xmlChildrenNode);
		/* Envs */
		if (!strcmp (tree->name, "QueryEnv")) 
			query_env_build_from_xml_tree (conf, tree);
		
		tree = tree->next;
	}

	if (!ok) {
		gtk_object_unref (GTK_OBJECT (q));
		q = NULL;
	} 
	else 
		query_activate_all_fields (q);

	return q;
}

static void
build_recursive_where_tree_from_xml (Query * q, GNode * node,
				     xmlNodePtr tree)
{
	/* FIXME VIV */
	/* GNode *nnode; */
/* 	QueryWhere *qwh; */
/* 	xmlNodePtr treelist; */

/* 	treelist = tree; */
/* 	while (treelist) { */
/* 		qwh = QUERY_WHERE (query_where_new (q)); */
/* 		query_where_load_from_xml (qwh, treelist); */
	
/* 		nnode = g_node_new (qwh); */
/* 		g_node_insert_before (node, NULL, nnode); */
/* 		if (treelist->xmlChildrenNode) */
/* 			build_recursive_where_tree_from_xml (q, nnode, */
/* 							     treelist->xmlChildrenNode); */
/* 		treelist = treelist->next; */
/* 	} */
}


/*
 * QueryViews management
 */

static void query_add_view_check (Query *q, QueryView *qv);

void 
query_add_view_with_obj (Query *q, GtkObject *obj)
{
	QueryView *qv;

	g_return_if_fail (IS_QUERY (q));
	g_return_if_fail (obj);
	g_return_if_fail (IS_QUERY (obj) || IS_DB_TABLE (obj));
	
	qv = QUERY_VIEW (query_view_new (q));
	query_view_set_view_obj (qv, obj);

	query_add_view_check (q, qv);
	query_add_view (q, qv);
}


/* query_add_view_check
 *
 * checks the view list to see if it already exists one (or more) instances of
 * the view. If so, defines alias information: number of occurrence and alias name
 * which is table_occ
 *
 */

static void
query_add_view_check (Query *q, QueryView *qv)
{
	GSList *list;
	gint found;

	list = q->views;
	found = -1;
	while (list) {
		QueryView *list_qv;
		list_qv = QUERY_VIEW (list->data);
		if ((list_qv->obj == qv->obj) && (found < list_qv->occ))
			found = list_qv->occ;
		list = g_slist_next (list);
	}
	if (found > -1) {
		qv->occ = found + 1;
		qv->alias = g_strdup_printf ("%s_%d", query_view_get_real_name (qv), qv->occ);
	}
}

static void query_view_destroy_cb (GtkObject *obj, Query *q);
void 
query_add_view (Query *q, QueryView *qv)
{
	g_return_if_fail (q);
	g_return_if_fail (qv);
	g_return_if_fail (GTK_IS_OBJECT (qv->obj));

	g_assert (qv->obj);
	g_assert (IS_QUERY (qv->obj) || IS_DB_TABLE (qv->obj));		

	q->views = g_slist_append (q->views, qv);
	gtk_signal_connect (GTK_OBJECT (qv), "destroy",
			    GTK_SIGNAL_FUNC (query_view_destroy_cb), q);

#ifdef debug_signal
	g_print (">> 'QUERY_VIEW_ADDED' from query_add_view\n");
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[QUERY_VIEW_ADDED], qv);
#ifdef debug_signal
	g_print ("<< 'QUERY_VIEW_ADDED' from query_add_view\n");
#endif	


	/* FIXME: look into the 'top level' query and make a copy
	   of the relevant joins into this query.
	*/
}

static void 
query_view_destroy_cb (GtkObject *obj, Query *q)
{
	QueryView *qv = QUERY_VIEW (obj);
	
	if (qv)
		query_del_view (q, qv);	
}

void 
query_del_view (Query *q, QueryView *qv)
{
	GSList *fields;
	
	g_return_if_fail (q);
	g_return_if_fail (qv);
	g_return_if_fail (g_slist_find (q->views, qv));

	/* When a query view is removed, remove any QueryField which depended
	   on this query view */
	fields = q->fields;
	while (fields) {
		GSList *list;

		list = query_field_get_monitored_objects (QUERY_FIELD (fields->data));
		if (g_slist_find (list, qv->obj)) {
			query_del_field (q, QUERY_FIELD (fields->data));
			fields = q->fields;
		}
		else
			fields = g_slist_next (fields);
		g_slist_free (list);
	}
	

	q->views = g_slist_remove (q->views, qv);
#ifdef debug_signal
	g_print (">> 'QUERY_VIEW_REMOVED' from query_del_view\n");
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[QUERY_VIEW_REMOVED], qv);
#ifdef debug_signal
	g_print ("<< 'QUERY_VIEW_REMOVED' from query_del_view\n");
#endif	
	gtk_object_unref (GTK_OBJECT (qv));
}




/*
 * QueryFields Management
 */

/* gets the newt ID which is free in the query. I proposed != 0,
   then the ID will either be the one proposed, or the next one free 
   (with a warning, because it should not happen) */
guint
query_get_unique_field_id (Query *q, guint proposed_id)
{
	guint id = 0;

	if (proposed_id) {
		if (query_get_field_by_id (q, proposed_id)) {
			g_warning ("query_get_unique_field_id(): id %d is already used!", proposed_id);
			return query_get_unique_field_id (q, 0);
		}
		else {
			id = proposed_id;
			if (id > q->fields_counter)
				q->fields_counter = id + 1;
		}
	}
	else {
		id = q->fields_counter;
		q->fields_counter ++;
	}

	return id;
}

static void field_destroy_cb (QueryField *field, Query *q);
static void field_field_modified_cb (QueryField *field, Query *q);
static void field_name_modified_cb (QueryField *field, Query *q);
static void field_alias_modified_cb (QueryField *field, Query *q);
void 
query_add_field (Query *q, QueryField *field)
{
	g_return_if_fail (q);
	g_return_if_fail (field);

	/* name duplication issues: we append _num to avoid duplication */
	if (field->name) {
		gchar *name;
		guint i = 1;
		GSList *list;

		name = g_strdup (field->name);
		list = q->fields;
		while (list) {
			if (QUERY_FIELD (list->data)->name &&
			    !strcmp (name, QUERY_FIELD (list->data)->name)) {
				g_free (name);
				name = g_strdup_printf ("%s_%d", field->name, i++);
				list = q->fields;
			}
			else
				list = g_slist_next (list);
		}
		if (strcmp (name, field->name)) 
			query_field_set_name (field, name);
		g_free (name);
	}

	/* alias duplication issues: we append _num to avoid duplication */
	if (field->alias) {
		gchar *name;
		guint i = 1;
		GSList *list;

		name = g_strdup (field->alias);
		list = q->fields;
		while (list) {
			if (QUERY_FIELD (list->data)->alias &&
			    !strcmp (name, QUERY_FIELD (list->data)->alias)) {
				g_free (name);
				name = g_strdup_printf ("%s_%d", field->alias, i++);
				list = q->fields;
			}
			else
				list = g_slist_next (list);
		}
		if (strcmp (name, field->alias)) 
			query_field_set_alias (field, name);
		g_free (name);
	}

	q->fields = g_slist_append (q->fields, field);

#ifdef debug_signal
	g_print (">> 'FIELD_CREATED' from query_add_field\n");
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[FIELD_CREATED], field);
#ifdef debug_signal
	g_print ("<< 'FIELD_CREATED' from query_add_field\n");
#endif

	/* fetch the "destroy" signal */
	gtk_signal_connect (GTK_OBJECT (field), "destroy",
			    GTK_SIGNAL_FUNC (field_destroy_cb), q);

	/* fetch the "field_modified" and "field_type_changed" signals */
	gtk_signal_connect (GTK_OBJECT (field), "field_modified",
			    GTK_SIGNAL_FUNC (field_field_modified_cb), q);
	gtk_signal_connect (GTK_OBJECT (field), "field_type_changed",
			    GTK_SIGNAL_FUNC (field_field_modified_cb), q);

	/* fetch the "name_modified" signal */
	gtk_signal_connect (GTK_OBJECT (field), "name_changed",
			    GTK_SIGNAL_FUNC (field_name_modified_cb), q);

	/* fetch the "alias_modified" signal */
	gtk_signal_connect (GTK_OBJECT (field), "alias_changed",
			    GTK_SIGNAL_FUNC (field_alias_modified_cb), q);
}


static void 
field_destroy_cb (QueryField *field, Query *q)
{
	g_return_if_fail (q);
	g_return_if_fail (field);

	if (g_slist_find (q->fields, field)) {
		q->fields = g_slist_remove (q->fields, field);
		
#ifdef debug_signal
		g_print (">> 'FIELD_DROPPED' from field_destroy_cb\n");
#endif
		gtk_signal_emit (GTK_OBJECT (q), query_signals[FIELD_DROPPED], field);
#ifdef debug_signal
		g_print ("<< 'FIELD_DROPPED' from field_destroy_cb\n");
#endif
		gtk_object_unref (GTK_OBJECT (field));

	}
}


static void 
field_field_modified_cb (QueryField *field, Query *q)
{
	q->conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'FIELD_MODIFIED' from field_field_modified_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[FIELD_MODIFIED], field);
#ifdef debug_signal
	g_print ("<< 'FIELD_MODIFIED' from field_field_modified_cb\n");
#endif
}


static void 
field_name_modified_cb (QueryField *field, Query *q)
{
	q->conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'FIELD_NAME_MODIFIED' from field_name_modified_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[FIELD_NAME_MODIFIED], field);
#ifdef debug_signal
	g_print ("<< 'FIELD_NAME_MODIFIED' from field_name_modified_cb\n");
#endif
}

static void 
field_alias_modified_cb (QueryField *field, Query *q)
{
	q->conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'FIELD_ALIAS_MODIFIED' from field_alias_modified_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[FIELD_ALIAS_MODIFIED], field);
#ifdef debug_signal
	g_print ("<< 'FIELD_ALIAS_MODIFIED' from field_alias_modified_cb\n");
#endif
}


void 
query_del_field (Query *q, QueryField *field)
{
	field_destroy_cb (field, q);
}

QueryField *
query_get_field_by_name (Query * q, gchar * name)
{
	QueryField *qf = NULL;
	GSList *list;

	list = q->fields;
	while (list && !qf) {
		if (!strcmp ( QUERY_FIELD(list->data)->name, name))
			qf = QUERY_FIELD (list->data);
		else
			list = g_slist_next (list);
	}

	return qf;
}

QueryField *
query_get_field_by_xmlid(Query * q, gchar * xmlid)
{
	/* The XML ID string is provided by each QueryField
	   in the QueryField::query_field_get_xml_id() function
	*/
	
	QueryField *qf = NULL;
	gchar *str, *ptr;
	guint id;
	GSList *token;

	g_return_val_if_fail (xmlid, NULL);

	str = g_strdup (xmlid);
	ptr = strtok (str, ":");
	ptr += 2;
	id = atoi (ptr);
	if (id != q->id)
		return NULL;
	
	ptr = strtok (NULL, ":");
	ptr +=2;
	id = atoi (ptr);
	g_free(str);
	token = q->fields;
	while (token && !qf) {
		if (QUERY_FIELD (token->data)->id == id)
			qf = QUERY_FIELD (token->data);
		token = g_slist_next (token);
	}

	return qf;
}


QueryField *
query_get_field_by_id (Query * q, guint id)
{
	QueryField *qf = NULL;
	GSList *list = q->fields;

	/* looking in this query first */
	while (list && !qf) {
		if (QUERY_FIELD(list->data)->id == id)
			qf = QUERY_FIELD (list->data);
		else
			list = g_slist_next (list);
	}
	
	return qf;
}


void
query_swap_fields (Query * q, QueryField *f1, QueryField *f2)
{
	GSList *list1, *list2;
	gpointer data;

	list1 = g_slist_find (q->fields, f1);
	list2 = g_slist_find (q->fields, f2);

	if (list1 && list2) {
		/* swapping list1->data and list2->data */
		data = list1->data;
		list1->data = list2->data;
		list2->data = data;

#ifdef debug_signal
		g_print (">> 'CHANGED' from query_swap_fields\n");
#endif
		gtk_signal_emit (GTK_OBJECT (q), query_signals[CHANGED]);
#ifdef debug_signal
		g_print ("<< 'CHANGED' from query_swap_fields\n");
#endif
	}
}


static void sub_query_created (Query *sub_query, Query *new_query, Query *parent_query);
static void sub_query_dropped (Query *sub_query, Query *old_query, Query *parent_query);
void 
query_add_sub_query (Query *q, Query *sibling, Query *sub_query)
{
	gint pos = -1;

	g_return_if_fail (q);
	g_return_if_fail (sub_query);
	g_return_if_fail (IS_QUERY (q));
	g_return_if_fail (IS_QUERY (sub_query));

	/* test for incompatibilities between the queries */
	if (! query_is_query_compatible (q, sub_query))
		return;
 
	if (sibling) {
		g_return_if_fail (IS_QUERY (sibling));
		if (!g_slist_find (q->sub_queries, sibling)) {
			g_warning ("sibling not child of query in %s line %d", __FILE__, __LINE__);
			return;
		}
		pos = g_slist_index (q->sub_queries, sibling);
	}
	/* if the query already has a parent then we remove that 
	   dependency */
	if (sub_query->parent) {
		gtk_object_ref (GTK_OBJECT (sub_query)); /* to avoid destruction */
		query_del_sub_query (sub_query->parent, sub_query);
	}
	
	/* make sure the sub query is not yet there */
	g_assert (!g_slist_find (q->sub_queries, sub_query));

	/* add the query to the list */
	q->sub_queries = g_slist_insert (q->sub_queries, sub_query, pos);
	sub_query->parent = q;

	g_print ("sub query %s added to query %s at pos %d\n", sub_query->name, q->name, pos);
	
#ifdef debug_signal
	g_print (">> 'QUERY_CREATED' from query_add_sub_query (Query=%s)\n", q->name);
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[QUERY_CREATED], sub_query);
#ifdef debug_signal
	g_print ("<< 'QUERY_CREATED' from query_add_sub_query (Query=%s)\n", q->name);
#endif	

	/* to have a propagation of the signal */
	gtk_signal_connect (GTK_OBJECT (sub_query), "query_created",
			    GTK_SIGNAL_FUNC (sub_query_created), q);
	gtk_signal_connect (GTK_OBJECT (sub_query), "query_dropped",
			    GTK_SIGNAL_FUNC (sub_query_dropped), q);
}

/* test for incompatibilities between the queries
   - sub query with fields linked to an inaccessible query
   - sub query which does not have the right kind of fields for UNION, etc queries
   WARNING: q can be NULL, to test for a query
            to add as a top query (immediate child of conf->top_query).
*/
static GSList * query_fields_list (Query *q, GSList *sofar);
gboolean 
query_is_query_compatible (Query *q, Query *sub_query)
{
	gboolean comp = TRUE;
	GSList *list, *hold;
	QueryField *qf;

	/* sub query with fields linked to an inaccessible query */
	list = query_fields_list (sub_query, NULL);
	while (list && comp) {
		gboolean test = TRUE;
		qf = QUERY_FIELD (list->data);
		if (qf->field_type == QUERY_FIELD_QUERY) {
			Query *query;
			/* test if the query represented by the field is present among the sub queries
			   of sub_query */
			query = query_field_query_get_query (qf);
			if (! g_slist_find (sub_query->sub_queries, query))
				test = FALSE;
#ifdef debug
			g_warning ("Rejected sub query insertion: query incompatible test 1\n");
#endif
		}
		if (test && (qf->field_type == QUERY_FIELD_QUERY_FIELD)) {

		}

		if (!test)
			comp = FALSE;
		hold = list;
		list = g_slist_remove_link (list, list);
		g_slist_free_1 (hold);
	}
	/* if there is a remaining in list, free it */
	if (list)
		g_slist_free (list);

	return comp;
}

static GSList *
query_fields_list (Query *q, GSList *sofar)
{
	GSList *list, *list2=sofar;

	if (q->fields) {
		list = g_slist_copy (q->fields);
		list2 = g_slist_concat (sofar, list);
	}

	list = q->sub_queries;
	while (list) {
		list2 = query_fields_list (QUERY (list->data), list2);
		list = g_slist_next (list);
	}

	return list2;
}

/* this is called when a sub query Q2 of the query parent_query Q1 emits a signal
   to tell a sub query Q3 has been added to Q2, so that Q1 can emit the same
   signal which gets propagated to the top level query */
static void 
sub_query_created (Query *sub_query, Query *new_query, Query *parent_query)
{
#ifdef debug_signal
		g_print (">> 'QUERY_CREATED' from sub_query_created\n");
#endif
		gtk_signal_emit (GTK_OBJECT (parent_query), query_signals[QUERY_CREATED], new_query);
#ifdef debug_signal
		g_print ("<< 'QUERY_CREATED' from sub_query_created\n");
#endif		

}

/* same as the sub_query_added() function but for the removal of queries */
static void 
sub_query_dropped (Query *sub_query, Query *old_query, Query *parent_query)
{
#ifdef debug_signal
		g_print (">> 'QUERY_DROPPED' from sub_query_dropped\n");
#endif
		gtk_signal_emit (GTK_OBJECT (parent_query), query_signals[QUERY_DROPPED], old_query);
#ifdef debug_signal
		g_print ("<< 'QUERY_DROPPED' from sub_query_dropped\n");
#endif		
}

void 
query_del_sub_query (Query *q, Query *sub_query)
{
	g_return_if_fail (q);
	g_return_if_fail (sub_query);
	g_return_if_fail (IS_QUERY (q));
	g_return_if_fail (IS_QUERY (sub_query));

	if (g_slist_find (q->sub_queries, sub_query)) {
		q->sub_queries = g_slist_remove (q->sub_queries, sub_query);
		
		/* to stop getting signals from that sub query */
		gtk_signal_disconnect_by_func (GTK_OBJECT (sub_query),
					       GTK_SIGNAL_FUNC (sub_query_created), q);
		gtk_signal_disconnect_by_func (GTK_OBJECT (sub_query),
					       GTK_SIGNAL_FUNC (sub_query_dropped), q);
		
		sub_query->parent = NULL;
#ifdef debug_signal
		g_print (">> 'QUERY_DROPPED' from query_del_sub_query\n");
#endif
		gtk_signal_emit (GTK_OBJECT (q), query_signals[QUERY_DROPPED], sub_query);
#ifdef debug_signal
		g_print ("<< 'QUERY_DROPPED' from query_del_sub_query\n");
#endif	
		gtk_object_unref (GTK_OBJECT (sub_query));
	}
}


/*
 * Query joins management
 */


static void
query_place_list (Query *query, GSList *list, QueryView *view);

static guint
find_views_in_joins_lists (QueryView *vant, QueryView *vsuc, GSList **plists, GSList **pjoins);

/* return codes for the find_views* function */
#define FOUND_NONE 0	/* no view found */
#define FOUND_VANT 1	/* ant view was found */
#define FOUND_VSUC 2	/* suc view was found */

static GSList *g_slist_set_append (GSList *list, gpointer elem);

/* query_list_joins
 * print the lists of joins list
 * as pointers
 */
static void 
query_list_joins (Query *q)
{
	GSList *list, *sub_list;

	list = q->joins;
	while (list) {
		g_print ("Master list %p (->%p)\n", list, list->data);

		sub_list = (GSList *)(list->data);
		while (sub_list) {
			g_print ("\tSub list %p (->%p)\n", sub_list, sub_list->data);
			sub_list = g_slist_next (sub_list);
		}
		list = g_slist_next (list);
	}
}

/* query_print_joins
 *
 * prints the list of joins lists 
 *  (for debug purposes)
 *
 */

static void 
query_print_joins (Query *q)
{
	GSList *list1, *list2;

	list1 = q->joins;
	while (list1) {
		list2 = (GSList *) (list1->data);
		while (list2) {
			query_join_print(QUERY_JOIN(list2->data));
			list2 = g_slist_next(list2);
			if (list2 != NULL) g_print(" ");
		}
		g_print("\n");
		list1 = g_slist_next(list1);
	}
}

/* find_views_in_joins_lists
 *
 * Auxiliary function to find a view in a list of joins lists.
 * It looks for two views (vant, vsuc) simultaneously.
 * One of the two views can be NULL.
 * It assumes the list is properly organised (see query_add_join)
 * plists is a pointer to the main list pointer and there should be a sub-list
 * pjoins can be NULL or point to an element of the first sublist
 * *plists will return the main lis element refering to the sub-list
 * where the view was found (if so) and *pjoins will point to the
 * element in the sub-list where the join was found.
 * 
 */

static guint
find_views_in_joins_lists (QueryView *vant, QueryView *vsuc, GSList **plists, GSList **pjoins)
{
	GSList *lists = *plists;
	GSList *joins = *pjoins;
	QueryJoin *jelem;
	QueryView *el_ant, *el_suc;
	guint found;

	if (*plists == NULL) return FOUND_NONE;

	found = FOUND_NONE;
	do {
		if (joins == NULL) { /* search a new sub-list */
	    		joins = (GSList *) (lists->data);
			jelem = QUERY_JOIN (joins->data);
			el_ant = query_join_get_ant_view (jelem);
			if (vant == el_ant)
				found = FOUND_VANT;
			else if (vsuc == el_ant)
				found = FOUND_VSUC;
		}
		else {
			jelem = QUERY_JOIN (joins->data);
			el_suc = query_join_get_suc_view (jelem);
			if (vant == el_suc)
				found = FOUND_VANT;
			else if (vsuc == el_suc)
				found = FOUND_VSUC;
			if (!found) { /* progress to next element and/or sub-list*/
				joins = g_slist_next (joins);
				if (joins == NULL) {
					lists = g_slist_next (lists);
				}
			}
		}
	} while (lists && !found) ;

	*plists = lists;	/* sub-list where view was found */
	*pjoins = joins;	/* join where view was found */
	return found;
}


/* g_slist_set_append 
 *
 * appends the element elem to end of the list, except if it is 
 * already in the list
 *
 */

static GSList *g_slist_set_append (GSList *list, gpointer elem)
{
	GSList *aux;

	if (list == NULL) {
		list = g_slist_prepend(NULL, elem);
	}
	else {
		aux = list;
		while ((aux->next != NULL) && (elem != aux->data)) {
			aux = aux->next;
		}

		if (elem != aux->data) {
			aux->next = g_slist_prepend(NULL, elem);
		}
	}

	return list;
}


static void query_place_join (Query *q, QueryJoin *join);

/* query_place_list
 *
 * Place joins from list starting with the one having view
 *
 * Algorithm:
 * create a queue having "view" as initial element
 * for each view in the queue
 *   for each join having this view in list
 *      remove the join from list and place it in the query
 *      get the other view in the join and queue it (no duplicates)
 *   
 */

static void
query_place_list (Query *query, GSList *list, QueryView *view)
{
	GSList *queue = NULL;	/* view queue */
	GSList *q;		/* auxiliary to queue */
	GSList *lst;		/* list iterator */
	GSList *ant;		/* iterator going before lst */
	QueryJoin *join;
	gboolean merge;

	queue = g_slist_prepend(queue, view);
	lst = list;
	while (lst != NULL) {
		/* pop view from the queue */
		view = queue->data;
		q = queue;
		queue = g_slist_remove_link (queue, q);
		g_slist_free_1 (q);

		/* place joins in list having view as a member */
		lst = list;
		ant = list;
		while (lst != NULL) {
			QueryView *vant, *vsuc;
			join = QUERY_JOIN(lst->data);
			vant = query_join_get_ant_view (join);
			vsuc = query_join_get_suc_view (join);
			merge = TRUE;
			if (view == vsuc) {
				query_join_swap_views (join);
				queue = g_slist_set_append (queue, vant);
			}
			else if (view == vant)
				queue = g_slist_set_append (queue, vsuc);
			else
				merge = FALSE;

			if (merge) {
				GSList *hold;
				/* remove join (lst2) from list2 */
				hold = lst;
				if (lst == list) { /* join is first elem of list2 */
					ant = lst->next;
					list = ant;
					lst = ant;
				}
				else {
					ant->next = lst->next;
					lst = lst->next;
				}
				hold->next = NULL;

				/* insert the join (hold) after view in list1 */
				query_place_join (query, QUERY_JOIN (hold->data));
			}
			else { /* go to next elem in lst2 */
				ant = lst;
				lst = lst->next;
			}
		}
	}

	/* release leftovers in queue */
	while (queue != NULL) {
		q = queue;
		queue = g_slist_remove_link (queue, q);
		g_slist_free_1 (q);
	}
}


/* query_place_join
 *
 * Inserts a join object in the query joins list. This operation ensures
 * the resulting list represents a valid join sequence i.e., the SQL FROM 
 * statement can be derived from it just by following the list sequence.
 *
 * After inserting a join, its ant_view holds the left side view and the
 * suc_view holds the right side view. Note that the concept of left/right
 * only makes sense inside a concrete SQL statement string. In a visual
 * query, there is no such concept. A visual indication needs to be used
 * in order to signal that the non-joined records of a table should appear.
 * For instance, an asterisk * (a la Oracle) or a small circle, could appear
 * near the table's end of the join line.
 * 
 * Duplicate joins are not added.
 * 
 * The views in the join must already exist in the query views list. This is
 * needed because if a table needs to appear twice in a join sequence, one
 * of the tables needs to be referred by an alias. This alias should already
 * exist in the visual part (the user can modify the name), hence it should
 * exist in the views list.
 *
 * Disconnected join list are supported. This allows two or more separate
 * groups of tables (now links between groups). This is a cartesian product
 * between the groups). Each group is represented by a G_SList. The query
 * contains a G_SList of these sub-lists.
 * 
 * Join list organization: (a join is (ant_view, suc_view, condition) )
 * - 1st join is (t1, t2, c2) and represents "t1 JOIN t2 ON c2"
 * - 2nd join is (t1 or t2, t3, c3)
 * - nth join is (one of t1..tn-1, tn, cn)
 *
 * Notes:
 * - in the 2nd to nth join, the left view must exist in a previous join.
 * - when a loop is created in a query, the corresponding join has two
 *   views who already exist in two separate joins. The new join is then
 *   placed imediately after the second of the existing joins.
 * 
 * A disconnected query join list contains more than one join sub-list. In
 * this case, no view from each sub-list appears in the other sub-list. 
 * These sub-lists are separated by commas in the corresponding SQL statement.
 * If a new join is added with one view in a 1st group and the other in a
 * 2nd group, then the two sub-lists have to be properly merged (the order
 * of the joins in the 2nd list usually change and sometimes two views in
 * the same join have to be swapped (in which case a LEFT JOIN becomes a
 * RIGHT JOIN, for instance).
 *
 */

static void 
query_place_join (Query *q, QueryJoin *join)
{
	QueryView *vant, *vsuc;

	g_return_if_fail (q);
	g_return_if_fail (join);
	vant = query_join_get_ant_view (join);
	g_return_if_fail (vant);
	vsuc = query_join_get_suc_view (join);
	g_return_if_fail (vsuc);
	g_return_if_fail (vant != vsuc);

	if (q->joins == NULL) {
		q->joins = g_slist_prepend (NULL, g_slist_prepend (NULL, join));
	}
	else {
		/* see if one of the join's views are in the joins lists */
		GSList *list1, *list2;
		GSList *joins=NULL, *joins2;
		guint found;

		list1 = q->joins;
		found = find_views_in_joins_lists (vant, vsuc, &list1, &joins);
		if (found == FOUND_NONE) {
			/* append a new joins list having this join */
			q->joins = g_slist_append (q->joins,
						g_slist_prepend (NULL, join));
		}
		else {
			/* swap views if needed */
			GSList *elem;
			if (found == FOUND_VSUC) {
				query_join_swap_views (join);
				vsuc = vant; /* No need for old vsuc! */
			}

			/* search for the 2nd view, vsuc */
			joins2 = joins->next;
			if (joins2 == NULL) {
				list2 = g_slist_next(list1);
			}
			else
				list2 = list1;
			if (list2 == NULL)
				found = FOUND_NONE;
			else {
				joins2 = (GSList *) list2->data;
				found = find_views_in_joins_lists (NULL, vsuc, &list2, &joins2);
			}

			if ((list1 == list2) && joins2) { /* found loop in the list */
				query_join_set_skip_view(join);
				joins = joins2;
			}

			/* - insert new join after joins position */
			elem = g_slist_prepend (NULL, join);
			elem->next = joins->next;
			joins->next = elem;

			if (found != FOUND_NONE) {
				if (list1 != list2) { /* have merge two sub-lists */
					q->joins = g_slist_remove_link (q->joins, list2);
					query_place_list (q, list2->data, vsuc);
					g_slist_free_1 (list2);
				}
			}
		}
	}
}


static void join_destroy_cb (QueryJoin *join, Query *q);
static void join_modified_cb (QueryJoin *join, Query *q);
static void join_pairs_modified_cb (QueryJoin *join, QueryJoinPair *pair, Query *q);

void 
query_add_join (Query *q, QueryView *v1, QueryField *f1, QueryView  *v2, QueryField *f2)
{
	QueryJoin *qj = NULL;
	QueryView *qj_vant = NULL, *qj_vsuc = NULL;
	GSList *list;

	g_print ("***Before query_add_join()\n");
	query_list_joins (q);

	/* Look for an existing QueryJoin or define one */
	list = q->joins;
	while (list && !qj) {
		GSList *list2;
		list2 = (GSList *) (list->data);
		while (list2 && !qj) {
			qj_vant = QUERY_JOIN (list2->data)->ant_view;
			qj_vsuc = QUERY_JOIN (list2->data)->suc_view;
			if (((qj_vant == v1) && (qj_vsuc == v2)) ||
			    ((qj_vant == v2) && (qj_vsuc == v1)))
				qj = QUERY_JOIN (list2->data);
			list2 = g_slist_next (list2);
		}
		list = g_slist_next (list);
	}
			
	if (!qj) {
		qj = QUERY_JOIN (query_join_new (q, v1, v2));
		query_place_join (q, qj);

#ifdef debug_signal
		g_print (">> 'JOIN_CREATED' from query_add_join\n");
#endif
		gtk_signal_emit (GTK_OBJECT (q), query_signals[JOIN_CREATED], qj);
#ifdef debug_signal
		g_print ("<< 'JOIN_CREATED' from query_add_join\n");
#endif

		/* fetch the "destroy" signal */
		gtk_signal_connect (GTK_OBJECT (qj), "destroy",
				    GTK_SIGNAL_FUNC (join_destroy_cb), q);

		/* fetch the "type_changed" signal */
		gtk_signal_connect (GTK_OBJECT (qj), "type_changed",
				    GTK_SIGNAL_FUNC (join_modified_cb), q);
		gtk_signal_connect (GTK_OBJECT (qj), "pair_added",
				    GTK_SIGNAL_FUNC (join_pairs_modified_cb), q);
		gtk_signal_connect (GTK_OBJECT (qj), "pair_removed",
				    GTK_SIGNAL_FUNC (join_pairs_modified_cb), q);
	}

	query_join_add_pair (qj, GTK_OBJECT (f1), GTK_OBJECT (f2));

	g_print ("***After query_add_join()\n");
	query_list_joins (q);
}

static void 
join_destroy_cb (QueryJoin *join, Query *q)
{
	GSList *lists, *antl;
	GSList *joins, *antj;
	GSList *slist2, *tmp;

	g_return_if_fail (q);
	g_return_if_fail (join);
	g_return_if_fail (q->joins);

	g_print ("***Before join_destroy_cb(%p)\n", join);
	query_list_joins (q);

	/* search for the join in q->joins */
	lists = q->joins;	/* points to a sub-list (lists->data) */
	antl = lists;		/* points to the sub-list before lists */
	joins = (GSList *) (lists->data);
	antj = joins;		/* points to the element before joins */

	while (lists && (join != joins->data)) {
		antj = joins;
		joins = g_slist_next (joins);
		if (! joins) { /* search in a new sub-list */
			antl = lists;
			lists = g_slist_next (lists);
			if (lists) {
				joins = (GSList *) (lists->data);
				antj = joins;
			}
		}
	}

	/* g_return_if_fail (lists);*/
	/* sometimes this callback is called twice and lists is empty */
	if (!lists) return;

	/* remove the join from the list */
	slist2 = joins->next;	/* holds join sub-list after the join item */
	if (antj == joins) { /* it's the first join in the list */ 
		/* the sub-list becomes empty and has to be removed */
		if (antl == lists) 	/* it's the first sub-list */
			q->joins = lists->next;
		else
			antl->next = lists->next;
		g_slist_free_1 (lists);
	}

	if (query_join_skip_view (QUERY_JOIN(joins->data))) {
		/* the join _identifies_ a loop; just remove it */
		antj->next = joins->next;
	}
	else if (slist2 && query_join_skip_view (QUERY_JOIN(slist2->data))) {
		/* the join is _part_ of a loop; just remove it */
		antj->next = joins->next;
		/* the next join identifies a loop; unset it */
		query_join_unset_skip_view (QUERY_JOIN(slist2->data));
	}
	else {
		/* split the list and re-place the 2nd half in the query */
		antj->next = NULL;
		while (slist2) {
			query_join_unset_skip_view (QUERY_JOIN (slist2->data));
			query_place_join (q, QUERY_JOIN (slist2->data));
			tmp = slist2;
			slist2 = g_slist_next (slist2);
			g_slist_free_1 (tmp);
		}
	}

	g_slist_free_1 (joins);


#ifdef debug_signal
	g_print (">> 'JOIN_DROPPED' from query_del_join\n");
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[JOIN_DROPPED], join);
#ifdef debug_signal
	g_print ("<< 'JOIN_DROPPED' from query_del_join\n");
#endif
	/* now we don't care about the object */
	/* unref calls destroy which calls shutdown, which emits a destroy
	 * signal which makes this callback being called twice *sigh* */
	gtk_object_unref (GTK_OBJECT (join));

	g_print ("***After join_destroy_cb(%p)\n", join);
	query_list_joins (q);
}

static void 
join_modified_cb (QueryJoin *join, Query *q)
{
	q->conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'JOIN_MODIFIED' from join_modified_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[JOIN_MODIFIED], join);
#ifdef debug_signal
	g_print ("<< 'JOIN_MODIFIED' from join_modified_cb\n");
#endif
}

static void 
join_pairs_modified_cb (QueryJoin *join, QueryJoinPair *pair, Query *q)
{
	join_modified_cb (join, q);	
}


void 
query_del_join (Query * q, QueryJoin *join)
{
	join_destroy_cb (join, q);
}


/*
 * Query WHERE/HAVING management
 */

void 
query_add_where (Query * q, QueryWhere *where,
		     QueryWhere * parent,
		     QueryWhere * sibling)
{
	/* modeled after query_add_field */
	GNode *n_parent, *n_sibling;

	g_return_if_fail (q);
	g_return_if_fail (where);

	/* tests to assert the validity of parameters */
	n_parent = query_where_gnode_find (q, parent);
	n_sibling = query_where_gnode_find (q, sibling);
	if (n_parent && !g_node_is_ancestor (q->where_tree, n_parent))
		return;
	if (n_parent && n_sibling && (n_sibling->parent != n_parent))
		return;
	
	g_node_insert_data_before(n_parent, n_sibling, where);
	
#ifdef debug_signal
	g_print (">> 'WHERE_CREATED' from query_add_where\n");
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[WHERE_CREATED], where);
#ifdef debug_signal
	g_print ("<< 'WHERE_CREATED' from query_add_where\n");
#endif	
	/* FIXME: catch signals from the QueryWhere object */
}

void 
query_del_where (Query * q, QueryWhere *where)
{
	/* modeled after query_del_field */
	GNode *n_where;

	g_return_if_fail (q);
	g_return_if_fail (where);

	n_where = query_where_gnode_find (q, where);

	if (n_where) {
		/* removal from the tree */
		g_node_destroy (n_where);
		
		/* flag set */
		where->signal_already_emitted = TRUE;
		
#ifdef debug_signal
		g_print (">> 'WHERE_DROPPED' from query_del_where\n");
#endif
		gtk_signal_emit (GTK_OBJECT (q), query_signals[WHERE_DROPPED], where);
#ifdef debug_signal
		g_print ("<< 'WHERE_DROPPED' from query_del_where\n");
#endif
		/* now we don't care about the object */
		gtk_object_unref (GTK_OBJECT (where));
	}
}

void 
query_move_where (Query * q,
		      QueryWhere * where,
		      QueryWhere * to_parent,
		      QueryWhere * to_sibling)
{
	GNode *n_where;

	n_where = query_where_gnode_find (q, where);

	if (n_where) {
		GNode *n_parent, *n_sibling = NULL;

		g_node_unlink (n_where);
		n_parent = query_where_gnode_find (q, to_parent);
		if (to_sibling)
			n_sibling = query_where_gnode_find (q, to_sibling);
		if (n_parent)
			g_node_insert_before (n_parent, n_sibling, n_where);
		else
			g_node_insert_before (q->where_tree, n_sibling, n_where);	
#ifdef debug_signal
		g_print (">> 'WHERE_MOVED' from query_move_where\n");
#endif
		gtk_signal_emit (GTK_OBJECT (q),
				 query_signals[WHERE_MOVED], where);
#ifdef debug_signal
		g_print ("<< 'WHERE_MOVED' from query_move_where\n");
#endif
	}
}

GNode *
query_where_gnode_find (Query * q, QueryWhere * where)
{
	g_return_val_if_fail (q != NULL, NULL);
	g_return_val_if_fail (where != NULL, NULL);

	return g_node_find (q->where_tree, G_LEVEL_ORDER, G_TRAVERSE_ALL, where);
}


/*
 * Management of the Envs
 */
void 
query_add_env (Query *q, GtkObject *env)
{
	g_return_if_fail (q);
	g_return_if_fail (env);
	g_return_if_fail (IS_QUERY (q));
	g_return_if_fail (GTK_IS_OBJECT (env));

	/* make sure the env is not yet there */
	g_assert (!g_slist_find (q->envs, env));

	/* add the query to the list */
	q->envs = g_slist_append (q->envs, env);
	
#ifdef debug_signal
	g_print (">> 'ENV_CREATED' from query_add_env (Query=%s)\n", q->name);
#endif
	gtk_signal_emit (GTK_OBJECT (q), query_signals[ENV_CREATED], env);
#ifdef debug_signal
	g_print ("<< 'ENV_CREATED' from query_add_env (Query=%s)\n", q->name);
#endif	

	q->conf->save_up_to_date = FALSE;
}

void 
query_del_env (Query *q, GtkObject *env)
{
	g_return_if_fail (q);
	g_return_if_fail (env);
	g_return_if_fail (IS_QUERY (q));
	g_return_if_fail (GTK_IS_OBJECT (env));

	if (g_slist_find (q->envs, env)) {
		q->envs = g_slist_remove (q->envs, env);
		
#ifdef debug_signal
		g_print (">> 'ENV_DROPPED' from query_del_env\n");
#endif
		gtk_signal_emit (GTK_OBJECT (q), query_signals[ENV_DROPPED], env);
#ifdef debug_signal
		g_print ("<< 'ENV_DROPPED' from query_del_env\n");
#endif	
		gtk_object_unref (env);
	}

	q->conf->save_up_to_date = FALSE;
}

/*
 * Interrogation helper functions 
 */

gint
query_is_table_field_printed (Query * q, DbField * field)
{
	gint result = -1, i;
	GSList *list;
	QueryField *qf;
	DbTable *table;

	table = database_find_table_from_field (q->conf->db, field);
	if (!table)
		return result;

	list = q->fields;
	i = 0;
	while (list && (result == -1)) {
		qf = QUERY_FIELD (list->data);
		if ((qf->field_type == QUERY_FIELD_FIELD) && 
		    qf->is_printed) {
			GSList *objs;
			
			objs = query_field_get_monitored_objects (qf);
			if (g_slist_find (objs, table))
				result = i;
			g_slist_free (objs);

			i++;
		}
		list = g_slist_next (list);
	}

	return result;
}

QueryField *
query_get_field_by_pos (Query * q, gint pos)
{
	QueryField *qf = NULL;
	gint i;
	GSList *list;

	if (pos < 0)
		return NULL;
	list = q->fields;
	i = -1;
	while (list && !qf) {
		if (qf->is_printed) {
			i++;
			if (i == pos)
				qf = QUERY_FIELD (list->data);
		}
		list = g_slist_next (list);
	}

	return qf;
}

gboolean
query_is_table_concerned (Query * q, DbTable * table)
{
	gboolean found = FALSE;
	GSList *list;
	QueryField *qf;

	list = q->fields;
	while (list && !found) {
		qf = QUERY_FIELD (list->data);
		if (qf->field_type == QUERY_FIELD_FIELD) {
			GSList *objs;
			
			objs = query_field_get_monitored_objects (qf);
			if (g_slist_find (objs, table))
				found = TRUE;;
			g_slist_free (objs);
		}
		list = g_slist_next (list);
	}

	return found;
}


static gchar *get_select_part_of_sql (Query * q, GSList * missing_values);
static gchar *get_from_part_of_sql (Query * q);
static gchar *get_where_part_of_sql (Query * q, GSList * missing_values);

/*
 * Getting the SQL version of the query
 */
gchar *
query_get_select_query (Query * q, GSList * missing_values)
{
	gchar *str, *retval = NULL;
	GString *SQL;

	if (q->type == QUERY_TYPE_SQL)
		retval = g_strdup (q->text_sql);
	else {
		SQL = g_string_new ("SELECT\n");

		/* SELECT objects */
		str = get_select_part_of_sql (q, missing_values);
		if (str) {
			g_string_append (SQL, str);
			g_free (str);
		}

		/* FROM relations */
		str = get_from_part_of_sql (q);
		if (str) {
			g_string_sprintfa (SQL, "\nFROM\n%s", str);
			g_free (str);
		}

		/* WHERE conditions */
		str = get_where_part_of_sql (q, missing_values);
		if (str) {
			g_string_sprintfa (SQL, "\nWHERE\n%s", str);
			g_free (str);
		}

		retval = SQL->str;
		g_string_free (SQL, FALSE);
	}

	return retval;
}


/* creates a new string with the objects that go in the SELECT part to SQL */
static gchar *
get_select_part_of_sql (Query * q, GSList * missing_values)
{
	gchar *str, *retval = NULL;
	GString *objects;
	gboolean first;
	GSList *fields;

	g_return_val_if_fail (q != NULL, NULL);

	fields = q->fields;
	first = TRUE;
	objects = g_string_new ("");
	while (fields) {
		QueryField *qf;
		qf = QUERY_FIELD (fields->data);
		
		if (qf->alias) {
			if (first)
				first = FALSE;
			else
				g_string_append (objects, ",\n");

			str = query_field_render_as_sql (qf, missing_values);
			g_string_sprintfa (objects, "%s as %s", str,
					   qf->alias);
			g_free (str);
		}
		fields = g_slist_next (fields);
	}

	if (!first) {
		retval = objects->str;
		g_string_free (objects, FALSE);
	}
	else
		g_string_free (objects, TRUE);

	return retval;
}


/* returns the WHERE part of a query while taking care of the automatic links
   and any other WHERE constraint imposed to the query 
   The allocated string has to be freed */
static gchar *
get_where_part_of_sql (Query * q, GSList * missing_values)
{
	gboolean firstnode = TRUE;
	GString *gstr;
	gchar *wh = NULL;

	gstr = g_string_new ("");
	/* FIXME: VIV if (q->where_tree->children) { */
/* 		GNode *gnode = q->where_tree->children; */

/* 		while (gnode) { */
/* 			if (firstnode) */
/* 				firstnode = FALSE; */
/* 			else */
/* 				g_string_append (gstr, "AND "); */
/* 			wh = query_where_render_as_sql (QUERY_WHERE (gnode->data), */
/* 							    missing_values); */
/* 			g_string_append (gstr, wh); */
/* 			g_free (wh); */
/* 			g_string_append (gstr, "\n"); */
/* 			gnode = gnode->next; */
/* 		} */
/* 	} */

	if (!firstnode) {
		wh = gstr->str;
		g_string_free (gstr, FALSE);
	}
	else 
		g_string_free (gstr, TRUE);

	return wh;
}


/* Creates the FROM part of the SQL statement
 * The general case is a comma separated list of relations (views) where a
 * relation can be a table or colection of joined tables.
 * The token "FROM" is not returned, only the list of tables
 *
 * ex: tblAlone, tblA INNER JOIN tblB ON tblA.ID=tblB.AID 
 *     LEFT JOIN tblC ON tblB.ID=tblC.BID
 *
 * Restrictions:
 *  - only equi-joins; TODO FM: introduce theta joins (add new condition 
 *    column in the Joins' clist and edit it in a dialog box, or inline
 *    if widget supports it)
 *   
 * Note: those remaining tables (without join conditions) will create a
 *   cartesian product (CROSS), unless the user manually specifys joining
 *   conditions in the WHERE part of the query). The join conditions in the
 *   WHERE part as not automatically identified as such. This means they are
 *   not extracted and converted to the FROM part.
 *
 */

static gchar *
get_from_part_of_sql (Query * q)
{
	GSList *lists, *list, *views;
	QueryJoin *jelem;
	QueryView *el_ant, *el_suc;
	gboolean first;
	GString *joins=NULL, *all_joins=NULL;
	gchar *retval;

	views = q->views;
	while (views) {
		lists = q->joins;
		list = NULL;
		el_ant = QUERY_VIEW (views->data);
		if (FOUND_NONE == find_views_in_joins_lists (el_ant, NULL, &lists, &list)) {
			if (all_joins)
				g_string_append (all_joins, ", ");
			else
				all_joins = g_string_new ("");
			g_string_append (all_joins, query_view_get_alias_exp (el_ant));
		}
		views = g_slist_next (views);
	}

	lists = q->joins;
	while (lists) {
		list = (GSList *) (lists->data);
		first = TRUE;
		joins = g_string_new ("");
		while (list) {
			jelem = QUERY_JOIN (list->data);
			el_ant = query_join_get_ant_view (jelem);
			el_suc = query_join_get_suc_view (jelem);
			if (first) {	/* Ex: "tableAnt INNER JOIN tableSuc ON condition" */
				first = FALSE;
				g_string_sprintf (joins, "%s %s %s ON %s",
						query_view_get_alias_exp (el_ant),
 						query_join_get_join_type_sql (jelem),
						query_view_get_alias_exp (el_suc),
						query_join_get_condition (jelem));
			}
			else if (query_join_skip_view (jelem))
				/* Ex: "... AND condition" */
				g_string_sprintfa (joins, " AND %s",
						query_join_get_condition (jelem));
			else
				/* Ex: "... INNER_JOIN tableSuc ON condition" */
				g_string_sprintfa (joins, " %s %s ON %s",
 					   	query_join_get_join_type_sql (jelem),
						query_view_get_alias_exp (el_suc),
						query_join_get_condition (jelem));
			list = g_slist_next (list);
		}
		if (all_joins)
			g_string_append (all_joins, ", ");
		else
			all_joins = g_string_new ("");
		g_string_append (all_joins, joins->str);
		g_string_free (joins, TRUE);
		lists = g_slist_next (lists);
	}

	if (all_joins) {
		retval = all_joins->str;
		g_string_free (all_joins, FALSE);
	}
	else
		retval = NULL;

	return retval;
}


#ifdef debug
static gchar* query_type_to_str (Query *q)
{
	gchar *str;
	switch (q->type) {
	case QUERY_TYPE_STD:
		str = "Standard";
		break;
	case QUERY_TYPE_UNION:
		str = "Union";
		break;
	case QUERY_TYPE_INTERSECT:
		str = "Intersect";
		break;
	default:
		str = "Unknown!!!";
	}

	return str;
}

static gchar* field_type_to_str (QueryFieldType t) 
{
	gchar *str;

	switch (t) {
	case QUERY_FIELD_FIELD:
		str = "Table's field";
		break;
	case QUERY_FIELD_ALLFIELDS:
		str = "Table.*";
		break;
	case QUERY_FIELD_AGGREGATE:
		str = "Aggregate";
		break;
	case QUERY_FIELD_FUNCTION:
		str = "Function";
		break;
	case QUERY_FIELD_VALUE:
		str = "Constant value";
		break;
	case QUERY_FIELD_QUERY:
		str = "Whole query";
		break;
	case QUERY_FIELD_QUERY_FIELD:
		str = "Other query's field";
		break;
	default:
		str = "UNKNOWN TYPE";
	}
	return str;
}

/* structure debug function */
static void static_query_dump_contents (Query *q, guint offset);
void 
query_dump_contents (Query *q)
{
	g_return_if_fail (q);
	static_query_dump_contents (q, 0);
}

static void 
static_query_dump_contents (Query *q, guint offset)
{
	GSList *list, *sub_list;
	gchar *str;
	guint i;

	str = g_new0 (gchar, offset+1);
	for (i=0; i<offset; i++)
		str[i] = ' ';
	str[offset] = 0;

	g_print ("%s" D_COL_H1 "==== QUERY DEBUG DUMP -> %p (parent = %p) ====\n" D_COL_NOR, str, q, q->parent);
	g_print ("%s" D_COL_H2 "Id:" D_COL_NOR "    %d\n", str, q->id);
	g_print ("%s" D_COL_H2 "Name:" D_COL_NOR "  %s\n", str, q->name);
	g_print ("%s" D_COL_H2 "Descr:" D_COL_NOR " %s\n", str, q->descr);
	g_print ("%s" D_COL_H2 "Type:" D_COL_NOR "  %s\n", str, query_type_to_str (q));
	g_print ("%s" D_COL_H2 "State:" D_COL_NOR, str);
	if (query_fields_activated (q))
		g_print (D_COL_OK " All fields activated\n" D_COL_NOR);
	else
		g_print (D_COL_ERR " NON ACTIVATED\n" D_COL_NOR);

	/* QueryViews */
	g_print ("%s" D_COL_H2 "Query Views:\n" D_COL_NOR, str);
	list = q->views;
	if (!list)
		g_print ("%s\tNONE\n", str);
	else {
		while (list) {
			QueryView *qv;
			qv = (QueryView *)(list->data);
			if (IS_DB_TABLE (qv->obj))
				g_print ("%s\ttable: %s ", str, DB_TABLE (qv->obj)->name);
			else {
				if (IS_QUERY (qv->obj))
					g_print ("%s\tquery: %s (id=%d) ", str,QUERY (qv->obj)->name, 
						 QUERY (qv->obj)->id);
				else
					g_print (D_COL_ERR "%s\tUNKNOWN!!! " D_COL_NOR, str);
			}
			g_print ("%sOCC=%d ", str, qv->occ);
			if (qv->alias)
				g_print ("%salias=%s\n", str, qv->alias);
			else
				g_print ("\n");
			list = g_slist_next (list);
		}
	}

	/* Query Fields */
	g_print ("%s" D_COL_H2 "Query fields:\n" D_COL_NOR, str);
	list = q->fields;
	if (!list)
		g_print ("%s\tNONE\n", str);
	else {
		while (list) {
			QueryField *qf;
			GSList *mo, *iter;

			qf = QUERY_FIELD (list->data);
			g_print ("%s  " D_COL_H2 "* field:" D_COL_NOR " %s (@ %p)\n", str, qf->name, qf);
			g_print ("%s\tid: %d\n", str, qf->id);
			if (qf->is_printed)
				g_print ("%s\tPrinted, alias: %s\n", str, qf->alias);
			else
				g_print ("%s\tNot printed:\n", str);
			g_print ("%s\tfield type:     %s\n", str, field_type_to_str (qf->field_type));
			mo = query_field_get_monitored_objects (qf);
			g_print ("%s\tmonitored objs: ", str);
			iter = mo;
			while (iter) {
				g_print ("%p ", iter->data);
				iter = g_slist_next (iter);
			}
			if (mo) {
				g_print ("\n");
				g_slist_free (mo);
			}
			else
				g_print ("NONE\n");

			if (qf->activated) {
				gchar *sql;

				sql = query_field_render_as_sql (qf, NULL);
				g_print ("%s\tSQL= %s\n", str, sql);
				g_free (sql);
				g_print ("%s\t" D_COL_OK "activated\n" D_COL_NOR , str);
			}
			else
				g_print("%s\t" D_COL_ERR "not activated\n" D_COL_NOR, str);
			list = g_slist_next (list);
		}
	}


	/* QueryJoins */
	g_print ("%s" D_COL_H2 "Query Joins:\n" D_COL_NOR, str);
	list = q->joins;
	if (!list)
		g_print ("%s\tNONE\n", str);
	while (list) {
		g_print ("%s" D_COL_H2 "\tMaster list " D_COL_NOR "%p (->%p)\n", str, list, list->data);
		sub_list = (GSList*)(list->data);
		while (sub_list) {
			QueryJoin *qj;
			GSList *list2;
			qj = QUERY_JOIN (sub_list->data);
			g_print ("%s\t%p->Join with %d pair(s)\n", str, sub_list, g_slist_length (qj->pairs));
			list2 = qj->pairs;
			while (list2) {
				QueryJoinPair *pair = QUERY_JOIN_PAIR_CAST (list2->data);
				g_print ("%s\t\t* ", str);
				if (IS_DB_FIELD (pair->ant_field))
					g_print ("DbField %s", DB_FIELD (pair->ant_field)->name);
				else
					g_print ("QueryField %s", QUERY_FIELD (pair->ant_field)->name);
				if (IS_DB_FIELD (pair->suc_field))
					g_print (" -- DbField %s\n", DB_FIELD (pair->suc_field)->name);
				else
					g_print (" -- QueryField %s\n", QUERY_FIELD (pair->suc_field)->name);
				
				list2 = g_slist_next (list2);
			}
			sub_list = g_slist_next (sub_list);
		}

		list = g_slist_next (list);
	} 

	/* Sub Queries */
	g_print ("%s" D_COL_H2 "Sub queries:\n" D_COL_NOR, str);
	list = q->sub_queries;
	if (!list)
		g_print ("%s\tNONE\n", str);
	else {
		while (list) {
			Query *sq;
			sq = QUERY (list->data);
			static_query_dump_contents (sq, offset+5);
			list = g_slist_next (list);
		}
	}
	
	g_free (str);
}

#endif
