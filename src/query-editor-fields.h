/* query-editor-fields.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY_EDITOR_FIELDS__
#define __QUERY_EDITOR_FIELDS__

#include <gnome.h>
#include "query.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */


/*
 *
 * the QueryEditorFields Widget
 *
 */

#define QUERY_EDITOR_FIELDS(obj)          GTK_CHECK_CAST (obj, query_editor_fields_get_type(), QueryEditorFields)
#define QUERY_EDITOR_FIELDS_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, query_editor_fields_get_type (), QueryEditorFieldsClass)
#define IS_QUERY_EDITOR_FIELDS(obj)       GTK_CHECK_TYPE (obj, query_editor_fields_get_type ())


	typedef struct _QueryEditorFields QueryEditorFields;
	typedef struct _QueryEditorFieldsClass QueryEditorFieldsClass;

	typedef enum {
		QEF_EXPR      = 1 << 0,
		QEF_COND      = 1 << 1,
		QEF_UNSPEC    = 1 << 2,
		QEF_NOTNAMED  = 1 << 3,
		QEF_LAST
	} QueryEditorFieldsShowType;


	/* struct for the object's data */
	struct _QueryEditorFields
	{
		GtkVBox        object;

		Query         *query;
		QueryField    *selection;

		/* Widgets */
		GtkWidget     *mainlist;

		GtkWidget     *edit_btn;
		GtkWidget     *del_btn;

		QueryEditorFieldsShowType show_type;
	};

	/* struct for the object's class */
	struct _QueryEditorFieldsClass
	{
		GtkVBoxClass parent_class;
	};

	/* generic widget's functions */
	guint query_editor_fields_get_type (void);

	GtkWidget *query_editor_fields_new (Query *q);


/*
 *
 * the QueryEditorExpr Widget
 *
 */


#define QUERY_EDITOR_EXPR(obj)          GTK_CHECK_CAST (obj, query_editor_expr_get_type(), QueryEditorExpr)
#define QUERY_EDITOR_EXPR_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, query_editor_expr_get_type (), QueryEditorExprClass)
#define IS_QUERY_EDITOR_EXPR(obj)       GTK_CHECK_TYPE (obj, query_editor_expr_get_type ())


	typedef struct _QueryEditorExpr QueryEditorExpr;
	typedef struct _QueryEditorExprClass QueryEditorExprClass;


	/* struct for the object's data */
	struct _QueryEditorExpr
	{
		GtkVBox            object;

		QueryEditorFields *qef;
		QueryField        *orig_qf;
		QueryField        *top_qf;
		gboolean           data_detached;

		/* information about the selected QF for edition */
		QueryField        *sel_qf;
		QueryField        *parent_sel_qf;
		gint               parent_sel_ref;

		/* independant list of QueryFields while the edition takes place */
		GSList            *fields;

		/* Widgets */
		GtkWidget         *frame;
		GtkWidget         *current_expr;
		GtkWidget         *expr_container;
		GtkWidget         *current_area;
		GtkWidget         *area_container;
		GtkWidget         *name_entry;
		GtkWidget         *alias_entry;
		GtkWidget         *type_omenu;
	};

	/* struct for the object's class */
	struct _QueryEditorExprClass
	{
		GtkVBoxClass       parent_class;

		void (*status)     (QueryEditorExpr * qee, gboolean active);
	};

	/* generic widget's functions */
	guint      query_editor_expr_get_type (void);

	/* Calling with qf=NULL is for the creation of a new QueryField */
	GtkWidget *query_editor_expr_new      (QueryEditorFields * qef, QueryField *qf);

	/* This function removes any signal it catches from the QueryField objects
	   it manages, so we can work freely with them with no problem for the QueryEditorFields
	   widget (which won't be refreshed anymore) */
	void       query_editor_expr_detach   (QueryEditorExpr * qee);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
