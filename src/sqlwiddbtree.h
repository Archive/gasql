/* sqlwiddbtree.h
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __SQL_WID_DB_TREE__
#define __SQL_WID_DB_TREE__

#include <gtk/gtksignal.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkscrolledwindow.h>
#include "database.h"
#include "conf-manager.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define SQL_WID_DB_TREE(obj)          GTK_CHECK_CAST (obj, sql_wid_db_tree_get_type(), SqlWidDbTree)
#define SQL_WID_DB_TREE_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, sql_wid_db_tree_get_type (), SqlWidDbTreeClass)
#define IS_SQL_WID_DB_TREE(obj)       GTK_CHECK_TYPE (obj, sql_wid_db_tree_get_type ())


	typedef struct _SqlWidDbTree SqlWidDbTree;
	typedef struct _SqlWidDbTreeClass SqlWidDbTreeClass;

	typedef enum
	{
		SQL_WID_DB_TREE_TABLES =           1 << 0,
		SQL_WID_DB_TREE_SEQS =             1 << 1,
		SQL_WID_DB_TREE_TABLE_FIELDS =     1 << 2,
		SQL_WID_DB_TREE_QUERIES =          1 << 3,
		SQL_WID_DB_TREE_QUERY_FIELDS =     1 << 4,
		SQL_WID_DB_TREE_TABLES_SEL =       1 << 5,
		SQL_WID_DB_TREE_SEQS_SEL =         1 << 6,
		SQL_WID_DB_TREE_TABLE_FIELDS_SEL = 1 << 7,
		SQL_WID_DB_TREE_QUERY_FIELDS_SEL = 1 << 8
	}
	SqlWidDbTreeAffMode;

	/* struct for the object's data */
	struct _SqlWidDbTree
	{
		GtkVBox      object;

		guint        mode;
		ConfManager *conf;
		GtkWidget   *sw;	  /* the scrolled window */
		GtkWidget   *tree;	  /* root Tree */
		GtkWidget   *tables;	  /* Tree */
		GtkWidget   *sequences;	  /* Tree */
		GtkWidget   *queries;	  /* Tree */
		GtkWidget   *tables_ti;	  /* Tree Item */
		GtkWidget   *sequences_ti;/* Tree Item */
		GtkWidget   *queries_ti;  /* Tree Item */
		gpointer     selection;
	};

	/* struct for the object's class */
	struct _SqlWidDbTreeClass
	{
		GtkVBoxClass parent_class;

		void (*field_selected) (SqlWidDbTree * wid, DbTable * table, DbField * field);
		void (*table_selected) (SqlWidDbTree * wid, DbTable * table);
		void (*seq_selected)   (SqlWidDbTree * wid, DbSequence * seq);
	};

	/* generic widget's functions */
	guint      sql_wid_db_tree_get_type (void);
	GtkWidget *sql_wid_db_tree_new      (ConfManager *conf);

	/* widget's behaviour */
	void       sql_wid_db_tree_set_mode (SqlWidDbTree * wid, guint mode);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
