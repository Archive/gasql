/* mainpagequery.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mainpagequery.h"
#include "query.h"
#include "query-editor.h"
#include "form.xpm"
#include "query.xpm"
#include "grid.xpm"
/*#include "query-exec.h"*/
#include "query-create-druid.h"

static void main_page_query_class_init (MainPageQueryClass * class);
static void main_page_query_init (MainPageQuery * wid);
static void main_page_query_initialize (MainPageQuery * wid);

/* structure associated to each row */
typedef struct
{
	Query       *query;
	GtkWidget   *edit_dlg;
}
Row_Data;

/*
 * static functions 
 */
static gint press_handler_event (GtkWidget * widget,
				 GdkEventButton * event, MainPageQuery * mpq);
static void selection_made (GtkCTree * ctree,
			    GtkCTreeNode * row,
			    gint column, MainPageQuery * mpq);
static void selection_unmade (GtkCTree * ctree,
			      GtkCTreeNode * row,
			      gint column, MainPageQuery * mpq);
static void drop_query_button_cb (GtkWidget * button, MainPageQuery * mpq);
static void edit_query_button_cb (GtkWidget * button, MainPageQuery * mpq);
static void exec_query_button_cb (GtkWidget * button, MainPageQuery * mpq);
static void create_query_button_cb (GtkWidget * button, MainPageQuery * mpq);
static void copy_query_button_cb (GtkWidget * button, MainPageQuery * mpq);
static void paste_query_button_cb (GtkWidget * button, MainPageQuery * mpq);
static void paste_sub_query_button_cb (GtkWidget * button, MainPageQuery * mpq);

static void conn_closed_cb (GtkWidget * serveur, MainPageQuery * mpq);
static void add_query (ConfManager *conf, Query *new_query, MainPageQuery * mpq);
static void remove_query (ConfManager *conf, Query *old_query, MainPageQuery * mpq);

/* static variables */
static GdkPixmap *query_icon = NULL;
static GdkBitmap *query_mask = NULL;
static GdkPixmap *form_icon = NULL;
static GdkBitmap *form_mask = NULL;
static GdkPixmap *grid_icon = NULL;
static GdkBitmap *grid_mask = NULL;

guint
main_page_query_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Main_Page_Query",
			sizeof (MainPageQuery),
			sizeof (MainPageQueryClass),
			(GtkClassInitFunc) main_page_query_class_init,
			(GtkObjectInitFunc) main_page_query_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_vbox_get_type (), &f_info);
	}

	return f_type;
}

static void
main_page_query_class_init (MainPageQueryClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
}

static void
main_page_query_init (MainPageQuery * wid)
{
	GtkWidget *sw, *bb;

	/* setting spaces,... */
	gtk_container_set_border_width (GTK_CONTAINER (wid), GNOME_PAD / 2);

	/* Scrolled Window for CTree */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (wid), sw, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (sw);

	/* CTree */
	wid->ctree = gtk_ctree_new (2, 0);
	gtk_clist_set_row_height (GTK_CLIST (wid->ctree), 22);
	gtk_clist_set_column_title (GTK_CLIST (wid->ctree), 0,
				    _("Query"));
	gtk_clist_set_column_title (GTK_CLIST (wid->ctree), 1,
				    _("Description"));
	gtk_clist_set_selection_mode (GTK_CLIST (wid->ctree),
				      GTK_SELECTION_SINGLE);
	gtk_clist_column_titles_show (GTK_CLIST (wid->ctree));
	gtk_clist_column_titles_passive (GTK_CLIST (wid->ctree));
	gtk_clist_set_column_auto_resize (GTK_CLIST (wid->ctree), 0, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (wid->ctree), 1, TRUE);
	gtk_container_add (GTK_CONTAINER (sw), wid->ctree);
	gtk_widget_show (wid->ctree);
	gtk_signal_connect (GTK_OBJECT (wid->ctree), "tree_select_row",
			    GTK_SIGNAL_FUNC (selection_made), wid);
	gtk_signal_connect (GTK_OBJECT (wid->ctree), "tree_unselect_row",
			    GTK_SIGNAL_FUNC (selection_unmade), wid);
	gtk_signal_connect (GTK_OBJECT (wid->ctree),
			    "button_press_event",
			    GTK_SIGNAL_FUNC (press_handler_event), wid);

	/*
	 * buttons for the queries
	 */

	/* Button Box */
	bb = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);
	gtk_box_pack_end (GTK_BOX (wid), bb, FALSE, TRUE, GNOME_PAD);
	gtk_widget_show (bb);
	wid->queries_bbox = bb;

	/* New Query Button */
	wid->new_query = gtk_button_new_with_label (_("Create Query"));
	gtk_container_add (GTK_CONTAINER (bb), wid->new_query);
	gtk_widget_show (wid->new_query);
	gtk_signal_connect (GTK_OBJECT (wid->new_query), "clicked",
			    GTK_SIGNAL_FUNC (create_query_button_cb), wid);

	/* Edit Query Button */
	wid->edit_query = gtk_button_new_with_label (_("Properties"));
	gtk_container_add (GTK_CONTAINER (bb), wid->edit_query);
	gtk_widget_show (wid->edit_query);
	gtk_signal_connect (GTK_OBJECT (wid->edit_query), "clicked",
			    GTK_SIGNAL_FUNC (edit_query_button_cb), wid);

	/* View Query Button */
	wid->exec_query = gtk_button_new_with_label (_("Execute"));
	gtk_container_add (GTK_CONTAINER (bb), wid->exec_query);
	gtk_signal_connect (GTK_OBJECT (wid->exec_query), "clicked",
			    GTK_SIGNAL_FUNC (exec_query_button_cb), wid);
	gtk_widget_show (wid->exec_query);

	/* Remove Query Button */
	wid->remove_query = gtk_button_new_with_label (_("Delete"));
	gtk_container_add (GTK_CONTAINER (bb), wid->remove_query);
	gtk_widget_show (wid->remove_query);
	gtk_signal_connect (GTK_OBJECT (wid->remove_query), "clicked",
			    GTK_SIGNAL_FUNC (drop_query_button_cb), wid);

	wid->sel_node = NULL;
	wid->new_dlgs = NULL;
}

GtkWidget *
main_page_query_new (ConfManager * conf)
{
	GtkObject *obj;
	MainPageQuery *wid;

	obj = gtk_type_new (main_page_query_get_type ());
	wid = MAIN_PAGE_QUERY (obj);
	wid->conf = conf;

	main_page_query_initialize (wid);

	gtk_signal_connect (GTK_OBJECT (conf), "query_added",
			    GTK_SIGNAL_FUNC (add_query), wid);
	gtk_signal_connect (GTK_OBJECT (conf), "query_removed",
			    GTK_SIGNAL_FUNC (remove_query), wid);

	gtk_signal_connect (GTK_OBJECT (conf->srv), "conn_closed",
			    GTK_SIGNAL_FUNC (conn_closed_cb), wid);
	return GTK_WIDGET (obj);
}

static void
main_page_query_initialize (MainPageQuery * mpq)
{

	gtk_widget_set_sensitive (mpq->remove_query, FALSE);
	gtk_widget_set_sensitive (mpq->exec_query, FALSE);
	gtk_widget_set_sensitive (mpq->edit_query, FALSE);
	gtk_widget_set_sensitive (mpq->new_query, FALSE);
	conf_manager_register_sensitive_on_connect (mpq->conf, GTK_WIDGET (mpq->new_query));
}


/*
 * Widget specific static functions 
 */

/* Callback to handle the button presses over the CTree */
static gint
press_handler_event (GtkWidget * widget,
		     GdkEventButton * event, MainPageQuery * mpq)
{
	gint row, col;
	GtkCTreeNode *tnode;
	Row_Data *rd;

	if (GTK_IS_CTREE (widget)) {
		/* setting the right selection */
		gtk_clist_get_selection_info (GTK_CLIST (widget),
					      event->x, event->y, &row, &col);
		tnode = gtk_ctree_node_nth (GTK_CTREE (widget), row);
		
		if (tnode && (mpq->sel_node != tnode) && (event->button != 1))
			gtk_ctree_select (GTK_CTREE (widget), tnode);
		if (!tnode && mpq->sel_node)
			gtk_ctree_unselect (GTK_CTREE (widget), mpq->sel_node);

		rd = (Row_Data *)
			gtk_ctree_node_get_row_data (GTK_CTREE (widget), tnode);

		if (event->button == 3) {
			GtkWidget *menu, *wid;

			menu = gtk_menu_new ();
			wid = gtk_menu_item_new_with_label (_("Properties"));
			gtk_signal_connect (GTK_OBJECT (wid), "activate",
					    GTK_SIGNAL_FUNC (edit_query_button_cb),
					    mpq);
			gtk_menu_append (GTK_MENU (menu), wid);
			gtk_widget_show (wid);
			gtk_widget_set_sensitive (wid, FALSE);
			if (rd && mpq->sel_node)
				gtk_widget_set_sensitive (wid, TRUE);
			
			wid = gtk_menu_item_new_with_label (_("Execute"));
			gtk_signal_connect (GTK_OBJECT (wid), "activate",
					    GTK_SIGNAL_FUNC (exec_query_button_cb),
					    mpq);
			gtk_menu_append (GTK_MENU (menu), wid);
			gtk_widget_show (wid);
			gtk_widget_set_sensitive (wid, FALSE);
			if (rd && mpq->sel_node)
				gtk_widget_set_sensitive (wid, TRUE);
				
			wid = gtk_menu_item_new_with_label (_("Copy"));
			gtk_signal_connect (GTK_OBJECT (wid), "activate",
					    GTK_SIGNAL_FUNC (copy_query_button_cb),
					    mpq);
			gtk_menu_append (GTK_MENU (menu), wid);
			gtk_widget_show (wid);
			gtk_widget_set_sensitive (wid, FALSE);
			if (rd && mpq->sel_node)
				gtk_widget_set_sensitive (wid, TRUE);

			wid = gtk_menu_item_new_with_label (_("Paste"));
			gtk_signal_connect (GTK_OBJECT (wid), "activate",
					    GTK_SIGNAL_FUNC (paste_query_button_cb),
					    mpq);
			gtk_menu_append (GTK_MENU (menu), wid);
			gtk_widget_show (wid);
			gtk_widget_set_sensitive (wid, FALSE);
			if (conf_manager_get_clipboard_object (mpq->conf)) {
				GtkObject *q;

				q = conf_manager_get_clipboard_object (mpq->conf);
				if (IS_QUERY (q))
					gtk_widget_set_sensitive (wid, TRUE);
			}

			wid = gtk_menu_item_new_with_label (_("Paste as sub query"));
			gtk_signal_connect (GTK_OBJECT (wid), "activate",
					    GTK_SIGNAL_FUNC (paste_sub_query_button_cb),
					    mpq);
			gtk_menu_append (GTK_MENU (menu), wid);
			gtk_widget_show (wid);
			gtk_widget_set_sensitive (wid, FALSE);
			if (conf_manager_get_clipboard_object (mpq->conf)) {
				GtkObject *q;

				q = conf_manager_get_clipboard_object (mpq->conf);
				if (rd && mpq->sel_node && IS_QUERY (q))
					gtk_widget_set_sensitive (wid, TRUE);
			}
			
			wid = gtk_menu_item_new_with_label (_("Delete"));
			gtk_signal_connect (GTK_OBJECT (wid), "activate",
					    GTK_SIGNAL_FUNC (drop_query_button_cb),
					    mpq);
			gtk_menu_append (GTK_MENU (menu), wid);
			gtk_widget_show (wid);
			gtk_widget_set_sensitive (wid, FALSE);
			if (rd && mpq->sel_node)
				gtk_widget_set_sensitive (wid, TRUE);
			
			gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
					NULL, NULL, event->button,
					event->time);
			/* Tell calling code that we have handled this event */
			return TRUE;
		}
	}

	return FALSE;
}


static void
selection_made (GtkCTree * ctree,
		GtkCTreeNode * row, gint column, MainPageQuery * mpq)
{
	Row_Data *rd;

	mpq->sel_node = row;
	rd = (Row_Data *) gtk_ctree_node_get_row_data (GTK_CTREE (mpq->ctree),
						       row);
	gtk_widget_show (mpq->queries_bbox);
	gtk_widget_set_sensitive (mpq->remove_query, TRUE);
	gtk_widget_set_sensitive (mpq->edit_query, TRUE);
	gtk_widget_set_sensitive (mpq->exec_query, TRUE);
}

static void
selection_unmade (GtkCTree * ctree,
		  GtkCTreeNode * row, gint column, MainPageQuery * mpq)
{
	if (mpq->sel_node == row) {
		mpq->sel_node = NULL;
		gtk_widget_set_sensitive (mpq->remove_query, FALSE);
		gtk_widget_set_sensitive (mpq->edit_query, FALSE);
		gtk_widget_set_sensitive (mpq->exec_query, FALSE);
	}
}

static void drop_query_question_cb (gint reply, MainPageQuery * mpq);
static void
drop_query_button_cb (GtkWidget * button, MainPageQuery * mpq)
{
	GtkWidget *dlg;
	gchar *que;
	Row_Data *rd;

	if (mpq->sel_node) {
		rd = (Row_Data *) gtk_ctree_node_get_row_data (GTK_CTREE (mpq->ctree),
							       mpq->sel_node);

		que = g_strdup_printf (_("Remove the query %s?"),
				       rd->query->name);
		dlg = gnome_app_question_modal (GNOME_APP (mpq->conf->app),
						que,
						(GnomeReplyCallback)
						drop_query_question_cb, mpq);
		g_free (que);
		gtk_widget_show (dlg);
	}
}

static void
drop_query_question_cb (gint reply, MainPageQuery * mpq)
{
	if (reply == GNOME_YES) {
		Row_Data *rd;
		rd = (Row_Data *) gtk_ctree_node_get_row_data (GTK_CTREE (mpq->ctree),
							       mpq->sel_node);

		/* we should not have the top query here */
		g_assert (rd->query->parent);

		query_del_sub_query (rd->query->parent, rd->query);
	}
}

static void query_edit_dlg_clicked_cb (GnomeDialog *dlg, gint button_number, Row_Data * rd);
static void query_edit_dlg_destroy_cb (GtkObject * obj, Row_Data * rd);
static void query_edit_dlg_query_destroy_cb (GtkObject * obj, GnomeDialog *dlg);
static void query_edit_dlg_name_changed_cb (Query *q, GnomeDialog *dlg);
static void
edit_query_button_cb (GtkWidget * button, MainPageQuery * mpq)
{
	if (mpq->sel_node) {
		Row_Data *rd;
		rd = (Row_Data *)
			gtk_ctree_node_get_row_data (GTK_CTREE (mpq->ctree),
						     mpq->sel_node);
		if (1 || !rd->edit_dlg) {
			GtkWidget *dlg, *editor;
			/* when the DLG is closed, query_edit_dlg_destroy_cb is called */
			dlg = gnome_dialog_new ("", GNOME_STOCK_BUTTON_CLOSE, NULL);
			query_edit_dlg_name_changed_cb (rd->query, GNOME_DIALOG (dlg));
			editor = query_editor_new (rd->query);
			gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), editor, TRUE,
					    TRUE, 0);
			gtk_signal_connect (GTK_OBJECT (dlg), "destroy",
					    GTK_SIGNAL_FUNC (query_edit_dlg_destroy_cb), rd);
			gtk_signal_connect (GTK_OBJECT (dlg), "clicked",
					    GTK_SIGNAL_FUNC (query_edit_dlg_clicked_cb), rd);

			gtk_signal_connect_while_alive (GTK_OBJECT (rd->query), "name_changed",
							GTK_SIGNAL_FUNC (query_edit_dlg_name_changed_cb), dlg,
							GTK_OBJECT (dlg));
			rd->edit_dlg = dlg;
			gtk_window_set_policy (GTK_WINDOW (dlg), FALSE, TRUE, FALSE);
			gtk_widget_show_all (dlg);
		}
		else
			gdk_window_raise (rd->edit_dlg->window);
	}
}

static void 
query_edit_dlg_query_destroy_cb (GtkObject * obj, GnomeDialog *dlg)
{
	gnome_dialog_close (GNOME_DIALOG (dlg));
}

static void 
query_edit_dlg_clicked_cb (GnomeDialog *dlg, gint button_number, Row_Data * rd)
{
	gnome_dialog_close (GNOME_DIALOG (dlg));
}

static void
query_edit_dlg_destroy_cb (GtkObject * obj, Row_Data * rd)
{
	rd->edit_dlg = NULL;
}

static void 
query_edit_dlg_name_changed_cb (Query *q, GnomeDialog *dlg)
{
	gchar *str;

	str = g_strdup_printf (_("Edition of query: %s"), q->name);
	gtk_window_set_title (GTK_WINDOW (dlg), str);
	g_free (str);
}

static void
exec_query_button_cb (GtkWidget * button, MainPageQuery * mpq)
{
	Row_Data *rd;

	if (mpq->sel_node) {
		Query *q;

		rd = (Row_Data *)
			gtk_ctree_node_get_row_data (GTK_CTREE (mpq->ctree), mpq->sel_node);
		q = rd->query;
		if (!q->envs) {	/* program error, should not happen */
			gnome_app_message (GNOME_APP (q->conf->app),
					   _("A program error has occured:\n"
					     "This query has no QueryEnv, please make "
					     "a bug report."));
			return;
		}
		
		/* FIXME: do the execution part */
	}
}


static void create_dialog_cancel_cb (QueryCreateDruid *druid, GtkWidget *dlg);
static void create_dialog_finish_cb (QueryCreateDruid *druid, GtkWidget *dlg);
static void
create_query_button_cb (GtkWidget * button, MainPageQuery * mpq)
{
	GtkWidget *wid, *dlg;


	dlg = gnome_dialog_new (_("Creation of a new query"), NULL);
	wid = query_create_druid_new (mpq->conf);

	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox),
			    wid, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (wid);
	gtk_object_set_data (GTK_OBJECT (dlg), "druid", wid);
	gtk_object_set_data (GTK_OBJECT (dlg), "mpq", mpq);


	gtk_signal_connect (GTK_OBJECT (wid), "finish",
			    GTK_SIGNAL_FUNC (create_dialog_finish_cb), dlg);

	gtk_signal_connect (GTK_OBJECT (wid), "cancel",
			    GTK_SIGNAL_FUNC (create_dialog_cancel_cb), dlg);

	mpq->new_dlgs = g_slist_append (mpq->new_dlgs, dlg);
	gtk_widget_show (dlg);
}

static void create_dialog_cancel_cb (QueryCreateDruid *druid, GtkWidget *dlg)
{
	MainPageQuery *mpq;

	mpq = gtk_object_get_data (GTK_OBJECT (dlg), "mpq");
	mpq->new_dlgs = g_slist_remove (mpq->new_dlgs, dlg);
	gnome_dialog_close (GNOME_DIALOG (dlg));
}

static void create_dialog_finish_cb (QueryCreateDruid *druid, GtkWidget *dlg)
{
	Query *q;

	g_assert (druid->q);

	gtk_object_ref (GTK_OBJECT (druid->q));
	q = druid->q;
	create_dialog_cancel_cb (druid, dlg);

	/* FIXME: do sth with the query now... */
#ifdef debug
	query_dump_contents (q);
#endif
	
}



static void
copy_query_button_cb (GtkWidget * button, MainPageQuery * mpq)
{
	Row_Data *rd;

	if (mpq->sel_node) {
		rd = (Row_Data *)
			gtk_ctree_node_get_row_data (GTK_CTREE (mpq->ctree),
						     mpq->sel_node);
		if (rd->query) {
			gchar *str;

			/* put it into the clipboard */
			str = query_get_xml_id (rd->query);
			conf_manager_set_clipboard_str (mpq->conf, str);
			g_free (str);
		}
	}
}

static void
paste_query_button_cb (GtkWidget * button, MainPageQuery * mpq)
{
	Row_Data *rd;
	GtkObject *obj;
	Query *pasteq, *refq = NULL, *newq;
	Query *compq = NULL;

	obj = conf_manager_get_clipboard_object (mpq->conf);
	g_return_if_fail (obj);
	g_return_if_fail (IS_QUERY (obj));
	pasteq = QUERY (obj);
	
	if (mpq->sel_node) {
		rd = (Row_Data *)
			gtk_ctree_node_get_row_data (GTK_CTREE (mpq->ctree),
						     mpq->sel_node);
		refq = rd->query;
	}

	/* test pasted query compatibility with the where we awnt to paste */
	if (refq) 
		compq = refq->parent;

	if (! query_is_query_compatible (compq, pasteq)) {
		gchar *str;
		GtkWidget *dlg;
		if (refq && (compq != QUERY (refq->conf->top_query)))
			str = g_strdup_printf (_("Can't paste the query before query of '%s'\nbecause "
						 "of incompatbile composition between the query to paste\n"
						 "and the parent query '%s'."),
					       refq->name, compq->name);
		else
			str = g_strdup (_("Can't paste the query as a top query because its\n"
					  "structure does not permit it."));
		dlg = gnome_warning_dialog (str);
		g_free (str);
		gtk_widget_show (dlg);
		return;
	}
		

	newq = QUERY (query_new_copy (pasteq));
	g_assert (newq);	
	
#ifdef debug
	g_print ("pasting query %s ", newq->name);
	if (refq) 
		g_print ("Before query %s\n", refq->name);
	else
		g_print ("At the end\n");
#endif
	if (refq)
		query_add_sub_query (refq->parent, refq, newq);
	else
		query_add_sub_query (QUERY (mpq->conf->top_query), refq, newq);
}

static void
paste_sub_query_button_cb (GtkWidget * button, MainPageQuery * mpq)
{
	Row_Data *rd;
	GtkObject *obj;
	Query *pasteq, *refq = NULL;
	
	obj = conf_manager_get_clipboard_object (mpq->conf);
	g_return_if_fail (obj);
	g_return_if_fail (IS_QUERY (obj));
	pasteq = QUERY (obj);

	if (mpq->sel_node) {
		rd = (Row_Data *)
			gtk_ctree_node_get_row_data (GTK_CTREE (mpq->ctree),
						     mpq->sel_node);
		refq = rd->query;
		if (refq) {
			Query *newq;

			/* test pasted query compatibility with the where we awnt to paste */
			if (! query_is_query_compatible (refq, pasteq)) {
				gchar *str;
				GtkWidget *dlg;
				str = g_strdup_printf (_("Can't paste the query as a sub query of '%s'\n"
							 "because of incompatbile composition between the queries."),
						       refq->name);

				dlg = gnome_warning_dialog (str);
				g_free (str);
				gtk_widget_show (dlg);
				return;
			}

			newq = QUERY (query_new_copy (pasteq));
			g_assert (newq);
#ifdef debug
			g_print ("pasting query %s as sub query of %s\n", newq->name, refq->name);
#endif

			query_add_sub_query (refq, NULL, newq);
		}
	}
}


static void
conn_closed_cb (GtkWidget * serveur, MainPageQuery * mpq)
{
	GtkCTreeNode *tnode;
	GList *list;
	GSList *slist;
	Row_Data *rd;

	list = GTK_CLIST (mpq->ctree)->row_list;

	while (list) {
		tnode = GTK_CTREE_NODE (list);
		rd = (Row_Data *) gtk_ctree_node_get_row_data (GTK_CTREE (mpq->ctree),
							       tnode);
		remove_query (mpq->conf, rd->query, mpq);
		list = GTK_CLIST (mpq->ctree)->row_list;
	}

	/* close any add dialog */
	slist = mpq->new_dlgs;
	while (slist) {
		gnome_dialog_close (GNOME_DIALOG (slist->data));
		slist = g_slist_next (slist);
	}
	g_slist_free (mpq->new_dlgs);
	mpq->new_dlgs = NULL;
}

static Row_Data     *get_query_row_data (MainPageQuery * mpq, Query * q);
static GtkCTreeNode *get_query_ctreenode (MainPageQuery * mpq, Query * q);
static void edit_query_query_changed_cb (Query * q, MainPageQuery * mpq);
static void
remove_query (ConfManager *conf, Query *old_query, MainPageQuery * mpq)
{
	Row_Data *rd;
	GtkCTreeNode *tnode;
	GSList *subs;

	g_print ("MPQ Removing Query %s\n", old_query->name);
	g_return_if_fail (mpq != NULL);

	tnode = get_query_ctreenode (mpq, old_query);
	rd = get_query_row_data (mpq, old_query);

	/* sub queries first */
	subs = old_query->sub_queries;
	while (subs) {
		Query *sq;

		sq = QUERY (subs->data);
		remove_query (conf, sq, mpq);
		subs = g_slist_next (subs);
	}

	/* sensitiveness of the buttons */
	if (mpq->sel_node && (mpq->sel_node == tnode)) {
		gtk_widget_set_sensitive (mpq->remove_query, FALSE);
		gtk_widget_set_sensitive (mpq->edit_query, FALSE);
		gtk_widget_set_sensitive (mpq->exec_query, FALSE);
	}

	/* the work needs to be saved */
	mpq->conf->save_up_to_date = FALSE;

	if (rd) {
		if (rd->edit_dlg)
			gnome_dialog_close (GNOME_DIALOG (rd->edit_dlg));
		g_free (rd);
	}

	/* disconnect "name_changed" signal */
	if (old_query->parent)
		gtk_signal_disconnect_by_func (GTK_OBJECT (old_query),
					       GTK_SIGNAL_FUNC (edit_query_query_changed_cb),
					       mpq);

	/* in the end: remove entry from CTree (will also remove the sub nodes
	   for the sub queries) */
	gtk_ctree_remove_node (GTK_CTREE (mpq->ctree), tnode);
}


static void
add_query (ConfManager *conf, Query *new_query, MainPageQuery * mpq)
{

	GSList *subs;

	g_return_if_fail (mpq != NULL);
	g_return_if_fail (new_query != NULL);

	if (!query_icon)
		query_icon = gdk_pixmap_create_from_xpm_d (GTK_WIDGET
							   (mpq->conf->app)->
							   window, &query_mask,
							   NULL, query_xpm);
	
	if (new_query != QUERY (conf->top_query)) {
		Row_Data *rd;
		GtkCTreeNode *tnode;
		gchar *row[2];

		rd = g_new0 (Row_Data, 1);
		rd->query = new_query;
		rd->edit_dlg = NULL;
		row[0] = g_strdup (new_query->name);
		if (new_query->descr)
			row[1] = g_strdup (new_query->descr);
		else
			row[1] = g_strdup ("");
		
		if (new_query->parent == QUERY (conf->top_query)) {
			/* does not appear as a sub query of another query */
			GSList *sibling;
			GtkCTreeNode *tsibling = NULL;

			sibling = g_slist_find (new_query->parent->sub_queries, new_query);
			g_assert (sibling);
			sibling = g_slist_next (sibling);
			if (sibling) 
				tsibling =  get_query_ctreenode(mpq, QUERY (sibling->data));

			tnode = gtk_ctree_insert_node (GTK_CTREE (mpq->ctree), NULL, tsibling,
						       row, GNOME_PAD / 2., query_icon,
						       query_mask, query_icon, query_mask,
						       FALSE, FALSE);
		}
		else { 
			/* it's a sub query of a query already displayed */
			GSList *sibling;
			GtkCTreeNode *tparent=NULL, *tsibling = NULL;

			tparent = get_query_ctreenode (mpq, new_query->parent);
			g_assert (tparent);

			sibling = g_slist_find (new_query->parent->sub_queries, new_query);
			g_assert (sibling);
			sibling = g_slist_next (sibling);
			if (sibling) 
				tsibling =  get_query_ctreenode(mpq, QUERY (sibling->data));

			tnode = gtk_ctree_insert_node (GTK_CTREE (mpq->ctree), tparent, tsibling,
						       row, GNOME_PAD / 2., query_icon,
						       query_mask, query_icon, query_mask,
						       FALSE, FALSE);
		}
		g_free (row[0]);
		g_free (row[1]);
		
		gtk_ctree_node_set_row_data (GTK_CTREE (mpq->ctree), tnode, rd);
		gtk_signal_connect (GTK_OBJECT (rd->query), "name_changed",
				    GTK_SIGNAL_FUNC (edit_query_query_changed_cb),
				    mpq);
	}

	subs = new_query->sub_queries;
	while (subs) {
		Query *sq;

		sq = QUERY (subs->data);
		add_query (conf, sq, mpq);
		subs = g_slist_next (subs);
	}
}

static Row_Data     *
get_query_row_data (MainPageQuery * mpq, Query * q)
{
	GtkCTreeNode *tnode;

	tnode = get_query_ctreenode (mpq, q);
	if (tnode)
		return (Row_Data *) gtk_ctree_node_get_row_data (GTK_CTREE (mpq->ctree), tnode);
	else
		return NULL;
}

struct tree_parse_data {
	GtkCTreeNode *retval;
	Query *q;
};

static void parse_func (GtkCTree *ctree, GtkCTreeNode *node, struct tree_parse_data *data );
static GtkCTreeNode *
get_query_ctreenode (MainPageQuery * mpq, Query * q)
{
	
	struct tree_parse_data *data;
	GtkCTreeNode *retval = NULL;

	data = g_new0 (struct tree_parse_data, 1);
	data->q = q;
	gtk_ctree_pre_recursive (GTK_CTREE (mpq->ctree), NULL, 
				 (GtkCTreeFunc) parse_func, data);

	retval = data->retval;
	g_free (data);
        return retval;
}

static void 
parse_func (GtkCTree *ctree, GtkCTreeNode *node, struct tree_parse_data *data)
{
	Row_Data *rd;

	if (data->retval) 
		return; /* already found */

	rd = (Row_Data *) gtk_ctree_node_get_row_data (ctree, node);
	g_assert (rd);
	if (rd->query == data->q)
		data->retval = node;
}


static void
edit_query_query_changed_cb (Query * q, MainPageQuery * mpq)
{
	Row_Data *rd = NULL;
	GtkCTreeNode *tnode;

	tnode = get_query_ctreenode (mpq, q);
	if (tnode)
		rd = (Row_Data *) gtk_ctree_node_get_row_data (GTK_CTREE (mpq->ctree), tnode);

	if (rd && (rd->query == q)) {
		/* updating names and description */
		gtk_ctree_node_set_pixtext (GTK_CTREE (mpq->ctree), tnode, 0, q->name,
					    GNOME_PAD / 2., query_icon, query_mask);
		if (q->descr)
			gtk_ctree_node_set_text (GTK_CTREE (mpq->ctree), tnode,
						 1, q->descr);
		else
			gtk_ctree_node_set_text (GTK_CTREE (mpq->ctree), tnode,
						 1, "");
	}
}
