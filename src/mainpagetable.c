/* mainpagetable.c
 *
 * Copyright (C) 1999 - 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "mainpagetable.h"
#include "tableedit.h"
#include "query.h"
#include "query-env.h"
#include "query-exec.h"

static void main_page_table_class_init (MainPageTableClass * class);
static void main_page_table_init (MainPageTable * wid);
static void main_page_table_initialize (MainPageTable * wid);


typedef struct
{
	ConfManager *conf;
	DbTable     *table;
	Query       *query;
	QueryEnv    *env;
	GtkWidget   *edit_dlg;
}
Row_Data;


/*
 * static functions 
 */
static void selection_made (GtkWidget * wid,
			    gint row,
			    gint column,
			    GdkEventButton * event, gpointer data);
static gint press_handler_event (GtkWidget * widget,
				 GdkEventButton * event, MainPageTable * mpt);
static void remove_table_cb (GtkObject * obj, MainPageTable * wid);
static void edit_table_cb (GtkObject * obj, MainPageTable * wid);
static void view_records_button_cb (GtkObject * obj, MainPageTable * mpt);
static void database_added_cb (ConfManager *conf, Database *db, MainPageTable * mpt);
static void database_removed_cb (ConfManager *conf, Database *db, MainPageTable * mpt);
static void main_page_table_conn_close_cb (GtkObject * obj,
					   MainPageTable * mpt);
static void main_page_table_add_cb (GtkObject * obj, DbTable * table,
				    MainPageTable * wid);
static void main_page_table_drop_cb (GtkObject * obj, DbTable * table,
				     MainPageTable * wid);
static void main_page_db_updated_cb (Database * db, MainPageTable * mpt);


guint
main_page_table_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Main_Page_Table",
			sizeof (MainPageTable),
			sizeof (MainPageTableClass),
			(GtkClassInitFunc) main_page_table_class_init,
			(GtkObjectInitFunc) main_page_table_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_vbox_get_type (), &f_info);
	}

	return f_type;
}

static void
main_page_table_class_init (MainPageTableClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
}

static void
main_page_table_init (MainPageTable * wid)
{
	GtkWidget *sw, *bb;
	gint i;

	/* setting spaces,... */
	gtk_container_set_border_width (GTK_CONTAINER (wid), GNOME_PAD / 2);

	/* Scrolled Window for CList */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (wid), sw, TRUE, TRUE, GNOME_PAD);
	gtk_widget_show (sw);

	/* CList */
	wid->clist = gtk_clist_new (4);
	gtk_clist_set_column_title (GTK_CLIST (wid->clist), 0, _("Name"));
	gtk_clist_set_column_title (GTK_CLIST (wid->clist), 1, _("Type"));
	gtk_clist_set_column_title (GTK_CLIST (wid->clist), 2, _("Owner"));
	gtk_clist_set_column_title (GTK_CLIST (wid->clist), 3, _("Comments"));
	gtk_clist_set_selection_mode (GTK_CLIST (wid->clist),
				      GTK_SELECTION_SINGLE);
	for (i = 0; i < 4; i++)
		gtk_clist_set_column_auto_resize (GTK_CLIST (wid->clist), i,
						  TRUE);
	gtk_clist_column_titles_show (GTK_CLIST (wid->clist));
	gtk_clist_column_titles_passive (GTK_CLIST (wid->clist));
	gtk_container_add (GTK_CONTAINER (sw), wid->clist);
	gtk_widget_show (wid->clist);
	gtk_signal_connect (GTK_OBJECT (wid->clist), "select_row",
			    GTK_SIGNAL_FUNC (selection_made), wid);
	gtk_signal_connect (GTK_OBJECT (wid->clist),
			    "button_press_event",
			    GTK_SIGNAL_FUNC (press_handler_event), wid);

	/* Button Box */
	bb = gtk_hbutton_box_new ();
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);
	gtk_box_pack_end (GTK_BOX (wid), bb, FALSE, TRUE, GNOME_PAD);
	gtk_widget_show (bb);

	/* Remove Table Button */
	wid->remove_table = gtk_button_new_with_label (_("Drop Table"));
	gtk_container_add (GTK_CONTAINER (bb), wid->remove_table);
	gtk_widget_show (wid->remove_table);
	gtk_signal_connect (GTK_OBJECT (wid->remove_table), "clicked",
			    GTK_SIGNAL_FUNC (remove_table_cb), wid);

	/* Edit Table Button */
	wid->edit_table = gtk_button_new_with_label (_("Properties"));
	gtk_container_add (GTK_CONTAINER (bb), wid->edit_table);
	gtk_widget_show (wid->edit_table);
	gtk_signal_connect (GTK_OBJECT (wid->edit_table), "clicked",
			    GTK_SIGNAL_FUNC (edit_table_cb), wid);

	/* View Table Button */
	wid->view_table = gtk_button_new_with_label (_("View records"));
	gtk_container_add (GTK_CONTAINER (bb), wid->view_table);
	gtk_widget_show (wid->view_table);
	gtk_signal_connect (GTK_OBJECT (wid->view_table), "clicked",
			    GTK_SIGNAL_FUNC (view_records_button_cb), wid);

	/* New Table Button */
	wid->new_table = gtk_button_new_with_label (_("Create Table"));
	gtk_container_add (GTK_CONTAINER (bb), wid->new_table);
	/* FIXME: use bonobo controls to create a table */
	gtk_widget_show (wid->new_table);

	wid->sel_row = -1;
}

GtkWidget *
main_page_table_new (ConfManager * conf)
{
	GtkObject *obj;
	MainPageTable *wid;

	obj = gtk_type_new (main_page_table_get_type ());
	wid = MAIN_PAGE_TABLE (obj);
	wid->conf = conf;

	main_page_table_initialize (wid);

	gtk_signal_connect (GTK_OBJECT (conf), "database_added",
			    GTK_SIGNAL_FUNC (database_added_cb), wid);

	gtk_signal_connect (GTK_OBJECT (conf), "database_removed",
			    GTK_SIGNAL_FUNC (database_removed_cb), wid);

	gtk_signal_connect (GTK_OBJECT (conf->srv), "conn_closed",
			    GTK_SIGNAL_FUNC (main_page_table_conn_close_cb),
			    obj);

	return GTK_WIDGET (obj);
}

static void database_added_cb (ConfManager *conf, Database *db, MainPageTable * mpt)
{
	gtk_signal_connect (GTK_OBJECT (conf->db),
			    "table_created",
			    GTK_SIGNAL_FUNC (main_page_table_add_cb),
			    mpt);
	gtk_signal_connect (GTK_OBJECT (conf->db),
			    "table_dropped",
			    GTK_SIGNAL_FUNC (main_page_table_drop_cb),
			    mpt);
	gtk_signal_connect (GTK_OBJECT (conf->db), "updated",
			    GTK_SIGNAL_FUNC (main_page_db_updated_cb),
			    mpt);
}

static void database_removed_cb (ConfManager *conf, Database *db, MainPageTable * mpt)
{
	/* nothing to do about it */
}


static void
main_page_table_initialize (MainPageTable * wid)
{
	gtk_widget_set_sensitive (wid->remove_table, FALSE);
	gtk_widget_set_sensitive (wid->edit_table, FALSE);
	gtk_widget_set_sensitive (wid->view_table, FALSE);
	gtk_widget_set_sensitive (wid->new_table, FALSE);
	conf_manager_register_sensitive_on_connect (wid->conf,
						    GTK_WIDGET (wid->new_table));
}

void
main_page_table_set_data_dlg (MainPageTable * mpt, guint row, GtkWidget * dlg)
{
	Row_Data *rdata = NULL;

	rdata = (Row_Data *) gtk_clist_get_row_data (GTK_CLIST (mpt->clist),
						     row);
	if (rdata)
		rdata->edit_dlg = dlg;
}

GtkWidget *
main_page_table_get_data_dlg (MainPageTable * mpt, guint row)
{
	Row_Data *rdata = NULL;

	rdata = (Row_Data *) gtk_clist_get_row_data (GTK_CLIST (mpt->clist),
						     row);
	if (rdata)
		return rdata->edit_dlg;
	else
		return NULL;
}

gint
main_page_table_get_row (MainPageTable * mpt, DbTable * table)
{
	gint i = 0;
	gboolean found = FALSE;
	Row_Data *rdata;

	while ((i < GTK_CLIST (mpt->clist)->rows) && !found) {
		rdata = gtk_clist_get_row_data (GTK_CLIST (mpt->clist), i);
		if (rdata && (rdata->table == table))
			found = TRUE;
		else
			i++;
	}
	if (found)
		return i;
	else
		return -1;
}



static void
selection_made (GtkWidget * wid,
		gint row, gint column, GdkEventButton * event, gpointer data)
{
	MainPageTable *mpt = MAIN_PAGE_TABLE (data);

	mpt->sel_row = row;

	/* set sensitiveness of buttons */
	gtk_widget_set_sensitive (mpt->remove_table, TRUE);
	gtk_widget_set_sensitive (mpt->edit_table, TRUE);
	gtk_widget_set_sensitive (mpt->view_table, TRUE);
}


static gint
press_handler_event (GtkWidget * widget,
		     GdkEventButton * event, MainPageTable * mpt)
{
	gint row, col;

	if (GTK_IS_CLIST (widget)) {
		/* setting the right selection */
		gtk_clist_get_selection_info (GTK_CLIST (widget),
					      event->x, event->y, &row, &col);
		if ((mpt->sel_row != row) && (event->button != 1))
			gtk_clist_select_row (GTK_CLIST (widget), row, col);


		/* depending on what the clicks were */
		if (event->type == GDK_2BUTTON_PRESS) {
			Row_Data *rd;

			rd = (Row_Data *)
				gtk_clist_get_row_data (GTK_CLIST (widget),
							row);
			view_records_button_cb (NULL, mpt);
			/*edit_table_cb(NULL, mpt); */
			return TRUE;
		}

		if (event->button == 3) {
			GtkWidget *menu, *wid;

			menu = gtk_menu_new ();
			wid = gtk_menu_item_new_with_label (_("Properties"));
			gtk_signal_connect (GTK_OBJECT (wid), "activate",
					    GTK_SIGNAL_FUNC (edit_table_cb),
					    mpt);
			gtk_menu_append (GTK_MENU (menu), wid);
			gtk_widget_show (wid);
			wid = gtk_menu_item_new_with_label (_
							    ("View records"));
			gtk_signal_connect (GTK_OBJECT (wid), "activate",
					    GTK_SIGNAL_FUNC
					    (view_records_button_cb), mpt);
			gtk_menu_append (GTK_MENU (menu), wid);
			gtk_widget_show (wid);
			gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL,
					NULL, event->button, event->time);
			/* Tell calling code that we have handled this event */
			return TRUE;
		}
	}

	return FALSE;
}

static void remove_table_answer_cb (gint reply, MainPageTable * wid);
static void remove_table_dlg_destroy (GtkObject * obj, gpointer data);
static void
remove_table_cb (GtkObject * obj, MainPageTable * wid)
{
	Row_Data *rdata = NULL;
	gchar *str;
	GtkWidget *dlg;

	if (wid->sel_row >= 0)
		rdata = (Row_Data *)
			gtk_clist_get_row_data (GTK_CLIST (wid->clist),
						wid->sel_row);
	if (rdata) {
		if (rdata->table->is_view)
			str = g_strdup_printf (_
					       ("Do you really wish to drop the view '%s'?"),
					       rdata->table->name);
		else
			str = g_strdup_printf (_
					       ("Do you really wish to drop the table '%s'?"),
					       rdata->table->name);
		dlg = gnome_app_question (GNOME_APP (rdata->conf->app), str,
					  (GnomeReplyCallback)
					  remove_table_answer_cb, wid);
		gtk_signal_connect_while_alive (GTK_OBJECT (wid->conf->srv),
						"conn_closed",
						GTK_SIGNAL_FUNC
						(remove_table_dlg_destroy),
						dlg, GTK_OBJECT (dlg));
		g_free (str);
	}
}

static void
remove_table_answer_cb (gint reply, MainPageTable * wid)
{
	Row_Data *rdata = NULL;
	gchar *query;

	if (wid->sel_row >= 0)
		rdata = (Row_Data *)
			gtk_clist_get_row_data (GTK_CLIST (wid->clist),
						wid->sel_row);

	switch (reply) {
	case GNOME_YES:
		if (rdata) {
			if (rdata->table->is_view)
				query = g_strdup_printf ("drop view %s\n",
							 rdata->table->name);
			else
				query = g_strdup_printf ("drop table %s\n",
							 rdata->table->name);
			server_access_do_query (rdata->conf->srv, query);
			g_free (query);
			database_refresh (rdata->conf->db, rdata->conf->srv);
		}
		break;
	case GNOME_NO:
		break;
	}
}

static void
remove_table_dlg_destroy (GtkObject * obj, gpointer data)
{
	gnome_dialog_close (GNOME_DIALOG (data));
}



static void
edit_table_cb (GtkObject * obj, MainPageTable * wid)
{
	Row_Data *rdata = NULL;
	GtkWidget *dlg;

	if (wid->sel_row >= 0)
		rdata = (Row_Data *)
			gtk_clist_get_row_data (GTK_CLIST (wid->clist),
						wid->sel_row);
	if (rdata) {
		dlg = table_edit_dialog_new (wid->conf, rdata->table);
		if (dlg)
			gtk_widget_show (dlg);
	}
}

static void
view_records_button_cb (GtkObject * obj, MainPageTable * mpt)
{
	Row_Data *rdata = NULL;
	gchar *str;

	return;
	if (mpt->sel_row >= 0)
		rdata = (Row_Data *)
			gtk_clist_get_row_data (GTK_CLIST (mpt->clist),
						mpt->sel_row);
	if (rdata) {
		QueryExec *qx;

		if (!rdata->query) {	/* build the query here */
			QueryField *qf;

			if (rdata->table->is_view)
				str = g_strdup_printf (_("contents of view %s"),
						       rdata->table->name);
			else
				str = g_strdup_printf (_("contents of table %s"),
						       rdata->table->name);
			rdata->query = QUERY (query_new (str, NULL, mpt->conf));
			g_free (str);
			qf = QUERY_FIELD (query_field_new (rdata->query, rdata->table->name, 
								   QUERY_FIELD_ALLFIELDS));
			/* FIXME: tell that the qf object hust created refers to the
			   table we want */
			query_add_field (QUERY (rdata->query), qf);

			rdata->env =
				QUERY_ENV (query_env_new
					       (rdata->query));
			query_env_set_modif_table (rdata->env,
						       rdata->table);
			rdata->env->actions =
				QUERY_ACTION_INSERT | QUERY_ACTION_REFRESH |
				QUERY_ACTION_EDIT | QUERY_ACTION_DELETE |
				QUERY_ACTION_COMMIT | QUERY_ACTION_FIRST |
				QUERY_ACTION_LAST | QUERY_ACTION_PREV |
				QUERY_ACTION_NEXT;
		}

		/* FIXME VIV qx = QUERY_EXEC (query_exec_new (rdata->env)); 
		query_env_register_exec_obj (rdata->env, GTK_OBJECT (qx));
		query_exec_run (qx);*/
	}
}

static void drop_associated_query (Row_Data * rdata);
static void
main_page_table_conn_close_cb (GtkObject * obj, MainPageTable * mpt)
{
	Row_Data *rdata;
	gint i, nb = GTK_CLIST (mpt->clist)->rows;

	for (i = 0; i < nb; i++) {
		rdata = (Row_Data *)
			gtk_clist_get_row_data (GTK_CLIST (mpt->clist), 0);
		drop_associated_query (rdata);
		g_free (rdata);
		gtk_clist_remove (GTK_CLIST (mpt->clist), 0);
	}
	gtk_widget_set_sensitive (mpt->remove_table, FALSE);
	gtk_widget_set_sensitive (mpt->edit_table, FALSE);
	gtk_widget_set_sensitive (mpt->view_table, FALSE);
	gtk_widget_set_sensitive (mpt->new_table, FALSE);
	mpt->sel_row = -1;
}


/* this CB is intended to be connected to the "table_created" signal of the 
   Database objects it represents.  So:
   - obj is a Database object
*/
static void comments_table_changed_cb (DbTable * table, Row_Data * rdata);
static void
main_page_table_add_cb (GtkObject * obj, DbTable * table,
			MainPageTable * wid)
{
	gchar *col[4];
	gchar *str;
	gint i = 0, j;
	gboolean stop = FALSE;
	Row_Data *rdata;

	col[0] = g_strdup (table->name);
	if (table->is_view)
		col[1] = g_strdup (_("View"));
	else
		col[1] = g_strdup (_("Table"));
	col[2] = g_strdup (table->owner);
	if (table->comments)
		col[3] = g_strdup (table->comments);
	else
		col[3] = g_strdup ("");
	/* where to put it ? */
	while ((i < GTK_CLIST (wid->clist)->rows) && !stop) {
		gtk_clist_get_text (GTK_CLIST (wid->clist), i, 0, &str);
		if (strcmp (str, table->name) > 0) {
			stop = TRUE;
		}
		else
			i++;
	}

	gtk_clist_insert (GTK_CLIST (wid->clist), i, col);
	for (j = 0; j < 4; j++)
		g_free (col[j]);

	rdata = (Row_Data *) g_malloc (sizeof (Row_Data));
	rdata->conf = wid->conf;
	rdata->table = table;
	rdata->query = NULL;
	rdata->env = NULL;
	rdata->edit_dlg = NULL;
	gtk_clist_set_row_data (GTK_CLIST (wid->clist), i, rdata);
	gtk_signal_connect (GTK_OBJECT (table), "comments_changed",
			    GTK_SIGNAL_FUNC (comments_table_changed_cb),
			    rdata);
}

static void
comments_table_changed_cb (DbTable * table, Row_Data * rdata)
{
	gint row;
	GtkWidget *clist;

	clist = MAIN_PAGE_TABLE (rdata->conf->tables_page)->clist;
	row = gtk_clist_find_row_from_data (GTK_CLIST (clist), rdata);
	if (row >= 0)
		gtk_clist_set_text (GTK_CLIST (clist), row, 3,
				    table->comments);
}



/* this CB is intended to be connected to the "table_dropped" signal of the 
   Database objects it represents.  So:
   - obj is a Database object
*/

static void
main_page_table_drop_cb (GtkObject * obj, DbTable * table,
			 MainPageTable * wid)
{
	gchar *str;
	gint i = 0;
	gboolean found = FALSE;
	Row_Data *rdata;

	while ((i < GTK_CLIST (wid->clist)->rows) && !found) {
		gtk_clist_get_text (GTK_CLIST (wid->clist), i, 0, &str);
		if (strcmp (str, table->name) == 0) {
			found = TRUE;
			rdata = (Row_Data *)
				gtk_clist_get_row_data (GTK_CLIST
							(wid->clist), i);
			/* the associated query is not valid anymore */
			drop_associated_query (rdata);
			g_free (rdata);
			gtk_clist_remove (GTK_CLIST (wid->clist), i);
			if (wid->sel_row == i) {
				wid->sel_row = -1;
				/* un sensitive buttons */
				gtk_widget_set_sensitive (wid->remove_table,
							  FALSE);
				gtk_widget_set_sensitive (wid->edit_table,
							  FALSE);
				gtk_widget_set_sensitive (wid->view_table,
							  FALSE);
			}
		}
		i++;
	}
}

static void
drop_associated_query (Row_Data * rdata)
{
	if (rdata->env) {
		gtk_object_unref (GTK_OBJECT (rdata->env));
		rdata->env = NULL;
	}
	if (rdata->query) {
		gtk_object_unref (GTK_OBJECT (rdata->query));
		rdata->query = NULL;
	}
}


static void
main_page_db_updated_cb (Database * db, MainPageTable * mpt)
{
	gint row;
	Row_Data *rd;
	for (row = 0; row < GTK_CLIST (mpt->clist)->rows; row++) {
		rd = (Row_Data *)
			gtk_clist_get_row_data (GTK_CLIST (mpt->clist), row);
		if (rd->table->is_view)
			gtk_clist_set_text (GTK_CLIST (mpt->clist), row, 1,
					    _("View"));
		else
			gtk_clist_set_text (GTK_CLIST (mpt->clist), row, 1,
					    _("Table"));
		if (rd->table->owner)
			gtk_clist_set_text (GTK_CLIST (mpt->clist), row, 2,
					    rd->table->owner);
		else
			gtk_clist_set_text (GTK_CLIST (mpt->clist), row, 2,
					    "");

		/* the associated query is not valid anymore */
		drop_associated_query (rd);
	}
}
