/* server-rs.h
 *
 * Copyright (C) 1999,2000,2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __SERVER_RESULTSET__
#define __SERVER_RESULTSET__

#include <gtk/gtksignal.h>
#include <gtk/gtkobject.h>
#include <GDA.h>
#include <gda-connection.h>
#include <gda-recordset.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define SERVER_RESULTSET(obj)          GTK_CHECK_CAST (obj, server_resultset_get_type(), ServerResultset)
#define SERVER_RESULTSET_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, server_resultset_get_type (), ServerResultsetClass)
#define IS_SERVER_RESULTSET(obj)       GTK_CHECK_TYPE (obj, server_resultset_get_type ())


	typedef struct _ServerResultset ServerResultset;
	typedef struct _ServerResultsetClass ServerResultsetClass;

	/* struct for the object's data */
	struct _ServerResultset
	{
		GtkObject object;

		GdaRecordset *recset;
		gint current_row;
		guint nbtuples;
		GString *query;	/* FIXME: is it really used? */
	};

	/* struct for the object's class */
	struct _ServerResultsetClass
	{
		GtkObjectClass parent_class;

		void (*dummy) (ServerResultset * qres);	/* unused */
	};

	/* generic widget's functions */
	guint server_resultset_get_type (void);
	GtkObject *server_resultset_new (GdaRecordset * res);
	void server_resultset_free (ServerResultset * qres);

	/* other functions for this object */
	gint server_resultset_get_nbtuples (ServerResultset * qres);
	gint server_resultset_get_nbcols (ServerResultset * qres);
	/* rows in [1..N] and cols in [0..N-1] */
	/* gchar* IS allocated! */
	gchar *server_resultset_get_item (ServerResultset * qres,
				       guint row, guint col);
	GdaField *server_resultset_get_gdafield (ServerResultset * qres,
					      guint row, guint col);

	/* convenience function, mem allocated or NULL */
	gchar *server_resultset_stringify (GdaField * field);
	gchar *server_resultset_get_col_name (ServerResultset * qres, gint col);
	void server_resultset_dump (ServerResultset * qres);


	/* creates a GSList with each node pointing to a string which appears
	   in the tabular.
	   the created list will have to be freed, as well as its strings contents.
	   WARNING: only one dimension tabulars allowed! */
	GSList *server_resultset_convert_tabular_to_list (gchar * tab);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
