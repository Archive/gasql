/* interface_cb.h
 *
 * Copyright (C) 1999,2000 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include "conf-manager.h"
#include "query.h"
#include "gnome-db-login.h"
#include "choicecombo.h"

/*
 * User Interface actions CBs
 */

/* CB to attach to menu/menubar for the user to open/close the connection */
void sql_conn_open_cb (GtkWidget * widget, ConfManager * conf);
void sql_conn_close_cb (GtkWidget * widget, ConfManager * conf);
/* CB to attach to connections preferences */
void options_config_cb (GtkWidget * widget, ConfManager * conf);
/* CB to view the global DB relations (user action) */
void sql_show_relations_cb (GtkObject * obj, ConfManager * conf);
/* CB to view DB system informations */
void sql_data_view_cb (GtkWidget * widget, ConfManager * conf);
/* CB to update the memory representation of the DB */
void sql_mem_update_cb (GtkWidget * widget, ConfManager * conf);
/* CBs for plugins */
void rescan_display_plugins_cb (GtkObject * obj, ConfManager * conf);
void config_display_plugins_cb (GtkObject * obj, ConfManager * conf);
/* CBs to configure the DBMS using bonobo controls */
void users_settings_cb (GtkObject * obj, ConfManager * conf);
void users_access_cb (GtkObject * obj, ConfManager * conf);
void users_groups_cb (GtkObject * obj, ConfManager * conf);
/* CB for the printer stuff */
void printer_setup_cb (GtkObject * obj, ConfManager * conf);
/* CBs to set the tab of the notebook to display the right page */
void show_tables_page_cb (GtkWidget * wid, ConfManager * conf);
void show_seqs_page_cb (GtkWidget * wid, ConfManager * conf);
void show_queries_page_cb (GtkWidget * shortcut, ConfManager * conf);
void show_forms_page_cb (GtkWidget * shortcut, ConfManager * conf);
/* embedding the gnome-db manager */
void run_gnomedb_manager_cb (GtkWidget * w, ConfManager * conf);
/* CBs from the FILE menu */
gint set_opened_file (ConfManager * conf, gchar * filetxt);
void file_new_cb (GtkWidget * widget, ConfManager * conf);
void file_open_cb (GtkWidget * widget, ConfManager * conf);
void file_close_cb (GtkWidget * widget, ConfManager * conf);
void file_save_cb (GtkWidget * widget, ConfManager * conf);
void file_save_as_cb (GtkWidget * widget, ConfManager * conf);
void quit_cb (GtkWidget * widget, ConfManager * conf);



/*
 * CBs to signals emitted by Database objects
 */

/* CB to attach to the ServerAccess signals */
void sql_server_conn_open_cb (GtkObject * wid, ConfManager * conf);
void sql_server_conn_to_close_cb (GtkObject * wid, ConfManager * conf);
void sql_server_conn_close_cb (GtkObject * wid, ConfManager * conf);
/* DB to link to the "table_dropped" and "seq_dropped" signals of Database 
   to update the relations */
void wid_db_rel_list_update (GtkObject * obj, gpointer item,
			     ConfManager * conf);
/* CB to link to the "error" signal of ServerAccess */
void sql_server_catch_errors_cb (GtkWidget * wid, GList * list,
				 ConfManager * conf);
