/* gladefeditor.h
 *
 * Copyright (C) 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GLADE_FORM_EDITOR__
#define __GLADE_FORM_EDITOR__

#include <gnome.h>
#include <glade/glade.h>
#include <config.h>
#include "gladedefs.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */ 
	
#define GLADE_FORM_EDITOR(obj)          GTK_CHECK_CAST (obj, glade_form_editor_get_type(), GladeFormEditor)
#define GLADE_FORM_EDITOR_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, glade_form_editor_get_type (), GladeFormEditorClass) 
#define IS_GLADE_FORM_EDITOR(obj)       GTK_CHECK_TYPE (obj, glade_form_editor_get_type ())
	
	
	typedef struct _GladeFormEditor        GladeFormEditor;
	typedef struct _GladeFormEditorClass   GladeFormEditorClass;

	/* struct for the object's data */
	struct _GladeFormEditor {
		GtkVBox object;
    
		GladeFormStruct *form_struct;
		GtkWidget *glade_widget;

		/* user interface widgets */
		GtkWidget *scrolled_window;
		GtkWidget *combo_sel_widget; /* combo box for selected widget */
		GtkWidget *combo_sel_entry;  /* combo box for selected form entry */
		GtkWidget *clist;            /* clist for pairs */
		GtkWidget *apply_button;

		/* widgets used for the selection of widgets in the form */
		GtkWidget *selected;
		GtkWidget *signal_selected;
		GSList    *form_widgets; /* form's selectable widgets */
	};

	/* struct for the object's class */
	struct _GladeFormEditorClass {
		GtkVBoxClass parent_class;

		void (*form_changed) (GladeFormEditor *gfe);
	};

	/* generic widget's functions */
	guint        glade_form_editor_get_type (void);
	GtkWidget*   glade_form_editor_new (void);

	/* setting the form to be displayed returns TRUE if everything OK*/
	gboolean     glade_form_editor_set_form (GladeFormEditor *gfe, 
						 gchar *mem_glade_doc, gchar *top_widget);

#ifdef __cplusplus
}
#endif /* __cplusplus */   

#endif
