/* canvas-query-view.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "canvas-query-view.h"
#include "canvas-field.h"

static void canvas_query_view_class_init (CanvasQueryViewClass * class);
static void canvas_query_view_init (CanvasQueryView * item);
static void canvas_query_view_destroy (GtkObject * object);

static void canvas_query_view_set_arg    (GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id);
static void canvas_query_view_get_arg    (GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id);


enum
{
	ARG_0,
	ARG_QUERY,
	ARG_QUERY_VIEW
};


guint
canvas_query_view_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"CanvasQueryView",
			sizeof (CanvasQueryView),
			sizeof (CanvasQueryViewClass),
			(GtkClassInitFunc) canvas_query_view_class_init,
			(GtkObjectInitFunc) canvas_query_view_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (canvas_base_get_type (), &f_info);
	}

	return f_type;
}

static void
canvas_query_view_class_init (CanvasQueryViewClass * class)
{
	GtkObjectClass *object_class = NULL;


	object_class = (GtkObjectClass *) class;

	object_class->destroy = canvas_query_view_destroy;

	/* Arguments */
	gtk_object_add_arg_type ("CanvasQueryView::query", GTK_TYPE_POINTER, GTK_ARG_READWRITE, ARG_QUERY);
	gtk_object_add_arg_type ("CanvasQueryView::query_view", GTK_TYPE_POINTER, GTK_ARG_READWRITE, ARG_QUERY_VIEW);
	object_class->set_arg = canvas_query_view_set_arg;
	object_class->get_arg = canvas_query_view_get_arg;
	
}


static void
canvas_query_view_init (CanvasQueryView * cqv)
{
	cqv->query = NULL;
	cqv->view = NULL;

	cqv->x_text_space = 3.;
	cqv->y_text_space = 3.;

	cqv->title_text_height = 0.;
	cqv->title_text_width = 0.;
	cqv->max_text_width = 0.;
	cqv->field_items = NULL;
	cqv->bg_frame = NULL;
	cqv->fields_text_height = 0.;
}


static void
canvas_query_view_destroy (GtkObject * object)
{
	GtkObjectClass *parent_class = NULL;

	parent_class = gtk_type_class (canvas_base_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CANVAS_QUERY_VIEW (object));

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void post_init (CanvasQueryView * cqv);
static void 
canvas_query_view_set_arg    (GtkObject            *object,
			      GtkArg               *arg,
			      guint                 arg_id)
{
	CanvasQueryView *cqv;
	gpointer ptr;

	cqv = CANVAS_QUERY_VIEW (object);

	switch (arg_id) {
	case ARG_QUERY:
		ptr = GTK_VALUE_POINTER (*arg);
		g_assert (IS_QUERY (ptr));
		cqv->query = QUERY (ptr);
		break;
	case ARG_QUERY_VIEW:
		ptr = GTK_VALUE_POINTER (*arg);
		if (ptr)
			cqv->view = QUERY_VIEW (ptr);
		break;
	}

	if (cqv->query && cqv->view)
		post_init (cqv);

}

static void 
canvas_query_view_get_arg    (GtkObject            *object,
			GtkArg               *arg,
			guint                 arg_id)
{
	g_print ("GetArg %d\n", arg_id);
}


static void redraw_view_contents (CanvasQueryView *cqv);
static void contents_changed_cb  (GtkObject *obj, GtkObject *obj2, CanvasQueryView *cqv);
static void 
post_init (CanvasQueryView * cqv)
{
	GnomeCanvasItem *item;
	gchar *str;

	/* Title of the Table or query */
	str = query_view_get_textual (cqv->view);
	item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cqv),
				     GNOME_TYPE_CANVAS_TEXT,
				     "font", CQV_DEFAULT_VIEW_FONT_BOLD,
				     "text", str,
				     "x", cqv->x_text_space, 
				     "y", cqv->y_text_space,
				     "fill_color", "black",
				     "justification", GTK_JUSTIFY_RIGHT, 
				     "anchor", GTK_ANCHOR_NORTH_WEST, 
				     NULL);
	g_free (str);

	/* Getting text metrics */
	cqv->title_text_height = item->y2 - item->y1;
	cqv->title_text_width = item->x2 - item->x1;
	cqv->max_text_width = item->x2 - item->x1;


	/* Drawing the contents */
	redraw_view_contents (cqv);

	/* Signals to keep the display up to date */
	gtk_signal_connect_while_alive (GTK_OBJECT (cqv->view->obj), "field_created",
					GTK_SIGNAL_FUNC (contents_changed_cb), cqv,
					GTK_OBJECT (cqv));
	gtk_signal_connect_while_alive (GTK_OBJECT (cqv->view->obj), "field_dropped",
					GTK_SIGNAL_FUNC (contents_changed_cb), cqv,
					GTK_OBJECT (cqv));
}

static void 
contents_changed_cb  (GtkObject *obj, GtkObject *obj2, CanvasQueryView *cqv)
{
	redraw_view_contents (cqv);
}

static void field_drag_action_cb (CanvasBase *cb, CanvasBase *drag_from, CanvasBase *drag_to, CanvasQueryView *cqv);
static gdouble compute_y (CanvasQueryView * cqv, gpointer entry);
static int button_item_event(GnomeCanvasItem *ci, GdkEvent *event, CanvasQueryView *cqv);
static void 
redraw_view_contents (CanvasQueryView *cqv)
{
	GSList *list;
	gboolean is_table;
	GnomeCanvasItem *item;
	gchar *str;
	gdouble x, sqsize, radius;

	/* Destroy any existing GnomeCanvasItem for the fields */
	list = cqv->field_items;
	while (list) {
                gtk_object_destroy (GTK_OBJECT (list->data));
                list = g_slist_next (list);
        }
	g_slist_free (cqv->field_items);
        cqv->field_items = NULL;

	/* Building new fields */
	is_table = IS_DB_TABLE (cqv->view->obj);
	if (is_table)
		list = DB_TABLE (cqv->view->obj)->fields;
	else
		list = QUERY (cqv->view->obj)->fields;
	
	sqsize = cqv->title_text_height * 0.6;
	while (list) {
		if (IS_DB_FIELD (list->data) || 
		    (IS_QUERY_FIELD (list->data) && QUERY_FIELD (list->data)->is_printed)) {
			item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cqv),
						     canvas_field_get_type(),
						     "x", cqv->x_text_space, 
						     "y", compute_y (cqv, list->data),
						     "canvas_query_view", cqv,
						     "field", list->data,
						     NULL);
			
			gtk_object_set_data (GTK_OBJECT (item), "fieldptr", list->data);
			cqv->field_items = g_slist_append (cqv->field_items, item);
			
			gtk_signal_connect (GTK_OBJECT (item), "drag_action",
					    GTK_SIGNAL_FUNC (field_drag_action_cb), cqv);
		}

		list = g_slist_next (list);
	}

	/* "button" to close the QueryView and top little frame */
	sqsize = cqv->title_text_height * 0.8;
	x = cqv->max_text_width + cqv->x_text_space;
	if (x - sqsize - 2*cqv->x_text_space < cqv->title_text_width)
		x = cqv->title_text_width + sqsize + 2*cqv->x_text_space;

	item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cqv),
				     GNOME_TYPE_CANVAS_RECT,
				     "x1", x - sqsize,
				     "y1", cqv->y_text_space,
				     "x2", x,
				     "y2", cqv->y_text_space + sqsize,
				     "fill_color", "white",
				     "outline_color", "black",
				     "width_units", 1.0,
				     NULL);
	gnome_canvas_item_raise_to_top (item);
	gtk_signal_connect(GTK_OBJECT(item),"event",
			   GTK_SIGNAL_FUNC(button_item_event), cqv);

	if (x - cqv->x_text_space > cqv->max_text_width)
		cqv->max_text_width = x - cqv->x_text_space;

	str = "";
	if (is_table) {
		if (DB_TABLE (cqv->view->obj)->is_view)
			str = CQV_VIEW_COLOR;
		else
			str = CQV_TABLE_COLOR;
	}
	else 
		str = CQV_QUERY_COLOR;

	radius = sqsize * .2;
	item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (cqv),
				     GNOME_TYPE_CANVAS_ELLIPSE,
				     "x1", x - sqsize/2. - radius,
				     "y1", cqv->y_text_space + sqsize/2. - radius,
				     "x2", x - sqsize/2. + radius,
				     "y2", cqv->y_text_space + sqsize/2. + radius,
				     "fill_color", str,
				     "outline_color", "black",
				     "width_units", 1.0,
				     NULL);
	gnome_canvas_item_raise_to_top (item);
	gtk_signal_connect(GTK_OBJECT(item),"event",
			   GTK_SIGNAL_FUNC(button_item_event), cqv);

	/* Top little frame */
	item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqv),
				      GNOME_TYPE_CANVAS_RECT,
				      "x1", (double) 0,
				      "y1", (double) 0,
				      "x2", cqv->max_text_width + 2. * cqv->x_text_space,
				      "y2", cqv->title_text_height + 2 * cqv->y_text_space,
				      "outline_color", "black",
				      "fill_color", str, 
				      "width_units", 1.0, 
				      NULL);
	gnome_canvas_item_lower_to_bottom (item);

	/* Outline frame */
	if (cqv->bg_frame)
                gtk_object_destroy (GTK_OBJECT (cqv->bg_frame));

	item = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cqv),
				      GNOME_TYPE_CANVAS_RECT,
				      "x1", (double) 0,
				      "y1", (double) 0,
				      "x2", cqv->max_text_width + 2. * cqv->x_text_space,
				      "y2", compute_y (cqv, NULL),
				      "outline_color", "black",
				      "fill_color", "white",
				      "width_units", 1.0, NULL);
	cqv->bg_frame = item;
	gnome_canvas_item_lower_to_bottom (item);
}

static void 
field_drag_action_cb (CanvasBase *cb, CanvasBase *drag_from, CanvasBase *drag_to, CanvasQueryView *cqv)
{
#ifdef debug_signal
	g_print (">> 'DRAG_ACTION' from %s::field_drag_action_cb()\n", __FILE__);
#endif
	gtk_signal_emit_by_name (GTK_OBJECT (cqv), "drag_action", drag_from, drag_to);
#ifdef debug_signal
	g_print ("<< 'DRAG_ACTION' from %s::field_drag_action_cb()\n", __FILE__);
#endif
}

/* if entry is NULL, then it gives the "would be" position of the after
   last field in the Query or DbTable */
static gdouble
compute_y (CanvasQueryView * cqv, gpointer entry)
{
	double calc;
	gint pos;
	GSList *list;
	gboolean list_to_free = FALSE;

	if (IS_DB_TABLE (cqv->view->obj))
		list = DB_TABLE (cqv->view->obj)->fields;
	else {
		GSList *iter;
		
		list = NULL;
		iter = QUERY (cqv->view->obj)->fields;
		while (iter) {
			if (QUERY_FIELD (iter->data)->is_printed)
				list = g_slist_append (list, iter->data);
			iter = g_slist_next (iter);
		}

		list_to_free = TRUE;
	}

	if (entry && g_slist_find (list, entry))
		pos = g_slist_index (list, entry);
	else
		pos = g_slist_length (list);

	if (list_to_free)
		g_slist_free (list);

	calc = (3 * (cqv->y_text_space) + cqv->title_text_height) +
                pos * (cqv->y_text_space + cqv->fields_text_height);

        return calc;
}


static void create_alias_cb(GtkWidget * button, CanvasQueryView *cqv);
static void delete_view_cb(GtkWidget * button, CanvasQueryView *cqv);
static int 
button_item_event(GnomeCanvasItem *ci, GdkEvent *event, CanvasQueryView *cqv)
{
	gboolean done = TRUE;
	GtkWidget *menu, *entry;

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		menu = gtk_menu_new ();
		entry = gtk_menu_item_new_with_label (_("Create alias"));
		gtk_signal_connect (GTK_OBJECT (entry), "activate",
				    GTK_SIGNAL_FUNC (create_alias_cb), cqv);
		gtk_menu_append (GTK_MENU (menu), entry);
		gtk_widget_show (entry);
		entry = gtk_menu_item_new_with_label (_("Delete"));
		gtk_signal_connect (GTK_OBJECT (entry), "activate",
				    GTK_SIGNAL_FUNC (delete_view_cb), cqv);
		gtk_menu_append (GTK_MENU (menu), entry);
		gtk_widget_show (entry);
		gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
				NULL, NULL, ((GdkEventButton *)event)->button,
				((GdkEventButton *)event)->time);
		break;
	default:
		done = FALSE;
		break;
	}

	return done;	
}

static void 
create_alias_cb(GtkWidget * button, CanvasQueryView *cqv)
{
	query_add_view_with_obj (cqv->query, cqv->view->obj);
}

static void 
delete_view_cb(GtkWidget * button, CanvasQueryView *cqv)
{
	query_del_view (cqv->query, cqv->view);
}


GnomeCanvasItem *
canvas_query_view_find_field (CanvasQueryView *cqv, GtkObject *field)
{
	GnomeCanvasItem *found = NULL;
	GSList *list;

	g_return_val_if_fail (IS_CANVAS_QUERY_VIEW (cqv), NULL);
	list = cqv->field_items;
	while (list && !found) {
		if (CANVAS_FIELD (list->data)->field == field)
			found = GNOME_CANVAS_ITEM (list->data);
		list = g_slist_next (list);
	}

	return found;
}
