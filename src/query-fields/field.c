/* field.c
 *
 * Copyright (C) 2001 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


/*
 * This module represents QueryField of type QUERY_FIELD_FIELD:
 * a field of a QueryView (DbTable (table or view) or Query).
 */

#include <config.h>
#include "../query.h"
#include "../database.h"
#include "../sqlwiddbtree.h"
#include "../query-field-private.h"

static void        q_init            (QueryField *qf);
static void        q_destroy         (QueryField *qf);
static void        q_deactivate      (QueryField *qf);
static void        q_activate        (QueryField *qf);
static GtkWidget * q_get_edit_widget (QueryField *qf);
static GtkWidget * q_get_sel_widget  (QueryField *qf, GtkSignalFunc func, gpointer data);
static gchar     * q_render_as_sql   (QueryField *qf, GSList * missing_values);
static xmlNodePtr  q_render_as_xml   (QueryField *qf, GSList * missing_values);
static gchar     * q_render_as_string(QueryField *qf, GSList * missing_values);
static void        q_save_to_xml     (QueryField *qf, xmlNodePtr node);
static void        q_load_from_xml   (QueryField *qf, xmlNodePtr node);
static void        q_copy_other_field(QueryField *qf, QueryField *other);
static gboolean    q_is_equal_to     (QueryField *qf, QueryField *other);
static GSList    * q_get_monitored_objects (QueryField *qf);
static void        q_replace_comp    (QueryField *qf, gint ref, GtkObject *old, GtkObject *new);

/* callbacks from the refering object */
static void        q_obj_destroyed_cb (GtkObject *obj, QueryField *qf);

typedef struct {
	/* The view and field attributs should either be both NULL or both non NULL */
	QueryView   *view;
	GtkObject   *field; /* can be either DbField if view is DbTable or QueryField if view is Query */
	/* XML field name if we did not find it in the first place (like TVxxx:FIyyy) */
	gchar       *field_name;
	/* "destroy" signal from the QueryView */
	guint        view_signal_id;
	/* "destroy" signal from the field */
	guint        field_signal_id;
} private_data;

#define QF_PRIVATE_DATA(qf) ((private_data *) qf->private_data)

QueryFieldIface * 
query_field_field_get_iface()
{
	QueryFieldIface *iface;

	iface = g_new0 (QueryFieldIface, 1);
	iface->field_type = QUERY_FIELD_FIELD;
	iface->name = "field";
	iface->pretty_name = _("Field of a table or sub-query");
	iface->init = q_init;
	iface->destroy = q_destroy;
	iface->deactivate = q_deactivate;
	iface->activate = q_activate;
	iface->get_edit_widget = q_get_edit_widget;
	iface->get_sel_widget = q_get_sel_widget;
	iface->render_as_sql = q_render_as_sql;
	iface->render_as_xml = q_render_as_xml;
	iface->render_as_string = q_render_as_string;
	iface->save_to_xml = q_save_to_xml;
	iface->load_from_xml = q_load_from_xml;
	iface->copy_other_field = q_copy_other_field;
	iface->is_equal_to = q_is_equal_to;
	iface->get_monitored_objects = q_get_monitored_objects;
	iface->replace_comp = q_replace_comp;

	return iface;
}

static void        
q_init            (QueryField *qf)
{
	private_data *data;
	data = g_new0 (private_data, 1);
	qf->private_data = (gpointer) data;
	QF_PRIVATE_DATA(qf)->view = NULL;
	QF_PRIVATE_DATA(qf)->field = NULL;
	QF_PRIVATE_DATA(qf)->field_name = NULL;
	QF_PRIVATE_DATA(qf)->view_signal_id = 0;
	QF_PRIVATE_DATA(qf)->field_signal_id = 0;
}

static void        
q_destroy         (QueryField *qf)
{
	query_field_deactivate (qf);
	if (qf->private_data) {
		if (QF_PRIVATE_DATA(qf)->field_name)
			g_free (QF_PRIVATE_DATA(qf)->field_name);
		g_free (qf->private_data);
		qf->private_data = NULL;
	}
}


static gchar * qf_get_xml_id (QueryField *qf);
static void
q_deactivate      (QueryField *qf)
{
	if (! qf->activated)
		return;

	/* This function disconnects any event handler from any object
	   this QueryField wants to receive events from.
	   Here we disconnect from the view if we are connected */
		
	gtk_signal_disconnect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->view), 
			       QF_PRIVATE_DATA(qf)->view_signal_id);
	gtk_signal_disconnect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->field), 
			       QF_PRIVATE_DATA(qf)->field_signal_id);
	if (QF_PRIVATE_DATA(qf)->field_name) {
		g_free (QF_PRIVATE_DATA(qf)->field_name);
		QF_PRIVATE_DATA(qf)->field_name = NULL;
	}

	QF_PRIVATE_DATA(qf)->field_name = qf_get_xml_id (qf);
	g_print ("q_deactivate/QF_PRIVATE_DATA(qf)->field_name=%s\n", QF_PRIVATE_DATA(qf)->field_name);
	QF_PRIVATE_DATA(qf)->view = NULL;
	QF_PRIVATE_DATA(qf)->field = NULL;
	
	QF_PRIVATE_DATA(qf)->view_signal_id = 0;
	QF_PRIVATE_DATA(qf)->field_signal_id = 0;
	query_field_set_activated (qf, FALSE);
}

static gchar *
qf_get_xml_id (QueryField *qf)
{
	gchar *str = NULL;

	if (QF_PRIVATE_DATA(qf)->field) {
		if (IS_QUERY_FIELD (QF_PRIVATE_DATA(qf)->field))
			str = g_strdup_printf ("%s/%s", 
					       query_view_get_xml_id (QF_PRIVATE_DATA(qf)->view),
					       query_field_get_xml_id (QUERY_FIELD (QF_PRIVATE_DATA(qf)->field)));
		else {
			DbTable *table;

			table = database_find_table_from_field (qf->query->conf->db, 
								DB_FIELD (QF_PRIVATE_DATA(qf)->field));
			if (table) /* table can be NULL if we are destroying it or the DbField */
				str = g_strdup_printf ("%s/%s", 
						       query_view_get_xml_id (QF_PRIVATE_DATA(qf)->view),
						       db_field_get_xml_id (DB_FIELD (QF_PRIVATE_DATA(qf)->field),
									    qf->query->conf->db));
		}
	}

	return str;
}

static void
q_activate        (QueryField *qf)
{
	/* this function gets references to any object this QueryField wants to 
	   receive events from. */

	GtkObject *field;
	QueryView *view;
	
	if (qf->activated)
		return;

	field = QF_PRIVATE_DATA(qf)->field;
	view = QF_PRIVATE_DATA(qf)->view;
	
	if (!field && QF_PRIVATE_DATA(qf)->field_name) {
		gchar *str, *ptr;

		str = g_strdup (QF_PRIVATE_DATA(qf)->field_name);
		ptr = strtok (str, "/");
		ptr = strtok (NULL, "/");

		view = query_view_find_from_xml_name (qf->query->conf, qf->query, str);
		if (view) {
			DbField *dfield = NULL;
			QueryField *qfield = NULL;

			dfield = database_find_field_from_xml_name (qf->query->conf->db, ptr);
			if (!dfield)
				qfield = query_get_field_by_xmlid (qf->query, ptr);
										       
			if ((IS_QUERY (view->obj) && qfield) ||
			    (IS_DB_TABLE (view->obj) && dfield)) {
				g_free (QF_PRIVATE_DATA(qf)->field_name);
				QF_PRIVATE_DATA(qf)->field_name = NULL;	
				field = (qfield != NULL) ? GTK_OBJECT (qfield) : GTK_OBJECT (dfield);
			}
		}
		g_free (str);
	}


	if (field && view) {
		QF_PRIVATE_DATA(qf)->view = view;
		QF_PRIVATE_DATA(qf)->field = field;

		QF_PRIVATE_DATA(qf)->view_signal_id = 
			gtk_signal_connect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->view), "destroy",
					    GTK_SIGNAL_FUNC (q_obj_destroyed_cb), qf);
		QF_PRIVATE_DATA(qf)->field_signal_id = 
			gtk_signal_connect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->field), "destroy",
					    GTK_SIGNAL_FUNC (q_obj_destroyed_cb), qf);
		/* now it's activated */ 
		query_field_set_activated (qf, TRUE);
	}
}

static void        
q_obj_destroyed_cb (GtkObject *obj, QueryField *qf)
{
	/* if the field or table disappear, then destroy is the result */
	gtk_object_destroy (GTK_OBJECT (qf));
}


static void widget_field_sel_cb (GtkWidget * widget, DbTable * t, DbField * f, QueryField *qf);
static GtkWidget * 
q_get_edit_widget (QueryField *qf)
{
	GtkWidget *wid, *frame, *vb;

	frame = gtk_frame_new (_("Field of a table or a sub-query"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD/2.);

	vb = gtk_vbox_new (FALSE, GNOME_PAD/2.);
	gtk_container_add (GTK_CONTAINER (frame), vb);
	gtk_container_set_border_width (GTK_CONTAINER (vb), GNOME_PAD/2.);

	wid = sql_wid_db_tree_new (qf->query->conf);
	gtk_widget_set_usize (GTK_WIDGET (wid), 200, 100);
	sql_wid_db_tree_set_mode (SQL_WID_DB_TREE (wid),
				  SQL_WID_DB_TREE_TABLE_FIELDS |
				  SQL_WID_DB_TREE_TABLE_FIELDS_SEL |
				  SQL_WID_DB_TREE_QUERY_FIELDS |
				  SQL_WID_DB_TREE_QUERY_FIELDS_SEL);
	gtk_signal_connect_while_alive (GTK_OBJECT (wid), "field_selected",
					GTK_SIGNAL_FUNC (widget_field_sel_cb), qf,
					GTK_OBJECT (qf));
	gtk_box_pack_start (GTK_BOX (vb), wid, TRUE, TRUE, GNOME_PAD/2.);
	gtk_widget_show_all (vb);

	if (QF_PRIVATE_DATA(qf)->field) {
		/* select the field in the widget */
		/* FIXME: TODO */
	}

	return frame;
}

static GtkWidget * 
q_get_sel_widget  (QueryField *qf, GtkSignalFunc func, gpointer data)
{
	GtkWidget *button;
	gchar *str;

	if (qf->activated) {
		gchar *str1, *str2;
		str1 = query_view_get_name (QF_PRIVATE_DATA(qf)->view);
		str2 = query_view_get_field_name (QF_PRIVATE_DATA(qf)->view, QF_PRIVATE_DATA(qf)->field);
		str = g_strdup_printf ("%s.%s", str1, str2);
		g_free (str2);
	}
	else
		str = g_strdup (_("Table or sub-query's FIELD"));

	button = gtk_button_new_with_label (str);	
	g_free (str);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    func, data);
	gtk_object_set_data (GTK_OBJECT (button), "qf", qf);

	/* Set the "QF_obj_emit_sig" attribute so that we can attach attributes to that button
	   which will be transmitted when the user clicks on it */
	gtk_object_set_data (GTK_OBJECT (button), "QF_obj_emit_sig", button);

	return button;
}

/* we consider the selected field is the the one being
   represented in the QueryField */
static void
widget_field_sel_cb (GtkWidget * widget,
		     DbTable * t, DbField * f, QueryField *qf)
{

	/* test if anything has changed */
	if (QF_PRIVATE_DATA(qf)->field == GTK_OBJECT (f)) 
		return;

	query_field_deactivate (qf);
	query_field_field_set_field (qf, NULL, f);
	query_field_activate (qf);

#ifdef debug_signal
	g_print (">> 'FIELD_MODIFIED' from widget_field_sel_cb\n");
#endif
	gtk_signal_emit_by_name (GTK_OBJECT (qf), "field_modified");
#ifdef debug_signal
	g_print ("<< 'FIELD_MODIFIED' from widget_field_sel_cb\n");
#endif	
}


static gchar * 
q_render_as_sql   (QueryField *qf, GSList * missing_values)
{
	gchar *str = NULL;

	if (qf->activated) {
		gchar *str1, *str2;
		str1 = query_view_get_name (QF_PRIVATE_DATA(qf)->view);
		/* FIXME: find a more clean rule for the QueryField of the sub-query */
		str2 = query_view_get_field_name (QF_PRIVATE_DATA(qf)->view, QF_PRIVATE_DATA(qf)->field);
		str = g_strdup_printf ("%s.%s", str1, str2);
		g_free (str2);
	}

	return str;
}

static xmlNodePtr  
q_render_as_xml   (QueryField *qf, GSList * missing_values)
{
	return NULL;
}

static gchar * 
q_render_as_string(QueryField *qf, GSList * missing_values)
{
	gchar *str = NULL;
	
	str = q_render_as_sql (qf, missing_values);
	if (!str) 
		str = g_strdup ("A table's field");

	return str;
}

static void  
q_save_to_xml     (QueryField *qf, xmlNodePtr node)
{
	if (qf->activated) {
		gchar *str;
	
		g_print("Saving field to node %p...", node);	

		/* node object ref */
		str = query_view_get_xml_id (QF_PRIVATE_DATA(qf)->view);
		xmlSetProp (node, "object", str);
		g_free (str);

		if (IS_QUERY_FIELD (QF_PRIVATE_DATA(qf)->field))
			str = query_field_get_xml_id (QUERY_FIELD (QF_PRIVATE_DATA(qf)->field));
		else
			str = db_field_get_xml_id (DB_FIELD (QF_PRIVATE_DATA(qf)->field),
						   qf->query->conf->db);
		xmlSetProp (node, "object_ext", str);
		g_free (str);
		
		xmlSetProp (node, "type", "field");
		g_print("done\n");
	}
	else
		g_warning ("QueryField not activated; can't save\n");
}

static void        
q_load_from_xml   (QueryField *qf, xmlNodePtr node)
{
	query_field_deactivate (qf);
	
	/* check we have a QueryField */
	if (!strcmp (node->name, "QueryField")) {
		gchar *str, *str2;

		str = xmlGetProp (node, "type");
		if (!str || (str && strcmp (str, "field"))) {
			if (str) g_free (str);
			return;
		}

		str = xmlGetProp (node, "object");
		str2 = xmlGetProp (node, "object_ext");
		if (str2) {
			if (QF_PRIVATE_DATA(qf)->field_name)
				g_free (QF_PRIVATE_DATA(qf)->field_name);
			QF_PRIVATE_DATA(qf)->field_name = g_strdup_printf ("%s/%s", str, str2);;
			g_free (str2);
		}
		g_free (str);
		query_field_activate (qf);
	}
}

static void        
q_copy_other_field(QueryField *qf, QueryField *other)
{
	/* we can't call q_destroy(qf) because we don't know what the type
	   of QueryField it was before. This is normally done by the
	   QueryField object before the copy */

	if (QF_PRIVATE_DATA(other)->field) {
		QF_PRIVATE_DATA(qf)->view = QF_PRIVATE_DATA(other)->view;
		QF_PRIVATE_DATA(qf)->field = QF_PRIVATE_DATA(other)->field;
		query_field_activate (qf);
	}
	else {
		if (QF_PRIVATE_DATA(other)->field_name) {
			QF_PRIVATE_DATA(qf)->field_name = g_strdup (QF_PRIVATE_DATA(other)->field_name);
			query_field_activate (qf);
		}
	}
}

static gboolean
q_is_equal_to (QueryField *qf, QueryField *other)
{
	gboolean retval = FALSE;

	if (qf->activated && other->activated) {
		if ((QF_PRIVATE_DATA(qf)->view == QF_PRIVATE_DATA(other)->view) &&
		    (QF_PRIVATE_DATA(qf)->field == QF_PRIVATE_DATA(other)->field))
			retval = TRUE;
	}

	return retval;
}


static GSList *
q_get_monitored_objects (QueryField *qf)
{
	GSList *list = NULL;

	if (qf->activated) {
		list = g_slist_prepend (NULL, QF_PRIVATE_DATA(qf)->field);
		list = g_slist_prepend (list, QF_PRIVATE_DATA(qf)->view);
	}

	return list;
}

static void
q_replace_comp (QueryField *qf, gint ref, GtkObject *old, GtkObject *new)
{
	/* References can be to QueryViews can change. The obj attribute of the
	 new and the old QueryViews can either:
	 1) be the same and the the field WILL NOT change
	 2) be different and in this case things get complicated...
	*/
	if ((GTK_OBJECT (QF_PRIVATE_DATA(qf)->view) == old) && 
	    (IS_QUERY_VIEW (old) && IS_QUERY_VIEW (new))) {
		if (QUERY_VIEW (old)->obj == QUERY_VIEW (new)->obj) { /* case 1 */
			/* we can keep it activated because the field does not change */
			gtk_signal_disconnect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->view), 
					       QF_PRIVATE_DATA(qf)->view_signal_id);
			QF_PRIVATE_DATA(qf)->view = QUERY_VIEW (new);
			QF_PRIVATE_DATA(qf)->view_signal_id = 
				gtk_signal_connect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->view), "destroy",
						    GTK_SIGNAL_FUNC (q_obj_destroyed_cb), qf);
		}
		else { /* case 2 */
			gboolean done = FALSE;
			if (qf->activated) {
				gint i;
				GSList *list;
				GtkObject *field;
				
				if (IS_DB_TABLE (QUERY_VIEW (old)->obj))
					list = DB_TABLE (QUERY_VIEW (old)->obj)->fields;
				else
					list = QUERY (QUERY_VIEW (old)->obj)->fields;

				i = g_slist_index (list, QF_PRIVATE_DATA(qf)->field);

				if (IS_DB_TABLE (QUERY_VIEW (new)->obj))
					list = DB_TABLE (QUERY_VIEW (new)->obj)->fields;
				else
					list = QUERY (QUERY_VIEW (new)->obj)->fields;
				field = g_slist_nth_data (list, i);

				query_field_deactivate (qf);
				if (field && 
				    ((IS_QUERY_FIELD (field) && IS_QUERY_FIELD (QF_PRIVATE_DATA(qf)->field)) &&
				     (IS_DB_FIELD (field) && IS_DB_FIELD (QF_PRIVATE_DATA(qf)->field)))) {
					gchar *str1, *str2, *str3;
					str1 = query_view_get_xml_id (QUERY_VIEW (new));
				
					if (IS_QUERY_FIELD (field))
						str2 = query_field_get_xml_id (QUERY_FIELD (field));
					else
						str2 = db_field_get_xml_id (DB_FIELD (field), qf->query->conf->db);
					str3 = g_strdup_printf ("%s/%s", str1, str2);
					g_free (str1);
					g_free (str2);
					g_print ("===> %s\n", str3);
					if (QF_PRIVATE_DATA(qf)->field_name)
						g_free (QF_PRIVATE_DATA(qf)->field_name);
					QF_PRIVATE_DATA(qf)->field_name = str3;

					query_field_activate (qf);
					done = TRUE;
				}
			}

			if (!done) 
				g_warning ("Cound not replace %p by %p in Field QueryField %p (named %s/%s)\n",
					   old, new, qf, qf->name, qf->alias);
		}
	}
	return;
}


/* 
 * 
 * QueryField object's different implementations
 * 
 *
 */

ServerDataType  *
query_field_field_get_data_type (QueryField *qf)
{
	g_assert (qf);
	g_assert (IS_QUERY_FIELD (qf));
	g_assert (qf->field_type == QUERY_FIELD_FIELD);

	if (! qf->activated)
		return NULL;
	else {
		if (IS_DB_FIELD (QF_PRIVATE_DATA(qf)->field))
			return DB_FIELD (QF_PRIVATE_DATA(qf)->field)->type;
		else
			return NULL; /* FIXME */
	}
}

void
query_field_field_set_field (QueryField *qf, QueryView *qv, DbField *field)
{
	gboolean activated = FALSE;
	gboolean done = FALSE;
	
	g_assert (qf);
	g_assert (IS_QUERY_FIELD (qf));
	g_assert (qf->field_type == QUERY_FIELD_FIELD);

	if (qf->activated) {
		activated = TRUE;
		query_field_deactivate (qf);
	}

	/* Test to find a QueryView for that field, otherwise do nothing */
	if (field) {
		if (qv && IS_DB_TABLE (qv->obj) && g_slist_find (DB_TABLE (qv->obj)->fields, field)) {
			QF_PRIVATE_DATA(qf)->view = qv;
			QF_PRIVATE_DATA(qf)->field = GTK_OBJECT (field);
			done = TRUE;
		}
		if (!done) {
			/* ignoring qv now, try to find a good QueryView */
			QueryView *qv2 = NULL;
			GSList *list;

			list = qf->query->views;
			while (list && !qv2) {
				if (IS_DB_TABLE (QUERY_VIEW (list->data)->obj) &&
				    g_slist_find (DB_TABLE (QUERY_VIEW (list->data)->obj)->fields, field))
					qv2 = QUERY_VIEW (list->data);
				list = g_slist_next (list);
			}
			if (qv2) {
				QF_PRIVATE_DATA(qf)->view = qv2;
				QF_PRIVATE_DATA(qf)->field = GTK_OBJECT (field);
				done = TRUE;
			}
		}
	}

	if (done) {
		if (activated) 
			query_field_activate (qf);
	}
}
