/* fquery.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


/*
 * This module represents QueryField of type QUERY_FIELD_FIELD:
 * a field of a Query
 */

#include <config.h>
#include "../query.h"
#include "../query-field-private.h"
#include "../database.h"
#include "../sqlwiddbtree.h"

static void        q_init            (QueryField *qf);
static void        q_destroy         (QueryField *qf);
static void        q_deactivate      (QueryField *qf);
static void        q_activate        (QueryField *qf);
static GtkWidget * q_get_edit_widget (QueryField *qf);
static GtkWidget * q_get_sel_widget  (QueryField *qf, GtkSignalFunc func, gpointer data);
static gchar     * q_render_as_sql   (QueryField *qf, GSList * missing_values);
static xmlNodePtr  q_render_as_xml   (QueryField *qf, GSList * missing_values);
static gchar     * q_render_as_string(QueryField *qf, GSList * missing_values);
static void        q_save_to_xml     (QueryField *qf, xmlNodePtr node);
static void        q_load_from_xml   (QueryField *qf, xmlNodePtr node);
static void        q_copy_other_field(QueryField *qf, QueryField *other);
static gboolean    q_is_equal_to     (QueryField *qf, QueryField *other);
static GSList    * q_get_monitored_objects (QueryField *qf);
static void        q_replace_comp    (QueryField *qf, gint ref, GtkObject *old, GtkObject *new);

/* callbacks from the refering object */
static void        q_obj_destroyed_cb (GtkObject *obj, QueryField *qf);

/* FIXME: insert the other query's field as well */
typedef struct {
	Query       *query;
	/* XML field name if we did not find it in the first place (like TVxxx:FIyyy) */
	gchar       *query_name;
	/* "destroy" signal from the Query */
	guint        signal_id;
} private_data;

#define QF_PRIVATE_DATA(qf) ((private_data *) qf->private_data)

QueryFieldIface * 
query_field_query_get_iface()
{
	QueryFieldIface *iface;

	iface = g_new0 (QueryFieldIface, 1);
	iface->field_type = QUERY_FIELD_QUERY_FIELD;
	iface->name = "query";
	iface->pretty_name = _("Other query's field");
	iface->init = q_init;
	iface->destroy = q_destroy;
	iface->deactivate = q_deactivate;
	iface->activate = q_activate;
	iface->get_edit_widget = q_get_edit_widget;
	iface->get_sel_widget = q_get_sel_widget;
	iface->render_as_sql = q_render_as_sql;
	iface->render_as_xml = q_render_as_xml;
	iface->render_as_string = q_render_as_string;
	iface->save_to_xml = q_save_to_xml;
	iface->load_from_xml = q_load_from_xml;
	iface->copy_other_field = q_copy_other_field;
	iface->is_equal_to = q_is_equal_to;
	iface->get_monitored_objects = q_get_monitored_objects;
	iface->replace_comp = q_replace_comp;

	return iface;
}

static void        
q_init            (QueryField *qf)
{
	private_data *data;
	data = g_new0 (private_data, 1);
	qf->private_data = (gpointer) data;
	QF_PRIVATE_DATA(qf)->query = NULL;
	QF_PRIVATE_DATA(qf)->query_name = NULL;
	QF_PRIVATE_DATA(qf)->signal_id = 0;
}

static void        
q_destroy         (QueryField *qf)
{
	query_field_deactivate (qf);
	if (qf->private_data) {
		g_free (qf->private_data);
		qf->private_data = NULL;
	}
}

static void
q_deactivate      (QueryField *qf)
{
	if (! qf->activated)
		return;

	/* This function disconnects any event handler from any object
	   this QueryField wants to receive events from.
	   Here we disconnect from the table if we are connected */
		
	gtk_signal_disconnect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->query), 
			       QF_PRIVATE_DATA(qf)->signal_id);

	if (QF_PRIVATE_DATA(qf)->query_name) {
		g_free (QF_PRIVATE_DATA(qf)->query_name);
		QF_PRIVATE_DATA(qf)->query_name = NULL;
	}

	QF_PRIVATE_DATA(qf)->query_name = query_get_xml_id (QF_PRIVATE_DATA(qf)->query),
	QF_PRIVATE_DATA(qf)->query = NULL;
	
	QF_PRIVATE_DATA(qf)->signal_id = 0;
	query_field_set_activated (qf, FALSE);
}

static void
q_activate        (QueryField *qf)
{
	/* this function gets references to any object this QueryField wants to 
	   receive events from. */

	Query *query;
	
	if (qf->activated)
		return;

	query = QF_PRIVATE_DATA(qf)->query;
	
	if (!query && QF_PRIVATE_DATA(qf)->query_name) {
		query = query_find_from_xml_name (qf->query->conf, NULL, 
						  QF_PRIVATE_DATA(qf)->query_name);
		if (query) {
			g_free (QF_PRIVATE_DATA(qf)->query_name);
			QF_PRIVATE_DATA(qf)->query_name = NULL;	
		}
	}


	if (query) {
		QF_PRIVATE_DATA(qf)->query = query;

		QF_PRIVATE_DATA(qf)->signal_id = 
			gtk_signal_connect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->query), "destroy",
					    GTK_SIGNAL_FUNC (q_obj_destroyed_cb), qf);
		query_field_set_activated (qf, TRUE);
	}
}

static void        
q_obj_destroyed_cb (GtkObject *obj, QueryField *qf)
{
	/* if the query disappear, then destroy is the result */
	gtk_object_destroy (GTK_OBJECT (qf));
}




static GtkWidget * 
q_get_edit_widget (QueryField *qf)
{
	GtkWidget *wid=NULL;

	/* TODO */

	return wid;
}

static GtkWidget * 
q_get_sel_widget  (QueryField *qf, GtkSignalFunc func, gpointer data)
{
	GtkWidget *button;
	gchar *str;

	if (qf->activated) 
		str = g_strdup_printf (_("Query %s"), QF_PRIVATE_DATA(qf)->query->name);
	else
		str = g_strdup (_("Query's FIELD"));

	button = gtk_button_new_with_label (str);	
	g_free (str);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    func, data);
	gtk_object_set_data (GTK_OBJECT (button), "qf", qf);

	/* Set the "QF_obj_emit_sig" attribute so that we can attach attributes to that button
	   which will be transmitted when the user clicks on it */
	gtk_object_set_data (GTK_OBJECT (button), "QF_obj_emit_sig", button);

	return button;
}

static gchar     * 
q_render_as_sql   (QueryField *qf, GSList * missing_values)
{
	gchar *str = NULL;

	/* TODO */

	return str;
}

static xmlNodePtr  
q_render_as_xml   (QueryField *qf, GSList * missing_values)
{
	return NULL;
}

static gchar * 
q_render_as_string(QueryField *qf, GSList * missing_values)
{
	gchar *str = NULL;

	if (qf->activated) {
		str = g_strdup_printf (_("(Query %s)"), QF_PRIVATE_DATA(qf)->query->name);
	}
			       
	return str;
}

static void  
q_save_to_xml     (QueryField *qf, xmlNodePtr node)
{
	if (qf->activated) {
		gchar *str;
	
		g_print("Saving field to node %p...", node);	

		/* node object ref */
		str = query_get_xml_id (QF_PRIVATE_DATA(qf)->query);
		xmlSetProp (node, "object", str);
		g_free (str);

		xmlSetProp (node, "type", "query");
		g_print("done\n");
	}
	else
		g_warning ("QueryField not activated; can't save\n");
}

static void        
q_load_from_xml   (QueryField *qf, xmlNodePtr node)
{
	query_field_deactivate (qf);
	
	/* check we have a QueryField */
	if (!strcmp (node->name, "QueryField")) {
		gchar *str;

		str = xmlGetProp (node, "type");
		if (!str || (str && strcmp (str, "query"))) {
			if (str) g_free (str);
			return;
		}

		str = xmlGetProp (node, "object");
		/* check we have a query field relating to a table's field */
		if ((*str == 'Q') && (*(str + 1) == 'U')) {
			QF_PRIVATE_DATA(qf)->query_name = g_strdup (str);
			query_field_activate (qf);
		}
		g_free (str);
	}
}

static void        
q_copy_other_field(QueryField *qf, QueryField *other)
{
	/* we can't call q_destroy(qf) because we don't know what the type
	   of QueryField it was before. This is normally done by the
	   QueryField object before the copy */

	if (QF_PRIVATE_DATA(other)->query) {
		QF_PRIVATE_DATA(qf)->query = QF_PRIVATE_DATA(other)->query;
		query_field_activate (qf);
	}
}

static gboolean
q_is_equal_to (QueryField *qf, QueryField *other)
{
	gboolean retval = FALSE;

	if (other->field_type != qf->field_type)
		return FALSE;

	if (qf->activated && other->activated) {
		/* FIXME: test for the query's field as well */
		if (QF_PRIVATE_DATA(qf)->query == QF_PRIVATE_DATA(other)->query)
			retval = TRUE;
	}

	return retval;
}

static GSList *
q_get_monitored_objects (QueryField *qf)
{
	GSList *list = NULL;

	if (qf->activated)
		list = g_slist_prepend (NULL, QF_PRIVATE_DATA(qf)->query);

	return list;
}

static void
q_replace_comp (QueryField *qf, gint ref, GtkObject *old, GtkObject *new)
{
	/* no reference to other Objects yet. */
	return;
}


/* 
 * 
 * QueryField object's different implementations
 * 
 *
 */

Query *
query_field_query_get_query (QueryField *qf)
{
	g_assert (qf);
	g_assert (IS_QUERY_FIELD (qf));
	g_assert (qf->field_type == QUERY_FIELD_QUERY);

	if (qf->activated == FALSE)
		return NULL;
	else
		return QF_PRIVATE_DATA(qf)->query;
}
