/* func.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


/*
 * This module represents QueryField of type QUERY_FIELD_FUNCTION:
 * a function
 */

#include <config.h>
#include "../query.h"
#include "../query-field-private.h"
#include "../database.h"
#include "../sqlwiddbtree.h"

static void        q_init            (QueryField *qf);
static void        q_destroy         (QueryField *qf);
static void        q_deactivate      (QueryField *qf);
static void        q_activate        (QueryField *qf);
static GtkWidget * q_get_edit_widget (QueryField *qf);
static GtkWidget * q_get_sel_widget  (QueryField *qf, GtkSignalFunc func, gpointer data);
static gchar     * q_render_as_sql   (QueryField *qf, GSList * missing_values);
static xmlNodePtr  q_render_as_xml   (QueryField *qf, GSList * missing_values);
static gchar     * q_render_as_string(QueryField *qf, GSList * missing_values);
static void        q_save_to_xml     (QueryField *qf, xmlNodePtr node);
static void        q_load_from_xml   (QueryField *qf, xmlNodePtr node);
static void        q_copy_other_field(QueryField *qf, QueryField *other);
static gboolean    q_is_equal_to     (QueryField *qf, QueryField *other);
static GSList    * q_get_monitored_objects (QueryField *qf);
static void        q_replace_comp    (QueryField *qf, gint ref, GtkObject *old, GtkObject *new);

/* callbacks from the refering object */
static void        q_obj_destroyed_cb (GtkObject *obj, QueryField *qf);

typedef struct {
	ServerFunction *func;

	/* XML field name if we did not find it in the first place (like TVxxx:FIyyy) */
	gchar          *func_name;
	/* "destroy" signal from the ServerFunction */
	guint           func_signal_id;

	/* list of references to arguments (struct Arg)  */
	GSList         *args;
} private_data;


/* structure to hold an argument */
typedef struct {
	QueryField *qf;
	gchar      *xml;
	guint       signal_id;
} Arg;


#define QF_PRIVATE_DATA(qf) ((private_data *) qf->private_data)
#define QF_FUNC_ARG(arg) ((Arg *) (arg))

QueryFieldIface * 
query_field_function_get_iface()
{
	QueryFieldIface *iface;

	iface = g_new0 (QueryFieldIface, 1);
	iface->field_type = QUERY_FIELD_FUNCTION;
	iface->name = "function";
	iface->pretty_name = _("Function");
	iface->init = q_init;
	iface->destroy = q_destroy;
	iface->deactivate = q_deactivate;
	iface->activate = q_activate;
	iface->get_edit_widget = q_get_edit_widget;
	iface->get_sel_widget = q_get_sel_widget;
	iface->render_as_sql = q_render_as_sql;
	iface->render_as_xml = q_render_as_xml;
	iface->render_as_string = q_render_as_string;
	iface->save_to_xml = q_save_to_xml;
	iface->load_from_xml = q_load_from_xml;
	iface->copy_other_field = q_copy_other_field;
	iface->is_equal_to = q_is_equal_to;
	iface->get_monitored_objects = q_get_monitored_objects;
	iface->replace_comp = q_replace_comp;

	return iface;
}

#ifdef debug
static void
func_dump (QueryField *qf)
{
	GSList *list;
	g_print ("DUMPING FUNCTION QF %p:\n", qf);
	list = QF_PRIVATE_DATA(qf)->args;
	while (list) {
		Arg *arg = QF_FUNC_ARG(list->data);
		g_print ("\tARG %p\n", arg);
		g_print ("\t\tqf=%p, xml=%s, signal=%d\n", arg->qf, arg->xml, arg->signal_id);
		list = g_slist_next (list);
	}
}
#endif

static void        
q_init            (QueryField *qf)
{
	private_data *data;
	data = g_new0 (private_data, 1);
	qf->private_data = (gpointer) data;
	QF_PRIVATE_DATA(qf)->func = NULL;
	QF_PRIVATE_DATA(qf)->func_name = NULL;
	QF_PRIVATE_DATA(qf)->func_signal_id = 0;
	QF_PRIVATE_DATA(qf)->args = NULL;
}

static void free_args (QueryField *qf);

static void        
q_destroy         (QueryField *qf)
{
	q_deactivate (qf);
	if (qf->private_data) {
		if (QF_PRIVATE_DATA(qf)->func_name)
			g_free (QF_PRIVATE_DATA(qf)->func_name);

		free_args (qf);

		g_free (qf->private_data);
		qf->private_data = NULL;
	}
}

static void 
free_args (QueryField *qf)
{
	GSList *list;

	list = QF_PRIVATE_DATA(qf)->args;
	while (list) {
		if (QF_FUNC_ARG(list->data)->xml)
			g_free (QF_FUNC_ARG(list->data)->xml);
		
		if (QF_FUNC_ARG(list->data)->qf) 
			gtk_signal_disconnect (GTK_OBJECT (QF_FUNC_ARG(list->data)->qf), 
					       QF_FUNC_ARG(list->data)->signal_id);
		g_free (list->data);
		
		list = g_slist_next (list);
	}
	g_slist_free (QF_PRIVATE_DATA(qf)->args);
	QF_PRIVATE_DATA(qf)->args = NULL;
}


static void sub_qf_status_changed_cb (QueryField *qf, QueryField *pqf);
static void
q_deactivate      (QueryField *qf)
{
	GSList *list;
	g_print ("q_deactivate (%p)\n", qf);

	/* This function disconnects any event handler from any object
	   this QueryField wants to receive events from.
	   Here we disconnect from the function if we are connected */

	if (!qf->activated)
		return;

	/* Function part */
	if (QF_PRIVATE_DATA(qf)->func) {
		if (QF_PRIVATE_DATA(qf)->func_name) {
			g_free (QF_PRIVATE_DATA(qf)->func_name);
			QF_PRIVATE_DATA(qf)->func_name = NULL;
		}
		
		QF_PRIVATE_DATA(qf)->func_name = g_strdup_printf ("PR%s", QF_PRIVATE_DATA(qf)->func->objectid);
		
		gtk_signal_disconnect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->func), 
				       QF_PRIVATE_DATA(qf)->func_signal_id);
		QF_PRIVATE_DATA(qf)->func = NULL;
		QF_PRIVATE_DATA(qf)->func_signal_id = 0;
	}		


	/* Taking care of the arguments */
	list = QF_PRIVATE_DATA(qf)->args;
	while (list) {
		/* unreference that QueryField argument */
		/* g_print ("Now calls query_field_free_ref (%p)\n", QF_FUNC_ARG(list->data)->qf); */
/* 		query_field_free_ref (QF_FUNC_ARG(list->data)->qf); */		
		
		if (QF_FUNC_ARG(list->data)->xml &&
		    QF_FUNC_ARG(list->data)->qf) {
			g_free (QF_FUNC_ARG(list->data)->xml);
			QF_FUNC_ARG(list->data)->xml = NULL;
		}

		if (QF_FUNC_ARG(list->data)->qf) {
			gtk_signal_disconnect (GTK_OBJECT (QF_FUNC_ARG(list->data)->qf), 
					       QF_FUNC_ARG(list->data)->signal_id);
			QF_FUNC_ARG(list->data)->signal_id = 0;
			QF_FUNC_ARG(list->data)->xml = query_field_get_xml_id (QF_FUNC_ARG(list->data)->qf);
			gtk_signal_disconnect_by_func (GTK_OBJECT (QF_FUNC_ARG(list->data)->qf),
						       GTK_SIGNAL_FUNC (sub_qf_status_changed_cb), qf);
		}
		QF_FUNC_ARG(list->data)->qf = NULL;

		list = g_slist_next (list);
	}

	query_field_set_activated (qf, FALSE);
}

static QueryField *get_field_from_xml (GSList *list, gchar *xmlid);
static void arg_destroyed_cb (QueryField *arg, QueryField *qf);
static void
q_activate        (QueryField *qf)
{
	/* this function gets references to any object this QueryField wants to 
	   receive events from. */

	ServerFunction *func;
	
	if (qf->activated)
		return;

	func = QF_PRIVATE_DATA(qf)->func;
	
	if (!func && QF_PRIVATE_DATA(qf)->func_name) {
		func = server_function_get_from_xml_id (qf->query->conf->srv, 
							QF_PRIVATE_DATA(qf)->func_name);
		if (func) {
			g_free (QF_PRIVATE_DATA(qf)->func_name);
			QF_PRIVATE_DATA(qf)->func_name = NULL;	
		}
	}


	if (func) {
		GSList *list;
		gboolean allfound = TRUE;

		QF_PRIVATE_DATA(qf)->func = func;

		if (QF_PRIVATE_DATA(qf)->func_signal_id)
			gtk_signal_disconnect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->func), 
					       QF_PRIVATE_DATA(qf)->func_signal_id);
		QF_PRIVATE_DATA(qf)->func_signal_id = 
			gtk_signal_connect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->func), "destroy",
					    GTK_SIGNAL_FUNC (q_obj_destroyed_cb), qf);

		/* Taking care of the arguments */
		list = QF_PRIVATE_DATA(qf)->args;
		while (list) {
			if (! QF_FUNC_ARG(list->data)->qf) {
				if (QF_FUNC_ARG(list->data)->xml) {
					QueryField *aqf;
					GSList **qfs;
					
					/* fetch the list of QueryFields to work with */
					qfs = gtk_object_get_data (GTK_OBJECT (qf), "qf_list");

					if (!qfs)
						qfs = &(qf->query->fields);
					
					/* fetch the argument QueryField */
					aqf = get_field_from_xml (*qfs, QF_FUNC_ARG(list->data)->xml);

					if (aqf) {
						QF_FUNC_ARG(list->data)->signal_id = 
							gtk_signal_connect (GTK_OBJECT (aqf), "destroy",
									    GTK_SIGNAL_FUNC (arg_destroyed_cb), qf);
						QF_FUNC_ARG(list->data)->qf = aqf;
						if (! aqf->activated) {
							query_field_activate (aqf);
							if (! aqf->activated)
								allfound = FALSE;
						}
						gtk_signal_connect (GTK_OBJECT (aqf), "status_changed",
								    GTK_SIGNAL_FUNC (sub_qf_status_changed_cb), qf);
					}
					else 
						allfound = FALSE;
				}
				else
					allfound = FALSE;
			}
			else {
				if (! QF_FUNC_ARG(list->data)->qf->activated) {
					query_field_activate (QF_FUNC_ARG(list->data)->qf);
					if (! QF_FUNC_ARG(list->data)->qf->activated)
						allfound = FALSE;
				}
			}
			
			list = g_slist_next (list);
		}


		/* make sure we have the right number of arguments */
		if (g_slist_length (QF_PRIVATE_DATA(qf)->args) != 
		    g_slist_length (QF_PRIVATE_DATA(qf)->func->args)) {
			g_warning ("Wrong number of arguments for Function\n");
			allfound = FALSE;
		}

		/* pronounce the activation if OK */
		if (allfound) {
			GSList *list;

			/* add a reference to the QueryField object as arguments */
			list = QF_PRIVATE_DATA(qf)->args;
			while (list) {
				query_field_use_ref (QF_FUNC_ARG(list->data)->qf);
				list = g_slist_next (list);
			}

			/* now it's activated */
			query_field_set_activated (qf, TRUE);
		}
	}
}

static void 
sub_qf_status_changed_cb (QueryField *qf, QueryField *pqf)
{
	if (qf->activated)
		query_field_activate (pqf);
	else 
		query_field_set_activated (pqf, FALSE);
}

static QueryField *
get_field_from_xml (GSList *list, gchar *xmlid)
{
	QueryField *qf = NULL;
	gchar *str, *ptr;
	guint id;
	GSList *token;

	g_return_val_if_fail (xmlid, NULL);

	str = g_strdup (xmlid);
	ptr = strtok (str, ":");
	ptr += 2;
	
	ptr = strtok (NULL, ":");
	ptr +=2;
	id = atoi (ptr);
	g_free(str);

	token = list;
	while (token && !qf) {
		if (QUERY_FIELD (token->data)->id == id)
			qf = QUERY_FIELD (token->data);
		token = g_slist_next (token);
	}

	return qf;
}


static void        
q_obj_destroyed_cb (GtkObject *obj, QueryField *qf)
{
	/* if the field or table disappear, then destroy is the result */
	gtk_object_destroy (GTK_OBJECT (qf));
}

static void
arg_destroyed_cb (QueryField *arg, QueryField *qf)
{
	/* the steps are to deactivate the function, and empyt the arg */
	GSList *list;
	Arg *found = NULL;
	
	list = QF_PRIVATE_DATA(qf)->args;
	while (list && !found) {
		if (QF_FUNC_ARG(list->data)->qf == arg)
			found = QF_FUNC_ARG(list->data);
		list = g_slist_next (list);
	}

	if (!found) {
		g_warning ("Can't find argument destroyed!");
		return;
	}

	query_field_deactivate (qf);
	if (found->xml) { 
		g_free (found->xml);
		found->xml = NULL;
	}

#ifdef debug_signal
	g_print (">> 'FIELD_MODIFIED' from query-fields/arg_destroyed_cb\n");
#endif
	gtk_signal_emit_by_name (GTK_OBJECT (qf), "field_modified");
#ifdef debug_signal
	g_print ("<< 'FIELD_MODIFIED' from query-fields/arg_destroyed_cb\n");
#endif	
}


static void user_defined_functions_toggled_cb (GtkWidget * wid, GtkCList *clist);
static void system_defined_functions_toggled_cb (GtkWidget * wid, GtkCList *clist);
static void function_selected_cb (GtkWidget * wid, gint row, gint column,
				  GdkEventButton * event, GtkCList *clist);
static GtkWidget * 
q_get_edit_widget (QueryField *qf)
{
	GtkWidget *frame, *vb, *hb, *sw, *clist, *user, *sys;
	gchar *titles[] = { N_("Function"), N_("Description") };

	frame = gtk_frame_new (_("Function"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD/2.);

	vb = gtk_vbox_new (FALSE, GNOME_PAD/2.);
	gtk_container_add (GTK_CONTAINER (frame), vb);
	gtk_container_set_border_width (GTK_CONTAINER (vb), GNOME_PAD/2.);

	hb = gtk_hbox_new (TRUE, GNOME_PAD);
	gtk_box_pack_start (GTK_BOX (vb), hb, FALSE, TRUE, 0);

	clist = gtk_clist_new_with_titles (2, titles);
	gtk_object_set_data (GTK_OBJECT (clist), "qf", qf);

	user = gtk_check_button_new_with_label (_("User defined functions"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (user), TRUE);

	gtk_signal_connect (GTK_OBJECT (user), "toggled",
			    GTK_SIGNAL_FUNC (user_defined_functions_toggled_cb), clist);
	gtk_box_pack_start (GTK_BOX (hb), user, FALSE, TRUE, GNOME_PAD / 2);

	sys = gtk_check_button_new_with_label (_("System functions"));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (sys), FALSE);
	gtk_signal_connect (GTK_OBJECT (sys), "toggled",
			    GTK_SIGNAL_FUNC (system_defined_functions_toggled_cb), clist);
	gtk_box_pack_start (GTK_BOX (hb), sys, FALSE, TRUE, GNOME_PAD / 2);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_box_pack_start (GTK_BOX (vb), sw, TRUE, TRUE, 0);

	gtk_clist_set_selection_mode (GTK_CLIST (clist), GTK_SELECTION_SINGLE);
	gtk_clist_set_reorderable (GTK_CLIST (clist), FALSE);
	gtk_container_add (GTK_CONTAINER (sw), clist);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 0, TRUE);
	gtk_clist_set_column_auto_resize (GTK_CLIST (clist), 1, TRUE);
	gtk_clist_column_titles_passive (GTK_CLIST (clist));
	
	gtk_widget_show_all (vb);

	if (QF_PRIVATE_DATA(qf)->func) {
		gint row;
		if (QF_PRIVATE_DATA(qf)->func->is_user)
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (user), TRUE);
		else
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (sys), TRUE);
		
		row = gtk_clist_find_row_from_data (GTK_CLIST (clist), QF_PRIVATE_DATA(qf)->func);
		gtk_clist_select_row (GTK_CLIST (clist), row, 0);
		gtk_clist_moveto (GTK_CLIST (clist), row, 0, 0, 0);
	}

	gtk_signal_connect (GTK_OBJECT (clist), "select_row",
			    GTK_SIGNAL_FUNC (function_selected_cb), clist);

	return frame;
}

static void populate_functions (GtkCList *clist);

static void 
user_defined_functions_toggled_cb (GtkWidget * wid, GtkCList *clist)
{
	gtk_object_set_data (GTK_OBJECT (clist), "user_func", 
			     GINT_TO_POINTER (GTK_TOGGLE_BUTTON (wid)->active));
	gtk_clist_freeze (clist);
	gtk_clist_clear (clist);
	populate_functions (clist);
	gtk_clist_thaw (clist);
}

static void 
system_defined_functions_toggled_cb (GtkWidget * wid, GtkCList *clist)
{
	gtk_object_set_data (GTK_OBJECT (clist), "sys_func", 
			     GINT_TO_POINTER (GTK_TOGGLE_BUTTON (wid)->active));
	gtk_clist_freeze (clist);
	gtk_clist_clear (clist);
	populate_functions (clist);
	gtk_clist_thaw (clist);
}

static void 
populate_functions (GtkCList *clist)
{
	gchar *line[2];
	gint i;
	gboolean user, sys;
	QueryField *qf;
	GSList *list;

	qf = QUERY_FIELD (gtk_object_get_data (GTK_OBJECT (clist), "qf"));
	user = gtk_object_get_data (GTK_OBJECT (clist), "user_func") ? TRUE : FALSE;
	sys = gtk_object_get_data (GTK_OBJECT (clist), "sys_func") ? TRUE : FALSE;

	list = qf->query->conf->srv->data_functions;
	while (list) {
		if ((user && (SERVER_FUNCTION (list->data)->is_user)) ||
		    (sys && (!SERVER_FUNCTION (list->data)->is_user))) {
			line[0] = g_strdup (SERVER_FUNCTION (list->data)->sqlname);
			line[1] = g_strdup (SERVER_FUNCTION (list->data)->descr);
			i = gtk_clist_append (clist, line);
			gtk_clist_set_row_data (clist, i, list->data);
			g_free (line[0]);
			g_free (line[1]);
		}
		list = g_slist_next (list);
	}
}

static void 
function_selected_cb (GtkWidget * wid, gint row, gint column,
		      GdkEventButton * event, GtkCList *clist)
{
	ServerFunction *func;
	QueryField *qf;
	
	func = SERVER_FUNCTION (gtk_clist_get_row_data (GTK_CLIST (wid), row));
	qf = QUERY_FIELD (gtk_object_get_data (GTK_OBJECT (clist), "qf"));

	if (QF_PRIVATE_DATA(qf)->func != func) {
		GSList *list;

		query_field_deactivate (qf);

		/* Function's name */
		QF_PRIVATE_DATA(qf)->func = func;
		
		/* Arguments */
		free_args (qf);
		list = func->args;
		while (list) {
			QF_PRIVATE_DATA(qf)->args = g_slist_append (QF_PRIVATE_DATA(qf)->args, 
								    g_new0 (Arg, 1));
			list = g_slist_next (list);
		}
		query_field_activate (qf);
#ifdef debug_signal
		g_print (">> 'FIELD_MODIFIED' from function_selected_cb\n");
#endif
		gtk_signal_emit_by_name (GTK_OBJECT (qf), "field_modified");
#ifdef debug_signal
		g_print ("<< 'FIELD_MODIFIED' from function_selected_cb\n");
#endif	
	}
}

static GtkWidget * 
q_get_sel_widget  (QueryField *qf, GtkSignalFunc func, gpointer data)
{
	GtkWidget *hbox;
	GtkWidget *button, *label;
	gchar *str;
	GSList *list;
	gboolean firstarg = TRUE;
	guint i;

	hbox = gtk_hbox_new (FALSE, GNOME_PAD/2.);
	
	/* button for the function's name */
	if (QF_PRIVATE_DATA(qf)->func)
		str = g_strdup_printf ("%s", QF_PRIVATE_DATA(qf)->func->sqlname);
	else
		str = g_strdup (_("Function"));
	button = gtk_button_new_with_label (str);	
	g_free (str);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    func, data);
	gtk_object_set_data (GTK_OBJECT (button), "qf", qf);
	gtk_box_pack_start (GTK_BOX (hbox), button, TRUE, TRUE, 0);
	gtk_widget_show (button);	

	label = gtk_label_new (" (");
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	gtk_widget_show (label);

	/* Set the "QF_obj_emit_sig" attribute so that we can attach attributes to that button
	   which will be transmitted when the user clicks on it */
	gtk_object_set_data (GTK_OBJECT (hbox), "QF_obj_emit_sig", button);

	/* arguments */
	i = 0;
	list = QF_PRIVATE_DATA(qf)->args;
	while (list) {
		GtkObject *obj;
		GtkWidget *wid;
		
		/* separator */
		if (firstarg) 
			firstarg = FALSE;
		else {
			label = gtk_label_new (", ");
			gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
			gtk_widget_show (label);
		}

		/* sub widget */
		wid = query_field_get_select_widget (QF_FUNC_ARG(list->data)->qf, func, data);
		gtk_box_pack_start (GTK_BOX (hbox), wid, TRUE, TRUE, 0);
		gtk_widget_show (wid);

		obj = gtk_object_get_data (GTK_OBJECT (wid), "QF_obj_emit_sig");
		gtk_object_set_data (GTK_OBJECT (obj), "pqf", qf);
		gtk_object_set_data (GTK_OBJECT (obj), "ref", GUINT_TO_POINTER (i));

		list = g_slist_next (list);
		i++;
	}

	label = gtk_label_new (")");
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	gtk_widget_show (label);

	return hbox;
}

static gchar     * 
q_render_as_sql   (QueryField *qf, GSList * missing_values)
{
	gchar *str = NULL;

	if (qf->activated) {
		GString *string;
		GSList *list;
		gboolean firstarg = TRUE;

		string = g_string_new ("");
		g_string_sprintfa (string, "%s (", QF_PRIVATE_DATA(qf)->func->sqlname);
		
		list = QF_PRIVATE_DATA(qf)->args;
		while (list) {
			gchar *str2;
			if (firstarg) 
				firstarg = FALSE;
			else
				g_string_append (string, ", ");

			str2 = query_field_render_as_sql (QF_FUNC_ARG(list->data)->qf, NULL);
			g_string_append (string, str2);
			g_free (str2);
			list = g_slist_next (list);
		}
		
		g_string_append (string, ")");
		str = string->str;
		g_string_free (string, FALSE);
	}

	return str;
}

static xmlNodePtr  
q_render_as_xml   (QueryField *qf, GSList * missing_values)
{
	return NULL;
}

static gchar * 
q_render_as_string(QueryField *qf, GSList * missing_values)
{
	gchar *str = NULL;

	if (qf->activated) {
		GString *string;
		GSList *list;
		gboolean firstarg = TRUE;
		
		string = g_string_new ("");
		g_string_sprintfa (string, "%s (", QF_PRIVATE_DATA(qf)->func->sqlname);
		
		list = QF_PRIVATE_DATA(qf)->args;
		while (list) {
			gchar *str2;
			if (firstarg) 
				firstarg = FALSE;
			else
				g_string_append (string, ", ");
			
			str2 = query_field_render_as_string (QF_FUNC_ARG(list->data)->qf, NULL);
			g_string_append (string, str2);
			g_free (str2);
			
			list = g_slist_next (list);
		}
		
		g_string_append (string, ")");
		str = string->str;
		g_string_free (string, FALSE);
	}
	else 
		str = g_strdup (_("--Arguments Missing--"));

	return str;
}

static void  
q_save_to_xml     (QueryField *qf, xmlNodePtr node)
{
	if (qf->activated) {
		GSList *list;
		gchar *str;
		xmlNodePtr arg;

		g_print("Saving field to node %p...", node);	

		/* node object ref */
		str = g_strdup_printf ("PR%s", QF_PRIVATE_DATA(qf)->func->objectid); 
		xmlSetProp (node, "object", str);
		g_free (str);

		xmlSetProp (node, "type", "function");
		list = QF_PRIVATE_DATA(qf)->args;
		while (list) {
			arg = xmlNewChild (node, NULL, "QueryFieldArg", NULL);
			str = query_field_get_xml_id (QF_FUNC_ARG(list->data)->qf);
			xmlSetProp (arg, "ref", str);
			g_free (str);

			list = g_slist_next (list);
		}

		g_print("done\n");
	}
	else
		g_warning ("QueryField not activated; can't save\n");
}

static void        
q_load_from_xml   (QueryField *qf, xmlNodePtr node)
{
	query_field_deactivate (qf);
	
	/* check we have a QueryField */
	if (!strcmp (node->name, "QueryField")) {
		gchar *str;
		xmlNodePtr arg;

		str = xmlGetProp (node, "type");
		if (!str || (str && strcmp (str, "function"))) {
			if (str) g_free (str);
			return;
		}

		str = xmlGetProp (node, "object");
		/* check we have a query field relating to a function */
		if ((*str == 'P') && (*(str + 1) == 'R')) {
			QF_PRIVATE_DATA(qf)->func_name = g_strdup (str);
		}
		g_free (str);

		/* Getting the arguments */
		arg = node->xmlChildrenNode;
		while (arg) {
			if (*arg->name == 'Q') {
				str = xmlGetProp (arg, "ref");
				if (str) {
					Arg *arg;
					
					arg = g_new0 (Arg, 1);
					arg->xml = str;
					QF_PRIVATE_DATA(qf)->args = g_slist_append (QF_PRIVATE_DATA(qf)->args,
										    arg);
				}
			}
			arg = arg->next;
		}

		query_field_activate (qf);
	}
}

static void        
q_copy_other_field(QueryField *qf, QueryField *other)
{
	GSList *list;
	
	/* we can't call q_destroy(qf) because we don't know what the type
	   of QueryField it was before. This is normally done by the
	   QueryField object before the copy */

	if (QF_PRIVATE_DATA(other)->func) 
		QF_PRIVATE_DATA(qf)->func = QF_PRIVATE_DATA(other)->func;
	else {
		if (QF_PRIVATE_DATA(other)->func_name) 
			QF_PRIVATE_DATA(qf)->func_name = g_strdup (QF_PRIVATE_DATA(other)->func_name);
	}

	/* arguments of the function */
	list = QF_PRIVATE_DATA(other)->args;
	while (list) {
		Arg *arg;

		arg = g_new0 (Arg, 1);
		arg->xml = g_strdup (QF_FUNC_ARG(list->data)->xml);
		QF_PRIVATE_DATA(qf)->args = g_slist_append (QF_PRIVATE_DATA(qf)->args, arg);
		list = g_slist_next (list);
	}
	query_field_activate (qf);
}

static gboolean
q_is_equal_to (QueryField *qf, QueryField *other)
{
	gboolean retval = FALSE;

	if (qf->activated && other->activated) {
		if (QF_PRIVATE_DATA(qf)->func == QF_PRIVATE_DATA(other)->func)
			retval = TRUE;
	}

	return retval;
}



static GSList *
q_get_monitored_objects (QueryField *qf)
{
	GSList *list = NULL, *args;

	g_assert (qf && IS_QUERY_FIELD (qf));

	if (qf->activated)
		list = g_slist_prepend (NULL, QF_PRIVATE_DATA(qf)->func);

	g_print ("QF_PRIVATE_DATA(qf) = %p\n", QF_PRIVATE_DATA(qf));
	args = QF_PRIVATE_DATA(qf)->args;
	while (args) {
		if (QF_FUNC_ARG(args->data)->qf)
			list = g_slist_prepend (list, QF_FUNC_ARG(args->data)->qf);
		args = g_slist_next (args);
	}

	return list;
}


static void
q_replace_comp (QueryField *qf, gint ref, GtkObject *old, GtkObject *new)
{
	gboolean replaced = FALSE;

	g_assert (qf && IS_QUERY_FIELD (qf));

	if ((!old || (old && IS_QUERY_FIELD (old)) ) &&
	    (new && IS_QUERY_FIELD (new))) {
#ifdef debug
		g_print ("For QF %p, replacing (old=%p, ref=%d) with %p\n", qf, old, ref, new);
		func_dump (qf); /* REMOVE */
#endif

		if (old) {
			Arg *arg = NULL;
			GSList *list = QF_PRIVATE_DATA(qf)->args;

			while (list && !arg) {
				if (QF_FUNC_ARG(list->data)->qf == QUERY_FIELD (old))
					arg = QF_FUNC_ARG(list->data);
				list = g_slist_next (list);
			}
			if (arg) {
				query_field_deactivate (qf);
				if (arg->xml)
					g_free (arg->xml);
				arg->xml = query_field_get_xml_id (QUERY_FIELD (new));
				query_field_activate (qf);
				replaced = TRUE;
			}
		}
		else {
			Arg *arg;
		
			arg = g_slist_nth_data (QF_PRIVATE_DATA(qf)->args, ref);
			if (arg) {
				query_field_deactivate (qf);
				if (arg->xml)
					g_free (arg->xml);
				arg->xml = NULL;
				arg->xml = query_field_get_xml_id (QUERY_FIELD (new));
				query_field_activate (qf);
				replaced = TRUE;
			}
		}

		if (!replaced)
			g_warning ("Reference to be replaced is NULL\n");
		else {
#ifdef debug_signal
			g_print (">> 'FIELD_MODIFIED' from function_selected_cb\n");
#endif
			gtk_signal_emit_by_name (GTK_OBJECT (qf), "field_modified");
#ifdef debug_signal
			g_print ("<< 'FIELD_MODIFIED' from function_selected_cb\n");
#endif			
		}
	}
}


/* 
 * 
 * QueryField object's different implementations
 * 
 *
 */

