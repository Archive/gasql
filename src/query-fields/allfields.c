/* allfield.c
 *
 * Copyright (C) 2001 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


/*
 * This module represents QueryField of type QUERY_FIELD_ALLFIELDS:
 * all the fields of a table or a view
 */

#include <config.h>
#include "../query.h"
#include "../query-field-private.h"
#include "../database.h"
#include "../sqlwiddbtree.h"

static void        q_init            (QueryField *qf);
static void        q_destroy         (QueryField *qf);
static void        q_deactivate      (QueryField *qf);
static void        q_activate        (QueryField *qf);
static GtkWidget * q_get_edit_widget (QueryField *qf);
static GtkWidget * q_get_sel_widget  (QueryField *qf, GtkSignalFunc func, gpointer data);
static gchar     * q_render_as_sql   (QueryField *qf, GSList * missing_values);
static xmlNodePtr  q_render_as_xml   (QueryField *qf, GSList * missing_values);
static gchar     * q_render_as_string(QueryField *qf, GSList * missing_values);
static void        q_save_to_xml     (QueryField *qf, xmlNodePtr node);
static void        q_load_from_xml   (QueryField *qf, xmlNodePtr node);
static void        q_copy_other_field(QueryField *qf, QueryField *other);
static gboolean    q_is_equal_to     (QueryField *qf, QueryField *other);
static GSList    * q_get_monitored_objects (QueryField *qf);
static void        q_replace_comp    (QueryField *qf, gint ref, GtkObject *old, GtkObject *new);

/* callbacks from the refering object */
static void        q_table_destroyed_cb (GtkObject *field, QueryField *qf);

typedef struct {
	DbTable     *table;
	/* XML table name if we did not find it in the first place (like TVxxx:FIyyy) */
	gchar       *table_name;
	/* "destroy" signal from the DbTable */
	guint        signal_id;
} private_data;

#define QF_PRIVATE_DATA(qf) ((private_data *) qf->private_data)

QueryFieldIface * 
query_field_allfields_get_iface()
{
	QueryFieldIface *iface;

	iface = g_new0 (QueryFieldIface, 1);
	iface->field_type = QUERY_FIELD_ALLFIELDS;
	iface->name = "allfields";
	iface->pretty_name = _("All fields of a table");
	iface->init = q_init;
	iface->destroy = q_destroy;
	iface->deactivate = q_deactivate;
	iface->activate = q_activate;
	iface->get_edit_widget = q_get_edit_widget;
	iface->get_sel_widget = q_get_sel_widget;
	iface->render_as_sql = q_render_as_sql;
	iface->render_as_xml = q_render_as_xml;
	iface->render_as_string = q_render_as_string;
	iface->save_to_xml = q_save_to_xml;
	iface->load_from_xml = q_load_from_xml;
	iface->copy_other_field = q_copy_other_field;
	iface->is_equal_to = q_is_equal_to;
	iface->get_monitored_objects = q_get_monitored_objects;
	iface->replace_comp = q_replace_comp;

	return iface;
}

static void        
q_init            (QueryField *qf)
{
	private_data *data;
	data = g_new0 (private_data, 1);
	qf->private_data = (gpointer) data;
	QF_PRIVATE_DATA(qf)->table = NULL;
	QF_PRIVATE_DATA(qf)->table_name = NULL;
	QF_PRIVATE_DATA(qf)->signal_id = 0;
	
}

static void        
q_destroy         (QueryField *qf)
{
	query_field_deactivate (qf);
	if (qf->private_data) {
		if (QF_PRIVATE_DATA(qf)->table_name)
			g_free (QF_PRIVATE_DATA(qf)->table_name);
		g_free (qf->private_data);
		qf->private_data = NULL;
	}
}

static void
q_deactivate      (QueryField *qf)
{
	if (! qf->activated)
		return;

	/* This function disconnects any event handler from any object
	   this QueryField wants to receive events from.
	   Here we disconnect from the table if we are connected */
	if (QF_PRIVATE_DATA(qf)->table) {
		gtk_signal_disconnect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->table), 
				       QF_PRIVATE_DATA(qf)->signal_id);
		if (QF_PRIVATE_DATA(qf)->table_name) {
			g_free (QF_PRIVATE_DATA(qf)->table_name);
			QF_PRIVATE_DATA(qf)->table_name = NULL;
		}

		QF_PRIVATE_DATA(qf)->table_name = g_strdup_printf ("TV%s", 
								   QF_PRIVATE_DATA(qf)->table->name);

		QF_PRIVATE_DATA(qf)->table = NULL;
	}
	
	QF_PRIVATE_DATA(qf)->signal_id = 0;
	query_field_set_activated (qf, FALSE);
}

static void
q_activate        (QueryField *qf)
{
	/* this function gets references to any object this QueryField wants to 
	   receive events from.
	   Here we connect to the table of the field we are refering to */
	DbTable *table;
	
	if (qf->activated)
		return;

	table = QF_PRIVATE_DATA(qf)->table;
	
	if (!table && QF_PRIVATE_DATA(qf)->table_name) {
		table = database_find_table_from_xml_name (qf->query->conf->db, 
							   QF_PRIVATE_DATA(qf)->table_name);
		if (table) {
			g_free (QF_PRIVATE_DATA(qf)->table_name);
			QF_PRIVATE_DATA(qf)->table_name = NULL;	
		}
	}


	if (table) {
		QF_PRIVATE_DATA(qf)->table = table;
		QF_PRIVATE_DATA(qf)->signal_id = 
			gtk_signal_connect (GTK_OBJECT (QF_PRIVATE_DATA(qf)->table), "destroy",
					    GTK_SIGNAL_FUNC (q_table_destroyed_cb), qf);
		query_field_set_activated (qf, TRUE);
	}
}

static void
widget_table_sel_cb (GtkWidget * widget,
		     DbTable * t, QueryField *qf);

static GtkWidget * 
q_get_edit_widget (QueryField *qf)
{
	GtkWidget *wid, *frame, *vb;

	frame = gtk_frame_new (_("Table"));
	gtk_container_set_border_width (GTK_CONTAINER (frame), GNOME_PAD/2.);

	vb = gtk_vbox_new (FALSE, GNOME_PAD/2.);
	gtk_container_add (GTK_CONTAINER (frame), vb);
	gtk_container_set_border_width (GTK_CONTAINER (vb), GNOME_PAD/2.);

	wid = sql_wid_db_tree_new (qf->query->conf);
	gtk_widget_set_usize (GTK_WIDGET (wid), 200, 100);
	sql_wid_db_tree_set_mode (SQL_WID_DB_TREE (wid),
				  SQL_WID_DB_TREE_TABLES |
				  SQL_WID_DB_TREE_TABLES_SEL);
	gtk_signal_connect_while_alive (GTK_OBJECT (wid), "table_selected",
					GTK_SIGNAL_FUNC (widget_table_sel_cb), qf,
					GTK_OBJECT (qf));

	gtk_box_pack_start (GTK_BOX (vb), wid, TRUE, TRUE, GNOME_PAD/2.);
	gtk_widget_show_all (vb);
	
	if (QF_PRIVATE_DATA(qf)->table) {
		/* select the table in the widget */
		/* FIXME: TODO */
	}

	return frame;
}

static GtkWidget * 
q_get_sel_widget (QueryField *qf, GtkSignalFunc func, gpointer data)
{
	GtkWidget *button;
	gchar *str;

	if (qf->activated)
		str = g_strdup_printf("%s.*", QF_PRIVATE_DATA(qf)->table->name);
	else
		str = g_strdup (_("TABLE.*"));
	button = gtk_button_new_with_label (str);
	g_free (str);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    func, data);
	gtk_object_set_data (GTK_OBJECT (button), "qf", qf);

	/* Set the "QF_obj_emit_sig" attribute so that we can attach attributes to that button
	   which will be transmitted when the user clicks on it */
	gtk_object_set_data (GTK_OBJECT (button), "QF_obj_emit_sig", button);

	return button;
}

/* we consider the selected table is the the one being
   represented in the QueryField */
static void
widget_table_sel_cb (GtkWidget * widget,
		     DbTable * t, QueryField *qf)
{

	/* test if anything has changed */
	if (QF_PRIVATE_DATA(qf)->table == t) 
		return;

	query_field_deactivate (qf);
	query_field_allfields_set_table (qf, t);
	query_field_activate (qf);

#ifdef debug_signal
	g_print (">> 'FIELD_MODIFIED' from widget_field_sel_cb\n");
#endif
	gtk_signal_emit_by_name (GTK_OBJECT (qf), "field_modified");
#ifdef debug_signal
	g_print ("<< 'FIELD_MODIFIED' from widget_field_sel_cb\n");
#endif	
}

static void        
q_table_destroyed_cb (GtkObject *field, QueryField *qf)
{
	/* if the field disappears, then destroy is the result */
	gtk_object_destroy (GTK_OBJECT (qf));
}

static gchar     * 
q_render_as_sql   (QueryField *qf, GSList * missing_values)
{
	gchar *str = NULL;

	if (qf->activated) {
		/* FIXME: we don't have an alias policy, define it and
		   replace the name of the table with the alias */
		str = g_strdup_printf ("%s.*", QF_PRIVATE_DATA(qf)->table->name);
	}

	return str;
}

static xmlNodePtr  
q_render_as_xml   (QueryField *qf, GSList * missing_values)
{
	return NULL;
}

static gchar * 
q_render_as_string(QueryField *qf, GSList * missing_values)
{
	gchar *str = NULL;

	if (qf->activated) {
		str = g_strdup_printf ("%s.*", QF_PRIVATE_DATA(qf)->table->name);
	}

	return str;
}

static void  
q_save_to_xml     (QueryField *qf, xmlNodePtr node)
{
	if (qf->activated) {
		gchar *str;

		g_print("Saving all fields to node %p...", node);

		/* node object ref */
		str = g_strdup_printf ("TV%s", QF_PRIVATE_DATA(qf)->table->name);
		xmlSetProp (node, "object", str);
		g_free (str);

		xmlSetProp (node, "type", "allfields");
		g_print("done\n");
	}
	else
		g_warning ("QueryField not activated; can't save\n");
}

static void        
q_load_from_xml   (QueryField *qf, xmlNodePtr node)
{
	query_field_deactivate (qf);

	/* check we have a QueryField */
	if (!strcmp (node->name, "QueryField")) {
		gchar *str;

		str = xmlGetProp (node, "type");
		if (!str || (str && strcmp (str, "allfields"))) {
			if (str) g_free (str);
			return;
		}

		str = xmlGetProp (node, "object");
		/* check we have a query field relating to a table's field */
		if ((*str == 'T') && (*(str + 1) == 'V')) {
			QF_PRIVATE_DATA(qf)->table_name = g_strdup (str);
			query_field_activate (qf);
		}
		g_free (str);
	}
}

static void        
q_copy_other_field(QueryField *qf, QueryField *other)
{
	/* we can't call q_destroy(qf) because we don't know what the type
	   of QueryField it was before. This is normally done by the
	   QueryField object before the copy */

	if (QF_PRIVATE_DATA(other)->table) {
		QF_PRIVATE_DATA(qf)->table = QF_PRIVATE_DATA(other)->table;
		query_field_activate (qf);
	}
	else {
		if (QF_PRIVATE_DATA(other)->table_name) {
			QF_PRIVATE_DATA(qf)->table_name = g_strdup (QF_PRIVATE_DATA(other)->table_name);
			query_field_activate (qf);
		}
	}
}

static gboolean
q_is_equal_to (QueryField *qf, QueryField *other)
{
	gboolean retval = FALSE;;

	if (qf->activated && other->activated) {
		if (QF_PRIVATE_DATA(qf)->table == QF_PRIVATE_DATA(other)->table)
			retval = TRUE;
	}

	return retval;
}

static GSList *
q_get_monitored_objects (QueryField *qf)
{
	GSList *list = NULL;

	if (qf->activated)
		list = g_slist_prepend (NULL, QF_PRIVATE_DATA(qf)->table);

	return list;
}

static void
q_replace_comp (QueryField *qf, gint ref, GtkObject *old, GtkObject *new)
{
	/* no reference to other QueryFields */
	return;
}

/* 
 * 
 * QueryField object's different implementations
 * 
 *
 */


void
query_field_allfields_set_table (QueryField *qf, DbTable *table)
{
	gboolean activated = FALSE;
	gboolean done = FALSE;

	g_assert (qf);
	g_assert (IS_QUERY_FIELD (qf));
	g_assert (qf->field_type == QUERY_FIELD_ALLFIELDS);

	if (qf->activated) {
		activated = TRUE;
		query_field_deactivate (qf);
	}

	/* Test to find a QueryView for that field, otherwise do nothing */
	if (table) {
		GSList *list;
		gboolean found = FALSE;
		QueryView *qv;

		list = qf->query->views;
		while (list && !found) {
			qv = QUERY_VIEW (list->data);
			if (IS_DB_TABLE (qv->obj) && (DB_TABLE (qv->obj) == table))
				found = TRUE;
			list = g_slist_next (list);
		}
		
		if (found) {
			QF_PRIVATE_DATA(qf)->table = table;
			done = TRUE;
		}

	}

	if (done) {
		if (activated) 
			query_field_activate (qf);
	}
}
