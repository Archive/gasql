/* query-editor-fields.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "packedclist.h"
#include "query-editor-fields.h"


/*
 *  
 *
 * Implementation of the QueryEditorFields Widget
 * 
 *
 */



static GtkObject *parent_class = NULL;

static void query_editor_fields_class_init (QueryEditorFieldsClass * class);
static void query_editor_fields_init (QueryEditorFields * qef);
static void query_editor_fields_initialize (QueryEditorFields * qef);
static void query_editor_fields_destroy (GtkObject *obj);

guint
query_editor_fields_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Query_Editor_Fields",
			sizeof (QueryEditorFields),
			sizeof (QueryEditorFieldsClass),
			(GtkClassInitFunc) query_editor_fields_class_init,
			(GtkObjectInitFunc) query_editor_fields_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};
		
		f_type = gtk_type_unique (gtk_vbox_get_type (), &f_info);
	}

	return f_type;
}

static void
query_editor_fields_class_init (QueryEditorFieldsClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	object_class->destroy = query_editor_fields_destroy;
	parent_class = gtk_type_class (gtk_vbox_get_type ());
}

static void
query_editor_fields_init (QueryEditorFields * qef)
{
	qef->query = NULL;
	qef->selection = NULL;
	qef->mainlist = NULL;

	qef->edit_btn = NULL;
	qef->del_btn = NULL;

	qef->show_type = QEF_EXPR;
}

static void query_destroy_cb (Query *q, QueryEditorFields *qef);
static void query_type_changed_cb (Query *q, QueryEditorFields *qef);
static void query_editor_fields_initialize (QueryEditorFields * qef);
static void query_field_created_cb (Query *q, QueryField *new_field, QueryEditorFields *qef);
static void query_field_dropped_cb (Query *q, QueryField *old_field, QueryEditorFields *qef);
static void query_field_name_alias_changed_cb (Query *q, QueryField *field, QueryEditorFields *qef);
GtkWidget *
query_editor_fields_new (Query * q)
{
	GtkObject *obj;
	QueryEditorFields *qef;

	g_return_val_if_fail (q, NULL);
	g_return_val_if_fail (IS_QUERY (q), NULL);

	obj = gtk_type_new (query_editor_fields_get_type ());
	qef = QUERY_EDITOR_FIELDS (obj);
	qef->query = q;

	query_editor_fields_initialize (qef);

	/* signals */
	gtk_signal_connect_while_alive (GTK_OBJECT (q), "destroy",
					GTK_SIGNAL_FUNC (query_destroy_cb), qef,
					GTK_OBJECT (qef));

	gtk_signal_connect_while_alive (GTK_OBJECT (qef->query), "type_changed",
					GTK_SIGNAL_FUNC (query_type_changed_cb), qef,
					GTK_OBJECT (qef));

	gtk_signal_connect_while_alive (GTK_OBJECT (qef->query), "field_created",
					GTK_SIGNAL_FUNC (query_field_created_cb), qef,
					GTK_OBJECT (qef));

	gtk_signal_connect_while_alive (GTK_OBJECT (qef->query), "field_dropped",
					GTK_SIGNAL_FUNC (query_field_dropped_cb), qef,
					GTK_OBJECT (qef));

	gtk_signal_connect_while_alive (GTK_OBJECT (qef->query), "field_modified",
					GTK_SIGNAL_FUNC (query_field_name_alias_changed_cb), qef,
					GTK_OBJECT (qef));

	gtk_signal_connect_while_alive (GTK_OBJECT (qef->query), "field_name_modified",
					GTK_SIGNAL_FUNC (query_field_name_alias_changed_cb), qef,
					GTK_OBJECT (qef));

	gtk_signal_connect_while_alive (GTK_OBJECT (qef->query), "field_alias_modified",
					GTK_SIGNAL_FUNC (query_field_name_alias_changed_cb), qef,
					GTK_OBJECT (qef));


	return GTK_WIDGET (obj);
}

static void 
query_editor_fields_destroy (GtkObject *object)
{
	QueryEditorFields *qef;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_EDITOR_FIELDS (object));

	qef = QUERY_EDITOR_FIELDS (object);

	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void 
query_destroy_cb (Query *q, QueryEditorFields *qef)
{
	gtk_object_destroy (GTK_OBJECT (qef));
}

/* Update the GUI because of Query changes */
static void 
query_type_changed_cb (Query *q, QueryEditorFields *qef)
{
	gboolean sensi;

	sensi = q->type == QUERY_TYPE_STD ? TRUE : FALSE;
	gtk_widget_set_sensitive (GTK_WIDGET (qef), sensi);
}

static gboolean
query_field_to_be_displayed (QueryField *qf, QueryEditorFields *qef)
{
	gboolean to_display = FALSE;


	if ((qef->show_type & QEF_EXPR) &&
	    ((qf->name && *(qf->name)) || (qf->is_printed)))
		to_display = TRUE;

	if ((qef->show_type & QEF_COND));
	/* FIXME */

	if ((qef->show_type & QEF_UNSPEC));
	/* FIXME */

	if ((qef->show_type & QEF_NOTNAMED) &&
	    (!qf->name || (qf->name && !*(qf->name)) || (!qf->is_printed)))
		to_display = TRUE;

	return to_display;
}

static void
query_field_update_displayed_fields (Query *q, QueryEditorFields *qef)
{
	GSList *list;
	gboolean to_display;
	gint pos;

	gtk_clist_freeze (GTK_CLIST (PACKED_CLIST (qef->mainlist)->clist));
	list = q->fields;
	while (list) {
		to_display = query_field_to_be_displayed (QUERY_FIELD (list->data), qef);
		pos = gtk_clist_find_row_from_data (GTK_CLIST (PACKED_CLIST (qef->mainlist)->clist),
						    list->data);
		if ((pos < 0) && to_display) { /* Add entry */
			gchar *line[6];
			gint pos;

			line[0] = QUERY_FIELD (list->data)->name;
			line[1] = QUERY_FIELD (list->data)->alias;
			line[2] = query_field_render_as_string (QUERY_FIELD (list->data), NULL);
			line[3] = NULL;
			line[4] = NULL;
			line[5] = NULL;

			pos = g_slist_index (q->fields, list->data);
			pos = gtk_clist_insert (GTK_CLIST (PACKED_CLIST (qef->mainlist)->clist), pos, line);
			gtk_clist_set_row_data (GTK_CLIST (PACKED_CLIST (qef->mainlist)->clist), pos, list->data);
			g_free (line[2]);
		}

		if ((pos >= 0) && to_display) {
			/* Update entry */
			gchar *line[6];
			gint i;
			
			line[0] = QUERY_FIELD (list->data)->name;
			line[1] = QUERY_FIELD (list->data)->alias;
			line[2] = query_field_render_as_string (QUERY_FIELD (list->data), NULL);
			line[3] = NULL;
			line[4] = NULL;
			line[5] = NULL;
			
			for (i=0; i<6; i++) 
				gtk_clist_set_text (GTK_CLIST (PACKED_CLIST (qef->mainlist)->clist),
						    pos, i, line[i]);
			g_free (line[2]);
		}
		else {
			if ((pos >= 0) && !to_display) /* Remove entry */
				gtk_clist_remove (GTK_CLIST (PACKED_CLIST (qef->mainlist)->clist), pos);
		}

		list = g_slist_next (list);
	}
	gtk_clist_thaw (GTK_CLIST (PACKED_CLIST (qef->mainlist)->clist));
}

static void 
query_field_created_cb (Query *q, QueryField *new_field, QueryEditorFields *qef)
{
	query_field_update_displayed_fields (q, qef);
}

static void 
query_field_dropped_cb (Query *q, QueryField *old_field, QueryEditorFields *qef)
{
	gint pos;

	pos = gtk_clist_find_row_from_data (GTK_CLIST (PACKED_CLIST (qef->mainlist)->clist),
					    old_field);
	if (pos >= 0) 
		gtk_clist_remove (GTK_CLIST (PACKED_CLIST (qef->mainlist)->clist), pos);

	query_field_update_displayed_fields (q, qef);
}

static void 
query_field_name_alias_changed_cb (Query *q, QueryField *field, QueryEditorFields *qef)
{
	query_field_update_displayed_fields (q, qef);
}

static void obj_mitem_display (GtkButton *button, QueryEditorFields * qef);
static void obj_selected_cb (GtkCList *clist, gint row, gint column,
			     GdkEventButton *event, QueryEditorFields * qef);
static void obj_unselected_cb (GtkCList *clist, gint row, gint column,
			       GdkEventButton *event, QueryEditorFields * qef);
static void add_expression_cb (GtkWidget *button, QueryEditorFields * qef);
static void edit_expression_cb (GtkWidget *button, QueryEditorFields * qef);
static void del_expression_cb (GtkWidget *button, QueryEditorFields * qef);
static void
query_editor_fields_initialize (QueryEditorFields * qef)
{
	gchar *titles[] = {N_("Name"), N_("Print as"), 
			   N_("Value"), N_("Grouping"), 
			   N_("Order"), N_("Misc.")};
	GtkWidget *pcl;
	GtkWidget *table/*, *wid*/;
	GtkWidget *bb, *button;
	

	/* Main part */
	table = gtk_table_new (2, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD/2.);
	gtk_box_pack_start (GTK_BOX (qef), table, TRUE, TRUE, GNOME_PAD/2.);

	pcl = packed_clist_new_with_titles (6, titles, FALSE);
	gtk_table_attach_defaults (GTK_TABLE (table), pcl, 0, 2, 0, 1);
	qef->mainlist = pcl;
	gtk_signal_connect (GTK_OBJECT (PACKED_CLIST (pcl)->clist), "select_row",
			    GTK_SIGNAL_FUNC (obj_selected_cb), qef);
	gtk_signal_connect (GTK_OBJECT (PACKED_CLIST (pcl)->clist), "unselect_row",
			    GTK_SIGNAL_FUNC (obj_unselected_cb), qef);

	query_field_update_displayed_fields (qef->query, qef);

	/* Action area */
	bb = gtk_hbutton_box_new ();
	gtk_table_attach (GTK_TABLE (table), bb, 1, 2, 1, 2, 
			  GTK_SHRINK | GTK_FILL, 0, 0, 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);

	button = gtk_button_new_with_label (_("Add"));
	gtk_container_add (GTK_CONTAINER (bb), button);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (add_expression_cb), qef);

	button = gtk_button_new_with_label (_("Edit"));
	gtk_container_add (GTK_CONTAINER (bb), button);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (edit_expression_cb), qef);
	qef->edit_btn = button;
	gtk_widget_set_sensitive (button, FALSE);


	button = gtk_button_new_with_label (_("Del"));
	gtk_container_add (GTK_CONTAINER (bb), button);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (del_expression_cb), qef);
	qef->del_btn = button;
	gtk_widget_set_sensitive (button, FALSE);


	/* Button to display options options part */
	button = gtk_button_new_with_label (_("?"));
	/*wid = gtk_arrow_new (GTK_ARROW_RIGHT, GTK_SHADOW_NONE);*/
	/*gtk_container_add (GTK_CONTAINER (button), wid);*/
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (obj_mitem_display), qef);
	gtk_table_attach (GTK_TABLE (table), button, 0, 1, 1, 2, 
			  GTK_SHRINK | GTK_FILL, 0, 0, 0);


	/* Show everything but the widget itself */
	gtk_widget_show_all (table);

	/* Initial settings display */
	query_type_changed_cb (qef->query, qef);
}

static void obj_mitem_toggled_cb (GtkCheckMenuItem *mitem, QueryEditorFields * qef);
static void 
obj_mitem_display (GtkButton *button, QueryEditorFields * qef)
{
	GtkWidget *menu, *mitem;

	menu = gtk_menu_new ();

	mitem = gtk_check_menu_item_new_with_label (_("Query expressions"));
	gtk_object_set_data (GTK_OBJECT (mitem), "show_type", GINT_TO_POINTER (QEF_EXPR)); 
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (mitem), qef->show_type & QEF_EXPR);
	gtk_signal_connect (GTK_OBJECT (mitem), "toggled",
			    GTK_SIGNAL_FUNC (obj_mitem_toggled_cb), qef);
	gtk_menu_append (GTK_MENU (menu), mitem);
	gtk_widget_show (mitem);

	mitem = gtk_check_menu_item_new_with_label (_("Query conditions"));
	gtk_object_set_data (GTK_OBJECT (mitem), "show_type", GINT_TO_POINTER (QEF_COND)); 
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (mitem), qef->show_type & QEF_COND);
	gtk_signal_connect (GTK_OBJECT (mitem), "toggled",
			    GTK_SIGNAL_FUNC (obj_mitem_toggled_cb), qef);
	gtk_menu_append (GTK_MENU (menu), mitem);
	gtk_widget_show (mitem);

	mitem = gtk_check_menu_item_new_with_label (_("Values to specify"));
	gtk_object_set_data (GTK_OBJECT (mitem), "show_type", GINT_TO_POINTER (QEF_UNSPEC)); 
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (mitem), qef->show_type & QEF_UNSPEC);
	gtk_signal_connect (GTK_OBJECT (mitem), "toggled",
			    GTK_SIGNAL_FUNC (obj_mitem_toggled_cb), qef);
	gtk_menu_append (GTK_MENU (menu), mitem);
	gtk_widget_show (mitem);

	mitem = gtk_check_menu_item_new_with_label (_("Unnamed expressions"));
	gtk_object_set_data (GTK_OBJECT (mitem), "show_type", GINT_TO_POINTER (QEF_NOTNAMED)); 
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (mitem), qef->show_type & QEF_NOTNAMED);
	gtk_signal_connect (GTK_OBJECT (mitem), "toggled",
			    GTK_SIGNAL_FUNC (obj_mitem_toggled_cb), qef);
	gtk_menu_append (GTK_MENU (menu), mitem);
	gtk_widget_show (mitem);

	gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
			NULL, NULL, 0, 0);
}

/* Changes what objects are being displayed */
static void
obj_mitem_toggled_cb (GtkCheckMenuItem *mitem, QueryEditorFields * qef)
{
	QueryEditorFieldsShowType type;

	type = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (mitem), "show_type"));
	if (mitem->active)
		qef->show_type = qef->show_type | type;
	else
		qef->show_type = qef->show_type & (~type);

	query_field_update_displayed_fields (qef->query, qef);
}

static void 
obj_selected_cb (GtkCList *clist, gint row, gint column,
		 GdkEventButton *event, QueryEditorFields * qef)
{
	QueryField *qf;

	qf = QUERY_FIELD (gtk_clist_get_row_data (GTK_CLIST (PACKED_CLIST (qef->mainlist)->clist), row));
	qef->selection = qf;
	gtk_widget_set_sensitive (qef->edit_btn, TRUE);
	gtk_widget_set_sensitive (qef->del_btn, TRUE);
}

static void 
obj_unselected_cb (GtkCList *clist, gint row, gint column,
		   GdkEventButton *event, QueryEditorFields * qef)
{
	qef->selection = NULL;
	gtk_widget_set_sensitive (qef->edit_btn, FALSE);
	gtk_widget_set_sensitive (qef->del_btn, FALSE);
}


static void qee_status_changed_cb (QueryEditorExpr * qee, gboolean status, GnomeDialog *dlg);
static gint qee_dlg_close_cb      (GnomeDialog *dialog, QueryEditorFields * qef);
static void qee_dlg_clicked_cb    (GnomeDialog *dialog, gint btnno, QueryEditorFields * qef);
static void qee_destroy_cb        (QueryEditorExpr * qee, GnomeDialog *dialog);
static void 
add_expression_cb (GtkWidget *button, QueryEditorFields * qef)
{
	GtkWidget *wid, *dlg;

	dlg = gnome_dialog_new (_("New expression"), GNOME_STOCK_BUTTON_OK,
				GNOME_STOCK_BUTTON_CANCEL, NULL);
	
	gtk_window_set_policy (GTK_WINDOW (dlg), TRUE, TRUE, FALSE);
	gnome_dialog_set_sensitive (GNOME_DIALOG (dlg), 0, FALSE);


	wid = query_editor_expr_new (qef, NULL);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), wid, TRUE, TRUE, GNOME_PAD/2.);
	gtk_widget_show (wid);
	gtk_widget_show (dlg);

	gtk_signal_connect (GTK_OBJECT (wid), "status",
			    GTK_SIGNAL_FUNC (qee_status_changed_cb), dlg);
	gtk_object_set_data (GTK_OBJECT (dlg), "qee", wid);

	gtk_signal_connect (GTK_OBJECT (dlg), "clicked",
			    GTK_SIGNAL_FUNC (qee_dlg_clicked_cb), qef);
	gtk_signal_connect (GTK_OBJECT (dlg), "close",
			    GTK_SIGNAL_FUNC (qee_dlg_close_cb), qef);
	gtk_signal_connect (GTK_OBJECT (wid), "destroy",
			    GTK_SIGNAL_FUNC (qee_destroy_cb), dlg);
}

static void
qee_status_changed_cb (QueryEditorExpr * qee, gboolean status, GnomeDialog *dlg)
{
	gnome_dialog_set_sensitive (GNOME_DIALOG (dlg), 0, status);
}

static gint
qee_dlg_close_cb    (GnomeDialog *dialog, QueryEditorFields * qef)
{
	gpointer wid;

	wid = gtk_object_get_data (GTK_OBJECT (dialog), "qee");
	gtk_signal_disconnect_by_func (GTK_OBJECT (wid), 
				       GTK_SIGNAL_FUNC (qee_status_changed_cb), dialog);
	gtk_object_set_data (GTK_OBJECT (dialog), "qee", NULL);

	return FALSE; /* don't block the closing */
}

static void do_after_edition_merge_back (QueryEditorExpr * qee);
static void 
qee_dlg_clicked_cb    (GnomeDialog *dialog, gint btnno, QueryEditorFields * qef)
{
	gpointer wid;

	switch (btnno) {
	case 0:
		wid = gtk_object_get_data (GTK_OBJECT (dialog), "qee");
		do_after_edition_merge_back (QUERY_EDITOR_EXPR (wid));
		gnome_dialog_close (dialog);
		break;
	case 1:
		gnome_dialog_close (dialog);
		break;
	}
}

static GSList *list_used_query_fields_recursive (GSList *fields, QueryField *qf, GSList *list);
static void list_destroy_qf_cb (QueryField *qf, GSList **list);
static QueryField *find_similar_query_field (Query *q, GHashTable *equals, GHashTable *diffs, 
					     QueryField *qf);
static void 
do_after_edition_merge_back (QueryEditorExpr * qee)
{
	GSList *used_list, *list, *unused_list;
	GHashTable *equals, *diffs;
	gboolean orig_qf_replaced;

	QueryField *top_qf, *orig_qf;
	GSList *fields;

	g_return_if_fail (qee->top_qf);

	/* We now take ownership of the list of QueryField objects which were in the
	   QueryEditorExpr object */
	fields = qee->fields;
	top_qf = qee->top_qf;
	orig_qf = qee->orig_qf;
	query_editor_expr_detach (qee);

	/* Step 1, action 1: 
	 * list all the QueryFields which are referenced by the
	 * top QueryField being edited 
	 */
	g_print ("Top QF: %p\n", top_qf);
	used_list = list_used_query_fields_recursive (fields, top_qf, NULL);
	


	/* Step 1, action 2: 
	 * remove QueryFields present in the working list and not in that
	 * list we just made 
	 */
	/* 1.2.1: get the "destroy" signal from any QueryField */
	list = fields;
	while (list) {
		/* we want the fetch that signal because it will trigger the removal of the
		   QueryField from the fields list */
		gtk_signal_connect (GTK_OBJECT (list->data), "destroy",
				    GTK_SIGNAL_FUNC (list_destroy_qf_cb), &(fields));

		/* now, each QF will have to look into the query's list of fields
		   to activate itself if necessary, not in another list provided
		   in the "qf_list" attribute */
		gtk_object_set_data (GTK_OBJECT (list->data), "qf_list", NULL);
		list = g_slist_next (list);	
	}

	/* 1.2.2: remove unused QFs */
	list = fields;
	unused_list = NULL;
	while (list) {
		if (!g_slist_find (used_list, list->data)) {
			g_print ("Removed QF %p\n", list->data);
			unused_list = g_slist_prepend (unused_list, list->data);
			gtk_object_destroy (GTK_OBJECT (list->data));
			list = fields;
		}
		else
			list = g_slist_next (list);
	}
	g_print ("QFs: used=%d unused=%d total remaining now=%d\n", g_slist_length (used_list),
		 g_slist_length (unused_list), g_slist_length (fields));



	/* Step 2:
	 * for each QF in the remaining list ,try to find the corresponding one in
	 * the query->fields list (and when found, update the 'equals' hash table)
	 */
	/* 2.1 to 2.2 */
	equals = g_hash_table_new (NULL, NULL);
	diffs = g_hash_table_new (NULL, NULL);
	list = fields;
	while (list) {
		find_similar_query_field (qee->qef->query, equals, diffs, QUERY_FIELD (list->data));
		list = g_slist_next (list);
	}
	g_print ("Hash tables sizes: Equals=%d, Diffs=%d, length (liste)=%d\n", 
		 g_hash_table_size (equals), 
		 g_hash_table_size (diffs), g_slist_length (fields));



	/* 2.3 */
	list = fields;
	while (list) {
		GSList *olist, *iter, *ofields;
		gpointer data;

		ofields = fields;
		olist = query_field_get_monitored_objects (QUERY_FIELD (list->data));
		iter = olist;
		while (iter) {
			if (IS_QUERY_FIELD (iter->data)) {
				if ((data = g_hash_table_lookup (equals, iter->data))) {
					query_field_replace_ref_ptr (QUERY_FIELD (list->data),
								     GTK_OBJECT (iter->data),
								     GTK_OBJECT (data));
				}
			}
			iter = g_slist_next (iter);
		}
		g_slist_free (olist);

		/* the fields list may have changed because of the QF's replacement */
		if (ofields == fields)
			list = g_slist_next (list);
		else
			list = fields;
	}



	/* Step 3:
	 * Destroying the QueryFields in the equals hash, moving the other ones into
	 * the query itself, and resolving any naming issues that might have occured
	 */
	/* 3.1 */
	orig_qf_replaced = TRUE;
	list = fields;
	while (list) {
		if (g_hash_table_lookup (diffs, list->data)) {
			/* addition to the query */
			query_add_field (qee->qef->query, QUERY_FIELD (list->data));
			/* The name and alias may be changed during the insertion, but will be set
			   to the right value at the end of step 3.2 */
		}
		else {
			gpointer data;

			data = g_hash_table_lookup (equals, list->data);
			g_assert (data); /* equals and diffs hash tables MUST be disjoint */

			/* is it orig_qf ? */
			if (orig_qf && 
			    (QUERY_FIELD (data) == orig_qf))
				orig_qf_replaced = FALSE;

			/* replacing the name if necessary */
			if (QUERY_FIELD (list->data)->name && *QUERY_FIELD (list->data)->name)
				query_field_set_name (QUERY_FIELD (data), QUERY_FIELD (list->data)->name);

			/* replacing the alias if necessary */
			if (QUERY_FIELD (list->data)->alias && *QUERY_FIELD (list->data)->alias)
				query_field_set_alias (QUERY_FIELD (data), QUERY_FIELD (list->data)->alias);
		}

		list = g_slist_next (list);
	}
	
	/* 3.2 
	   Each QueryField in the Query which referenced the edited QueryField MUST be told to 
	   update its reference to the new QueryField which has replaced the edited one.*/
	if (!g_slist_find (fields, top_qf)) {
		g_warning ("Top QF %p Not present into fields list anymore, stopping edition of QF %p!",
			   top_qf, orig_qf);
		orig_qf_replaced = FALSE;
	}

	if (orig_qf_replaced && orig_qf) {
		gchar *name = NULL, *alias = NULL;

		/* Replace any reference to orig_qf to top_qf */
		list = qee->qef->query->fields;
		while (list) {
			GSList *olist;

			olist = query_field_get_monitored_objects (QUERY_FIELD (list->data));
			while (olist) {
				if (IS_QUERY_FIELD (olist->data) &&
				    (QUERY_FIELD (olist->data) == orig_qf))
					query_field_replace_ref_ptr (QUERY_FIELD (list->data), 
								     GTK_OBJECT (olist->data),
								     GTK_OBJECT (top_qf));
				olist = g_slist_next (olist);
			}			
			list = g_slist_next (list);
		}

		/* Keep the name and alias for the QueryField which has replaced orig_qf
		   and for which the name and alias have changed since the orig_qf was still there */
		if (orig_qf->name)
			name = g_strdup (orig_qf->name);
		if (orig_qf->alias)
			alias = g_strdup (orig_qf->alias);

		/* Remove the orig_qf QueryField */
		query_del_field (qee->qef->query, orig_qf);

		/* Set back the right name and alias */
		if (name) {
			query_field_set_name (top_qf, name);
			g_free (name);
		}
		if (alias) {
			query_field_set_alias (top_qf, alias);
			g_free (alias);
		}
	}

	/* Last step:
	   Freeing the fields list and the hash tables in the end 
	*/
	list = fields;
	while (list) {
		if (g_hash_table_lookup (equals, list->data)) {
			gtk_object_destroy (GTK_OBJECT (list->data));
			list = fields;
		}
		else
			list = g_slist_next (list);
	}

	/* Remove the callback established in 2.2 */
	list = fields;
	while (list) {
		gtk_signal_disconnect_by_func (GTK_OBJECT (list->data),
					       GTK_SIGNAL_FUNC (list_destroy_qf_cb), 
					       &(fields));
		list = g_slist_next (list);
	}
	
	g_slist_free (fields);

	g_hash_table_destroy (equals);
	g_hash_table_destroy (diffs);
}


/* Finds a similar QueryField in the fields of the Query passed as argument
 * ('qf' does not need to be in the Query's fields list) */
static QueryField *
find_similar_query_field (Query *q, GHashTable *equals, GHashTable *diffs, QueryField *qf)
{
	QueryField *similar = NULL;
	GSList *list;
	gpointer value;
	gboolean search_done = FALSE;

	/* If we have already done the research, then it is easy */
	if ((value = g_hash_table_lookup (equals, qf)))
		return QUERY_FIELD (value);
	if (g_hash_table_lookup (diffs, qf))
		return NULL;

	/* Real search necessary */
	list = q->fields;
	while (list && !search_done) {
		if (query_field_is_equal (qf, QUERY_FIELD (list->data))) {
			QueryField *simqf = QUERY_FIELD (list->data);
			GSList *olist, *simolist, *iter, *simiter;
			gboolean subs_all_similars = TRUE;


			/* Make sure any monitored QF has a similar QF in the query */
			olist = query_field_get_monitored_objects (qf);
			simolist = query_field_get_monitored_objects (simqf);

			g_print ("olist %d, simolist %d\n", g_slist_length (olist), g_slist_length (simolist));

			if (g_slist_length (olist) == g_slist_length (simolist)) {
				iter = olist;
				simiter = simolist;
				
				while (iter && subs_all_similars) {
					if (IS_QUERY_FIELD (iter->data) &&
					    IS_QUERY_FIELD (simiter->data)) {
						QueryField *sqf;
						
						g_print ("iter=%p, simiter=%p\n", iter->data,
							 simiter->data);
						if ((sqf = find_similar_query_field (q, equals, diffs,
										     QUERY_FIELD (iter->data)))) {
							g_print ("sqf= %p, simiter->data = %p\n", sqf,
								 simiter->data);
							if (sqf != QUERY_FIELD (simiter->data))
								subs_all_similars = FALSE;
						}
						else
							subs_all_similars = FALSE;	
					}
					else {
						if ((IS_QUERY_FIELD (iter->data) &&
						     !IS_QUERY_FIELD (simiter->data)) ||
						    (!IS_QUERY_FIELD (iter->data) &&
						     IS_QUERY_FIELD (simiter->data)))
							subs_all_similars = FALSE;
					}
					
					iter = g_slist_next (iter);
					simiter = g_slist_next (simiter);
				}
			}
			else
				subs_all_similars = FALSE;

			g_slist_free (olist);
			g_slist_free (simolist);

			/* update the hash tables */
			if (subs_all_similars) {
				similar = QUERY_FIELD (list->data); 
				g_hash_table_insert (equals, qf, similar);
				g_print ("Similar QF: %p <=> %p\n", qf, similar);
			
				search_done = TRUE; /* end of the search */
			}
		}
		list = g_slist_next (list);
	}
	if (!similar) {
		g_hash_table_insert (diffs, qf, qf);
		g_print ("No Similar for QF: %p\n", qf);
	}

	return similar;
}


static GSList *
list_used_query_fields_recursive (GSList *fields, QueryField *qf, GSList *list)
{
	GSList *retlist = list, *olist, *iter;
	g_assert (qf);

	/* This Query Field? */
	if (g_slist_find (fields, qf)) {
		if (!g_slist_find (retlist, qf))
			retlist = g_slist_append (retlist, qf);
	}
	else
		g_warning ("Can't find referenced QueryField in QueryEditorFields's fields list!\n");

	olist = query_field_get_monitored_objects (qf);
	iter = olist;
	while (iter) {
		if (IS_QUERY_FIELD (iter->data)) {
			retlist = list_used_query_fields_recursive (fields, 
								    QUERY_FIELD (iter->data), retlist);
		}
		iter = g_slist_next (iter);
	}
	g_slist_free (olist);

	return retlist;
}

/* Remove from the list a QF which has been destroyed */
static void 
list_destroy_qf_cb (QueryField *qf, GSList **list)
{
	if (g_slist_find (*list, qf)) {
		*list = g_slist_remove (*list, qf);
	}
}


static void
qee_destroy_cb        (QueryEditorExpr * qee, GnomeDialog *dialog)
{
	/* close the dialog */
	gtk_widget_destroy (GTK_WIDGET (dialog));
}

static void 
edit_expression_cb (GtkWidget *button, QueryEditorFields * qef)
{
	if (qef->selection) {
		GtkWidget *wid, *dlg;
		dlg = gnome_dialog_new (_("Expression's edition"), GNOME_STOCK_BUTTON_OK,
					GNOME_STOCK_BUTTON_CANCEL, NULL);
		gtk_window_set_policy (GTK_WINDOW (dlg), TRUE, TRUE, FALSE);
		gnome_dialog_set_sensitive (GNOME_DIALOG (dlg), 0, qef->selection->activated);		

		wid = query_editor_expr_new (qef, qef->selection);
		gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), wid, TRUE, TRUE, GNOME_PAD/2.);
		gtk_widget_show (wid);
		gtk_widget_show (dlg);

		gtk_signal_connect (GTK_OBJECT (wid), "status",
				    GTK_SIGNAL_FUNC (qee_status_changed_cb), dlg);
		gtk_object_set_data (GTK_OBJECT (dlg), "qee", wid);

		gtk_signal_connect (GTK_OBJECT (dlg), "clicked",
				    GTK_SIGNAL_FUNC (qee_dlg_clicked_cb), qef);
		gtk_signal_connect (GTK_OBJECT (dlg), "close",
				    GTK_SIGNAL_FUNC (qee_dlg_close_cb), qef);
		gtk_signal_connect (GTK_OBJECT (wid), "destroy",
				    GTK_SIGNAL_FUNC (qee_destroy_cb), dlg);
	}
}


static void 
del_expression_cb (GtkWidget *button, QueryEditorFields * qef)
{
	if (qef->selection) 
		gtk_object_destroy (GTK_OBJECT (qef->selection));
}













/************************************************************************************************************/




/*
 *  
 *
 * Implementation of the QueryEditorExpr Widget
 * 
 *
 */

enum {
	STATUS,
	LAST_EXPR_SIGNAL
};

static gint expr_signals[LAST_EXPR_SIGNAL] = { 0 };


static GtkObject *expr_parent_class = NULL;

static void query_editor_expr_class_init (QueryEditorExprClass * class);
static void query_editor_expr_init (QueryEditorExpr * qee);
static void query_editor_expr_initialize (QueryEditorExpr * qee);
static void query_editor_expr_destroy (GtkObject *obj);

guint
query_editor_expr_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Query_Editor_Expr",
			sizeof (QueryEditorExpr),
			sizeof (QueryEditorExprClass),
			(GtkClassInitFunc) query_editor_expr_class_init,
			(GtkObjectInitFunc) query_editor_expr_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};
		
		f_type = gtk_type_unique (gtk_vbox_get_type (), &f_info);
	}

	return f_type;
}

static void
query_editor_expr_class_init (QueryEditorExprClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	object_class->destroy = query_editor_expr_destroy;

	expr_signals[STATUS] =
		gtk_signal_new ("status", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (QueryEditorExprClass, status),
				gtk_marshal_NONE__BOOL, GTK_TYPE_NONE, 1,
				GTK_TYPE_BOOL);

	gtk_object_class_add_signals (object_class, expr_signals, LAST_EXPR_SIGNAL);
	class->status = NULL;

	expr_parent_class = gtk_type_class (gtk_vbox_get_type ());
}

static void
query_editor_expr_init (QueryEditorExpr * qee)
{
	qee->qef = NULL;
	qee->orig_qf = NULL;
	qee->top_qf = NULL;
	qee->sel_qf = NULL;
	qee->fields = NULL;
	qee->data_detached = FALSE; /* => QueryFields managed by this widget */

	qee->parent_sel_qf = NULL;
	qee->parent_sel_ref = -1;

	qee->frame = NULL;
	qee->current_expr = NULL;
	qee->expr_container = NULL;
	qee->current_area = NULL;
	qee->area_container = NULL;
	qee->name_entry = NULL;
	qee->alias_entry = NULL;
	qee->type_omenu = NULL;
}

static void set_top_qf (QueryEditorExpr *qee, QueryField *top_qf);
static void qef_destroy_cb (Query *q, QueryEditorExpr *qee);
static void expr_query_field_dropped_cb (Query *q, QueryField *old_field, QueryEditorExpr *qee);
GtkWidget *
query_editor_expr_new (QueryEditorFields * qef, QueryField *qf)
{
	GtkObject *obj;
	QueryEditorExpr *qee;

	g_return_val_if_fail (qef, NULL);
	g_return_val_if_fail (IS_QUERY_EDITOR_FIELDS (qef), NULL);

	obj = gtk_type_new (query_editor_expr_get_type ());
	qee = QUERY_EDITOR_EXPR (obj);
	qee->qef = qef;
	qee->orig_qf = qf;

	if (qf) {
		/* make a copy of qf to be the QueryField we are working on */
		set_top_qf (qee, QUERY_FIELD (query_field_new_copy_all (qf, &(qee->fields))));
		qee->sel_qf = qee->top_qf;
	}
	

	query_editor_expr_initialize (qee);

	/* signals */
	gtk_signal_connect_while_alive (GTK_OBJECT (qef), "destroy",
					GTK_SIGNAL_FUNC (qef_destroy_cb), qee,
					GTK_OBJECT (qee));

	gtk_signal_connect_while_alive (GTK_OBJECT (qee->qef->query), "field_dropped",
					GTK_SIGNAL_FUNC (expr_query_field_dropped_cb), qee,
					GTK_OBJECT (qee));

	return GTK_WIDGET (obj);
}

static void top_qf_status_changed (QueryField *qf, QueryEditorExpr *qee);
static void top_qf_destroy_cb (QueryField *qf, QueryEditorExpr *qee);
static void 
set_top_qf (QueryEditorExpr *qee, QueryField *top_qf)
{
	if (qee->top_qf != top_qf) {
		if (qee->top_qf) {
			gtk_signal_disconnect_by_func (GTK_OBJECT (qee->top_qf),
						       top_qf_status_changed, qee);
			gtk_signal_disconnect_by_func (GTK_OBJECT (qee->top_qf),
						       top_qf_destroy_cb, qee);
		}
		qee->top_qf = top_qf;
		if (top_qf) {
			gtk_signal_connect (GTK_OBJECT (qee->top_qf), "status_changed",
					    GTK_SIGNAL_FUNC (top_qf_status_changed), qee);
			gtk_signal_connect (GTK_OBJECT (qee->top_qf), "destroy",
					    GTK_SIGNAL_FUNC (top_qf_destroy_cb), qee);
		}
	}
}

static void 
top_qf_destroy_cb (QueryField *qf, QueryEditorExpr *qee)
{
	set_top_qf (qee, NULL);
}

static void 
top_qf_status_changed (QueryField *qf, QueryEditorExpr *qee)
{
#ifdef debug_signal
	g_print (">> 'STATUS' from top_qf_status_changed\n");
#endif
	gtk_signal_emit (GTK_OBJECT (qee), expr_signals[STATUS], qee->top_qf->activated);
#ifdef debug_signal
	g_print ("<< 'STATUS' from top_qf_status_changed\n");
#endif
}

static void 
query_editor_expr_destroy (GtkObject *object)
{
	QueryEditorExpr *qee;
	GSList *list;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_EDITOR_EXPR (object));

	qee = QUERY_EDITOR_EXPR (object);

	if (!qee->data_detached) {
		if (qee->top_qf) 
			set_top_qf (qee, NULL); /* to disconnect signals properly */
		
		/* free the list of QueryFields */
		list = qee->fields;
		while (list) {
			gtk_object_unref (GTK_OBJECT (list->data));
			list = g_slist_next (list);
		}
		g_slist_free (qee->fields);
		qee->fields = NULL;

		qee->parent_sel_ref = -1;
		qee->parent_sel_qf = NULL;
	}

	if (GTK_OBJECT_CLASS (expr_parent_class)->destroy)
		(*GTK_OBJECT_CLASS (expr_parent_class)->destroy) (object);
}


static void 
qef_destroy_cb (Query *q, QueryEditorExpr *qee)
{
	gtk_object_destroy (GTK_OBJECT (qee));
}


static void 
expr_query_field_dropped_cb (Query *q, QueryField *old_field, QueryEditorExpr *qee)
{
	if (old_field == qee->orig_qf)
		qee->orig_qf = NULL;
}


#ifdef debug
static void dump_fields (GtkWidget *button, QueryEditorExpr * qee);
#endif
static void expr_name_changed_cb (GtkEntry *entry, QueryEditorExpr * qee); 
static void expr_alias_changed_cb (GtkEntry *entry, QueryEditorExpr * qee); 
static void qee_refresh (QueryEditorExpr * qee);
static void
query_editor_expr_initialize (QueryEditorExpr * qee)
{
	GtkWidget *vb, *hb, *label, *frame, *table, *entry;
	GtkWidget *menu, *optionmenu;

#ifdef debug
	GtkWidget *button;
#endif
	
	/* Expression part */
	hb = gtk_hbox_new (FALSE, GNOME_PAD/2.);
	gtk_box_pack_start (GTK_BOX (qee), hb, FALSE, TRUE, 0);
	
	label = gtk_label_new (_("Complete expression:"));
	gtk_box_pack_start (GTK_BOX (hb), label, FALSE, TRUE, GNOME_PAD/2.);

	vb = gtk_vbox_new (FALSE, GNOME_PAD/2.);
	gtk_box_pack_start (GTK_BOX (hb), vb, FALSE, TRUE, GNOME_PAD/2.);
	qee->expr_container = vb;

#ifdef debug
	button = gtk_button_new_with_label ("Dump");
	gtk_box_pack_start (GTK_BOX (hb), button, FALSE, TRUE, GNOME_PAD/2.);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (dump_fields), qee);
#endif

	/* Properties */
	frame = gtk_frame_new (_("Selected expression's details"));
	gtk_box_pack_start (GTK_BOX (qee), frame, TRUE, TRUE, 0);
	qee->frame = frame;

	vb = gtk_vbox_new (FALSE, GNOME_PAD/2.);
	gtk_container_add (GTK_CONTAINER (frame), vb);

	table = gtk_table_new (3, 2, FALSE);
	
	gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD/2.);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD/2.);
	gtk_box_pack_start (GTK_BOX (vb), table, FALSE, TRUE, 0);
	label = gtk_label_new (_("Type:"));
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1,
			  0, 0, 0, 0);

	optionmenu = gtk_option_menu_new ();
	qee->type_omenu = optionmenu;
	menu = gtk_menu_new ();

	gtk_option_menu_set_menu (GTK_OPTION_MENU (optionmenu), menu);
	gtk_table_attach_defaults (GTK_TABLE (table), optionmenu, 1, 2, 0, 1);

	label = gtk_label_new (_("Name:"));
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2,
			  0, 0, 0, 0);

	entry = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 1, 2,
			  GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0, 0);
	qee->name_entry = entry;
	gtk_signal_connect (GTK_OBJECT (entry), "changed",
			    GTK_SIGNAL_FUNC (expr_name_changed_cb), qee);

	label = gtk_label_new (_("Print name:"));
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3,
			  0, 0, 0, 0);

	entry = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 2, 3,
			  GTK_EXPAND | GTK_SHRINK | GTK_FILL, 0, 0, 0);
	qee->alias_entry = entry;
	gtk_signal_connect (GTK_OBJECT (entry), "changed",
			    GTK_SIGNAL_FUNC (expr_alias_changed_cb), qee);

	qee->area_container = gtk_vbox_new (FALSE, GNOME_PAD/2.);
	gtk_box_pack_start (GTK_BOX (vb), qee->area_container, TRUE, TRUE, 0);


	/* show all the sub widgets of qee */
	gtk_widget_show_all (hb);
	gtk_widget_show_all (frame);
	if (!qee->sel_qf)
		gtk_widget_set_sensitive (frame, FALSE);

	/* finish with an initial refresh */
	qee_refresh (qee);

	gtk_widget_set_usize (GTK_WIDGET (qee), 380, 300);
}

static void 
expr_name_changed_cb (GtkEntry *entry, QueryEditorExpr * qee)
{
	if (qee->sel_qf)
		query_field_set_name (qee->sel_qf, gtk_entry_get_text (entry));
}

static void 
expr_alias_changed_cb (GtkEntry *entry, QueryEditorExpr * qee)
{
	if (qee->sel_qf)
		query_field_set_alias (qee->sel_qf, gtk_entry_get_text (entry));
}


static void qee_refresh_sel_area (QueryEditorExpr * qee);
static void expr_type_act_cb (GtkMenuItem *mitem, QueryEditorExpr *qee);
static void expr_alias_act_cb (GtkMenuItem *mitem, QueryEditorExpr *qee);
static void expr_selected_cb (GtkWidget *wid, QueryEditorExpr *qee);
static void expr_qf_changed_cb (QueryField *qf, QueryEditorExpr *qee);
static void 
qee_refresh (QueryEditorExpr * qee)
{
	GtkWidget *area;
	GtkWidget *menu, *smenu, *mitem, *smitem;
	GSList *list;
	QueryField *qf;
	gint i=1, pos=0;

	/* Set the Type's option menu's possible values */
	gtk_option_menu_remove_menu (GTK_OPTION_MENU (qee->type_omenu));
	menu = gtk_menu_new ();
	gtk_option_menu_set_menu (GTK_OPTION_MENU (qee->type_omenu), menu);

	mitem = gtk_menu_item_new_with_label (_("Undefined!"));
	gtk_menu_append (GTK_MENU (menu), mitem);
	gtk_widget_set_sensitive (mitem , FALSE);
	gtk_object_set_data (GTK_OBJECT (mitem), "qftype", GINT_TO_POINTER (-1));

	list = QUERY_FIELD_CLASS(gtk_type_class (query_field_get_type ()))->field_types;
	while (list) {
		mitem = gtk_menu_item_new_with_label (_(((QueryFieldIface *)(list->data))->pretty_name));
		gtk_menu_append (GTK_MENU (menu), mitem);
		gtk_object_set_data (GTK_OBJECT (mitem), "qftype", 
				     GINT_TO_POINTER (((QueryFieldIface *)(list->data))->field_type));
		gtk_signal_connect (GTK_OBJECT (mitem), "activate",
				    GTK_SIGNAL_FUNC (expr_type_act_cb), qee);
		if ((qee->sel_qf) && 
		    ((QueryFieldIface *)(list->data))->field_type == qee->sel_qf->field_type)
			pos = i;

		list = g_slist_next (list);
		i++;
	}
	gtk_option_menu_set_history (GTK_OPTION_MENU (qee->type_omenu), pos);

	/* sub menu for the names objects */
	mitem = NULL;
	smenu = NULL;
	list = qee->fields;
	while (list) { /* named objects already in the working list */
		qf = QUERY_FIELD (list->data);
		if (qf->name && *(qf->name)) {
			gchar *str;

			if (!mitem) {
				mitem = gtk_menu_item_new_with_label (_("From named expressions"));
				gtk_menu_append (GTK_MENU (menu), mitem);
				smenu = gtk_menu_new ();
				gtk_menu_item_set_submenu (GTK_MENU_ITEM (mitem), smenu);
			}
			str = g_strdup_printf ("%s (*)", qf->name);
			smitem = gtk_menu_item_new_with_label (str);
			g_free (str);
			gtk_object_set_data (GTK_OBJECT (smitem), "qfalias", qf);
			gtk_signal_connect (GTK_OBJECT (smitem), "activate",
					    GTK_SIGNAL_FUNC (expr_alias_act_cb), qee); 
			gtk_menu_append (GTK_MENU (smenu), smitem);
		}
		list = g_slist_next (list);
	}
	list = qee->qef->query->fields;
	while (list) { /* named objects still not copied into the working list */
		qf = QUERY_FIELD (list->data);
		if (qf->name && *(qf->name)) {
			QueryField *otherqf;
			GSList *there;
			gboolean found = FALSE;
			
			there = qee->fields;
			while (there && !found) { /* look for duplicates */
				otherqf = QUERY_FIELD (there->data);
				if (otherqf->name && *(otherqf->name) && !strcmp (otherqf->name, qf->name))
					found = TRUE;
				there = g_slist_next (there);
			}
			
			if (!found) {
				if (!mitem) {
					mitem = gtk_menu_item_new_with_label (_("From named expressions"));
					gtk_menu_append (GTK_MENU (menu), mitem);
					smenu = gtk_menu_new ();
					gtk_menu_item_set_submenu (GTK_MENU_ITEM (mitem), smenu);
				}
				smitem = gtk_menu_item_new_with_label (qf->name);
				gtk_object_set_data (GTK_OBJECT (smitem), "qfalias", qf);
				gtk_signal_connect (GTK_OBJECT (smitem), "activate",
						    GTK_SIGNAL_FUNC (expr_alias_act_cb), qee); 
				gtk_menu_append (GTK_MENU (smenu), smitem);
			}
		}
		list = g_slist_next (list);
	}

	gtk_widget_show_all (menu);

	/* selection area */
	qee_refresh_sel_area (qee);

	/* fill in the entries: name, alias */
	gtk_signal_handler_block_by_func (GTK_OBJECT (qee->name_entry),
					  GTK_SIGNAL_FUNC (expr_name_changed_cb), qee);
	gtk_signal_handler_block_by_func (GTK_OBJECT (qee->alias_entry),
					  GTK_SIGNAL_FUNC (expr_alias_changed_cb), qee);
	if (qee->sel_qf) {
		if (qee->sel_qf->name)
			gtk_entry_set_text (GTK_ENTRY (qee->name_entry), qee->sel_qf->name);
		else
			gtk_entry_set_text (GTK_ENTRY (qee->name_entry), "");
		if (qee->sel_qf->alias)
			gtk_entry_set_text (GTK_ENTRY (qee->alias_entry), qee->sel_qf->alias);
		else
			gtk_entry_set_text (GTK_ENTRY (qee->alias_entry), "");
	}
	else {
		gtk_entry_set_text (GTK_ENTRY (qee->name_entry), "");
		gtk_entry_set_text (GTK_ENTRY (qee->alias_entry), "");
	}
	gtk_signal_handler_unblock_by_func (GTK_OBJECT (qee->name_entry),
					    GTK_SIGNAL_FUNC (expr_name_changed_cb), qee);
	gtk_signal_handler_unblock_by_func (GTK_OBJECT (qee->alias_entry),
					    GTK_SIGNAL_FUNC (expr_alias_changed_cb), qee);


	/* display the QueryField's specific part */
	if (qee->current_area) {
		gtk_widget_destroy (qee->current_area);
		qee->current_area = NULL;
	}
	if (qee->sel_qf)
		area = query_field_get_edit_widget (qee->sel_qf);
	else {
		area = gtk_label_new ("");
		gtk_widget_set_usize (area, 200, 100);
	}
	gtk_box_pack_start (GTK_BOX (qee->area_container), area, TRUE, TRUE, GNOME_PAD);	
	gtk_widget_show (area);
	qee->current_area = area;

	/* connects the "field_modified" signal from the QueryFields */
	list = qee->fields;
	while (list) {
		if (! GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (list->data), "sig"))) {
			guint sig;		
			sig = gtk_signal_connect (GTK_OBJECT (list->data), "field_modified",
						  GTK_SIGNAL_FUNC (expr_qf_changed_cb), qee);
			gtk_object_set_data (GTK_OBJECT (list->data), "sig", GUINT_TO_POINTER (sig));
		}
		list = g_slist_next (list);
	}
}

static void
qee_refresh_sel_area (QueryEditorExpr * qee)
{
	GtkWidget *expr;

	/* The expression display is created/replaced */
	if (qee->current_expr)
		gtk_widget_destroy (qee->current_expr);
	expr = query_field_get_select_widget (qee->top_qf, 
					      GTK_SIGNAL_FUNC (expr_selected_cb), qee);
	gtk_box_pack_start (GTK_BOX (qee->expr_container), expr, FALSE, TRUE, GNOME_PAD/2.);
	qee->current_expr = expr;
	gtk_widget_show (expr);
}


/* the user has selected a QueryField from the complete expression */
static void 
expr_selected_cb (GtkWidget *wid, QueryEditorExpr *qee)
{
	QueryField *qf;
	gpointer ptr;

	/* QueryField to be edited */
	ptr = gtk_object_get_data (GTK_OBJECT (wid), "qf");
	if (!ptr) {
		/* the user can create a new QueryField */
		gtk_widget_set_sensitive (qee->frame, TRUE);

		/* clean the display */
		qee->sel_qf = NULL;
		qee_refresh (qee);
	}
	else {
		gtk_widget_set_sensitive (qee->frame, TRUE);
		qf = QUERY_FIELD (ptr);
		if (qf != qee->sel_qf) {
			qee->sel_qf = qf;
			qee_refresh (qee);
		}
	}

	/* get some info on an upper level QueryField which needs to
	 be told if the selected QueryField is replaced by another */
	ptr = gtk_object_get_data (GTK_OBJECT (wid), "pqf");
	if (ptr) {
		gpointer ref;
		ref = gtk_object_get_data (GTK_OBJECT (wid), "ref");

		qee->parent_sel_qf = QUERY_FIELD (ptr);
		qee->parent_sel_ref = GPOINTER_TO_INT (ref);
	}
}

static void 
expr_qf_changed_cb (QueryField *qf, QueryEditorExpr *qee)
{
	qee_refresh_sel_area (qee);
}

static void replace_query_field (QueryEditorExpr *qee, QueryField *new_field);

/* This function is called when a menu item from the menu of the different types of QF available
   for creation is activated */
static void 
expr_type_act_cb (GtkMenuItem *mitem, QueryEditorExpr *qee)
{
	gint type;
	QueryField *qf = NULL;
	QueryFieldType qftype;

	type = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (mitem), "qftype"));
	if (type == -1)
		qf = NULL;
	else {
		/* create a new QF */
		qftype = (QueryFieldType) type;
		qf = QUERY_FIELD (query_field_new (qee->qef->query, NULL, qftype));
		gtk_object_set_data (GTK_OBJECT (qf), "qf_list", &(qee->fields));
		qee->fields = g_slist_append (qee->fields, qf);
	}


	replace_query_field (qee, qf);

	/* finaly refresh! */
	qee->sel_qf = qf;
	qee_refresh (qee);
}

static void 
expr_alias_act_cb (GtkMenuItem *mitem, QueryEditorExpr *qee)
{
	gpointer ptr;
	
	ptr = gtk_object_get_data (GTK_OBJECT (mitem), "qfalias");
	if (ptr) {
		QueryField *alias;

		alias = QUERY_FIELD (ptr);
		
		if (! g_slist_find (qee->fields, alias)) 
			/* copy recursively that QueryField into qee->fields */
			alias = QUERY_FIELD (query_field_new_copy_all (alias, &(qee->fields)));

		replace_query_field (qee, alias);

		/* finaly refresh! */
		qee->sel_qf = alias;
		qee_refresh (qee);
	}
}


/* replace the current selected QueryField with the new one */
static void
replace_query_field (QueryEditorExpr *qee, QueryField *new_field)
{
	/* if we are editing the main QueryField, then update qee->top_qf */
	g_print ("Top=%p, sel=%p\n", qee->top_qf, qee->sel_qf);
	if (qee->sel_qf == qee->top_qf) {
		gboolean status = FALSE;

		if (qee->top_qf)
			status = qee->top_qf->activated;

		set_top_qf (qee, new_field);
		qee->sel_qf = new_field;


		if (status != qee->top_qf->activated)
			top_qf_status_changed (qee->top_qf, qee);
	}

	/* now do the replacement in a parent QueryField if applicable */
	if (qee->parent_sel_qf) {
		query_field_replace_ref_int (qee->parent_sel_qf, qee->parent_sel_ref, new_field);
	}
}

void
query_editor_expr_detach (QueryEditorExpr * qee)
{
	GSList *list;
	guint sig;

	g_return_if_fail (qee && IS_QUERY_EDITOR_EXPR (qee));

	/* disconnects the "field_modified" signal from the QueryFields */
	list = qee->fields;
	while (list) {
		if ((sig = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (list->data), "sig")))) {
			gtk_signal_disconnect (GTK_OBJECT (list->data), sig);
			gtk_object_set_data (GTK_OBJECT (list->data), "sig", NULL);
		}
		list = g_slist_next (list);
	}
	/* DON'T FREE THE qee->fields LIST HERE */
	qee->fields = NULL;

	/* Selection and top_qf */
	set_top_qf (qee, NULL);
	qee->parent_sel_ref = -1;
	qee->parent_sel_qf = NULL;


	/* Widget status */
	qee->data_detached = TRUE;
}








/* 
 * Debug functions 
 */
#ifdef debug
static gchar* field_type_to_str (QueryFieldType t) 
{
	gchar *str;

	switch (t) {
	case QUERY_FIELD_FIELD:
		str = "QUERY_FIELD_FIELD";
		break;
	case QUERY_FIELD_ALLFIELDS:
		str = "QUERY_FIELD_ALLFIELDS";
		break;
	case QUERY_FIELD_AGGREGATE:
		str = "QUERY_FIELD_AGGREGATE";
		break;
	case QUERY_FIELD_FUNCTION:
		str = "QUERY_FIELD_FUNCTION";
		break;
	case QUERY_FIELD_VALUE:
		str = "QUERY_FIELD_VALUE";
		break;
	case QUERY_FIELD_QUERY:
		str = "QUERY_FIELD_QUERY";
		break;
	case QUERY_FIELD_QUERY_FIELD:
		str = "QUERY_FIELD_QUERY_FIELD";
		break;
	default:
		str = "UNKNOWN TYPE";
	}
	return str;
}

static void 
dump_fields (GtkWidget *button, QueryEditorExpr * qee)
{
	GSList *list = qee->fields;
	gchar *str="";
	GSList *monitored, *iter;

	g_print ("%s" D_COL_H2 "--------------------------------------\n" D_COL_NOR, str);

	g_print ("%s" D_COL_H2 "Top Query fields:%p\n" D_COL_NOR, str, qee->top_qf);
	g_print ("%s" D_COL_H2 "Sel Query fields:%p\n" D_COL_NOR, str, qee->sel_qf);
	while (list) {
		QueryField *qf;
		qf = QUERY_FIELD (list->data);
		g_print ("%s" D_COL_H2 "Query field:%p\n" D_COL_NOR, str, qf);

		g_print ("%s  " D_COL_H2 "* field:" D_COL_NOR " %s\n", str, qf->name);
		g_print ("%s\tid: %d\n", str, qf->id);
		g_print ("%s\talias : %s\n", str, qf->alias);
		g_print ("%s\tfield type: %s\n", str, field_type_to_str (qf->field_type));
		monitored = query_field_get_monitored_objects (qf);
		iter = monitored;
		while (iter) {
			if (iter == monitored)
				g_print ("%s\tmonitored objects:", str);
			g_print (" %p", iter->data);
			iter = g_slist_next (iter);
		}
		if (monitored) {
			g_print ("\n");
			g_slist_free (monitored);
		}
		if (qf->activated) {
			gchar *sql;
			
			g_print ("%s\t" D_COL_OK "activated\n" D_COL_NOR , str);
			sql = query_field_render_as_sql (qf, NULL);
			g_print ("%s\tSQL= %s\n", str, sql);
			g_free (sql);
		}
		else
			g_print("%s\t" D_COL_ERR "not activated\n" D_COL_NOR, str);
		list = g_slist_next (list);
	}
}
#endif
