/* gladechooser.h
 *
 * Copyright (C) 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GLADE_CHOOSER__
#define __GLADE_CHOOSER__

#include <gnome.h>
#include <glade/glade.h>
#include <config.h>

#include "gladedefs.h"

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */ 
	
#define GLADE_CHOOSER(obj)          GTK_CHECK_CAST (obj, glade_chooser_get_type(), GladeChooser)
#define GLADE_CHOOSER_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, glade_chooser_get_type (), GladeChooserClass) 
#define IS_GLADE_CHOOSER(obj)       GTK_CHECK_TYPE (obj, glade_chooser_get_type ())
	
	
	typedef struct _GladeChooser        GladeChooser;
	typedef struct _GladeChooserClass   GladeChooserClass;
  

	/* struct for the object's data */
	struct _GladeChooser {
		GtkVBox object;
    
		GladeFormStruct *form_struct;
		GtkWidget *glade_widget;

		/* user interface widgets */
		GtkWidget *scrolled_window;
		GtkWidget *combo;
		GtkWidget *apply_button;
	};

	/* struct for the object's class */
	struct _GladeChooserClass {
		GtkVBoxClass parent_class;

		void (*form_changed) (GladeChooser *gchooser);
	};

	/* generic widget's functions */
	guint        glade_chooser_get_type (void);
	GtkWidget*   glade_chooser_new (void);

	/* returns the glade widget and remove it from the GladeChooser widget; the
	   widget is ref'ed and should be unref'ed after putting it in another container */
	GtkWidget*   fetch_glade_widget (GladeChooser *gchooser);
  
#ifdef __cplusplus
}
#endif /* __cplusplus */   

#endif
