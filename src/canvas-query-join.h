/* canvas-query-join.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __CANVAS_QUERY_JOIN__
#define __CANVAS_QUERY_JOIN__

#include <gnome.h>
#include "canvas-base.h"
#include "query.h"
#include "config.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */


/* QueryJoin item for the canvas.  
 *
 * In addition to the GnomeCanvasGroup and CanvasBase arguments, the following object arguments 
 * are available::
 *
 * name                 type                    read/write      description
 * ------------------------------------------------------------------------------------------
 * query                pointer                 RW              The Query to which the QueryView is attached
 * query_join           pointer                 RW              The QueryJoin being displayed
 * 
 * 
 * NOTE: the "query" and "query_join" arguments are required.
 * 
 */


#define CQJ_DEFAULT_VIEW_FONT "6x13"
#define CQJ_DEFAULT_VIEW_FONT_BOLD "6x13bold"


#define CANVAS_QUERY_JOIN(obj)          GTK_CHECK_CAST (obj, canvas_query_join_get_type(), CanvasQueryJoin)
#define CANVAS_QUERY_JOIN_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, canvas_query_join_get_type (), CanvasQueryJoinClass)
#define IS_CANVAS_QUERY_JOIN(obj)       GTK_CHECK_TYPE (obj, canvas_query_join_get_type ())


	typedef struct _CanvasQueryJoin CanvasQueryJoin;
	typedef struct _CanvasQueryJoinClass CanvasQueryJoinClass;


	/* struct for the object's data */
	struct _CanvasQueryJoin
	{
		CanvasBase          object;

		/* objects being represented */
		Query              *query;
		QueryJoin          *join;

		/* presentation parameters */
		gdouble             x_text_space;
		gdouble             y_text_space;

		/* UI building information */
		GnomeCanvasItem    *bg_frame;
		GSList             *items;

		/* References to other GnomeCanvasItems */
		GnomeCanvasItem    *ant_cview;
		GnomeCanvasItem    *suc_cview;

		/* Dialog for the properties */
		GnomeDialog        *props_dlg;
	};

	/* struct for the object's class */
	struct _CanvasQueryJoinClass
	{
		CanvasBaseClass     parent_class;
	};

	/* generic widget's functions */
	guint      canvas_query_join_get_type (void);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
