/* conf-manager.c
 *
 * Copyright (C) 2001 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "interface_cb.h"
#include "conf-manager.h"
#include "mainpagetable.h"
#include "mainpageseq.h"
#include "mainpagequery.h"
#include "mainpageenv.h"
#include <gnome-db.h>

static void conf_manager_class_init (ConfManagerClass * class);
static void conf_manager_init (ConfManager * conf);
static void conf_manager_destroy (GtkObject * object);

/* global variables for some home made icons */
static gchar *connect_icon = NULL;
static gchar *disconnect_icon = NULL;
static gchar *connect_icon_small = NULL;
static gchar *disconnect_icon_small = NULL;
static gchar *relations_icon = NULL;
static gchar *relations_icon_small = NULL;

enum
{
	DATABASE_ADDED,
	DATABASE_REMOVED,
	QUERY_ADDED,
	QUERY_REMOVED,
	ENV_ADDED,
	ENV_REMOVED,
	LAST_SIGNAL
};

static gint conf_manager_signals[LAST_SIGNAL] = { 0, 0, 0, 0, 0, 0 };


guint
conf_manager_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"ConfManager",
			sizeof (ConfManager),
			sizeof (ConfManagerClass),
			(GtkClassInitFunc) conf_manager_class_init,
			(GtkObjectInitFunc) conf_manager_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_object_get_type (), &f_info);
	}

	return f_type;
}

static void
conf_manager_class_init (ConfManagerClass * class)
{
	GtkObjectClass *object_class = NULL;

	object_class = (GtkObjectClass *) class;
	conf_manager_signals[DATABASE_ADDED] = 
		gtk_signal_new ("database_added",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ConfManagerClass, database_added),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	conf_manager_signals[DATABASE_REMOVED] = 
		gtk_signal_new ("database_removed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ConfManagerClass, database_removed),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	conf_manager_signals[QUERY_ADDED] = 
		gtk_signal_new ("query_added",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ConfManagerClass, query_added),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	conf_manager_signals[QUERY_REMOVED] = 
		gtk_signal_new ("query_removed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ConfManagerClass, query_removed),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	conf_manager_signals[ENV_ADDED] = 
		gtk_signal_new ("env_added",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ConfManagerClass, env_added),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	conf_manager_signals[ENV_REMOVED] = 
		gtk_signal_new ("env_removed",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ConfManagerClass, env_removed),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);


	gtk_object_class_add_signals (object_class, conf_manager_signals, LAST_SIGNAL);
	class->database_added = NULL;
	class->database_removed = NULL;
	class->query_added = NULL;
	class->query_removed = NULL;
	class->env_added = NULL;
	class->env_removed = NULL;
	object_class->destroy = conf_manager_destroy;
}

static void
conf_manager_init (ConfManager * conf)
{
	conf->app = NULL;
	conf->appbar = NULL;
	conf->nb = NULL;
	conf->working_box = NULL;
	conf->error_dlg = NULL;
	conf->manager_bonobo_win = NULL;
	conf->relations_dialog = NULL;

	conf->tables_page = NULL;
	conf->queries_page = NULL;

	conf->widgets_show_on_connect = NULL;
	conf->widgets_show_on_disconnect = NULL;
	conf->widgets_sensitive_on_connect = NULL;
	conf->widgets_sensitive_on_disconnect = NULL;

	conf->srv = NULL;
	conf->db = NULL;
	conf->config_dlg = NULL;
	conf->conn_open_requested = FALSE;
	conf->config_plugins_dlg = NULL;
	conf->top_query = NULL;
	conf->id_serial = 0;

	conf->working_file = NULL;
	conf->save_up_to_date = TRUE;
	conf->close_after_saving = FALSE;
	conf->quit_after_saving = FALSE;
	conf->loading_in_process = FALSE;

	conf->plugins_dir = NULL;

	conf->check_dlg = NULL;
	conf->check_pbar = NULL;
	conf->check_link_name = NULL;
	conf->check_errors = NULL;
	conf->check_perform = FALSE;

	conf->users_list_dlg = NULL;
	conf->users_groups_dlg = NULL;
	conf->users_acl_dlg = NULL;

	conf->printer = NULL;
	conf->printcontext = NULL;

	conf->datasource = NULL;
	conf->user_name = NULL;
	conf->user_passwd = NULL;
	conf->file_to_open = NULL;

	conf->clipboard_str = NULL;
	conf->clipboard_obj = NULL;

	conf->created_envs = NULL;
}

static void widgets_conn_management_cb (ServerAccess *srv, ConfManager *conf);
static void objects_conn_management_cb (ServerAccess *srv, ConfManager *conf);

GtkObject *
conf_manager_new ()
{
	GtkObject *obj;
	ConfManager *conf;

	obj = gtk_type_new (conf_manager_get_type ());
	conf = CONF_MANAGER (obj);

	return obj;
}

static void build_main_window (ConfManager *conf);
void progress_cb (GtkObject * obj, gchar * msg, guint now, guint total,
		  ConfManager * conf);

void 
conf_manager_finalize (ConfManager *conf)
{
	CORBA_ORB orb;	

	/* new ServerAccess object */
	orb = gda_corba_get_orb ();
	conf->srv = SERVER_ACCESS (server_access_new (orb));
	g_assert (conf->srv);
	conf->srv->description = g_strdup ("Main SQL server");

	/* callbacks to create and destroy the Database and top Query objects */
	gtk_signal_connect (GTK_OBJECT (conf->srv), "conn_opened",
			    GTK_SIGNAL_FUNC (objects_conn_management_cb), conf);
	gtk_signal_connect (GTK_OBJECT (conf->srv), "conn_closed",
			    GTK_SIGNAL_FUNC (objects_conn_management_cb), conf);

	/* conf->srv is not NULL */
	gtk_signal_connect (GTK_OBJECT (conf->srv), "conn_opened",
			    GTK_SIGNAL_FUNC (sql_server_conn_open_cb), conf);
	gtk_signal_connect (GTK_OBJECT (conf->srv), "conn_to_close",
			    GTK_SIGNAL_FUNC (sql_server_conn_to_close_cb),
			    conf);
	gtk_signal_connect (GTK_OBJECT (conf->srv), "conn_closed",
			    GTK_SIGNAL_FUNC (sql_server_conn_close_cb), conf);
	gtk_signal_connect (GTK_OBJECT (conf->srv), "error",
			    GTK_SIGNAL_FUNC (sql_server_catch_errors_cb),
			    conf);

	/* those 2 CBs are to show/hide and order sensitiveness of objects */
	gtk_signal_connect (GTK_OBJECT (conf->srv), "conn_opened",
			    GTK_SIGNAL_FUNC (widgets_conn_management_cb), conf);
	gtk_signal_connect (GTK_OBJECT (conf->srv), "conn_closed",
			    GTK_SIGNAL_FUNC (widgets_conn_management_cb), conf);
       
	gtk_signal_connect (GTK_OBJECT (conf->srv), "progress",
			    GTK_SIGNAL_FUNC (progress_cb), conf);

	/* building all the main window stuff here */
	build_main_window (conf);
}

static gint register_icon (gchar * name, gchar * filename);
static void add_toolbar (GnomeApp * app, ConfManager * conf);
static void add_menus (GnomeApp * app, ConfManager * conf);
static void todo_cb (GtkWidget * widget, ConfManager * conf);
static void about_cb (GtkWidget * widget, ConfManager * conf);
static gint delete_event_cb (GtkWidget * wid, GdkEvent * event, ConfManager * conf);
#ifdef debug
static void debug_remove_table_cb (GtkWidget * widget, ConfManager * conf);
static void debug_remove_field_cb (GtkWidget * widget, ConfManager * conf);
static void debug_test_cb (GtkWidget * widget, ConfManager * conf);
static void debug_mark_cb (GtkWidget * widget, ConfManager * conf);
static void debug_dump_cb (GtkWidget * widget, ConfManager * conf);
static void debug_dump_queries_cb (GtkWidget * widget, ConfManager * conf);
#endif
static void 
build_main_window (ConfManager *conf)
{
	GtkWidget *vb, *wid, *owid, *hb, *sc;
	gchar *str;


	str = conf_manager_get_title (conf);
	conf->app = gnome_app_new ("gASQL", str);
	g_free (str);
	gtk_widget_set_usize (conf->app, 600, 388);
	conf->appbar = gnome_appbar_new (TRUE, TRUE, GNOME_PREFERENCES_USER);
	gnome_appbar_set_default (GNOME_APPBAR (conf->appbar), _("Ready."));
	gnome_app_set_statusbar (GNOME_APP (conf->app), conf->appbar);
	gtk_widget_show (conf->appbar);
	gtk_signal_connect (GTK_OBJECT (conf->app), "delete_event",
			    GTK_SIGNAL_FUNC (delete_event_cb), conf);

	/* vbox as the contents of the app */
	vb = gtk_vbox_new (TRUE, 0);
	gnome_app_set_contents (GNOME_APP (conf->app), vb);
	gtk_widget_show (vb);

	/* welcome statement if no connection opened */
	conf->welcome = gtk_packer_new ();
	gtk_packer_set_default_border_width (GTK_PACKER (conf->welcome),
					     GNOME_PAD * 2);
	wid = gtk_label_new (_("You need to configure and open the "
			       "connection\n" "before doing anything else."));
	gtk_packer_add_defaults (GTK_PACKER (conf->welcome), wid,
				 GTK_SIDE_TOP, GTK_ANCHOR_CENTER, 0);
	wid = gnome_pixmap_new_from_file (PIXMAPDIR "/gasql_foot.png");
	gtk_packer_add_defaults (GTK_PACKER (conf->welcome), wid,
				 GTK_SIDE_RIGHT, GTK_ANCHOR_SOUTH, 0);

	gtk_box_pack_start (GTK_BOX (vb), conf->welcome, TRUE, TRUE, 0);
	gtk_widget_show_all (conf->welcome);

	/* hb for the shortcut and the notebook */
	hb = gtk_hbox_new (FALSE, 0);
	gtk_container_set_border_width (GTK_CONTAINER (hb), 0);
	conf->working_box = hb;
	gtk_box_pack_start (GTK_BOX (vb), hb, TRUE, TRUE, 0);

	/* shortcut part */
	sc = gnome_db_shortcut_new ();
	if (register_icon ("gasql_tables", "gasql_tables.png"))
		str = "gasql_tables";
	else
		str = GNOME_STOCK_PIXMAP_BOOK_RED;
	gnome_db_shortcut_append (GNOME_DB_SHORTCUT (sc),
				  _("Tables & Views"),
				  _("Work on tables and views"),
				  str,
				  (GtkSignalFunc) show_tables_page_cb, conf);

	if (register_icon ("gasql_sequences", "gasql_sequences.png"))
		str = "gasql_sequences";
	else
		str = GNOME_STOCK_PIXMAP_BOOK_BLUE;
	gnome_db_shortcut_append (GNOME_DB_SHORTCUT (sc),
				  _("Sequences"),
				  _("Work on sequences"),
				  str,
				  (GtkSignalFunc) show_seqs_page_cb, conf);

	if (register_icon ("gasql_queries", "gasql_queries.png"))
		str = "gasql_queries";
	else
		str = GNOME_STOCK_PIXMAP_BOOK_GREEN;
	gnome_db_shortcut_append (GNOME_DB_SHORTCUT (sc),
				  _("Queries"),
				  _("Manage queries"),
				  str,
				  (GtkSignalFunc) show_queries_page_cb, conf);

	if (register_icon ("gasql_forms", "gasql_forms.png"))
		str = "gasql_forms";
	else
		str = GNOME_STOCK_PIXMAP_BOOK_GREEN;
	gnome_db_shortcut_append (GNOME_DB_SHORTCUT (sc),
				  _("Forms"),
				  _("Manage forms"),
				  str,
				  (GtkSignalFunc) show_forms_page_cb, conf);


	gtk_box_pack_start (GTK_BOX (hb), sc, FALSE, FALSE, 0);
	gtk_widget_show (sc);

	/* notebook */
	conf->nb = gtk_notebook_new ();
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (conf->nb), FALSE);
	gtk_box_pack_start (GTK_BOX (hb), conf->nb, TRUE, TRUE, 0);
	gtk_widget_show (conf->nb);
	gtk_widget_set_usize (GTK_WIDGET (conf->nb), 420, 270);

	/* Tables */
	owid = main_page_table_new (conf);
	gtk_widget_show (owid);

	wid = gtk_label_new (_("Tables & Views"));
	gtk_widget_show (owid);
	gtk_notebook_append_page (GTK_NOTEBOOK (conf->nb), owid, wid);
	conf->tables_page = owid;

	/* Sequences */
	wid = gtk_label_new (_("Sequences"));
	gtk_widget_show (wid);
	owid = main_page_seq_new (conf);
	gtk_widget_show (owid);
	gtk_notebook_append_page (GTK_NOTEBOOK (conf->nb), owid, wid);
	conf->sequences_page = owid;


	/* Queries */
	wid = gtk_label_new (_("Queries"));
	gtk_widget_show (wid);
	owid = main_page_query_new (conf);
	conf->queries_page = owid;
	gtk_widget_show (owid);
	gtk_notebook_append_page (GTK_NOTEBOOK (conf->nb), owid, wid);
	gtk_widget_show (conf->app);

	/* Forms */
	vb = gtk_vbox_new (FALSE, 0);
/* 	wid = gtk_label_new ("This is a test page displaying another instance\n" */
/* 			     "of the queries list page; the two pages can be used at the\n" */
/* 			     "same time, and still be both updated!"); */
/* 	gtk_box_pack_start (GTK_BOX (vb), wid, FALSE, FALSE, GNOME_PAD/2.); */
	 owid = main_page_env_new (conf); 
 	gtk_box_pack_start (GTK_BOX (vb), owid, TRUE, TRUE, GNOME_PAD/2.); 
	gtk_widget_show_all (vb);
	wid = gtk_label_new (_("Forms"));
	gtk_notebook_append_page (GTK_NOTEBOOK (conf->nb), vb, wid);


	/* menus */
	if (register_icon ("gasql_connect", "gasql_connect.png"))
		connect_icon = "gasql_connect";
	else
		connect_icon = GNOME_STOCK_PIXMAP_EXEC;

	if (register_icon ("gasql_disconnect", "gasql_disconnect.png"))
		disconnect_icon = "gasql_disconnect";
	else
		disconnect_icon = GNOME_STOCK_PIXMAP_EXEC;

	if (register_icon ("gasql_connect_small", "gasql_connect_small.png"))
		connect_icon_small = "gasql_connect_small";
	else
		connect_icon_small = GNOME_STOCK_PIXMAP_EXEC;

	if (register_icon ("gasql_disconnect_small", "gasql_disconnect_small.png"))
		disconnect_icon_small = "gasql_disconnect_small";
	else
		disconnect_icon_small = GNOME_STOCK_PIXMAP_EXEC;

	if (register_icon ("gasql_relations", "gasql_rels.png"))
		relations_icon = "gasql_relations";
	else
		relations_icon = GNOME_STOCK_PIXMAP_ABOUT;

	if (register_icon ("gasql_relations_small", "gasql_rels_small.png"))
		relations_icon_small = "gasql_relations_small";
	else
		relations_icon_small = GNOME_STOCK_PIXMAP_ABOUT;

	add_menus (GNOME_APP (conf->app), conf);
	add_toolbar (GNOME_APP (conf->app), conf);

}

static gint
register_icon (gchar * name, gchar * filename)
{
	GnomeStockPixmapEntryPath *new_entry;
	gchar *str;

	new_entry = g_malloc (sizeof (GnomeStockPixmapEntryPath));
	new_entry->type = GNOME_STOCK_PIXMAP_TYPE_PATH;
	new_entry->label = NULL;
	str = gnome_pixmap_file (filename);
	if (!str)
		/* try to add the Pixmap install path to the file */
		str = g_strdup_printf ("%s/%s", PIXMAPDIR, filename);
	new_entry->pathname = str;
	new_entry->width = 0;
	new_entry->height = 0;
	return gnome_stock_pixmap_register (name, GNOME_STOCK_PIXMAP_REGULAR,
					    (GnomeStockPixmapEntry *)
					    new_entry);
}

static void
add_toolbar (GnomeApp * app, ConfManager * conf)
{
	/* toolbar struct */
	GnomeUIInfo toolbar[] = {
		{
		 GNOME_APP_UI_ITEM, N_("Connect"),
		 N_("Opens the SQL server connection"),
		 sql_conn_open_cb, conf, NULL,
		 GNOME_APP_PIXMAP_STOCK, connect_icon,
		 0, 0, NULL},
		{
		 GNOME_APP_UI_ITEM, N_("Disconnect"),
		 N_("Closes the SQL server connection"),
		 sql_conn_close_cb, conf, NULL,
		 GNOME_APP_PIXMAP_STOCK, disconnect_icon,
		 0, 0, NULL},
		{
		 GNOME_APP_UI_ITEM, N_("Relations"), N_("Relations Scheme"),
		 sql_show_relations_cb, conf, NULL,
		 GNOME_APP_PIXMAP_STOCK, relations_icon,
		 0, 0, NULL},
		GNOMEUIINFO_END
	};

	gnome_app_create_toolbar (app, toolbar);
	/* widgets to be shown or hidden depending on the connexion state */
	conf_manager_register_show_on_connect (conf, (toolbar[1]).widget);
	conf_manager_register_show_on_disconnect (conf, (toolbar[0]).widget);
	/*conf_manager_register_sensitive_on_disconnect(conf, (toolbar[2]).widget); */
	conf_manager_register_sensitive_on_connect (conf, (toolbar[2]).widget);	/*3! */
}



static void
add_menus (GnomeApp * app, ConfManager * conf)
{
	/* file menu */
	GnomeUIInfo file_menu[] = {
		GNOMEUIINFO_MENU_NEW_ITEM (N_("_New Workspace"),
					   N_
					   ("Switch to a new workspace to work on "
					    "another database"), file_new_cb,
					   conf),
		GNOMEUIINFO_MENU_OPEN_ITEM (file_open_cb, conf),
		GNOMEUIINFO_MENU_CLOSE_ITEM (file_close_cb, conf),
		GNOMEUIINFO_MENU_SAVE_ITEM (file_save_cb, conf),
		GNOMEUIINFO_MENU_SAVE_AS_ITEM (file_save_as_cb, conf),
		{
		 GNOME_APP_UI_ITEM, N_("Export"), NULL, todo_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_NONE, NULL,
		 0, 0, NULL},
		GNOMEUIINFO_MENU_PRINT_SETUP_ITEM (printer_setup_cb, conf),
		GNOMEUIINFO_MENU_EXIT_ITEM (quit_cb, conf),
		GNOMEUIINFO_END
	};

	/* plugins menu */
	GnomeUIInfo plugins_menu[] = {
		{
		 GNOME_APP_UI_ITEM, N_("Rescan list"), NULL,
		 rescan_display_plugins_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_NONE, NULL,
		 0, 0, NULL},
		GNOMEUIINFO_MENU_PREFERENCES_ITEM (config_display_plugins_cb,
						   conf),
		GNOMEUIINFO_END
	};

	/* users management menu */
	GnomeUIInfo users_menu[] = {
		{
		 GNOME_APP_UI_ITEM, N_("Users"), NULL,
		 users_settings_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_NONE, NULL,
		 0, 0, NULL},
		{
		 GNOME_APP_UI_ITEM, N_("Users groups"), NULL,
		 users_groups_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_NONE, NULL,
		 0, 0, NULL},
		{
		 GNOME_APP_UI_ITEM, N_("Users access settings"), NULL,
		 users_access_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_NONE, NULL,
		 0, 0, NULL},
		GNOMEUIINFO_END
	};


	/* settings menu */
	GnomeUIInfo settings_menu[] = {
		{
		 GNOME_APP_UI_ITEM, N_("Database Manager"),
		 N_("The GNOME-DB manager"),
		 run_gnomedb_manager_cb, conf,
		 NULL, GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_INDEX,
		 0, 0, NULL},
		GNOMEUIINFO_SUBTREE (_("Users management"), &users_menu),
		GNOMEUIINFO_SUBTREE (_("Plugins"), &plugins_menu),
		GNOMEUIINFO_END
	};

	/* help menu */
	GnomeUIInfo help_menu[] = {
		/*GNOMEUIINFO_HELP (_("gASQL")),FIXME: REMOVE COMMENTS*/
		GNOMEUIINFO_SEPARATOR,
		GNOMEUIINFO_MENU_ABOUT_ITEM (about_cb, conf),
		GNOMEUIINFO_END
	};

	/* connection menu */
	GnomeUIInfo conn_menu[] = {
		{
		 GNOME_APP_UI_ITEM, N_("Connection preferences"), NULL,
		 options_config_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_PREFERENCES,
		 0, 0, NULL},
		{
		 GNOME_APP_UI_ITEM, N_("Connect Sql server"), NULL,
		 sql_conn_open_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_STOCK, connect_icon_small, /*GNOME_STOCK_PIXMAP_EXEC,*/
		 0, 0, NULL},
		{
		 GNOME_APP_UI_ITEM, N_("Disconnect Sql server"), NULL,
		 sql_conn_close_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_STOCK, disconnect_icon_small, /*GNOME_STOCK_PIXMAP_EXEC,*/
		 0, 0, NULL},
		GNOMEUIINFO_END
	};

	/* database menu */
	GnomeUIInfo db_menu[] = {
		{
		 GNOME_APP_UI_ITEM, N_("Relations Scheme"), NULL,
		 sql_show_relations_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_STOCK, relations_icon_small,
		 0, 0, NULL},
		GNOMEUIINFO_SEPARATOR,
		{
		 GNOME_APP_UI_ITEM, N_("Integrity check"), NULL,
		 NULL /*integrity_check_cb*/,
		 conf, NULL,
		 GNOME_APP_PIXMAP_NONE, NULL,
		 0, 0, NULL},
		GNOMEUIINFO_SEPARATOR,
		{
		 GNOME_APP_UI_ITEM, N_("System informations"),
		 N_("View the data types, functions, "
		    "aggregates,..."), sql_data_view_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_PROPERTIES,
		 0, 0, NULL},
		{
		 GNOME_APP_UI_ITEM, N_("Refresh memory structure"),
		 N_("Updates the memory "
		    "representation\nof the database's structure"),
		 sql_mem_update_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_REFRESH,
		 0, 0, NULL},
		{
		 GNOME_APP_UI_ITEM, N_("Create a new DB"),
		 N_("Creates a new database"), todo_cb,
		 conf, NULL,
		 GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_NEW,
		 0, 0, NULL},
		GNOMEUIINFO_END
	};

#ifdef debug
	/* debug menu */
	GnomeUIInfo debug_menu[] = {
		{
			GNOME_APP_UI_ITEM, "Dump Queries", NULL, debug_dump_queries_cb,
			conf, NULL,
			GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_CONVERT,
			0, 0, NULL},
		{
			GNOME_APP_UI_ITEM, "Dump DB Structure", NULL, debug_dump_cb,
			conf, NULL,
			GNOME_APP_PIXMAP_STOCK, GNOME_STOCK_PIXMAP_CONVERT,
			0, 0, NULL},
		{
			GNOME_APP_UI_ITEM, "Insert Mark in output", NULL,
			debug_mark_cb,
			conf, NULL,
			GNOME_APP_PIXMAP_NONE, NULL,
			0, 0, NULL},
		{
			GNOME_APP_UI_ITEM, "Remove table 'products'", NULL, debug_remove_table_cb,
			conf, NULL,
			GNOME_APP_PIXMAP_NONE, NULL,
			0, 0, NULL},
		{
			GNOME_APP_UI_ITEM, "Remove 'products->price' field", NULL, debug_remove_field_cb,
			conf, NULL,
			GNOME_APP_PIXMAP_NONE, NULL,
			0, 0, NULL},
		{
			GNOME_APP_UI_ITEM, "Current test", NULL, debug_test_cb,
			conf, NULL,
			GNOME_APP_PIXMAP_NONE, NULL,
			0, 0, NULL},
		GNOMEUIINFO_END
	};
#endif

	/* ALL menus */
	GnomeUIInfo main_menu[] = {
		GNOMEUIINFO_MENU_FILE_TREE (file_menu),
		GNOMEUIINFO_SUBTREE (N_("Connection"), &conn_menu),
		GNOMEUIINFO_SUBTREE (N_("Data Base"), &db_menu),
		GNOMEUIINFO_MENU_SETTINGS_TREE (&settings_menu),
#ifdef debug
		GNOMEUIINFO_SUBTREE ("Debug", &debug_menu),
#endif
		GNOMEUIINFO_MENU_HELP_TREE (help_menu),
		GNOMEUIINFO_END
	};

	gnome_app_create_menus (app, main_menu);

	/* widgets to be shown or hidden depending on the connexion state */
	conf_manager_register_show_on_connect (conf, (db_menu[0]).widget);
	conf_manager_register_show_on_connect (conf, (db_menu[1]).widget);
	conf_manager_register_show_on_connect (conf, (db_menu[2]).widget);
	conf_manager_register_show_on_connect (conf, (db_menu[3]).widget);
	conf_manager_register_show_on_connect (conf, (db_menu[4]).widget);
	conf_manager_register_show_on_connect (conf, (db_menu[5]).widget);
	conf_manager_register_show_on_disconnect (conf, (db_menu[6]).widget);

	conf_manager_register_sensitive_on_disconnect (conf,
						     (conn_menu[0]).widget);
	conf_manager_register_sensitive_on_disconnect (conf,
						     (conn_menu[1]).widget);
	conf_manager_register_sensitive_on_connect (conf,
						  (conn_menu[2]).widget);
	conf_manager_register_sensitive_on_disconnect (conf,
						     (file_menu[1]).widget);
	conf_manager_register_sensitive_on_disconnect (conf,
						     (file_menu[2]).widget);
	conf_manager_register_sensitive_on_connect (conf,
						  (file_menu[5]).widget);
}

#ifdef debug
/* DEBUG FUNCTIONS */

/* in no debug mode, this is a static function of the Database object */
extern void database_load_clean_unused_tables_views (Database * db, ServerAccess * srv);

static void
debug_remove_table_cb (GtkWidget * widget, ConfManager * conf)
{
	GSList *list;
	DbTable *products;

	list = conf->db->tables;
	while (list) {
		DB_ITEM (list->data)->updated = TRUE;
		list = g_slist_next (list);
	}

	products = database_find_table_from_name (conf->db, "products");
	if (products) {
		g_print ("\n\n--------------- removing table products ---------------   \n\n");
		DB_ITEM (products)->updated = FALSE;
	}
	database_load_clean_unused_tables_views (conf->db, conf->srv);
}

/* in no debug mode, this is a static function of the Database object */
extern void db_table_clean_fields (Database * db, DbTable * t);

static void
debug_remove_field_cb (GtkWidget * widget, ConfManager * conf)
{
	DbTable *products;

	products = database_find_table_from_name (conf->db, "products");
	if (products) {
		DbField *price;

		price = db_table_find_field_by_name (products, "price");
		if (price) {
			GSList *list;

			g_print ("\n\n--------------- removing products'price field ---------   \n\n");
			list = products->fields;
			while (list) {
				DB_ITEM (list->data)->updated = TRUE;
				list = g_slist_next (list);
			}

			DB_ITEM (price)->updated = FALSE;

			/* now apply the Database's procedure */
			db_table_clean_fields (conf->db, products);
		}
	}
}

static void
debug_test_cb (GtkWidget * widget, ConfManager * conf)
{

}

static void
debug_dump_cb (GtkWidget * widget, ConfManager * conf)
{
	database_dump_tables (conf->db);
	database_dump_links (conf->db);
	server_data_type_show_types (conf->srv->data_types);
	server_function_show_functions (conf->srv->data_functions);
	server_aggregate_show_aggregates (conf->srv->data_aggregates);
}

static void
debug_dump_queries_cb (GtkWidget * widget, ConfManager * conf)
{
	g_print ("==== CONF MANAGER DEBUG DUMP ====\n");
	g_print ("QUERY serial id = %d\n", conf->id_serial);
	g_print ("Clipboard str=%s\n",conf->clipboard_str); 
	query_dump_contents (QUERY (conf->top_query));
}

static void
debug_mark_cb (GtkWidget * widget, ConfManager * conf)
{
	static int mark = 0;

	g_print ("\n\n" D_COL_H0 "-------------------------------------------------------" D_COL_NOR 
		 " %d\n\n", mark++);
}

/* END OF DEBUG FUNCTIONS */
#endif

static void
todo_cb (GtkWidget * widget, ConfManager * conf)
{
	gnome_ok_dialog_parented (_("Function still non implemented\n"),
				  GTK_WINDOW (conf->app));
}

static void
about_cb (GtkWidget * widget, ConfManager * conf)
{
	static GtkWidget *dialog = NULL;

	if (dialog != NULL) {
		g_assert (GTK_WIDGET_REALIZED (dialog));
		gdk_window_show (dialog->window);
		gdk_window_raise (dialog->window);
	}
	else {
		const gchar *authors[] = {
			"Vivien Malerba <malerba@gnome-db.org>",
			"Fernando Martins <fmartins@hetnet.nl>",
			"Gerhard Dieringer <DieringG@eba-haus.de> German translations",
			"Mauro Colorio <linuxbox@interfree.it> Italian translations",
			"Ali Pakkan <apakkan@hotmail.com> Turk translations",
			"Christian Rose <menthos@menthos.com> Swedish translations",
			"Martin Lacko <lacko@host.sk> Slovak translation",
			"Valek Filippov <frob@df.ru> Russian translation",
			NULL
		};
		dialog = gnome_about_new (_("gASQL"), VERSION,
					  "(C) 1999-2001 Vivien Malerba",
					  authors,
					  _
					  ("A Database admin tool for any SQL database "
					   "accessible with the gnome-db module."),
					  PIXMAPDIR "/gasql.png");
		gtk_signal_connect (GTK_OBJECT (dialog), "destroy",
				    GTK_SIGNAL_FUNC (gtk_widget_destroyed),
				    &dialog);
		gnome_dialog_set_parent (GNOME_DIALOG (dialog),
					 GTK_WINDOW (conf->app));
		gtk_widget_show (dialog);
	}
}


static gint
delete_event_cb (GtkWidget * wid, GdkEvent * event, ConfManager * conf)
{
	/* quit with confirmation */
	quit_cb (NULL, conf);

	/* we don't want the "destroy" signal to be emitted now */
	return TRUE;
}


/* CB to display sth when updating the DB structure */
void
progress_cb (GtkObject * obj, gchar * msg, guint now, guint total,
	     ConfManager * conf)
{
	if (msg) {
		gnome_appbar_set_status (GNOME_APPBAR (conf->appbar), msg);
		if (total == 0)
			total = now + 10;	/* FIXME! */
		gnome_appbar_set_progress (GNOME_APPBAR (conf->appbar),
					   (gfloat) now / (gfloat) total);
	}
	else {
		gnome_appbar_clear_stack (GNOME_APPBAR (conf->appbar));
		gnome_appbar_set_progress (GNOME_APPBAR (conf->appbar), 0.);
	}
	gtk_main_iteration ();
}


static void 
widgets_conn_management_cb (ServerAccess *srv, ConfManager *conf)
{
	GSList *list;

	list = conf->widgets_sensitive_on_connect;
	while (list) {
		if (server_access_is_open (conf->srv))
			gtk_widget_set_sensitive (GTK_WIDGET (list->data),
						  TRUE);
		else
			gtk_widget_set_sensitive (GTK_WIDGET (list->data),
						  FALSE);
		list = g_slist_next (list);
	}
	list = conf->widgets_sensitive_on_disconnect;
	while (list) {
		if (server_access_is_open (conf->srv))
			gtk_widget_set_sensitive (GTK_WIDGET (list->data),
						  FALSE);
		else
			gtk_widget_set_sensitive (GTK_WIDGET (list->data),
						  TRUE);
		list = g_slist_next (list);
	}
	list = conf->widgets_show_on_connect;
	while (list) {
		if (server_access_is_open (conf->srv))
			gtk_widget_show (GTK_WIDGET (list->data));
		else
			gtk_widget_hide (GTK_WIDGET (list->data));
		list = g_slist_next (list);
	}
	list = conf->widgets_show_on_disconnect;
	while (list) {
		if (server_access_is_open (conf->srv))
			gtk_widget_hide (GTK_WIDGET (list->data));
		else
			gtk_widget_show (GTK_WIDGET (list->data));
		list = g_slist_next (list);
	}
}

static void access_db_link_cb (Database * obj, ConfManager * conf);
static void conf_update_save_flag (GtkObject * widget, ConfManager * conf);
static void query_created_cb (Query *top_query, Query *new_query, ConfManager * conf);
static void query_dropped_cb (Query *top_query, Query *new_query, ConfManager * conf);
static void 
objects_conn_management_cb (ServerAccess *srv, ConfManager *conf)
{
	if (server_access_is_open (conf->srv)) {
		guint id;

		g_assert (!conf->db);
		conf->db = DATABASE (database_new (conf->srv));

		gtk_signal_connect (GTK_OBJECT (conf->db), "fault", 
				    GTK_SIGNAL_FUNC (access_db_link_cb), conf); 
		gtk_signal_connect (GTK_OBJECT (conf->db), "updated",
				    GTK_SIGNAL_FUNC (conf_update_save_flag),
				    conf);
		gtk_signal_connect (GTK_OBJECT (conf->db), "progress",
				    GTK_SIGNAL_FUNC (progress_cb), conf);

#ifdef debug_signal
		g_print (">> 'DATABASE_ADDED' from objects_conn_management_cb\n");
#endif
		gtk_signal_emit (GTK_OBJECT (conf), conf_manager_signals[DATABASE_ADDED], conf->db);
#ifdef debug_signal
		g_print ("<< 'DATABASE_ADDED' from objects_conn_management_cb\n");
#endif	

		g_assert (!conf->top_query);
		id = conf->id_serial; /* save the original counter to restore it after query creation */
		conf->id_serial = 0;
		conf->top_query = query_new ("Top Query", NULL, conf);
		if (id != 0)
			conf->id_serial = id;
#ifdef debug_signal
		g_print (">> 'QUERY_ADDED' from objects_conn_management_cb\n");
#endif
		gtk_signal_emit (GTK_OBJECT (conf), conf_manager_signals[QUERY_ADDED], conf->top_query);
#ifdef debug_signal
		g_print ("<< 'QUERY_ADDED' from objects_conn_management_cb\n");
#endif	
		gtk_signal_connect (GTK_OBJECT (conf->top_query), "query_created",
				    GTK_SIGNAL_FUNC (query_created_cb), conf);
		gtk_signal_connect (GTK_OBJECT (conf->top_query), "query_dropped",
				    GTK_SIGNAL_FUNC (query_dropped_cb), conf);
	}
	else {
		g_assert (conf->db);
#ifdef debug_signal
		g_print (">> 'DATABASE_REMOVED' from objects_conn_management_cb\n");
#endif
		gtk_signal_emit (GTK_OBJECT (conf), conf_manager_signals[DATABASE_REMOVED], conf->db);
#ifdef debug_signal
		g_print ("<< 'DATABASE_REMOVED' from objects_conn_management_cb\n");
#endif	
		gtk_object_unref (GTK_OBJECT (conf->db));
		conf->db = NULL;

		g_assert (conf->top_query);
#ifdef debug_signal
		g_print (">> 'QUERY_REMOVED' from objects_conn_management_cb\n");
#endif
		gtk_signal_emit (GTK_OBJECT (conf), conf_manager_signals[QUERY_REMOVED], conf->top_query);
#ifdef debug_signal
		g_print ("<< 'QUERY_REMOVED' from objects_conn_management_cb\n");
#endif			
		gtk_object_unref (conf->top_query);
		conf->top_query = NULL;
	}
}


static void env_created_cb (Query *query, GtkObject *env, ConfManager * conf);
static void env_dropped_cb (Query *query, GtkObject *env, ConfManager * conf);
static void env_modified_cb (QueryEnv *qev, ConfManager *conf);
static void 
query_created_cb (Query *top_query, Query *new_query, ConfManager * conf)
{
	conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'QUERY_ADDED' from query_created_cb (ConfManager)\n");
#endif
	gtk_signal_emit (GTK_OBJECT (conf), conf_manager_signals[QUERY_ADDED], new_query);
#ifdef debug_signal
	g_print ("<< 'QUERY_ADDED' from query_created_cb (ConfManager)\n");
#endif	

	/* to manage the QueryEnvs */
	gtk_signal_connect (GTK_OBJECT (new_query), "env_created",
			    GTK_SIGNAL_FUNC (env_created_cb), conf);
	gtk_signal_connect (GTK_OBJECT (new_query), "env_dropped",
			    GTK_SIGNAL_FUNC (env_dropped_cb), conf);


	/* the ConfManager creates a new QueryEnv if there is none and
           if we have a non sub query */
	if (!conf->loading_in_process &&
	    !new_query->envs && (new_query->parent == QUERY (conf->top_query))) {
		GtkObject *env;
		env = query_env_new (new_query);
		/* tell that we have created the Env here */
		conf->created_envs = g_slist_append (conf->created_envs, env);
		gtk_signal_connect (GTK_OBJECT (env), "modified",
				    GTK_SIGNAL_FUNC (env_modified_cb), conf);
	}
}

static void 
query_dropped_cb (Query *top_query, Query *query, ConfManager * conf)
{
	conf->save_up_to_date = FALSE;
#ifdef debug_signal
	g_print (">> 'QUERY_REMOVED' from query_dropped_cb (ConfManager)\n");
#endif
	gtk_signal_emit (GTK_OBJECT (conf), conf_manager_signals[QUERY_REMOVED], query);
#ifdef debug_signal
	g_print ("<< 'QUERY_REMOVED' from query_dropped_cb (ConfManager)\n");
#endif	

	/* If the only QueryEnv is the one we created in the ConfManager, and has not
	   been modified, then remove it */
	if (query->envs && 
	    (g_slist_length (query->envs) == 1) &&
	    g_slist_find (conf->created_envs, query->envs->data)) 
		query_del_env (query, GTK_OBJECT (query->envs->data));

	/* to manage the QueryEnvs */
	gtk_signal_disconnect_by_func (GTK_OBJECT (query), 
				       GTK_SIGNAL_FUNC (env_created_cb), conf);
	gtk_signal_disconnect_by_func (GTK_OBJECT (query), 
				       GTK_SIGNAL_FUNC (env_dropped_cb), conf);

}

static void env_created_cb (Query *query, GtkObject *env, ConfManager * conf)
{
	QueryEnv *qev;
	GSList *list;

	g_assert (query);
	g_assert (env);
	g_assert (IS_QUERY (query));
	g_assert (IS_QUERY_ENV (env));

	qev = QUERY_ENV (env);
	if (qev->q && (qev->q != query)) {
		g_warning ("QueryEnv added to a query but not referencing it, corrected (%s at %d)\n",
			   __FILE__, __LINE__);
		qev->q = query;
	}

#ifdef debug_signal
	g_print (">> 'ENV_ADDED' from env_added_cb (ConfManager)\n");
#endif
	gtk_signal_emit (GTK_OBJECT (conf), conf_manager_signals[ENV_ADDED], env);
#ifdef debug_signal
	g_print ("<< 'ENV_ADDED' from env_added_cb (ConfManager)\n");
#endif	

	/* if the env belongs to a query for which the ConfManager had made a QueryEnv qev, then qev
	   is not considered anymore as having been created by the ConfManager */
	list = conf->created_envs;
	while (list) {
		qev = QUERY_ENV (list->data);
		if ((qev->q == query) && (qev != QUERY_ENV (env))) {
			list = g_slist_next (list);
			conf->created_envs = g_slist_remove (conf->created_envs, qev);
		}
		else
			list = g_slist_next (list);
	}
}

static void env_dropped_cb (Query *query, GtkObject *env, ConfManager * conf)
{
	g_assert (env);
	g_assert (IS_QUERY_ENV (env));

#ifdef debug_signal
	g_print (">> 'ENV_REMOVED' from env_dropped_cb (ConfManager)\n");
#endif
	gtk_signal_emit (GTK_OBJECT (conf), conf_manager_signals[ENV_REMOVED], env);
#ifdef debug_signal
	g_print ("<< 'ENV_REMOVED' from env_dropped_cb (ConfManager)\n");
#endif

	/* if this env was one created by the ConfManager, remove it from the list */
	if (g_slist_find (conf->created_envs, env)) {
		conf->created_envs = g_slist_remove (conf->created_envs, env);
		gtk_signal_disconnect_by_func (GTK_OBJECT (env), 
					       GTK_SIGNAL_FUNC (env_modified_cb), conf);
	}
}

static void 
env_modified_cb (QueryEnv *qev, ConfManager *conf)
{
	/* if the env is still in the created_envs list (it should be because we are here)
	   then we disconnect the listeneing to the "modified" signal and 
	   remove the env from the list */
	if (g_slist_find (conf->created_envs, qev)) {
		gtk_signal_disconnect_by_func (GTK_OBJECT (qev), 
					       GTK_SIGNAL_FUNC (env_modified_cb), conf);
		conf->created_envs = g_slist_remove (conf->created_envs, qev);
	}
}

/* if an error occurs in Database ("fault" signal) */
static void
access_db_link_cb (Database * obj, ConfManager * conf)
{
	GnomeDialog *dlg;
	dlg = GNOME_DIALOG (_
			    ("An error has occured in the internal representation "
			     "\nof the database structure. The application will "
			     "now be closed.\n"));
	server_access_free (conf->srv);
	gnome_dialog_run_and_close (dlg);
	g_print ("Faulty Database: Exiting...\n");
	exit (1);
}

static void 
conf_update_save_flag (GtkObject * widget, ConfManager * conf)
{
	conf->save_up_to_date = FALSE;
}


static void
conf_manager_destroy (GtkObject * object)
{
	ConfManager *conf;
	GtkObjectClass *parent_class = NULL;

	parent_class = gtk_type_class (gtk_object_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CONF_MANAGER (object));

	conf = CONF_MANAGER (object);

	if (conf->srv) {
		gtk_object_unref (GTK_OBJECT (conf->srv));
		conf->srv = NULL;
	}

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void destroy_widget_connect_cb (GtkObject * obj, ConfManager * conf);

void 
conf_manager_register_show_on_connect (ConfManager *conf, GtkWidget * wid)
{
	conf->widgets_show_on_connect =
		g_slist_append (conf->widgets_show_on_connect, wid);
	gtk_signal_connect (GTK_OBJECT (wid), "destroy",
			    GTK_SIGNAL_FUNC (destroy_widget_connect_cb), conf);
	widgets_conn_management_cb (conf->srv, conf);
}

void 
conf_manager_register_show_on_disconnect (ConfManager *conf, GtkWidget * wid)
{
	conf->widgets_show_on_disconnect =
		g_slist_append (conf->widgets_show_on_disconnect, wid);
	gtk_signal_connect (GTK_OBJECT (wid), "destroy",
			    GTK_SIGNAL_FUNC (destroy_widget_connect_cb), conf);
	widgets_conn_management_cb (conf->srv, conf);
}

void 
conf_manager_register_sensitive_on_connect (ConfManager *conf, GtkWidget * wid)
{
	conf->widgets_sensitive_on_connect =
		g_slist_append (conf->widgets_sensitive_on_connect, wid);
	gtk_signal_connect (GTK_OBJECT (wid), "destroy",
			    GTK_SIGNAL_FUNC (destroy_widget_connect_cb), conf);
	widgets_conn_management_cb (conf->srv, conf);
}

void 
conf_manager_register_sensitive_on_disconnect (ConfManager *conf, GtkWidget * wid)
{
	conf->widgets_sensitive_on_disconnect =
		g_slist_append (conf->widgets_sensitive_on_disconnect, wid);
	gtk_signal_connect (GTK_OBJECT (wid), "destroy",
			    GTK_SIGNAL_FUNC (destroy_widget_connect_cb), conf);
	widgets_conn_management_cb (conf->srv, conf);
}

static void
destroy_widget_connect_cb (GtkObject * obj, ConfManager *conf)
{
	gboolean found = FALSE;

	if (g_slist_find (conf->widgets_sensitive_on_connect, obj)) {
		conf->widgets_sensitive_on_connect = g_slist_remove (conf->widgets_sensitive_on_connect,
								  obj);
		found = TRUE;
	}

	if (!found && g_slist_find (conf->widgets_sensitive_on_disconnect, obj)) {
		conf->widgets_sensitive_on_disconnect = g_slist_remove (conf->widgets_sensitive_on_disconnect,
								     obj);
		found = TRUE;
	}

	if (!found && g_slist_find (conf->widgets_show_on_connect, obj)) {
		conf->widgets_show_on_connect = g_slist_remove (conf->widgets_show_on_connect,
							     obj);
		found = TRUE;
	}

	if (!found && g_slist_find (conf->widgets_show_on_disconnect, obj)) {
		conf->widgets_show_on_disconnect =	g_slist_remove (conf->widgets_show_on_disconnect,
								obj);
		found = TRUE;
	}
}


/* returns an unique number for this conf structure */
guint 
conf_manager_get_id_serial (ConfManager *conf)
{
	guint ret;

	ret = conf->id_serial++;
	return ret;
}

/* returns a string for the main window's title bar which will have to be freed */
gchar *
conf_manager_get_title (ConfManager *conf)
{
	gchar *str;

	if (conf->working_file)
		str = g_strdup_printf (_("gASQL Version %s - %s"), VERSION,
				       conf->working_file);
	else
		str = g_strdup_printf (_("gASQL Version %s - No file"),
				       VERSION);
	return str;
}


/* Clipboard management */
static void free_clipboard (ConfManager *conf);

void      
conf_manager_set_clipboard_str (ConfManager *conf, gchar *str)
{
	GtkObject *obj = NULL;
	g_return_if_fail (conf);
	g_return_if_fail (IS_CONF_MANAGER (conf));
	


	free_clipboard (conf);

	conf->clipboard_str = g_strdup (str);

	/* try to find an object from the str */
	obj = GTK_OBJECT (query_find_from_xml_name (conf, NULL, str));
	if (obj) {
		conf->clipboard_obj = query_new_copy (QUERY (obj));
		gtk_object_ref (conf->clipboard_obj);
		if (QUERY (conf->clipboard_obj)->parent) 
			query_del_sub_query (QUERY (conf->clipboard_obj)->parent,
					     QUERY (conf->clipboard_obj));
	}
	/* others cases of objects... */
	
	/* FIXME: think about a signal to emit */
}

gchar *
conf_manager_get_clipboard_str (ConfManager *conf)
{
	g_return_val_if_fail (conf, NULL);
	g_return_val_if_fail (IS_CONF_MANAGER (conf), NULL);

	return conf->clipboard_str;
}

void      
conf_manager_set_clipboard_object (ConfManager *conf, GtkObject *obj)
{
	g_return_if_fail (conf);
	g_return_if_fail (IS_CONF_MANAGER (conf));
	g_return_if_fail (GTK_IS_OBJECT (obj));

	free_clipboard (conf);

	conf->clipboard_obj = obj;
	gtk_object_ref (conf->clipboard_obj);
	
	/* FIXME: think about a signal to emit */
}

GtkObject *
conf_manager_get_clipboard_object (ConfManager *conf)
{
	g_return_val_if_fail (conf, NULL);
	g_return_val_if_fail (IS_CONF_MANAGER (conf), NULL);

	return conf->clipboard_obj;
}

static void 
free_clipboard (ConfManager *conf)
{
	if (conf->clipboard_str) {
		g_free (conf->clipboard_str);
		conf->clipboard_str = NULL;
	}

	if (conf->clipboard_obj) {
		gtk_object_unref (conf->clipboard_obj);
		conf->clipboard_obj = NULL;
	}

	/* FIXME: think about a signal to emit */
}
