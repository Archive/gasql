/* server-rs.c
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <stdio.h>
#include <config.h>
#include "server-rs.h"
#include <string.h>

static void server_resultset_class_init (ServerResultsetClass * class);
static void server_resultset_init (ServerResultset * srv);
static void server_resultset_destroy (GtkObject * object);
static void server_resultset_res_destroyed (GdaRecordset * res,
					 ServerResultset * qres);
static void rs_row_changed (GdaRecordset * rs, ServerResultset * qres);

/* signals */
enum
{
	DUMMY,
	LAST_SIGNAL
};

static gint server_resultset_signals[LAST_SIGNAL] = { 0 };


/* get a pointer to the parents to be able to call their destructor */
static GtkObject *parent_class = NULL;


guint
server_resultset_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"ServerRseultset",
			sizeof (ServerResultset),
			sizeof (ServerResultsetClass),
			(GtkClassInitFunc) server_resultset_class_init,
			(GtkObjectInitFunc) server_resultset_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_object_get_type (), &f_info);
	}

	return f_type;
}

static void
server_resultset_class_init (ServerResultsetClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	server_resultset_signals[DUMMY] =
		gtk_signal_new ("dummy",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (ServerResultsetClass, dummy),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	gtk_object_class_add_signals (object_class, server_resultset_signals,
				      LAST_SIGNAL);
	class->dummy = NULL;
	object_class->destroy = server_resultset_destroy;
}

static void
server_resultset_init (ServerResultset * qres)
{
	qres->query = g_string_new ("");
	qres->recset = NULL;
	qres->nbtuples = 0;
	qres->current_row = 0;
}

GtkObject *
server_resultset_new (GdaRecordset * res)
{
	GtkObject *obj;
	gulong pos /*, nb */ ;

	ServerResultset *qres;
	obj = gtk_type_new (server_resultset_get_type ());
	qres = SERVER_RESULTSET (obj);
	qres->recset = res;
	gtk_signal_connect_while_alive (GTK_OBJECT (res), "destroy",
					GTK_SIGNAL_FUNC
					(server_resultset_res_destroyed), qres,
					GTK_OBJECT (res));

	/* FIXME: use the gda_connection_supports function */
	/* setting the number of tuples to the correct value */
	/* pos = gda_recordset_move_last(res); */
/*   if (pos == GDA_RECORDSET_INVALID_POSITION) */
/*     qres->nbtuples = 0; */
/*   else  */
/*     { */
/*       nb = pos; */
/*       pos = gda_recordset_move_first(res); */
/*       if (pos == GDA_RECORDSET_INVALID_POSITION) */
/* 	qres->nbtuples = 0; */
/*       else */
/* 	qres->nbtuples = nb-pos+1; */
/*     } */
	pos = gda_recordset_move_first (res);
	if (pos == GDA_RECORDSET_INVALID_POSITION)
		qres->nbtuples = 0;
	else
		qres->nbtuples = res->affected_rows;
	qres->current_row = pos;

	/* whenever someone changes the position, we are informed */
	gtk_signal_connect (GTK_OBJECT (res), "row_changed",
			    GTK_SIGNAL_FUNC (rs_row_changed), obj);
	/*fprintf(stderr, "Init current position: %d\n", qres->current_row); */
	return obj;
}

static void
rs_row_changed (GdaRecordset * rs, ServerResultset * qres)
{
	qres->current_row = rs->current_index;
	/*fprintf(stderr, "Current row is now %d\n", qres->current_row); */
}

static void
server_resultset_res_destroyed (GdaRecordset * res, ServerResultset * qres)
{
	/* no more data available */
	qres->recset = NULL;
	qres->nbtuples = 0;
	qres->current_row = 0;
}

static void
server_resultset_destroy (GtkObject * object)
{
	ServerResultset *qres;

	parent_class = gtk_type_class (gtk_object_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_SERVER_RESULTSET (object));

	qres = SERVER_RESULTSET (object);
	g_string_free (qres->query, TRUE);
	if (qres->recset)
		gda_recordset_free (qres->recset);
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

void
server_resultset_free (ServerResultset * qres)
{
	gtk_object_destroy (GTK_OBJECT (qres));
}

gint
server_resultset_get_nbtuples (ServerResultset * qres)
{
	return qres->nbtuples;
}

gint
server_resultset_get_nbcols (ServerResultset * qres)
{
	if (qres->recset)
		return gda_recordset_rowsize (qres->recset);
	else
		return 0;
}


/* One function to set the current row of the Recordset */
static void
set_recset_pos (ServerResultset * qres, guint row)
{
	gulong pos;

	/* g_print("-----> set_recset_pos current row: %d and want: %d\n",  */
/* 	  (gint)qres->current_row, */
/* 	  row); */

	if (((gint) row - (gint) qres->current_row < 0) &&
	    (qres->recset->cursor_type == GDA_OPEN_FWDONLY)) {
		/* g_print("\t before rewind row is (%p)\n", qres->recset->current_row); */
		gda_recordset_move_first (qres->recset);
		qres->current_row = 1;
		/* g_print("\t rewinded to row 1 (%p)\n", qres->recset->current_row); */
	}
	if (row - qres->current_row > 0) {
		/* g_print("\t before moving row is (%p)\n", qres->recset->current_row); */
/*       g_print("\t moving forward %ld\n", row-qres->current_row); */
		pos = gda_recordset_move (qres->recset,
					  row - qres->current_row, 0);

		if (pos == GDA_RECORDSET_INVALID_POSITION) {
			g_error ("There was an error fetching the row OR "
				 "row out of range EXIT\n");
			exit (1);
		}
	}
	/* g_print("\t Current row is now %d (%p, current_index:%d)\n", qres->current_row, */
/* 	  qres->recset->current_row, qres->recset->current_index); */
}

/* warning: rows start at 1 and cols start at 0 
 * ANY returned string MUST then be freed!
 */
gchar *
server_resultset_get_item (ServerResultset * qres, guint row, guint col)
{
	GdaField *field;

	if ((row < 0) || (row > qres->nbtuples) ||
	    (col < 0) || (col > gda_recordset_rowsize (qres->recset)))
		return NULL;
	if (row == 0)
		g_warning ("Warning: you asked for row 0!\n");

	if (qres->recset) {
		if (qres->current_row != row)
			set_recset_pos (qres, row);

		field = gda_recordset_field_idx (qres->recset, col);
		if (field) {
			return server_resultset_stringify (field);	/* warning: to dealloc! */
		}
		else
			return NULL;
	}
	else
		return NULL;
}

/* warning: rows start at 1 and cols start at 0 */
GdaField *
server_resultset_get_gdafield (ServerResultset * qres, guint row, guint col)
{
	GdaField *field = NULL;

	if ((row < 0) || (row > qres->nbtuples) ||
	    (col < 0) || (col > gda_recordset_rowsize (qres->recset)))
		return NULL;
	if (row == 0)
		g_warning ("Warning: you asked for row 0!\n");

	if (qres->recset) {
		if (qres->current_row != row)
			set_recset_pos (qres, row);

		field = gda_recordset_field_idx (qres->recset, col);
	}

	return field;
}

gchar *
server_resultset_get_col_name (ServerResultset * qres, gint col)
{
	GdaField *field;
	if (qres->recset) {
		if ((field = gda_recordset_field_idx (qres->recset, col))) {
			return field->attributes->name;
		}
		else
			return NULL;
	}
	return NULL;
}

void
server_resultset_dump (ServerResultset * qres)
{
	guint nbtup, nbcols;
	guint *max, i, j, total;
	gchar *str;

	nbtup = server_resultset_get_nbtuples (qres);
	nbcols = server_resultset_get_nbcols (qres);

	g_print ("-- ServerResultset DUMP --\n");
	g_print ("* nbtuples: %d\n", nbtup);
	g_print ("* nbcols:   %d\n", nbcols);
	if (qres->recset) {
		max = (guint *) g_malloc (sizeof (guint) * nbcols);
		for (i = 0; i < nbcols; i++)
			max[i] = 0;
		/* max size for the columns */
		for (j = 0; j < nbtup; j++)
			for (i = 0; i < nbcols; i++) {
				str = server_resultset_get_item (qres, j, i);
				if (strlen (str) > max[i])
					max[i] = strlen (str);
				g_free (str);
			}
		total = 1;
		for (i = 0; i < nbcols; i++)
			total += max[i] + 3;

		/* top line */
		g_print ("/");
		for (i = 0; i < total - 2; i++)
			g_print ("-");
		g_print ("\\\n");

		/* printing */
		for (j = 0; j < nbtup; j++) {
			for (i = 0; i < nbcols; i++) {
				str = server_resultset_get_item (qres, j, i);
				g_print ("| %-*s ", max[i], str);
				g_free (str);
			}
			g_print ("|\n");
		}

		/* bottom line */
		g_print ("\\");
		for (i = 0; i < total - 2; i++)
			g_print ("-");
		g_print ("/\n");

		g_free (max);
	}
}

/* creates a GSList with each node pointing to a string which appears
   in the tabular.
   the created list will have to be freed, as well as its strings contents.
   WARNING: only on dimension tabulars allowed! */
GSList *
server_resultset_convert_tabular_to_list (gchar * tab)
{
	GSList *list;
	gchar *wtab, *ptr, *ptr2;

	list = NULL;
	if (tab && (*tab == '{') && (tab[strlen (tab) - 1] == '}')) {
		wtab = g_strdup (tab);
		wtab[strlen (wtab) - 1] = '\0';
		ptr = wtab + 1;
		ptr = strtok (ptr, ",");
		while (ptr) {
			ptr2 = g_strdup (ptr);
			list = g_slist_append (list, ptr2);
			ptr = strtok (NULL, ",");
		}
		g_free (wtab);
	}
	else {
		if (!tab || (*tab != '\0'))
			g_print ("Field %s is not a tabular\n", tab);
	}

	return list;
}

gchar *
server_resultset_stringify (GdaField * field)
{
	gchar *retval;

	g_return_val_if_fail (GDA_IS_FIELD (field), 0);

	if (gda_field_is_null (field))
		retval = NULL;
	else
		retval = gda_stringify_value (NULL, 0, field);

	return retval;
}
