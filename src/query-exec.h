/* query-exec.h
 *
 * Copyright (C) 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY_EXEC__
#define __QUERY_EXEC__

#include <gnome.h>
#include "query-env.h"
#include "server-rs.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define QUERY_EXEC(obj)          GTK_CHECK_CAST (obj, query_exec_get_type(), QueryExec)
#define QUERY_EXEC_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, query_exec_get_type (), QueryExecClass)
#define IS_QUERY_EXEC(obj)       GTK_CHECK_TYPE (obj, query_exec_get_type ())

	typedef struct _ExecEntity ExecEntity;
	typedef struct _QueryExec QueryExec;
	typedef struct _QueryExecClass QueryExecClass;

	struct _ExecEntity
	{
		GtkWidget *entity;
		GtkWidget *dlg;
	};

	/* struct for the object's data */
	struct _QueryExec
	{
		GtkObject object;

		Query *q;	/* q is a copy of env->q, so here is a ref to the q to be used */
		QueryEnv *env;
		ServerResultset *qres;

		/* private data */
		GtkWidget *missing_data_dlg;	/* dialog to ask for missing values */
		GSList *missing_data;
		gchar *query;	/* query once it has been created */
		GSList *exec_entities;
	};

	/* struct for the object's class */
	struct _QueryExecClass
	{
		GtkObjectClass parent_class;

		void (*not_valid) (QueryExec * qe);
	};

	/* generic widget's functions */
	guint query_exec_get_type (void);
	GtkObject *query_exec_new (QueryEnv * env);

	/* services */
	void query_exec_run (QueryExec * exec);
	GtkWidget *query_exec_get_consult_widget (QueryExec * exec);
	GtkWidget *query_exec_get_consult_grid (QueryExec * exec);
	GtkWidget *query_exec_get_insert_widget (QueryExec * exec);

	/* helper function to build forms or grids */
	GtkWidget *query_exec_get_action_buttons (QueryExec * exec);
#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
