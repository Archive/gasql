/* session.c
 *
 * Copyright (C) 1999 - 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "session.h"
#include "conf-manager.h"

gint
session_die_cb (GnomeClient * client, gpointer client_data)
{
	gtk_exit (0);

	return FALSE;
}

gint
session_save_state_cb (GnomeClient * client,
		       gint phase,
		       GnomeRestartStyle restart_style,
		       gint shutdown,
		       GnomeInteractStyle interact_style,
		       gint fast, gpointer client_data)
{
	gchar *argv[3];
	gint x, y, w, h;
	ConfManager *conf = CONF_MANAGER (client_data);

	gdk_window_get_geometry (conf->app->window, &x, &y, &w, &h, NULL);
	/* Save the state using gnome-config stuff. */
	gnome_config_push_prefix (gnome_client_get_config_prefix (client));

	gnome_config_set_int ("Geometry/x", x);
	gnome_config_set_int ("Geometry/y", y);
	gnome_config_set_int ("Geometry/w", w);
	gnome_config_set_int ("Geometry/h", h);

	gnome_config_pop_prefix ();
	gnome_config_sync ();
	gnome_client_set_clone_command (client, 1, argv);
	gnome_client_set_restart_command (client, 1, argv);

	return TRUE;
}
