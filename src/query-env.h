/* query-env.h
 *
 * Copyright (C) 2001 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY_ENV__
#define __QUERY_ENV__

#include <gnome.h>
#include "query.h"
#include "conf-manager.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define QUERY_ENV(obj)          GTK_CHECK_CAST (obj, query_env_get_type(), QueryEnv)
#define QUERY_ENV_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, query_env_get_type (), QueryEnvClass)
#define IS_QUERY_ENV(obj)       GTK_CHECK_TYPE (obj, query_env_get_type ())


	typedef struct _QueryEnv QueryEnv;
	typedef struct _QueryEnvClass QueryEnvClass;
	typedef enum _QueryActions QueryActions;

	/* all the actions that can be done from a Form (and some from a Grid) */
	enum _QueryActions
	{
		QUERY_ACTION_FIRST = 1 << 0,	/* move to the first record of the result */
		QUERY_ACTION_LAST = 1 << 1,
		QUERY_ACTION_PREV = 1 << 2,
		QUERY_ACTION_NEXT = 1 << 3,
		QUERY_ACTION_INSERT = 1 << 4,	/* Ask for a new empty Form */
		QUERY_ACTION_COMMIT = 1 << 5,	/* commit the changes done */
		QUERY_ACTION_DELETE = 1 << 6,
		QUERY_ACTION_CHOOSE = 1 << 7,	/* when the user is prompted for a value */
		QUERY_ACTION_REFRESH = 1 << 8,	/* ask to refresh */
		QUERY_ACTION_EDIT = 1 << 9,
		QUERY_ACTION_VIEWALL = 1 << 10,
		QUERY_ACTION_LAST_ENUM
	};

	/* struct for the object's data */
	struct _QueryEnv
	{
		GtkObject object;

		Query     *q;
		DbTable   *modif_table;
		gchar     *name;
		gchar     *descr;

		guint      actions;	/* default possible actions (OR'ed QueryActions) */

		gpointer   form;	/* libglade struct for the form, or NULL */
		gboolean   form_is_default; /* TRUE if a form is created by default (otherwise a grid) */

		GSList    *execs_list;	/* list of all the QueryExec objects associated */
	};

	/* struct for the object's class */
	struct _QueryEnvClass
	{
		GtkObjectClass parent_class;

		void (*name_changed)        (QueryEnv * qe);
		void (*type_changed)        (QueryEnv * qe);	/* defaults as form or grid */
		void (*modif_table_changed) (QueryEnv * qe);
		void (*modified)            (QueryEnv * qe);
		void (*actions_changed)     (QueryEnv * qe);
	};

	/* generic widget's functions */
	guint      query_env_get_type (void);
	GtkObject *query_env_new (Query * q);

	/* register the attached QueryExec objects to maintain the execs_list list */
	void       query_env_register_exec_obj   (QueryEnv * env, GtkObject * obj);
	void       query_env_set_name            (QueryEnv * env, gchar * name);
	void       query_env_set_descr           (QueryEnv * env, gchar * descr);
	void       query_env_set_modif_table     (QueryEnv * env, DbTable * table);
	void       query_env_set_type            (QueryEnv * env, gboolean form_is_default);
	void       query_env_set_actions         (QueryEnv * env, guint actions);
	void       query_env_add_action          (QueryEnv * env, QueryActions action);
	void       query_env_del_action          (QueryEnv * env, QueryActions action);

	/* XML manipulation */
	gchar     *query_env_get_xml_id          (QueryEnv * env);
	QueryEnv  *query_env_build_from_xml_tree (ConfManager * conf, xmlNodePtr toptree);
	void       query_env_build_xml_tree      (QueryEnv * env, xmlNodePtr toptree);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
