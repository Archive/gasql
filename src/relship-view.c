/* relship-view.c
 *
 * Copyright (C) 2002 Vivien Malerba
 * Copyright (C) 2002 Fernando Martins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "relship.h"
#include "relship-view.h"
#include "canvas-query-view.h"
#include "canvas-query-join.h"
#include "canvas-field.h"

/*
 * 
 * RelShipView object
 * 
 */

enum
{
	ITEM_MOVED,
	LAST_SIGNAL
};

static gint relship_signals[LAST_SIGNAL] = { 0 };


static void relship_view_class_init (RelShipViewClass * class);
static void relship_view_init       (RelShipView * rs);
static void relship_view_destroy    (GtkObject * object);
static void relship_view_post_init  (RelShipView * rs);

guint
relship_view_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"RelShipView",
			sizeof (RelShipView),
			sizeof (RelShipViewClass),
			(GtkClassInitFunc) relship_view_class_init,
			(GtkObjectInitFunc) relship_view_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gnome_canvas_get_type (), &f_info);
	}

	return f_type;
}

static void
relship_view_class_init (RelShipViewClass * class)
{
	GtkObjectClass *object_class = NULL;

	object_class = (GtkObjectClass *) class;

	relship_signals[ITEM_MOVED] =
		gtk_signal_new ("item_moved",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (RelShipViewClass, item_moved),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	gtk_object_class_add_signals (object_class, relship_signals,
				      LAST_SIGNAL);
	class->item_moved = NULL;

	object_class->destroy = relship_view_destroy;
}


static void
relship_view_init (RelShipView * rs)
{
	rs->query = NULL;
	rs->items = NULL;
	rs->background = NULL;
}

static void widget_size_changed_cb(GtkWidget *wid, GtkAllocation *alloc, RelShipView *rs);
static int canvas_event(GnomeCanvasItem *item, GdkEvent *event, RelShipView * rs);
GtkWidget *
relship_view_new (Query *q) 
{
	GtkObject *obj;
	RelShipView *rs;

	g_return_val_if_fail (q, NULL);
	g_return_val_if_fail (IS_QUERY (q), NULL);

	obj = gtk_type_new (relship_view_get_type ());
	rs = RELSHIP_VIEW (obj);

	rs->query = q;
	relship_view_post_init (rs);

	gtk_signal_connect (GTK_OBJECT (rs),"size_allocate",
			    GTK_SIGNAL_FUNC(widget_size_changed_cb), rs);

	gtk_signal_connect (GTK_OBJECT (rs->background),"event",
			    GTK_SIGNAL_FUNC(canvas_event), rs);

	return GTK_WIDGET (obj);
}

/* sets the background canvas item to occupy at least the whole window so the
   user can't click outside of it. */ 
static void widget_size_changed_cb(GtkWidget *wid, GtkAllocation *alloc, RelShipView *rs)
{
	double wx1, wy1, wx2, wy2;
	double sx1, sy1, sx2, sy2;

	gnome_canvas_get_scroll_region (GNOME_CANVAS (rs), &sx1, &sy1, &sx2, &sy2);
	gnome_canvas_window_to_world (GNOME_CANVAS (rs), 0., 0., &wx1, &wy1);
	gnome_canvas_window_to_world (GNOME_CANVAS (rs), alloc->width, alloc->height, &wx2, &wy2);

	if (wx1 > sx1) wx1 = sx1;
	if (wy1 > sy1) wy1 = sy1;
	if (wx2 < sx2) wx2 = sx2;
	if (wy2 < sy2) wy2 = sy2;
	
	gnome_canvas_item_set (rs->background, 
			       "x1", wx1,
			       "y1", wy1,
			       "x2", wx2,
			       "y2", wy2,
			       NULL);
}


static GtkWidget *build_context_menu (RelShipView * rs);
static int 
canvas_event(GnomeCanvasItem *item, GdkEvent *event, RelShipView * rs)
{
	gboolean done = TRUE;

	if (gtk_object_get_data (GTK_OBJECT (gnome_canvas_root (GNOME_CANVAS (rs))), "dragged_from")) {
		/* Dragging cancelled */
		gtk_object_set_data (GTK_OBJECT (gnome_canvas_root (GNOME_CANVAS (rs))), "dragged_from", NULL);
	}

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		if (((GdkEventButton *) event)->button == 3) {
			GtkWidget *menu;

			menu = build_context_menu (rs);
			gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
					NULL, NULL, ((GdkEventButton *)event)->button,
					((GdkEventButton *)event)->time);
			
			done = FALSE;
			break;
		}
	default:
		done = FALSE;
		break;
	}

	return done;	
}


static void add_query_view_menu_cb (GtkMenuItem *mitem, RelShipView * rs);
static GtkWidget *
build_context_menu (RelShipView * rs)
{
	GtkWidget *menu, *entry, *menu2;
	GSList *list;

	menu = gtk_menu_new ();

	/* Tables */
	entry = gtk_menu_item_new_with_label (_("Add a table"));
	gtk_menu_append (GTK_MENU (menu), entry);
	gtk_widget_show (entry);

	menu2 = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (entry), menu2);

	list = rs->query->conf->db->tables;
	while (list) {
		entry = gtk_menu_item_new_with_label (DB_TABLE (list->data)->name);
		gtk_menu_append (GTK_MENU (menu2), entry);
		gtk_widget_show (entry);
		gtk_object_set_data (GTK_OBJECT (entry), "qv", list->data);
		gtk_signal_connect (GTK_OBJECT (entry), "activate", 
				    GTK_SIGNAL_FUNC (add_query_view_menu_cb), rs); 
		list = g_slist_next (list);
	}

	/* Queries */
	if (rs->query != QUERY (rs->query->conf->top_query)) {
		entry = gtk_menu_item_new_with_label (_("Add a query"));
		gtk_menu_append (GTK_MENU (menu), entry);
		gtk_widget_show (entry);
		
		menu2 = gtk_menu_new ();
		gtk_menu_item_set_submenu (GTK_MENU_ITEM (entry), menu2);

		/* Adding the sub queries */
		list = rs->query->sub_queries;
		while (list) {
			entry = gtk_menu_item_new_with_label (QUERY (list->data)->name);
			gtk_menu_append (GTK_MENU (menu2), entry);
			gtk_widget_show (entry);
			gtk_object_set_data (GTK_OBJECT (entry), "qv", list->data);
			gtk_signal_connect (GTK_OBJECT (entry), "activate",
					    GTK_SIGNAL_FUNC (add_query_view_menu_cb), rs);
			list = g_slist_next (list);
		}
		
		/* Adding the parent query if possible */
		if (rs->query->parent != QUERY (rs->query->conf->top_query)) {
			entry = gtk_menu_item_new_with_label (rs->query->parent->name);
			gtk_menu_append (GTK_MENU (menu2), entry);
			gtk_widget_show (entry);
			gtk_object_set_data (GTK_OBJECT (entry), "qv", rs->query->parent);
			gtk_signal_connect (GTK_OBJECT (entry), "activate",
					    GTK_SIGNAL_FUNC (add_query_view_menu_cb), rs);
		}
	}

	/* A "Print" entry */
	entry = gtk_menu_item_new_with_label (_("Print"));
	gtk_menu_append (GTK_MENU (menu), entry);
	gtk_widget_show (entry);
	gtk_widget_set_sensitive (entry, FALSE);

	return menu;
}

static void 
add_query_view_menu_cb (GtkMenuItem *mitem, RelShipView * rs)
{
	GtkObject *obj;

	obj = GTK_OBJECT (gtk_object_get_data (GTK_OBJECT (mitem), "qv"));

	query_add_view_with_obj (rs->query, obj);	
}

static void
relship_view_destroy (GtkObject * object)
{
	GtkObjectClass *parent_class = NULL;

	parent_class = gtk_type_class (gnome_canvas_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_RELSHIP_VIEW (object));


	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


static void query_view_added_cb (Query *q, QueryView *view, RelShipView *rs);
static void query_view_removed_cb (Query *q, QueryView *view, RelShipView *rs);
static void query_join_added_cb (Query *q, QueryJoin *join, RelShipView *rs);
static void query_join_removed_cb (Query *q, QueryJoin *join, RelShipView *rs);
static void query_destroy_cb (Query *q, RelShipView *rs);
static void 
relship_view_post_init  (RelShipView * rs)
{
	GSList *list;
	gdouble x1, y1, x2, y2;

	gnome_canvas_get_scroll_region (GNOME_CANVAS (rs), &x1, &y1, &x2, &y2);
	rs->background = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (rs)),
						GNOME_TYPE_CANVAS_RECT,
						"x1", 0.,
						"y1", 0.,
						"x2", 10.,
						"y2", 10.,
						"fill_color", "gray90",
						"outline_color", "black",
						"width_units", 1.0,
						NULL);
	gnome_canvas_item_lower_to_bottom (rs->background);

	/* display all the QueryViews which are present */
	list = rs->query->views;
	while (list) {
		query_view_added_cb (rs->query, QUERY_VIEW (list->data), rs);
		
		list = g_slist_next (list);
	}

	/* display all the QueryJoins which are present */
	list = rs->query->joins;
	while (list) {
		GSList *list2;
		list2 = (GSList *) (list->data);
		while (list2) {
			query_join_added_cb (rs->query, QUERY_JOIN (list2->data), rs);
			list2 = g_slist_next (list2);
		}
		list = g_slist_next (list);
	}

	/* connect for the QueryViews management */
	gtk_signal_connect_while_alive (GTK_OBJECT (rs->query), "query_view_added",
					GTK_SIGNAL_FUNC (query_view_added_cb), rs,
					GTK_OBJECT (rs));

	gtk_signal_connect_while_alive (GTK_OBJECT (rs->query), "query_view_removed",
					GTK_SIGNAL_FUNC (query_view_removed_cb), rs,
					GTK_OBJECT (rs));

	gtk_signal_connect_while_alive (GTK_OBJECT (rs->query), "destroy",
					GTK_SIGNAL_FUNC (query_destroy_cb), rs,
					GTK_OBJECT (rs));

	/* connect for the QueryJoins management */
	gtk_signal_connect_while_alive (GTK_OBJECT (rs->query), "join_created",
					GTK_SIGNAL_FUNC (query_join_added_cb), rs,
					GTK_OBJECT (rs));

	gtk_signal_connect_while_alive (GTK_OBJECT (rs->query), "join_dropped",
					GTK_SIGNAL_FUNC (query_join_removed_cb), rs,
					GTK_OBJECT (rs));
}

static void item_moved_cb (CanvasBase *item, RelShipView *rs);
static void drag_action_cb (CanvasBase *repport, CanvasBase *drag_to, CanvasBase * drag_from, RelShipView *rs);
static void 
query_view_added_cb (Query *q, QueryView *view, RelShipView *rsv)
{
	gdouble x = 50., y = 50.;
	GnomeCanvasItem *item;
	RelShip *rs;
	RelShipItemData *id;

	rs = RELSHIP (relship_find (q));
	id = relship_find_item (rs, GTK_OBJECT (view));
	x = id->x;
	y = id->y;

	item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (rsv)),
				      canvas_query_view_get_type (),
				      "query", rsv->query,
				      "query_view", view,
				      "x", x,
				      "y", y,
				      NULL);

	rsv->items = g_slist_append (rsv->items, item);

	gtk_signal_connect (GTK_OBJECT (item), "moved",
			    GTK_SIGNAL_FUNC (item_moved_cb), rsv);

	gtk_signal_connect (GTK_OBJECT (item), "drag_action",
			    GTK_SIGNAL_FUNC (drag_action_cb), rsv);
}

static void 
item_moved_cb (CanvasBase *item, RelShipView *rs)
{
	double ix1, iy1, ix2, iy2;
	double sx1, sy1, sx2, sy2;
	gboolean changed = FALSE;

	gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (item), &ix1, &iy1, &ix2, &iy2);
	gnome_canvas_get_scroll_region (GNOME_CANVAS (rs), &sx1, &sy1, &sx2, &sy2);

	if (ix1 < sx1) {
		sx1 = ix1 - 2.;
		changed = TRUE;
	}

	if (iy1 < sy1) {
		sy1 = iy1 - 2.;
		changed = TRUE;
	}

	if (ix2 > sx2) {
		sx2 = ix2 + 2.;
		changed = TRUE;
	}

	if (iy2 > sy2) {
		sy2 = iy2 + 2.;
		changed = TRUE;
	}

	if (changed) {
		/* FIXME: there is a bug in the canvas because this function displays trash */
		gnome_canvas_set_scroll_region (GNOME_CANVAS (rs), sx1, sy1, sx2, sy2);
	}

#ifdef debug_signal
	g_print (">> 'ITEM_MOVED' from %s::item_moved_cb\n", __FILE__);
#endif
	gtk_signal_emit (GTK_OBJECT (rs), relship_signals[ITEM_MOVED], item);
#ifdef debug_signal
	g_print ("<< 'ITEM_MOVED' from %s::item_moved_cb\n", __FILE__);
#endif
}

static void 
drag_action_cb (CanvasBase *repport, CanvasBase *drag_from, CanvasBase * drag_to, RelShipView *rs)
{
	g_return_if_fail (IS_RELSHIP_VIEW (rs));

	/* Dragging of field to field */
	if (IS_CANVAS_FIELD (drag_from) && IS_CANVAS_FIELD (drag_to)) {
		if (CANVAS_FIELD (drag_from)->cqv == CANVAS_FIELD (drag_to)->cqv) {
			gchar *str;
			GtkWidget *dlg;

			if (IS_DB_FIELD (CANVAS_FIELD (drag_from)->field))
				str = _("To create a self join, create an alias of this table (or view)\n"
					"and then join the two.");
			else
				str = _("To create a self join, create an alias of this query\n"
					"and then join the two.");
			dlg = gnome_app_message (GNOME_APP (rs->query->conf->app), str);
			gtk_window_set_modal (GTK_WINDOW (dlg), TRUE);
		}
		else {
			query_add_join (rs->query,
					CANVAS_FIELD (drag_from)->cqv->view,
					CANVAS_FIELD (drag_from)->field,
					CANVAS_FIELD (drag_to)->cqv->view,
					CANVAS_FIELD (drag_to)->field);
		}
	}
}

static void 
query_view_removed_cb (Query *q, QueryView *view, RelShipView *rs)
{
	GSList *list = rs->items;
	gboolean found = FALSE;

	while (list && !found) {
		GnomeCanvasItem *item;

		item = GNOME_CANVAS_ITEM (list->data);
		if (IS_CANVAS_QUERY_VIEW (item) &&
		    (CANVAS_QUERY_VIEW (item)->view == view)) {
			found = TRUE;
			gtk_object_destroy (GTK_OBJECT (item));
			rs->items = g_slist_remove_link (rs->items, list);
			g_slist_free_1 (list);
		}
		else
			list = g_slist_next (list);
	}

	if (!found)
		g_warning ("Strange, a QueryView being removed was not displayed... at %s line %d\n", 
			   __FILE__, __LINE__);
}


static void 
query_join_added_cb (Query *q, QueryJoin *join, RelShipView *rs)
{
	GnomeCanvasItem *item;
	
	item = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS (rs)),
				      canvas_query_join_get_type (),
				      "query", rs->query,
				      "query_join", join,
				      "x", 0.,
				      "y", 0.,
				      NULL);

	rs->items = g_slist_append (rs->items, item);

	gtk_signal_connect (GTK_OBJECT (item), "drag_action",
			    GTK_SIGNAL_FUNC (drag_action_cb), rs);
}

static void 
query_join_removed_cb (Query *q, QueryJoin *join, RelShipView *rs)
{
	GSList *list = rs->items;
	gboolean found = FALSE;

	while (list && !found) {
		GnomeCanvasItem *item;

		item = GNOME_CANVAS_ITEM (list->data);
		if (IS_CANVAS_QUERY_JOIN (item) &&
		    (CANVAS_QUERY_JOIN (item)->join == join)) {
			found = TRUE;
			gtk_object_destroy (GTK_OBJECT (item));
			rs->items = g_slist_remove_link (rs->items, list);
			g_slist_free_1 (list);
		}
		else
			list = g_slist_next (list);
	}

	if (!found)
		g_warning ("Strange, a QueryJoin being removed was not displayed... at %s line %d\n", 
			   __FILE__, __LINE__);
}


static void 
query_destroy_cb (Query *q, RelShipView *rs)
{
	gtk_object_destroy (GTK_OBJECT (rs));
}

void 
relship_view_refresh_items (RelShipView * rsv)
{
	GSList *list = rsv->items;
	RelShip *rs;
	gdouble x1, y1, x2, y2;

	rs = RELSHIP (relship_find (rsv->query));

	while (list) {
		GtkObject *obj = NULL;
		
		if (IS_CANVAS_QUERY_VIEW (list->data))
			obj = GTK_OBJECT (CANVAS_QUERY_VIEW (list->data)->view);
		if (obj) {
			RelShipItemData *id;
			id = relship_find_item (rs, obj);

			gnome_canvas_item_get_bounds (GNOME_CANVAS_ITEM (list->data),
						      &x1, &y1, &x2, &y2);
			if ((x1 != id->x) || (y1 != id->y)) {
				gnome_canvas_item_move (GNOME_CANVAS_ITEM (list->data),
							id->x - x1, id->y - y1);
#ifdef debug_signal
				g_print (">> 'MOVED' from %s::relship_view_refresh_items()\n", __FILE__);
#endif
				gtk_signal_emit_by_name (GTK_OBJECT (list->data), "moved");
#ifdef debug_signal
				g_print ("<< 'MOVED' from %s::relship_view_refresh_items()\n", __FILE__);
#endif
			}
		}
		list = g_slist_next (list);
	}
}

GnomeCanvasItem *
relship_view_find_query_view (RelShipView * rs, QueryView *qv)
{
	GnomeCanvasItem *cqv = NULL;
	GSList *list;

	g_return_val_if_fail (IS_RELSHIP_VIEW (rs), NULL);

	list = rs->items;
	while (list && !cqv) {
		if (IS_CANVAS_QUERY_VIEW (list->data) && 
		    (CANVAS_QUERY_VIEW (list->data)->view == qv))
			cqv = GNOME_CANVAS_ITEM (list->data);
		list = g_slist_next (list);
	}

	return cqv;
}
