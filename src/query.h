/* query.h
 *
 * Copyright (C) 1999 - 2002 Vivien Malerba
 * Copyright (C) 2001 - 2002 Fernando Martins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __QUERY__
#define __QUERY__

#include <gtk/gtkobject.h>
#include "conf-manager.h"
#include "database.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */


	/* Declarations of the new types for the objects */
	typedef struct _Query Query;
	typedef struct _QueryClass QueryClass;

	typedef struct _QueryView QueryView;
	typedef struct _QueryViewClass QueryViewClass;

	typedef struct _QueryField QueryField;
	typedef struct _QueryFieldClass QueryFieldClass;

	typedef struct _QueryJoin QueryJoin;
	typedef struct _QueryJoinClass QueryJoinClass;

	typedef struct _QueryWhere QueryWhere;
	typedef struct _QueryWhereClass QueryWhereClass;



/*
 * 
 * Query object
 * 
 */


#define QUERY(obj)          GTK_CHECK_CAST (obj, query_get_type(), Query)
#define QUERY_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, query_get_type (), QueryClass)
#define IS_QUERY(obj)       GTK_CHECK_TYPE (obj, query_get_type ())

	/* different possible types for a query */
	typedef enum {
		QUERY_TYPE_STD,
		QUERY_TYPE_UNION,
		QUERY_TYPE_INTERSECT,
		QUERY_TYPE_SQL,
		QUERY_TYPE_LAST
	} QueryType;



	/* a struct to hold values which are missing to pass to a
	   query to render itself as SQL or XML */
	typedef struct _QueryMissingValue QueryMissingValue;
	struct _QueryMissingValue
	{
		QueryField *field; /* referenced field for the missing value */
		gchar *sql;	      /* value */
	};



	/* struct for the object's data */
	struct _Query
	{
		GtkObject          object;

		/* parents and children */
		Query             *parent;       /* to the parent query if it exists */
		GSList            *sub_queries;  /* Query objects if they exist */

		ConfManager       *conf;
		guint              id;	    /* comes from conf->id_serial, unique */
		gchar             *name;
		gchar             *descr;
		QueryType          type;

		GSList            *views;   /* QueryView structs */
		guint              fields_counter; /* to give unique IDs to QueryField objects */
		GSList            *fields;  /* pointers are to QueryFields objects */
		GSList            *joins;   /* pointers are to QueryJoins objects */

		/* GNodes tree of QueryWhere objects. */
		GNode             *where_tree;

		GSList            *group_by_list;	/* Not yet used */
		GSList            *order_by_list;	/* Not yet used */

		/* Last entered SQL text */
		gchar             *text_sql;

		GSList            *envs;       /* QueryEnv associated to this query */
	};

	/* struct for the object's class */
	struct _QueryClass
	{
		GtkObjectClass parent_class;

		void (*changed)                   (Query * q);
		void (*name_changed)              (Query * q);
		void (*type_changed)              (Query * q);

		void (*field_created)             (Query * q, QueryField * new_field);
		void (*field_dropped)             (Query * q, QueryField * field);
		/* emited when field modified and when field is moved */
		void (*field_modified)            (Query * q, QueryField * field);
		void (*field_name_modified)       (Query * q, QueryField * field);
		void (*field_alias_modified)      (Query * q, QueryField * field);

		void (*join_created)              (Query * q, QueryJoin * join);
		void (*join_dropped)              (Query * q, QueryJoin * join);
		void (*join_modified)             (Query * q, QueryJoin * join);

		void (*where_created)             (Query * q, QueryWhere * qwhere);
		/* WARNING: the next signal is emitted while the node is still in the GNodes tree,
		   and it is removed and destroyed only after the signal callbacks are finished */
		void (*where_dropped)             (Query * q, QueryWhere * qwhere);
		void (*where_modified)            (Query * q, QueryWhere * qwhere);
		void (*where_moved)               (Query * q, QueryWhere * qwhere);

		/* We are talking about GtkObjects here because we don't use the
		   contents of the QueryEnv, just keep a list with it */
		void (*env_created)               (Query * q, GtkObject *env);
		void (*env_dropped)               (Query * q, GtkObject *env);

		/* signals managing the list of "sources" in which the
		   query's fields must be it they are of table's field of query'field
		   type */
		void (*query_view_added)          (Query * q,  QueryView *qv);
		void (*query_view_removed)        (Query * q,  QueryView *qv);

		/* signals the creation or deletion of a query which is a sub query
		   (or a sub-sub query, etc) */
		void (*query_created)             (Query * q, Query *new_query);
		void (*query_dropped)             (Query * q, Query *old_query);
	};



	/* 
	 * generic widget's functions 
	 */
	guint      query_get_type            (void);
	GtkObject *query_new                 (gchar * name, Query *parent_query, ConfManager * conf);
	GtkObject *query_new_copy            (Query * q);


	/*
	 * Helper functions
	 */
	/* if NULL passed as gchar*, nothing happens for that gchar* */
	void query_set_name                  (Query * q, gchar * name, gchar * descr);
	
	/* sets the QueryType */
	void query_set_query_type            (Query * q, QueryType type);

	/* sets the text SQL and toggles the query type */
	void query_set_text_sql              (Query * q, gchar * sql);


	/*
	 * XML 
	 */
	gchar *query_get_xml_id              (Query *q);
	Query *query_find_from_xml_name      (ConfManager * conf, Query * start_query, gchar *xmlname);
	Query *query_build_from_xml_tree     (ConfManager * conf, xmlNodePtr node, Query *parent);
	void   query_build_xml_tree          (Query * q, xmlNodePtr toptree,
					      ConfManager * conf);

	/* 
	 * Query contents manipulation
	 */
	void     query_add_view                  (Query *q, QueryView *qv);
	void     query_add_view_with_obj         (Query *q, GtkObject *obj);
	void     query_del_view                  (Query *q, QueryView *qv); /* qv gets free'd */

	guint    query_get_unique_field_id       (Query *q, guint proposed_id);
	void     query_add_field                 (Query *q, QueryField *field);
	void     query_del_field                 (Query *q, QueryField *field);

	void     query_add_sub_query             (Query *q, Query *sibling, Query *sub_query);
	void     query_del_sub_query             (Query *q, Query *sub_query);
	gboolean query_is_query_compatible       (Query *q, Query *query_to_add);

	/* find a field in the query */
	QueryField *query_get_field_by_name  (Query * q, gchar * name);
	QueryField *query_get_field_by_xmlid (Query * q, gchar * xmlid);

	/* find a field in the query where it belongs only (otherwise use XML name) */
	QueryField *query_get_field_by_id    (Query * q, guint id);
	void        query_swap_fields        (Query * q, QueryField *f1, QueryField *f2);
	gboolean    query_fields_activated   (Query *q);

	/*
	 * Query joins management
	 */
/*	void query_add_join                  (Query * q, QueryJoin *join);*/
	void query_add_join		     (Query *q, QueryView *v1, QueryField *f1, QueryView  *v2, QueryField *f2);
	void query_del_join                  (Query * q, QueryJoin *join);

	/*
	 * Query WHERE/HAVING management
	 */
	/* insert the new node before the given sibling */
	void   query_add_where               (Query * q, QueryWhere *where,
					      QueryWhere * parent, QueryWhere * sibling);
	void   query_del_where               (Query * q, QueryWhere *where);
	void   query_move_where              (Query * q, QueryWhere * where,
					      QueryWhere * to_parent, QueryWhere * to_sibling);
	GNode *query_where_gnode_find        (Query * q, QueryWhere * where);

	/*
	 * Query Envs management 
	 * (GtkObject because the QueryEnv is an object we don't know about here)
	 */
	void   query_add_env                 (Query * q, GtkObject *env);
	void   query_del_env                 (Query * q, GtkObject *env);

	/*
	 * Interrogation helper functions 
	 */

	/* tells if a particular field appears in the print list of the Query:
	   returns -1 if not present and >=0 otherwise, the number being the column
	   number of the ServerResultset of the SELECT query */
	gint        query_is_table_field_printed  (Query * q, DbField * field);

	/* returns the QueryField of the query given the column number it occupies
	   int the ServerResultset of the SELECT row */
	QueryField *query_get_field_by_pos        (Query * q, gint pos);

	/* tells if a table appears somewhere in the query even if no field
	   is printed */
	gboolean    query_is_table_concerned      (Query * q, DbTable * table);


	/*
	 * Getting the SQL versions of the query
	 * missing_value can be NULL if the missing values are not defined
	 */
	gchar      *query_get_select_query        (Query * q, GSList * missing_values);

	/* getting the SQL query to select all the fields of a table given
	   the entry number in the ServerResultset for the SELECT version of the query
	   Used to request a confirmation before deletion
	   WARNING: returns only the part of the query after the FROM statement,
	   so it can be used with DELETE, UPDATE as well */
	gchar      *query_get_select_all_query_with_values (Query * q, gint row);

#ifdef debug
	/* structure debug function */
	void        query_dump_contents           (Query *q);
#endif




/*
 * 
 * QueryView object
 * 
 */


#define QUERY_VIEW(obj)          GTK_CHECK_CAST (obj, query_view_get_type(), QueryView)
#define QUERY_VIEWCLASS(klass)  GTK_CHECK_CLASS_CAST (klass, query_view_get_type (), QueryViewClass)
#define IS_QUERY_VIEW(obj)       GTK_CHECK_TYPE (obj, query_view_get_type ())


	struct _QueryView
	{
		GtkObject  object;

		Query     *query;
		GtkObject *obj;       /* Query or DbTable */
		gint       occ;       /* occurrence in case of aliases, starts at 0 */
		gchar     *alias;     /* new name given as alias, if any */
		gchar     *alias_exp; /* new name given as alias, if any */
		gchar     *ref;       /* may be used to find a reference to the Query */
	};


	/* struct for the object's class */
	struct _QueryViewClass
	{
		GtkObjectClass parent_class;
	};



	/* 
	 * generic widget's functions 
	 */
	guint      query_view_get_type            (void);
	GtkObject *query_view_new                 (Query *q);
	GtkObject *query_view_new_copy            (QueryView *qv);
	void       query_view_set_query           (QueryView *qv, Query *q);
	void       query_view_set_view_obj        (QueryView *qv, GtkObject *obj);
	
	/* XML saving and loading */
	gchar      *query_view_get_xml_id         (QueryView *qv);
	xmlNodePtr  query_view_save_to_xml        (QueryView *qv);
	void        query_view_load_from_xml      (QueryView *qv, xmlNodePtr node);
	QueryView  *query_view_find_from_xml_name (ConfManager * conf, Query * start_query, gchar *xmlname);
	gboolean    query_view_contains_field     (QueryView *qv, GtkObject *field); /* field can be DbField
											or QueryField */
	/* Helper function */
	gchar     *query_view_get_textual         (QueryView *qv);
	gchar 	  *query_view_get_name		  (QueryView *qv); /* don't free the returned value */
	gchar 	  *query_view_get_real_name	  (QueryView *qv);
	gchar     *query_view_get_field_name     (QueryView *qv, GtkObject *qvf); /* free the value */
	gchar 	  *query_view_get_alias_exp	  (QueryView *qv);
	gboolean   query_view_is_alias		  (QueryView *qv);

/*
 * 
 * QueryField object:
 * 
 * Can be of several types: a table's field, a function, an aggregate, a value,
 *                          a subquery or field in a sub query, etc
 * 
 */

#define QUERY_FIELD(obj)          GTK_CHECK_CAST (obj, query_field_get_type(), QueryField)
#define QUERY_FIELD_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, query_field_get_type (), QueryFieldClass)
#define IS_QUERY_FIELD(obj)       GTK_CHECK_TYPE (obj, query_field_get_type ())

	typedef struct _QueryFieldIface QueryFieldIface;

	typedef enum
	{
		QUERY_FIELD_FIELD,
		QUERY_FIELD_ALLFIELDS,
		QUERY_FIELD_AGGREGATE,
		QUERY_FIELD_FUNCTION,
		QUERY_FIELD_VALUE,
		QUERY_FIELD_QUERY,
		QUERY_FIELD_QUERY_FIELD,
		QUERY_FIELD_LAST
	}
	QueryFieldType;

	/*
	 * Description of the common interface of all the
	 * QueryFields:
	 * - init():
	 *   will initialise any private data, connect signals, etc
	 *   for the QueryField type; the QueryField object has already 
	 *   been initialised by the GTK object system
	 * - destroy():
	 *   will only free any memory, signal handlers, etc from the
	 *   object, not free the object itself
	 * - get_edit_widget():
	 *   returns a widget to edit the contents of a QueryField
	 * - get_sel_widget():
	 *   returns a widget to enable the selection of the QueryField but also
	 *   of any QueryField which is being used by this one.
	 *   This function MUST connect a selection event to the "func" callback and
	 *   MUST set the "qf" property to the widget emitting that signal (as well as
	 *   optionnaly the "pqf" or "ref" ones).
	 * - get_monitored_objects():
	 *   returns a list (to be freed by the caller)
	 *   with all the objects the QueryField receives signals from.
	 * - render_as_string():
	 *   will give a string describng the query field
	 *   the missing_values argument can always be NULL
	 * - get_xml_id():
	 *   will return the ID part of the XML node where the QueryField
	 *   saves itself, so that other objects can reference it in their XML element.
	 * - replace_comp():
	 *   replace a QueryField with another (it is possible to give ref=-1 or old=NULL
	 *   but not the two at the same time)
	 *   
	 */
	struct _QueryFieldIface {
		QueryFieldType field_type;
		gchar        * name; /* the name of the 'type' attribute in the XML file */
		gchar        * pretty_name; /* A name understandable by the user */
		void        (* init)            (QueryField *qf);
		void        (* destroy)         (QueryField *qf);
		void        (* deactivate)      (QueryField *qf);
		void        (* activate)        (QueryField *qf);
		GtkWidget * (* get_edit_widget) (QueryField *qf);
		GtkWidget * (* get_sel_widget)  (QueryField *qf, GtkSignalFunc func, gpointer data);
		gchar     * (* render_as_sql)   (QueryField *qf, GSList * missing_values);
		xmlNodePtr  (* render_as_xml)   (QueryField *qf, GSList * missing_values);
		gchar     * (* render_as_string)(QueryField *qf, GSList * missing_values);
		void        (* save_to_xml)     (QueryField *qf, xmlNodePtr node);
		void        (* load_from_xml)   (QueryField *qf, xmlNodePtr node);
		void        (* copy_other_field)(QueryField *qf, QueryField *other);
		gboolean    (* is_equal_to)     (QueryField *qf, QueryField *other);
		GSList    * (* get_monitored_objects) (QueryField *qf);
		void        (* replace_comp)    (QueryField *qf, gint ref, GtkObject *old, GtkObject *new);
	};

	/* struct for the object's data */
	struct _QueryField
	{
		GtkObject object;

		Query         *query;
		gchar         *name;
		gchar         *alias;
		gboolean       is_printed;   /* TRUE if it appears in the resulting columns */
		QueryFieldType field_type;
		
		guint          id; /* Id unique only inside the Query */
		
		/* TRUE if the object has references to all the objects
		   it wants to receive events from */
		gboolean       activated;
		
		/* private data which can be used by the different query field types */
		gpointer       private_data;
		guint          ref_counter;
	};

	/* struct for the object's class */
	struct _QueryFieldClass
	{
		GtkObjectClass parent_class;

		void (*field_modified)     (QueryField *qf);
		void (*field_type_changed) (QueryField *qf);
		void (*name_changed)       (QueryField *qf);
		void (*alias_changed)      (QueryField *qf);
		void (*status_changed)     (QueryField *qf);

		/* list of query field types, static to the class */
		GSList *field_types; /* of type QueryFieldIface */
	};

	/* 
	 * generic widget's functions 
	 */
	guint       query_field_get_type          (void);
	/* q can't be NULL */
	GtkObject  *query_field_new               (Query *q, gchar * name, QueryFieldType field_type);
	/* detects the kind of object to build and makes a new one. q can't be NULL */
	GtkObject  *query_field_new_objref        (Query *q, gchar * name, gchar *type); 

	/* create a copy object (the name is not copied though and is NULL) */
	GtkObject  *query_field_new_copy          (QueryField *field);

	/* create a copy of the object and of all the other objects
	   related to that object (recursively). It is NOT possible to pass NULL
	   as list (this param returns the list of QueryField created).
	(*list) MUST be NULL or everything in the list will be lost */
	GtkObject  *query_field_new_copy_all      (QueryField *field, GSList **list);

	/* Counter's management */
	void        query_field_use_ref           (QueryField *field);
	void        query_field_free_ref          (QueryField *field);
	

	/* deactivate => don't receive events from other objects */
	void        query_field_deactivate        (QueryField * qf);
	/* activate => find objects to get events from and connect 
	   objects should try to activate themselves as soon as possible
	   and if not, then remain in a state to be activated later */
	void        query_field_activate          (QueryField * qf);
	void        query_field_get_activated     (QueryField * qf);

	void        query_field_set_name          (QueryField * qf, gchar * name);
	void        query_field_set_alias         (QueryField * qf, gchar * alias);
	void        query_field_set_is_printed    (QueryField * qf, gboolean is_printed);
	void        query_field_set_id            (QueryField * qf, guint id);

	/* copies the contents of a QueryField into another alredy existing */
	void        query_field_copy_object       (QueryField *dest, QueryField *orig);
	gboolean    query_field_is_equal          (QueryField *field, QueryField *other);

	GtkWidget * query_field_get_edit_widget   (QueryField *qf);
	GtkWidget * query_field_get_select_widget (QueryField *qf, GtkSignalFunc func, gpointer data);

	/* string to free after usage */
	gchar     * query_field_render_as_sql     (QueryField *qf, GSList * missing_values);

	xmlNodePtr  query_field_render_as_xml     (QueryField *qf, GSList * missing_values);

	/* string to free after usage */
	gchar     * query_field_render_as_string  (QueryField *qf, GSList * missing_values);

	xmlNodePtr  query_field_save_to_xml       (QueryField *qf);
	void        query_field_load_from_xml     (QueryField *qf, xmlNodePtr node);

	/* free the returned list */
	GSList    * query_field_get_monitored_objects (QueryField *qf);

	void        query_field_replace_ref_ptr   (QueryField *qf, GtkObject *old, GtkObject *new);
	void        query_field_replace_ref_int   (QueryField *qf, gint ref, QueryField *new);

	/*
	 * XML id manipulation
	 */
	/* string to free after usage */
	gchar     * query_field_get_xml_id      (QueryField *qf);

/*
 * 
 * QueryField object's different implementations
 * 
 * These are the interfaces with which the QueryField manipulates the
 * different implementations.
 *
 * However, because we sometimes need to have a direct access to some
 * properties, there can be some functions to get those properties which
 * are not used by the QueryField object's implementation (which uses only
 * virtual methods.
 * 
 */

/* Representation of a table's of a view's field */
	QueryFieldIface * query_field_field_get_iface     ();
	ServerDataType  * query_field_field_get_data_type (QueryField *qf);
	/* qv can be NULL */
	void              query_field_field_set_field     (QueryField *qf, QueryView *qv, DbField *field);


/* Representation of all the fields of a table or a view */
	QueryFieldIface * query_field_allfields_get_iface ();
	void              query_field_allfields_set_table (QueryField *qf, DbTable *table);


/* Representation of a constant */
	QueryFieldIface * query_field_constant_get_iface      ();
	ServerDataType  * query_field_constant_get_data_type  (QueryField *qf);
	gboolean          query_field_constant_input_required (QueryField *qf);

/* Representation of a function */
	QueryFieldIface * query_field_function_get_iface      ();

/* Representation of a whole query */
	QueryFieldIface * query_field_query_get_iface         ();
	Query           * query_field_query_get_query         (QueryField *qf);

/* Representation of another query's field */
	/*QueryField      * query_field_query_field_get_field   (QueryField *qf);*/

/*
 * 
 * QueryJoin object
 * 
 * 
 */

#define QUERY_JOIN(obj)          GTK_CHECK_CAST (obj, query_join_get_type(), QueryJoin)
#define QUERY_JOIN_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, query_join_get_type (), QueryJoinClass)
#define IS_QUERY_JOIN(obj)       GTK_CHECK_TYPE (obj, query_join_get_type ())
#define QUERY_JOIN_PAIR_CAST(ptr) ((QueryJoinPair*)ptr)

	/* Type of join between two tables (views) */
	typedef enum {
                QUERY_JOIN_INNER,
                QUERY_JOIN_LEFT_OUTER,
                QUERY_JOIN_RIGHT_OUTER,
                QUERY_JOIN_FULL_OUTER,
                QUERY_JOIN_CROSS,
                LAST_QUERY_JOIN_TYPE
        } QueryJoinType; 

	typedef enum {
		QUERY_JOIN_1_1,
		QUERY_JOIN_1_N,
		QUERY_JOIN_N_1,
		QUERY_JOIN_UNDEFINED,
		LAST_QUERY_JOIN_CARD
	} QueryJoinCard;

	/* pairs of (table's or query's) fields composing the join */
	typedef struct _QueryJoinPair QueryJoinPair;
	struct _QueryJoinPair {
		QueryJoin     *qj;
		GtkObject     *ant_field;
		GtkObject     *suc_field;
	};

	/* struct for the object's data */
	struct _QueryJoin
	{
		GtkObject object;

		Query         *query;
		QueryJoinType  join_type;
		
		/* tables or queries for the join (the object is either a DbTable or a Query) */
		QueryView     *ant_view;
		QueryView     *suc_view;

		/* pairs of fields in the join (QueryJoinPair) */
		GSList        *pairs;         /* list of field pairs being linked */
		gchar         *condition;     /* holds the join condition */
		gboolean       cond_modified; /* condition was modified by the user? */
		/* FIXME?: join condition should be stored as an expression tree? */

		/* cardinality of the join */
		QueryJoinCard  card;

		/* referential integrity */
		gboolean       ref_integrity;
		gboolean       cascade_update;
		gboolean       cascade_delete;

		gboolean       skip_view;    /* deal with duplicate suc_views (appear in loops) */
	};


	/* struct for the object's class */
	struct _QueryJoinClass
	{
		GtkObjectClass parent_class;

		void (*type_changed) (QueryJoin *qj);
		void (*card_changed) (QueryJoin *qj);
		void (*pair_added)   (QueryJoin *qj, QueryJoinPair *pair);
		void (*pair_removed) (QueryJoin *qj, QueryJoinPair *pair);
	};

	/* 
	 * generic widget's functions 
	 */
	guint          query_join_get_type          (void);
	GtkObject     *query_join_new               (Query *q, QueryView *ant_view, QueryView*suc_view);

	void           query_join_set_join_type     (QueryJoin *qj, QueryJoinType jt);
	QueryJoinType  query_join_get_join_type     (QueryJoin *qj);

	void           query_join_set_card          (QueryJoin *qj, QueryJoinCard card);
	QueryJoinCard  query_join_get_card          (QueryJoin *qj);

	gchar	      *query_join_get_join_type_sql (QueryJoin *qj);

	QueryView     *query_join_get_ant_view      (QueryJoin *qj);
	QueryView     *query_join_get_suc_view      (QueryJoin *qj);
	void	       query_join_swap_views	    (QueryJoin *qj);

	void	       query_join_set_condition     (QueryJoin *qj, gchar *cond);
	gchar	      *query_join_get_condition     (QueryJoin *qj);

	void	       query_join_set_skip_view     (QueryJoin *qj);
	void	       query_join_unset_skip_view   (QueryJoin *qj);
	gboolean       query_join_skip_view     (QueryJoin *qj);

	void           query_join_add_pair          (QueryJoin *qj, GtkObject *field1, GtkObject *field2);
	void           query_join_del_pair          (QueryJoin *qj, QueryJoinPair *pair);

	QueryJoin     *query_join_copy              (QueryJoin *qj);

	gchar         *query_join_render_as_sql     (QueryJoin *qj); 
	xmlNodePtr     query_join_render_as_xml     (QueryJoin *qj); 
					  
	xmlNodePtr     query_join_save_to_xml       (QueryJoin *qj);
	GtkObject     *query_join_new_from_xml      (ConfManager *conf, xmlNodePtr node);
	
	xmlNodePtr     query_join_list_save_to_xml  (GSList *jlist);
	GSList        *query_join_list_new_from_xml (ConfManager *conf, xmlNodePtr node);

	void query_join_print (QueryJoin *qj); /* DEBUG FER */
	

/*
 * 
 * QueryWhere object
 * 
 * 
 */

#define QUERY_WHERE(obj)          GTK_CHECK_CAST (obj, query_where_get_type(), QueryWhere)
#define QUERY_WHERE_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, query_where_get_type (), QueryWhereClass)
#define IS_QUERY_WHERE(obj)       GTK_CHECK_TYPE (obj, query_where_get_type ())


	/* the different kinds of QueryWhere objects */
	typedef enum {
		QUERY_WHERE_COND,
		QUERY_WHERE_AND,
		QUERY_WHERE_OR,
		QUERY_WHERE_NOT,
		LAST_QUERY_WHERE_TYPE
	} QueryWhereType;

	typedef enum
        {
                QUERY_WHERE_OP_EQUAL,
                QUERY_WHERE_OP_DIFF,
                QUERY_WHERE_OP_SUP,
                QUERY_WHERE_OP_SUPEQUAL,
                QUERY_WHERE_OP_INF,
                QUERY_WHERE_OP_INFEQUAL,
                QUERY_WHERE_OP_LIKE,
                QUERY_WHERE_OP_REGEX,
                QUERY_WHERE_OP_IN,
		QUERY_WHERE_OP_BETWEEN,
		LAST_QUERY_WHERE_OP
        } QueryWhereOpType;

	/* struct for the object's data */
	struct _QueryWhere
	{
		GtkObject        object;

		Query           *query;
		QueryWhereType   where_type;
		
		/* FALSE by default, set to TRUE if the "where_dropped" signal
		   has been emitted for this object from Query (to prevent emitting 
		   it a second time when the object emits the "destroy" signal) */
		gboolean         signal_already_emitted;

		QueryField      *left_op;
		QueryField      *right_op;
		QueryWhereOpType op_type;
	};

	/* struct for the object's class */
	struct _QueryWhereClass
	{
		GtkObjectClass parent_class;

		void (* optype_changed) (QueryWhere *qwh);
		void (* field_changed)  (QueryWhere *qwh);
	};


	/* 
	 * generic widget's functions 
	 */
	guint       query_where_get_type         (void);
	GtkObject  *query_where_new              (Query *q);


	xmlNodePtr  query_where_save_to_xml      (QueryWhere *qwh);
	void        query_where_load_from_xml    (QueryWhere *qwh, xmlNodePtr node);

	gchar     * query_where_render_as_sql    (QueryWhere *qwh, GSList * missing_values);
	xmlNodePtr  query_where_render_as_xml    (QueryWhere *qwh, GSList * missing_values);
	void        query_where_set_type         (QueryWhere *qwh, QueryWhereOpType wht);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
