/* tableedit.h
 *
 * Copyright (C) 1999 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __TABLE_EDIT__
#define __TABLE_EDIT__

#include <gnome.h>
#include "database.h"
#include "conf-manager.h"
#include "mainpagetable.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define TABLE_EDIT(obj)          GTK_CHECK_CAST (obj, table_edit_get_type(), TableEdit)
#define TABLE_EDIT_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, table_edit_get_type (), TableEditClass)
#define IS_TABLE_EDIT(obj)       GTK_CHECK_TYPE (obj, table_edit_get_type ())


	typedef struct _TableEdit TableEdit;
	typedef struct _TableEditClass TableEditClass;


	/* struct for the object's data */
	struct _TableEdit
	{
		GtkVBox object;

		/* environment information */
		ConfManager *conf;
		DbTable *table;

		/* table name */
		GtkWidget *te_table_name;
		GtkWidget *te_table_comments;

		/* selected field in the main CList */
		DbField *field;
		GtkWidget *te_field;
		
		GtkWidget *frame;

		/* table fields */
		GtkWidget *clist_table_fields;
		GtkWidget *box_table_parents;

		/* GList for the available sequences list */
		GtkWidget *combobox;	/* sequences list */
		GList *seqlist;
	};

	/* struct for the object's class */
	struct _TableEditClass
	{
		GtkVBoxClass parent_class;
	};

	/* generic widget's functions */
	guint table_edit_get_type (void);
	GtkWidget *table_edit_new (ConfManager * conf, DbTable * t);
	/* CB */
	void table_edit_drop_table_cb (GtkObject * obj, DbTable * table,
				       gpointer data);


	/********************/
	/* HELPER functions */
	/********************/

	/* creates a TableEdit object and puts it into a GnomeDialog */
	GtkWidget *table_edit_dialog_new (ConfManager * conf,
					  DbTable * t);
#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
