/* conf-manager.h
 *
 * Copyright (C) 2001 - 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __CONF_MANAGER_H_
#define __CONF_MANAGER_H_

#include <gnome.h>
#include <config.h>
#include <dlfcn.h>

#include "server-access.h"
#include "database.h"
#include <libgnomeprint/gnome-printer.h>
#include <libgnomeprint/gnome-print.h>
#include <libgnomeprint/gnome-printer-dialog.h>


#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

#define CONF_MANAGER(obj)          GTK_CHECK_CAST (obj, conf_manager_get_type(), ConfManager)
#define CONF_MANAGER_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, conf_manager_get_type (), ConfManagerClass)
#define IS_CONF_MANAGER(obj)       GTK_CHECK_TYPE (obj, conf_manager_get_type ())


	typedef struct _ConfManager ConfManager;
	typedef struct _ConfManagerClass ConfManagerClass;


	/* struct for the object's data */
	struct _ConfManager
	{
		GtkObject object;

		/* main interface widgets */
		GtkWidget           *app;
		GtkWidget           *appbar;
		GtkWidget           *error_dlg;
		GtkWidget           *manager_bonobo_win;	/* to run the gnome-db manager */
		GtkWidget           *relations_dialog;

		/* pages in the notebook */
		GtkWidget           *welcome;
		GtkWidget           *nb;		/* notebook */
		GtkWidget           *working_box;	/* hb which contains the shortcut and the notebook */
		GtkWidget           *tables_page;
		GtkWidget           *sequences_page;
		GtkWidget           *queries_page;

		/* menu widgets */
		GSList              *widgets_show_on_connect;
		GSList              *widgets_show_on_disconnect;
		GSList              *widgets_sensitive_on_connect;
		GSList              *widgets_sensitive_on_disconnect;

		/* Main objects */
		ServerAccess        *srv;
		Database            *db;
		GtkWidget           *config_dlg;
		gboolean             conn_open_requested;	/* set to TRUE if we want to open the
								   connection right after it is configured */
		GtkWidget           *config_plugins_dlg;
		GtkObject           *top_query; /* NULL if conn. not opened */

		/* serial counters */
		guint                id_serial;	/* to identify in a unique way each query, it is
						   used as a sequence. Each new query
						   that uses it makes it being increased by one. */

		/* file to open, ... */
		gchar               *working_file;
		gboolean             save_up_to_date;
		gboolean             close_after_saving;
		gboolean             quit_after_saving;
		gboolean             loading_in_process;

		/* plugins */
		gchar               *plugins_dir;

		/* Clipboard management */
		gchar               *clipboard_str;
		GtkObject           *clipboard_obj;

		/* integrity check */
		GtkWidget           *check_dlg;	/* GnomeDialog if opened */
		GtkWidget           *check_pbar;	/* progress bar for the checking */
		GtkWidget           *check_link_name;	/* label to hold the current link name */
		GtkWidget           *check_errors;	/* clist for the errors */
		gboolean             check_perform;

		/* config dialogs with bonobo controls inside */
		GtkWidget           *users_list_dlg;
		GtkWidget           *users_groups_dlg;
		GtkWidget           *users_acl_dlg;

		/* printing support */
		GnomePrinter        *printer;
		GnomePrintContext   *printcontext;

		/* initial preferences */
		gchar               *datasource;
		gchar               *user_name;
		gchar               *user_passwd;
		gchar               *file_to_open;
		
		/* QueryEnvs created by the ConfManager which are removed if they have not
		   been modified, if they are the only Env and if the query is moved out of the 
		   Queries tree */
		GSList              *created_envs;
	};

	/* struct for the object's class */
	struct _ConfManagerClass
	{
		GtkObjectClass parent_class;
		
		/* Warning: tells that a Database object is or is not useable, not that there is
		   or is not anymore a database on the DBM server */
		void (*database_added)   (ConfManager * conf, Database *db);
		void (*database_removed) (ConfManager * conf, Database *db);
		
		/* signal the addition or removal of a query in the queries tree,
		   not when a new Query object is created or when it is destroyed. */
		void (*query_added)      (ConfManager * conf, GtkObject * new_query);
		void (*query_removed)    (ConfManager * conf, GtkObject * old_query);

		void (*env_added)      (ConfManager * conf, GtkObject * env);
		void (*env_removed)    (ConfManager * conf, GtkObject * env);
	};

	/* generic object's functions */
	guint      conf_manager_get_type                         (void);
	GtkObject *conf_manager_new                              (void);

	/* to be called once the CORBA stuff has been initialized */
	void       conf_manager_finalize                         (ConfManager *conf);
	
	
	/*
	 * the widgets given here will be automatically shown/hidden
	 * or their sensitiveness modified upon connection/deconnection
	 */
	void       conf_manager_register_show_on_connect         (ConfManager * c, GtkWidget * wid);
	void       conf_manager_register_show_on_disconnect      (ConfManager * c, GtkWidget * wid);
	void       conf_manager_register_sensitive_on_connect    (ConfManager * c, GtkWidget * wid);
	void       conf_manager_register_sensitive_on_disconnect (ConfManager * c, GtkWidget * wid);
	
	/* returns an unique number for this conf structure */
	guint      conf_manager_get_id_serial                    (ConfManager * c);

	/* returns a string for the main window's title bar which will have to be freed */
	gchar     *conf_manager_get_title                        (ConfManager * c);


	/* Clipboard management */
	/* See the documentation for more details on how to use the functions */
	void       conf_manager_set_clipboard_str                (ConfManager *conf, gchar *str);
	gchar     *conf_manager_get_clipboard_str                (ConfManager *conf);
	void       conf_manager_set_clipboard_object             (ConfManager *conf, GtkObject *obj);
	GtkObject *conf_manager_get_clipboard_object             (ConfManager *conf);

#ifdef __cplusplus
}
#endif				/* __cplusplus */

#ifdef debug
#define D_COL_NOR "\033[0m"
#define D_COL_H0 "\033[;34;7m"
#define D_COL_H1 "\033[;36;7m"
#define D_COL_H2 "\033[;36;4m"
#define D_COL_OK "\033[;32m"
#define D_COL_ERR "\033[;31;1m"
#define AAA(X) g_print (D_COL_H1 "DEBUG MARK %d\n" D_COL_NOR, X)
#endif

#endif
