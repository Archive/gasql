/* gasql.c
 *
 * Copyright (C) 1999-2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include <gnome.h>
#include <tree.h>		/* libxml/tree.h ? */
#include <parser.h>		/* libxml/tree.h ? */
#include <liboaf/liboaf.h>
#include <bonobo.h>

#include "session.h"
#include "server-access.h"
#include "server-rs.h"
#include "database.h"
#include "sqlwiddbtree.h"
#include "query.h"

/* configuration of the general data */
#include "conf-manager.h"

/* main interface callbacks */
/*#include "interface_cb.h"*/

#include <gnome-db.h>


static gint prepare_app (ConfManager * conf);

extern int xmlDoValidityCheckingDefaultValue;





extern gint set_opened_file (ConfManager * conf, gchar * filetxt);
static gint
prepare_app (ConfManager * conf)
{
	GnomeClient *client;
	gchar *str;

	/* create the objects now that the Corba stuff is initialized */
	conf_manager_finalize (conf);

	gtk_widget_show (conf->app);


	/* session management */
	client = gnome_master_client ();
	gtk_signal_connect (GTK_OBJECT (client), "save_yourself",
			    GTK_SIGNAL_FUNC (session_save_state_cb), conf);
	gtk_signal_connect (GTK_OBJECT (client), "die",
			    GTK_SIGNAL_FUNC (session_die_cb), NULL);


	/* getting the previously saved parameters: TO BE REPLACED BY GCONF  */
	gnome_config_push_prefix ("/gASQL/SqlServer/");
	str = gnome_config_get_string ("plugins_dir");
	if (str)
		conf->plugins_dir = str;
	else
		conf->plugins_dir = g_strdup (PLUGINSINSTALLDIR);
	gnome_config_pop_prefix ();
	server_access_rescan_display_plugins (conf->srv, conf->plugins_dir);


	/* analysis of command line parameters */
	if (conf->datasource)
		g_string_assign (conf->srv->gda_datasource, conf->datasource);
	if (conf->user_name)
		g_string_assign (conf->srv->user_name, conf->user_name);
	if (conf->user_passwd)
		g_string_assign (conf->srv->password, conf->user_passwd);

	if (conf->file_to_open)
		set_opened_file (conf, conf->file_to_open);

	return 0;		/* => everything OK */
}


int
main (int argc, char *argv[])
{
	ConfManager *conf;
	/* Command line options */
	poptContext return_ctx;
	const char **args;
	gchar *ds = NULL, *user = NULL, *pass = NULL;

	struct poptOption options[] = {
		{"datasource", 'd', POPT_ARG_STRING, &ds, 0,
		 N_("datasource name"),
		 N_("SOURCE")},
		{"user", 'u', POPT_ARG_STRING, &user, 0, N_("user name"),
		 N_("USER-NAME")},
		{"password", 'p', POPT_ARG_STRING, &pass, 0, N_("password"),
		 N_("PASSWORD")},
		{NULL, '\0', 0, NULL, 0}	/* end the list */
	};

	/* i18n and i10n */
	bindtextdomain (PACKAGE, PACKAGE_LOCALE_DIR);
	textdomain (PACKAGE);

	/* CORBA and GNOME init */
	gda_init (PACKAGE, VERSION, argc, argv);
	gnome_init_with_popt_table (PACKAGE,	/* app_id */
				    VERSION,	/* app_version */
				    argc, argv, options,	/* options */
				    0,	/* popt_flags */
				    &return_ctx);	/* return_ctx */

	/*if (bonobo_init (gda_corba_get_orb (), NULL, NULL) == FALSE)
		g_error (_("Could not initialize bonobo"));
	bonobo_activate();*/
	
	/* to make gASQL work with libxml >= V1.8.8 and 2.x */
	LIBXML_TEST_VERSION conf = CONF_MANAGER (conf_manager_new ());

	/* taking care of the command line arguments */
	if (ds)
		conf->datasource = g_strdup (ds);
	if (user)
		conf->user_name = g_strdup (user);
	if (pass)
		conf->user_passwd = g_strdup (pass);

	/* remaining arguments on the command line */
	args = poptGetArgs (return_ctx);
	if (args) {
		if (args[0]) {	/* opens the specified file */
			conf->file_to_open = g_strdup (args[0]);
		}
	}

	/* XML DTD checking */
	xmlDoValidityCheckingDefaultValue = 0;


	/*
	 * We can't make any CORBA calls unless we're in the main
	 * loop. So we delay creating the container here.
	 */
	gtk_idle_add ((GtkFunction) prepare_app, conf);
	bonobo_main ();

	return 0;
}
