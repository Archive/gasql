/* packedclist.c
 *
 * Copyright (C) 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "packedclist.h"

static void packed_clist_class_init (PackedCListClass * class);
static void packed_clist_init (PackedCList * wid);
static void packed_clist_destroy (GtkObject * object);

enum
{
	OBJECTS_SWAPPED,
	LAST_SIGNAL
};

static gint packed_clist_signals[LAST_SIGNAL] = { 0 };


guint
packed_clist_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Packed_Clist",
			sizeof (PackedCList),
			sizeof (PackedCListClass),
			(GtkClassInitFunc) packed_clist_class_init,
			(GtkObjectInitFunc) packed_clist_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_hbox_get_type (), &f_info);
	}

	return f_type;
}

static void
packed_clist_class_init (PackedCListClass * class)
{
	GtkObjectClass *object_class = NULL;

	object_class = (GtkObjectClass *) class;
	packed_clist_signals[OBJECTS_SWAPPED] =
		gtk_signal_new ("objects_swapped",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (PackedCListClass,
						   objects_swapped),
				gtk_marshal_NONE__POINTER_POINTER, GTK_TYPE_NONE, 2,
				GTK_TYPE_POINTER, GTK_TYPE_POINTER);

	gtk_object_class_add_signals (object_class, packed_clist_signals,
				      LAST_SIGNAL);
	class->objects_swapped = NULL;
	object_class->destroy = packed_clist_destroy;
}

static void
packed_clist_init (PackedCList * wid)
{
	wid->actual_selection = NULL;
	wid->arrows_box = NULL;
	wid->arrow_up = NULL;
	wid->arrow_down = NULL;
}

/* filalize the widget setup */
static void packed_clist_post_init (PackedCList * wid, gint columns, gchar *titles[],
				    gboolean buttons_go_right);

GtkWidget *
packed_clist_new (gint columns, gboolean buttons_go_right)
{
	return packed_clist_new_with_titles(columns, NULL, buttons_go_right);
}


GtkWidget *
packed_clist_new_with_titles (gint columns, gchar *titles[],
			      gboolean buttons_go_right)
{
	GtkObject *obj;
	PackedCList *clist;

	obj = gtk_type_new (packed_clist_get_type ());
	clist = PACKED_CLIST (obj);

	/* building the actual interface */
	packed_clist_post_init (clist, columns, titles, buttons_go_right);

	return GTK_WIDGET (obj);
}

static void up_button_click_cb (GtkWidget *button, PackedCList * clist);
static void dn_button_click_cb (GtkWidget *button, PackedCList * clist);
static void clist_select_row_cb (GtkWidget *real_clist, 
				 gint row, gint column, GdkEvent *event,
				 PackedCList * clist);
static void clist_unselect_row_cb (GtkWidget *real_clist, 
				   gint row, gint column, GdkEvent *event,
				   PackedCList * clist);
static void 
packed_clist_post_init (PackedCList * clist, gint columns, gchar *titles[],
			gboolean buttons_go_right)
{
	GtkWidget *bb, *button, *arrow, *sw, *real_clist;
	gint i;
	
	/* arrows button box */
	bb = gtk_vbutton_box_new ();
	gtk_button_box_set_child_size (GTK_BUTTON_BOX (bb), 15, 15);
	gtk_container_set_border_width (GTK_CONTAINER (bb), GNOME_PAD);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);
	if (buttons_go_right)
		gtk_box_pack_end (GTK_BOX (clist), bb, FALSE, TRUE, 0);
	else
		gtk_box_pack_start (GTK_BOX (clist), bb, FALSE, TRUE, 0);
	clist->arrows_box = bb;

	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bb), button);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (up_button_click_cb),
			    clist);
	arrow = gtk_arrow_new (GTK_ARROW_UP, GTK_SHADOW_OUT);
	gtk_container_add (GTK_CONTAINER (button), arrow);
	clist->arrow_up = button;

	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (bb), button);
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (dn_button_click_cb),
			    clist);
	arrow = gtk_arrow_new (GTK_ARROW_DOWN, GTK_SHADOW_OUT);
	gtk_container_add (GTK_CONTAINER (button), arrow);
	clist->arrow_down = button;

	gtk_widget_show_all (bb);

	/* real clist in its scrolled window */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_ALWAYS);
	gtk_box_pack_start (GTK_BOX (clist), sw, TRUE, TRUE, 0);

	real_clist = gtk_clist_new_with_titles (columns, titles);
	gtk_clist_set_selection_mode (GTK_CLIST (real_clist),
				      GTK_SELECTION_SINGLE);
	gtk_container_add (GTK_CONTAINER (sw), real_clist);
	clist->clist = real_clist;

	for (i = 0; i < columns; i++) 
		gtk_clist_set_column_auto_resize (GTK_CLIST (real_clist), i, TRUE);

	gtk_clist_column_titles_passive (GTK_CLIST (real_clist));
	gtk_clist_set_reorderable (GTK_CLIST (real_clist), FALSE);

	gtk_signal_connect (GTK_OBJECT (real_clist), "select_row",
			    GTK_SIGNAL_FUNC (clist_select_row_cb), clist);
	gtk_signal_connect (GTK_OBJECT (real_clist), "unselect_row",
			    GTK_SIGNAL_FUNC (clist_unselect_row_cb), clist);

	gtk_widget_show_all (sw);

	/* initial sensitiveness of the arrow buttons */
	gtk_widget_set_sensitive (clist->arrow_up, FALSE);
	gtk_widget_set_sensitive (clist->arrow_down, FALSE);
}

static void objects_move(PackedCList * clist, gint offset);

static void 
up_button_click_cb (GtkWidget *button, PackedCList * clist)
{
	objects_move(clist, -1);
}

static void 
dn_button_click_cb (GtkWidget *button, PackedCList * clist)
{
	objects_move(clist, 1);
}

static void
objects_move(PackedCList * clist, gint offset)
{
	gint row;
	gpointer data;
	
	row = gtk_clist_find_row_from_data (GTK_CLIST (clist->clist),
					    clist->actual_selection);
	if (row == -1)
		return;

	data = gtk_clist_get_row_data (GTK_CLIST (clist->clist), row + offset);

	if (!data)
		g_warning ("data != NULL failed");

	gtk_clist_swap_rows (GTK_CLIST (clist->clist), row, row + offset);
	packed_clist_refresh_arrows(clist);

#ifdef debug_signal
	g_print (">> 'OBJECTS_SWAPPED' from packedclist->up_button_click_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (clist), packed_clist_signals[OBJECTS_SWAPPED], 
			 clist->actual_selection, data);
#ifdef debug_signal
	g_print ("<< 'OBJECTS_SWAPPED' from packedclist->up_button_click_cb\n");
#endif
}

static void 
clist_select_row_cb (GtkWidget *real_clist, 
		     gint row, gint column, GdkEvent *event,
		     PackedCList * clist)
{
	gboolean sens_up = TRUE, sens_dn = TRUE;

	clist->actual_selection = gtk_clist_get_row_data (GTK_CLIST (real_clist), row);

	if (row == 0) {
		sens_up = FALSE;
	}
	if (row == GTK_CLIST (real_clist)->rows-1) {
		sens_dn = FALSE;
	}

	gtk_widget_set_sensitive (clist->arrow_up, sens_up);
	gtk_widget_set_sensitive (clist->arrow_down, sens_dn);
}

static void 
clist_unselect_row_cb (GtkWidget *real_clist, 
		       gint row, gint column, GdkEvent *event,
		       PackedCList * clist)
{
	clist->actual_selection = NULL;
	gtk_widget_set_sensitive (clist->arrow_up, FALSE);
	gtk_widget_set_sensitive (clist->arrow_down, FALSE);
}

static void
packed_clist_destroy (GtkObject * object)
{
	PackedCList *clist;
	GtkObjectClass *parent_class = NULL;

	parent_class = gtk_type_class (gtk_hbox_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_PACKED_CLIST (object));

	clist = PACKED_CLIST (object);

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

void 
packed_clist_refresh_arrows (PackedCList *clist)
{
	if (clist->actual_selection) {
		gboolean sens_up = TRUE, sens_dn = TRUE;
		gint row;
		row = gtk_clist_find_row_from_data (GTK_CLIST (clist->clist),
						    clist->actual_selection);
		if (row == 0) {
			sens_up = FALSE;
		}
		if (row == GTK_CLIST (clist->clist)->rows-1) {
			sens_dn = FALSE;
		}
		
		gtk_widget_set_sensitive (clist->arrow_up, sens_up);
		gtk_widget_set_sensitive (clist->arrow_down, sens_dn);
	}
}

void
packed_clist_set_show_arrows (PackedCList *clist, gboolean show)
{
	g_return_if_fail (clist != NULL);
	g_return_if_fail (IS_PACKED_CLIST (clist));

	if (show)
		gtk_widget_show (clist->arrows_box);
	else
		gtk_widget_hide (clist->arrows_box);
}
