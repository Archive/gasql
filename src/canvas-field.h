/* canvas-field.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __CANVAS_FIELD__
#define __CANVAS_FIELD__

#include <gnome.h>
#include "canvas-query-view.h"


#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */


/* QueryView item for the canvas.  
 *
 * In addition to the GnomeCanvasGroup and CanvasBase arguments, the following object arguments 
 * are available::
 *
 * name                 type                    read/write      description
 * ------------------------------------------------------------------------------------------
 * canvas_query_view    pointer                 RW              The CanvasQueryView being displayed
 * field                pointer                 RW              The field being displayed (QueryField or DbField)
 * 
 * 
 * NOTE: the "canvas_query_view" argument is required.
 * 
 */


#define CANVAS_FIELD(obj)          GTK_CHECK_CAST (obj, canvas_field_get_type(), CanvasField)
#define CANVAS_FIELD_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, canvas_field_get_type (), CanvasFieldClass)
#define IS_CANVAS_FIELD(obj)       GTK_CHECK_TYPE (obj, canvas_field_get_type ())


	typedef struct _CanvasField CanvasField;
	typedef struct _CanvasFieldClass CanvasFieldClass;


	/* struct for the object's data */
	struct _CanvasField
	{
		CanvasBase          object;

		/* objects being represented */
		CanvasQueryView    *cqv;
		gpointer            field; /* either QueryField or DbField */

		/* UI building information */
		GnomeCanvasItem    *bg;
		GnomeCanvasItem    *text;
	};

	/* struct for the object's class */
	struct _CanvasFieldClass
	{
		CanvasBaseClass     parent_class;
	};

	/* generic widget's functions */
	guint      canvas_field_get_type      (void);

	void       canvas_field_set_highlight (CanvasField *cf, gboolean highlight);
#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
