/* canvas-base.h
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __CANVAS_BASE__
#define __CANVAS_BASE__

#include <gnome.h>
#include "config.h"

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */

/*
 * 
 * CanvasBase item: an item with the following capabilities:
 *    - can be moved around and emits the "moved" signal after the move.
 *    - is being put on top when the user clicks on it (button 1)
 *    - manage dragging operation (if allowed to)
 *
 * The following arguments are available (added to the ones from GnomeCanvasGroup):
 * name                 type                    read/write      description
 * ------------------------------------------------------------------------------------------
 * allow_move           boolean                 RW              Allow the item to be moved
 * allow_drag           boolean                 RW              The item can initiate a grag process
 * tooltip_object       pointer                 RW              The object to give to the tooltip 
 *                                                              canvas item, NULL if no tooltip
 * 
 * NOTE: 1 - any of "allow_move" and "allow_drag" props MUST be set.
 *       2 - these 2 props can't be TRUE at the same time.
 * 
 */


#define CANVAS_BASE(obj)          GTK_CHECK_CAST (obj, canvas_base_get_type(), CanvasBase)
#define CANVAS_BASE_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, canvas_base_get_type (), CanvasBaseClass)
#define IS_CANVAS_BASE(obj)       GTK_CHECK_TYPE (obj, canvas_base_get_type ())


	typedef struct _CanvasBase      CanvasBase;
	typedef struct _CanvasBaseClass CanvasBaseClass;


	/* struct for the object's data */
	struct _CanvasBase
	{
		GnomeCanvasGroup    object;

		gboolean            moving;
		double              xstart;
		double              ystart;
		gboolean            allow_move;
		gboolean            allow_drag;
		GtkObject          *tooltip;
	};

	/* struct for the object's class */
	struct _CanvasBaseClass
	{
		GnomeCanvasGroupClass parent_class;

		void (*moved)        (CanvasBase * cb);
		void (*moved_cont)   (CanvasBase * cb);
		void (*drag_action)  (CanvasBase * cb, CanvasBase * dragged_from, CanvasBase * dragged_to);

		/* signals to be caught by the children to update theit display */
		void (*enter_notify) (CanvasBase * cb);
		void (*leave_notify) (CanvasBase * cb);
	};

	/* generic widget's functions */
	guint      canvas_base_get_type (void);




/*
 * 
 * "Drag item" GnomeCanvas item: a CanvasBase item which is used to represent
 * an element being dragged, and destroys itself when the mouse button is released
 *
 */


#define CANVAS_DRAG_CURSOR(obj)          GTK_CHECK_CAST (obj, canvas_drag_cursor_get_type(), CanvasDragCursor)
#define CANVAS_DRAG_CURSOR_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, canvas_drag_cursor_get_type (), CanvasDragCursorClass)
#define IS_CANVAS_DRAG_CURSOR(obj)       GTK_CHECK_TYPE (obj, canvas_drag_cursor_get_type ())


	typedef struct _CanvasDragCursor      CanvasDragCursor;
	typedef struct _CanvasDragCursorClass CanvasDragCursorClass;


	/* struct for the object's data */
	struct _CanvasDragCursor
	{
		CanvasBase          object;

		GnomeCanvasItem    *item;
	};

	/* struct for the object's class */
	struct _CanvasDragCursorClass
	{
		CanvasBaseClass parent_class;
	};

	/* generic widget's functions */
	guint      canvas_drag_cursor_get_type (void);




/*
 * 
 * "Tooltip item" GnomeCanvas item: a CanvasGroup item which is used to present
 * a tip when the mouse does not move for a time (handled by the CanvasBase object)
 *
 */


#define CANVAS_TIP(obj)          GTK_CHECK_CAST (obj, canvas_tip_get_type(), CanvasTip)
#define CANVAS_TIP_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, canvas_tip_get_type (), CanvasTipClass)
#define IS_CANVAS_TIP(obj)       GTK_CHECK_TYPE (obj, canvas_tip_get_type ())


	typedef struct _CanvasTip      CanvasTip;
	typedef struct _CanvasTipClass CanvasTipClass;


	/* struct for the object's data */
	struct _CanvasTip
	{
		GnomeCanvasGroup    object;

		GtkObject          *tip_obj;

		/* presentation parameters */
		gdouble             x_text_space;
		gdouble             y_text_space;
	};

	/* struct for the object's class */
	struct _CanvasTipClass
	{
		GnomeCanvasGroupClass parent_class;
	};

	/* generic widget's functions */
	guint      canvas_tip_get_type (void);




#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
