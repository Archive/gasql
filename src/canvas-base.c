/* canvas-base.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#include "canvas-base.h"
#include "database.h"
#include "query.h"

/*
 * 
 * CanvasBase object
 * 
 */


static void canvas_base_class_init (CanvasBaseClass * class);
static void canvas_base_init       (CanvasBase * item);
static void canvas_base_destroy    (GtkObject * object);

static void canvas_base_set_arg    (GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id);
static void canvas_base_get_arg    (GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id);

enum
{
	MOVED,
	MOVED_CONT,
	DRAG_ACTION,
	ENTER_NOTIFY,
	LEAVE_NOTIFY,
	LAST_SIGNAL
};

enum
{
	ARG_0,
	ARG_ALLOW_MOVE,
	ARG_ALLOW_DRAG,
	ARG_TOOLTIP_OBJ
};

static gint canvas_base_signals[LAST_SIGNAL] = { 0, 0, 0, 0, 0 };


guint
canvas_base_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"CanvasBase",
			sizeof (CanvasBase),
			sizeof (CanvasBaseClass),
			(GtkClassInitFunc) canvas_base_class_init,
			(GtkObjectInitFunc) canvas_base_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gnome_canvas_group_get_type (), &f_info);
	}

	return f_type;
}

static void
canvas_base_class_init (CanvasBaseClass * class)
{
	GtkObjectClass *object_class = NULL;


	object_class = (GtkObjectClass *) class;
	canvas_base_signals[MOVED] =
		gtk_signal_new ("moved",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (CanvasBaseClass,  moved),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	canvas_base_signals[MOVED_CONT] =
		gtk_signal_new ("moved_cont",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (CanvasBaseClass,  moved_cont),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	canvas_base_signals[DRAG_ACTION] =
		gtk_signal_new ("drag_action",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (CanvasBaseClass, drag_action),
				gtk_marshal_NONE__POINTER_POINTER,
				GTK_TYPE_NONE, 2, GTK_TYPE_POINTER,
				GTK_TYPE_POINTER);
	canvas_base_signals[ENTER_NOTIFY] =
		gtk_signal_new ("enter_notify",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (CanvasBaseClass,  enter_notify),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	canvas_base_signals[LEAVE_NOTIFY] =
		gtk_signal_new ("leave_notify",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (CanvasBaseClass,  leave_notify),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);

	gtk_object_class_add_signals (object_class, canvas_base_signals,
				      LAST_SIGNAL);
	class->moved = NULL;
	class->moved_cont = NULL;
	class->drag_action = NULL;
	class->enter_notify = NULL;
	class->leave_notify = NULL;
	object_class->destroy = canvas_base_destroy;

	/* Arguments */
	gtk_object_add_arg_type ("CanvasBase::allow_move", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_ALLOW_MOVE);
	gtk_object_add_arg_type ("CanvasBase::allow_drag", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_ALLOW_DRAG);
	gtk_object_add_arg_type ("CanvasBase::tooltip_object", GTK_TYPE_POINTER, GTK_ARG_READWRITE, ARG_TOOLTIP_OBJ);
	object_class->set_arg = canvas_base_set_arg;
	object_class->get_arg = canvas_base_get_arg;
}

static int item_event(CanvasBase *cb, GdkEvent *event, gpointer data);

static void
canvas_base_init (CanvasBase * item)
{
	item->moving = FALSE;
	item->xstart = 0;
	item->ystart = 0;
	item->allow_move = TRUE;
	item->allow_drag = FALSE;
	item->tooltip = NULL;
	
	gtk_signal_connect(GTK_OBJECT(item),"event",
			   GTK_SIGNAL_FUNC(item_event), NULL);
}


static void
canvas_base_destroy (GtkObject * object)
{
	GtkObjectClass *parent_class = NULL;


	parent_class = gtk_type_class (gnome_canvas_group_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CANVAS_BASE (object));

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


static void tooltip_destroy_cb (GtkObject *tooltip, CanvasBase *cb);
static void 
canvas_base_set_arg    (GtkObject            *object,
			GtkArg               *arg,
			guint                 arg_id)
{
	CanvasBase *cb;
	gpointer obj;

	cb = CANVAS_BASE (object);

	switch (arg_id) {
	case ARG_ALLOW_MOVE:
		cb->allow_move = GTK_VALUE_BOOL (*arg);
		if (cb->allow_move && cb->allow_drag)
			cb->allow_drag = FALSE;
		break;
	case ARG_ALLOW_DRAG:
		cb->allow_drag = GTK_VALUE_BOOL (*arg);
		if (cb->allow_drag && cb->allow_move)
			cb->allow_move = FALSE;
		break;
	case ARG_TOOLTIP_OBJ:
		obj = GTK_VALUE_POINTER (*arg);
		if (!obj) {
			if (cb->tooltip) {
				gtk_signal_disconnect_by_func (cb->tooltip,
							       GTK_SIGNAL_FUNC (tooltip_destroy_cb),
							       cb);
				cb->tooltip = NULL;
			}
		}
		else {
			if (GTK_IS_OBJECT (obj)) {
				cb->tooltip = GTK_OBJECT (obj);
				gtk_signal_connect_while_alive (GTK_OBJECT (obj), "destroy",
								GTK_SIGNAL_FUNC (tooltip_destroy_cb),
								cb, GTK_OBJECT (cb));
			}
		}
	}

}

static void 
tooltip_destroy_cb (GtkObject *tooltip, CanvasBase *cb)
{
	cb->tooltip = NULL;
}

static void 
canvas_base_get_arg    (GtkObject            *object,
			GtkArg               *arg,
			guint                 arg_id)
{
	g_print("Get Arg\n");
}



static void end_of_drag_cb (GtkObject *cursor, CanvasBase *cb);
static gint add_tip_timeout (CanvasBase *cb);
static gint display_tip_timeout (CanvasBase *cb);
static int 
item_event(CanvasBase *cb, GdkEvent *event, gpointer data)
{
	gboolean done = TRUE;
	GnomeCanvasItem *ci;
	GtkObject *obj;
	guint id;
	gdouble pos;

	switch (event->type) {
	case GDK_ENTER_NOTIFY:
		/* Drag management */
		ci = gtk_object_get_data (GTK_OBJECT (gnome_canvas_root (GNOME_CANVAS_ITEM (cb)->canvas)),
					  "dragged_from");
		if (ci && IS_CANVAS_BASE (ci)) {
#ifdef debug_signal
			g_print (">> 'DRAG_ACTION' from %s::item_event()\n", __FILE__);
#endif
			gtk_signal_emit (GTK_OBJECT (cb), canvas_base_signals[DRAG_ACTION], ci, cb);
#ifdef debug_signal
			g_print ("<< 'DRAG_ACTION' from %s::item_event()\n", __FILE__);
#endif
			gtk_object_set_data (GTK_OBJECT (gnome_canvas_root (GNOME_CANVAS_ITEM (cb)->canvas)),
					     "dragged_from", NULL);
		}
		gtk_signal_emit (GTK_OBJECT (cb), canvas_base_signals[ENTER_NOTIFY]);
		done = TRUE; /* change ? */ 
		break;
	case GDK_LEAVE_NOTIFY:
		/* remove tooltip */
		obj = gtk_object_get_data (GTK_OBJECT (cb), "tip");
                if (obj)
                        gtk_object_destroy (obj);
                id = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (cb), "addtipid"));
                if (id != 0) {
                        gtk_timeout_remove (id);
                        gtk_object_set_data (GTK_OBJECT (cb), "addtipid", NULL);
                }
                id = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (cb), "displaytipid"));
                if (id != 0) {
                        gtk_timeout_remove (id);
                        gtk_object_set_data (GTK_OBJECT (cb), "displaytipid", NULL);
                }
		gtk_signal_emit (GTK_OBJECT (cb), canvas_base_signals[LEAVE_NOTIFY]);
		done = TRUE; /* change ? */
		break;
	case GDK_BUTTON_PRESS:
		switch (((GdkEventButton *) event)->button) {
		case 1:
			if (cb->allow_move) {
				/* movement management */
				gnome_canvas_item_raise_to_top (GNOME_CANVAS_ITEM (cb));
				cb->xstart = ((GdkEventMotion *) event)->x;
				cb->ystart = ((GdkEventMotion *) event)->y;
				cb->moving = TRUE;
			}
			if (cb->allow_drag) {
				/* remove tooltip */
				obj = gtk_object_get_data (GTK_OBJECT (cb), "tip");
				if (obj)
					gtk_object_destroy (obj);
				id = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (cb), "addtipid"));
				if (id != 0) {
					gtk_timeout_remove (id);
					gtk_object_set_data (GTK_OBJECT (cb), "addtipid", NULL);
				}
				id = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (cb), "displaytipid"));
				if (id != 0) {
					gtk_timeout_remove (id);
					gtk_object_set_data (GTK_OBJECT (cb), "displaytipid", NULL);
				}

				/* start dragging */
				gtk_signal_emit (GTK_OBJECT (cb), canvas_base_signals[LEAVE_NOTIFY]);
				ci = gnome_canvas_item_new (GNOME_CANVAS_GROUP (cb),
							    canvas_drag_cursor_get_type (),
							    "x", ((GdkEventButton *) event)->x - 5.,
							    "y", ((GdkEventButton *) event)->y - 5.,
							    "fill_color", "white",
							    NULL);
				CANVAS_BASE (ci)->xstart = ((GdkEventButton *) event)->x;
				CANVAS_BASE (ci)->ystart = ((GdkEventButton *) event)->y;
				CANVAS_BASE (ci)->moving = TRUE;
				gnome_canvas_item_raise_to_top (GNOME_CANVAS_ITEM (ci));
				gnome_canvas_item_reparent (ci, gnome_canvas_root (GNOME_CANVAS_ITEM (cb)->canvas));
				gnome_canvas_item_grab (ci, GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
							NULL, GDK_CURRENT_TIME);
				/* while_alive to take care of the case when an object is destroyed before
				   the end of the drag progess */
				gtk_signal_connect_while_alive (GTK_OBJECT (ci), "destroy",
								GTK_SIGNAL_FUNC (end_of_drag_cb), cb,
								GTK_OBJECT (cb));
			}
			break;
		default:
			break;
		}
		break;
	case GDK_BUTTON_RELEASE:
		if (cb->allow_move) {
			if (cb->moving)
				cb->moving = FALSE;
#ifdef debug_signal
			g_print (">> 'MOVED' from %s::item_event()\n", __FILE__);
#endif
			gtk_signal_emit (GTK_OBJECT (cb), canvas_base_signals[MOVED]);
#ifdef debug_signal
			g_print ("<< 'MOVED' from %s::item_event()\n", __FILE__);
#endif
		}	
		break;
	case GDK_MOTION_NOTIFY:
		if (cb->moving) {
			GdkEventMotion *evm = (GdkEventMotion *) (event);

			g_assert (IS_CANVAS_BASE (cb));
			gnome_canvas_item_move (GNOME_CANVAS_ITEM (cb), 
						(double) evm->x - cb->xstart, 
						(double) evm->y - cb->ystart);
			cb->xstart = ((GdkEventMotion *) event)->x;
			cb->ystart = ((GdkEventMotion *) event)->y;
			gtk_signal_emit (GTK_OBJECT (cb), canvas_base_signals[MOVED_CONT]);
		}
		else {
			if (! gtk_object_get_data (GTK_OBJECT (cb), "tip")) {
				/* set tooltip */
				id = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (cb), "addtipid"));
				if (id != 0) {
					gtk_timeout_remove (id);
					gtk_object_set_data (GTK_OBJECT (cb), "addtipid", NULL);
				}
				id = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (cb), "displaytipid"));
				if (id != 0) {
					gtk_timeout_remove (id);
					gtk_object_set_data (GTK_OBJECT (cb), "displaytipid", NULL);
				}
				id = gtk_timeout_add (200, (GtkFunction) add_tip_timeout, cb);
				gtk_object_set_data (GTK_OBJECT (cb), "addtipid", GUINT_TO_POINTER (id));
				pos = ((GdkEventMotion *)(event))->x;
				gtk_object_set_data (GTK_OBJECT (cb), "mousex", GINT_TO_POINTER ((gint) pos));
				pos = ((GdkEventMotion *)(event))->y;
				gtk_object_set_data (GTK_OBJECT (cb), "mousey", GINT_TO_POINTER ((gint) pos));
			}
		}
		break;

	default:
		done = FALSE;
		break;
	}

	return done;
}

static void 
end_of_drag_cb (GtkObject *cursor, CanvasBase *cb)
{
	gtk_object_set_data (GTK_OBJECT (gnome_canvas_root (GNOME_CANVAS_ITEM (cb)->canvas)), 
			     "dragged_from", cb);
}


static gint add_tip_timeout (CanvasBase *cb)
{
        guint id;

        id = gtk_timeout_add (1000, (GtkFunction) display_tip_timeout, cb);
        gtk_object_set_data (GTK_OBJECT (cb), "addtipid", NULL);
        gtk_object_set_data (GTK_OBJECT (cb), "displaytipid", GUINT_TO_POINTER (id));
        return FALSE;
}


static void tip_destroy (GtkObject *tip, CanvasBase *cb);
static gint display_tip_timeout (CanvasBase *cb)
{
        GnomeCanvasItem *tip;
        gdouble x, y;

	if (cb->tooltip) {
		/* display the tip */
		gtk_object_set_data (GTK_OBJECT (cb), "displaytipid", NULL);
		x = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (cb), "mousex"));
		y = GPOINTER_TO_INT (gtk_object_get_data (GTK_OBJECT (cb), "mousey"));
		
		tip = gnome_canvas_item_new (gnome_canvas_root (GNOME_CANVAS_ITEM (cb)->canvas),
					     canvas_tip_get_type(),
					     "x", x+7.,
					     "y", y+3.,
					     "tooltip_object", cb->tooltip,
					     NULL);
		gtk_object_set_data (GTK_OBJECT (cb), "tip", tip);
		gtk_signal_connect_while_alive (GTK_OBJECT (tip), "destroy",
						GTK_SIGNAL_FUNC (tip_destroy), cb,
						GTK_OBJECT (cb));
	}
	return FALSE;
}

static void tip_destroy (GtkObject *tip, CanvasBase *cb)
{
        gtk_object_set_data (GTK_OBJECT (cb), "tip", NULL);
}












/*
 * 
 * CanvasDragCursor object
 * 
 */

static void canvas_drag_cursor_class_init (CanvasDragCursorClass * class);
static void canvas_drag_cursor_init       (CanvasDragCursor * drag);
static void canvas_drag_cursor_destroy    (GtkObject * object);

static void canvas_drag_cursor_set_arg    (GtkObject            *object,
					   GtkArg               *arg,
					   guint                 arg_id);
static void canvas_drag_cursor_get_arg    (GtkObject            *object,
					   GtkArg               *arg,
					   guint                 arg_id);


enum
{
	ARG_00,
	ARG_FILLCOLOR
};


guint
canvas_drag_cursor_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"CanvasDragCursor",
			sizeof (CanvasDragCursor),
			sizeof (CanvasDragCursorClass),
			(GtkClassInitFunc) canvas_drag_cursor_class_init,
			(GtkObjectInitFunc) canvas_drag_cursor_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (canvas_base_get_type (), &f_info);
	}
	
	return f_type;
}

static void
canvas_drag_cursor_class_init (CanvasDragCursorClass * class)
{
	GtkObjectClass *object_class = NULL;


	object_class = (GtkObjectClass *) class;

	object_class->destroy = canvas_drag_cursor_destroy;

	/* Arguments */
	gtk_object_add_arg_type ("CanvasDragCursor::fill_color", GTK_TYPE_POINTER, GTK_ARG_WRITABLE, ARG_FILLCOLOR);
	object_class->set_arg = canvas_drag_cursor_set_arg;
	object_class->get_arg = canvas_drag_cursor_get_arg;
	
}

static int cursor_item_event(CanvasDragCursor *cur, GdkEvent *event, gpointer data);

static void
canvas_drag_cursor_init (CanvasDragCursor * drag)
{
	drag->item = NULL;

	gtk_signal_connect(GTK_OBJECT(drag),"event",
			   GTK_SIGNAL_FUNC(cursor_item_event), NULL);
}

static int 
cursor_item_event(CanvasDragCursor *cur, GdkEvent *event, gpointer data)
{
	gboolean done = TRUE;
	
	switch (event->type) {
	case GDK_BUTTON_RELEASE:
		gtk_object_destroy (GTK_OBJECT (cur));
		break;
	default:
		done = FALSE;
		break;
	}

	return done;	
}

static void
canvas_drag_cursor_destroy (GtkObject * object)
{
	GtkObjectClass *parent_class = NULL;

	parent_class = gtk_type_class (canvas_base_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CANVAS_DRAG_CURSOR (object));

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static void post_init (CanvasDragCursor * drag);
static void 
canvas_drag_cursor_set_arg    (GtkObject            *object,
			GtkArg               *arg,
			guint                 arg_id)
{
	CanvasDragCursor *cd;
	gchar *str;

	cd = CANVAS_DRAG_CURSOR (object);
	if (!cd->item)
		post_init (cd);

	switch (arg_id) {
	case ARG_FILLCOLOR:
		str = GTK_VALUE_POINTER (*arg);
		gnome_canvas_item_set (cd->item, "fill_color", str, NULL);
		break;
	}

}

static void 
canvas_drag_cursor_get_arg    (GtkObject            *object,
			GtkArg               *arg,
			guint                 arg_id)
{
	g_print ("GetArg %d\n", arg_id);
}



static void 
post_init (CanvasDragCursor * drag)
{
	drag->item = gnome_canvas_item_new(GNOME_CANVAS_GROUP (drag),
					   GNOME_TYPE_CANVAS_RECT,
					   "x1", (double) 0,
					   "y1", (double) 0,
					   "x2", (double) 20,
					   "y2", (double) 7,
					   "outline_color", "black",
					   "fill_color", "white",
					   "width_pixels", 2,
					   NULL);
}









/*
 * 
 * CanvasTip object
 * 
 */


static void canvas_tip_class_init (CanvasTipClass * class);
static void canvas_tip_init       (CanvasTip * tip);
static void canvas_tip_destroy    (GtkObject * object);

static void canvas_tip_set_arg    (GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id);
static void canvas_tip_get_arg    (GtkObject            *object,
				    GtkArg               *arg,
				    guint                 arg_id);
static void canvas_tip_post_init  (CanvasTip * tip);

enum
{
	ARG_TIP0,
	ARG_OBJ
};


guint
canvas_tip_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"CanvasTip",
			sizeof (CanvasTip),
			sizeof (CanvasTipClass),
			(GtkClassInitFunc) canvas_tip_class_init,
			(GtkObjectInitFunc) canvas_tip_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gnome_canvas_group_get_type (), &f_info);
	}

	return f_type;
}

static void
canvas_tip_class_init (CanvasTipClass * class)
{
	GtkObjectClass *object_class = NULL;


	object_class = (GtkObjectClass *) class;

	object_class->destroy = canvas_tip_destroy;

	/* Arguments */
	gtk_object_add_arg_type ("CanvasTip::tooltip_object", GTK_TYPE_POINTER, GTK_ARG_READWRITE, ARG_OBJ);
	object_class->set_arg = canvas_tip_set_arg;
	object_class->get_arg = canvas_tip_get_arg;
	
}

static void
canvas_tip_init (CanvasTip * tip)
{
	tip->tip_obj = NULL;

	tip->x_text_space = 3.;
	tip->y_text_space = 3.;
}


static void
canvas_tip_destroy (GtkObject * object)
{
	GtkObjectClass *parent_class = NULL;


	parent_class = gtk_type_class (gnome_canvas_group_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_CANVAS_TIP (object));

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


static void 
canvas_tip_set_arg    (GtkObject            *object,
			GtkArg               *arg,
			guint                 arg_id)
{
	CanvasTip *tip;
	gpointer obj;

	tip = CANVAS_TIP (object);

	switch (arg_id) {
	case ARG_OBJ:
		obj = GTK_VALUE_POINTER (*arg);
		if (!obj) {
			g_warning ("Tooltip object set to NULL!");
		}
		else {
			if (GTK_IS_OBJECT (obj)) 
				tip->tip_obj = GTK_OBJECT (obj);
		}
		break;
	}

	if (tip->tip_obj)
		canvas_tip_post_init (tip);
}

static void 
canvas_tip_get_arg    (GtkObject            *object,
			GtkArg               *arg,
			guint                 arg_id)
{
	g_print("Get Arg\n");
}


static void build_dbfield_tip (CanvasTip * tip);
static void build_queryjoin_tip (CanvasTip * tip);

static void 
canvas_tip_post_init  (CanvasTip * tip)
{
	gboolean done = FALSE;

	g_return_if_fail (tip->tip_obj);
	if (IS_DB_FIELD (tip->tip_obj)) {
		build_dbfield_tip (tip);
		done = TRUE;
	}
	if (!done && IS_QUERY_JOIN (tip->tip_obj)) {
		build_queryjoin_tip (tip);
		done = TRUE;
	}
	
	if (!done)
		g_warning ("Tooltip: unknown object type\n");
}


#define TIP_DEFAULT_FONT "6x13"

static void 
build_dbfield_tip (CanvasTip * tip)
{
	GnomeCanvasItem *text, *bg;
	DbField *field;
	gchar *str;

	field =  DB_FIELD (tip->tip_obj);
	if (field->default_val)
		str = g_strdup_printf (_("Field type: %s\n"
				       "Def. value: %s"), field->type->sqlname,
				       field->default_val);
	else
		str = g_strdup_printf (_("Field type: %s"), field->type->sqlname);

	text = gnome_canvas_item_new(GNOME_CANVAS_GROUP (tip),
				     GNOME_TYPE_CANVAS_TEXT,
				     "x", tip->x_text_space,
				     "y", tip->y_text_space,
				     "font", TIP_DEFAULT_FONT,
				     "text", str,
				     "justification", GTK_JUSTIFY_LEFT, 
				     "anchor", GTK_ANCHOR_NORTH_WEST,
				     NULL);
	g_free (str);

	bg = gnome_canvas_item_new(GNOME_CANVAS_GROUP (tip),
				   GNOME_TYPE_CANVAS_RECT,
				   "x1", (double) 0,
				   "y1", (double) 0,
				   "x2", text->x2 - text->x1 + 2*tip->x_text_space,
				   "y2", text->y2 - text->y1 + 2*tip->y_text_space,
				   "outline_color", "black",
				   "fill_color", "lightyellow",
				   "width_pixels", 1,
				   NULL);

	gnome_canvas_item_lower_to_bottom (bg);
}

static void 
build_queryjoin_tip (CanvasTip * tip)
{
	GnomeCanvasItem *text, *bg;
	QueryJoin *qj;
	gchar *str;

	qj =  QUERY_JOIN (tip->tip_obj);

	str = g_strdup ("Join tooltip:\nTODO from FM's implementation\n"
			"of the char * functions\n\n"
			"Translate the symbols");
	text = gnome_canvas_item_new(GNOME_CANVAS_GROUP (tip),
				     GNOME_TYPE_CANVAS_TEXT,
				     "x", tip->x_text_space,
				     "y", tip->y_text_space,
				     "font", TIP_DEFAULT_FONT,
				     "text", str,
				     "justification", GTK_JUSTIFY_LEFT, 
				     "anchor", GTK_ANCHOR_NORTH_WEST,
				     NULL);
	g_free (str);

	bg = gnome_canvas_item_new(GNOME_CANVAS_GROUP (tip),
				   GNOME_TYPE_CANVAS_RECT,
				   "x1", (double) 0,
				   "y1", (double) 0,
				   "x2", text->x2 - text->x1 + 2*tip->x_text_space,
				   "y2", text->y2 - text->y1 + 2*tip->y_text_space,
				   "outline_color", "black",
				   "fill_color", "lightyellow",
				   "width_pixels", 1,
				   NULL);

	gnome_canvas_item_lower_to_bottom (bg);
}
