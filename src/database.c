/* database.c
 *
 * Copyright (C) 1999 - 2001 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "database.h"
#include <string.h>
#include "conf-manager.h"

/*
 *
 * Main object: Database
 *
 */

static void database_class_init (DatabaseClass * class);
static void database_init (Database * db);
static void database_destroy (GtkObject * object);

/* "fault" signal callback */
static void database_cb (GtkObject * obj, gpointer data);

/* widget inner callbacks to signals */
static void s_table_created_dropped (Database * db, DbTable * t);
static void s_seq_created_dropped (Database * db, DbSequence * t);
static void s_field_created_dropped (Database * db, DbTable * t,
				     DbField * field);
static void s_fs_link_created_dropped (Database * db, DbSequence * seq,
				       DbField * f);
/* updates the DB structure */
static void database_load_tables (Database * db, ServerAccess * srv);
static void database_load_views (Database * db, ServerAccess * srv);
#ifdef debug
void database_load_clean_unused_tables_views (Database * db, ServerAccess * srv);
#else
static void database_load_clean_unused_tables_views (Database * db, ServerAccess * srv);
#endif
static void database_load_sequences (Database * db, ServerAccess * srv);

/* loads or reloads the fields of a table */
static void db_table_load_fields (DbTable * t,
				       ServerAccess * srv, Database * db);
static void db_field_load_contents (ServerAccess * srv, DbField * field,
					 GdaRecordset * recset);
static void database_catch_field_create_cb (GtkObject * obj,
					  DbField * new_field,
					  gpointer data);
static void database_catch_field_drop_cb (GtkObject * obj, DbField * field,
					gpointer data);

static gpointer serveraccess_binding_func (GtkObject * obj);

/* signals */
enum
{
	UPDATED,
	STRUCT_SAVED,
	FAULT,
	TABLE_CREATED,
	TABLE_CREATED_FILLED,
	TABLE_DROPPED,
	SEQ_CREATED,
	SEQ_DROPPED,
	FIELD_CREATED,
	FIELD_DROPPED,
	FS_LINK_CREATED,
	FS_LINK_DROPPED,
	PROGRESS,
	LASTDB_SIGNAL
};

static gint database_signals[LASTDB_SIGNAL] = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};

GtkType
database_get_type (void)
{
	static GtkType f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"Sql_Db",
			sizeof (Database),
			sizeof (DatabaseClass),
			(GtkClassInitFunc) database_class_init,
			(GtkObjectInitFunc) database_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_object_get_type (), &f_info);
	}

	return f_type;
}

static void
database_class_init (DatabaseClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;
	database_signals[FAULT] =
		gtk_signal_new ("fault",
				GTK_RUN_LAST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass, fault),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	database_signals[STRUCT_SAVED] =
		gtk_signal_new ("struct_saved", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass, struct_saved),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	database_signals[UPDATED] =
		gtk_signal_new ("updated", GTK_RUN_FIRST, object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass, updated),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);

	database_signals[TABLE_CREATED] =
		gtk_signal_new ("table_created",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass, table_created),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	database_signals[TABLE_CREATED_FILLED] =
		gtk_signal_new ("table_created_filled",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass,
						   table_created_f),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	database_signals[TABLE_DROPPED] =
		gtk_signal_new ("table_dropped",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass, table_dropped),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	database_signals[SEQ_CREATED] =
		gtk_signal_new ("seq_created",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass, seq_created),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	database_signals[SEQ_DROPPED] =
		gtk_signal_new ("seq_dropped",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass, seq_dropped),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	database_signals[FIELD_CREATED] =
		gtk_signal_new ("field_created",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass, field_created),
				gtk_marshal_NONE__POINTER_POINTER,
				GTK_TYPE_NONE, 2, GTK_TYPE_POINTER,
				GTK_TYPE_POINTER);

	database_signals[FIELD_DROPPED] =
		gtk_signal_new ("field_dropped",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass, field_dropped),
				gtk_marshal_NONE__POINTER_POINTER,
				GTK_TYPE_NONE, 2, GTK_TYPE_POINTER,
				GTK_TYPE_POINTER);

	database_signals[FS_LINK_CREATED] =
		gtk_signal_new ("fs_link_created",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass,
						   fs_link_created),
				gtk_marshal_NONE__POINTER_POINTER,
				GTK_TYPE_NONE, 2, GTK_TYPE_POINTER,
				GTK_TYPE_POINTER);

	database_signals[FS_LINK_DROPPED] =
		gtk_signal_new ("fs_link_dropped",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass,
						   fs_link_dropped),
				gtk_marshal_NONE__POINTER_POINTER,
				GTK_TYPE_NONE, 2, GTK_TYPE_POINTER,
				GTK_TYPE_POINTER);

	database_signals[PROGRESS] =
		gtk_signal_new ("progress",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DatabaseClass, progress),
				gtk_marshal_NONE__POINTER_UINT_UINT,
				GTK_TYPE_NONE, 3, GTK_TYPE_POINTER,
				GTK_TYPE_UINT, GTK_TYPE_UINT);

	gtk_object_class_add_signals (object_class, database_signals,
				      LASTDB_SIGNAL);
	class->updated = NULL;
	class->struct_saved = NULL;
	class->fault = NULL;
	class->table_created = s_table_created_dropped;
	class->table_created_f = NULL;	/*s_table_created_dropped; */
	class->table_dropped = s_table_created_dropped;
	class->seq_created = s_seq_created_dropped;
	class->seq_dropped = s_seq_created_dropped;
	class->field_created = s_field_created_dropped;
	class->field_dropped = s_field_created_dropped;
	class->fs_link_created = s_fs_link_created_dropped;
	class->fs_link_dropped = s_fs_link_created_dropped;
	class->progress = NULL;
	object_class->destroy = database_destroy;
}

static void
database_init (Database * db)
{
	db->name = NULL;
	db->is_fault = FALSE;
	db->tables = NULL;
	db->sequences = NULL;
	db->users = NULL;
}

GtkObject *
database_new (ServerAccess * srv)
{
	GtkObject *obj;
	Database *db;

	obj = gtk_type_new (database_get_type ());
	db = DATABASE (obj);

	db->srv = srv;

	/* declaring that tables' fields are objects that one can bind plugins to */
	server_access_declare_object_bindable (srv, serveraccess_binding_func);

	return obj;
}

static void database_empty_all (Database * db);
static void
database_destroy (GtkObject * object)
{
	Database *db;
	GtkObject *parent_class = NULL;

	parent_class = gtk_type_class (gtk_object_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_DATABASE (object));

	db = DATABASE (object);

	/* destroy tables and sequences */
	database_empty_all (db);

	/* the name */
	if (db->name) {
		g_free (db->name);
		db->name = NULL;
	}

	/* the users list */
	while (db->users) {
		GSList *hold = db->users;
		g_free (db->users->data);
		db->users = g_slist_remove_link (db->users, db->users);
		g_slist_free_1 (hold);
	}

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

static gpointer
serveraccess_binding_func (GtkObject * obj)
{
	g_return_val_if_fail ((obj != NULL), NULL);
	if (IS_DB_FIELD (obj))
		return DB_FIELD (obj)->type;
	else
		return NULL;
}


static void
database_empty_all (Database * db)
{
	gpointer data;
	GSList *hold;

	/* tables */
	while (db->tables) {
		hold = db->tables;
		data = db->tables->data;
		db->tables = g_slist_remove_link (db->tables, db->tables);
		g_slist_free_1 (hold);
#ifdef debug_signal
		g_print (">> 'TABLE_DROPPED' from database_empty_all\n");
#endif
		gtk_signal_emit (GTK_OBJECT (db),
				 database_signals[TABLE_DROPPED],
				 DB_TABLE (data));
#ifdef debug_signal
		g_print ("<< 'TABLE_DROPPED' from database_empty_all\n");
#endif
		gtk_object_unref (GTK_OBJECT (data));
	}
	db->tables = NULL;

	/* sequences */
	while (db->sequences) {
		hold = db->sequences;
		data = db->sequences->data;
		db->sequences =	g_slist_remove_link (db->sequences, db->sequences);
		g_slist_free_1 (hold);
#ifdef debug_signal
		g_print (">> 'SEQ_DROPPED' from database_empty_all\n");
#endif
		gtk_signal_emit (GTK_OBJECT (db), database_signals[SEQ_DROPPED],
				 DB_SEQUENCE (data));
#ifdef debug_signal
		g_print ("<< 'SEQ_DROPPED' from database_empty_all\n");
#endif
		gtk_object_unref (GTK_OBJECT (data));
	}
	db->sequences = NULL;

}

static void
s_fs_link_created_dropped (Database * db, DbSequence * seq, DbField * f)
{
#ifdef debug_signal
	g_print (">> 'UPDATED' from s_fs_link_created_dropped\n");
#endif
	gtk_signal_emit (GTK_OBJECT (db), database_signals[UPDATED]);
#ifdef debug_signal
	g_print ("<< 'UPDATED' from s_fs_link_created_dropped\n");
#endif
}

static void
s_table_created_dropped (Database * db, DbTable * t)
{
#ifdef debug_signal
	g_print (">> 'UPDATED' from s_table_created_dropped\n");
#endif
	gtk_signal_emit (GTK_OBJECT (db), database_signals[UPDATED]);
#ifdef debug_signal
	g_print ("<< 'UPDATED' from s_table_created_dropped\n");
#endif
}

static void
s_seq_created_dropped (Database * db, DbSequence * t)
{
#ifdef debug_signal
	g_print (">> 'UPDATED' from s_seq_created_dropped\n");
#endif
	gtk_signal_emit (GTK_OBJECT (db), database_signals[UPDATED]);
#ifdef debug_signal
	g_print ("<< 'UPDATED' from s_seq_created_dropped\n");
#endif
}

static void
s_field_created_dropped (Database * db, DbTable * t, DbField * field)
{
#ifdef debug_signal
	g_print (">> 'UPDATED' from s_field_created_dropped\n");
#endif
	gtk_signal_emit (GTK_OBJECT (db), database_signals[UPDATED]);
#ifdef debug_signal
	g_print ("<< 'UPDATED' from s_field_created_dropped\n");
#endif
}

/****************************************/
/*                                      */
/* Callbacks                            */
/*                                      */
/****************************************/

/* this callback is called whenever a table or sequence 
   emits the signal "load_fault" */
static void
database_cb (GtkObject * obj, gpointer data)
{
	DATABASE (data)->is_fault = TRUE;
#ifdef debug_signal
	g_print (">> 'FAULT' from database_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (data), database_signals[FAULT]);
#ifdef debug_signal
	g_print ("<< 'FAULT' from database_cb\n");
#endif
	/* note: The default signal handler for that signal will ecit(1) the
	   application */
}

/* This CB is called whenever a table emits the "field_created" signal, 
   so the DB object can forward that signal and emit the "field_created" signal
   for its listeners. Data points to the DB object */
static void
database_catch_field_create_cb (GtkObject * obj,
			      DbField * new_field, gpointer data)
{
#ifdef debug_signal
	g_print (">> 'FIELD_CREATED' from database_catch_field_create_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (data), database_signals[FIELD_CREATED],
			 obj, new_field);
#ifdef debug_signal
	g_print ("<< 'FIELD_CREATED' from database_catch_field_create_cb\n");
#endif
}

/* This CB is called whenever a table emits the "field_dropped" signal, 
   so the DB object can forward that signal and emit the "field_dropped" signal
   for its listeners. Data points to the DB object */
static void
database_catch_field_drop_cb (GtkObject * obj,
			    DbField * field, gpointer data)
{
	/* emit signal */
#ifdef debug_signal
	g_print (">> 'FIELD_DROPPED' from database_catch_field_drop_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (data), database_signals[FIELD_DROPPED],
			 obj, field);
#ifdef debug_signal
	g_print ("<< 'FIELD_DROPPED' from database_catch_field_drop_cb\n");
#endif
}

/****************************************/
/*                                      */
/* Tables and fields manipulation       */
/*                                      */
/****************************************/
GSList *
find_user_name (Database * db, gchar * uname)
{
	GSList *list;
	GSList *found = NULL;

	g_return_val_if_fail ((!db->is_fault), NULL);

	list = db->users;
	while (!found && list) {
		if (!strcmp (uname, (gchar *) (list->data))) {
			found = list;
		}
		list = g_slist_next (list);
	}
	if (!found) {
		db->users = g_slist_append (db->users, g_strdup (uname));
		found = g_slist_last (db->users);
	}

	return found;
}

DbTable *
database_find_table_from_name (Database * db, gchar * name)
{
	GSList *list = db->tables;

	g_return_val_if_fail ((!db->is_fault), NULL);

	while (list && (strcmp (name, ((DbTable *) (list->data))->name))) {
		list = g_slist_next (list);
	}
	if (list)
		return (DbTable *) (list->data);
	else
		return NULL;
}

DbTable *
database_find_table_from_xml_name (Database * db, gchar * xmlname)
{
	return database_find_table_from_name (db, xmlname + 2);
}

DbSequence *
database_find_sequence_by_name (Database * db, gchar * name)
{
	GSList *list = db->sequences;

	g_return_val_if_fail ((!db->is_fault), NULL);

	while (list && (strcmp (name, ((DbSequence *) (list->data))->name))) {
		list = g_slist_next (list);
	}
	if (list)
		return (DbSequence *) (list->data);
	else
		return NULL;
}

DbSequence *
database_find_sequence_by_xml_name (Database * db, gchar * xmlname)
{
	return database_find_sequence_by_name (db, xmlname + 2);
}

DbSequence *
database_find_sequence_to_field (Database * db, DbField * field)
{
	GSList *list;
	DbSequence *seq = NULL;

	g_return_val_if_fail ((!db->is_fault), NULL);

	list = db->sequences;
	while (list && !seq) {
		if (g_slist_find
		    (DB_SEQUENCE (list->data)->field_links, field))
			seq = DB_SEQUENCE (list->data);
		else
			list = g_slist_next (list);
	}
	return seq;
}

DbField *
database_find_field_from_names (Database * db, gchar * table, gchar *field)
{
	DbTable *tab;
	DbField *f = NULL;
	GSList *list;

	g_return_val_if_fail ((!db->is_fault), NULL);

	tab = database_find_table_from_name (db, table);
	if (tab) {
		list = tab->fields;
		while (list && !f) {
			if (!strcmp (DB_FIELD (list->data)->name, field))
				f = DB_FIELD (list->data);
			list = g_slist_next (list);
		}
	}
	return f;
}

DbField *
database_find_field_from_xml_name (Database * db, gchar *xmlname)
{
	gchar *ptr, *cpy;
	DbField *field;
	cpy = g_strdup (xmlname);
	ptr = strstr (cpy, ":FI");
	*ptr = 0;
	field = database_find_field_from_names (db, cpy + 2, ptr + 3);
	g_free (cpy);
	return field;
}


DbTable *
database_find_table_from_field (Database * db, DbField * field)
{
	DbTable *table = NULL;
	GSList *tlist = db->tables;
	GSList *flist;

	g_return_val_if_fail ((!db->is_fault), NULL);

	while (tlist && !table) {
		flist = ((DbTable *) (tlist->data))->fields;
		while (flist && !table) {
			if (flist->data == field)
				table = (DbTable *) (tlist->data);
			flist = g_slist_next (flist);
		}
		tlist = g_slist_next (tlist);
	}

	return table;
}

guint
database_get_field_order (Database * db, DbTable * table, DbField * field)
{
	DbTable *tab;
	GSList *list;
	guint order, i;

	g_return_val_if_fail ((!db->is_fault), 0);

	if (!table)
		tab = database_find_table_from_field (db, field);
	else
		tab = table;

	if (!tab)		/* field does not belong to any table */
		return 0;

	list = tab->fields;
	order = 0;
	i = 0;
	while (list && !order) {
		i++;
		if (list->data == field)
			order = i;
		list = g_slist_next (list);
	}
	return order;
}

void
database_refresh (Database * db, ServerAccess * srv)
{
	g_return_if_fail ((!db->is_fault));

	/* the update procedure */
	if (server_access_is_open (srv)) {
		database_load_tables (db, srv);
		database_load_views (db, srv);
		database_load_clean_unused_tables_views (db, srv);
		database_load_sequences (db, srv);
	}
	else
		g_warning ("Database::database_refresh() -> connection closed!\n");

#ifdef debug_signal
	g_print (">> 'UPDATED' from database_refresh\n");
#endif
	gtk_signal_emit (GTK_OBJECT (db), database_signals[UPDATED]);
#ifdef debug_signal
	g_print ("<< 'UPDATED' from database_refresh\n");
#endif
}

static void
database_load_tables (Database * db, ServerAccess * srv)
{
	DbTable *table;
	GSList *list, *list2;
	gboolean created;
	GdaRecordset *rst;
	GdaField *field;
	gchar *str;
	guint now, total;

	g_return_if_fail ((!db->is_fault));

	if (db->name) {
		g_free (db->name);
		db->name = NULL;
	}
	db->name = g_strdup (srv->gda_datasource->str);

	/* getting the tables list */
	rst = gda_connection_open_schema (GDA_CONNECTION (srv),
					  GDA_Connection_GDCN_SCHEMA_TABLES,
					  GDA_Connection_EXTRA_INFO, "t",
					  GDA_Connection_no_CONSTRAINT);

	if (rst) {
		/* FIXME: use the supports() function to see if the total number of
		   tuples can be got that way */
		total = rst->affected_rows;

		now = 0;
		gda_recordset_move (rst, 1, 0);
		while (!gda_recordset_eof (rst)) {
			now++;
			field = gda_recordset_field_idx (rst, 0);
			str = gda_stringify_value (NULL, 0, field);	/* table name */
			table = database_find_table_from_name (db, str);
			if (!table) {	/* the table doesn't already exist, create it */
				table = DB_TABLE (db_table_new ());
				table->name = str;
				str = NULL;

				gtk_signal_connect (GTK_OBJECT (table),
						    "load_fault",
						    GTK_SIGNAL_FUNC
						    (database_cb), db);
				gtk_signal_connect (GTK_OBJECT (table),
						    "field_created",
						    GTK_SIGNAL_FUNC
						    (database_catch_field_create_cb),
						    db);
				gtk_signal_connect (GTK_OBJECT (table),
						    "field_dropped",
						    GTK_SIGNAL_FUNC
						    (database_catch_field_drop_cb),
						    db);
				db->tables =
					g_slist_append (db->tables, table);
				created = TRUE;
			}
			else {
				created = FALSE;
			}
			if (str)
				g_free (str);

			/* (re)computes the comments */
			field = gda_recordset_field_idx (rst, 2);	/* comments */
			if (field && gda_field_get_string_value (field)) {
				str = gda_stringify_value (NULL, 0, field);
				db_table_set_comments (table, NULL, str,
							    FALSE);
				g_free (str);
			}
			/* (re)computes the owner */
			field = gda_recordset_field_idx (rst, 1);	/* owner */
			if (field && gda_field_get_string_value (field)) {
				GSList *list;
				str = gda_stringify_value (NULL, 0, field);
				list = find_user_name (db, str);
				table->owner = (gchar *) (list->data);
				g_free (str);
			}

			if (created) {	/* to emit signals now because then owner and comments filled */
#ifdef debug_signal
				g_print (">> 'TABLE_CREATED' from database_load_tables\n");
#endif
				gtk_signal_emit (GTK_OBJECT (db),
						 database_signals
						 [TABLE_CREATED], table);
#ifdef debug_signal
				g_print ("<< 'TABLE_CREATED' from database_load_tables\n");
#endif
			}

			db_table_load_fields (table, srv, db);
			if (created) {
#ifdef debug_signal
				g_print (">> 'TABLE_CREATED_FILLED' from database_load_tables\n");
#endif
				gtk_signal_emit (GTK_OBJECT (db),
						 database_signals
						 [TABLE_CREATED_FILLED],
						 table);
#ifdef debug_signal
				g_print ("<< 'TABLE_CREATED_FILLED' from database_load_tables\n");
#endif
			}
			/* mark the table as updated */
			((DbItem *) table)->updated = TRUE;

			gtk_signal_emit_by_name (GTK_OBJECT (db), "progress",
						 _
						 ("Updating the list of tables..."),
						 now, total);
			gda_recordset_move_next (rst);
		}
		gda_recordset_free (rst);

		if (!(db->is_fault)) {
			/***************************************************************/
			/* 2nd step: re-compute the parents table list for every table */
			/***************************************************************/
			list = db->tables;
			while (list) {
				if ((DB_TABLE (list->data)->is_view ==
				     FALSE)
				    && (DB_ITEM (list->data)->updated ==
					TRUE)) {
					g_slist_free (DB_TABLE
						      (list->data)->parents);
					DB_TABLE (list->data)->parents =
						NULL;
					/* get the parents' list */
					rst = gda_connection_open_schema
						(GDA_CONNECTION (srv),
						 GDA_Connection_GDCN_SCHEMA_TAB_PARENTS,
						 GDA_Connection_OBJECT_NAME,
						 DB_TABLE (list->data)->
						 name,
						 GDA_Connection_no_CONSTRAINT);

					if (rst) {
						list2 = NULL;
						gda_recordset_move (rst, 1,
								    0);
						while (!gda_recordset_eof
						       (rst)) {
							field = gda_recordset_field_idx (rst, 0);
							str = gda_stringify_value (NULL, 0, field);
							table = database_find_table_from_name (db, str);
							g_free (str);
							if (table)
								list2 = g_slist_append (list2, table);
							gda_recordset_move_next
								(rst);
						}
						DB_TABLE (list->data)->
							parents = list2;
						gda_recordset_free (rst);
					}
				}
				list = g_slist_next (list);
			}
		}
	}
	else {
		/* An error occured, the db emits the fault signal */
#ifdef debug_signal
		g_print (">> 'FAULT' from database_load_tables\n");
#endif
		gtk_signal_emit (GTK_OBJECT (db), database_signals[FAULT]);
#ifdef debug_signal
		g_print ("<< 'FAULT' from database_load_tables\n");
#endif
	}
	/* if an error occurs, the DB remains non accessible */
	gtk_signal_emit_by_name (GTK_OBJECT (db), "progress", NULL, 0, 0);
}

static void
database_load_views (Database * db, ServerAccess * srv)
{
	DbTable *table;
	gboolean created;
	GdaRecordset *rst;
	GdaField *field;
	gchar *str;
	guint now, total;

	g_return_if_fail ((!db->is_fault));

	if (db->name) {
		g_free (db->name);
		db->name = NULL;
	}
	db->name = g_strdup (srv->gda_datasource->str);

	/* getting the tables list */
	rst = gda_connection_open_schema (GDA_CONNECTION (srv),
					  GDA_Connection_GDCN_SCHEMA_VIEWS,
					  GDA_Connection_EXTRA_INFO, "t",
					  GDA_Connection_no_CONSTRAINT);

	if (rst) {
		/* FIXME: use the supports() function to see if the total number of
		   tuples can be got that way */
		total = rst->affected_rows;

		now = 0;
		gda_recordset_move (rst, 1, 0);
		while (!gda_recordset_eof (rst)) {
			now++;
			field = gda_recordset_field_idx (rst, 0);
			str = gda_stringify_value (NULL, 0, field);	/* table name */
			table = database_find_table_from_name (db, str);
			if (!table) {	/* the table doesn't already exist, create it */
				table = DB_TABLE (db_table_new ());
				table->name = str;
				str = NULL;

				gtk_signal_connect (GTK_OBJECT (table),
						    "load_fault",
						    GTK_SIGNAL_FUNC
						    (database_cb), db);
				gtk_signal_connect (GTK_OBJECT (table),
						    "field_created",
						    GTK_SIGNAL_FUNC
						    (database_catch_field_create_cb),
						    db);
				gtk_signal_connect (GTK_OBJECT (table),
						    "field_dropped",
						    GTK_SIGNAL_FUNC
						    (database_catch_field_drop_cb),
						    db);
				db->tables =
					g_slist_append (db->tables, table);
				created = TRUE;
			}
			else {
				created = FALSE;
			}
			if (str)
				g_free (str);

			/* set it here because otherwise if an object was a table and 
			   becomes a view, this won't be set correctly */
			table->is_view = TRUE;

			/* (re)computes the comments */
			field = gda_recordset_field_idx (rst, 2);	/* comments */
			if (gda_field_get_string_value (field)) {
				str = gda_stringify_value (NULL, 0, field);
				db_table_set_comments (table, NULL, str, FALSE);
				g_free (str);
			}
			/* (re)computes the owner */
			field = gda_recordset_field_idx (rst, 1);	/* owner */
			if (gda_field_get_string_value (field)) {
				GSList *list;
				str = gda_stringify_value (NULL, 0, field);
				list = find_user_name (db, str);
				table->owner = (gchar *) (list->data);
				g_free (str);
			}

			if (created) {	/* to emit signals now because then owner and comments filled */
#ifdef debug_signal
				g_print (">> 'TABLE_CREATED' from database_load_tables\n");
#endif
				gtk_signal_emit (GTK_OBJECT (db),
						 database_signals
						 [TABLE_CREATED], table);
#ifdef debug_signal
				g_print ("<< 'TABLE_CREATED' from database_load_tables\n");
#endif
			}
			db_table_load_fields (table, srv, db);
			if (created) {
#ifdef debug_signal
				g_print (">> 'TABLE_CREATED_FILLED' from database_load_tables\n");
#endif
				gtk_signal_emit (GTK_OBJECT (db),
						 database_signals
						 [TABLE_CREATED_FILLED],
						 table);
#ifdef debug_signal
				g_print ("<< 'TABLE_CREATED_FILLED' from database_load_tables\n");
#endif
			}
			/* mark the table as updated */
			((DbItem *) table)->updated = TRUE;

			gtk_signal_emit_by_name (GTK_OBJECT (db), "progress",
						 _("Updating the list of views..."),
						 now, total);
			gda_recordset_move_next (rst);
		}
		gda_recordset_free (rst);
	}
	gtk_signal_emit_by_name (GTK_OBJECT (db), "progress", NULL, 0, 0);
}

#ifdef debug
void
#else
static void
#endif
database_load_clean_unused_tables_views (Database * db, ServerAccess * srv)
{
	GSList *list, *list2, *list_hold;

	g_return_if_fail ((!db->is_fault));

	list = db->tables;
	while (list) {
		if (!DB_ITEM (list->data)->updated) {
			GtkObject *obj;
			/* we want to emit the signal "table_dropped" with a pointer to 
			   a table which still exists, but is not anymore in the list 
			   of tables in the DB. */
			obj = GTK_OBJECT (list->data);
			/* we first remove all the links to any field of that table */
			list2 = DB_TABLE (obj)->fields;
			while (list2) {
				database_delete_seq_field_link (db, NULL, DB_FIELD
								(list2->data));
				list2 = g_slist_next (list2);
			}
			list_hold = g_slist_next (list);
			list2 = list;
			db->tables = g_slist_remove_link (db->tables, list);
			g_slist_free_1 (list2);
			list = list_hold;
#ifdef debug_signal
			g_print (">> 'TABLE_DROPPED' from database_load_tables\n");
#endif
			gtk_signal_emit (GTK_OBJECT (db),
					 database_signals[TABLE_DROPPED], obj);
#ifdef debug_signal
			g_print ("<< 'TABLE_DROPPED' from database_load_tables\n");
#endif
			gtk_object_destroy (obj);
		}
		else {
			DB_ITEM (list->data)->updated = FALSE;
			list = g_slist_next (list);
		}
	}
}


static void
database_load_sequences (Database * db, ServerAccess * srv)
{
	DbSequence *seq;
	GSList *list, *list_hold, *hold;
	GdaRecordset *rs;
	GdaField *field;
	gchar *str;
	guint now, total;
	gboolean created;

	g_return_if_fail ((!db->is_fault));

	/* 1: getting the sequence list */
	rs = gda_connection_open_schema (GDA_CONNECTION (srv),
					 GDA_Connection_GDCN_SCHEMA_SEQUENCES,
					 GDA_Connection_EXTRA_INFO, "t",
					 GDA_Connection_no_CONSTRAINT);
	if (rs) {
		/* FIXME: use the supports() function to see if the total number of
		   tuples can be got that way */
		total = rs->affected_rows;

		now = 0;
		gda_recordset_move (rs, 1, 0);
		while (!gda_recordset_eof (rs)) {
			now++;
			field = gda_recordset_field_idx (rs, 0);
			str = gda_stringify_value (NULL, 0, field);
			created = FALSE;
			seq = database_find_sequence_by_name (db, str);
			if (!seq) {
				seq = DB_SEQUENCE (db_sequence_new ());
				seq->name = str;
				str = NULL;
				gtk_signal_connect (GTK_OBJECT (seq),
						    "load_fault",
						    GTK_SIGNAL_FUNC
						    (database_cb), db);
				db->sequences =
					g_slist_append (db->sequences, seq);
				created = TRUE;
			}
			if (str)
				g_free (str);

			/* (re)computes the comments */
			field = gda_recordset_field_idx (rs, 2);	/* comments */
			if (gda_field_get_string_value (field)) {
				str = gda_stringify_value (NULL, 0, field);	/* str allocated */
				if (seq->comments)
					g_free (seq->comments);
				seq->comments = str;
			}

			/* (re)computes the owner */
			field = gda_recordset_field_idx (rs, 1);	/* owner */
			if (gda_field_get_string_value (field)) {
				GSList *list;
				str = gda_stringify_value (NULL, 0, field);
				list = find_user_name (db, str);
				seq->owner = (gchar *) (list->data);
				g_free (str);
			}

			if (created) {	/* then emit a signal */
#ifdef debug_signal
				g_print (">> 'SEQ_CREATED' from database_load_sequences (%s)\n", seq->name);
#endif
				gtk_signal_emit (GTK_OBJECT (db),
						 database_signals[SEQ_CREATED],
						 seq);
#ifdef debug_signal
				g_print ("<< 'SEQ_CREATED' from database_load_sequences\n");
#endif
			}

			/* mark the sequence as updated */
			DB_ITEM (seq)->updated = TRUE;
			gtk_signal_emit_by_name (GTK_OBJECT (db), "progress",
						 _
						 ("Updating the list of sequences..."),
						 now, total);
			gda_recordset_move_next (rs);
		}
		gda_recordset_free (rs);

		/* 2nd step: destroy sequences that do not exist anymore */
		list = db->sequences;
		while (list) {
			if (!DB_ITEM (list->data)->updated) {
				GtkObject *obj;

				/* we want to emit the "seq_dropped" signal with a DbSequence 
				   object
				   which still exists but is not anymore in the Database 
				   structure */
				obj = GTK_OBJECT (list->data);
				/* we want to remove all the links from that sequence first */
				database_delete_seq_field_link (db,
							      DB_SEQUENCE
							      (obj), NULL);

				list_hold = g_slist_next (list);
				hold = list;
				db->sequences = g_slist_remove_link (db->sequences, list);
				g_slist_free_1 (hold);
				list = list_hold;
				/* let's first remove any link from that sequence */
				database_delete_seq_field_link (db,
							      DB_SEQUENCE
							      (obj), NULL);
#ifdef debug_signal
				g_print (">> 'SEQ_DROPPED' from database_load_sequences (%s)\n", DB_SEQUENCE (obj)->name);
#endif
				gtk_signal_emit (GTK_OBJECT (db),
						 database_signals[SEQ_DROPPED],
						 obj);
#ifdef debug_signal
				g_print ("<< 'SEQ_DROPPED' from database_load_sequences\n");
#endif
				gtk_object_destroy (obj);
			}
			else {
				DB_ITEM (list->data)->updated = FALSE;
				list = g_slist_next (list);
			}
		}
	}
	else {
		/* An error occured, the db emits the fault signal, if the
		 * sequences are supported */
		if (srv->features.sequences) {
#ifdef debug_signal
			g_print (">> 'FAULT' from database_load_sequences\n");
#endif
			gtk_signal_emit (GTK_OBJECT (db),
					 database_signals[FAULT]);
#ifdef debug_signal
			g_print ("<< 'FAULT' from database_load_sequences\n");
#endif
		}
	}
	/* if an error occurs, the DB remains non accessible */
	gtk_signal_emit_by_name (GTK_OBJECT (db), "progress", NULL, 0, 0);
}


#ifdef debug
/****************************************/
/*                                      */
/* Dumping the tables and structure     */
/*                                      */
/****************************************/

void
database_dump_tables (Database * db)
{
	GSList *list, *plist;

	g_return_if_fail ((!db->is_fault));

	list = db->tables;
	g_print ("*** Dumping Database Structure ***\n");
	while (list) {
		g_print ("  TABLE %s", DB_TABLE (list->data)->name);
		if (DB_TABLE (list->data)->parents) {
			g_print (", parents= ");
			plist = DB_TABLE (list->data)->parents;
			while (plist) {
				g_print ("%s ",
					 DB_TABLE (plist->data)->name);
				plist = g_slist_next (plist);
			}
			g_print (".\n");
		}
		else
			g_print (", no known parents.\n");
		db_table_dump_fields (DB_TABLE (list->data));
		list = g_slist_next (list);
	}
}

void
database_dump_links (Database * db)
{
	GSList *list, *list2;

	g_return_if_fail ((!db->is_fault));

	g_print ("*** Dumping Sequences Relations ***\n");
	list = db->sequences;
	while (list) {
		list2 = DB_SEQUENCE (list->data)->field_links;
		while (list2) {
			g_print ("\t%-25s --> %s.%s\n",
				 DB_SEQUENCE (list->data)->name,
				 database_find_table_from_field (db,
							       DB_FIELD
							       (list2->
								data))->name,
				 DB_FIELD (list2->data)->name);
			list2 = g_slist_next (list2);
		}
		list = g_slist_next (list);
	}
}

#endif


/* if the link does not already exists, insert the link between the
   sequence and the field */
void
database_insert_seq_field_link (Database * db, DbSequence * seq,
			      DbField * field)
{
	GSList *list;
	gboolean found = FALSE;

	g_return_if_fail ((!db->is_fault));

	if ((database_find_sequence_by_name (db, seq->name) == seq) &&
	    (database_find_table_from_field (db, field) != NULL)) {
		list = seq->field_links;
		while (list && !found) {
			if (list->data == field)
				found = TRUE;
			list = g_slist_next (list);
		}
		if (!found) {	/* then insert it */
			seq->field_links =
				g_slist_append (seq->field_links, field);
			/* emit a signal */
#ifdef debug_signal
			g_print (">> 'FS_LINK_CREATED' from database_insert_seq_field_link\n");
#endif
			gtk_signal_emit (GTK_OBJECT (db),
					 database_signals[FS_LINK_CREATED], seq,
					 field);
#ifdef debug_signal
			g_print ("<< 'FS_LINK_CREATED' from database_insert_seq_field_link\n");
#endif
		}
	}
	else
		g_warning
			("Database::database_insert_seq_field_link\tyou are trying to access "
			 "the fields of the wrong Database structure!\n");
}

/* removes from memory the specified link or all links if seq==NULL or
   field==NULL */
void
database_delete_seq_field_link (Database * db, DbSequence * seq,
			      DbField * field)
{
	GSList *list;
	DbField *f;

	g_return_if_fail ((!db->is_fault));

	if (field)
		if (((seq
		      && (database_find_sequence_by_name (db, seq->name) == seq)
		      && (database_find_table_from_field (db, field) != NULL)))
		    || (!seq
			&& (database_find_table_from_field (db, field) !=
			    NULL))) {
			if (seq) {
				seq->field_links =
					g_slist_remove (seq->field_links,
							field);
#ifdef debug_signal
				g_print (">> 'FS_LINK_DROPPED' from database_delete_seq_field_link " "1 (%s->%s)\n", seq->name, field->name);
#endif
				gtk_signal_emit (GTK_OBJECT (db),
						 database_signals
						 [FS_LINK_DROPPED], seq,
						 field);
#ifdef debug_signal
				g_print ("<< 'FS_LINK_DROPPED' from database_delete_seq_field_link\n");
#endif
			}
			else {
				list = db->sequences;
				while (list) {
					if (g_slist_find
					    (DB_SEQUENCE (list->data)->
					     field_links, field)) {
						DB_SEQUENCE (list->data)->
							field_links =
							g_slist_remove
							(DB_SEQUENCE
							 (list->data)->
							 field_links, field);
#ifdef debug_signal
						g_print (">> 'FS_LINK_DROPPED' from database_delete_seq_field_link " "2 (%s->%s)\n", DB_SEQUENCE (list->data)->name, field->name);
#endif
						gtk_signal_emit (GTK_OBJECT
								 (db),
								 database_signals
								 [FS_LINK_DROPPED],
								 DB_SEQUENCE
								 (list->data),
								 field);
#ifdef debug_signal
						g_print ("<< 'FS_LINK_DROPPED' " "from database_delete_seq_field_link\n");
#endif
					}
					list = g_slist_next (list);
				}
			}
		}
		else
			g_warning
				("Database::database_delete_seq_field_link\nyou are "
				 "trying to access "
				 "the fields of the wrong Database structure!\n");
	else /* field = NULL */ if (seq) {
		while (seq->field_links) {
			GSList *hold;
			f = DB_FIELD (seq->field_links->data);
			hold = seq->field_links;
			seq->field_links =
				g_slist_remove_link (seq->field_links, seq->field_links);
			g_slist_free_1 (hold);
#ifdef debug_signal
			g_print (">> 'FS_LINK_DROPPED' from database_delete_seq_field_link " "(%s->%s)\n", seq->name, f->name);
#endif
			gtk_signal_emit (GTK_OBJECT (db),
					 database_signals[FS_LINK_DROPPED], seq,
					 f);
#ifdef debug_signal
			g_print ("<< 'FS_LINK_DROPPED' from database_delete_seq_field_link\n");
#endif
		}
		seq->field_links = NULL;
	}
}


/*************************************************/
/*                                               */
/* XML storage of the Database object            */
/*                                               */
/*************************************************/
static void db_table_load_from_xml_node (Database * db,
					      xmlNodePtr tabletree);
static void db_sequence_load_from_xml_node (Database * db, xmlNodePtr tabletree);
/* to store Database object */
void
database_build_xml_tree (Database * db, xmlDocPtr doc)
{
	xmlNodePtr toptree, tree, subtree, subsubtree;
	GSList *list, *list2;
	DbTable *table;
	DbSequence *seq;
	DbField *field;
	gint order;
	gchar *str;
	ServerDataType *datatype = NULL;

	g_return_if_fail ((!db->is_fault));

	/* main node */
	toptree = xmlNewChild (doc->xmlRootNode, NULL, "DATABASE", NULL);
	xmlSetProp (toptree, "name", db->name);

	/* tables */
	tree = xmlNewChild (toptree, NULL, "TABLES", NULL);
	list = db->tables;
	while (list) {
		table = DB_TABLE (list->data);
		subtree = xmlNewChild (tree, NULL, "table", NULL);
		str = db_table_get_xml_id (table);
		xmlSetProp (subtree, "name", str);
		g_free (str);
		if (table->comments)
			xmlSetProp (subtree, "comments", table->comments);
		if (table->is_user_comments)
			xmlSetProp (subtree, "user_comments", "t");
		if (table->owner)
			xmlSetProp (subtree, "owner", table->owner);
		if (table->is_view)
			xmlSetProp (subtree, "is_view", "t");

		/* table's inheritance */
		list2 = table->parents;
		order = 1;
		while (list2) {
			subsubtree =
				xmlNewChild (subtree, NULL, "parent", NULL);
			str = db_table_get_xml_id (DB_TABLE (list2->data)),
			xmlSetProp (subsubtree, "table", str);
			g_free (str);
			str = g_strdup_printf ("%d", order);
			xmlSetProp (subsubtree, "order", str);
			g_free (str);
			order++;
			list2 = g_slist_next (list2);
		}

		/* table's fields */
		list2 = table->fields;
		while (list2) {
			SqlDataDisplayFns *fns;

			field = DB_FIELD (list2->data);
			subsubtree =
				xmlNewChild (subtree, NULL, "field", NULL);
			str = db_field_get_xml_id (field, db);
			xmlSetProp (subsubtree, "name", str);
			g_free (str);
			datatype = field->type;
			str = g_strdup_printf ("DT%s", datatype->sqlname);
			xmlSetProp (subsubtree, "type", str);
			g_free (str);
			/* if ((datatype->numparams >= 1) && (field->length > 0)) { */
/* 	str = g_strdup_printf("%d", field->length); */
/* 	xmlSetProp(subsubtree, "length", str); */
/* 	g_free(str); */
/*       } */
			str = g_strdup_printf ("%d", field->length);
			xmlSetProp (subsubtree, "length", str);
			g_free (str);

			if (field->null_allowed)
				xmlSetProp (subsubtree, "notnull", "f");
			else
				xmlSetProp (subsubtree, "notnull", "t");
			if (field->is_key)
				xmlSetProp (subsubtree, "iskey", "t");
			else
				xmlSetProp (subsubtree, "iskey", "f");
			if (field->default_val)
				xmlSetProp (subsubtree, "default",
					    field->default_val);

			/* if there is a plugin for that field, write it */
			if ((fns =
			     g_hash_table_lookup (db->srv->types_objects_hash,
						  field))) {
				xmlSetProp (subsubtree, "plugin",
					    fns->plugin_name);
			}
			list2 = g_slist_next (list2);
		}
		list = g_slist_next (list);
	}

	/* Sequences */
	tree = xmlNewChild (toptree, NULL, "SEQUENCES", NULL);

	list = db->sequences;
	while (list) {
		seq = DB_SEQUENCE (list->data);
		subtree = xmlNewChild (tree, NULL, "sequence", NULL);
		str = db_sequence_get_xml_id (seq);
		xmlSetProp (subtree, "name", str);
		g_free (str);
		if (seq->comments)
			xmlSetProp (subtree, "comments", seq->comments);
		if (seq->owner)
			xmlSetProp (subtree, "owner", seq->owner);

		/* sequence links */
		list2 = seq->field_links;
		while (list2) {
			field = DB_FIELD (list2->data);
			table = database_find_table_from_field (db, field);
			if (table) {
				subsubtree =
					xmlNewChild (subtree, NULL, "seq_link",
						     NULL);
				str = db_field_get_xml_id (field, db);
				xmlSetProp (subsubtree, "field", str);
				g_free (str);
			}
			list2 = g_slist_next (list2);
		}
		list = g_slist_next (list);
	}
}

/* to load sqldb object 
   the Database object MUST NOT have any table, sequence or field_links. 
   The XML document MUST be a valid one regarding the DTD.
   Otherwise the result is not known!
*/
gboolean
database_build_db_from_xml_tree (Database * db, xmlNodePtr node)
{
	xmlNodePtr tree, subtree;
	GSList *list, *sublist;

	g_return_val_if_fail ((!db->is_fault), FALSE);

	if (db->tables || db->sequences) {
		gnome_error_dialog (_
				    ("INTERNAL ERROR:\nTrying to load an Database object "
				     "which already have some data inside. Clean it "
				     "before."));
		return FALSE;
	}
	/* sets the DB name */
	if (db->name)
		g_free (db->name);
	db->name = xmlGetProp (node, "name");

	tree = node->xmlChildrenNode;
	while (tree) {
		/* Tables loading */
		if (!strcmp (tree->name, "TABLES")) {
			subtree = tree->xmlChildrenNode;
			while (subtree) {
				db_table_load_from_xml_node (db,
								  subtree);
				subtree = subtree->next;
			}
		}

		/* Sequences Loading */
		if (tree && !strcmp (tree->name, "SEQUENCES")) {
			subtree = tree->xmlChildrenNode;
			while (subtree) {
				db_sequence_load_from_xml_node (db, subtree);
				subtree = subtree->next;
			}
		}

		if (tree)
			tree = tree->next;
	}


	/* sending ALL signals */
	list = db->tables;
	while (list) {
#ifdef debug_signal
		g_print (">> 'TABLE_CREATED_FILLED' from database_build_db_from_xml_tree\n");
#endif
		gtk_signal_emit (GTK_OBJECT (db),
				 database_signals[TABLE_CREATED_FILLED],
				 list->data);
#ifdef debug_signal
		g_print ("<< 'TABLE_CREATED_FILLED' from database_build_db_from_xml_tree\n");
#endif
		list = g_slist_next (list);
	}

	list = db->sequences;
	while (list) {
#ifdef debug_signal
		g_print (">> 'SEQ_CREATED' from database_build_db_from_xml_tree\n");
#endif
		gtk_signal_emit (GTK_OBJECT (db), database_signals[SEQ_CREATED],
				 list->data);
#ifdef debug_signal
		g_print ("<< 'SEQ_CREATED' from database_build_db_from_xml_tree\n");
#endif
		sublist = DB_SEQUENCE (list->data)->field_links;
		while (sublist) {
#ifdef debug_signal
			g_print (">> 'FS_LINK_CREATED' from database_build_db_from_xml_tree\n");
#endif
			gtk_signal_emit (GTK_OBJECT (db),
					 database_signals[FS_LINK_CREATED],
					 list->data, sublist->data);
#ifdef debug_signal
			g_print ("<< 'FS_LINK_CREATED' from database_build_db_from_xml_tree\n");
#endif
			sublist = g_slist_next (sublist);
		}
		list = g_slist_next (list);
	}

	return TRUE;
}

/*
 *
 * Object common to memory representation of the DB: DbItems
 * items such as tables, fields, sequences, links inherit this
 * object
 *
 */

static void db_item_class_init (DbItemClass * class);
static void db_item_init (DbItem * mi);
static void db_item_destroy (GtkObject * object);

/* signals */
enum
{
	LOAD_FAULT,
	LAST_SIGNAL
};

static gint db_item_signals[LAST_SIGNAL] = { 0 };


GtkType
db_item_get_type (void)
{
	static GtkType f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"DbItem",
			sizeof (DbItem),
			sizeof (DbItemClass),
			(GtkClassInitFunc) db_item_class_init,
			(GtkObjectInitFunc) db_item_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_object_get_type (), &f_info);
	}

	return f_type;
}

static void
db_item_class_init (DbItemClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;

	db_item_signals[LOAD_FAULT] =
		gtk_signal_new ("load_fault",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DbItemClass,
						   load_fault),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);
	gtk_object_class_add_signals (object_class, db_item_signals,
				      LAST_SIGNAL);
	class->load_fault = NULL;

	object_class->destroy = db_item_destroy;
}

static void
db_item_init (DbItem * mi)
{
	mi->updated = FALSE;
}


GtkObject *
db_item_new (void)
{
	GtkObject *obj;

	obj = gtk_type_new (db_item_get_type ());
	return obj;
}

static void
db_item_destroy (GtkObject * object)
{
	GtkObject *parent_class = NULL;

	parent_class = gtk_type_class (gtk_object_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_DB_ITEM (object));

	/* does not destroy anything here, just a link to the parent object */

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


/*
 *
 * Object representing a table
 *
 */

static void db_table_class_init (DbTableClass * class);
static void db_table_init (DbTable * table);
static void db_table_destroy (GtkObject * object);
static void db_table_cb (GtkObject * obj, gpointer data);

enum
{
	TAB_FIELD_CREATED,
	TAB_FIELD_DROPPED,
	TAB_COMMENTS_CHANGED,
	LASTTA_SIGNAL
};

static gint db_table_signals[LASTTA_SIGNAL] = { 0, 0, 0 };


GtkType
db_table_get_type (void)
{
	static GtkType f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"DbTable",
			sizeof (DbTable),
			sizeof (DbTableClass),
			(GtkClassInitFunc) db_table_class_init,
			(GtkObjectInitFunc) db_table_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (db_item_get_type (), &f_info);
	}

	return f_type;
}

static void
db_table_class_init (DbTableClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;

	db_table_signals[TAB_FIELD_CREATED] =
		gtk_signal_new ("field_created",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DbTableClass,
						   field_created),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);

	db_table_signals[TAB_FIELD_DROPPED] =
		gtk_signal_new ("field_dropped",
				GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DbTableClass,
						   field_dropped),
				gtk_marshal_NONE__POINTER, GTK_TYPE_NONE, 1,
				GTK_TYPE_POINTER);
	db_table_signals[TAB_COMMENTS_CHANGED] =
		gtk_signal_new ("comments_changed", GTK_RUN_FIRST,
				object_class->type,
				GTK_SIGNAL_OFFSET (DbTableClass,
						   comments_changed),
				gtk_signal_default_marshaller, GTK_TYPE_NONE,
				0);

	gtk_object_class_add_signals (object_class, db_table_signals,
				      LASTTA_SIGNAL);

	object_class->destroy = db_table_destroy;
	class->field_created = NULL;
	class->field_dropped = NULL;
	class->comments_changed = NULL;
}

static void
db_table_init (DbTable * table)
{
	table->name = NULL;
	table->parents = NULL;
	table->fields = NULL;
	table->comments = NULL;
	table->is_user_comments = FALSE;
	table->is_view = FALSE;
}


GtkObject *
db_table_new (void)
{
	GtkObject *obj;

	obj = gtk_type_new (db_table_get_type ());
	return obj;
}

static void
db_table_destroy (GtkObject * object)
{
	DbTable *table;
	DbItem *parent_class = NULL;
	GSList *list;

	parent_class = gtk_type_class (db_item_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_DB_TABLE (object));

	table = DB_TABLE (object);
	/* we do free the parent GSList 
	   (only the lists, not the objects refered to) */
	g_slist_free (table->parents);
	table->parents = NULL;
	/* we do free the fields GSList and destroy every field */
	list = table->fields;
	while (list) {
		gtk_object_destroy (GTK_OBJECT (list->data));
		list = g_slist_next (list);
	}
	g_slist_free (table->fields);
	table->fields = NULL;
	/* we free the name */
	if (table->name)
		g_free (table->name);
	table->name = NULL;

	/* free comments */
	if (table->comments)
		g_free (table->comments);
	table->comments = NULL;

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

/* this callback is called whenever a field emits the signal "load_fault" */
static void
db_table_cb (GtkObject * obj, gpointer data)
{
#ifdef debug_signal
	g_print (">> 'LOAD_FAULT' from db_table_cb\n");
#endif
	gtk_signal_emit (GTK_OBJECT (data), db_item_signals[LOAD_FAULT]);
#ifdef debug_signal
	g_print ("<< 'LOAD_FAULT' from db_table_cb\n");
#endif
}

void
db_table_set_comments (DbTable * table,
		       gpointer conf_manager,
		       gchar * comments, gboolean is_user_comments)
{
	ConfManager *conf;

	g_assert (IS_CONF_MANAGER (conf_manager));
	conf = CONF_MANAGER (conf_manager);

	if (is_user_comments ||
	    (!table->is_user_comments && !is_user_comments)) {
		if (table->comments)
			g_free (table->comments);
		table->comments = g_strdup (comments);
		table->is_user_comments = is_user_comments;
		if (conf)
			conf->save_up_to_date = FALSE;
		gtk_signal_emit (GTK_OBJECT (table),
				 db_table_signals[TAB_COMMENTS_CHANGED]);
	}

	if (*(table->comments) == '\0') {
		g_free (table->comments);
		table->comments = NULL;
		table->is_user_comments = FALSE;
	}
}

gchar * 
db_table_get_xml_id (DbTable * table)
{
	gchar *str;

	g_return_val_if_fail (table, NULL);
	g_return_val_if_fail (IS_DB_TABLE (table), NULL);

	str = g_strdup_printf ("TV%s", table->name);
	return str;
}

/****************************************/
/*                                      */
/* Fields manipulation                  */
/*                                      */
/****************************************/

DbField *
db_table_find_field_by_name (DbTable * t, gchar * name)
{
	GSList *list = t->fields;

	while (list && (strcmp (name, DB_FIELD (list->data)->name))) {
		list = g_slist_next (list);
	}
	if (list)
		return DB_FIELD (list->data);
	else
		return NULL;
}

#ifdef debug
void db_table_clean_fields (Database * db, DbTable * t);
#else
static void db_table_clean_fields (Database * db, DbTable * t);
#endif

static void
db_table_load_fields (DbTable * t, ServerAccess * srv, Database * db)
{
	gint i;
	DbField *field;
	GdaRecordset *rs;
	GdaField *gfield;
	gchar *str;

	g_return_if_fail ((!db->is_fault));

	rs = gda_connection_open_schema (GDA_CONNECTION (srv),
					 GDA_Connection_GDCN_SCHEMA_COLS,
					 GDA_Connection_OBJECT_NAME, t->name,
					 GDA_Connection_no_CONSTRAINT);
	if (rs) {
		gda_recordset_move (rs, 1, 0);
		i = 0;
		while (!gda_recordset_eof (rs)) {
			gfield = gda_recordset_field_idx (rs, 0);
			str = gda_stringify_value (NULL, 0, gfield);
			field = db_table_find_field_by_name (t, str);
			g_free (str);
			if (field)
				db_field_load_contents (srv, field, rs);
			else {
				field = DB_FIELD (db_field_new ());
				gtk_signal_connect (GTK_OBJECT (field),
						    "load_fault",
						    GTK_SIGNAL_FUNC
						    (db_table_cb), t);
				t->fields =
					g_slist_insert (t->fields, field, i);
				/* updating the field's contents */
				db_field_load_contents (srv, field, rs);
#ifdef debug_signal
				g_print (">> 'TAB_FIELD_CREATED' from db_table_load_fields\n");
#endif
				gtk_signal_emit (GTK_OBJECT (t),
						 db_table_signals
						 [TAB_FIELD_CREATED], field);
#ifdef debug_signal
				g_print ("<< 'TAB_FIELD_CREATED' from db_table_load_fields\n");
#endif
			}

			/* mark the field as updated */
			DB_ITEM (field)->updated = TRUE;

			gda_recordset_move_next (rs);
			i++;
		}
		gda_recordset_free (rs);

		/* 2nd step: destroy fields that do not exist anymore */
		db_table_clean_fields (db, t);

	}
}

#ifdef debug
void 
#else
static void
#endif
db_table_clean_fields (Database * db, DbTable * t)
{
	GSList *list, *list_hold, *hold;

	list = t->fields;
	while (list) {
		if (!(DB_ITEM (list->data)->updated)) {
			GtkObject *obj;
			/* we want to emit the "field_dropped" signal with the field
			   structure still existing, but not anymore in the table's list
			   of fields. It is destroyed after the signal has been handled 
			   correctly */
			obj = GTK_OBJECT (list->data);
			
			/* we want to remove any link to that field here, will be done
			   by the Database object at reception of the "field_dropped" signal 
			   from DbTable */
			
			/* removing any link to or from that field, 
			   and any sequence link to that
			   field */
			database_delete_seq_field_link (db, NULL, DB_FIELD (obj));
			
			list_hold = g_slist_next (list);
			hold = list;
			t->fields = g_slist_remove_link (t->fields, list);
			list = list_hold;
			g_slist_free_1 (hold);
#ifdef debug_signal
			g_print (">> 'TAB_FIELD_DROPPED' from db_table_clean_fields\n");
#endif
			gtk_signal_emit (GTK_OBJECT (t), db_table_signals
					 [TAB_FIELD_DROPPED], obj);
#ifdef debug_signal
			g_print ("<< 'TAB_FIELD_DROPPED' from db_table_clean_fields\n");
#endif
			gtk_object_destroy (obj);
		}
		else {
			DB_ITEM (list->data)->updated = FALSE;
			list = g_slist_next (list);
		}
	}
}

/****************************************/
/*                                      */
/* Dumping the fields and structure     */
/*                                      */
/****************************************/

#ifdef debug
void
db_table_dump_fields (DbTable * t)
{
	GSList *list;

	list = t->fields;
	g_print ("\tField name                  Type Length  Other\n");
	while (list) {
		g_print ("\t+ ");
		db_field_dump (DB_FIELD (list->data));
		list = g_slist_next (list);
	}
}
#endif

void
db_table_dump_as_graph (DbTable * t, FILE * st)
{
	GSList *list = t->fields;
	fprintf (st, "node: { title:\"table:%s\"\n", t->name);
	fprintf (st, "        label: \"");

	/* this table's fields */
	fprintf (st, "\\fb\\fu%s\\fn", t->name);
	while (list) {
		fprintf (st, "\\n%s", DB_FIELD (list->data)->name);
		list = g_slist_next (list);
	}
	fprintf (st, "\"\n      }\n");

	/* links from the parent tables */
	list = t->parents;
	while (list) {
		fprintf (st,
			 "edge: { sourcename: \"table:%s\" targetname: \"table:%s\""
			 " linestyle: dashed }\n",
			 DB_TABLE (list->data)->name, t->name);
		list = g_slist_next (list);
	}
}


/* creates the DbTable. 
   the table MUST NOT exist before. */
static void
db_table_load_from_xml_node (Database * db, xmlNodePtr tabletree)
{
	gchar *str;
	xmlNodePtr node;
	DbField *field;
	ServerDataType *type;
	DbTable *table, *t2;
	gboolean used;

	g_return_if_fail ((!db->is_fault));

	table = DB_TABLE (db_table_new ());
	db->tables = g_slist_append (db->tables, table);
	str = xmlGetProp (tabletree, "name");
	table->name = g_strdup (str + 2);
	g_free (str);

	used = FALSE;
	str = xmlGetProp (tabletree, "user_comments");
	if (str) {
		used = (*str == 't') ? TRUE : FALSE;
		g_free (str);
	}

	str = xmlGetProp (tabletree, "comments");
	if (str) {
		db_table_set_comments (table, NULL, str, used);
		g_free (str);
	}

	str = xmlGetProp (tabletree, "owner");
	table->owner = (gchar *) ((find_user_name (db, str))->data);
	g_free (str);

	str = xmlGetProp (tabletree, "is_view");
	if (str) {
		table->is_view = (*str == 't') ? TRUE : FALSE;
		g_free (str);
	}

	gtk_signal_connect (GTK_OBJECT (table), "load_fault",
			    GTK_SIGNAL_FUNC (database_cb), db);
	gtk_signal_connect (GTK_OBJECT (table), "field_created",
			    GTK_SIGNAL_FUNC (database_catch_field_create_cb),
			    db);
	gtk_signal_connect (GTK_OBJECT (table), "field_dropped",
			    GTK_SIGNAL_FUNC (database_catch_field_drop_cb), db);
#ifdef debug_signal
	g_print (">> 'TABLE_CREATED' from db_table_load_from_xml_node\n");
#endif
	gtk_signal_emit (GTK_OBJECT (db), database_signals[TABLE_CREATED],
			 table);
#ifdef debug_signal
	g_print ("<< 'TABLE_CREATED' from db_table_load_from_xml_node\n");
#endif

	node = tabletree->xmlChildrenNode;
	while (node) {
		if (!strcmp (node->name, "parent")) {
			xmlNodePtr pt;
			str = xmlGetProp (node, "table");
			pt = xmlGetID (node->doc, str)->node;
			g_free (str);
			str = xmlGetProp (pt, "name");
			t2 = database_find_table_from_name (db, str + 2);
			g_free (str);
			if (!g_slist_find (table->parents, t2))
				table->parents =
					g_slist_append (table->parents, t2);
		}

		if (!strcmp (node->name, "field")) {
			gchar *ptr;

			str = xmlGetProp (node, "name");
			field = DB_FIELD (db_field_new ());
			gtk_signal_connect (GTK_OBJECT (field), "load_fault",
					    GTK_SIGNAL_FUNC
					    (db_table_cb), table);
			table->fields = g_slist_append (table->fields, field);
			ptr = strstr (str, ":FI");
			field->name = g_strdup (ptr + 3);
			g_free (str);
#ifdef debug_signal
			g_print (">> 'TAB_FIELD_CREATED' from "
				 "db_table_load_from_xml_node\n");
#endif
			gtk_signal_emit (GTK_OBJECT (table),
					 db_table_signals
					 [TAB_FIELD_CREATED], field);
#ifdef debug_signal
			g_print ("<< 'TAB_FIELD_CREATED' from "
				 "db_table_load_from_xml_node\n");
#endif

			str = xmlGetProp (node, "type");
			/* type should not be NULL because of the DTD */
			type = server_data_type_get_from_xml_id (db->srv, str);
			g_free (str);
			field->type = type;

			str = xmlGetProp (node, "length");
			field->length = atoi (str);
			g_free (str);

			str = xmlGetProp (node, "notnull");
			switch (*str) {
			case 't':
				field->null_allowed = FALSE;
				break;
			default:
				field->null_allowed = TRUE;
				break;
			}
			g_free (str);

			str = xmlGetProp (node, "iskey");
			switch (*str) {
			case 't':
				field->is_key = TRUE;
				break;
			default:
				field->is_key = FALSE;
				break;
			}
			g_free (str);

			str = xmlGetProp (node, "default");
			if (str)
				field->default_val = str;

			/* plugin for this data type */
			str = xmlGetProp (node, "plugin");
			if (str) {
				GSList *plist;
				gboolean found = FALSE;
				SqlDataDisplayFns *fns;
				plist = db->srv->data_types_display;
				while (plist && !found) {
					fns = (SqlDataDisplayFns *) (plist->
								     data);
					if ((fns->plugin_name)
					    && !strcmp (fns->plugin_name,
							str)) {
						server_access_bind_object_display
							(db->srv,
							 GTK_OBJECT (field),
							 fns);
						found = TRUE;
					}
					plist = g_slist_next (plist);
				}
				g_free (str);
			}
		}

		node = node->next;
	}
}


/*
 *
 * Object representing a field
 *
 */

static void db_field_class_init (DbFieldClass * class);
static void db_field_init (DbField * field);
static void db_field_destroy (GtkObject * object);

/* No signals */
GtkType
db_field_get_type (void)
{
	static GtkType f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"DbField",
			sizeof (DbField),
			sizeof (DbFieldClass),
			(GtkClassInitFunc) db_field_class_init,
			(GtkObjectInitFunc) db_field_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (db_item_get_type (), &f_info);
	}

	return f_type;
}

static void
db_field_class_init (DbFieldClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;

	object_class->destroy = db_field_destroy;
}

static void
db_field_init (DbField * field)
{
	field->name = NULL;
	field->type = NULL;
	field->type = NULL;
	field->length = 0;
	field->null_allowed = TRUE;
	field->is_key = FALSE;
	field->default_val = NULL;
}


GtkObject *
db_field_new (void)
{
	GtkObject *obj;

	obj = gtk_type_new (db_field_get_type ());
	return obj;
}

static void
db_field_destroy (GtkObject * object)
{
	DbField *field;
	DbItem *parent_class = NULL;

	parent_class = gtk_type_class (db_item_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_DB_FIELD (object));

	field = DB_FIELD (object);

	/* we free the name */
	if (field->name)
		g_free (field->name);
	field->name = NULL;
	field->type = NULL;
	if (field->default_val) {
		g_free (field->default_val);
		field->default_val = NULL;
	}

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}


/****************************************/
/*                                      */
/* Managing the contents of the field   */
/*                                      */
/****************************************/

static void
db_field_load_contents (ServerAccess * srv, DbField * field,
			     GdaRecordset * recset)
{
	gchar *str;
	GdaField *gfield;

	if (field->name)
		g_free (field->name);
	gfield = gda_recordset_field_idx (recset, 0);
	field->name = gda_stringify_value (NULL, 0, gfield);;

	gfield = gda_recordset_field_idx (recset, 1);
	str = gda_stringify_value (NULL, 0, gfield);
	field->type = server_access_get_type_from_name (srv, str);
	g_free (str);
	if (!field->type)
		field->type = server_access_get_type_from_name (srv, "varchar");

	gfield = gda_recordset_field_idx (recset, 2);
	str = gda_stringify_value (NULL, 0, gfield);
	field->length = atoi (str);
	g_free (str);

	gfield = gda_recordset_field_idx (recset, 4);
	if (gda_field_get_boolean_value (gfield))
		field->null_allowed = TRUE;
	else
		field->null_allowed = FALSE;

	/* FIXME: field->is_key needs to be set */
	gfield = gda_recordset_field_idx (recset, 5);
	if (gda_field_get_boolean_value (gfield))
		field->is_key = TRUE;
	else
		field->is_key = FALSE;

	/* Default value */
	gfield = gda_recordset_field_idx (recset, 6);
	if (field->default_val) {
		g_free (field->default_val);
		field->default_val = NULL;
	}
	if (gfield && gfield->real_value->_u.v._u.lvc) {
		str = gda_stringify_value (NULL, 0, gfield);
		field->default_val = str;
	}
}

gchar * 
db_field_get_xml_id (DbField * field, Database *db)
{
	gchar *str = NULL;
	DbTable *table;

	g_return_val_if_fail (field, NULL);
	g_return_val_if_fail (IS_DB_FIELD (field), NULL);

	table = database_find_table_from_field (db, field);
	if (table)
		str = g_strdup_printf ("TV%s:FI%s", table->name, field->name);
	else
		g_warning ("db_field_get_xml_id : table is NULL (field=%s)!\n", field->name);

	return str;
}


/****************************************/
/*                                      */
/* Dumping the field                    */
/*                                      */
/****************************************/
#ifdef debug
void
db_field_dump (DbField * field)
{
	/* prints all on one line */
/*  gchar*       server_access_get_data_type (ServerAccess *srv, gchar *oid);*/

	g_print ("%-20s %-10s(%d)\t%3d   ", field->name,
		 field->type->sqlname, field->type->server_type,
		 field->length);
	if (field->null_allowed)
		g_print ("NULL allowed ");
	else
		g_print ("NOT NULL     ");
	if (field->is_key)
		g_print ("Key");
	if (field->default_val)
		g_print ("Defaults to %s", field->default_val);
	g_print ("\n");
}
#endif



/*
 *
 * Object representing a sequence
 *
 */

static void db_sequence_class_init (DbSequenceClass * class);
static void db_sequence_init (DbSequence * seq);
static void db_sequence_destroy (GtkObject * object);


/* No signals */
GtkType
db_sequence_get_type (void)
{
	static GtkType f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"DbSeq",
			sizeof (DbSequence),
			sizeof (DbSequenceClass),
			(GtkClassInitFunc) db_sequence_class_init,
			(GtkObjectInitFunc) db_sequence_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (db_item_get_type (), &f_info);
	}

	return f_type;
}

static void
db_sequence_class_init (DbSequenceClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;

	object_class->destroy = db_sequence_destroy;
}

static void
db_sequence_init (DbSequence * seq)
{
	seq->name = NULL;
	seq->field_links = NULL;
	seq->comments = NULL;
	seq->owner = NULL;
}


GtkObject *
db_sequence_new (void)
{
	GtkObject *obj;

	obj = gtk_type_new (db_sequence_get_type ());
	return obj;
}

static void
db_sequence_destroy (GtkObject * object)
{
	DbSequence *seq;
	DbItem *parent_class = NULL;

	parent_class = gtk_type_class (db_item_get_type ());
	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_DB_SEQUENCE (object));

	seq = DB_SEQUENCE (object);

	/* field links destroying */
	if (seq->field_links) {
		g_slist_free (seq->field_links);
		seq->field_links = NULL;
	}

	/* name destroying */
	if (seq->name)
		g_free (seq->name);
	seq->name = NULL;

	/* comments destroying */
	if (seq->comments)
		g_free (seq->comments);
	seq->comments = NULL;

	/* for the parent class */
	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}

gchar * 
db_sequence_get_xml_id (DbSequence * seq)
{
	gchar *str;

	g_return_val_if_fail (seq, NULL);
	g_return_val_if_fail (IS_DB_SEQUENCE (seq), NULL);

	str = g_strdup_printf ("SE%s", seq->name);
	return str;	
}


/* creates the DbSequence.
   the DbSequence object MUST not exist before. */
static void
db_sequence_load_from_xml_node (Database * db, xmlNodePtr seqtree)
{
	gchar *str;
	DbSequence *seq;
	xmlNodePtr subtree;

	g_return_if_fail ((!db->is_fault));

	str = xmlGetProp (seqtree, "name");
	seq = DB_SEQUENCE (db_sequence_new ());
	seq->name = g_strdup (str + 2);
	g_free (str);

	gtk_signal_connect (GTK_OBJECT (seq), "load_fault",
			    GTK_SIGNAL_FUNC (database_cb), db);
	db->sequences = g_slist_append (db->sequences, seq);

	str = xmlGetProp (seqtree, "comments");
	if (str)
		seq->comments = str;

	str = xmlGetProp (seqtree, "owner");
	seq->owner = (gchar *) ((find_user_name (db, str))->data);
	g_free (str);

	/* see if there is any link from that sequence to a table's field */
	subtree = seqtree->xmlChildrenNode;
	while (subtree) {
		if (!strcmp (subtree->name, "seq_link")) {
			DbField *field;
			str = xmlGetProp (subtree, "field");
			if (str) {
				field = database_find_field_from_xml_name (db, str);
				g_free (str);
				if (field)
					seq->field_links = g_slist_append (seq->field_links, field);
			}
		}
		subtree = subtree->next;
	}
}


