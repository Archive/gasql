/* query-create-druid.h
 * Copyright (C) 2001 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __query_create_druid_h__
#define __query_create_druid_h__ 1

#include <libgnome/gnome-defs.h>
#include <libgnomeui/gnome-druid.h>
#include "query.h"
#include "conf-manager.h"

#define QUERY_CREATE_DRUID(obj)          GTK_CHECK_CAST (obj, query_create_druid_get_type(), QueryCreateDruid)
#define QUERY_CREATE_DRUID_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, query_create_druid_get_type (), QueryCreateDruidClass)
#define IS_QUERY_CREATE_DRUID(obj)       GTK_CHECK_TYPE (obj, query_create_druid_get_type ())


typedef struct _QueryCreateDruid        QueryCreateDruid;
typedef struct _QueryCreateDruidClass   QueryCreateDruidClass;

struct _QueryCreateDruid {
	GnomeDruid              druid;
	ConfManager      *conf;
	Query                  *q;
	GtkTooltips            *tips;

	GnomeDruidPageStart    *start_page;
	/* informations page */
	GnomeDruidPageStandard *info_page;
	GtkWidget              *name_entry;
	GtkWidget              *descr_entry;
	GtkWidget              *query_type;
	GtkWidget              *wizard_type;

	/* fields selection page */
	GnomeDruidPageStandard *fields_page;
	GtkWidget              *fields_to_sel;
	GtkWidget              *fields_selected;
	GtkWidget              *add_field_button;
	GtkWidget              *del_field_button;

	/* last page */
	GnomeDruidPageFinish   *finish_page;
};

struct _QueryCreateDruidClass {
	GnomeDruidClass parent_class;

	void (*finish)(QueryCreateDruid *druid);
};

GtkType    query_create_druid_get_type (void);

GtkWidget *query_create_druid_new      (ConfManager * conf);
Query     *query_create_druid_get_query(QueryCreateDruid *druid);

#endif
