/* query-env-editor.c
 *
 * Copyright (C) 2002 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <config.h>
#include "query-env-editor.h"
#include "choicecombo.h"
#include "gladefeditor.h"


static void query_env_editor_class_init (QueryEnvEditorClass * class);
static void query_env_editor_init (QueryEnvEditor * qe);
static void query_env_editor_destroy (GtkObject * object);


/* get a pointer to the parents to be able to call their destructor */
static GtkObject *parent_class = NULL;


guint
query_env_editor_get_type (void)
{
	static guint f_type = 0;

	if (!f_type) {
		GtkTypeInfo f_info = {
			"QueryEnvEditor",
			sizeof (QueryEnvEditor),
			sizeof (QueryEnvEditorClass),
			(GtkClassInitFunc) query_env_editor_class_init,
			(GtkObjectInitFunc) query_env_editor_init,
			(GtkArgSetFunc) NULL,
			(GtkArgGetFunc) NULL
		};

		f_type = gtk_type_unique (gtk_vbox_get_type (), &f_info);
	}

	return f_type;
}

static void
query_env_editor_class_init (QueryEnvEditorClass * class)
{
	GtkObjectClass *object_class;

	object_class = (GtkObjectClass *) class;

	object_class->destroy = query_env_editor_destroy;
	parent_class = gtk_type_class (gtk_vbox_get_type ());
}


static void
query_env_editor_init (QueryEnvEditor * qed)
{
	qed->env = NULL;

	qed->glade_import_dlg = NULL;
	qed->no_insert = TRUE;
	qed->layout_label = NULL;
	
	qed->name_entry = NULL;
	qed->descr_entry = NULL;

	qed->types_option_menu = NULL;
	qed->form_menu_item = NULL;
	qed->grid_menu_item = NULL;

	qed->tables_option_menu = NULL;
	qed->tables_menu = NULL;
}


static void env_name_changed_cb (QueryEnv *env, QueryEnvEditor *qed);
static void env_type_changed_cb (QueryEnv *env, QueryEnvEditor *qed);
static void env_actions_changed_cb (QueryEnv *env, QueryEnvEditor *qed);
static void env_modif_table_changed_cb (QueryEnv *env, QueryEnvEditor *qed);
static void query_view_added_cb (Query *q, QueryView *view, QueryEnvEditor *qed);
static void query_view_removed_cb (Query *q, QueryView *view, QueryEnvEditor *qed);
static void edit_post_init (QueryEnvEditor *qed);
GtkWidget *
query_env_editor_new (QueryEnv * env)
{
	GtkObject *obj;
	QueryEnvEditor *qed;

	g_return_val_if_fail (env != NULL, NULL);
	g_return_val_if_fail (IS_QUERY_ENV (env), NULL);

	obj = gtk_type_new (query_env_editor_get_type ());
	qed = QUERY_ENV_EDITOR (obj);
	qed->env = env;

	edit_post_init (qed);

	/* connection to signals from Database */
	gtk_signal_connect_while_alive (GTK_OBJECT (env), "name_changed",
					GTK_SIGNAL_FUNC
					(env_name_changed_cb), qed,
					obj);
	gtk_signal_connect_while_alive (GTK_OBJECT (env), "type_changed",
					GTK_SIGNAL_FUNC
					(env_type_changed_cb), qed,
					obj);
	gtk_signal_connect_while_alive (GTK_OBJECT (env), "modif_table_changed",
					GTK_SIGNAL_FUNC
					(env_modif_table_changed_cb), qed,
					obj);
	gtk_signal_connect_while_alive (GTK_OBJECT (env), "actions_changed",
					GTK_SIGNAL_FUNC
					(env_actions_changed_cb), qed,
					obj);
	gtk_signal_connect_while_alive (GTK_OBJECT (env->q), "query_view_added",
					GTK_SIGNAL_FUNC
					(query_view_added_cb), qed,
					obj);
	gtk_signal_connect_while_alive (GTK_OBJECT (env->q), "query_view_removed",
					GTK_SIGNAL_FUNC
					(query_view_removed_cb), qed,
					obj);

	return GTK_WIDGET (obj);
}


/* declaration here because used just below to block signal propagation */
static void edit_name_changed_cb (GtkEditable *editable, QueryEnvEditor *qed);
static void edit_descr_changed_cb (GtkEditable *editable, QueryEnvEditor *qed);
static void edit_type_mitem_activate_cb (GtkMenuItem * menu_item, QueryEnvEditor * qed);
static void edit_table_mitem_activate_cb (GtkMenuItem * menu_item, QueryEnvEditor * qed);
static GtkWidget *get_tables_optionmenu_menu (QueryEnvEditor * qed);
static void edit_dlg_cb_toggled_cb (GtkToggleButton * toggle_button, QueryEnvEditor * qed);

/* 
 * update the widget to respond to the changes
 * from the QueryEnv => first suspend the signals propagation,
 * to avoid an infinite loop, and then put it back 
 */
static void 
env_name_changed_cb (QueryEnv *env, QueryEnvEditor *qed)
{
	/* update the name entry */
	gtk_signal_handler_block_by_func (GTK_OBJECT (qed->name_entry),
					  GTK_SIGNAL_FUNC (edit_name_changed_cb), qed);
	if (env->name)
		gtk_entry_set_text (GTK_ENTRY (qed->name_entry), env->name);
	else
		gtk_entry_set_text (GTK_ENTRY (qed->name_entry), "");
	gtk_signal_handler_unblock_by_func (GTK_OBJECT (qed->name_entry),
					    GTK_SIGNAL_FUNC (edit_name_changed_cb), qed);

	/* update the descr entry */
	gtk_signal_handler_block_by_func (GTK_OBJECT (qed->descr_entry),
					  GTK_SIGNAL_FUNC (edit_descr_changed_cb), qed);
	if (env->descr)
		gtk_entry_set_text (GTK_ENTRY (qed->descr_entry), env->descr);
	else
		gtk_entry_set_text (GTK_ENTRY (qed->descr_entry), "");
	gtk_signal_handler_unblock_by_func (GTK_OBJECT (qed->descr_entry),
					    GTK_SIGNAL_FUNC (edit_descr_changed_cb), qed);

}

static void 
env_type_changed_cb (QueryEnv *env, QueryEnvEditor *qed)
{
	gtk_signal_handler_block_by_func (GTK_OBJECT (qed->form_menu_item),
					  GTK_SIGNAL_FUNC (edit_type_mitem_activate_cb), qed);
	gtk_signal_handler_block_by_func (GTK_OBJECT (qed->grid_menu_item),
					  GTK_SIGNAL_FUNC (edit_type_mitem_activate_cb), qed);

	if (env->form_is_default)
		gtk_option_menu_set_history (GTK_OPTION_MENU (qed->types_option_menu), 0);
	else
		gtk_option_menu_set_history (GTK_OPTION_MENU (qed->types_option_menu), 1);


	gtk_signal_handler_unblock_by_func (GTK_OBJECT (qed->form_menu_item),
					    GTK_SIGNAL_FUNC (edit_type_mitem_activate_cb), qed);
	gtk_signal_handler_unblock_by_func (GTK_OBJECT (qed->grid_menu_item),
					    GTK_SIGNAL_FUNC (edit_type_mitem_activate_cb), qed);
}

static void 
env_modif_table_changed_cb (QueryEnv *env, QueryEnvEditor *qed)
{
	GList *list;
	gboolean found = FALSE;
	gint i = 0;

	list = GTK_MENU_SHELL (qed->tables_menu)->children;
	while (list && !found) {
		DbTable *table;
		table = gtk_object_get_data (GTK_OBJECT (list->data), "table");
		if (table == env->modif_table) {
			gtk_signal_handler_block_by_func (GTK_OBJECT (list->data),
							  GTK_SIGNAL_FUNC (edit_table_mitem_activate_cb), 
							  qed);
			gtk_option_menu_set_history (GTK_OPTION_MENU (qed->tables_option_menu), i);
			gtk_signal_handler_unblock_by_func (GTK_OBJECT (list->data),
							    GTK_SIGNAL_FUNC (edit_table_mitem_activate_cb), 
							    qed);
			found = TRUE;
		}
		list = g_list_next (list);
		i++;
	}
}

/* list of properties for the action button names */
static gchar *actions_props[] = {"fr", "lr", "pr", "nr", "ne", "co", "de", "ch", "re", "ed", "va",
                                 "ERROR"};

static void 
env_actions_changed_cb (QueryEnv *env, QueryEnvEditor *qed)
{
	QueryActions action;
	GtkWidget *cb;
	guint i = 0;

	for (action = QUERY_ACTION_FIRST; action < QUERY_ACTION_LAST_ENUM; action = action *2) {
		cb = gtk_object_get_data (GTK_OBJECT (qed), actions_props[i]);
		if ((gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (cb)) && 
		      !(env->actions & action)) ||
		    (!gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (cb)) && 
		     (env->actions & action))) {
			gtk_signal_handler_block_by_func (GTK_OBJECT (cb),
							  GTK_SIGNAL_FUNC (edit_dlg_cb_toggled_cb), qed);
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cb), env->actions & action );
			gtk_signal_handler_unblock_by_func (GTK_OBJECT (cb),
							    GTK_SIGNAL_FUNC (edit_dlg_cb_toggled_cb), qed);
		}
		i++;
	}
}

static void
query_view_added_cb (Query *q, QueryView *view, QueryEnvEditor *qed)
{
	if (IS_DB_TABLE (view->obj)) {
		DbTable *add_table = NULL;
		GList *list;
	
		list = GTK_MENU_SHELL (qed->tables_menu)->children;
		while (list && !add_table) {
			DbTable *table;
			table = gtk_object_get_data (GTK_OBJECT (list->data), "table");
			if (table == DB_TABLE (view->obj))
				add_table = table;
			list = g_list_next (list);
		}
		
		/* if table not present, then add it */
		if (!add_table) {
			GtkWidget *mitem;
			gint i;
			gboolean halt;
			DbTable *table;

			add_table = DB_TABLE (view->obj);

			mitem = gtk_menu_item_new_with_label (add_table->name);
			gtk_signal_connect (GTK_OBJECT (mitem), "activate",
					    GTK_SIGNAL_FUNC (edit_table_mitem_activate_cb), qed);
			gtk_object_set_data (GTK_OBJECT (mitem), "table", add_table);
			
			list = GTK_MENU_SHELL (qed->tables_menu)->children;
			halt = FALSE;
			i = 0;
			while (list && !halt) {
				table = gtk_object_get_data (GTK_OBJECT (list->data), "table");
				if (strcmp (add_table->name, table->name) < 0)
					halt = TRUE;
				else {
					list = g_list_next (list);
					i++;
				}
			}

			gtk_menu_insert (GTK_MENU (qed->tables_menu), mitem, i);
			gtk_widget_show (mitem);
		}
	}
}

static void query_view_removed_cb (Query *q, QueryView *view, QueryEnvEditor *qed)
{
	if (IS_DB_TABLE (view->obj)) {
		gboolean another = FALSE;
		GSList *views;
		
		/* is there another QueryView for the same table (an alias) ? */
		views = q->views;
		while (views && !another) {
			if (IS_DB_TABLE (QUERY_VIEW (views->data)->obj) &&
			    (DB_TABLE (QUERY_VIEW (views->data)->obj) == DB_TABLE (view->obj)))
				another = TRUE;
			views = g_slist_next (views);
		}

		if (!another) {
			/* re-create the list of potentially modified tables */
			GtkWidget *optionmenu_menu;

			gtk_option_menu_remove_menu (GTK_OPTION_MENU (qed->tables_option_menu));
			
			optionmenu_menu = get_tables_optionmenu_menu (qed);
			qed->tables_menu = optionmenu_menu;

			gtk_option_menu_set_menu (GTK_OPTION_MENU (qed->tables_option_menu),
						  optionmenu_menu);
			gtk_widget_show (optionmenu_menu);

			if (qed->env->modif_table == DB_TABLE (view->obj))
				query_env_set_modif_table (qed->env, NULL);
		}
	}
}


static void
query_env_editor_destroy (GtkObject * object)
{
	QueryEnvEditor *qed;

	g_return_if_fail (object != NULL);
	g_return_if_fail (IS_QUERY_ENV_EDITOR (object));

	qed = QUERY_ENV_EDITOR (object);


	if (qed->glade_import_dlg)
		gnome_dialog_close (GNOME_DIALOG (qed->glade_import_dlg));


	if (GTK_OBJECT_CLASS (parent_class)->destroy)
		(*GTK_OBJECT_CLASS (parent_class)->destroy) (object);
}



static GtkWidget *get_action_cb (QueryEnvEditor * qed, QueryActions action);
static void edit_layout_cb (GtkButton *button, QueryEnvEditor *qed);

static void 
edit_post_init (QueryEnvEditor *qed)
{
	GtkWidget *frame, *vbox, *label, *hbb, *button, *table, *cb, *entry;
	GtkWidget *optionmenu, *optionmenu_menu, *mitem;


	/* signals from other objects */
	if (!qed->env->q)
		g_error ("query_env_editor_show_props_dialog() env->q is NULL");

	/* Basic properties frame */
	frame = gtk_frame_new (_("Basic properties"));
	gtk_box_pack_start (GTK_BOX (qed), frame, TRUE, TRUE, 0);
	gtk_widget_show (frame);

	table = gtk_table_new (4, 2, FALSE);
	gtk_container_set_border_width (GTK_CONTAINER (table), GNOME_PAD / 2.);
	gtk_table_set_row_spacings (GTK_TABLE (table), GNOME_PAD / 4.);
	gtk_table_set_col_spacings (GTK_TABLE (table), GNOME_PAD / 4.);
	gtk_container_add (GTK_CONTAINER (frame), table);
	gtk_widget_show (table);

	label = gtk_label_new (_("Name:"));
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, 0, 0, 0, 0);
	gtk_widget_show (label);

	entry = gtk_entry_new ();
	if (qed->env->name)
		gtk_entry_set_text (GTK_ENTRY (entry), qed->env->name);
	gtk_table_attach_defaults (GTK_TABLE (table), entry, 1, 2, 0, 1);
	gtk_widget_show (entry);
	gtk_signal_connect (GTK_OBJECT (entry), "changed",
			    GTK_SIGNAL_FUNC (edit_name_changed_cb), qed);
	qed->name_entry = entry;

	label = gtk_label_new (_("Description:"));
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, 0, 0, 0, 0);
	gtk_widget_show (label);

	entry = gtk_entry_new ();
	if (qed->env->descr)
		gtk_entry_set_text (GTK_ENTRY (entry), qed->env->descr);
	gtk_table_attach_defaults (GTK_TABLE (table), entry, 1, 2, 1, 2);
	gtk_widget_show (entry);
	gtk_signal_connect (GTK_OBJECT (entry), "changed",
			    GTK_SIGNAL_FUNC (edit_descr_changed_cb), qed);
	qed->descr_entry = entry;

	label = gtk_label_new (_("Modified table:"));
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, 0, 0, 0, 0);
	gtk_widget_show (label);

	optionmenu = gtk_option_menu_new ();
	qed->tables_option_menu = optionmenu;

	optionmenu_menu = get_tables_optionmenu_menu (qed);
	qed->tables_menu = optionmenu_menu;

	gtk_option_menu_set_menu (GTK_OPTION_MENU (optionmenu), optionmenu_menu);
	gtk_widget_show (optionmenu_menu);
	gtk_table_attach_defaults (GTK_TABLE (table), optionmenu, 1, 2, 2, 3);
	gtk_widget_show (optionmenu);


	label = gtk_label_new (_("View type:"));
	gtk_table_attach (GTK_TABLE (table), label,
			  0, 1, 3, 4, 0, 0, 0, 0);
	gtk_widget_show (label);

	optionmenu = gtk_option_menu_new ();
	qed->types_option_menu = optionmenu;

	optionmenu_menu = gtk_menu_new ();
	mitem = gtk_menu_item_new_with_label (_("Form"));
	gtk_menu_append (GTK_MENU (optionmenu_menu), mitem);
	gtk_widget_show (mitem);
	gtk_object_set_data (GTK_OBJECT (mitem), "data", GINT_TO_POINTER (1));
	gtk_signal_connect (GTK_OBJECT (mitem), "activate",
			    GTK_SIGNAL_FUNC (edit_type_mitem_activate_cb), qed);
	qed->form_menu_item = mitem;

	mitem = gtk_menu_item_new_with_label (_("Grid"));
	gtk_menu_append (GTK_MENU (optionmenu_menu), mitem);
	gtk_widget_show (mitem);
	gtk_signal_connect (GTK_OBJECT (mitem), "activate",
			    GTK_SIGNAL_FUNC (edit_type_mitem_activate_cb), qed);
	qed->grid_menu_item = mitem;

	gtk_option_menu_set_menu (GTK_OPTION_MENU (optionmenu), optionmenu_menu);
	gtk_widget_show (optionmenu_menu);
	gtk_table_attach_defaults (GTK_TABLE (table), optionmenu, 1, 2, 3, 4);
	gtk_widget_show (optionmenu);
	env_type_changed_cb (qed->env, qed);

	/* Actions frame */
	frame = gtk_frame_new (_("Action buttons"));
	gtk_box_pack_start (GTK_BOX (qed), frame, TRUE, TRUE, 0);
	gtk_widget_show (frame);

	vbox = gtk_vbox_new (FALSE, GNOME_PAD / 2.);
	gtk_container_add (GTK_CONTAINER (frame), vbox);
	gtk_widget_show (vbox);

	label = gtk_label_new (_("Action buttons for the grid and form vues:"));
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	table = gtk_table_new (3, 4, FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);
	gtk_widget_show (table);

	cb = get_action_cb (qed, QUERY_ACTION_FIRST);
	gtk_table_attach_defaults (GTK_TABLE (table), cb, 0, 1, 0, 1);
	gtk_widget_show (cb);

	cb = get_action_cb (qed, QUERY_ACTION_LAST);
	gtk_table_attach_defaults (GTK_TABLE (table), cb, 0, 1, 1, 2);
	gtk_widget_show (cb);

	cb = get_action_cb (qed, QUERY_ACTION_COMMIT);
	gtk_table_attach_defaults (GTK_TABLE (table), cb, 0, 1, 2, 3);
	gtk_widget_show (cb);

	cb = get_action_cb (qed, QUERY_ACTION_PREV);
	gtk_table_attach_defaults (GTK_TABLE (table), cb, 1, 2, 0, 1);
	gtk_widget_show (cb);

	cb = get_action_cb (qed, QUERY_ACTION_NEXT);
	gtk_table_attach_defaults (GTK_TABLE (table), cb, 1, 2, 1, 2);
	gtk_widget_show (cb);

	cb = get_action_cb (qed, QUERY_ACTION_CHOOSE);
	gtk_table_attach_defaults (GTK_TABLE (table), cb, 1, 2, 2, 3);
	gtk_widget_show (cb);

	cb = get_action_cb (qed, QUERY_ACTION_INSERT);
	gtk_table_attach_defaults (GTK_TABLE (table), cb, 2, 3, 0, 1);
	gtk_widget_show (cb);

	cb = get_action_cb (qed, QUERY_ACTION_EDIT);
	gtk_table_attach_defaults (GTK_TABLE (table), cb, 2, 3, 1, 2);
	gtk_widget_show (cb);

	cb = get_action_cb (qed, QUERY_ACTION_DELETE);
	gtk_table_attach_defaults (GTK_TABLE (table), cb, 2, 3, 2, 3);
	gtk_widget_show (cb);

	cb = get_action_cb (qed, QUERY_ACTION_REFRESH);
	gtk_table_attach_defaults (GTK_TABLE (table), cb, 0, 1, 3, 4);
	gtk_widget_show (cb);

	cb = get_action_cb (qed, QUERY_ACTION_VIEWALL);
	gtk_table_attach_defaults (GTK_TABLE (table), cb, 1, 2, 3, 4);
	gtk_widget_show (cb);


	/* Form layout frame */
	frame = gtk_frame_new (_("Form Layout"));
	gtk_box_pack_start (GTK_BOX (qed), frame, TRUE, TRUE, 0);
	gtk_widget_show (frame);

	vbox = gtk_vbox_new (FALSE, GNOME_PAD / 2.);
	gtk_container_add (GTK_CONTAINER (frame), vbox);
	gtk_widget_show (vbox);

	if (qed->env->form)
		label = gtk_label_new (_("A customized form layout is now used"));
	else
		label = gtk_label_new (_("The default (provided by " PACKAGE ")\nform layout is now used"));
	qed->layout_label = label;
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE,
			    GNOME_PAD / 2.);
	gtk_widget_show (label);

	hbb = gtk_hbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (vbox), hbb, FALSE, TRUE,
			    GNOME_PAD / 2.);
	gtk_widget_show (hbb);

	button = gtk_button_new_with_label (_("Edit Layout"));
	gtk_signal_connect (GTK_OBJECT (button), "clicked",
			    GTK_SIGNAL_FUNC (edit_layout_cb), qed);
	gtk_container_add (GTK_CONTAINER (hbb), button);
	gtk_widget_show (button);
}

static void
edit_name_changed_cb (GtkEditable * editable, QueryEnvEditor * qed)
{
	query_env_set_name (qed->env, gtk_entry_get_text (GTK_ENTRY (editable)));
}

static void
edit_descr_changed_cb (GtkEditable * editable, QueryEnvEditor * qed)
{
	query_env_set_descr (qed->env, gtk_entry_get_text (GTK_ENTRY (editable)));
}

static void 
edit_type_mitem_activate_cb (GtkMenuItem * menu_item, QueryEnvEditor * qed)
{
	gpointer data;

	data = gtk_object_get_data (GTK_OBJECT (menu_item), "data");
	query_env_set_type (qed->env, data ? TRUE : FALSE);
}



static GtkWidget *
get_action_cb (QueryEnvEditor * qed, QueryActions action)
{
	GtkWidget *cb;
	gchar *str;
	guint i = 0;

	switch (action) {
	case QUERY_ACTION_FIRST:
		str = _("First record");
		break;
	case QUERY_ACTION_LAST:
		str = _("Last record");
		i = 1;
		break;
	case QUERY_ACTION_PREV:
		str = _("Previous record");
		i = 2;
		break;
	case QUERY_ACTION_NEXT:
		str = _("Next record");
		i = 3;
		break;
	case QUERY_ACTION_INSERT:
		str = _("New entry");
		i = 4;
		break;
	case QUERY_ACTION_COMMIT:
		str = _("Commit");
		i = 5;
		break;
	case QUERY_ACTION_DELETE:
		str = _("Delete");
		i = 6;
		break;
	case QUERY_ACTION_CHOOSE:
		str = _("Choose one");
		i = 7;
		break;
	case QUERY_ACTION_REFRESH:
		str = _("Refresh");
		i = 8;
		break;
	case QUERY_ACTION_EDIT:
		str = _("Edit");
		i = 9;
		break;
	case QUERY_ACTION_VIEWALL:
		str = _("View all");
		i = 10;
		break;
	default: /* should not occur */
		str = "_ERROR_";
		i = 11;
		break;
	}

	cb = gtk_check_button_new_with_label (str);
	gtk_object_set_data (GTK_OBJECT (cb), "act", GUINT_TO_POINTER (action));
	gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (cb), (qed->env->actions & action));

	gtk_object_set_data (GTK_OBJECT (qed), actions_props[i], cb);
	gtk_signal_connect (GTK_OBJECT (cb), "toggled",
			    GTK_SIGNAL_FUNC (edit_dlg_cb_toggled_cb), qed);

	return cb;
}

static void
edit_dlg_cb_toggled_cb (GtkToggleButton * toggle_button, QueryEnvEditor * qed)
{
	QueryActions action;

	action = GPOINTER_TO_UINT (gtk_object_get_data (GTK_OBJECT (toggle_button), "act"));
	if (qed->env->actions & action)
		query_env_del_action (qed->env, action);
	else
		query_env_add_action (qed->env, action);
}



/*
 * To change the default Form Layout
 */

static void glade_import_dlg_closed (GnomeDialog * dialog, QueryEnvEditor * qed);
static void 
edit_layout_cb (GtkButton *button, QueryEnvEditor *qed)
{
	if (qed->glade_import_dlg) {
		gdk_window_raise (qed->glade_import_dlg->window);
	}
	else {
		GtkWidget *dlg, *gfe;
		
		dlg = gnome_dialog_new (_("Edition of form layout"), GNOME_STOCK_BUTTON_CLOSE, NULL);
		gfe = glade_form_editor_new ();
		gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), gfe, TRUE, TRUE, 0);

		gtk_signal_connect (GTK_OBJECT (dlg), "close",
				    GTK_SIGNAL_FUNC (glade_import_dlg_closed), qed);
		gnome_dialog_set_close (GNOME_DIALOG (dlg), TRUE);

		qed->glade_import_dlg = dlg;

		gtk_widget_set_usize (dlg, 800, 550);
		gtk_window_set_policy (GTK_WINDOW (dlg), TRUE, TRUE, FALSE);
		gtk_widget_show_all (dlg);
	}
}

static void 
glade_import_dlg_closed (GnomeDialog * dialog, QueryEnvEditor * qed)
{
	qed->glade_import_dlg = NULL;
}




/*
 * determine a number of stars for each table which can potentially
 * be modified
 */
enum _ModifTableLevel
{
	LINKS_TO_TABLE = 1 << 3,
	KEY_PRESENT = 1 << 2,
	ALL_FIELDS = 1 << 1,
	ALL_FIELDS_WITH_DEFAULT = 1 << 0
};

struct _ModifTable
{
	DbTable *table;
	enum _ModifTableLevel level;
	gint nbstars;
};


/* returns a list of struct _ModifTable to be freed by the caller */
static GSList *list_potential_modif_tables (QueryEnv * env);
static void update_no_insert_info (QueryEnvEditor * qed);

/* returns the list of tables which can be modified for a given query 
   and sets the modif_table to a default value if not set, or set it to NULL
   if no table can be modified 
*/
static GtkWidget *
get_tables_optionmenu_menu (QueryEnvEditor * qed)
{
	GtkWidget *optionmenu_menu, *mitem;
	GSList *tables, *hold;
	gint i;

	tables = list_potential_modif_tables (qed->env);
	optionmenu_menu = gtk_menu_new ();

	i = 0;
	while (tables) {
		gchar *str, *stars="";
		struct _ModifTable *mt;

		mt = (struct _ModifTable *) (tables->data);
		switch (mt->nbstars) {
		case 0:
			stars = "";
			break;
		case 1:
			stars = "*";
			break;
		case 2:
			stars = "**";
			break;
		case 3:
			stars = "***";
			break;
		case 4:
			stars = "****";
			break;
		}
		if (mt->table) {
			if (mt->nbstars)
				str = g_strdup_printf ("%s (%s)", mt->table->name, stars);
			else
				str = g_strdup (mt->table->name);
		}
		else
			str = g_strdup (_("NONE"));
		mitem = gtk_menu_item_new_with_label (str);
		g_free (str);

		gtk_object_set_data (GTK_OBJECT (mitem), "table", mt->table);
		gtk_menu_append (GTK_MENU (optionmenu_menu), mitem);
		if (mt->table == qed->env->modif_table)
			gtk_menu_set_active (GTK_MENU (optionmenu_menu), i);
		gtk_signal_connect (GTK_OBJECT (mitem), "activate",
				    GTK_SIGNAL_FUNC (edit_table_mitem_activate_cb), qed);
		gtk_widget_show (mitem);
		i++;

		g_free (tables->data);
		hold = tables;
		tables = g_slist_remove_link (tables, tables);
		g_slist_free_1 (hold);
	}

	update_no_insert_info (qed);
	return optionmenu_menu;
}

static GSList *
list_potential_modif_tables (QueryEnv * env)
{
	GSList *list = NULL, *views;
	struct _ModifTable *mt;
	DbTable *table;

	/* 1st step : list without order */
	views = env->q->views;
	while (views) {
		if (IS_DB_TABLE (QUERY_VIEW (views->data)->obj)) {
			table = DB_TABLE (QUERY_VIEW (views->data)->obj);
			if (!table->is_view) {
				gboolean listed = FALSE;
				
				mt = g_new0 (struct _ModifTable, 1);
				mt->table = table;
				list = g_slist_append (list, mt);
				listed = TRUE;
			}
		}
		views = g_slist_next (views);
	}

	/* 2nd step: associate priorities level to selected tables */
	views = list;
	while (views) {
		gboolean ltest, ltest2;
		GSList *tlist;
		mt = (struct _ModifTable *) (views->data);

		/* KEY_PRESENT: is there a key field printed in the query */
		ltest = FALSE;
		tlist = mt->table->fields;
		while (!ltest && tlist) {
			if ((DB_FIELD (tlist->data)->is_key) &&
			    query_is_table_field_printed (env->q, DB_FIELD (tlist->data)))
				ltest = TRUE;
			tlist = g_slist_next (tlist);
		}
		mt->level = ltest ? KEY_PRESENT : 0;

		/* LINKS_TO_TABLE: are all the links related to the table pointing 
		   TO a field in the table */
		ltest = TRUE;
		tlist = env->q->joins;
		while (ltest && tlist) {
			/* FIXME VIV QueryJoin *qj = QUERY_JOIN (tlist->data); */
			QueryJoin *qj;
			if (FALSE && (qj->join_type != QUERY_JOIN_CROSS) &&
			    (IS_DB_TABLE (qj->ant_view) && (DB_TABLE (qj->ant_view) == mt->table)))
				ltest = FALSE;
			tlist = g_slist_next (tlist);
		}
		mt->level = ltest ? mt->level | LINKS_TO_TABLE : mt->level;

		/* ALL_FIELDS_WITH_DEFAULT and ALL_FIELDS: are there respectively ALL the fields
		   with a NOT NULL value, and ALL the fields with a NOT NULL value minus the ones
		   with a default value, present in the printed part of a query? */
		ltest = TRUE;
		ltest2 = TRUE;	/* WITH_DEFAULT */
		tlist = mt->table->fields;
		while ((ltest || ltest2) && tlist) {
			if (!(DB_FIELD (tlist->data)->null_allowed) &&
			    (query_is_table_field_printed
			     (env->q, DB_FIELD (tlist->data)) == -1)) {
				/* do we have a link we use TO that field */
				gboolean found = FALSE;
				GSList *llist = env->q->joins;
				while (llist && !found) {
					/* FIXME VIV QueryJoin *qj = QUERY_JOIN (llist->data); */
/* 					if ((qj->join_type != QUERY_JOIN_CROSS) &&  */
/* 					    (qj->link->to == DB_FIELD (tlist->data))) */
/* 						found = TRUE; */
					llist = g_slist_next (llist);
				}
				if (!found) {
					ltest = FALSE;
					if (!DB_FIELD (tlist->data)->
					    default_val)
						ltest2 = FALSE;
				}
			}
			tlist = g_slist_next (tlist);
		}

		mt->level = ltest ? mt->level | ALL_FIELDS : mt->level;
		mt->level =
			ltest2 ? mt->level | ALL_FIELDS_WITH_DEFAULT : mt->
			level;

		views = g_slist_next (views);
	}

	/* 3rd step: prepend a "NONE" entry, and order the tables */
	views = list;
	while (views) {
		mt = (struct _ModifTable *) (views->data);

		if (mt->level & LINKS_TO_TABLE)
			mt->nbstars = 4;
		if (!mt->nbstars && (mt->level > (int) KEY_PRESENT))
			mt->nbstars = 3;
		if (!mt->nbstars && (mt->level == (int) KEY_PRESENT))
			mt->nbstars = 2;
		if (!mt->nbstars
		    && (mt->level >= (int) ALL_FIELDS_WITH_DEFAULT))
			mt->nbstars = 1;

		views = g_slist_next (views);
	}
	/* FIXME: order */

	/* "NONE" entry */
	mt = g_new0 (struct _ModifTable, 1);
	mt->table = NULL;
	list = g_slist_prepend (list, mt);

	return list;
}


static void 
edit_table_mitem_activate_cb (GtkMenuItem * menu_item, QueryEnvEditor * qed)
{
	DbTable *table;

	table = gtk_object_get_data (GTK_OBJECT (menu_item), "table");
	if (table)
		g_assert (IS_DB_TABLE (table));
	query_env_set_modif_table (qed->env, table);

	/* update the "no_insert" value */
	update_no_insert_info (qed);
}

static void 
update_no_insert_info (QueryEnvEditor * qed)
{
	GSList *list, *hold;
	gboolean found = FALSE;

	if (!qed->env->modif_table) {
		qed->no_insert = TRUE;
		return;
	}

	list = list_potential_modif_tables (qed->env);
	while (list) {
		struct _ModifTable *mt = (struct _ModifTable *) (list->data);
		if (mt->table == qed->env->modif_table) {
			qed->no_insert = (mt->level & ALL_FIELDS_WITH_DEFAULT) ? FALSE : TRUE;
			found = TRUE;
		}
		g_free (mt);
		hold = list;
		list = g_slist_remove_link (list, list);
		g_slist_free_1 (hold);
	}
	if (!found)
		qed->no_insert = TRUE;
}





/*
 * Properties edition in a dialog
 */
static void dlg_clicked_cb (GnomeDialog * dialog, gint button_number,
			    QueryEnvEditor * env);
static void dlg_env_name_changed_cb (QueryEnv *env, GnomeDialog * dialog); 
GtkWidget *
query_env_editor_get_in_dialog (QueryEnv * env)
{
	GtkWidget *dlg, *qed;

	g_return_val_if_fail (env != NULL, NULL);
	g_return_val_if_fail (IS_QUERY_ENV (env), NULL);

	dlg = gnome_dialog_new ("", GNOME_STOCK_BUTTON_CLOSE, NULL);
	dlg_env_name_changed_cb (env, GNOME_DIALOG (dlg));
	gnome_dialog_close_hides (GNOME_DIALOG (dlg), FALSE);

	/* dialog termination */
	gtk_signal_connect (GTK_OBJECT (dlg), "clicked",
			    GTK_SIGNAL_FUNC (dlg_clicked_cb),
			    env);

	qed = query_env_editor_new (env);
	gtk_box_pack_start (GTK_BOX (GNOME_DIALOG (dlg)->vbox), qed,
			    TRUE, TRUE, 0);
	gtk_widget_show (qed);

	gtk_signal_connect (GTK_OBJECT (env), "name_changed",
			    GTK_SIGNAL_FUNC (dlg_env_name_changed_cb), dlg);

	return dlg;
}


static void
dlg_clicked_cb (GnomeDialog * dialog, gint button_number, QueryEnvEditor * env)
{
	gnome_dialog_close (dialog);
}


static void dlg_env_name_changed_cb (QueryEnv *env, GnomeDialog * dialog)
{
	gchar *str;
	
	if (env->name && *(env->name))
		str = g_strdup_printf (_("Properties for Form '%s'"),
				       env->name);
	else
		str = g_strdup (_("Form Properties"));
	gtk_window_set_title (GTK_WINDOW (dialog), str);
	g_free (str);
}
