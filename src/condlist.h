/* condlist.h
 *
 * Copyright (C) 2000 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __COND_LIST__
#define __COND_LIST__

#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <gtk/gtkctree.h>
#include <gnome.h>

#ifdef __cplusplus
extern "C"
{
#endif				/* __cplusplus */


#define COND_LIST(obj)          GTK_CHECK_CAST (obj, cond_list_get_type(), CondList)
#define COND_LIST_CLASS(klass)  GTK_CHECK_CLASS_CAST (klass, cond_list_get_type (), CondListClass)
#define IS_COND_LIST(obj)       GTK_CHECK_TYPE (obj, cond_list_get_type ())

	typedef struct _CondList CondList;
	typedef struct _CondListClass CondListClass;

	/* struct for the object's data */
	struct _CondList
	{
		GtkHBox box;

		GtkWidget *ctree;
		gint nb_columns;
		gint tree_column;

		GtkWidget *up_button;
		GtkWidget *dn_button;
		GtkWidget *left_button;
		GtkWidget *right_button;
		GdkColor *where_select_bg;

		GSList *selection;

		GSList *no_select_list;	/* list of the top (1st child) non select nodes */
	};

	/* struct for the object's class */
	struct _CondListClass
	{
		GtkHBoxClass parent_class;

		void (*node_created) (CondList * cdlist, GtkCTreeNode * node);
		/* WARNING: with the following signal: it is sent while the GtkCTreeNode is still
		   in place in the GtkCTree, and it is removed only after the signal has propagated */
		void (*node_dropped) (CondList * cdlist, GtkCTreeNode * node);
		void (*tree_move) (CondList * cdlist,
				   GtkCTreeNode * node,
				   GtkCTreeNode * new_parent,
				   GtkCTreeNode * new_sibling);
		void (*selection_changed) (CondList * cdlist);
	};

	/* 
	 * generic widget's functions 
	 */
	GtkType cond_list_get_type (void);
	GtkWidget *cond_list_new (gint columns,
				  gint tree_column, gchar * titles[]);

	GtkCTreeNode *cond_list_insert_node (CondList * cdlist,
					     gpointer mem_ptr,
					     GtkCTreeNode * parent,
					     GtkCTreeNode * sibling,
					     gchar * text[],
					     gboolean is_leaf,
					     gboolean expanded);

	GtkCTreeNode *cond_list_insert_unselectable_node (CondList * cdlist,
							  gpointer mem_ptr,
							  GtkCTreeNode *
							  parent,
							  GtkCTreeNode *
							  sibling,
							  gchar * text[],
							  gboolean is_leaf,
							  gboolean expanded);

	void cond_list_move_node (CondList * cdlist,
				  GtkCTreeNode * node,
				  GtkCTreeNode * new_parent,
				  GtkCTreeNode * new_sibling);

	void cond_list_remove_node (CondList * cdlist, GtkCTreeNode * node);

	void cond_list_remove_node_from_data (CondList * cdlist,
					      gpointer mem_ptr);

	void cond_list_update_node (CondList * cdlist,
				    GtkCTreeNode * node, gchar * text[]);

	GtkCTreeNode *cond_list_find_node_from_data (CondList * cdlist,
						     gpointer mem_ptr);

	gpointer cond_list_find_data_from_node (CondList * cdlist,
						GtkCTreeNode * node);
#ifdef __cplusplus
}
#endif				/* __cplusplus */

#endif
